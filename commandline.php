<?php

// This is used by the "dlib" commandline
// It is either used (require) directly (only for the "install" command) or (most other commands) through a web request (server-side).

require_once 'demosphere/demosphere-commandline.php';
demosphere_commandline_config();
// Most commandline commands, are actually executed server-side (see doc in dlib_commandline_exec())
if(php_sapi_name() == "cli"){return;}
require_once 'dlib/commandline-tools.php';
dlib_commandline_exec();

?>