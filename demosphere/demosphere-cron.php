<?php

//! This function doesn't do the actual cron work, but dispatches
//! calls to functions in other components that do the actual work.
function demosphere_cron()
{
	global $base_url,$demosphere_config,$custom_config;

	require_once 'dlib/dlib-cron.php';
	dlib_cron_setup('root@localhost',$demosphere_config['admin_email'],
					$demosphere_config['hosted'] ?
					  "You should read the output of your cron:\n".
					  "tail -n 1000 /var/log/demosphere/".$demosphere_config['site_id']."-cron.log\n" : '');

	// Set this to help demosphere_cron_cleanup_dbsessions()
	$_SESSION['this-is-a-demosphere-cron-session']=true;

	// ********** simple cron tasks, run every time cron is called (normally hourly)

	echo date('r')." : cron : before demosphere_config_check_cron\n";
	require_once 'demosphere-config-check.php';
	demosphere_config_check_cron();
	echo date('r')." : cron : after  demosphere_config_check_cron\n";

	echo date('r')." : cron : before demosphere_user_cron\n";
	require_once 'demosphere-user.php';
	demosphere_user_cron();
	echo date('r')." : cron : after  demosphere_user_cron\n";

	echo date('r')." : cron : before mail_import_cron\n";
	require_once 'mail-import/mail-import.php';
	mail_import_cron();
	echo date('r')." : cron : after  mail_import_cron\n";

	echo date('r')." : cron : before demosphere_stats_awstats_cron\n";
	require_once 'demosphere/demosphere-stats.php';
	demosphere_stats_awstats_cron();
	echo date('r')." : cron : after  demosphere_stats_awstats_cron\n";

	echo date('r')." : cron : before demosphere_stats_daily_visits_update\n";
	demosphere_stats_daily_visits_update();
	echo date('r')." : cron : after  demosphere_stats_daily_visits_update\n";

	echo date('r')." : cron : before demosphere_stats_day_rankings\n";
	demosphere_stats_day_rankings();
	echo date('r')." : cron : after  demosphere_stats_day_rankings\n";

	echo date('r')." : cron : before demosphere_opinion_cron\n";
	require_once 'demosphere-opinion.php';
	demosphere_opinion_cron();
	echo date('r')." : cron : after  demosphere_opinion_cron\n";

	echo date('r')." : cron : before demosphere_email_subscription_cron\n";
	require_once 'demosphere-email-subscription.php';
	demosphere_email_subscription_cron();
	echo date('r')." : cron : after  demosphere_email_subscription_cron\n";

	echo date('r')." : cron : before demosphere_emails_bounces_past_24h\n";
	require_once 'demosphere-stats.php';
	require_once 'demosphere-emails.php';
	demosphere_emails_bounces_past_24h();
	echo date('r')." : cron : after  demosphere_emails_bounces_past_24h\n";

	echo date('r')." : cron : before demosphere_emails_received_update_aliases\n";
	require_once 'demosphere-emails.php';
	demosphere_emails_received_update_aliases();
	echo date('r')." : cron : after  demosphere_emails_received_update_aliases\n";

	echo date('r')." : cron : before demosphere_event_repetition_cron\n";
	require_once 'demosphere-event-repetition.php';
	demosphere_event_repetition_cron();
	echo date('r')." : cron : after  demosphere_event_repetition_cron\n";

	echo date('r')." : cron : before demosphere_cron_cleanup_tmp_files\n";
	demosphere_cron_cleanup_tmp_files();
	echo date('r')." : cron : after  demosphere_cron_cleanup_tmp_files\n";

	echo date('r')." : cron : before demosphere_image_proxy_cron\n";
	require_once 'demosphere-misc.php';
	demosphere_image_proxy_cron();
	echo date('r')." : cron : after  demosphere_image_proxy_cron\n";

	echo date('r')." : cron : before dlib_html_data_url_cron\n";
	require_once 'dlib/html-tools.php';
	dlib_html_data_url_cron();
	echo date('r')." : cron : after  dlib_html_data_url_cron\n";

	echo date('r')." : cron : before demosphere_cron_cleanup_tmp_misc\n";
	demosphere_cron_cleanup_tmp_misc();
	echo date('r')." : cron : after  demosphere_cron_cleanup_tmp_misc\n";

	echo date('r')." : cron : before demosphere_slm_sites_update\n";
	require_once 'demosphere-external-events.php';
	demosphere_slm_sites_update();
	echo date('r')." : cron : after  demosphere_slm_sites_update\n";

	echo date('r')." : cron : dlib_throttle_cron\n";
	require_once 'dlib/mail-and-mime.php';
	dlib_throttle_cron();
	echo date('r')." : cron : dlib_throttle_cron\n";

	echo date('r')." : cron : demosphere_panel_ptoken_software_forum_update\n";
	require_once 'demosphere-panel.php';
	demosphere_panel_ptoken_software_forum_update();
	echo date('r')." : cron : demosphere_panel_ptoken_software_forum_update\n";

	if(isset($custom_config['cron']))
	{
		echo date('r')." : cron : before custom\n";
		$custom_config['cron']();
		echo date('r')." : cron : after  custom\n";
	}

	echo date('r')." : cron : before Event::cleanupTrashBinCron\n";
	Event::cleanupTrashBinCron();
	echo date('r')." : cron : after  Event::cleanupTrashBinCron\n";

	echo date('r')." : cron : before delete old featured events\n";
	db_query("DELETE FROM variables USING variables,Event WHERE ".
			 "variables.type='fp-featured' AND Event.id=variables.name AND Event.startTime<%d",
			 Event::today());
	echo date('r')." : cron : after  delete old featured events\n";

	echo date('r')." : cron : before demosphere_self_edit_cleanup_old\n";
	require_once 'demosphere-self-edit.php';
	demosphere_self_edit_cleanup_old();
	echo date('r')." : cron : after  demosphere_self_edit_cleanup_old\n";

	echo date('r')." : cron : before demosphere_external_events_cron\n";
	require_once 'demosphere-external-events.php';
	demosphere_external_events_cron();
	echo date('r')." : cron : after  demosphere_external_events_cron\n";

	echo date('r')." : cron : before demosphere_built_in_pages_cron\n";
	require_once 'demosphere-built-in-pages.php';
	demosphere_built_in_pages_cron();
	echo date('r')." : cron : after  demosphere_built_in_pages_cron\n";

	echo date('r')." : cron : before demosphere_emails_hosted_fetch_data\n";
	require_once 'demosphere-emails.php';
	// if this fails cron will stop and we will get a message
	demosphere_emails_hosted_fetch_data();
	echo date('r')." : cron : after  demosphere_emails_hosted_fetch_data\n";

	echo date('r')." : cron : before clear cache for frontpage\n";
	demosphere_page_cache_clear($base_url.'/');
	demosphere_page_cache_clear($base_url.'/?%');
	demosphere_page_cache_clear($base_url.'/widget-html%');
	demosphere_page_cache_clear($base_url.'/widget-js%');
	demosphere_page_cache_clear($base_url.'/events.xml%');
	demosphere_page_cache_clear($base_url.'/events.ics%');
	echo date('r')." : cron : after  clear cache for frontpage\n";

	echo date('r')." : cron : before cleanup_dbsessions_hourly\n";
	demosphere_cron_cleanup_dbsessions_hourly();
	echo date('r')." : cron : after  cleanup_dbsessions_hourly\n";

	// ********** other cron tasks, that need to be run at specific intervals
 
	// ******* cleanup_dbsessions cron
	if(dlib_cron_can_start('cleanup_dbsessions_daily','daily'))
	{
		demosphere_cron_cleanup_dbsessions_daily();
		dlib_cron_finished('cleanup_dbsessions_daily');
	}

	// ******* place_cleanup cron
	if(dlib_cron_can_start('place_cleanup','daily'))
	{
		require_once 'demosphere-place.php';
		demosphere_place_cron();
		dlib_cron_finished('place_cleanup');
	}

	// ******* text matching cron
	if(dlib_cron_can_start('text_matching_cleanup','daily'))
	{
		require_once 'text-matching/text-matching.php'; 
		text_matching_cron();
		dlib_cron_finished('text_matching_cleanup');
	}

	// ******* comments cron
	if(dlib_cron_can_start('comments','daily'))
	{
		require_once 'dlib/comments/comments.php'; 
		comments_cron();
		dlib_cron_finished('comments');
	}

	// ******* feed import cron
	if(dlib_cron_can_start('feed_import',['period'=>'daily','times'=>['8:00','11:00','17:00','20:00']]))
	{
		require_once 'feed-import/feed-import.php';
		feed_import_cron();
		dlib_cron_finished('feed_import');
	}

	// ******* page watch cron
	if(dlib_cron_can_start('page_watch',['period'=>'daily','times'=>['8:00','13:00','19:00','23:00']]))
	{
		require_once 'page-watch/page-watch.php';
		page_watch_cron();
		dlib_cron_finished('page_watch');
	}

	// ******* search cron
	if(dlib_cron_can_start('search','monthly'))
	{
		require_once 'demosphere-search.php'; 
		demosphere_search_cron();
		dlib_cron_finished('search');
	}


	dlib_cron_shutdown();
}


// ***************************************
// Misc cron actions
// ***************************************

//! Delete any files in tmp_dir and others that are older than a few days.
//! Normaly files in tmp_dir should have a lifespan of a few minutes.
function demosphere_cron_cleanup_tmp_files()
{
	global $demosphere_config;
	$tmpDir=$demosphere_config['tmp_dir'];
	$old=time()-3600*24*15;// delete more than 15 days old
	foreach(scandir($tmpDir) as $dirEntry)
	{
		if($dirEntry=='.' || $dirEntry=='..' || $dirEntry=='.svn'){continue;}
		$mtime=filemtime($tmpDir.'/'.$dirEntry);
		if($mtime<$old)
		{
			system('rm -rf '.escapeshellarg($tmpDir.'/'.$dirEntry));
		}
	}
}

//! Delete empty anonymous sessions from database
//! Since 8/2019 we write all sessions, even for cached pages. 
//! This can lead to a lot of entries on busy days. We need to cleanup quickly.
function demosphere_cron_cleanup_dbsessions_hourly()
{
	// Delete short lived (<60s) anonymous sessions that have no data after 6h
	// Note: since sessions are only re-writen every 3 minutes, in practice this means 3min not 60s.
	db_query("DELETE FROM dbsessions WHERE data='' AND uid=0 AND last_access<first_access+60 AND last_access<%d",time()-3600*6);

	// Delete anonymous sessions that have no data after 36h
	db_query("DELETE FROM dbsessions WHERE data='' AND uid=0 AND last_access<%d",time()-3600*36);
}

//! Delete stale session information from database.
//! Some operations here may remove all data from some sessions allowing them to be deleted on next hourly cron.
function demosphere_cron_cleanup_dbsessions_daily()
{
	// Delete sessions created during demosphere_cron() 
	$data=db_one_col("SELECT n,data FROM dbsessions WHERE data LIKE '%%this-is-a-demosphere-cron-session%%' AND uid=0 AND last_access<%d",time()-3600*24);
	foreach($data as $n=>$data)
	{
		$decoded=dbsessions_session_decode($data);
		if(isset($decoded['this-is-a-demosphere-cron-session'])){db_query("DELETE FROM dbsessions WHERE n=%d",$n);}
	}

	// After one week, delete dlib_messages
	$data=db_one_col("SELECT n,data FROM dbsessions WHERE data LIKE '%%dlib_messages%%' AND last_access<%d",time()-3600*24*7);
	foreach($data as $n=>$data)
	{
		$decoded=dbsessions_session_decode($data);
		if(isset($decoded['dlib_messages']))
		{
			unset($decoded['dlib_messages']);
			db_query("UPDATE dbsessions SET data='%s' WHERE n=%d",dbsessions_session_encode($decoded),$n);
		}
	}

	// Cleanup public-form-events, selfEdit, demosphere_event_ajax_edit_update_lastlog
	$tmpFIXME=strtotime('sept 4 2019');// remove this in 10/2019
	$data=db_one_col("SELECT n,data FROM dbsessions WHERE ".
					 "data LIKE '%%public-form-events%%' OR ".
					 "data LIKE '%%selfEdit%%' OR ".
					 "data LIKE '%%demosphere_event_ajax_edit_update_lastlog%%'");
	foreach($data as $n=>$data)
	{
		$needsSave=false;
		$decoded=dbsessions_session_decode($data);
		if(isset($decoded['public-form-events']))
		{
			foreach(array_keys($decoded['public-form-events']) as $k)
			{
				if(($decoded['public-form-events'][$k]['time'] ?? $tmpFIXME)< time()-3600*24*30)
				{
					unset($decoded['public-form-events'][$k]);
					$needsSave=true;
				}
			}
			if(!count($decoded['public-form-events'])){unset($decoded['public-form-events']);$needsSave=true;}
		}
		if(isset($decoded['selfEdit']))
		{
			foreach(array_keys($decoded['selfEdit']) as $k)
			{
				$found=db_result("SELECT COUNT(*) FROM self_edit WHERE password='%s'",$decoded['selfEdit'][$k]);
				if($found==0)
				{
					unset($decoded['selfEdit'][$k]);
					$needsSave=true;
				}
			}
			if(!count($decoded['selfEdit'])){unset($decoded['selfEdit']);$needsSave=true;}
		}
		if(isset($decoded['demosphere_event_ajax_edit_update_lastlog']) && 
		   (     $decoded['demosphere_event_ajax_edit_update_lastlog'][0] ?? 0) < time()-3600)
		{
			unset($decoded['demosphere_event_ajax_edit_update_lastlog']);
			$needsSave=true;
		}

		if($needsSave){db_query("UPDATE dbsessions SET data='%s' WHERE n=%d",dbsessions_session_encode($decoded),$n);}
	}

	// Delete very old (1 year) sessions for admins and mods (might be an old, unused device... not great for security)
	$uids=db_one_col('SELECT id FROM User WHERE role IN (%d,%d)',User::roleEnum('admin'),User::roleEnum('moderator'));
	if(count($uids))
	{
		db_query("DELETE FROM dbsessions WHERE uid IN (".implode(',',$uids).")  AND last_access<%d",strtotime("1 year ago"));
	}

	// FIXME: there are lots (>5000) of sessions with permission denied messages (pas les droits nécessaires pour)
	// Probably crawlers. Need to fix that.
}

//! Cleanup misc tmp stuff
function demosphere_cron_cleanup_tmp_misc()
{
	// Cleanup temporary old event stored in variables. see demosphere_event_repetition_copy_form()
	$repCopies=variable_get_all('event-repetition-copy');
	foreach($repCopies as $name=>$repCopy)
	{
		if($repCopy->extraData['repetition_copy_ts']<time()-3600*24*48)
		{
			variable_delete($name,'event-repetition-copy');
		}
	}

	// Cleanup tmpdocs
	foreach(variable_get_all('tmpdoc') as $id=>$doc)
	{
		if($doc['created']<time()-3600*24)
		{
			$tm=TMDocument::getByTid('tmpdoc',$id);
			if($tm!==null){$tm->delete();}
			variable_delete($id,'tmpdoc');
		}
	}

}

?>