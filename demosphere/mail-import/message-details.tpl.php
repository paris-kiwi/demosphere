<h3 class="mail-import-top-title">_(Detailed information:) (<a href="$message->url()">$message->id</a>)</h3>
<ul id="details-info">
	<li><strong>Id:               </strong> $message->id             </li>
	<li><strong>Imap mailbox:     </strong> $message->imapMailbox    </li>
	<li><strong>Imap uid:         </strong> $message->imapUid        </li>
	<li><strong>Subject:          </strong> $message->subject        </li>
	<li><strong>Full to:          </strong> $message->to             </li>
	<li><strong>Simple to:        </strong> $message->firstTo        </li>
	<li><strong>CC:               </strong> <?: $message->getHeader('cc') ?></li>
	<li><strong>From:             </strong> $message->from           </li>
	<li><strong>Headers hash:     </strong> $message->headersHash    </li>
	<li><strong>Date fetched:     </strong> <?: dcomponent_format_date('full-date-time',$message->dateFetched) ?></li>
	<li><strong>Date sent:        </strong> <?: dcomponent_format_date('full-date-time',$message->date       ) ?></li>
	<li><strong>Message id:       </strong> $message->messageIdHeader</li>
	<li><strong>In reply to:      </strong> $message->inReplyToHeader</li>
	<li><strong>Subject hash:     </strong> $message->subjectHash    </li>
	<li><strong>Thread nb:        </strong> $message->threadNb       </li>
	<li><strong>List id:          </strong> $message->listId         </li>
	<li><strong>Importance:       </strong> $message->importance     </li>
	<li><strong>Source:           </strong> $message->source         </li>
	<li><strong>Is sent:          </strong> <?: $message->isSent        ? 'yes' : 'no' ?></li>
	<li><strong>Finished reading: </strong> <?: $message->finishedReading   ? dcomponent_format_date('full-date-time',$message->finishedReading  ) : 'no'   ?></li>
	<li><strong>Higlighted:       </strong> <?: $message->isHighlighted ? 'yes' : 'no' ?></li>
	<li><strong>First future date:</strong> <?: $message->firstFutureHLDate ? dcomponent_format_date('full-date-time',$message->firstFutureHLDate) : 'none' ?></li>
</ul>
<h3>_(Attachments / parts)</h3>
<table id="details-attachments">
	<tr>
		<th>name</th>
		<th>type</th>
		<th>size</th>
		<th>children</th>
		<th></th>
	</tr>
	<? foreach($message->parts as $name=>$part){ ?>
		<tr>
			<td>$part['name']</td>
			<td>$part['type']</td>
			<td>$part['size']</td>
			<td><?: implode(',',$part['children'])?></td>
			<td><?: val($part,'filename','') ?></td>
		</tr>
	<?}?>
</table>
<h3>_(Full headers:)</h3>
<pre id="message-headers">
<?= filter_xss(preg_replace("@^[a-z0-9-]+:@im",'<strong>$0</strong>',ent(@iconv("utf-8","utf-8//IGNORE",$message->headers)))) ?>
</pre>
<a href="$scriptUrl/message/$message->id/display-raw">_(Download raw mail data)</a>


