<?php
require_once 'demosphere/test/test-demosphere.php';

function test_mail_import()
{
	test_mail_import_basic();
	test_mail_import_mail_rules();
}

function test_mail_import_basic()
{
	global $mail_import_config;

	$testDir='demosphere/mail-import/test';

	system("rm -f ".$mail_import_config['cache_dir'].'/cached_*');
	db_query("DELETE FROM MailRule WHERE matchString IN ('sender@example.org','receiver@demosphere.net') OR matchString='test 1'");
	db_query("DELETE FROM Message WHERE messageIdHeader IN ('<20100211160501.GA19362@localhost>','<20100211160706.GB19362@localhost>')");


	$raw=file_get_contents($testDir.'/testmail-1');
	$m=Message::createFromRaw($raw);
	$id1=$m->id;
	test_equals($m->to,'receiver@demosphere.net');
	test_equals($m->from,'sender@example.org');
	test_equals($m->subject,'test 1 - simple text');
	test_equals($m->date,1265904301);
	$raw1=$m->getRawMail();
	test_equals(test_mail_import_fix_raw($raw),$raw1);

	$html=$m->getFullHtml();
	test_contains($html,'id="demosphere-message"');
	test_contains($html,'id="demosphere-subject"');
	test_contains($html,'This is a simple plain text mail.');
	$parts=$m->parts;
	test_assert(isset($parts['p-0']));
	test_equals($parts['p-0']['type'],'text/plain');
	test_equals($parts['p-0']['children'],[]);
	test_equals($parts['p-0']['size'],37);
	test_contains($m->headers,'Received:');
	$m->delete();

	// *** mail with image attachment

	$m=Message::createFromRaw(file_get_contents($testDir.'/testmail-2'));
	$id2=$m->id;
	$parts=$m->parts;
	$html=$m->getFullHtml();
	test_contains($html,'<img');
	test_contains($html,'This is a mail with one small image attachment.');
	test_equals($parts['p-0-1']['type'],'image/png');
	test_equals(md5($m->getPartContents('p-0-1')),'af9ec1bde75727a5f7b68c8d4ec5bb9a');
	test_equals($m->isHighlighted,false);
	test_equals($m->firstFutureHLDate,false);
	test_equals($m->importance,'direct');
	test_equals($m->parts['p-0-1']['filename'],'tiny.png');
	$m->delete();

	// *** future date highlight

	$f=file_get_contents($testDir.'/testmail-6');
	$date=time()+3600*24*2;
	$date=mktime(3,33,0, date('m',$date),date('d',$date),date('Y',$date));
	$f=str_replace('xx/yy/zzzz',date('d/m/Y',$date),$f);
	$m=Message::createFromRaw($f);
	test_different($m->isHighlighted,false);
	test_equals($m->firstFutureHLDate,$date);

	// test rehighlightAll
	$m->firstFutureHLDate=time()-3600*24*2;
	$m->save();
	$m=Message::fetch($m->id);
	test_different($m->firstFutureHLDate,$date);
	Message::rehighlightAll();
	$m=Message::fetch($m->id);
	test_equals($m->firstFutureHLDate,$date);
	$m->delete();

	// *** test cleanupCachedFiles
	test_assert(file_exists($mail_import_config['cache_dir'].'/cached_'.$id2.'_p-0-1'));
	Message::cleanupCachedFiles();
	test_assert(file_exists($mail_import_config['cache_dir'].'/cached_'.$id2.'_p-0-1'));
	$mail_import_config['cache_time']=-1;
	Message::cleanupCachedFiles();
	test_assert(!file_exists($mail_import_config['cache_dir'].'/cached_'.$id2.'_p-0-1'));

	// *** test open raw 
	list($imap,$fname)=mail_import_imap_open_raw($raw);
	$overviews=imap_fetch_overview($imap,"1");
	test_equals($overviews[0]->subject,'test 1 - simple text');

	system("rm -f ".$mail_import_config['cache_dir'].'/cached_*');
}

function test_mail_import_fix_raw($raw)
{
	// Remove first From line (envelope From)
	$rawFixed=preg_replace("@^From .*\r\n@sU",'',$raw);
	// Remove headers that are mailbox specific
	$rawFixed=preg_replace("@\r\nStatus:.*\r\n@iU","\r\n",$rawFixed);
	$rawFixed=preg_replace("@\r\nContent-length:.*\r\n@iU","\r\n",$rawFixed);
	//file_put_contents('/tmp/r0',$rawFixed);
	//file_put_contents('/tmp/r1',$raw1);
	return $rawFixed;
}

function test_mail_import_mail_rules()
{
	$testDir='demosphere/mail-import/test';

	db_query("DELETE FROM MailRule WHERE matchString IN ('sender@example.org','receiver@demosphere.net') OR matchString='test 1'");

	$raw=file_get_contents($testDir.'/testmail-1');
	$m=Message::createFromRaw($raw);
	test_equals($m->importance,'direct');	
	$rule=new MailRule(['matchType' =>MailRule::matchTypeEnum('from')       ,'matchString'=>'sender@example.org',
						'actionType'=>MailRule::actionTypeEnum('importance'),'actionString'=>'ordinary']);
	$rule->save();
	$m=Message::fetch($m->id);
	test_equals($m->importance,'ordinary');	
	$rule->delete();
	$m=Message::fetch($m->id);
	test_equals($m->importance,'direct');	

	$rule=new MailRule(['matchType' =>MailRule::matchTypeEnum('subject'),'matchString'=>'test 1',
						'actionType'=>MailRule::actionTypeEnum('importance'),'actionString'=>'ordinary']);
	$rule->save();
	$m=Message::fetch($m->id);
	test_equals($m->importance,'ordinary');	

	// newer rules have higher priority than older ones
	$rule1=new MailRule(['matchType' =>MailRule::matchTypeEnum('to'),'matchString'=>'receiver@demosphere.net',
						 'actionType'=>MailRule::actionTypeEnum('importance'),'actionString'=>'trash']);
	$rule1->save();
	$m=Message::fetch($m->id);
	test_equals($m->importance,'trash');	


	$rule->delete();
	$rule1->delete();
	$m->delete();
}

?>