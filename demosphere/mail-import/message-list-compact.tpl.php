<table class="message-list-compact">
	<?foreach($messages as $message){?>
		<? extract($message->viewDataCompact()); ?>
		<tr class="<?: $message->isHighlighted   ? 'highlighted'  : 'nothighlighted'?>
				   <?: $message->finishedReading ? 'finished '    : '' ?>
				   <?: $message->isAutoHidden()  ? 'auto-hidden ' : '' ?>
				   importance-$message->importance
					">
			<td class="finish" onclick="dcomponent_finish_dcitem(this,$message->id,'$scriptUrl$finishUrl')" >
			</td>
			<td class="importance icon-$message->importance" title="<?: t($message->importance) ?>">
			</td>
			<td class="address" title="$message->from">
				<a class="from overflow-popup" 
				   href="$base_url/search?search=%22<?: dlib_email_address_parts($message->isSent ? $message->firstTo : $message->from)['address'] ?>%22">
					$shortAddress 
				</a>
			</td>
			<td class="list">
				<? if($message->listId!=''){?> 
					<a title="<?:Message::mailingListName($message->listId)?>" 
					   href="$scriptUrl?display=list&listId=<?: urlencode($message->listId) ?>">
						_(list)
					</a>
				<?}?>
			</td>
			<td class="subject">
				<a href="$message->url()">
					<?: strlen(trim($message->subject))!=0 ? 
					$message->subject :
					'('.t('no subject').')' ?>
				</a>
			</td>
			<td class="attachments">
			</td>
			<td class="date">
				<a href="$message->url()">
					<?: dcomponent_format_date('short-date-time',$message->dateFetched) ?>
				</a>
			</td>
		</tr>
	<?}?>
</ul>
