
<? // ************** extra title right ******** ?>

<? begin_part('titleRight') ?>
	<? if($nbInThread){ ?>
		<a class="thread-link" href="$scriptUrl?display=thread&threadNb=$item->threadNb">_(thread) ($nbInThread)</a>
	<?}?>
	<a class="reply" href="$scriptUrl/send-mail/$item->id?action=reply" title="_(Reply to this email)"></a>
	<div class="reply-options dropdown dropdown-left" title="_(Reply to all or forward)">
		<span class="dropdown-button"></span>
		<ul>
			<li><a href="$scriptUrl/send-mail/$item->id?action=reply-all">_(Reply to all)</a></li>
			<li><a href="$scriptUrl/send-mail/$item->id?action=forward"  >_(Forward)</a></li>
		</ul>
	</div>
<? end_part('titleRight') ?>

<? // ************** info-left ******** ?>

<? begin_part('infoLeftBottom') ?>
	<li class="infoPart importance importance-$item->importance">
		<div class="dropdown importance-menu">
			<span class="dropdown-button icon-$item->importance"><?: t($item->importance)?></span>
			<ul>
				<? $tld=preg_replace('@^.*([^.]+\.[^.]+)$@U','$1',parse_url($base_url)['host']);?>
				<?if($item->importance=='direct'){?>
					<? foreach(['ordinary','trash'] as $newImportance){?>
						<? $dispNewImportance='<strong>'.ent(t($newImportance)).'</strong>'; ?>
						<? if($item->listId!=''){?>
							<li data-importance="$newImportance" data-match-type="list-id" class="icon-$newImportance">
								<?= MailRule::description('list-id','','importance',$dispNewImportance) ?>
							</li>
						<?}else
						   if($item->getMailingListSubject()!==false){?>
							<li data-importance="$newImportance" data-match-type="subject" class="icon-$newImportance">
								<?= MailRule::description('subject',ent($item->getMailingListSubject()),'importance',$dispNewImportance) ?>
							</li>
						<?}?>
						<? if($item->firstTo===''){?>
							<li data-importance="$newImportance" data-match-type="from-undisclosed" class="icon-$newImportance">
								<?= MailRule::description('from-undisclosed',ent($item->from),'importance',$dispNewImportance) ?>
							</li>
						<?}else
						   if(($user->email==='' || strpos($item->from,$user->email)===false) && 
							  strpos($item->from,$demosphere_config['contact_email'])===false && 
							  strpos($item->from,$tld)===false){?>
							<li data-importance="$newImportance" data-match-type="from" class="icon-$newImportance">
								<?= MailRule::description('from',ent($item->from),'importance',$dispNewImportance) ?>
							</li>
						<?}?>
						<? if($item->firstTo!=='' &&
							  strpos($item->firstTo,$user->email)===false && 
							  strpos($item->firstTo,$demosphere_config['contact_email'])===false && 
							  strpos($item->firstTo,$tld)===false){?>
							<li data-importance="$newImportance" data-match-type="to" class="icon-$newImportance">
								<?= MailRule::description('to',ent($item->firstTo),'importance',$dispNewImportance) ?>
							</li>
						<?}?>
					<?}?>
				<?}else
				  if($importanceRule!==false){?>
					<li data-delete-rule="$importanceRule->id">
						_(Remove rule that says:) 
						<?= MailRule::description($importanceRule->matchType,$importanceRule->matchType!=4 ? ent($importanceRule->matchString) : '',
												  $importanceRule->actionType,'<strong>'.ent(t($importanceRule->actionString)).'</strong>')?>
					</li>	
				<?}?>
     			<li class="advanced-rule">
     				<a href="$base_url/mail-import/message/$item->id/manage-rules">_(advanced rules)</a>
     			</li>
			</ul>
		</div>
	</li>

	<li class="infoPart import">
		<a target="_blank" href="$scriptUrl/message/$item->id/import-to-demosphere" title="_(Create event from this email)">
			_(import)
		</a>
	</li>

	<li class="infoPart date <?= $item->date<time()-3600*24*20 ? 'old-date' :'' ?>">
		<div class="relative"><?: dcomponent_format_date('relative-short-full',$item->dateFetched) ?></div>
		<div class="short">
			<?: time()-$item->dateFetched<3600*24*180 ? 
					dcomponent_format_date('day-month-abbrev',$item->dateFetched).' '.dcomponent_format_date('time',$item->dateFetched) : 
					dcomponent_format_date('short-day-month-year',$item->dateFetched) ?>
		</div>
		<? if($item->date!=0 && abs($item->dateFetched-$item->date)>3600*5 && $item->date<time()) { ?>
			<div class="strange">
				<?: time()-$item->date<3600*24*180 ? 
						dcomponent_format_date('day-month-abbrev',$item->date).' '.dcomponent_format_date('time',$item->date) : 
						dcomponent_format_date('short-day-month-year',$item->date) ?>
			</div>
		<?}?>
	</li>

	<li class="infoPart privacy" 
		title="_(Click here to unblock images. Privacy warning: by unblocking images, the sender might be notified that you have read this email. )">
		<? if($nbPrivacyBlockedImages!=0){ ?><?: t('@nb blocked images',['@nb'=>$nbPrivacyBlockedImages])?><?}?>
	</li>

	<? if($item->finishedReading){?>
		<li class="infoPart">
			_(finished at:)<br/>
			<?: dcomponent_format_date('shorter-date-time',$item->finishedReading)?>
		</li>
	<?}?>

	<li class="infoPart more-info">
		<a href="$scriptUrl/message/$item->id/display-details">+</a>
	</li>
<? end_part('infoLeftBottom') ?>

<? // ************** info-top-line0 ******** ?>

<? begin_part('infoTopLinePrepend') ?>
	<span class="infoPart tofrom">
		<a class="from overflow-popup" href="$base_url/search?search=%22<?: dlib_email_address_parts($item->from)['address'] ?>%22">
			<? $fromParts=dlib_email_address_parts($item->from); ?>
			<? if(isset($fromParts['clean-name'])){?>
				<strong class="name">$fromParts['clean-name']</strong> <span class="address-part">&lt;$fromParts['address']&gt;</span>
			<?}else{?>
				<strong>$fromParts['address']</strong>
			<?}?>
		</a>
		<?if($item->firstTo!==''){?>
			<span class="right-arrow"></span>
			<span class="to overflow-popup">$item->firstTo</span>
		<?}?>
		<? if($nbrecipients>1){?> 
			<span class="nbrecipients" title="$item->to; <?: $item->getHeader('cc') ?>">($nbrecipients)</span> 
		<?}?>
		<? if($item->listId!=''){?> 
			<a class="list-link" title="$item->listId" href="$scriptUrl?display=list&listId=<?: urlencode($item->listId) ?>">
				_(list)
			</a>
		<?}?>
	</span>
<? end_part('infoTopLinePrepend') ?>

<? // ************** info-top-line-1 ******** ?>

<? begin_part('infoTopLine0After') ?>
	<? if(count($attachments)){ ?>
		<p class="info-top line-1">
			<span class="attachments">
				<span class="attachments-label" title="_(attachments)"></span>
				<?foreach($attachments as $attachment){?>
					<a  class="infoPart attachment" href="$attachment['url']" target="_blank" 
						title="$attachment['fullType'] (<?: dlib_size_with_units($attachment['size']) ?>)">
							<span class="attachment-type simple-type-$attachment['simpleType']"></span>
							$attachment['filename']
					</a>
				<?}?>
			</span>
		</p>
	<?}?>
<? end_part('infoTopLine0After') ?>

