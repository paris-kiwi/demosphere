(function() {
window.mail_import_send_mail = window.mail_import_send_mail || {};
var ns=window.mail_import_send_mail;// shortcut

$(document).ready(function()
{
	$('#attach').click(function()
	{
		$('.attachment.detached').first().removeClass('detached');
	});
	$('.detach').click(function()
	{
		$(this).parents('.attachment').find('input').val('');
		$(this).parents('.attachment').addClass('detached');
	});

	$('#show-cc').click(function()
	{
		$('#edit-cc-wrapper').show();
		$('#edit-cc').focus();
		$(this).hide();
	});

	$('#show-bcc').click(function()
	{
		$('#edit-bcc-wrapper').show().focus();
		$('#edit-bcc').focus();
		$(this).hide();
	});

	if($('#edit-bcc').val()!=''){$('#edit-bcc-wrapper').show();$('#show-bcc').hide();}
	if($('#edit-cc' ).val()!=''){$('#edit-cc-wrapper' ).show();$('#show-cc' ).hide();}
});


// Exports:
//window.mail_import_send_mail.xyz=xyz;

// end namespace wrapper
}());
