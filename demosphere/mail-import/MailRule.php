<?php
/** \addtogroup mail_import 
 * @{ */

//! A MailRule matches fields in Messages and is used to change their "importance" and "source".
//! This is kept deliberately simple to:
//! - avoid confusing the user
//! - Being able to match rules directly in sql (performance).
class MailRule extends DBObject
{
	//! unique autoincremented id
	public $id;
	static $id_           =['type'=>'autoincrement'];

	//! What fields in Messages does this rule try to match.
	//! "from-undisclosed" matches "from" and also checks that "to"==="undisclosed recipients" (this is a common pattern used to mass-mail)
	public $matchType=0;
	static $matchType_    =['type'=>'enum','values'=>[0=>'from',1=>'from-undisclosed',2=>'to',3=>'subject',4=>'list-id']];

	//! What strings are we looking for in the fields that we want to match. Its use depends on $matchType.
	//! For example, if matchType is "from", then matchString could be an email address that we want to match on the "from" field of Messages. 
	public $matchString=0;
	static $matchString_  =[];

	//! Once we find a Message that matches, what fields should we change in that Message ?
	public $actionType=0;
	static $actionType_    =['type'=>'enum','values'=>[0=>'importance',1=>'source']];

	//! What value should we give to the field that we want to change. Its use depends on $actionType.
	//! For $actionType==='importance' => the importance ("direct", "ordinary" or "trash")
	//! For $actionType==='source'     => the source  (Example: "message received from xyz at $date")
	public $actionString=0;
	static $actionString_  =[];

	//! priority is used to sort rules and determines which rule is applied. It can be positive or negative.
	//! Only the first highest priority rule that matches is applied.
	//! Almost all rules have priority 0 (as of 7/2016, there is no simple way of changing it).
	//! If it is not really used, we should consider removing it.
	public $priority=0;
	static $priority_        =['type'=>'int'];

	// information for database load / save in DBObject
	static $dboStatic=false;

	function __construct(array $values=[])
	{
		foreach($values as $name=>$value)
		{
			if(!isset($this->$name)){continue;}
			$this->$name=$value;
		}
	}

	//! Rule match results are saved in Message.importance and Message.source for performance and convenience.
	//! We need to update Messages each time rules are changed.
	function save()
	{
		// Find previously affected Messages that "need refresh" by using currently stored values
		$oldMatchingMessageIds=[];
		if($this->id)
		{
			list($oldMatchType,$oldMatchString)=array_values(db_array('SELECT matchType,matchString FROM MailRule WHERE id=%d',$this->id));
			$oldMatchingMessageIds=MailRule::findMatchingMessageIdsForVals($oldMatchType,$oldMatchString);
		}

		parent::save();

		$newMatchingMessageIds=$this->findMatchingMessageIds();
		$refreshMessageIds=array_unique(array_merge($oldMatchingMessageIds,$newMatchingMessageIds));

		// re-apply rules
		self::refreshMailRules($refreshMessageIds);
	}

	//! Rule match results are saved in Message.imortance and Message.source for performance and convienience.
	//! We need to update Messages each time rules are deleted.
	function delete()
	{
		// Find previously affected Messages that "need refresh" by using currently stored values
		$oldMatchingMessageIds=[];
		if($this->id)
		{
			list($oldMatchType,$oldMatchString)=array_values(db_array('SELECT matchType,matchString FROM MailRule WHERE id=%d',$this->id));
			$oldMatchingMessageIds=MailRule::findMatchingMessageIdsForVals($oldMatchType,$oldMatchString);
		}

		parent::delete();

		// re-apply rules
		self::refreshMailRules($oldMatchingMessageIds);
	}

	//! refreshMailRules() for each Message in $ids
	//! If there are a lot of messages in $ids, this can be a huge task, as this implies re-saving each mail...
	//! which means reindexing (search), which means fetching data from imap...
	//! For large $ids array, we do not reindex, and postpone it until next cron
	static function refreshMailRules($ids)
	{
		$max=100;
		$ct=0;
		foreach($ids as $id)
		{
			$message=Message::fetch($id,false);
			if($message===null){continue;}
			$message->refreshMailRules();
			$message->save($ct<$max);
			if($ct===$max)
			{
				dlib_message_add('This rule change impacts a lot ('.count($ids).') of messages. '.
								 'Search re-indexing is postponed. Search results (for mails) might be slightly incorrect for an hour.','warning');
			}
			$ct++;
		}
		if($ct>$max)
		{
			$r=variable_get('mail-import-search-reindex','',[]);
			$r=array_unique(array_merge($r,$ids));
			variable_set('mail-import-search-reindex','',$r);
		}
	}

	//! Returns a matchString that can be used for a given $matchType.
	//! For example if we found a Message that we want to trash (spam) based on its "from", this will return the Message's "from" field.
	//! Special case: subject. The matchType 'subject' is a general match on a subject substring.
	//! However, in parts of our interface, we only provide an option to match mailing list subjects ("[example]" if subject is "[example] bla bla bla").
	//! So most of the time we need to return the mailing list subject ([example]) when $matchType=='subject'.
	static function suggestMatchStringFromMessage($message,$matchType,$preferMListSubject=true)
	{
		$res=false;
		switch(ctype_digit((string)$matchType) ? MailRule::$matchType_['values'][$matchType] : $matchType)
		{
		case 'from':             $res=$message->from;   break;
		case 'from-undisclosed': $res=$message->from;   break;
		case 'to':               $res=$message->firstTo;break;
		case 'subject':          return $preferMListSubject ? $message->getMailingListSubject() : $message->subject;
		case 'list-id':          return $message->listId;
		default: fatal('bad match type');
		}
		// For email addresses, only keep actual address not person's name (Name <abcd@example.org>)
		$res=dlib_email_address_parts($res)['address'];
		return $res;
	}

	//! Returns ids of Messages that match $matchType + $matchString.
	static function findMatchingMessageIdsForVals($matchType,$matchString)
	{
		return db_one_col("SELECT id FROM Message WHERE ".
						  ($matchType==0 ? " INSTR(Message.from   ,'%s')!=0 "                        : "").
						  ($matchType==1 ? " INSTR(Message.from   ,'%s')!=0 AND Message.firstTo='' " : "").
						  ($matchType==2 ? " INSTR(Message.firstTo,'%s')!=0 "                        : "").
						  ($matchType==3 ? " INSTR(Message.subject,'%s')!=0 "                        : "").
						  ($matchType==4 ? "       Message.listId ='%s'"                             : "").
						  "",$matchString);
	}

	//! Returns ids of Messages that match this rule.
	function findMatchingMessageIds(){return MailRule::findMatchingMessageIdsForVals($this->matchType,$this->matchString);}

	//! Returns ids of rules that match a Message, ordered by priority and id (highest priority first, highest id first).
	static function findMatchingMailRuleIds($message)
	{
		return db_one_col("SELECT id FROM MailRule WHERE ".
						  "(matchType=0 AND INSTR('%s',matchString)!=0                  ) OR ". 	
						  "(matchType=1 AND INSTR('%s',matchString)!=0 AND '%s'=''      ) OR ". 	
						  "(matchType=2 AND INSTR('%s',matchString)!=0                  ) OR ". 	
						  "(matchType=3 AND INSTR('%s',matchString)!=0                  ) OR ". 	
						  "(matchType=4 AND       '%s'=matchString                      )	". 	
						  "ORDER BY priority DESC, id DESC",
						  $message->from,   
						  $message->from,$message->firstTo,
						  $message->firstTo,
						  $message->subject,
						  $message->listId);
	}

	//! Finds best rules that match a $message and sets $message->importance and $message->source
	static function applyRulesToMessage($message)
	{
		$matchingRules=MailRule::fetchListFromIds(MailRule::findMatchingMailRuleIds($message));

		// *** importance
		$message->importance='direct';
		foreach($matchingRules as $rule)
		{
			if($rule->actionType!==MailRule::actionTypeEnum('importance')){continue;}
			$message->importance=$rule->actionString;
			break;
		}

		// *** source
		$message->source='';
		foreach($matchingRules as $rule)
		{
			if($rule->actionType!==MailRule::actionTypeEnum('source')){continue;}
			$message->source=$rule->actionString;
			break;
		}
	}

	//! Returns a translated text that describes a rule.
	//! $matchString and $actionString can be arbitrary html.
	//! For example, in mail_import_view_message_rule_manager(), $actionString can be an <input...> or a <select>.
	static function description($matchType,$matchString,$actionType,$actionString,$preferMListSubject=true)
	{
		$matchText=false;
		switch(ctype_digit((string)$matchType) ? MailRule::$matchType_['values'][$matchType] : $matchType)
		{
		case 'from':             
			$matchText=t('all mail received <strong>from</strong> !from'                                   ,['!from'       =>$matchString]);
            break;
		case 'from-undisclosed': 
			$matchText=t('all mail received <strong>from</strong> !from and sent to undisclosed recipients',['!from'       =>$matchString]);
            break;
		case 'to':               
			$matchText=t('all mail sent <strong>to</strong> !to'                                           ,['!to'         =>$matchString]);
            break;
		case 'subject':          
			$matchText=$preferMListSubject ? 
				       t('all mail with <strong>mailing list</strong> subject !listSubject'                ,['!listSubject'=>$matchString]) :
			           t('all mail whose <strong>subject</strong> contains !subject'                       ,['!subject'    =>$matchString]);
			break;
		case 'list-id':          
			$matchText=t('this <strong>mailing list</strong>').' '.$matchString;
            break;
		}

		switch($actionType)
		{
		case 'importance':
			return t('Change importance to "!importance" for !match',['!importance'=>$actionString,'!match'=>$matchText]);
		case 'source':
			return t('Change source to "!source" for !match',['!source'=>$actionString,'!match'=>$matchText]);
		}
		return false;
	}

	static function createTable()
	{
		db_query("DROP TABLE IF EXISTS MailRule");
		db_query("CREATE TABLE MailRule (
  `id`           int(11)      NOT NULL auto_increment,
  `matchType`    tinyint(3)   NOT NULL,
  `matchString`  varchar(250) NOT NULL,
  `actionType`   tinyint(3)   NOT NULL,
  `actionString` varchar(250) NOT NULL,
  `priority`        int(11)   NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `matchType`    (`matchType`),
  KEY `matchString`  (`matchString`),
  KEY `actionType`   (`actionType`),
  KEY `actionString` (`actionString`),
  KEY `priority`     (`priority`)
) DEFAULT CHARSET=utf8mb4;");
	}
}

/** @} */

?>