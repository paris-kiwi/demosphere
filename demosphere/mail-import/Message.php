<?php

/** \addtogroup mail_import 
 * @{ */

//! A Message is an email message. It is the main class in mail-import.
//!
//! When a new email is received using imap, a new Message is created. 
//! However, the actual raw email data remains on the imap server. 
//! A Message is just a lightweight, parsed version of the actual imap-stored email.
//! Mime parsing is done using the MimeMessage class.
//! A file based caching mechanism is used for email parts such as message attachments (including text/html).
//! Acces to uncached parts requires accessing the the imap stored email.
class Message extends DBObject
{
	//! unique autoincremented ID. 
	public $id;
	static $id_             =['type'=>'autoincrement'];

	//! Imap mailbox where the full raw data of this message actually resides.
	//! Typically 'inbox' or archived boxes like 'inbox-2016-08-2016-09'
	public $imapMailbox;
	static $imapMailbox_    =[];

	//! Unique imap id inside the mailbox given by $this->imapMailbox
	//! This generally does not change on imap server, but the rfc says it is possible. We detect that case and reindex.
	public $imapUid;
	static $imapUid_        =['type'=>'int'];

	//! Email subject
	public $subject;
	static $subject_        =[];

	//! "To:" email field. This is the full header that may contain several comma separated emails
	//! "undisclosed recipients" is represented as an empty string.
	public $to;
	static $to_             =['type'=>'email'];

	//! First email in the "To:" email field 
	//! This is needed by mail rules.
	//! "undisclosed recipients" is represented as an empty string.
	//! $firstTo is given by imap_fetch_overview. It is slightly different than the first email in To: header.
	//! The imap function does a little cleanup (removes quotes, adds a space before <...>, adds "missinghostname"...)
	public $firstTo;
	static $firstTo_        =['type'=>'email'];

	//! "From:" email field
	public $from;
	static $from_           =['type'=>'email'];

	//! "Date:" email field (timestamp).
	//! This is the date at which the mail was sent (specified by sender).
	public $date;
	static $date_           =['type'=>'timestamp'];

	//! Email headers, containing a lot of info like Cc, Bcc, List-Id ...
	public $headers;
	static $headers_        =[];

	//! Hash of email headers. 
	//! This is supposed to be unique, we use it to track duplicates and fix sync.
	public $headersHash='';
	static $headersHash_    =[];

	//! A description of all mail mime parts (attachments).
	//! See MimeMessage::simplifiedPartList()
	//! name, size (of encoded part), type, sometimes filename, and rarely contents (only for very small contents)
	//! Example: [ 'p-0-1' => ['name'=>'p-0-1', 'type'=>'image/png', 'size'=>123456, 'filename'=>'example.png' ],
	//!            'p-0-2' => [...], ...]
	public $parts;
	static $parts_          =['type'=>'array'];

	//! Timestamp of the moment we actually retrieved this mail (using imap).
	//! This is guaranteed to be reliable. $date could theoretically be wrong or forged.
	//! $dateFetched===0 for invalid Messages (crashed during construction)
	public $dateFetched=0;
	static $dateFetched_    =['type'=>'timestamp'];

	//! Message-Id header, used to track mails in threads.
	//! https://en.wikipedia.org/wiki/Conversation_threading
	//! http://www.sensefulsolutions.com/2010/08/how-does-email-threading-work-in-gmail.html
	public $messageIdHeader='';
	static $messageIdHeader_=[];

	//! In-Reply-To header, used to track mails in threads.
	public $inReplyToHeader='';
	static $inReplyToHeader_=[];

	//! Hash of simplified subject string that helps finding similar subjects
	//! Used to track mails in threads. md5 is ok (no security concerns) and it is faster.
	public $subjectHash='';
	static $subjectHash_    =[];

	//! An arbitrary number given to identify each mail in a conversation thread.
	//! This is set by linkIntoSameThread() and linkIntoThreads() 
	//! linkIntoThreads() is normally called during Message creation.
	public $threadNb=0;
	static $threadNb_       =['type'=>'int'];

	//! List-id or X-list header, used for mailing lists.
	public $listId='';
	static $listId_         =[];

	//! The importance ('direct','ordinary','trash',...) of this mail, computed using rules.
	//! Note: importance depends on rules. Rules can change, so Messages are automatically update when rules are saved/deleted.
	public $importance='direct';
	static $importance_     =[];
	static $importances=['direct','ordinary','trash'];

	//! The source  ('message received from list xyz') of this mail, computed using MailRule's.
	//! Note: source depends on rules. Rules can change, so Messages are automatically update when rules are saved/deleted.
	//! Source can be empty. Use getSourceOrDefault() to get actual source
	public $source;
	static $source_         =[];

	//! Whether this email is sent or received. 
	//! Messages can be sent through send-mail form or, for example, in Demosphere event edit form.
	public $isSent;
	static $isSent_         =['type'=>'bool'];

	//! If the user has finished using the content (clicked on the big cross).
	public $finishedReading;
	static $finishedReading_=['type'=>'timestamp'];

	//! If the content matches some keywords or future dates. 
	//! Note: dates that were "future" during parsing might no longer be
	//! future when  user actually reads the content, so this needs updating (cron calls Message::rehighlightAll()).
	public $isHighlighted=true;
	static $isHighlighted_  =['type'=>'bool'];

	//! If the mail contains highlighted future dates, this is first future date.
	//! Note this needs to be updated regularly.
	public $firstFutureHLDate;
	static $firstFutureHLDate_=['type'=>'timestamp'];

	// ************* non db members
	
	//! mime parsing : temporary MimeMessage object
	private $mime=null;

	//! information for database load / save in DBObject.
	static $dboStatic=false;

	//! This parses the email using MimeMessage and saves this Message to db. 
	//! If parsing fails, it throws an exception before the Message is saved.
	function __construct($imap,$imapUid)
	{
		$this->buildFromImap($imap,$imapUid);
	}

	//! This parses the email using MimeMessage either in rebuild or constructor mode.
	//! Normally called from the contructor. In that case $this->id==0 and it is automatically saved to db
	//! Otherwise, if called on an existing Message ($this->id!=0) then the changed values are NOT saved to db (rebuild).
	//! Rebuild is useful to "reset" a Message (for example from update-db) or while debugging.
	public function buildFromImap($imap,$imapUid)
	{
		global $mail_import_config;

		$isRebuild=isset($this->id) && $this->id!=0;

		$this->mime=new MimeMessage(false,$imap,$imapUid);

		require_once 'mail-import.php';
		$overview=mail_import_imap_fetch_overview($imap,$imapUid);
		
		$this->headers=imap_fetchheader($imap,$imapUid,FT_UID);
		mail_import_imap_errors($this->headers!==false);
		$this->headersHash=hash('SHA256',$this->headers);
		
		// Check for duplicate
		if(!$isRebuild)
		{
			$nbDups=db_result("SELECT COUNT(*) FROM Message WHERE headersHash='%s'",$this->headersHash);
			if($nbDups>0){throw new Exception('Message::__construct: aborting attempt to create a duplicate message');}
		}

		// Subject : our header parse is slightly more reliable than the one in imap overview (we dont fail on astral plane utf8)
		require_once 'dlib/mail-and-mime.php';
		$this->subject=dlib_cleanup_plain_text(dlib_mime_header_entry($this->headers,'subject'));
		if($this->subject===''){$this->subject='('.t('no subject').')';}
		$this->subjectHash=Message::hashSubject($this->subject);
		$this->firstTo=$overview['to'];
		$this->to     =(string)$this->getHeader('to');
		if($this->firstTo===false || 
		   preg_match('/^(undisclosed|unspecified|destinataires?)[- ](recipients?|inconnus?)/i',
					  trim($this->firstTo))>0)
		{
			$this->to='';
			$this->firstTo='';
		}
		$this->from   =$overview['from'];
		$this->date   =$overview['date'];

		$this->messageIdHeader          =$this->getHeader('Message-Id' );
		$this->inReplyToHeader          =$this->getHeader('In-Reply-To');
		$this->listId                   =$this->getHeader('List-id'    );
		if(!$this->listId){$this->listId=$this->getHeader('X-list'     );}

		$this->imapMailbox=mail_import_imap_current_mailbox_name($imap);
		$this->imapUid=$imapUid;
		if(!$isRebuild)
		{
			$this->dateFetched=time();
			$this->finishedReading=0;
		}

		$this->parts=$this->mime->simplifiedPartList();
		// add small contents (<200 bytes) to parts for performance
		// (parts are also cached in files, so this is only to avoid creating and accessing too many tiny files)
		foreach($this->parts as $name=>&$part)
		{
			if($part['size']!==false && $part['size']<200)
			{
				$contents=$this->getPartContents($name,true);
				// Double check. Not really necessary, but we never want to see huge contents here.
				if(strlen($contents)<200){$part['contents']=$this->getPartContents($name);}
			}
		}
		unset($part);

		// save, the following ops need a valid id and leave the message in a fairly reasonable state if they crash
		if(!$isRebuild){$this->save();}

		$this->refreshMailRules();
		$this->rehighlight();
		$this->linkIntoThreads();
		if(!$isRebuild)
		{
			$this->logDemoseventReferences();
		}

		// re-save: this is not strictly necessary, but avoids confusion
		if(!$isRebuild){$this->save();}
	}

	//! Create a mail from a raw mail string.
	//! Used by mail_import_view_send_mail() and other places in Demosphere that send email that should appear as a sent Message
	//! The $rawMail does not need to have a top 'From ...' line.
	static function createFromRaw($rawMail)
	{
		global $mail_import_config;

		require_once 'mail-import.php';
		$imap=mail_import_imap_get_connection();

		// We need $status->uidnext
		$status=imap_status($imap,$mail_import_config['imap_server'].$mail_import_config['imap_inbox'],SA_ALL);
		mail_import_imap_errors(is_object($status));

		// We use message-id to identify mail when we refetch it from imap. 
		// (there are other ways, if this becomes a problem)
		if(!preg_match('@^Message-id: *(<[^>]*>)@mi',$rawMail,$matches))
		{
			throw new Exception('Message::createFromRaw: could not find message-id');
		}
		$rawMessageId=$matches[1];

		$ok=imap_append($imap,$mail_import_config['imap_server'].$mail_import_config['imap_inbox'],$rawMail);
		mail_import_imap_errors($ok,'could not append on imap');
		
		// Now, find the inserted mail on the imap server, and create a Message
		$overviews=imap_fetch_overview($imap,$status->uidnext.':*',FT_UID);
		mail_import_imap_errors(is_array($overviews));
		$message=false;
		foreach($overviews as $overview)
		{
			if($overview->message_id===$rawMessageId)
			{
				$message=new Message($imap,$overview->uid);
			}
		}
		if($message===false){throw new Exception('Message::createFromRaw: failed to reread message inserted into imap');}

		return $message;
	}

	//! Save. 
	//! Special case: optionally don't reindex (slow). This breaks the search index, you will need to deal with that separately.
	function save($reindex=true)
	{
		parent::save();
		if($reindex)
		{
			TMDocument::reindexTid('mail_import',$this->id);
			$this->searchReindex();
		}
	}

	//! Really delete a Message, both from database and from imap server.
	//! This is rarely used. 
	function delete($deleteImap=true)
	{
		require_once 'mail-import.php';
		parent::delete();

		// Actually delete this message on imap
		if($deleteImap!==false)
		{
			$imap=$this->getImapConnection();
			$ok=imap_delete($imap,$this->imapUid,FT_UID);
			mail_import_imap_errors($ok);
			if($deleteImap!=='no-expunge')
			{
				mail_import_imap_errors(imap_expunge($imap));
			}
		}

		require_once 'dcomponent/dcomponent-common.php';
		dcomponent_search_proxy_call_catch('demosphere_search_dx_delete_document','log','mail',$this->id);
		TMDocument::reindexTid('mail_import',$this->id);
	}

	function getImapConnection()
	{
		require_once 'mail-import.php';
		if($this->imapMailbox==''){throw new Exception('Message::getImapConnection: no mailbox for this message');}
		$imap=mail_import_imap_get_connection($this->imapMailbox,$hasReindexed);
		// Very special case (not sure if it really happens) : imap_open detected that uids had changed and reindexed in db.
		// We need to update this object that no longer is in sync with db.
		if($hasReindexed)
		{
			list($this->imapMailbox,$this->imapUid)=array_values(db_array("SELECT imapMailbox,imapUid FROM Message WHERE id=%d",$this->id));
		}
		return $imap;
	}	

	//! Returns the value of single header given by name.
	//! Note: some email have bad leading spaces, so most of the time it is better to trim
	//! imap overview does trim, so we better do it for consistency (example: matching message ids!)
	function getHeader($name,$trim=true)
	{
		require_once 'dlib/mail-and-mime.php';
		$res=dlib_mime_header_entry($this->headers,$name);
		if($trim){$res=trim($res);}
		return $res;
	}

	//! Ordinary email that has no future dates or keywords is considered as "hidden"
	function isAutoHidden()
	{
		return $this->finishedReading==0 && $this->importance==='ordinary' && $this->isHighlighted==0;
	}

	function splitTo()
	{
		if($this->to===''){return [];}
		return explode(',',$this->to);
	}

	function splitCc()
	{
		$cc=$this->getHeader('cc');
		if(trim($cc)===''){return [];}
		return explode(',',$cc);
	}

	function url()
	{
		global $base_url;
		return $base_url.'/mail-import/message/'.$this->id;
	}

	function getRawMail($addTopFromLine=false)
	{
		require_once 'mail-import.php';
		$imap=$this->getImapConnection();
		$temp=fopen('php://temp','rw');
		$ok=imap_savebody($imap,$temp,$this->imapUid,'',FT_UID);
		mail_import_imap_errors($ok);
		rewind($temp);
		$rawMail=stream_get_contents($temp);
		fclose($temp);

		if($addTopFromLine)
		{
			$rawMail="From "."MAILER-DAEMON"." ".date('D M d H:i:s Y')."\r\n".$rawMail;			
		}
		return $rawMail;
	}

	//! Checks if this Message is part of a conversation thread.
	//! If thread is found, it uses linkIntoSameThread() which directly changes Messages in db
	public function linkIntoThreads()
	{
		global $demosphere_config;
		// Interesting ideas here:
		// http://www.sensefulsolutions.com/2010/08/how-does-email-threading-work-in-gmail.html

		//*** If we are a reply to another message, put us into that thread
		if($this->inReplyToHeader!='')
		{
			$messages=Message::fetchList("WHERE messageIdHeader='%s'",$this->inReplyToHeader);
			foreach($messages as $message)
			{
				Message::linkIntoSameThread($this,$message);
			}
		}
		//*** If we have been replied to, put us into that thread
		if($this->messageIdHeader!='')
		{
			$messages=Message::fetchList("WHERE inReplyToHeader='%s'",$this->messageIdHeader);
			foreach($messages as $message)
			{
				Message::linkIntoSameThread($message,$this);
			}
		}

		//*** search for Messages with similar subjects and at least one common address (other than this mailboxes destinations addresses)

		require_once 'mail-import.php';
		static $mailboxDestinations=false;
		if($mailboxDestinations===false){$mailboxDestinations=mail_import_destination_addresses();}

		// helper that returns all addresses used in a Message (from, to , cc - should we add listId ?)
		$allAdresses=function($message)use($mailboxDestinations)
			{
				$res=[];
				$raw=$this->splitTo();
				$raw=array_merge($raw,$this->splitCc());
				$raw[]=$this->from;
				$res=array_map(function($v){return dlib_email_address_parts($v)['address'];},$raw);
				return array_diff($res,$mailboxDestinations);
			};

		$thisAddresses=$allAdresses($this);

		// list of all Messages having a subject similar to this one
		$messages=Message::fetchList("WHERE subjectHash='%s' AND subjectHash!='' AND dateFetched>%d AND dateFetched<%d AND id!=%d LIMIT 100",
									 $this->subjectHash,
									 $this->dateFetched-3600*24*30,
									 $this->dateFetched+3600*24*30,
									 $this->id);
		foreach($messages as $message)
		{
			// now check for some link between messages (common recipients, cc, from...)
			$addresses=$allAdresses($message);
			if(count(array_intersect($thisAddresses,$addresses)))
			{
				Message::linkIntoSameThread($message,$this);
			}
		}
	}

	//! This is for debugging or updating db when code for threads has changed
	static function rebuildAllThreads()
	{
		db_query('UPDATE Message SET threadNb=0');
		$ids=db_one_col("SELECT id FROM Message ORDER BY id ASC");
		foreach($ids as $id)
		{
			$m=Message::fetch($id);
			$m->linkIntoThreads();
		}
	}

	//! Returns the url of an attachment (a part with non text or html contents)
	//! @param the name of this part (something like 'p-0-2-1')
	function getAttachmentUrl($partName)
	{
		global $base_url;
		$part=val($this->parts,$partName);
		if($part===false){fatal("Message::getAttachmentUrl: unknown part");}
		$url='mail-import/message/'.$this->id.'/view-attachment?part='.$partName;
		// always use safe domain as we can't be sure the content type won't be served as html by browser (and contain XSS)
		require_once 'dcomponent/dcomponent-common.php';
		return dcomponent_safe_domain_url($url,true);
	}

	//! Returns mime type for a given part name.
	function getPartType($name){return $this->parts[$name]['type'];}

	//! Returns the actual contents for a given part name.
	//! Text and HTML has been converted to UTF-8. HTML has also been cleaned up with tidy.
	//! Other types are returned raw.
	function getPartContents($name,$truncateTextAndHtml=false)
	{
		global $mail_import_config;
		// Small contents is stored in parts array, for faster access. 
		if(isset($this->parts[$name]['contents']) && $this->parts[$name]['contents']!==false)
		{
			return $this->parts[$name]['contents'];
		}

		// check if contents is cached in file
		$cachedFname=$mail_import_config['cache_dir'].'/cached_'.$this->id.'_'.$name;
		if($this->id && file_exists($cachedFname)){return file_get_contents($cachedFname);}

		// from now on, we need a MimeMessage object
		if($this->mime===null)
		{
			$imap=$this->getImapConnection();
			$this->mime=new MimeMessage(false,$imap,$this->imapUid);
		}

		// actually extract the data (and convert it to UTF-8 if necessary)
		$res=$this->mime->getFixedPartContents($name);

		// Some html has "data:image" urls with large images. 
		// However, 1) large data urls crash highlight regexp 2) they are blocked by CSP 3) They are rare 2/3500 4) Gmail does not display them
		// For now, replace them with a "blocked-image"
		if($this->parts[$name]['type']==='text/html')
		{
			$res=preg_replace_callback('@<img[^>]*>@is',
									   function($matches)
									   {
										   global $mail_import_config;
										   // Only simple regexp allowed here (otherwise regexp crashes)
										   return preg_replace('@src\s*=\s*["\'](data:.*)$@is',
															   ' src="'.$mail_import_config['dir_url'].'/blocked-image.png" ',
															   $matches[0]);
									   },$res);
		}
		
		// On the other hand, large documents wrongly recognized as text can be a problem too...
		if($truncateTextAndHtml===true){$truncateTextAndHtml=800000;}
		if($truncateTextAndHtml!==false && strlen($res)>$truncateTextAndHtml &&
		   ($this->parts[$name]['type']==='text/html' || 
			$this->parts[$name]['type']==='text/enriched' || 
			$this->parts[$name]['type']==='text/plain'))
		{
			$res=mb_substr($res,0,$truncateTextAndHtml).
				'>'.' message too long, truncated...';
		}

		if($this->parts[$name]['type']==='text/html')
		{
			// Do not re-convert to utf-8 (it has already been done by MimeMessage::getFixedPartContents())
			$res=dlib_html_cleanup_page_with_tidy($res,false);
		}

		// cache the contents
		if($this->id){file_put_contents($cachedFname,$res);}
		return $res;
	}

	//! Build an html version of this message including all attachments.
	//! $getHead is used to return markup that should be added to the page's <head> (typically <style> from html parts)
	function getFullHtml($addDemosphereSubject=true,$onlyText=false,&$getHead=false)
	{
		// This shouldn't happen, but it's better than a crash.
		if(!is_array($this->parts)){return 'Error: invalid mail';}

		if($getHead!==false){$getHead='';}

		$out='';
		$out.='<div id="demosphere-message">';
		if($addDemosphereSubject){$out.='<h2 id="demosphere-subject">'.ent($this->subject).'</h2>';}

		// A list multipart/alternatives that we should ignore
		$ignore=MimeMessage::multipartAltIgnore($this->parts);

		require_once 'dlib/mail-and-mime.php';

		// display each part that is not ignored
		$ct=0;
		foreach($this->parts as $partName=>$part)
		{
			if(isset($ignore[$partName])){continue;}

			$mimeType=$part['type'];

			// ignore message boundaries
			if(strpos($mimeType,'multipart/')===0 ||
			   $mimeType=='message/rfc822'         
			   ){continue;}

			$simpleOrMime=$mimeType;
			$simpleType=dlib_mime_to_simple_type($mimeType);
			if($simpleType!==false){$simpleOrMime='('.$simpleType.')';}

			$out.='<div class="demosphere-message-part '.
				               ($ct++ ? '' : 'first-part').' '.
				               ($mimeType==='text/html' || $mimeType==='text/plain' ? 'type-text-or-html': '').' '.
				               ($simpleType!==false ? 'simple-type-'.$simpleType : '').'">';
			switch($simpleOrMime)
			{
			case 'text/plain' :   
			case 'message/delivery-status':
				$contents=$this->getPartContents($partName,true);
				$contents=dlib_cleanup_plain_text($contents);
				$out.=Message::textToHtml($contents);
				break;
			case 'text/enriched': 
				$contents=$this->getPartContents($partName,true);
				require_once 'dlib/mail-and-mime.php';
				$out.=dlib_mime_text_enriched_render($contents);
				break;
			case 'text/html' :
				$html=$this->getPartContents($partName,true);
				list($head,$html)=$this->processHtml($html);
				if($getHead!==false){$getHead.=$head;}
				$out.=$html;
				break;
			case '(image)':
				if(!$onlyText)
				{
					$out.='<img src="'.ent($this->getAttachmentUrl($partName)).'" alt="image attachment"/>';
				}
				break;
			case '(office)':
			case '(pdf)':
				if(!$onlyText)
				{
					$ext=dlib_mime_to_extension($mimeType);
					$fname=val($this->parts[$partName],'filename','empty-filename');
					if(strrpos($fname,$ext)===strlen($fname)-strlen($ext)){$ext=false;}
					if($fname==='empty-filename'){$fname=false;}
					$out.=t('attachment:').' '.
						'<a href="'.ent($this->getAttachmentUrl($partName)).'">'.
						($ext!==false ? ent($ext) : '').
						($ext!==false && $fname!==false ? ':' : '').
						ent($fname).'</a>';
				}
				break;
			default:
				if(!$onlyText)
				{
					$fname=val($this->parts[$partName],'filename','empty-filename');
					$out.=t('Unknown attachment type:').' '.ent($mimeType).' : '.ent($fname);
				}
			}
			$out.='</div>';
		}
		$out.='</div>';
		return $out;
	}

	//! Prepares html part for display by splitting into head and body, fixing bad chars and resolving cross-references (image attachments used in html).
	//! Only <style> tags are retained from the orignal <head>
	//! Returns both the head and the processed body.
	function processHtml($html)
	{
		// page is already tidied, so we are sure it has a head and a body
		// (this also handles a very special case: multiple bodies using conditional comments)
		// very special case: this regex can fail because html is too big (this can happen when tidy has added lots of markup)
		if(!preg_match('@^(.*)<body\b(.*)$@s',$html,$m))
		{
			$m=['','','><p>Message::processHtml failed</p></body></html>'];
		}
		$rawHead=$m[1];
		$body=$m[2];

		// only keep style tags (if needed, we might decide to add others)
		$head='';
		if(preg_match_all('@<style.*</style>@sU',$rawHead,$matches))
		{
			$head=implode("\n",$matches[0]);
		}

		$body='<div'.$body;
		$body=str_replace(['</body>','</html>'],['</div>',''],$body);

		$body=str_replace('',"'",$body);
		$body=str_replace('','"',$body);
		$body=str_replace('','"',$body);
		$body=str_replace('',"-",$body);
		$body=str_replace('',"-",$body);

		// change html crossref ids (used for html images) to urls
		foreach(array_keys($this->parts) as $partName)
		{
			$body=str_replace('mime_message_crossref:'.$partName.':',
							  ent($this->getAttachmentUrl($partName)),
							  $body);
		}

		// add target="_blank" to links, so that user doesn't get confusing navigation inside iframe
		$body=preg_replace('@<a ([^>]*href=[\'"]https?:)@','<a target="_blank" $1',$body);
		
        // Clean up certain tracking urls so that they are not disabled by next step
		$body=dcomponent_url_remove_tracking_html($body);
		// Disable potential tracking links (ex: mailchimp)
		require_once 'mail-import.php';
		$body=preg_replace_callback('@(<a [^>]*href=[\'"])(https?://[^\'"]*)([\'"])@sU',function($matches)
		{
			$url=dlib_html_entities_decode_all($matches[2]);
			$res=$matches[0];
			if(mail_import_url_is_tracker($url))
			{
				if(preg_match('@class *= *[\'"]@sU',$res)){$res=preg_replace('@(class *= *[\'"])@sU','$1demosphere-tracker-link  ',$res);}
				else                                      {$res=preg_replace('@^<a @'       ,'<a class="demosphere-tracker-link" ',$res);}
				// Disable tracking link. Also put it in title.
				$res=preg_replace('@\bhref=([\'"])@','data-tracking-href=$1',$res);
				$res=preg_replace('@\btitle=([\'"]).*\1@u','',$res);
				$res=preg_replace('@^<a @','<a title="'.ent($url).'" ',$res);
			}
			return $res;
		},$body);
		return [$head,$body];
	}


	//! Same as getFullHtml, with future dates and keywords highlighted, and extra info (timestamp and keyword list)
	//! Results are cached in file for performance.
	function getHighlighted($onlyHtml=false)
	{
		global $mail_import_config;
		$cachedFname=$mail_import_config['cache_dir'].'/cached_'.$this->id.'_highlighted';
		if(file_exists($cachedFname) && filemtime($cachedFname)>time()-3600*4)
		{
			$data=json_decode(file_get_contents($cachedFname),true);
		}
		else
		{
			$html=$this->getFullHtml(true,false,$getHead);
			require_once 'demosphere-common.php';
			$data=highlight_future_dates_and_keywords($html,true,false);
			$data['head']=$getHead;
			file_put_contents($cachedFname,json_encode($data));
		}
		return  $onlyHtml ? $data['content'] : $data;
	}

	//! Returns an array of values needed to render a Message with dcitem.tpl.php and message-item.tpl.php
	public function viewData($isSingle=false)
	{
		global $base_url,$demosphere_config;

		$res=[];
		$res['classes']=
			'importance-'.$this->importance.' '.
			($this->finishedReading ? 'finished '     : '').
			($this->isAutoHidden()  ? 'auto-hidden '  : '').
			($isSingle              ? 'single '       : '');
		$res['title']=$this->subject;
		$res['titleUrl']=$this->url();
		$res['finishUrl']='/message/'.$this->id.'/finish';

		$res['highlightInfo']=$this->getHighlighted();
		$res['highlightInfo']['timestamps']=array_unique($res['highlightInfo']['timestamps']);
		sort($res['highlightInfo']['timestamps']);
		$html=$res['highlightInfo']['content'];

		$res['nbrecipients']=count($this->splitTo())+count($this->splitCc());

		// rule that currently determines importance (first matching importance rule)
		$res['importanceRule']=false;
		$matchingRules=MailRule::fetchListFromIds(MailRule::findMatchingMailRuleIds($this));
		foreach($matchingRules as $rule)
		{
			if($rule->actionType===MailRule::actionTypeEnum('importance'))
			{
				$res['importanceRule']=$rule;
				break;
			}
		}

		$res['nbInThread']=0;
		if($this->threadNb){$res['nbInThread']=db_result('SELECT COUNT(*) FROM Message WHERE threadNb=%d',$this->threadNb);}
		
		// **** build a list of parts that are actually displayed as "attachments" (typically: pdf and office documents)

		// ignore all multipart/alternatives except one
		$res['attachments']=[];
		$ignore=MimeMessage::multipartAltIgnore($this->parts);
		require_once 'dlib/mail-and-mime.php';
		foreach($this->parts as $partName =>$part)
		{
			$mimeType=$part['type'];
			$simpleType=dlib_mime_to_simple_type($mimeType);

			// These aren't real attachments
			if(strpos($mimeType,'multipart/')===0    ||
			   $mimeType=='message/delivery-status'  ||
			   $mimeType=='message/rfc822'        
			   ){continue;}

			// ignore images: they are already shown inside frame by getFullHtml()
			if($simpleType==='image'){continue;}

			// ignore text (already in window)
			if(strpos($mimeType,'text/')===0 && $mimeType!=='text/rtf' && $mimeType!=='text/richtext'){continue;}

			// ignored multipart alternatives
			if(isset($ignore[$partName])){continue;}

			$attachment['url']=$this->getAttachmentUrl($partName);
			$attachment['fullType']=$mimeType;
			$attachment['simpleType']=dlib_mime_to_simple_type($attachment['fullType']);
			if($attachment['simpleType']===false){$attachment['simpleType']='other';}
			$attachment['size']=$part['size'];
			$attachment['filename']=val($part,'filename','(no name)');

			$res['attachments'][]=$attachment;
		}

		// Count number of blocked html images
		$res['nbPrivacyBlockedImages']=
			preg_match_all('@(<img[^>]*)src=([\'"](?!'.preg_quote($demosphere_config['safe_base_url'].'/safe-domain/mail-import','@').'))@',$html,$unused);

		$res['events']=dcomponent_find_demosphere_event_references($html);

		$res['iframeUrl'       ]=                       dcomponent_safe_domain_url('mail-import/message/'.$this->id.'/inside-frame');
		$res['iframeExtraAttrs']='data-demospriv="'.ent(dcomponent_safe_domain_url('mail-import/message/'.$this->id.'/inside-frame&disablePrivacyChecks')).'"';
		$res['type']='message';
		$res['isHighlighted']=$this->isHighlighted;
		$thisDoc=TMDocument::getByTid('mail_import',$this->id);
		$res['textMatching']=$thisDoc===null ? '' :
			$thisDoc->displaySimilarDocInfo(false,$res['highlightInfo']['timestamps']);
		return $res;
	}

	//! Lightweight (fast) viewData for compact list display
	public function viewDataCompact()
	{
		global $base_url;
		
		// For short "from" only keep person's name (Name <abcd@example.org>)
		$parts=dlib_email_address_parts($this->isSent ? $this->firstTo : $this->from);
		$res['shortAddress']=($this->isSent ? '> ' : '');
		$res['shortAddress'].=isset($parts['clean-name']) ? $parts['clean-name'] : $parts['address'];
		if($this->isSent && count($this->splitTo())>1){$res['shortAddress'].='...';}

		$res['finishUrl']='/message/'.$this->id.'/finish';
		return $res;
	}

	//! Example: "[coding] this is a pretty subject" => "coding"
	public function getMailingListSubject()
	{
		if(preg_match('@(\[.*\])@U',$this->subject,$matches)){return $matches[1];}
		return false;
	}

	//! Add a log in each Demosphere Event referenced by this Message
	function logDemoseventReferences()
	{
		global $base_url;
		$eventUrls=dcomponent_find_demosphere_event_references($this->getFullHtml());
		$message=t("received mail with reference to this event:").' '.'received-ref-'.$this->id." : ".$this->url();
		dcomponent_tell_demosevents_we_found_reference(array_keys($eventUrls),$message);
	}

	//! Outputs an email attachment (part) according to its mime type.
	//! This function can only used for file attachments (images, pdf, office docs...), not normal text or html parts.
	//! This should only be called from safe domain (XSS).
	function viewAttachment($name)
	{
		$part=$this->parts[$name];
		$mimeType=$part['type'];
		$contents=$this->getPartContents($name);

		switch($mimeType)
		{
		case 'text/plain' :   
		case 'text/enriched': 
		case 'text/html':     
			// Avoid security (XSS) problems. This can also happen (very rare) when an <img src=""> incorrectly references an HTML part.
			dlib_bad_request_400('Message::viewAttachment() is only for non-text and non-html attachments');
			break;
		case 'text/richtext' :   
			$mimeType='text/rtf';break;
		}

		// security: enforce valid mime syntax (header injection)
		if(!preg_match('@^[0-9a-z+.-]+/[0-9a-z+.-]+$@Di',$mimeType)){$mimeType='';}
		// XSS risk: we output user supplied mime type + content. This must be called in safe domain.
		if(!demosphere_safe_domain_check()){fatal('Message parts can only be displayed on safe domain.');}

		if(headers_sent()){fatal('Headers already sent, so we can\'t set Content-type for attachment.');}
		header('Content-Type: '.$mimeType);
		// Add correct download filename when possible, without forcing download
		if(isset($part['filename']) && $mimeType!=='text/plain'  && $mimeType!=='text/html')
		{
			$filename=dlib_clean_filename($part['filename']);
			if(!preg_match('@[^_]{4,}@',$filename)){$filename='mail-attachment';}
			header("Content-Disposition: inline; filename=".$filename);
		}
		header("Content-Length: " .strlen($contents));
		
		echo $contents;
	}

	//! Returns a string describing this message as a source (ex: "Source: message received on ..."). 
	//! Uses $this->source if it was set by MailRules
	function getSourceOrDefault()
	{
		$date=dcomponent_format_date('approx-short-date-time',$this->date);
		if($this->source!==''){return str_replace('$date',$date,$this->source);}
		return t('mail received on !date',['!date'=>$date]);
	}

	//! Highlight future dates/keywords and set related fields
	function rehighlight()
	{
		$html=$this->getFullHtml();
		require_once 'dcomponent/dcomponent-common.php';
		$hinfo=dcomponent_highlight($html,true,false);
		$this->isHighlighted=
			count($hinfo['timestamps'    ])>0 || 
			count($hinfo['keywordMatches'])>0;
		$this->firstFutureHLDate=
			count($hinfo['timestamps'])>0 ? min($hinfo['timestamps']) : false;
	}

	//! Applies all MailRules to this message and sets $this->importance and $this->source
	function refreshMailRules()
	{
		MailRule::applyRulesToMessage($this);
		// manual "rules"
		if($this->getHeader('X-Spam-Flag')==='YES'){$this->importance='trash';}
	}

	//! Returns a displayable name for a mailing list with the given listId. 
	//! It is usually just listId, but can be "from" if listId is (guessed) not pretty.
	static function mailingListName($listId,$short=false)
	{
		$res=$listId;
		$badName=false;
		if(strpos($listId,'list-id')!==false){$badName=true;}
		if(strlen($listId)<4                ){$badName=true;}
		if($badName)
		{
			$from=db_result("SELECT `from` FROM Message WHERE listId='%s' GROUP BY `from` ORDER BY COUNT(*) DESC",$listId);
			if(strlen($from)>10)
			{
				$res=($short ? '' : 'From: ').$from;
			}
		}
		return $res;
	}

	//! Reindex this Message in sphinxsearch.
	//! This is called from Message::save()
	function searchReindex($justReturnVals=false,$error='log')
	{
		require_once 'dcomponent/dcomponent-common.php';
		// special case: invalid mail or trash mail
		if($this->dateFetched==0 || $this->importance==='trash')
		{
			if($justReturnVals){return ['title'=>'','publicText'=>'','privateText'=>''];}
			dcomponent_search_proxy_call_catch('demosphere_search_dx_delete_document','log','mail',$this->id);
			return;
		}

		$title=$this->subject;
		$publicText='';
		$privateText=dlib_fast_html_to_text($this->getFullHtml());
		$privateText.=' ... '.($this->isSent ? $this->firstTo : $this->from);
		// We might want to add getTo() and cc addresses, but it might be a bit noisy (for ex: search for contact will give too many results)

		if($justReturnVals){return compact('title','publicText','privateText');}

		dcomponent_search_proxy_call_catch('demosphere_search_dx_reindex_document',$error,
										   $this->id,$title,$publicText,$privateText,'mail',$this->dateFetched,
										   !($this->finishedReading || $this->isAutoHidden() ||  $this->importance==='trash'));
	}

 	//! Rehighlight Messages that have old future dates.
	//! This function is fast. It is called at the begining of each message list page load (and from cron).
	static function rehighlightAll()
	{
		global $demosphere_config; 
		$ids=db_one_col("SELECT id FROM Message WHERE dateFetched!=0 AND finishedReading=0 AND isHighlighted AND ".
						  "firstFutureHLDate!=0 AND firstFutureHLDate<%d",time()+$demosphere_config['future_date_offset']);
		foreach($ids as $id)
		{
			$message=Message::fetch($id);
			$message->rehighlight();
			$message->save();
		}
	}

 	//! Cleanup old cached files (cache_time : default is 1 week)
	static function cleanupCachedFiles($all=false)
	{
		global $mail_import_config;
		$dir=$mail_import_config['cache_dir'];
		if(!file_exists($dir)){fatal('Message::cleanupCachedFiles: directory does not exist');}
		$scanRes=scandir($dir);
		$old=time()-$mail_import_config['cache_time'];
		if($all!==false){$old=time()+1;}
		foreach($scanRes as $dirEntry)
		{
			if(preg_match('@^cached_[0-9]+@',$dirEntry) && filemtime($dir.'/'.$dirEntry)<$old){unlink($dir.'/'.$dirEntry);}
		}
	}

	//! Put two Messages into the same thread and return threadNb
	//! Note: this changes threadNb both in the db and in the objects ($m1 and $m2).
	static function linkIntoSameThread(Message $m1,Message $m2)
	{
		if($m2->threadNb!=0){$threadNb=$m2->threadNb;}
		else
		if($m1->threadNb!=0){$threadNb=$m1->threadNb;}
		else               
		{
			$threadNb=1+(int)db_result("SELECT MAX(threadNb) FROM Message");
		}
		// relabel whole thread if both already in thread
		if($m1->threadNb!=0 && $m2->threadNb!=0)
		{
			db_query("UPDATE Message SET threadNb=%d WHERE threadNb=%d OR threadNb=%d",$threadNb,$m1->threadNb,$m2->threadNb);
		}
		db_query("UPDATE Message SET threadNb=%d WHERE id IN (%d,%d)",$threadNb,$m1->id,$m2->id);
		$m1->threadNb=$threadNb;
		$m2->threadNb=$threadNb;
		return $threadNb;
	}

	//! Returns an md5 hash of a simplified version of the subject.
	//! Similar subjects have the same hash.
	//! Example : hashSubject("Fwd: Re: hello, there") === hashSubject("hello there !! ")
	static function hashSubject($subject)
	{
		if($subject===t('no subject') || $subject==='('.t('no subject').')'){$subject='';}
		$simple=dlib_simplify_text($subject);
		$simple=preg_replace('@\b(fw|fwd|re|tr|r)\b@','',$simple);
		$simple=preg_replace('@ +@',' ',$simple);
		$simple=preg_replace('@(^ | $)@','',$simple);
		if(trim($simple)===''){return '';}
		return hash('md5',$simple);
	}

	//! Warning: returned HTML is not guaranteed XSS safe. 
	static function textToHtml($text)
	{
		$replacements=[];
		$replTagBase='JB27SDF675SDF988';
		$replace=function($v)use(&$replacements,$replTagBase)
			{
				$tag=$replTagBase.rand().'#'.count($replacements).'#';
				$replacements[$tag]=$v;
				return $tag;
			};

		$text=dlib_cleanup_plain_text($text);

		// find quoted text in replies
		$text=preg_replace_callback('@((([\n]|^)> ?)[^\n]*){3,}@s',
									function($matches)use($replace)
									{
										$sep=str_replace("\n","",$matches[2]);
										$sep=str_replace(" ","",$sep);
										$sep.=" ?";
										return "\n".$replace('<div class="demosphere-reply">')."\n".
											preg_replace('@([\n]|^)'.$sep.'@s',"\n",$matches[0])."\n".
											$replace('</div>')."\n";
									},
									$text);

		// convert pseudo html
		$text=preg_replace_callback('@<((http|mailto)[^ \n]*)>@',function($matches)use($replace)
									{
										return $replace('<a href="'.ent($matches[1]).'">'.ent($matches[1]).'</a>');
									},$text);

		// html entities
		$text=ent($text);
		$text=strtr($text,$replacements);

		// remove white space in empty lines
		$text=preg_replace('@[\n][ ]+[\n]@s',"\n\n",$text);
		$text=preg_replace('@[\n][ ]+[\n]@s',"\n\n",$text);
		// remove empty lines at begining and end
		$text=preg_replace('@^[\n]*@s',"",$text);
		$text=preg_replace('@[\n]*$@s',"",$text);
		// no more than 1 empty line
		$text=preg_replace('@[\n][\n][\n]*@s',"\n\n",$text);
		// find paragraphs
		$text=preg_replace('@(^|[\n][\n])?(.+)([\n][\n]|$)@Us',"\n<p>\$2</p>\n",$text);

		// find line breaks
		$text=preg_replace('@([\n][^\n]{1,50})[\n]([^\n])@s',"\$1<br />\$2",$text);

		// line starting by a special char
		$text=preg_replace('@[\n]([-*_+=]|&gt;|&lt;)@s',"<br />\$1",$text);
		// short line followed by line starting by a capital letter
		$text=preg_replace('@(>[^>]{1,50})[\n](\p{Lu})@su',"\$1<br />$2",$text);

		// find line breaks
		$text=preg_replace('@([-_+=]{6,})[\n]@s',"\$1<br />",$text);

		// tirets
		$text=str_replace("","<br>-",$text);

		// find line breaks
		$text=preg_replace('@[\n](https?:)@s','<br />$1',$text);

		return $text;
	}
	
	//! Just for translation
	static function unused(){
		t('direct');
		t('ordinary');
		t('trash');
		t('Direct');
		t('Ordinary');
		t('Trash');
	}

	static function createTable()
	{
		db_query("DROP TABLE IF EXISTS Message");
		db_query("CREATE TABLE Message (
  `id`                int(11)      NOT NULL auto_increment,
  `imapMailbox`       varchar(100) NOT NULL,
  `imapUid`           int(11)      NOT NULL,
  `subject`           text         NOT NULL,
  `to`                text         NOT NULL,
  `firstTo`           text         NOT NULL,
  `from`              text         NOT NULL,
  `date`              int(11)      NOT NULL,
  `headers`           longblob     NOT NULL,
  `headersHash`       varchar(250) NOT NULL,
  `parts`             longblob     NOT NULL,
  `dateFetched`       int(11)      NOT NULL,
  `messageIdHeader`   varchar(250) NOT NULL,
  `inReplyToHeader`   varchar(250) NOT NULL,
  `subjectHash`       varchar(250) NOT NULL,
  `threadNb`          int(11)      NOT NULL,
  `listId`            varchar(250) NOT NULL,
  `importance`        varchar(100) NOT NULL,
  `source`            varchar(250) NOT NULL,
  `isSent`            boolean      NOT NULL,
  `finishedReading`   int(11)      NOT NULL,
  `isHighlighted`     int(11)      NOT NULL,
  `firstFutureHLDate` int(11)      NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `imapMailbox` (`imapMailbox`),
  KEY `imapUid` (`imapUid`),
  KEY `date` (`date`),
  KEY `dateFetched` (`dateFetched`),
  KEY `headersHash` (`headersHash`),
  KEY `messageIdHeader` (`messageIdHeader`),
  KEY `inReplyToHeader` (`inReplyToHeader`),
  KEY `subjectHash` (`subjectHash`),
  KEY `threadNb` (`threadNb`),
  KEY `listId` (`listId`),
  KEY `importance` (`importance`),
  KEY `isSent` (`isSent`),
  KEY `finishedReading` (`finishedReading`),
  KEY `isHighlighted` (`isHighlighted`),
  KEY `firstFutureHLDate` (`firstFutureHLDate`)
) DEFAULT CHARSET=utf8mb4;");
	}
}


/** @} */

?>