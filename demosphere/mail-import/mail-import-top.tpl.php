<nav id="mail-import-top">
	<a id="new-mail"                          href="$scriptUrl/send-mail"             title="_(Write and send a new mail)">_(New Mail)</a>
	<a id="refresh"  class="top-icon-button"  href="$scriptUrl?refetch=<?: time() ?>" title="_(click here to re-fetch new mail from mailbox)"></a>
	<span id="message-categories">
		<? foreach(['direct','ordinary','trash'] as $importance){?>
			<span class="<?: $importance===$options['importance'] && $importance!=='trash' ? 'active-backdrop' : 'no-backdrop' ?>">
				<a class="icon-$importance <?: $importance===$options['importance'] ? 'active' : 'not-active' ?>" 
				   href="$scriptUrl?display=importance&amp;importance=$importance" >
					<?: t(ucfirst($importance)) ?> <?if($importance!=='trash'){?>($nb['importance'][$importance]['unread']) <?}?>
				</a>
				<span class="backdrop-corner-left  backdrop-corner"></span>
				<span class="backdrop-corner-right backdrop-corner"></span>
			</span>
		<?}?>
		<a class="<?: $options['display']=='sent' ? 'active' : '' ?>" href="$scriptUrl?display=sent">
			_(Sent)
		</a>
	</span>
	<a id="lists-link" class="" href="$scriptUrl/mailing-lists">
			_(Lists)
	</a>
	<div id="finish-choices" class="dropdown">
		<span class="dropdown-button top-icon-button"></span>
		<ul>
			<li><a href="$scriptUrl/finish/ordinary">_(Finish: ordinary with no future date)</a></li>
			<li><a href="$scriptUrl/finish/old"     >_(Finish: old)</a></li>
		</ul>
	</div>
	<?if($options['display']!=='other'){?>
		<span id="display-density">
	        <a id="display-full"    class="top-icon-button <?: $options['density']=='full'    ? 'active' : '' ?>" 
			   href="<?: $url(['density'=>'full'   ]) ?>" title="_(Display list with full mails)"></a><?
		  ?><a id="display-compact" class="top-icon-button <?: $options['density']=='compact' ? 'active' : '' ?>" 
			   href="<?: $url(['density'=>'compact']) ?>" title="_(Display compact list of mail)"></a>
		</span>
		<span id="display-order">
	        <a id="order-urgent"    class="top-icon-button <?: $options['order']=='urgent'    ? 'active' : '' ?>" 
			   href="<?: $url(['order'=>'urgent']) ?>" title="_(Sort mail starting with most urgent.)">!!<br/>!</a><?
		  ?><span id="sort-icon" class="top-icon-button"></span><?
		  ?><a id="order-date"      class="top-icon-button <?: $options['order']=='date'      ? 'active' : '' ?>" 
			   href="<?: $url(['order'=>'date'  ]) ?>" title="_(Sort mail starting with most recent.)">
				<?: strftime('%b',strtotime('December'))?><br/>
				<?: strftime('%b',strtotime('January'))?>
			 </a>
		</span>
	<?}?>
	<form id="search-form" action="$base_url/search">
		<input type="text" name="search" placeholder="_(search)"/>
		<input type="hidden" name="allset"           value="1"/>
		<input type="hidden" name="include-mails"    value="1"/>
		<input type="hidden" name="include-feeds"    value="0"/>
		<input type="hidden" name="include-events"   value="0"/>
		<input type="hidden" name="include-comments" value="0"/>
	</form>
	<?= dlib_add_form_token('mail_import_mail_rule_ajax') ?>
</nav>
<nav id="pager-line">
	<div id="pager-line-menu" class="<? template_tmp_label('pager-line-menu-class') ?>">
		<span id="pager-line-label">
			<? if($options['display']==='list'      ){$notEmpty=true;?>
				_(Mailing list) <span id="menu-list-id" title="<?: Message::mailingListName($options['listId'])?>">
									<?: Message::mailingListName($options['listId'],true)?> :
								</span>
			<?}?>
			<? if($options['display']==='thread'    ){$notEmpty=true;?>
				<span id="thread-title" title="$threadTitle">_(Thread:) $threadTitle</span>
				<a id="finish-thread" href="$scriptUrl/finish/thread/$options['threadNb']" title="_(Finish all messages in this thread)"></a>
			<?}?>
		</span>
		<?  $active=[]; 
			foreach(['all','unread','finished','hidden',] as $select){$active[$select]=$options['select']===$select ? 'active' : '';} 
		?>
		<?if($options['importance']==='direct' ){$notEmpty=true;?>
			<a href="<?: $url(['select'=>'unread'  ]) ?>" class="$active['unread']  ">_(unread)   ($nb['importance']['direct']['unread'  ])</a>
			<a href="<?: $url(['select'=>'finished']) ?>" class="$active['finished']">_(finished) </a>
		<?}?>
		<?if($options['importance']=='ordinary'){$notEmpty=true;?>
			<a href="<?: $url(['select'=>'unread'  ]) ?>" class="$active['unread']  ">_(unread)   ($nb['importance']['ordinary']['unread'  ])</a>
			<a href="<?: $url(['select'=>'finished']) ?>" class="$active['finished']">_(finished) </a>
			<a href="<?: $url(['select'=>'hidden'  ]) ?>" class="$active['hidden']  ">_(hidden)   </a>
		<?}?>
		<?if($options['display']=='list'       ){$notEmpty=true;?>
			<a href="<?: $url(['select'=>'unread'  ]) ?>" class="$active['unread']  ">_(unread)   ($nb['list']['unread'  ])</a>
			<a href="<?: $url(['select'=>'finished']) ?>" class="$active['finished']">_(finished) </a>
			<a href="<?: $url(['select'=>'hidden'  ]) ?>" class="$active['hidden']  ">_(hidden)   </a>
			<a href="<?: $url(['select'=>'all'     ]) ?>" class="$active['all']     ">_(all)      </a>
		<?}?>
		<? template_tmp_label('pager-line-menu-class',isset($notEmpty) ? 'not-empty' : 'empty');?>
	</div>
	<?if(isset($pager)){?>
		<?= render('dlib/pager.tpl.php') ?>
	<?}?>
</nav>
