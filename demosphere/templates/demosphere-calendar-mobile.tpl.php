<? //! Displays a demosphere calendar like the one on the frontpage. ?>
<div class="dcal <?: $options['topics'] ? 'useTopics ' : '' ?> <?: $options['highlightTopic']!==false ? 'highlightTopic ' : '' ?> <?: $options['orderByLastBigChanges']!==false ? 'changes ' : '' ?> <?: dlib_get_locale_name()=='en' ? 'useLongTime' : '' ?> ">

<? if(count($events)==0){echo t('no available events');}?>

<? $nb=-1; ?>
<? foreach ($events as $event){ ?>
<? $nb++; ?>
<? extract($event->render); ?>

<? /* ********* Month break ************ */ ?>
<? if($is_first_event_of_month && $nb>0){ ?>
<h2 id="m<? echo date('n',$event->startTime);?>" class="month-line">
	<a href="#" class="ret">^</a>
		<?: mb_convert_case(strftime('%B', $event->startTime),MB_CASE_TITLE) ?>
		<span><?: date('Y',$event->startTime) ?></span>
</h2>
<? } ?>

<? /* ********* Week break ************ */ ?>

<? /* ********* Day header ************ */ ?>
<? if($is_first_event_of_day){ ?>
<div class="day">
	 <h3>
		<span class="day">
			<?: demos_format_date('week-and-day',$event->startTime) ?>
		</span>
		<span class="month"><?:strftime(' %B',$event->startTime)?><? 
			if($event->startTime<time()-3600*24*30*4){echo strftime(' %Y',$event->startTime);}?>
		</span>
	</h3>
	<ul class="day-events">
<? } ?>

<? /* ************************************************ */ ?>
<? /* ********* Actual row for this event ************ */ ?>
<? /* ************************************************ */ ?>

<li class="$row_classes">
	<a href="$url">
	<span class="c1"><?: strlen($time)>3 ? $time : '' ?></span>
	<?= strlen($time)>3 ? '&nbsp;- ' : '' ?><span class="c0"></span><span class="cx"></span>
	<? /* Column for event city/district */ ?>
	<span class="c3"><?: $event->usePlace()->useCity()->getShort() ?></span>
	<? /* Column for event title */ ?>
	<span class="c2"><?= $event->safeHtmlTitle() ?></span>
	</a>
</li>

<? /* ********* Day footer ************ */ ?>
<? if($is_last_event_of_day){ ?>
	</ul>
</div><? /* end day div */ ?>
<? } ?>


<? /* ********* End of calendar ************ */ ?>
<? } ?>
</div><? /* end demosphere-calendar div */?>
