<form class="copyform" method="post" >
<?= dlib_add_form_token('event-repetition-copy-form') ?>

<h2>_(Copy repetitions)</h2>
<? if($isJustChanged){ ?>
	<p id="main-help">
		<?if($isRepGroupSelfEdit){?>
			<?: t('You have successfully saved an event that has repetitions. You can now choose to copy your changes to other repetitions.')?>
		<?}else{?>
			<?: t('You have just saved an event that has repetitions. You can now copy any fields (Date,Time, Title...) of the new version of your event to other repetitions. To copy fields, please select them  bellow (click on the check boxes).')?>
		<?}?>
	</p>
	<div>
		<input type="submit" value="_(Do not change any other repetition)" name="no-change" />
		<input type="submit" value="<?: $isRepGroupSelfEdit ? t('Change all repetitions') : t('Change selected fields')?>" name="submit"/>
	</div>
<?}else{?>
	<p id="main-help"><?: t('You can copy fields from the current (green) event to fields in other events. Please select (click on the checkbox) each field you want to copy.')?></p>
	<div>
		<input type="submit" value="_(Change selected fields)" name="submit"/>
	</div>
<?}?>

<?if($isRepGroupSelfEdit && $isJustChanged){?>
	<fieldset id="more-options" class="collapsible collapsed">
		<legend><span>_(More options)</span></legend>
		<div class="collapsible-content">
			<p id="self-edit-help">
				<?: t('You can copy any fields (Time, Title, Place...) of the new version of your event to other repetitions. To copy fields, please select them  bellow (click on the check boxes).')?>
			</p>
<?}?>

<div class="copy-table-wrapper <? foreach($fieldHasChanges as $f=>$c){if(!$c){echo ' no-changes-'.$f;}} ?>">
	<table>
		<? $lastWasPast=false;?>
<? foreach($rows as $row){?>
	<? if($row==='table-header'){ ?>
		<tr>
			<th></th>
			<? foreach($fields as $field=>$name){ ?>
				<th class="col-$field">
					<span><?= filter_xss($name) ?></span>
				</th>
			<?}?>
		</tr>
	<? continue;}?>
	<? if($row==='event-change-title'){ ?>
		<tr id="event-change-title">
			<td colspan="6">_(The event you have just changed:)
				(<a href="$event_url/$oldEvent->id">$oldEvent->id</a>)</td>
		</tr>
	<? continue;}?>
	<? if($row==='repetitions-title'){ ?>
		<tr id="repetitions-title">
			<td colspan="6">_(Select the fields of repetitions you also want to change:)</td>
		</tr>
	<? continue;}?>
	<? extract($row); ?>
	<tr <?= $isOldJC ? 'id="old-event" ' :''?>
		<?= $isNewJC ? 'id="new-event" ' :''?>
		<?= $event->id==$newEvent->id && $isMainRepList ? 'id="new-event-in-rep" ' :''?>
		class="
			<?: $isMainRepList ? 'repetition ' : '' ?>
			<?: $isFuture ? 'future ':'past ' ?>
			<?: $lastWasPast && $isFuture ? 'firstFuture ' : ''?>">
		<? $lastWasPast=!$isFuture; ?>
		<th>
			<? if($isOldJC){?>
				_(before)
			<? }else if($isNewJC){?>
				_(after)
			<? }else{?>
				<a href="$event_url/$event->id">
					$event->id
				</a>
			<? }?>
		</th>
		<? foreach($cols as $field=>$col){ ?>
			<? extract($col); ?>
			<td class="col-$field 
				<? if($event->id!=$newEvent->id){?>
					<?: $changed      ? 'changed '	   :'notchanged '    ?>
					<?: $oldNewChange ? 'oldNewChange ':'noOldNewChange '?>
				<?}?>
				">
				<? if($event->id!==$newEvent->id){?>
					<input type="checkbox" name="$field-$event->id" <?= $checked ? 'checked="checked"' : '' ?> />
				<?}?>
				<?= $title ?>
				<?= $diffDisplay!==false ? $diffDisplay : ''?></td>
		<?}?>
	</tr>
<?}?>
<? if(count($rows)>4){ ?>
	<tr><td></td>
		<? foreach($cols as $field=>$col){ ?>
			<td class="togglecol toggle-$field col-$field">_(toggle)</td>
		<?}?>
	</tr>
<? }?>
</table>
</div>

<hr/>
<? // ****************** Legend ******** ?>
<h4>_(Legend:)</h4>
<dl id="legend">
	<dt id="grey"><span><input type="checkbox"/>_(grey text)</span></dt>
	<dd>_(This field has not changed. You do not need to copy it.)</dd>
	<dt id="red"><span><input type="checkbox"/>_(red text)</span></dt>
	<? if($isJustChanged){ ?>
		<dd>_(If you select this field, you might lose some information. This field in this repetition was different to the field in the event before you saved it.)</dd>
	<? }else{ ?>
		<dd><?: t('This field is different from the field in the current (green) event. You can select it to copy the field from the current (green) event.')?></dd>
	<? }if($isJustChanged){ ?>
		<dt><span><input type="checkbox"/>_(Normal text)</span></dt>
		<dd>_(You can safely select this field. It was identical to the field in the event before you saved it.)</dd>
	<?}?>
	<dt id="green"><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_(green background)</span></dt>
	<? if($isJustChanged){?>
		<dd>_(This is the event you have just saved. The first two lines show the event before and after you saved it. The third line, shows your event inside the list of repetitions.)</dd>
	<? }else{ ?>
		<dd>_(This is the current event.)</dd>
	<?}?>
	<? if($isJustChanged){ ?>
		<dt id="green-change"><span>&nbsp;&nbsp;_(green and orange)</span></dt>
		<dd>_(This is a field that you have just changed in the event you have just saved.)</dd>
	<?}?>
	<dt><span class="copy-selected"><input type="checkbox" checked="checked"/>_(Yellow border)</span></dt>
	<dd><?: t('This is a field that is selected for copying. If you save this form ("Change selected fields" button), the corresponding field from the current (green) event will be copied to this one.') ?></dd>
</dl>

<?if($isRepGroupSelfEdit && $isJustChanged){?>
		</div>
	</fieldset>
<?}?>
</form>