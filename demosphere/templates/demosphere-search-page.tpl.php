<div id="search-form">
	<? if($user->checkRoles('admin')){?><a id="reindex" href="$base_url/search-rebuild-index-form">re-index</a><?}?>
	<h2>_(Search for events)</h2>
	<form method="get">
		<input id="search-input" type="search" value="<?: $options['extended-query'] ? $search : str_replace('\\','',$search) ?>" name="search"/>
		<input id="search-submit" type="submit" value="_(Search)" />
	    <input type="hidden" name="allset" value="1"/>
		<? //****** Event options ?>
		<fieldset class="search-opt-group">
				<span>_(Events:)</span> 
		<span id="event-date-wrapper" class="wrapper">
			<?= demosphere_search_radio_buttons('event-date'  ,['past'=>t('past'),'future'=>t('future'),'all'=>t('all')    ],$options['event-date']) ?>
		</span>
		<? if($isEventAdmin){ ?>
			<span id="event-status-wrapper" class="wrapper">
				<?= demosphere_search_radio_buttons('event-status' ,['0'=>t('not-published'),'1'=>t('published'),'all'=>t('all')  ],$options['event-status']) ?>
			</span>
			<label id="include-events-wrapper" class="wrapper">
				<span>_(Include)</span>
				<input id="edit-include-events" type="checkbox" name="include-events" 
									<?= $options['include-events']    ? 'checked="checked"' : '' ?>/>
			</label>
		<?}?>
		</fieldset>
		<? //****** Comment options ?>
		<? if($demosphere_config['comments_site_settings']!=='disabled'){?>
			<label id="include-comments-wrapper" class="wrapper">
				<span>_(Comments:)</span>
				<input id="edit-include-comments" type="checkbox" name="include-comments" 
									<?= $options['include-comments']  ? 'checked="checked"' : '' ?>/>
			</label>
		<?}?>
		<? if($isEventAdmin){ ?>
			<? //****** Mail options ?>
			<span id="include-mails-wrapper" class="wrapper">
				<span>_(Mails:)</span>
			    <?= demosphere_search_radio_buttons('include-mails',['0'=>t('no'),'1'=>t('yes'),'box'=>t('aside')],$options['include-mails']) ?>
			</span>
			<? //****** Feed options ?>
			<span id="include-feeds-wrapper" class="wrapper">
				<span>_(Feeds:)</span>
				<?= demosphere_search_radio_buttons('include-feeds',['0'=>t('no'),'1'=>t('yes'),'box'=>t('aside')],$options['include-feeds']) ?>
			</span>
			<? //****** Extended query ?>
			<label id="extended-query-wrapper" class="wrapper">
				<span>_(Extended:)</span>
				<input id="edit-extended-query" type="checkbox" name="extended-query" 
									<?= $options['extended-query']    ? 'checked="checked"' : '' ?>/>
			</label>
		<?} ?>
		<? //****** Only title ?>
		<label id="only-title-wrapper" class="wrapper">
			<span>_(Only search title:)</span> <input id="edit-only-title"     type="checkbox" name="only-title"     
								<?= $options['only-title']        ? 'checked="checked"' : '' ?>/>
		</label>
	</form>
</div>

<? //****** Structure of result areas (main display + optional boxes in right column(s) ) ?>

<?if($mainSearchDisplay!==false){?>
	<div id="search-all" class="<?: count($boxes) ? 'has-boxes' : 'no-boxes' ?>">
		<div id="search-results-main">
			<?=$mainSearchDisplay?>
		</div>
		<?if(count($boxes)){?>
			<div id="search-results-boxes">
				<? foreach($boxes as $boxType=>$box){?>
					<div id="box-$boxType" class="search-results-box <?: $box['nbMatches'] ? '' : 'empty'?>">
						<h2><a href="$box['titleUrls']">$box['nbMatches'] <? $t=$boxType.'s';echo t($t)?></a></h2>
						<?=$box['searchResultsHtml']?>
					</div>
				<?}?>
			</div>
		<?}?>
	</div>
<?}?>
