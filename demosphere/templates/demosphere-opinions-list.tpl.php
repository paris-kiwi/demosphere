<?= render('dlib/pager.tpl.php') ?>
<table class="opinion-list">
	<tr class="">
		<th>_(Rating)</th>
		<?if($isEventAdmin){?>
			<th>_(Rep.)</th>
		<?}?>
		<?if(!$isSingleUser){?>
			<th></th>
		<?}?>
		<th>_(Op. date)</th>
		<th>_(Event)</th>
	</tr>
	<? foreach($opinions as $op){	?>	
		<? extract($reputationData[$op->id]); ?>
		<? $event=$op->useEvent();?>
		<tr id="userop-$op->id"
			class="	opinion-line
					<?: $op->isEnabled  ? '' : 'disabled' ?>
		            ">
			<td class="rating"><a href="<?:$event->url()?>#opinion-$op->id"><?: $op->rating>0 ? '+':''?>$op->rating</a></td>
			<?if($isEventAdmin){?>
				<td class="reputation" title="$display"><?= demosphere_opinion_display_reputation($op,$reputationData[$op->id]) ?></td>
			<?}?>
			<?if(!$isSingleUser){?>
				<td class="user"><a href="$base_url/user/$op->userId/opinions"><?: $op->useUser()->login ?></a></td>
			<?}?>
			<td><?: demos_format_date($isSingleUser ? 'short-date-time' : 'shorter-date-time',$op->time) ?></td>
			<td class="title" ><a href="<?:$event->url()?>"><?: demos_format_date('day-month-abbrev',$event->startTime) ?> : <?:$event->title?></a></td>
		</tr>
	<?}?>
</table>
