<h2 id="page-title">_(Share a ride admin)</h2>

<table id="carpool-manage" class="gray-table">
	<tr>
		<th></th>
		<th></th>
		<th>event date</th>
		<th></th>
		<th>name</th>
		<th>contact</th>
		<th></th>
		<th>time</th>
		<th>from</th>
		<th>comment</th>
		<th></th>
	</tr>
	<? foreach($carpools as $id=>$carpool){ ?>
		<tr>
		    <td><a href="$base_url/$demosphere_config['event_url_name']/$carpool->eventId/carpool"><img alt="" src="$base_url/dlib/view.png"/></a></td>
		    <td><a href="$base_url/carpool/$id/edit"><img alt="" src="$base_url/dlib/edit.png"/></a></td>
			<td><?: demos_format_date('short-date-time',$carpool->useEvent()->startTime) ?></td>
		    <td><?: $carpool->isDriver ? t('driver') : t('passenger') ?></td>
			<td>$carpool->name</td>
			<td>$carpool->contact</td>
			<td><?: $carpool->hideEmail ? 'hide-email' :'' ?></td>
			<td><? if($carpool->time){echo demos_format_date('short-date-time',$carpool->time);}  ?></td>
			<td><?= str_replace("\n",'<br/>',ent($carpool->from))?></td>
			<td><?= str_replace("\n",'<br/>',ent(dlib_truncate($carpool->comment,100)))?></td>
			<td><?: strip_tags($carpool->isRoundTrip ?  t('Round <br />trip') : t('One <br />way')) ?></td>
		</tr>
	<?}?>
</table>
