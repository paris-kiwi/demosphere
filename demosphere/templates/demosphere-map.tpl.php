<div id="locationShortcuts"><?= $locationShortcuts ?></div>
<div id="badBrowser" style="display:none;"><div><a href="#" onclick="$('#badBrowser').hide()">X</a></div><?= t('Your browser, microsoft internet explorer 6, does not work for this map. If it is possible for you, you should consider <a href="http://www.mozilla-europe.org/fr/firefox/">installing  firefox</a>, a free and open source browser.') ?></div>
<div id="dateSelWrapper">
	<div id="dateSelBoxLeftTop"	 class="dateRangeButton"><span>◀</span></div>
	<div id="dateSelBoxLeftBot"	 class="dateRangeButton"><span>▶</span></div>
	<div id="dateSelBoxRightTop" class="dateRangeButton"><span>◀</span></div>
	<div id="dateSelBoxRightBot" class="dateRangeButton"><span>▶</span></div>
	<div id="dateSelBox">
		<ul id="tickmarks"><li></li></ul>
		<div id="dateSlider" class="ui-widget-content"><div id="dateSliderInner"></div></div>
	</div>
</div>
<div id="bookmarkMapDialog" title="_(save bookmark)" style="padding:0 0 0 0.5em">
	<?= t('To save this map in your bookmarks, clicl on <p><a id="bookmarkMapLink" href="">this link</a></p> and then save your bookmark the way you are used to doing it on your browser. By doing this, you will be able to come back to your map with your own setting.') ?>
</div>  
<div id="mapOptionCol">
	<div id="dateRange">
		<input id="dateTextStart" type="text" value=""/><br/> 
		<input id="dateTextEnd"	  type="text" value=""/><br/>
		<input id="dateTextSubmit" type="submit" value="_(change)"/>
	</div>
	<div id="dynamicInfo"></div>
	<a href="#" id="bookmarkMapButton" ><span><span id="dot">•</span>_(save bookmark)</span></a>
	<h2 id="optionTitle">_(show options) [+]</h2>
	<ul id="optionList" <?= !$demosphere_config['map_show_option_list'] ? 'style="display:none"' : '' ?>>
		<li></li>
	</ul>
	<div id="helpButton"><a href="<?: Post::builtInUrl('map_help') ?>">_(help)</a></div>
</div>
<div id="map" ></div>
