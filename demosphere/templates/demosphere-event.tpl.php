<? // ******************** tabs for users that can edit event *********** ?>
<div id="event" class="stdHtmlContent $event->getStatus() status-$event->getModerationStatus() $needsAttentionLevel">
	<? if($event->getModerationStatus()==='rejected-shown'){ ?>
		<h4>$demosphere_config['rejected_shown_message']</h4>
	<?}?>
	<? // right color bar to show the color of this moderation status ?>
	<div class="statusBar" ></div>
	<div class="eventHead <? $user->id!=0 ? '' :'hasSecondaryLinks' ?>">
	<? // ******************** miscEventInfo : top info *********** ?>
	<div id="miscEventInfo">
		<? // ******************** topics, terms & dayrank  *********** ?>
		<div id="miscEventInfoLeft">
			<span id="topics" class="<?= count($topics)===0 ? 'no-topics' : '' ?>">
				<span class="topicsLabel">
					<?: count($topics)<=1 ? t('topic'): t('topics') ?> : 
				</span>
				<span class="topicsList">
					<? foreach($topics as $topic){ ?>
						<a data-topic="$topic->id" href="$base_url/?selectTopic=$topic->id">
							$topic->name</a>, 
					<?}?>
					<? cleanup_output('@,\s*$@'); ?>
				</span>
			</span>
			<? foreach($demosphere_config['term_vocabs'] as $vid=>$vocab){ ?>
				<? $terms=$event->getTermNames($vid); ?>
				<? if(count($terms)){?>
					<span class="vocab">
						<span class="vocabLabel">$vocab['name'] : </span>
						<span class="termsList">
							<? foreach($terms as $tid=>$term){ ?>
								<a data-term="$tid" href="$base_url/?selectVocabTerm=$tid">$term</a>, 
							<?}?>
							<? cleanup_output('@,\s*$@'); ?>
						</span>
					</span>
				<?}?>
			<?}?>
			<? // dayrank ?>
			<? if($dayrank!==false){ ?>
				<span class="dayrank dayrank$dayrank" 
					  title="<?:
						($dayrank==1 ? 
						 t(       'most viewed event for @date',$drdate) :
						 t('second most viewed event for @date',$drdate)) ?>">
					<span>&nbsp;</span>
					<a href="$base_url/?maxNbEventsPerDay=<?: $dayrank==1 ? 1:2 ?>">
						<?: $dayrank==1 ? t('most viewed') : t('2nd most viewed') ?>
					</a>
				</span>
			<?}?>
			<?if($event->getModerationStatus()=='incomplete'){?>
				<span class="incomplete" title="_(Some information or a confirmation is missing for this event. Please contact us if you have information.)">
				<span></span>_(incomplete)</span>
			<?}?>
		</div>

		<? // ******************** actionBox (comments, email, ical, share...)  *********** ?>
		<? spaceless ?>
		<div id="actionBoxWrapper" 
			 class="nb-boxes-<?: (int)$useComments + (int)$event->carpoolIsEnabled() + 1 + 1 + (count($demosphere_config['share_links'])>0 ? 1:0) ?>">
			<div id="actionBox">
				<? // actionBox comment ?>
				<? if($useComments){ ?>
					<a id="commentLink" class="actionBoxAction" 
					   rel="nofollow"
					   onclick="document.getElementById('comments').style.display='block';<? if(!$nbComments){?>dlib_comments.setup(true);<?}?>"
					   title="_(add more information or a comment)" 
					   href="#comments">
						<span class="actionBoxIcon"></span>
						<span class="actionBoxLinkText">_(Comment this) ($nbComments)</span>
					</a>
				<?}?>
				<? // actionBox comment ?>
				<? if($event->carpoolIsEnabled()){ ?>
					<a id="carpoolLink" class="actionBoxAction <?: count($carpools) ? 'hilight-action' : '' ?>" 
					   rel="nofollow"
					   title="_(share a ride to this event)" 
					   href="$event->url()/carpool" 
   						<? if(count($carpools)){?>
						   onmouseenter="document.getElementById('carpoolPopup').style.display='block';"
						   onmouseleave="document.getElementById('carpoolPopup').style.display='none';"
						<?}?> >
						<span class="actionBoxIcon"></span>
						<span class="actionBoxLinkText">_(Share a ride)
							<?: count($carpools) ? '('.count($carpools).')':''?></span>
						<? if(count($carpools)){?>
							<div id="carpoolPopup" style="display:none" onclick="window.location='$event->url()/carpool'">
								<table>
									<? foreach($carpools as $carpool){ ?>
										<tr class="<?: $carpool->isDriver ? 'is-driver':'is-passenger' ?>">
											<td class="carpool-driver"></td>
											<td class="carpool-name"  >$carpool->name</td>
											<td class="carpool-from"  >$carpool->from</td>
										</tr>
									<?}?>
								</table>
								<div id="carpool-more">_(more...)</div>
							</div>
						<?}?>
					</a>
				<?}?>
				<? // actionBox email ?>
				<a id="send-by-email-link" class="actionBoxAction" 
				   rel="nofollow" 
				   title="_(email this event to someone)" 
				   href="$event->url()/send-by-email" >
					<span class="actionBoxIcon"></span>
					<span class="actionBoxLinkText">_(Send)</span>
				</a>
				<? // actionBox iCal ?>
				<a id="iCalLink" class="actionBoxAction" 
				   rel="nofollow"
				   title="_(import this event into your calendar)" 
				   href="$event->url()/ical.ics" >
					<span class="actionBoxIcon"></span>
					<span class="actionBoxLinkText">_(iCal)</span>
				</a>
				<? // actionBox "share" (facebook, twitter...) ?>
				<? if(count($demosphere_config['share_links'])>0){ ?>
					<? // demosShareLink link: ?>
					<a id="demosShareLink" class="actionBoxAction"
					   rel="nofollow" 
					   title="_(share this event on social networking sites)" 
					   onclick="return false;"
					   onmousedown="return shhare_link_popup(event,this);" 
					   href="#" >
						<span class="actionBoxIcon"></span>
						<span class="actionBoxLinkText">_(Share)</span>
					</a>
					<? // Share / social networking JavaScript ?>
					<script type="text/javascript">
					function shhare_link_popup(event,obj)
					{
						var link=document.getElementById("demos-f-link");
						if(link!==null){link.innerHTML=link.getAttribute("data-inner");}
						link=document.getElementById("demos-t-link");
						if(link!==null){link.innerHTML=link.getAttribute("data-inner");}
						var popup=document.getElementById("demosShhareLinkPopup");
						if(popup.style.display=="block"){popup.style.display="none";}
						else							{popup.style.display="block";}
						return false;
					}
					</script>
					<? // Share / social networking popup ?>
					<div id="demosShhareLinkPopup" style="display:none">
						<? foreach($demosphere_config['share_links'] as $share){?>
						<? if($share['network']=='facebook'){ ?>
							<a id="demos-f-link" 
							   href=" $facebookUrl" <?// space before url: avoid adblock. HTML5 says ok ?>
							   data-inner="<?: '<img width="53" height="20" src="'.$base_url.'/demosphere/css/images/shhare-f.png" alt=""/>' ?>">
							</a>
						<?}?>
						<? if($share['network']=='twitter'){ ?>
							<a id="demos-t-link"
							   href=" $twitterUrl" <?// space before url: avoid adblock. HTML5 says ok ?>
							   data-inner="<?: '<img width="72" height="20" src="'.$base_url.'/demosphere/css/images/shhare-t.png" alt=""/>' ?>">
							</a>
						<?}?>
						&nbsp;
						<?}?>
					</div><!-- end demosShhareLinkPopup -->
				<?}?>
			</div><!-- end actionBox --> 
			<? end_spaceless ?>
		</div><!-- end actionBoxWrapper -->
	</div><!-- end miscEventInfo -->
</div><!-- end eventHead -->
<div id="eventMain" <?= $showSmallFrontpageTitle ? 'class="has-fp-title"' : '' ?>>
	<? // ******************** date / time  *********** ?>
	<div id="dateWrap">
		<h2 id="date">
			<span id="dateContents" ><?= $dateHtml.''.$timeHtml ?></span>
		</h2>
	</div><!-- end dateWrap --><? /* dateWrap for ie6, why ??? */ ?>

	<? // ******************** place *********** ?>
	<?= $placeHtml ?>

	<? // ******************** editor/moderator info *********** ?>
	<? if($showSmallFrontpageTitle){ ?>
		<p id="eventFrontpageTitle">_(frontpage title: )$event->title</p>
	<?}?>

	<? if($warnEventNotPublished){ ?>
		<div id="warnEventNotPublished">
			$warnEventNotPublished
			<?if($demosphere_config['enable_opinions'] && $user->contributorLevel>0){?>
				<a id="contributor-opinion-link" href="#opinions-event">[_(opinions)]</a>
			<?}?>
		</div>
	<?}?>

	<? if($priceForContributors){ ?>
		<div id="priceForContributors">
			$priceForContributors
		</div>
	<?}?>

	<? if($isEventAdmin){ $empty=true; ?>
		<div class="moderator-info is-empty">
			<span class="small-title">_(moderator info : )</span>
			<? if($nbOpinions){ $empty=false;?>
				<div id="opinionsModeratorInfo"><a href="#opinions-event">[<?= $nbOpinions ?> _(opinions)]</a></div>
			<?}?>
			<? if($event->externalSourceId!=0){ $empty=false; ?>
				<div>_(This is an external event. Normal users will be automatically redirected to the following page.)
				<a href="$event->extraData['external']['url']">$event->extraData['external']['url']</a></div>
			<?}?>
			<? if(isset($event->extraData['duplicate'])){ $empty=false; ?>
				<div>
					_(This is an duplicate event. Normal users will be automatically redirected to the following event.)
					<a href="$base_url/$demosphere_config['event_url_name']/$event->extraData['duplicate']">$event->extraData['duplicate']</a>
				</div>
			<?}?>
			<? if(!$event->showOnFrontpage && $event->status){ $empty=false; ?>
				<div class="is-private">[ _(This is event is published but is not shown on the frontpage.)</div>
			<?}?>
			<? if(strlen($event->moderationMessage)){ $empty=false; ?>
				<div class="moderation-message">[ <?= str_replace("\n",'<br/>',ent($event->moderationMessage)) ?> ]</div>
			<?}?>
			<? if($event->needsAttention){ $empty=false; ?>
				<div class="needs-attention">_(Needs attention:) 
					<? $t=Event::needsAttentionTitles();echo $t[$event->needsAttention];?>
				</div>
			<?}?>

			<? if($autoTitle && $demosphere_config['show_title_in_event']!=='always'){ $empty=false; ?>
				<div class="auto-title-message">[
					_(No title found in text, automatically adding it.)
					<? if($user->checkRoles('admin')){?>
						<a href="$base_url/configure/misc-config#edit-show-title-in-event-wrapper"></a>
					<?}?>
				]</div>
			<?}?>
			<? if(!$empty){cleanup_output('@moderator-info is-empty@','moderator-info');}?>
		</div>
	<?}?>
	
	<? // ******************** incomplete-message *********** ?>
	<?if($incompleteMessage!==false){?>
		<div id="incomplete-message">
		    <span id="incomplete-icon">⚠</span>
			<?= $incompleteMessage ?>
		</div>
	<?}?>
	<? // ******************** body *********** ?>
	<div id="htmlView">
		<?= $body ?>
	</div><!-- end htmlView -->
</div><!-- end eventMain -->
<script type="application/ld+json">
	<?= dlib_json_encode_esc($eventRdf) ?>
</script>
</div><!-- end event -->
<?= $opinions ?>
<?= $comments ?>