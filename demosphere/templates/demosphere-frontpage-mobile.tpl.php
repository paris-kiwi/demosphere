<!DOCTYPE html>
<html lang="$lang">
<head>
	<title>$title</title>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?= $robots ?>
	<?= implode("\n",$head) ?>
	<link rel="shortcut icon" href="$base_url/<?: demosphere_cacheable('files/images/favicon.ico')?>" type="image/x-icon" />
	<link rel="alternate" type="application/rss+xml" title="$siteName RSS" href="$rssLink" />
	<link rel="alternate" type="text/calendar" title="$siteName iCalendar" href="$icalLink" />
	<link rel="canonical" href="$std_base_url"/>
	<? // Tell browser to start downloading png now, don't wait for CSS to request it ?>
	<link rel="subresource" href="$base_url/<?: demosphere_cacheable('files/images/sprites/mobile.png') ?>">
	<meta name="description" content="$metaDescription"/>
	<meta name="keywords" content="$metaKeywords"/>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?= $css ?>
</head>
<body class="<?: implode(' ',$bodyClasses) ?>">
<?= $eventAdminMenubar ?>
<div id="wrap">
	<?= $bodyJavascript ?>
	<?= render('demosphere-mobile-menu.tpl.php'); ?>
	<div id="top">
	 <h2 id="siteName" class="<?: strlen($demosphere_config['site_name'])>22 ? 'long-name':''?>"><a href="$base_url"><?: $siteName ?></a></h2>
	</div>
	<div id="main">
		<?= $annouce ?>
		<?= $featured ?>
		<?= $cities ?>
		<div id="calendar">
			<?= $calendar ?>
			<? if($autoLimitMoreEventsUrl!==false){?>
				<div id="autoLimitMoreEventsUrl"><a href="$autoLimitMoreEventsUrl">_(show more events)</a></div>
			<?}?>
		</div>
	</div>
	<div id="footer">
	</div>
</div><!-- end wrap -->
<? // Javascript is here for performance ?>
<?= $js ?>
</body>
</html>
