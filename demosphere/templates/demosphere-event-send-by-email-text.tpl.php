$nameOrEmail ($senderEmail)
_(is sending you the following event from the calendar : )
<?= preg_replace('@^https?://(www[.])?@','',$base_url)?> :
		
$event->title

$event->url()
		
<? if($message!=''){?>

<?= t('A message from !nameOrEmail',['!nameOrEmail'=>$nameOrEmail])?> : 
$message

<?}?>

_(Event's date : )<?= demos_format_date('full-date-time',$event->startTime)?>


_(Event's location : )($city)
<?= str_replace("\r\n","\n",$place->address) ?>
	

_(For more information on this event, follow this link :)

$event->url()
	

----
_(If you notice that somebody is misusing this email service, please contact: contact@demosphere.net)
