<h1 class="page-title">_(List of places)</h1>
<p class="intro"><?: t('Browse the venues of events published on @sitename.',['@sitename'=>$siteName])?></p>
<p>
	<? if($options['hasDescription']){?>
		<a href="$base_url/$places_url_name">_(All places)</a>
	<?}else{?> 
		<a href="$base_url/$places_url_name?hasDescription=1">_(Only places with a description)</a>
	<?}?>
	<? if(count($regions)){?>
		; _(Show only:)
		<?foreach($regions as $urlName=>$region){?>
			<a href="$base_url/$places_url_name?selectFpRegion=$urlName<?: $options['hasDescription'] ? '&hasDescription=1':''?>">$region['name']</a>,
		<?}?>
		(<a href="$base_url/$places_url_name<?: $options['hasDescription'] ? '?hasDescription=1':''?>">_(all)</a>)
	<?}?>
</p>
<div class="places">
	<?= render('dlib/pager.tpl.php') ?>
	<? if(count($places)===0){?>
		<p>_(No places found)</p>
	<?}else{?>
		<table>
			<tr>_(n° ev.)<th></th><th></th><th></th></tr>
			<? foreach($places as $place){ ?>
				<tr>
					<td class="nbEvents"><?: $order[$place->id] ?></td>
					<td class="title"><a href="$base_url/$place_url_name/$place->id"><?: $place->nameOrTitle(false)?></a></td>
					<td class="city">$place->getCityName()</td>
				</tr>
			<?}?>
		</table>
	<?}?>
</div>
