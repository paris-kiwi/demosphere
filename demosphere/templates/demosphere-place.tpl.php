<div class="place <?: $hasMapImage ? ' hasMapImage':''?>" data-place-id="$place->id">
	<? // rounded corners ?>
	<span class="placeTopLeft" ><span></span></span>
	<div class="place-inner">
		<h3><a class="placeLabel" href="$placeUrl">_(Where) :</a>
			<a class="city" href="$base_url/?selectCityId=$place->cityId">$place->getCityName()</a>
		</h3>
		<? // address. ?>
		<? spaceless ?><? // spaces break publish form ?>
		<p class="address-text">
			<a href="$placeUrl" <?= $referencePlace->description ? '' :'rel="nofollow"' ?> >
				<?= $place->htmlAddress() ?>
			</a>
		</p>
		<? end_spaceless ?>

		<? // map image ?>
		<? if($hasMapImage){ ?>
			<p class="mapimage">
				<a class="mapimage-link" href="$mapUrl">
					<img   src="$mapImage['url']" alt="_(map)" 
						width ="$mapImage['width']" 
						height="$mapImage['height']" />
				</a>
			</p>
		<? }else if($isEventAdmin){ ?>
			<p class="no-map-warning">_(Warning: not positioned on map)</p><p class="no-map-warning-sub">_(Event selection tools for emails subscriptions, feeds, widgets, frontpage... will not work correctly with this place.)</p>
		<? } ?>

		<? // place-links ?>
		<? if($showPlaceLinks){ ?>
			<p class="place-links">
				<? if($referencePlace->description){ ?>
					<a class="place-description-link" href="$base_url/$demosphere_config['place_url_name']/$place->referenceId">
						<?: t('details for this place (@nb events)',['@nb'=>$nbEventsAtPlace])?>
					</a>
				<? }else if($nbEventsAtPlace){ ?>
					<a class="place-link" rel="nofollow" href="$base_url/$demosphere_config['place_url_name']/$place->referenceId">
					<?: t('@nb events at this place',['@nb'=>$nbEventsAtPlace]) ?></a>
				<? } ?>
			</p>
		<? } ?>
	</div><? // end place inner ?>
</div><!-- end place -->
