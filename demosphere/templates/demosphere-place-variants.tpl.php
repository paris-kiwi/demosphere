<h1 class="page-title">_(Place variants)</h1>
<p class="place-admin-help">
	_(Manage variants of this place. Check the first column to delete a variant. The second column is the replacement. Deleted variants will be replaced by the chosen replacement.)
</p>

<form method="post">
<?= dlib_add_form_token('demosphere_place_variants');?>
<input type="submit" name="submit" value="_(Delete selected variants)"/>
<table id="place-variants" class="place-diff-list">
	<tr>
		<th></th>
		<th></th>
		<th title="_(Number of events at this place)">_(ev.)</th>
		<th>_(place)</th>
		<th></th>
		<th>_(address)</th>
	</tr>
	<? foreach($places as $pid=>$place){ ?>
		<tr class="<?: $place->hideFromAltSuggestion ? 'hidden' : '' ?>
				   <?: $pid==$refPlace->id           ? 'first'  : '' ?> ">
			<td>
				<? if($pid!==$refPlace->id){?>
					<input type="checkbox" name="delete-$pid"/>
				<?}?>
			</td>
			<td>
				<input type="radio" value="$pid" name="destination" <?: $pid==$refPlace->id ? 'checked' : '' ?>/>
			</td>
			<td>$nbEvents[$pid]</td>
			<td>[<a href="$place->url()">$pid</a>]</td>
			<td class="<?: demosphere_place_sphere_distance_place($refPlace,$place)>1 ? 'mapdiff' : ''?>"><?: $place->zoom ? '🌍' : ''?></td>
		<td><?= demosphere_place_diff($refPlace,$place) ?></td>
		</tr>
	<?}?>
</table>
