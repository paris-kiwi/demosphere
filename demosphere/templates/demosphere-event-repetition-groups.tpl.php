<div id="repetition-groups-list">
<h1 class="page-title">_(Repetition groups)</h1>

<p>_(Include old repetitions)
	<select onchange="window.location.href=base_url+'/repetition-group?start='+this.options[this.selectedIndex].getAttribute('value')">
	    <option value="<?: strtotime('2 months ago') ?>">_(Choose)  </option>
	    <option value="<?: strtotime('2 months ago') ?>">_(2 months)</option>
	    <option value="<?: strtotime('1 year ago'  ) ?>">_(1 year)  </option>
	    <option value="0"                               >_(all)     </option>
	</select>
</p>

<table>
	<? $prevRuleType=false; ?>
	<?foreach($groups as $group){?>
		<? if($group->ruleType!==$prevRuleType){$prevRuleType=$group->ruleType;?>
			<tr class="rule-type-label">
				<th colspan="10">
					<? $l=RepetitionGroup::getRuleTypeLabels(); echo $group->ruleType ? $l[$group->ruleType] : t('Manual');?>
				</th>
			</tr>
		<?}?>
		<tr class="<?: $group->autoIsEnabled && ($group->ruleEnd==false || time()<$group->ruleEnd)   ? 'active' : 'inactive' ?>">
			<td class="nb"  ><?: $groupNbEv[$group->id]?></td>
			<td class="active"><?: $group->autoIsEnabled ? '✓' : '' ?></td>
			<td class="group"><a href="<?: $group->useReference()->url() ?>/repetition"><?= $group->useReference()->safeHtmlTitle()?></a></td>
		</tr>
	<?}?>
</table>
</div>