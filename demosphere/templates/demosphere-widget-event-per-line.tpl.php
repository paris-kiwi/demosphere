<div class="demosphere-widget">
<ul>
	<? foreach($events as $event){ ?>
		<li>
			<a class="event <?: $event->render['row_classes'] ?>" target="_top" href="$event->render['url']">
				<? $format=$options['day-header-format']; ?>
				<? if($format!==false){ ?>
					<span class="day">
						<?: strpos($format,'|')===0 ? 
								demos_format_date(substr($format,1),$event->startTime):
								strftime($format,$event->startTime) ?>
					</span>
				<?}?>
				<span class="date">
					<? $format=$options['time-format']; ?>
					<?: preg_replace('@0?3[:h]33@','-',strpos($format,'|')===0 ? 
							   demos_format_date(substr($format,1),$event->startTime):
							   strftime($format,$event->startTime)) ?>
				</span>
				<? if(val($options,'show-city')){ ?>
					<span class="location city"><?: $event->usePlace()->useCity()->getShort() ?></span> - 
				<?}?>
				<span>$event->title</span>
			</a>
		</li>
	<?}?>
</ul>
<?if(count($events)===0){?><?: t('(no events)') ?><?}?>

<p class="extra-message"><?: filter_xss($options['extra-message']) ?></p>
</div>
