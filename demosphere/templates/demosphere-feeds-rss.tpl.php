<?= '<?xml version="1.0" encoding="utf-8"?>'."\n" ?>
<rss version="2.0" xml:base="$base_url" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:georss="http://www.georss.org/georss" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
	<title>$demosphere_config['site_name'] - $demosphere_config['site_slogan']</title>
	<link>$base_url</link>
	<description>$demosphere_config['site_mission']</description>
	<language><?: dlib_get_locale_name() ?></language>
	<atom:link href="$channelLink" rel="self" type="application/rss+xml" />
	<? foreach($events as $event){ ?>
		<item>
			<title><?
				?><? if($startTimeInTitle){?><?
						?><? if(strpos($timeFormat,'|')===0){?><?: demos_format_date(substr($timeFormat,1),$event->startTime) ?><?
						?><?}else{?><?
								$tf=$timeFormat;
								if(date('G i',$event->startTime)=='3 33')
								{
									// hackish removal of 3:33 time in format :-(
									$tf=preg_replace('@%[HkIlMpPrRSTXzZ].*%[HkIlMpPrRSTXzZ]@','',$tf);
									$tf=preg_replace('@%[HkIlMpPrRSTXzZ]@','',$tf);
								}
								?><?: strftime($tf,$event->startTime) ?><?
						?><?}?> - <?
				?><?}?><?
				?><?: $cityInTitle ? $event->usePlace()->useCity()->getShort().' - ' : '' ?><?
				?>$event->title</title>
			<link>$event->url()</link>
			<description><?: $useDescription ? mb_substr(dlib_fast_html_to_text($event->body),0,400).'...' : '' ?></description>
			<comments>$event->url()#comments</comments>
			<? if(isset($event->render['topics']) && $event->render['topics']!=''){ ?>
				<? foreach(explode(',',$event->render['topics']) as $tid){ ?>
					<category>$topicNames[$tid]</category>
			<?}}?>
			<dcterms:temporal>
				<dcterms:period>start=<?: date('c',$event->startTime) ?>;scheme=W3C-DTF;</dcterms:period>
			</dcterms:temporal>
			<dcterms:spatial><?: $event->usePlace()->useCity()->name ?></dcterms:spatial>
			<pubDate><?: date('r', $useEventDateNotPub ? $event->startTime : $event->lastBigChange) ?></pubDate>
			<dc:creator>demosphere.net</dc:creator>
			<guid isPermaLink="false">$event->id at $base_url</guid>
			<? if($event->usePlace()->zoom){ ?>
				<georss:point><?: $event->usePlace()->latitude?> <?: $event->usePlace()->longitude ?></georss:point>
			<?}?>
		</item>
	<?}?>
</channel>
</rss>
