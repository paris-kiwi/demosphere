<h1 class="page-title">_(Repetitions)</h1>
<table>
<?foreach($events as $id=>$event){?>
	<tr class="" >
			<td class="dayofweek"><?:strftime('%a',$event->startTime)?></td>
			<?
				$day  ='<td class="date day  ">'.ent(strftime('%e',$event->startTime)).'</td>';
				$month='<td class="date month">'.ent(strftime('%b',$event->startTime)).'</td>';
				echo demosphere_date_time_swap_endian($day,$month,''); 
			?>
			<td class="time"><?: strftime('%R',$event->startTime)=='03:33' ?'': strftime('%R',$event->startTime)?></td>
			<td class="title">
				<? if($event->access('view')){?>
					<a href="$event->url()">
				<?}?>
					<?= $event->safeHtmlTitle()?>
				<? if($event->access('view')){?>
					</a>
				<?}?>
			</td>
			<td class="place"><?: $event->usePlace()->useCity()->getShort()?></td>

	</tr>
<?}?>
</table>
