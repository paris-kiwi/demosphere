<? //! Displays a demosphere calendar like the one on the frontpage. ?>
<div class="dcal <?: $options['topics'] ? 'useTopics ' : '' ?> <?: $options['highlightTopic']!==false ? 'highlightTopic ' : '' ?> <?: $options['orderByLastBigChanges']!==false ? 'changes ' : '' ?> <?: dlib_get_locale_name()=='en' ? 'useLongTime' : '' ?> ">

<? if(count($events)==0){echo t('no available events');}?>

<? $nb=-1; ?>
<? foreach ($events as $event){ ?>
<? $nb++; ?>
<? extract($event->render); ?>

<? /* ********* Month break ************ */ ?>
<? if($is_first_event_of_month && $nb>0){ ?>
<h2 id="m<? echo date('n',$event->startTime);?>" class="month-line">
	<a href="#" class="ret">^</a>
		<?: mb_convert_case(strftime('%B', $event->startTime),MB_CASE_TITLE) ?>
		<span><?: date('Y',$event->startTime) ?></span>
</h2>
<? } ?>

<? /* ********* Week break ************ */ ?>
<? if($is_first_event_of_week && $nb>0){ ?><hr class="week"/><? }?>

<? /* ********* Day header ************ */ ?>
<? if($is_first_event_of_day){ ?>
<div class="day" id="d<?: date('j-n',$event->startTime) ?>">
	<? 
		/* anchors for easy reference through links in small calendar on right column. */ 
		/* We also need to add intermediate days with no events. */
		foreach($day_transitions as $d)
		{
			if(($d-$events[0]->startTime)>3600*24*31*2 ||
			   date('j-n',$d)===date('j-n',$event->startTime)){continue;}
			echo '<span id="d'.date('j-n',$d).'"></span>';
		}
	?>
	<h3>
		<? if($is_today   ){?><span class="specialDay">_(today)</span><?}?>
		<? if($is_tomorrow){?><span class="specialDay">_(tomorrow)</span><?}?>
		<span class="wday">
			<?: demos_format_date('week-and-day',$event->startTime) ?>
		</span>
		<span class="month"><?:strftime(' %B',$event->startTime)?><? 
			if($event->startTime<time()-3600*24*30*4){echo strftime(' %Y',$event->startTime);}?>
		</span>
	</h3>
	<table>
<? } ?>

<? /* ************************************************ */ ?>
<? /* ********* Actual row for this event ************ */ ?>
<? /* ************************************************ */ ?>

<tr class="$row_classes">
	<? /* Column for topic icons */ ?>
	<td class="c0"></td>
	<? /* Column for event time */ ?>
	<td class="c1">
		<? if($options['showAdminLinks']){?><a href="$event->url()/edit"><?}?>
		$time
		<? if($options['showAdminLinks']){?></a><?}?>
	</td>
	<? /* Column for day rank */ ?>
	<td class="cx"></td>
	<? /* Column for event title */ ?>
	<td class="c2">
		<a <?= $tooltips_text ? 'title="'.ent($tooltips_text).'" ' : '' ?> href="$url">
			<?= $event->safeHtmlTitle(); ?>
		</a>
	</td>
	<? /* Column for event city/district */ ?>
	<td class="c3">
		<a href="<?: str_replace('9999999',$event->usePlace()->useCity()->id,$selectCityUrl) ?>">
			<?: $event->usePlace()->useCity()->getShort() ?>
		</a>
	</td>
</tr>

<? /* ********* Day footer ************ */ ?>
<? if($is_last_event_of_day){ ?>
</table>
</div><? /* end day div */ ?>
<? } ?>


<? /* ********* End of calendar ************ */ ?>
<? } ?>
</div><? /* end demosphere-calendar div */?>
