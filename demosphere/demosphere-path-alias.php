<?php

//! returns src (ex:post/123) from dest (ex: my-contact-page)
function demosphere_path_alias_src($dest)
{
	$src=db_result_check("SELECT src FROM path_alias WHERE dest='%s'",$dest);
	return $src===false ? $dest : $src;
}

//! returns dest (ex: my-contact-page) from src (ex:post/123) 
function demosphere_path_alias_dest($src)
{
	$dest=db_result_check("SELECT dest FROM path_alias WHERE src='%s'",$src);
	return $dest===false ? $src : $dest;
}

function demosphere_path_alias_set($src,$dest)
{
	if(strlen($src)==0 || strlen($dest)==0){fatal('demosphere_path_alias_set: invalid empty src="'.$src.'" , dest="'.$dest.'"');}
	db_query("REPLACE INTO path_alias VALUES ('%s','%s')",$src,$dest);
}


function demosphere_path_alias_install()
{
	db_query("DROP TABLE IF EXISTS path_alias");
	db_query("CREATE TABLE path_alias (
  `src`  varchar(191)  NOT NULL,
  `dest` varchar(191)  NOT NULL,
  PRIMARY KEY  (`src`),
  KEY `dest` (`dest`)
) DEFAULT CHARSET=utf8mb4");

}


?>