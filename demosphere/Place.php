<?php


/**
 * A Place object represents a geographical point with an address and a reference to a city.
 * Similar Places can be grouped together ("variants") 
 * Places are referenced by Event's.
 */
class Place extends DBObject
{
	//! id=1 is used for empty place
	public $id=0; 
	static $id_         =['type'=>'autoincrement'];
	//! Similar Places can be grouped into variants. Variants all point to a single reference place.
	//! When a Place is not a variant, referenceId is equals to id. This is the most common case.
	public $referenceId=0;
	static $referenceId_=['type'=>'foreign_key','class'=>'Place'];
	//! The city of this Place.
	public $cityId=0;
	static $cityId_     =['type'=>'foreign_key','class'=>'City'];
	//! A plain text address, usually containing several lines (\n).
	public $address=false;
	static $address_    =[];
	//! Geo coordinates of this place.
	public $latitude=false;
	static $latitude_   =['type'=>'float'];
	public $longitude=false;
	static $longitude_  =['type'=>'float'];
	//! note: zoom=0 is used to say "map is not used"
	public $zoom=false;
	static $zoom_       =['type'=>'int'];
	//! A title for this place
	//! This is empty most of the time, in that case the first line of the adress can bu used. See nameOrTitle()
	public $title;
	static $title_     =[];
	//! An html description for this place.
	//! This is empty most of the time.
	public $description;
	static $description_=['type'=>'html'];
	//! Places that have a description are considered as more important and can be displayed separately.
	public $hasDescription;
	static $hasDescription_=['type'=>'bool'];
	//! This is used for backward compatibility: 
	//! avoid suggesting many old variants that where entered without suggestions.
	public $hideFromAltSuggestion=false;
	static $hideFromAltSuggestion_=['type'=>'bool'];
	//! Id of User that can change this place, and events in it.
	//! This can only be used for references places.
	public $inChargeId;
	static $inChargeId_=['type'=>'foreign_key','class'=>'User'];
	//! Last unvalidated change by inChargeId user. 
	//! When mods validate the change, changedById is reset to 0.
	public $inChargeChanged=0;
	static $inChargeChanged_    =['type'=>'timestamp'];

 	static $dboStatic=false;

	//! return a list of fields that define place
	//! Warning: dont mess this up, as it will affect 
	//! mergeDuplicates that could delete many Places during cron run.
	static function fields($which='event-data',$ignore=[])
	{
		$res=$which;
		if(!is_array($which))
		{
			switch($which)
			{
			case 'event-data': $res=['cityId','address','latitude','longitude','zoom',];
				break;
			case 'all':        $res=array_keys(self::getSchema());
				break;
			default: fatal('Place::fields');
			}
		}
		return array_diff($res,$ignore);
	}

	function __construct()
	{
	}

	function save()
	{
		// Empty city is always 1
		if(!$this->cityId){$this->cityId=1;}

		if($this->referenceId)
		{
			$referencedPlace=$this->getReference(false);
			// sanity check: stale reference
			if($referencedPlace===null)
			{
				dlib_message_add('Place::save: place('.$this->id.') had reference to nonexistant place('.$this->referenceId.'). Reference was reset.','error');
				$this->referenceId=$this->id;
				$referencedPlace=$this;
			}
			else
			{
				// fix chained reference (we should reload new ref... too complex)
				$this->referenceId=$referencedPlace->referenceId;
			}
		}

		// Sanity check bad map coords:
		if($this->latitude==0 || $this->longitude==0){$this->zoom=0;}

		$this->hasDescription=trim($this->description)!=='';

		parent::save();

		// self reference if no reference
		if(!$this->referenceId)
		{
			$this->referenceId=$this->id;
			parent::save();
		}
		// ******* update search index
		require_once 'demosphere-place-search.php';
		demosphere_place_search_index_update($this);
		require_once 'demosphere-page-cache.php';
		demosphere_page_cache_clear_all();
	}
	function delete()
	{
		if($this->id==1){fatal('Do not delete place->id=1 (empty place)');}
		db_query("UPDATE Event SET placeId=1 WHERE placeId=%d",$this->id);
		db_query("UPDATE Place SET referenceId=id WHERE referenceId=%d",$this->id);
		db_query("DELETE FROM place_search_index WHERE pid=%d",$this->id);

		parent::delete();
	}

	function url()
	{
		global $base_url,$demosphere_config;
		return $base_url.'/'.$demosphere_config['place_url_name'].'/'.$this->id;
	}

	//! check consistency in placefields 
	static function checkPlaces()
	{
		$chainedRefs=db_one_col('SELECT p1.id FROM Place as p1,Place as p2 WHERE p1.referenceId=p2.id AND p2.referenceId!=p2.id');
		$staleRefs  =db_one_col('SELECT p1.id FROM Place as p1 WHERE NOT EXISTS (SELECT * FROM Place as p2 WHERE p1.referenceId=p2.id)');
		$badMap     =db_one_col('SELECT id FROM Place WHERE zoom!=0 AND (latitude=0 OR longitude=0)');
		self::checkError(compact('chainedRefs','staleRefs','badMap'));
	}

	//! check consistency in place / event relations
	static function checkPlaceEvent()
	{
		$stalePlaces=db_one_col('SELECT Event.id FROM Event WHERE '.
								  'NOT EXISTS (SELECT * FROM Place WHERE Event.placeId=Place.id)');
		self::checkError(compact('stalePlaces'));
	}

	static function checkError($errors)
	{
		foreach($errors as $name=>$errorList)
		{
			if(count($errorList))
			{
				echo 'Place::checkError: for:'.$name.': '.implode(', ',$errorList)."<br/>\n";
			}
		}
	}

	//! Delete places that are unused after a few weeks
	//! This should be called from cron. 
	static function cleanupUnusedPlaces()
	{
		// ********************* Old unpublished Events : remove references to places that are not really used 
		$old=time()-3600*24*30*3;
		// Create temp table with ok events (not old)
		db_query('CREATE TEMPORARY TABLE newevents AS (SELECT id,placeId FROM Event WHERE '.
				 // this event is published
				 'status!=0 OR '.
				 // this event is just-created
				 'moderationStatus=0 OR '.
				 // this event is recent (startTime)
				 'startTime>%d OR '.
				 // this event is recent (changed)
				 'changed>%d)',
				 $old,$old); 
		db_query('ALTER TABLE newevents ADD INDEX id (id)');
		db_query('ALTER TABLE newevents ADD INDEX placeId (placeId)');

		// Create temp table with old places (places that are not used in new events)
		db_query('CREATE TEMPORARY TABLE oldplaces AS (SELECT id,hasDescription FROM Place WHERE NOT EXISTS (SELECT * FROM newevents WHERE newevents.placeId=Place.id))'); 
		// Remove from old places: reference places and places with description 
		db_query('DELETE FROM oldplaces WHERE hasDescription!=0 OR EXISTS (SELECT * FROM Place WHERE Place.referenceId=oldplaces.id AND Place.referenceId!=Place.id )'); 
		// Now, finally... remove old places from old events
		db_query('UPDATE Event,oldplaces SET Event.placeId=1 WHERE Event.placeId=oldplaces.id');
		// Note: this hasn't really deleted those old places, but they are no logner referenced. So the next step can tag them for deletetion

		// ********************* Tag unused Places for deletion after a fixed delay (about a month)
		$deleteDelay=3600*24*30;
		// list of currently unused places
		$unused=db_one_col('SELECT p1.id FROM Place as p1 WHERE '.
						   // no description
						   'p1.hasDescription=0 AND '.
						   // not used by an event
						   'NOT EXISTS (SELECT * FROM Event WHERE Event.placeId=p1.id) AND '.
						   // not a reference for another Place
						   'NOT EXISTS (SELECT * FROM Place as p2 WHERE p2.id!=p1.id AND p2.referenceId=p1.id)');

		// untag places previously tagged as unused but that are now used
		db_query("DELETE FROM variables WHERE type='unused-place' ".
				   (count($unused)==0 ? '' :  "AND name NOT IN ('".implode("','",$unused)."')"));

		// find old unused places and delete them
		$now=time();
		foreach($unused as $pid)
		{
			if($pid==1){continue;}
			$firstSeenUnused=variable_get($pid,'unused-place');
			if($firstSeenUnused===false)
			{
				echo "found new unused place: $pid<br/>\n";
				variable_set($pid,'unused-place',$now);
				continue;
			}
			// places that are unused for a month are deleted
			if($now>$firstSeenUnused+$deleteDelay)
			{
				echo "deleting old unused place : $pid<br/>\n";
				$place=Place::fetch($pid);
				$place->delete();
			}else{echo "unused place $pid, not old enough to delete<br/>\n";}
		}
	}

	static function cleanupTitle($title)
	{
		$title=dlib_cleanup_plain_text($title,false);
		$title=preg_replace('@\s+@',' ',$title);
		return $title;
	}
	static function cleanupAddress($address)
	{
		$address=dlib_cleanup_plain_text($address,false);
		$address=str_replace("\r\n"      ,"\n",$address);
		$address=str_replace("\r"        ,"\n",$address);
		$address=preg_replace('@[ \t]+@' ,' ' ,$address);
		$address=preg_replace('@\s*$@m'  ,''  ,$address);
		$address=preg_replace('@^\s@m'   ,''  ,$address);
		$address=trim($address);
		return $address;
	}

	function createVariant()
	{
		$res=new Place();
		$res->cityId   =$this->cityId;
		$res->address  =$this->address;
		$res->latitude =$this->latitude;
		$res->longitude=$this->longitude;
		$res->zoom     =$this->zoom;
		$res->hideFromAltSuggestion=$this->hideFromAltSuggestion;
		return $res;
	}

	function makeName($addCity=true)
	{
		$address=$this->address;
		$address=explode("\n",$address);
		$name=preg_replace('@ *, *$@','',$address[0]);
		if(strlen($name)===0){$name='---';}
		if($addCity){$name.=', '.$this->getCityName();}
		return $name;
	}

	function nameOrTitle($addCity=true)
	{
		if($this->title!=''){return $this->title;}
		return $this->makeName($addCity);
	}

	function equals($other,$fields='event-data',$ignore=[])
	{
		return count($this->differences($other,$fields,$ignore))===0;
	}
	function differences($other,$fields='event-data',$ignore=[])
	{
		// Note: cities are compared by cityId 
		$res=[];
		foreach(Place::fields($fields,$ignore) as $field)
		{
			// don't compare lat,lng if map is not used for one of the places
			if(($field==='latitude' || $field==='longitude') &&
			   ($this->zoom==0 || $other->zoom==0)){continue;}
			if($this->$field!==$other->$field){$res[]=$field;}
		}
		return $res;
	}

	function isEmpty()
	{
		if($this->id===1){return true;}
		return ($this->getCityName(true)===false || $this->getCityName()==='')  &&
			strlen($this->address)==0 &&
			$this->zoom==0 &&
			$this->title=='' &&
			$this->description=='';
	}

	function referencedBy()
	{
		return db_one_col('SELECT id FROM Place WHERE referenceId=%d AND referenceId!=id',$this->id);
	}

	function events($onlyPublished=false)
	{
		return db_one_col('SELECT Event.id FROM Event WHERE '.
						  ($onlyPublished ? 'status=1 AND ' : '').
						  'placeId=%d',$this->id);
	}

	//! Is this place referenced from somewhere?
	//! Optionally ignore a specific event.
	function isUsedElsewhere($ignoreEventId=false)
	{
		if(!$this->id){return false;}
		if(count($this->referencedBy())){return true;}
		$events=$this->events();
		if($ignoreEventId){$events=array_diff($events,[$ignoreEventId]);}
		return count($events)!==0;
	}

	function getReference($exceptionOnFail=true)
	{
		if($this->referenceId==$this->id){return $this;}
		return Place::fetch($this->referenceId,$exceptionOnFail);
	}

	function getCityName($falseIfNotExists=false)
	{
		if($this->cityId==0){return $falseIfNotExists ? false : '-';}
		return db_result('SELECT name FROM City WHERE id = %d', $this->cityId);
	}
	function htmlAddress()
	{
		return nl2br(ent(trim($this->address)));
	}

	//! Returns the distance to a given point (lat,lng).
	function distance($lat,$lng)
	{
		if($this->zoom==0){return false;}
		$lat1=$lat             *2*M_PI/360;
		$lng1=$lng             *2*M_PI/360;
		$lat2=$this->latitude *2*M_PI/360;
		$lng2=$this->longitude*2*M_PI/360;
		$earthRadius=6371;
		return acos(sin($lat1)*sin($lat2) + 
					cos($lat1)*cos($lat2) *
					cos($lng2-$lng1)) * $earthRadius;
	}

	//! Find places matching a list of fields.
	static function find($fields,$limit=false)
	{
		$schema=static::getSchema();
		$conditions='';
		foreach($fields as $name=>$value)
		{
			$format=DBObject::dbFormat($schema[$name]['type']);
			$conditions.="\t".$name;
			// Case sensitive match
			if($format=="'%s'"){$conditions.=" COLLATE utf8mb4_bin ";}
			$conditions.='=';
			$conditions.=$format;
			$conditions.=" AND\n";
		}
		$conditions=preg_replace('@ AND\n$@s','',$conditions);
		$args=array_values($fields);
		array_unshift($args,"SELECT id FROM Place WHERE ".$conditions.($limit===false ? '': ' LIMIT '.intval($limit) ));
		$res=call_user_func_array('db_one_col',$args);
		return $res;
	}

	//! This is usefull for sites that import from external sources that don't re-use existing Places.
	//! Examples: post-from-other-site, external events (Politis)
	//! This is called from cron.
	static function mergeDuplicates()
	{
		echo "mergeDuplicates:\n";

		// ****** merge empty places to Place->id=1
		$emptyPlaces=db_one_col("SELECT Place.id FROM Place,City WHERE Place.cityId=City.id AND Place.address='' AND City.name='' AND Place.zoom=0 AND Place.referenceId=Place.id AND Place.hasDescription=0 AND Place.id!=1");
		if(count($emptyPlaces))
		{
			echo "merging empty places:".implode(',',$emptyPlaces)."\n";
			db_query('UPDATE Event SET placeId=1 WHERE placeId IN ('.implode(',',$emptyPlaces).')');
		}

		// ****** merge duplicates

		$hashFields=self::fields('all',['id','referenceId','inChargeChanged','hideFromAltSuggestion']);
		$pidToHash=db_one_col("SELECT id,MD5(CONCAT(".implode(",':@:!:',",$hashFields).")) FROM Place WHERE id!=1");
		$count=array_count_values($pidToHash);
		foreach($count as $hash=>$count)
		{
			if($count<=1){continue;}
			$pids=array_keys($pidToHash,$hash);
			echo "identical:".implode(',',$pids)."\n";

			// Select one of the places as the one that will remain
			$mergeToId=db_result("SELECT id FROM Place WHERE id IN (".implode(',',$pids).") ".
								 "ORDER BY id=referenceId DESC,hideFromAltSuggestion ASC,id ASC LIMIT 1");
			$mergeTo=Place::fetch($mergeToId);

			$mergePids=db_one_col("SELECT id FROM Place WHERE ".
								  "id IN(".implode(',',$pids).") AND ".
								  // don't merge places that reference other places than $mergeTo
								  "(referenceId=%d OR referenceId=id) AND ".
								  // don't merge places that have a description
								  "hasDescription=0 AND ".
								  // don't merge empty place (id=1)
								  "id!=1 AND ".
								  "id!=%d",
								  $mergeTo->referenceId,$mergeTo->id);

			if(count($mergePids)==0){continue;}
			echo "merge to ".$mergeToId.": ".implode(',',$mergePids)."\n";
			// update any places that have references to places in $mergePids
			$ok=db_query('UPDATE Place SET referenceId=%d WHERE referenceId IN ('.implode(',',$mergePids).') ',$mergeTo->referenceId);
			if($ok===false){fatal("failed cleanup duplicates: reref");}
			// update event placeId's
			$ok=db_query('UPDATE Event SET placeId=%d WHERE placeId IN ('.implode(',',$mergePids).')',$mergeTo->id);
			if($ok===false){fatal("failed cleanup duplicates: update placeId");}
			// delete all $mergePids
			foreach($mergePids as $mergePid)
			{
				Place::fetch($mergePid)->delete();
			}
		}
	}

	//! replace all uses of $old by this place
	function replace($old)
	{
		// update any places that have references to $old
		$ok=db_query('UPDATE Place SET referenceId=%d WHERE referenceId=%d',$this->referenceId,$old->id);
		if($ok===false){fatal("failed replace: reref");}
		// update all events that reference $old
		$ok=db_query('UPDATE Event SET placeId=%d WHERE placeId=%d',$this->id,$old->id);
		if($ok===false){fatal("failed replace: update placeId");}
		db_query("DELETE FROM place_search_index WHERE pid=%d",$old->id);
	}

	static function createTable()
	{
		db_query("DROP TABLE IF EXISTS Place");
		db_query("CREATE TABLE Place (
  `id` int(11) NOT NULL auto_increment,
  `cityId` int(11) NOT NULL,
  `address` text  NOT NULL,
  `latitude` DOUBLE  NOT NULL,
  `longitude` DOUBLE  NOT NULL,
  `zoom` TINYINT(2) NOT NULL,
  `title` varchar(330)  NOT NULL,
  `description` longtext NOT NULL,
  `hasDescription`  TINYINT(1) NOT NULL,
  `referenceId` int(11) NOT NULL,
  `hideFromAltSuggestion` TINYINT(1) NOT NULL,
  `inChargeId` int(11) NOT NULL,
  `inChargeChanged` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `cityId` (`cityId`),
  KEY `zoom` (`zoom`),
  KEY `hasDescription` (`hasDescription`),
  KEY `referenceId` (`referenceId`),
  KEY `hideFromAltSuggestion` (`hideFromAltSuggestion`),
  KEY `inChargeId` (`inChargeId`),
  KEY `inChargeChanged` (`inChargeChanged`)
) DEFAULT CHARSET=utf8mb4;");

	}
}

?>