<?php

/**
 * Format date acording to locale using predefined formats.
 * 
 * This	 returns a	formated  date or  time.   It is  used instead	of
 * strftime	 to	 allow	 translations  to  different  languages	 using
 * different time formats.
 * FIXME: we should consider using IntlDateFormatter
 */
function demos_format_date($description,$ts,$ts2=false)
{
	static $locale=false;
	if($locale===false)
	{
		$locale=setlocale(LC_TIME,"0");
		if(strpos($locale,'en_US')===0){$locale='en_US';}
		else                           {$locale=preg_replace('@[._].*$@','',$locale);}
	}
	// locale independent formats
	switch($description)
	{
	case 'demosphere-timestamp': 
		return str_replace(' 03:33','',strftime("%Y-%m-%d %R",$ts));
	}
	// locale dependent formats
	switch($locale)
	{
	case 'fr'    : return demos_format_date_french    ($description,$ts,$ts2);
	case 'en'    : return demos_format_date_english   ($description,$ts,$ts2);
	case 'en_US' : return demos_format_date_english_us($description,$ts,$ts2);
	case 'el'    : return demos_format_date_greek	  ($description,$ts,$ts2);
	case 'gl'    : return demos_format_date_galician  ($description,$ts,$ts2);
	case 'es'    : return demos_format_date_spanish   ($description,$ts,$ts2);
	case 'pt'    : return demos_format_date_portuguese($description,$ts,$ts2);
	case 'ca'    : return demos_format_date_catalan   ($description,$ts,$ts2);
	case 'de'    : return demos_format_date_german    ($description,$ts,$ts2);
	default: fatal("demos_format_date: unknown locale".$locale);
	}

}

//! Make sure all language functions are uptdated (use scripts/compare-date-time-language-fcts)
function demos_format_date_french($description,$ts,$ts2=false)
{
	switch($description)
	{
	case 'short-day-month': return strftime('%d/%m',$ts);
	case 'short-day-month-year': return strftime('%d/%m/%Y',$ts);
	case 'day-month-abbrev': return strftime('%e %b',$ts);
	case 'shorter-date-time': return str_replace(' 03:33','',strftime('%d/%m %R',$ts));
	case 'short-date-time': return str_replace(' 03:33','',strftime('%e %B %R',$ts));
	case 'short-week-date-time': return str_replace([' 03:33',':00',':'],['','h','h'],
													strftime('%a %e %B %R',$ts));
	case 'approx-short-date-time': return strftime('%e %B %Hh',$ts);
	case 'time': return str_replace('03:33','-',strftime('%R',$ts));
	case 'event-list': 
		return str_replace('|03:33','|',strftime('%a|%e|%b|%R',$ts));
	case 'panel-list': 
		return str_replace('|'.strftime('%Y').'|','||',
						   str_replace('|03:33','|',strftime('%a|%e|%b|%Y|%R',$ts)));
	case 'full-date-time':
	case 'full-date-time-date':
	case 'full-date-time-time':
	case 'full-date-time-no-year':
		$res=strftime($description==='full-date-time-no-year'?
					  '%A %e %B| à %R':
					  '%A %e %B %Y| à %R',$ts);
		$res=str_replace(
			[' 1 ',
			 ' à 03:33',
			 ' à 0',
			 ':00',
			 ':',],
			[' 1er ',
			 $description==='full-date-time-no-year' ? '' :  ' (heure non définie)',
			 ' à ',
			 ':',   
			 'h',],
						 $res);
		if($description==='full-date-time-date'){$res=substr($res,0,strpos($res,'|'));}
		else
		if($description==='full-date-time-time'){$res=substr($res,strpos($res,'|')+1);}
		else{$res=str_replace('|','',$res);}
		return $res;
	case 'week-and-day':
		return preg_replace('@ 1$@',' 1er',strftime('%A %e',$ts));
	case 'week-day-month':
		return preg_replace('@ 1 @',' 1er ',strftime('%A %e %B',$ts));
	case 'relative-short-full':
		$full=true;
	case 'relative-short': 
		if($ts==0){return '-';}
		$isRel=true;
		if($ts2===false){$ts2=time();$isRel=false;}
		$dt=$ts2-$ts;
		if(!isset($full) && $dt<0){return 'futur';}
		$f=demos_format_date_relative_short($ts,$ts2,
											[
												'y'=>[' an'   ,' ans'   ],
												'm'=>[' mois' ,' mois'  ],
												'd'=>[' jour' ,' jours' ],
												'h'=>[' heure',' heures'],
												'i'=>[' min'  ,' min'   ],
												's'=>['s'     ,'s'      ],
											]);
		if(!isset($full)){return $f;}
		if($isRel){return $dt<0 ? $f.' après' : $f.' avant' ;}
		return $dt<0 ? 'dans '.$f : 'il y a '.$f;
	default: fatal('demos_format_date: unknown description:'.$description);
	}
}

function demos_format_date_english($description,$ts,$ts2=false)
{
	switch($description)
	{
	case 'short-day-month': return strftime('%d/%m',$ts);
	case 'short-day-month-year': return strftime('%d/%m/%Y',$ts);
	case 'day-month-abbrev': return strftime('%e %b',$ts);
	case 'shorter-date-time': return str_replace(' 03:33','',strftime('%d/%m %R',$ts));
	case 'short-date-time': return str_replace(' 03:33','',strftime('%e %B %R',$ts));
	case 'short-week-date-time': return str_replace([' 03:33',':00',':'],['','h','h'],
													strftime('%a %e %B %R',$ts));
	case 'approx-short-date-time': return strftime('%e %B %Hh',$ts);
	case 'time': return str_replace('03:33','-',strftime('%R',$ts));
	case 'event-list': 
		return str_replace('|03:33','|',strftime('%a|%e|%b|%R',$ts));
	case 'panel-list': 
		return str_replace('|'.strftime('%Y').'|','||',
						   str_replace('|03:33','|',strftime('%a|%e|%b|%Y|%R',$ts)));
	case 'full-date-time':
	case 'full-date-time-date':
	case 'full-date-time-time':
	case 'full-date-time-no-year':
		$res=strftime($description==='full-date-time-no-year'?
					  '%A %e %B| at %R':
					  '%A %e %B %Y| at %R',$ts);
		$res=str_replace(
			[' at 03:33',
			 ' at 0',],
			[$description==='full-date-time-no-year' ? '' : ' (no time)',
			 ' at ',],
						 $res);
		if($description==='full-date-time-date'){$res=substr($res,0,strpos($res,'|'));}
		else
		if($description==='full-date-time-time'){$res=substr($res,strpos($res,'|')+1);}
		else{$res=str_replace('|','',$res);}
		return $res;
	case 'week-and-day':
		return preg_replace('@ 1$@',' 1st',strftime('%A %e',$ts));
	case 'week-day-month':
		return preg_replace('@ 1$@',' 1st ',strftime('%A %e %B',$ts));
	case 'relative-short-full':
		$full=true;
	case 'relative-short': 
		if($ts==0){return '-';}
		$isRel=true;
		if($ts2===false){$ts2=time();$isRel=false;}
		$dt=$ts2-$ts;
		if(!isset($full) && $dt<0){return 'future';}
		$f=demos_format_date_relative_short($ts,$ts2,
											[
												'y'=>[' year' ,' years' ],
												'm'=>[' month',' months'],
												'd'=>[' day'  ,' days'  ],
												'h'=>[' hour' ,' hours' ],
												'i'=>[' min'  ,' mins'  ],
												's'=>['s'     ,'s'      ],
											]);
		if(!isset($full)){return $f;}
		if($isRel){return $dt<0 ? $f.' after' : $f.' before';}
		return $dt<0 ? 'in '.$f : $f.' ago';
	default: fatal('demos_format_date: unknown description:'.$description);
	}
}

function demos_format_date_english_us($description,$ts,$ts2=false)
{
	switch($description)
	{
	case 'short-day-month': return strftime('%m/%d',$ts);
	case 'short-day-month-year': return strftime('%m/%d/%Y',$ts);
	case 'day-month-abbrev': return strftime('%b %e',$ts);
	case 'shorter-date-time': return str_replace(['  ',' 3:33 AM'],[' ',''],
												 strftime('%m/%d %l:%M %p',$ts));
	case 'short-date-time': return str_replace(['  ',', 3:33 AM'],[' ',''],
											   strftime('%B %e, %l:%M %p',$ts));
	case 'short-week-date-time': return str_replace(['  ',' 3:33 AM'],[' ',''],
													strftime('%a %B %e %l:%M %p',$ts));
	case 'approx-short-date-time': return strftime('%B %e',$ts).','.
			str_replace(' 0',' ',strftime(' %I%p',$ts));
	case 'time': return str_replace(['  ',' 3:33 AM'],[' ','-'],
									strftime('%l:%M %p',$ts));
	case 'event-list': 
		return str_replace('|03:33','|',strftime('%a|%b|%e|%R',$ts));
	case 'panel-list': 
		return str_replace('|'.strftime('%Y').'|','||',
						   str_replace(['| ','|3:33 AM'],['|','|'],
									   strftime('%a|%b|%e|%Y|%l:%M %p',$ts)));
	case 'full-date-time': 
	case 'full-date-time-date':
	case 'full-date-time-time':
	case 'full-date-time-no-year': 
		$res=strftime($description==='full-date-time-no-year'?
					  '%A, %B %e| at %l:%M %p':
					  '%A, %B %e, %Y| at %l:%M %p',
					  $ts);
		$res=str_replace(
			[' at  ',
			 ' at 3:33 AM',
			 ' at 0',
			 ':00',],
			[' at ',
			 ' (no time)',
			 ' at ',
			 '',],
						 $res);
		if($description==='full-date-time-date'){$res=substr($res,0,strpos($res,'|'));}
		else
		if($description==='full-date-time-time'){$res=substr($res,strpos($res,'|')+1);}
		else{$res=str_replace('|','',$res);}
		return $res;
	case 'week-and-day':
		return preg_replace('@ 1$@',' 1st',strftime('%A %e',$ts));
	case 'week-day-month':
		return preg_replace('@ 1$@',' 1st ',strftime('%A %B %e',$ts));
	case 'relative-short-full':
		$full=true;
	case 'relative-short': 
		if($ts==0){return '-';}
		$isRel=true;
		if($ts2===false){$ts2=time();$isRel=false;}
		$dt=$ts2-$ts;
		if(!isset($full) && $dt<0){return 'future';}
		$f=demos_format_date_relative_short($ts,$ts2,
											[
												'y'=>[' year'   ,' years'],
												'm'=>[' month' ,' months'],
												'd'=>[' day' ,' days'],
												'h'=>[' hour',' hours'],
												'i'=>[' min'  ,' mins'],
												's'=>['s'     ,'s'],
											]);
		if(!isset($full)){return $f;}
		if($isRel){return $dt<0 ? $f.' after' : $f.' before';}
		return $dt<0 ? 'in '.$f : $f.' ago';
	default: fatal('demos_format_date: unknown description:'.$description);
	}
}

function demos_format_date_greek($description,$ts,$ts2=false)
{
	static $months=['Ιανουαρίου','Φεβρουαρίου','Μαρτίου','Απριλίου',
					'Μαίου','Ιουνίου','Ιουλίου','Αυγούστου','Σεπτεμβρίου',
					'Οκτωβρίου','Νοεμβρίου','Δεκεμβρίου'];

	switch($description)
	{
	case 'short-day-month': return strftime('%d/%m',$ts);
	case 'short-day-month-year': return strftime('%d/%m/%Y',$ts);
	case 'day-month-abbrev': return strftime('%e %b',$ts);
	case 'shorter-date-time': return str_replace(' 03:33','',strftime('%d/%m %R',$ts));
	case 'short-date-time': return str_replace(' 03:33','',strftime('%e %B %R',$ts));
	case 'short-week-date-time': return str_replace(' 03:33','',strftime('%a %e %B %R',$ts));
	case 'approx-short-date-time': return strftime('%e %B %Hh',$ts);
	case 'time': return str_replace('03:33','-',strftime('%R',$ts));
	case 'event-list': 
		return str_replace('|03:33','|',strftime('%a|%e|%b|%R',$ts));
	case 'panel-list': 
		return str_replace('|'.strftime('%Y').'|','||',
						   str_replace('|03:33','|',strftime('%a|%e|%b|%Y|%R',$ts)));
	case 'full-date-time': 
	case 'full-date-time-date':
	case 'full-date-time-time':
	case 'full-date-time-no-year': 
		$res=strftime($description==='full-date-time-no-year'?
					  '%A %e MONTH| στις %R':
					  '%A %e MONTH %Y| στις %R',$ts);
		$res=str_replace(
			['MONTH',
			 ' στις 03:33',
			 ' στις 0',
			 ':',],
			[$months[intval(date('n',$ts))-1],
			 ' (χωρίς ακριβή ώρα)',
			 ' στις ',
			 '.',],
						 $res);
		$hour=intval(preg_replace('@^0*@','',strftime('%H',$ts)));
		if($hour<=10){$res.=strftime(' %p',$ts);}
		if($description==='full-date-time-date'){$res=substr($res,0,strpos($res,'|'));}
		else
		if($description==='full-date-time-time'){$res=substr($res,strpos($res,'|')+1);}
		else{$res=str_replace('|','',$res);}
		return $res;
	case 'week-and-day': return strftime('%A %e',$ts);
	case 'week-day-month':
		return str_replace('MONTH',$months[intval(date('n',$ts))-1],
						   strftime('%A %e MONTH',$ts));
	case 'relative-short-full':
		$full=true;
	case 'relative-short': 
		if($ts==0){return '-';}
		$isRel=true;
		if($ts2===false){$ts2=time();$isRel=false;}
		$dt=$ts2-$ts;
		if(!isset($full) && $dt<0){return 'futur';}
		$f=demos_format_date_relative_short($ts,$ts2,
											[
												'y'=>[' χρόνος',' χρόνια'],
												'm'=>[' μήνας' ,' μήνες' ],
												'd'=>[' ημέρα' ,' ημέρες'],
												'h'=>[' ώρα'   ,' ώρες'  ],
												'i'=>[' λεπτό' ,' λεπτά' ],
												's'=>['s'     ,'s'       ],
											]);
		if(!isset($full)){return $f;}
		if($isRel){return $dt<0 ? $f.' μετά' : $f.' πριν' ;}
		return $dt<0 ? 'σε '.$f : $f.' πριν';
	default: fatal('demos_format_date: unknown description:'.$description);
	}
}

function demos_format_date_galician($description,$ts,$ts2=false)
{
	switch($description)
	{
	case 'short-day-month': return strftime('%d/%m',$ts);
	case 'short-day-month-year': return strftime('%d/%m/%Y',$ts);
	case 'day-month-abbrev': return strftime('%e %b',$ts);
	case 'shorter-date-time': return str_replace(' 03:33','',strftime('%d/%m %R',$ts));
	case 'short-date-time': return str_replace(' 03:33','',strftime('%e %B %R',$ts));
	case 'short-week-date-time': return str_replace(' 03:33','',strftime('%a %e %B %R',$ts));
	case 'approx-short-date-time': return strftime('%e %B %Hh',$ts);
	case 'time': return str_replace('03:33','-',strftime('%R',$ts));
	case 'event-list': 
		return str_replace('|03:33','|',strftime('%a|%e|%b|%R',$ts));
	case 'panel-list': 
		return str_replace('|'.strftime('%Y').'|','||',
						   str_replace('|03:33','|',strftime('%a|%e|%b|%Y|%R',$ts)));
	case 'full-date-time': 
	case 'full-date-time-date':
	case 'full-date-time-time':
	case 'full-date-time-no-year': 
		$res=strftime($description==='full-date-time-no-year'?
					  '%A %e %B| - %R':
					  '%A %e %B %Y| - %R',$ts);
		$res=str_replace(
			[' à 03:33',
			 ' à 0',
			 ':00',
			 ':',],
			[' (sin hora)',
			 ' à ',
			 ':',   
			 'h',],
						 $res);
		if($description==='full-date-time-date'){$res=substr($res,0,strpos($res,'|'));}
		else
		if($description==='full-date-time-time'){$res=substr($res,strpos($res,'|')+1);}
		else{$res=str_replace('|','',$res);}
		return $res;
	case 'week-and-day':
		return preg_replace('@ 1$@',' 1er',strftime('%A %e',$ts));
	case 'week-day-month':
		return preg_replace('@ 1 @',' 1er ',strftime('%A %e %B',$ts));
	case 'relative-short-full':
		$full=true;
	case 'relative-short': 
		if($ts==0){return '-';}
		$isRel=true;
		if($ts2===false){$ts2=time();$isRel=false;}
		$dt=$ts2-$ts;
		if(!isset($full) && $dt<0){return 'futuro';}
		$f=demos_format_date_relative_short($ts,$ts2,
											[
												'y'=>[' ano'   ,' anos' ],
												'm'=>[' mes'   ,' meses'],
												'd'=>[' día'   ,' días' ],
												'h'=>[' hora'  ,' horas'],
												'i'=>[' minuto',' mins' ],
												's'=>['s'      ,'s'     ],
											]);
		if(!isset($full)){return $f;}
		if($isRel){return $dt<0 ? $f.' después' : $f.' antes' ;}
		return $dt<0 ? 'dentro '.$f : 'hace '.$f;
	default: fatal('demos_format_date: unknown description:'.$description);
	}
}

function demos_format_date_portuguese($description,$ts,$ts2=false)
{
	switch($description)
	{
	case 'short-day-month': return strftime('%d-%m',$ts);
	case 'short-day-month-year': return strftime('%d-%m-%Y',$ts);
	case 'day-month-abbrev': return strftime('%e %b',$ts);
	case 'shorter-date-time': return str_replace(' 03:33','',strftime('%d-%m %R',$ts));
	case 'short-date-time': return str_replace(' 03:33','',strftime('%e %B %R',$ts));
	case 'short-week-date-time': return str_replace(' 03:33','',strftime('%a %e %B %R',$ts));
	case 'approx-short-date-time': return strftime('%e %B %Hh',$ts);
	case 'time': return str_replace('03:33','-',strftime('%R',$ts));
	case 'event-list': 
		return str_replace('|03:33','|',strftime('%a|%e|%b|%R',$ts));
	case 'panel-list': 
		return str_replace('|'.strftime('%Y').'|','||',
						   str_replace('|03:33','|',strftime('%a|%e|%b|%Y|%R',$ts)));
	case 'full-date-time': 
	case 'full-date-time-date':
	case 'full-date-time-time':
	case 'full-date-time-no-year': 
		$w=intval(date('w',$ts));
		$feira='-feira';
		if($w===0 || $w===6){$feira='';}
		$res=strftime($description==='full-date-time-no-year'?
					  '%A'.$feira.', %e de %B| - %R':
					  '%A'.$feira.', %e de %B de %Y| - %R',$ts);
		$res=str_replace(
			[' 1 de ',
			 ' - 03:33',
			 ' - 0',
			 ':00',
			 ':',],
			[' 1º de ',
			 ' (sem hora)',
			 ' - ',
			 ':',   
			 'h',],
						 $res);
		if($description==='full-date-time-date'){$res=substr($res,0,strpos($res,'|'));}
		else
		if($description==='full-date-time-time'){$res=substr($res,strpos($res,'|')+1);}
		else{$res=str_replace('|','',$res);}
		return $res;
	case 'week-and-day':
		return strftime('%A %e',$ts);
	case 'week-day-month':
		return strftime('%A %e %B',$ts);
	case 'relative-short-full':
		$full=true;
	case 'relative-short': 
		if($ts==0){return '-';}
		$isRel=true;
		if($ts2===false){$ts2=time();$isRel=false;}
		$dt=$ts2-$ts;
		if(!isset($full) && $dt<0){return 'futuro';}
		$f=demos_format_date_relative_short($ts,$ts2,
											[
												'y'=>[' ano'   ,' anos' ],
												'm'=>[' mês'   ,' meses'],
												'd'=>[' dia'   ,' dias' ],
												'h'=>[' hora'  ,' horas'],
												'i'=>[' min'   ,' mins' ],
												's'=>['s'      ,'s'     ],
											]);
		if(!isset($full)){return $f;}
		if($isRel){return $dt<0 ? $f.' después' : $f.' antes' ;}
		return $dt<0 ? 'dentro '.$f : 'hace '.$f;
	default: fatal('demos_format_date: unknown description:'.$description);
	}
}

function demos_format_date_spanish($description,$ts,$ts2=false)
{
	switch($description)
	{
	case 'short-day-month': return strftime('%d/%m',$ts);
	case 'short-day-month-year': return strftime('%d/%m/%Y',$ts);
	case 'day-month-abbrev': return strftime('%e %b',$ts);
	case 'shorter-date-time': return str_replace(' 03:33','',strftime('%d/%m %R',$ts));
	case 'short-date-time': return str_replace(' 03:33','',strftime('%e %B %R',$ts));
	case 'short-week-date-time': return str_replace(' 03:33','',strftime('%a %e %B %R',$ts));
	case 'approx-short-date-time': return strftime('%e %B %Hh',$ts);
	case 'time': return str_replace('03:33','-',strftime('%R',$ts));
	case 'event-list': 
		return str_replace('|03:33','|',strftime('%a|%e|%b|%R',$ts));
	case 'panel-list': 
		return str_replace('|'.strftime('%Y').'|','||',
						   str_replace('|03:33','|',strftime('%a|%e|%b|%Y|%R',$ts)));
	case 'full-date-time':
	case 'full-date-time-date':
	case 'full-date-time-time':
	case 'full-date-time-no-year':
		$res=strftime($description==='full-date-time-no-year'?
					  '%A %e de %B| - %R':
					  '%A %e de %B de %Y| - %R',$ts);
		$res=str_replace(
			[' - 03:33',
			 ' - 0',
			 ':00',
			 ':',],
			[$description==='full-date-time-no-year' ? '' :  ' (sin hora)',
			 ' - ',
			 ':',   
			 'h',],
						 $res);
		if($description==='full-date-time-date'){$res=substr($res,0,strpos($res,'|'));}
		else
		if($description==='full-date-time-time'){$res=substr($res,strpos($res,'|')+1);}
		else{$res=str_replace('|','',$res);}
		return $res;
	case 'week-and-day':
		return strftime('%A %e',$ts);
	case 'week-day-month':
		return strftime('%A %e %B',$ts);
	case 'relative-short-full':
		$full=true;
	case 'relative-short': 
		if($ts==0){return '-';}
		$isRel=true;
		if($ts2===false){$ts2=time();$isRel=false;}
		$dt=$ts2-$ts;
		if(!isset($full) && $dt<0){return 'futuro';}
		$f=demos_format_date_relative_short($ts,$ts2,
											[
												'y'=>[' año'   ,' años' ],
												'm'=>[' mes'   ,' meses'],
												'd'=>[' día'   ,' días' ],
												'h'=>[' hora'  ,' horas'],
												'i'=>[' min'   ,' mins' ],
												's'=>['s'      ,'s'     ],
											]);
		if(!isset($full)){return $f;}
		if($isRel){return $dt<0 ? $f.' después' : $f.' antes' ;}
		return $dt<0 ? 'dentro '.$f : 'hace '.$f;
	default: fatal('demos_format_date: unknown description:'.$description);
	}
}


function demos_format_date_catalan($description,$ts,$ts2=false)
{
	switch($description)
	{
	case 'short-day-month': return strftime('%d/%m',$ts);
	case 'short-day-month-year': return strftime('%d/%m/%Y',$ts);
	case 'day-month-abbrev': return strftime('%e %b',$ts);
	case 'shorter-date-time': return str_replace(' 03:33','',strftime('%d/%m %R',$ts));
	case 'short-date-time': return str_replace(' 03:33','',strftime('%e %B %R',$ts));
	case 'short-week-date-time': return str_replace(' 03:33','',strftime('%a %e %B %R',$ts));
	case 'approx-short-date-time': return strftime('%e %B %Hh',$ts);
	case 'time': return str_replace('03:33','-',strftime('%R',$ts));
	case 'event-list': 
		return str_replace('|03:33','|',strftime('%a|%e|%b|%R',$ts));
	case 'panel-list': 
		return str_replace('|'.strftime('%Y').'|','||',
						   str_replace('|03:33','|',strftime('%a|%e|%b|%Y|%R',$ts)));
	case 'full-date-time': 
	case 'full-date-time-date':
	case 'full-date-time-time':
	case 'full-date-time-no-year': 
		$res=strftime($description==='full-date-time-no-year'?
					  '%A, %e XXX %B| - %R hores':
					  '%A, %e XXX %B de %Y| - %R hores',$ts);
		$res=str_replace(
			[' - 03:33',
			 ' - 0',
			 ':00',],
			[$description==='full-date-time-no-year' ? '' :  ' (sense temps)',
			 ' - ',
			 ':',],
						 $res);
		$res=preg_replace('@XXX (?=[aeiou])@',"d'",$res);
		$res=str_replace('XXX','de',$res);
		if($description==='full-date-time-date'){$res=substr($res,0,strpos($res,'|'));}
		else
		if($description==='full-date-time-time'){$res=substr($res,strpos($res,'|')+1);}
		else{$res=str_replace('|','',$res);}
		return $res;
	case 'week-and-day':
		return strftime('%A %e',$ts);
	case 'week-day-month':
		return strftime('%A %e %B',$ts);
	case 'relative-short-full':
		$full=true;
	case 'relative-short': 
		if($ts==0){return '-';}
		$isRel=true;
		if($ts2===false){$ts2=time();$isRel=false;}
		$dt=$ts2-$ts;
		if(!isset($full) && $dt<0){return 'futur';}
		$f=demos_format_date_relative_short($ts,$ts2,
											[
												'y'=>[' any'   ,' anys'  ],
												'm'=>[' mes'   ,' mesos' ],
												'd'=>[' dia'   ,' dies'  ],
												'h'=>[' hora'  ,' hores' ],
												'i'=>[' min'   ,' minuts'],
												's'=>['s'      ,'s'      ],
											]);
		if(!isset($full)){return $f;}
		if($isRel){return $dt<0 ? $f.' después' : $f.' antes' ;}
		return $dt<0 ? 'dentro '.$f : 'hace '.$f;
	default: fatal('demos_format_date: unknown description:'.$description);
	}
}


function demos_format_date_german($description,$ts,$ts2=false)
{
	switch($description)
	{
	case 'short-day-month': return strftime('%d.%m',$ts);
	case 'short-day-month-year': return strftime('%d.%m.%Y',$ts);
	case 'day-month-abbrev': return strftime('%e.%b',$ts);
	case 'shorter-date-time': return str_replace(' 03:33','',strftime('%d.%m %R',$ts));
	case 'short-date-time': return str_replace(' 03:33','',strftime('%e. %B %R',$ts));
	case 'short-week-date-time': return str_replace([' 03:33'],[''],
													strftime('%a %e %B %R',$ts));
	case 'approx-short-date-time': return strftime('%e. %B %H Uhr',$ts);
	case 'time': return str_replace('03:33','-',strftime('%R',$ts));
	case 'event-list': 
		return str_replace('|03:33','|',strftime('%a|%e|%b|%R',$ts));
	case 'panel-list': 
		return str_replace('|'.strftime('%Y').'|','||',
						   str_replace('|03:33','|',strftime('%a|%e|%b|%Y|%R',$ts)));
	case 'full-date-time':
	case 'full-date-time-date':
	case 'full-date-time-time':
	case 'full-date-time-no-year':
		$res=strftime($description==='full-date-time-no-year'?
					  '%A, %e. %B| - %R':
					  '%A, %e. %B %Y| - %R',$ts);
		$res=str_replace(
			[' - 03:33',
			],
			[$description==='full-date-time-no-year' ? '' :  ' (keine zeit)',
			],
						 $res);
		if($description==='full-date-time-date'){$res=substr($res,0,strpos($res,'|'));}
		else
		if($description==='full-date-time-time'){$res=substr($res,strpos($res,'|')+1);}
		else{$res=str_replace('|','',$res);}
		return $res;
	case 'week-and-day':
		return strftime('%A, %e.',$ts);
	case 'week-day-month':
		return strftime('%A, %e. %B',$ts);
	case 'relative-short-full':
		$full=true;
	case 'relative-short': 
		if($ts==0){return '-';}
		$isRel=true;
		if($ts2===false){$ts2=time();$isRel=false;}
		$dt=$ts2-$ts;
		if(!isset($full) && $dt<0){return 'zukunft';}
		$f=demos_format_date_relative_short($ts,$ts2,
											[
												'y'=>[' jahr'  ,' jahre'  ],
												'm'=>[' monat' ,' monate' ],
												'd'=>[' tag'   ,' tage'   ],
												'h'=>[' stunde',' stunden'],
												'i'=>[' min'   ,' min'    ],
												's'=>['s'      ,'s'       ],
											]);
		if(!isset($full)){return $f;}
		if($isRel){return $dt<0 ? $f.' danach' : $f.' vorher' ;}
		return $dt<0 ? 'in '.$f : 'vor '.$f ;
	default: fatal('demos_format_date: unknown description:'.$description);
	}
}


/** A simple function to help formating a timespan between two dates
 *
 * @param how many seconds ago
 * @param an array of arrays (see example)
 */
function demos_format_date_relative_short($ts1,$ts2,$unitLabels)
{
	$dt1=new DateTime();
	$dt1->setTimestamp($ts1);
	$dt2=new DateTime();
	$dt2->setTimestamp($ts2);
	$diff=$dt2->diff($dt1);

	$units=['y','m','d','h','i','s'];
	//foreach($units as $nUnit=>$unit){echo $unit.":".sprintf("%02d",$diff->$unit).", ";}
	foreach($units as $nUnit=>$unit)
	{
		if($diff->$unit!==0){break;}
	}
	$topUnit=$nUnit;
	$topUnitVal=$diff->$unit;

	// We only display the most significant unit. We need to round it to next (hidden) unit.
	$max=[1000000,12,30,24,60,60,];
	if($topUnit+1<count($units))
	{
		$roundUnitName=$units[$topUnit+1];
		if($diff->$roundUnitName>=$max[$topUnit+1]/2)
		{
			$topUnitVal++;
			if($topUnitVal>=$max[$topUnit]){$topUnit--;$topUnitVal=1;}
		}
	}

	return $topUnitVal.$unitLabels[$units[$topUnit]][$topUnitVal===1 ? 0 : 1];
}

//! hackish guess of 'short-day-month-year' format
function demosphere_date_time_short_day_month_year()
{
	$date=demos_format_date('short-day-month-year',strtotime('2001-03-04'));
	$sep='/';
	if(strpos($date,'.')!==false){$sep='.';}
	if(strpos($date,'-')!==false){$sep='-';}
	$date=str_replace(['1','3','4','2','0'],['%Y','%m','%d','',''],$date);
	return [$date,$sep];
}

/**
 * Parse a string into a timestamp, using "short-day-month-year  hh:mm" format.
 *
 * We don't use find-date-time, because times can be ambigous (early morning am/pm)
 * ("13 sep. 2011 8:00" is "8am" or "8pm" ?)
 */
function demosphere_date_time_parse_short($date,$time=false)
{
	global $demosphere_config;
	
	if($time===false || $time===''){$time='3:33';}

	require_once 'demosphere-date-time.php';
	list($fmt,$sep)=demosphere_date_time_short_day_month_year();
	$regex=str_replace(['.','%Y','%d','%m'],['\\.','(?P<year>\d\d\d\d)','(?P<day>\d\d?)','(?P<month>\d\d?)'],$fmt);

	if(!preg_match('@^'.$regex.'$@',trim($date), $dateMatches))
	{
		return false;
	}

	// check if this is a real, valid, date
	if(!checkdate($dateMatches['month'], $dateMatches['day'], $dateMatches['year'])){return false;}

    // sanity check and avoid timestamp int problems
    if($dateMatches['year']>2037 || $dateMatches['year']<1980){return false;}

	$timeParts=demosphere_date_time_parse_time($time);
	if($timeParts===false){return false;}

	$res=mktime($timeParts['hours'], $timeParts['minutes'], 0, 
				$dateMatches['month'], $dateMatches['day'], $dateMatches['year']);
	return $res;
}

//! Parse hh:mm into an array ['hours'=>...,'minutes'=>...]
//! false if invalid
function demosphere_date_time_parse_time($time)
{
	if(!preg_match('@^(?P<hours>[0-2]?\d)\s*[:hH]\s*(?P<minutes>\d\d)$@',
				   trim($time), $timeMatches))
	{
		return false;
	}
	if($timeMatches['hours']>23 || $timeMatches['minutes']>59){return false;}
	return array_intersect_key($timeMatches,['hours'=>true,'minutes'=>true]);
}

//! returns v1 v2 if date in current locale is small endian, and v2 v1 if middle endian (USA)
//! optinally returns string $v1.$sep.$v2 or $v2.$sep.$v1
function demosphere_date_time_swap_endian($v1,$v2,$sep=false)
{
	$date=demos_format_date('short-day-month-year',strtotime('2000-03-04'));
	$isMiddle=strpos($date,'3') < strpos($date,'4') && strpos($date,'2000')>strpos($date,'3');
	if($isMiddle){$res=[$v2,$v1];}
	else         {$res=[$v1,$v2];}
	return $sep===false ? $res : implode($sep,$res);
}

/**
 * return timestamp where "unset time" (3:33) is replaced by argument's time.
 *
 * events represent an	 undefined time in a timestamp	by using a
 * specific time : 3h33 . This	is a bit hacky, but simplifies things.
 * set_default_time changes the time (but not the date) of $ts if time
 * in $ts is unspecified (eg. 3h33). The time used is the time present
 * in $referenceTs.
 */
function set_default_time($ts,$referenceTs)
{
	// if time is undefined, use reference's time
	if(date('G i',$ts)=='3 33')
	{
		$ts=mktime(date('G',$referenceTs),
				   date('i',$referenceTs),0,
				   date('m',$ts),date('d',$ts),date('Y',$ts));
	}
	return $ts;
}
