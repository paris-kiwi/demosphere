<?php

function demosphere_post_view($pid)
{
	$post=Post::fetch($pid,false);
	if($post===null){dlib_not_found_404();}

	return demosphere_post_render($post);
}

function demosphere_post_access($euser,$postId,$op)
{
	$post=Post::fetch($postId,false);
	if($post===null){dlib_not_found_404();}
	if($op===''){$op='view';}

	return $euser->checkRoles('admin','moderator') || 
		($op=='view' && $post->status===1);
}

function demosphere_post_render($post)
{
	global $base_url,$demosphere_config,$user,$currentPage;
	$isEventAdmin=$user->checkRoles('admin','moderator');
	require_once 'demosphere-common.php';
	require_once 'demosphere/demosphere-page.php';

	$currentPage->title=$post->title;
	$currentPage->canonicalUrl=$demosphere_config['std_base_url'].'/'.demosphere_path_alias_dest('post/'.intval($post->id));
	// Enable google indexing only if no other get args other than $_GET['q']
	if(count($_GET)==1){$currentPage->robots=[];}
	
	$currentPage->bodyClasses[]="notFullWidth";
	$currentPage->bodyClasses[]="nonStdContent"; 
	$currentPage->addCssTpl('demosphere/css/demosphere-htmlview.tpl.css');

	demosphere_post_tabs($post,'view');

	$body=$post->body;

	if(isset($_GET['politis']))
	{
		$body=preg_replace('@(<a[^>]*href="[^"]*/publish)"@',
								 '$1?politis"',$body);
	}

	require_once 'demosphere-htmlview.php';
	$body=demosphere_htmlview_body($body);

	if(strpos($body,'demosphere_dtoken_')!==false)
	{
		require_once 'demosphere-dtoken.php';
		$body=demosphere_dtokens($body);
	}

	require_once 'dlib/comments/comments.php';
	$currentPage->addCssTpl('demosphere/css/demosphere-comments.tpl.css');
	$currentPage->addJs('dlib/comments/comments.js','file',['loadFunction'=>'load_comments_js']);
	$useComments=comments_page_settings('Post',$post->id)!=='disabled';
	$nbComments=Comment::nb('Post',$post->id);
	$comments=comments_page_render('Post',$post->id);

	return template_render('templates/demosphere-post.tpl.php',
						   [compact('post','body','useComments','nbComments','comments','isEventAdmin'),
						   ]);
}

function demosphere_post_edit_form($pid=false)
{
	global $base_url,$demosphere_config,$currentPage,$user;
	if($pid===false){$post=new Post('',$user->id,'');}	
	else
	{
		$post=Post::fetch($pid,false);
		if($post===null){dlib_not_found_404();}
	}

	$currentPage->title=t('Edit: ').oval($post,'title','new');

	demosphere_post_tabs($post,'edit');

	require_once 'dlib/form.php';
	list($objItems,$formOpts)=form_dbobject($post);

	$infoboxes=dlib_array_column($demosphere_config["front_page_infoboxes"],'pid');
	$isInfobox=array_search($post->id,$infoboxes)!==false;

	$items=[];

	// add small customizations (help text...) to the form special Posts (frontpage messages...).
	$message=false;
	$isBuiltIn=false;
	if($post->id==$demosphere_config['frontpage_messages'])
	{
		$message=t('<strong>Frontpage messages:</strong> Short messages that will be shown on the frontpage. You can enter several different messages separated by a (large) horizontal line. If you enter several messages, one will be randomly chosen. If a message begins by "#" then it is disactivated (it will not be chosen).');
	}
	else
	if($post->id==$demosphere_config['frontpage_announcement'])
	{
 		$message=t('<strong>Frontpage announcement:</strong> Enter an announcement that will be shown on the frontpage. You can use images. Float-right images (red right arrow) will actually be displayed on the left. For correct results you should put this type of images at the begining of your text.');
	}
	else
	if($isInfobox)
	{
 		$message=t('<strong>Infobox:</strong> This text is displayed in one of the infoboxes on the frontpage. To manage infoboxes, follow this link: ').
			'<a href="'.$base_url.'/configure/frontpage#front_page_infoboxes">configure</a>';
		$boxKey=dlib_first_key(preg_grep('@demosphereMapLink@',dlib_array_column($demosphere_config['front_page_infoboxes'],'class')));
		if($boxKey!==null && $post->id==$demosphere_config['front_page_infoboxes'][$boxKey]['pid'])
		{
			$message.='<br/>'.
				t('Colors are chosen from topics, in order. You can change a color by adding @topicnumber after the time. Example: 18:30@123');
		}
	}
	else
	{	
		require_once 'demosphere-built-in-pages.php';
		$message=demosphere_built_in_pages_post_edit_message($post->id);
		$isBuiltIn=$message!==false;
	}

	if($message!==false){$items['message']=['html'=>'<div class="post-top-message" style="clear:both">'.$message.'</div>'];}


	$items['title']=$objItems['title'];

	require_once 'htmledit/demosphere-htmledit.php';
	$items['body']=$objItems['body'];
	$items['body']['suffix']=demosphere_htmledit_js_config('edit-body');
	$items['body']['pre-submit']=function(&$v){$v=demosphere_post_body_cleanup($v);};
	require_once 'dlib/filter-xss.php';
	$items['body']['default-value']=filter_xss_admin(val($items['body'],'default-value',''));

	$items['status']=$objItems['status'];

	if($post->id)
	{
		$src='post/'.$post->id;
		$items['alias']=
			['type'=>'textfield',
			 'title'=>t('Alias'),
			 'default-value'=>demosphere_path_alias_dest($src)!==$src ? demosphere_path_alias_dest($src) : '' ,
			 'description'=>t('An optional pretty url for this post. If you specify "example", the url of this post will be: !base_url/example',['!base_url'=>$base_url]),
			 'validate_re'=>'@^[a-z0-9_-]+$@i',
			 'pre-submit'=>function($dest)use($post)
				{
					if($dest!=''){demosphere_path_alias_set('post/'.$post->id,$dest);}
					else
						if($post->id)
						{db_query("DELETE FROM path_alias WHERE src='%s'",'post/'.$post->id);;}
				}
			];
	}

	$items['save']=$objItems['save'];
	$items['save']['submit']=function()use($post,$base_url)
		{
			global $demosphere_config;
			// clear cache to refresh topic CSS for frontpage map link infobox
			$boxKey=dlib_first_key(preg_grep('@demosphereMapLink@',dlib_array_column($demosphere_config['front_page_infoboxes'],'class')));
			if($boxKey!==null && $post->id==$demosphere_config['front_page_infoboxes'][$boxKey]['pid'])
			{
				require_once 'demosphere-common.php';
				demosphere_cache_clear();
			}
			dlib_redirect($base_url.'/post/'.$post->id);
		};

	if(isset($objItems['delete']) &&
	   $post->id!==$demosphere_config['frontpage_messages'] &&
	   $post->id!==$demosphere_config['frontpage_announcement'] && 
	   !$isInfobox &&
	   !$isBuiltIn)
	{
		$items['delete']=$objItems['delete'];
		$items['delete']['redirect']=$base_url.'/backend/post';
	}

	return form_process($items,$formOpts);
}

function demosphere_post_body_cleanup($body)
{
	require_once 'demosphere/htmledit/demosphere-htmledit.php';
	return demosphere_htmledit_submit_cleanup($body,['extra-attributes'=>['id'],
													 'class-whitelist'=>false]);
}

//! Ajax call from side by side diff js to save an edited post
function demosphere_post_hdiff_save($id)
{
	dlib_check_form_token('hdiff');
	require_once 'demosphere/demosphere-post.php';
	$post=Post::fetch($id);
	$post->body=demosphere_post_body_cleanup($_POST['html']);
	$post->save();
	return ['ok'=>true];
}


function demosphere_post_tabs($post,$active)
{
	global $user,$base_url,$currentPage;

	// https://example.org/psot/add => no tabs
	if(!$post->id){return;}

	if(!demosphere_post_access($user,$post->id,'edit')){return;}

	// If post is viewed in dbobject-ui context, use dbobject-ui tabs instead.
	if(isset($_GET['dbobject-ui']))
	{
		require_once 'dlib/dbobject-ui.php';
		dbobject_ui_tabs('Post',$post,'post-'.$active);
		return;
	}

	$tabs=[];
	if($post->id)
	{
		$tabs['view']=['text'=>t('View'),'href'=>$base_url.'/post/'.$post->id];
		$tabs['edit']=['text'=>t('Edit'),'href'=>$base_url.'/post/'.$post->id.'/edit'];
	}
	if(isset($tabs[$active])){$tabs[$active]['active']=true;}
	$currentPage->tabs=$tabs;
}

//! Setup for class information displayed/edited by dbobject-ui
function demosphere_post_class_info(&$classInfo)
{
	$classInfo['tabs']=function(&$tabs,$post)
		{
			global $base_url;
			if($post)
			{
				$tabs['post-view']=['href'=>$base_url.'/post/'.$post->id.     '?dbobject-ui','text'=>t('View')];
				$tabs['post-edit']=['href'=>$base_url.'/post/'.$post->id.'/edit?dbobject-ui','text'=>t('Edit')];
				$tabs['view']['text']=t('Admin: view');
				$tabs['edit']['text']=t('Admin: edit');
				unset($tabs['add']);
			}
			else
			{
				$tabs['add']['href']=$base_url.'/post/add';
			}
		};
	$classInfo['delete_alter']=function(&$items,$post)
		{
			global $demosphere_config,$base_url;
			$infoboxes=dlib_array_column($demosphere_config["front_page_infoboxes"],'pid');
			if(array_search($post->id,$infoboxes)!==false ||
			   $post->id===$demosphere_config['frontpage_messages'] ||
			   $post->id===$demosphere_config['frontpage_announcement'] 
			  )
			{
				$items=['message'=>['html'=>'<h3>'.t('This post is used in a frontpage infobox or a message. You cannot delete it.').'</h3>'.
									'<p>(<a href="'.$base_url.'/configure/frontpage#front_page_infoboxes">configure infoboxes</a>)</p>'.
									'</h3>']];
			}
			if($post->builtIn!=='')
			{
				$items=['message'=>['html'=>'<h3>'.t('This is a built-in page. You may edit it, but do not delete it. If you do not want to use it, just un-publish it and remove any links to it.').'</h3>']];
			}
		};
	$classInfo['list_columns']=
		function(&$columns)
		{
			$new=['title'=>'box',
				  'show'=>function($post,&$show)
					{
						global $base_url,$demosphere_config;
						static $infoboxes=false;
						if($infoboxes===false)
						{
							$infoboxes=array_flip(dlib_array_column($demosphere_config['front_page_infoboxes'],'pid'));
						}
						$show['disp']=isset($infoboxes[$post->id]) ? '*' : '';
					},
				  ];
			$columns=dlib_array_insert_assoc($columns,'title','infobox',$new);
			$columns['title']['show']=function($post,&$show)
			{
				global $base_url;
				$show['disp']='<a href="'.$base_url.'/post/'.$post->id.'">'.$show['disp'].'</a>';
			};
		};

	// Change urls in list so that view & edit links point to post view & edit (not backend view & edit)
	$classInfo['list_action_url']=function($post,$action,$oldActionUrl)
		{
			global $base_url;
			if($action==='view'){return $base_url.'/post/'.$post->id.     '?dbobject-ui';}
			if($action==='edit'){return $base_url.'/post/'.$post->id.'/edit?dbobject-ui';}
			return $oldActionUrl($post,$action);
		};

}

?>