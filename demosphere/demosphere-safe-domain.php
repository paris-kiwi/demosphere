<?php
//! Controller for all accesses to safe domain name setup for viewing untrusted html.
function demosphere_safe_domain()
{
	global $user,$demosphere_config;
	global $base_url;
	$isLoggedIn=$user->id!=0;	
	if($isLoggedIn)
	{
		$ua=$_SERVER['HTTP_USER_AGENT'] ?? '';
		if(preg_match('@\bMSIE [0-9]+@'   ,$ua) ||
		   preg_match('@\bTrident/[0-9]+@',$ua) ||
		   preg_match('@\bEdge/(1[23456])@',$ua) )
		{
			dlib_permission_denied_403('Your Internet explorer or Edge browser is not supported. '.
									   'Please use Chrome, Firefox or another standards compliant browser. '.
									   'Dangerous access to untrusted html. Please logout of safe site.',false);			
		}
		dlib_permission_denied_403('dangerous access to untrusted html. Please logout of safe site.',false);
	}

	// check authentification
	$found_ip =$_SERVER['REMOTE_ADDR'];
	$found_url=$_SERVER['REQUEST_URI'];
	$found_url=preg_replace('@[&?]sdtoken=[a-z0-9-]*$@'  ,'',$found_url);
	$found_url=preg_replace('@[&?]sdsession=[a-z0-9-]*$@','',$found_url);
	$base_path=preg_replace('@^https?://[^/]*@','',$base_url).'/';
	if($base_path!='/'){$found_url=substr($found_url,strlen($base_path)-1);}
	//echo '$found_url:'.ent($found_url)."<br/>\n";
	
	if(!isset($_GET['sdsession'])){dlib_bad_request_400('demosphere_safe_domain: failed : no sdsession');}
	if(!isset($_GET['sdtoken'  ])){dlib_bad_request_400('demosphere_safe_domain: failed : no sdtoken');}
	$found_login=$_GET['sdsession'];
	$found_token=$_GET['sdtoken'  ];
	
	if($found_login!=='anon')
	{
		$sessions=variable_get('demosphere_safe_domain_sessions','',[]);
		$session=val($sessions,$found_login);
		if($session===false){dlib_permission_denied_403('demosphere_safe_domain: failed',false);}
		$good_token=hash('sha256',$session['passwd'].':'.$found_ip.':'.$found_url);
	}
	else
	{
		$good_token=hash('sha256',$demosphere_config['secret'].':'.$found_url);
	}

	if($found_token!==$good_token){dlib_permission_denied_403('Access denied to safe_domain. This should not happen. Please check your safe_base_url settings. This is needed to view unsafe html. Otherwise report a bug.',false);}

	// *** exec this request through standard url/path system
	$path=substr($found_url,strlen('/safe-domain/'));
	$path=preg_replace('@\?.*$@','',$path);
	demosphere_exec_request($path);
}

/** Construct a URL by adding the base safe domain url and computing sdtoken.
 * sdtoken is made of a login and a hash of a password and other information.
 * @param the path we want to link to 'xyz/abc' (if url is https://example.org/xyz/abc)
 * @param force the login instead of using the session stored login (This is only necessary for offline generated pages, like mail-import html image links).
 */
function demosphere_safe_domain_url($url,$anon=false)
{
	global $demosphere_config,$base_url;

	$args='/safe-domain/'.$url;
	if($anon===false)
	{
		$session=demosphere_get_safe_domain_session();
		if($session===false){dlib_permission_denied_403('demosphere_safe_domain_url: could not get/create session. Maybe you are calling this function from safe domain?',false);}
		$login=$session['login'];
		$sdtoken=hash('sha256',$session['passwd'].':'.$session['ip'].':'.$args);
	}
	else
	{
		$login='anon';
		$sdtoken=hash('sha256',$demosphere_config['secret'].':'.$args);
	}
	$res=$demosphere_config['safe_base_url'].$args.
		(strpos($args,'?')===false ? '?' : '&').
		'sdsession='.$login.'&sdtoken='.$sdtoken;
	return $res;
}

/** 
 * setup different domain name for viewing untrusted html.
 */
function demosphere_get_safe_domain_session()
{
	static $cached=false;
	if($cached!==false){return $cached;}
	global $user,$demosphere_config;
	$isLoggedIn=$user->id!=0;
	if(!$isLoggedIn){return false;}

	$login=val($_SESSION,'safe_domain_login');
	$sessions=variable_get('demosphere_safe_domain_sessions','',[]);

	// cleanup old, expired sessions
	$expireTime=time()-3600*24;
	foreach(array_keys($sessions) as $slogin)
	{
		if($sessions[$slogin]['created']<$expireTime)
		{
			unset($sessions[$slogin]);
			variable_set('demosphere_safe_domain_sessions','',$sessions);
		}
	}
	
	// Check if current session is valid.
	// Also check if user's IP has changed. 
	if($login!==false && 
	   ( !isset($sessions[$login]) ||
		 $sessions[$login]['ip']!==$_SERVER['REMOTE_ADDR']))
	{
		unset($_SESSION['safe_domain_login']);
		$login=false;
	}

	// create a new session if there is no valid session
	if($login===false)
	{
		$login=mt_rand();
		$current=['login'   =>$login,
				  'created'=>time(),
				  'passwd' =>mt_rand(),
				  'ip'	 =>$_SERVER['REMOTE_ADDR']];
		$_SESSION['safe_domain_login']=$login;
		$sessions[$login]=$current;
		variable_set('demosphere_safe_domain_sessions','',$sessions);
	}
	else
	{
		$current=$sessions[$login];
	}

	$cached=$current;
	return $current;
}

//! Are we actually on safe domain or not
function demosphere_safe_domain_check()
{
	return strpos($_GET['q'],'safe-domain/')===0 && 
		$GLOBALS['base_url']===$GLOBALS['demosphere_config']['safe_base_url'];
}


//! Downloads a remote file and serves it.
//! This is different from demosphere_image_proxy() : 
//! files are not not limited to images, they are not stored, and are not served directly by apache.
//! Example:
//! http://safe.example.demosphere.net/safe-domain-proxy/12345789abcd-123456890
function demosphere_safe_domain_proxy_view($proxyFname)
{
	global $base_url,$demosphere_config,$user;
	if($base_url!==$demosphere_config['safe_base_url']){dlib_permission_denied_403('proxy not on safe domain',false);}
	if($user->id!=0){dlib_permission_denied_403('dangerous access to safe domain. Please logout of safe site.',false);}

	$tmpFname=tempnam($demosphere_config['tmp_dir'],"safe-domain-proxy-");
	$remoteUrl=demosphere_safe_domain_proxy_remote_url($proxyFname);
	if($remoteUrl===false){dlib_permission_denied_403(false,false);}

	$log='';
	$headers=[];
	require_once 'dlib/download-tools.php';
	$ok=dlib_curl_download($remoteUrl,$tmpFname,['headers'=>&$headers,'info'=>['log'=>&$log]]);
	if($ok===false)
	{
		require_once 'dlib/filter-xss.php';
		header("HTTP/1.0 502 Bad Gateway");
		header('Content-Type: text/html');
		echo 'Proxy download failed for :<br/><a href="'.filter_xss_check_url($remoteUrl).'">'.
			filter_xss_check_url($remoteUrl).'</a><br/>'."\n".
			'<pre>'.ent($log).'</pre>';
		return;
	}

	// Optionally fix links inside CSS file so that they also use this proxy.
	if(isset($_GET['fix-css-urls']) && preg_match('@^text/css(;.*)?$@',$headers['content-type'] ?? ''))
	{
		$css=file_get_contents($tmpFname);
		$css=preg_replace_callback('@\burl\(\s*[\'"]?([^\)"\']*)[\'"]?\s*\)@',function($m)use($remoteUrl)
								   {
									   $url=$m[1];
									   if($url===''                     ){return $m[0];}
									   if(preg_match('@^https://@',$url)){return $m[0];}
									   if(preg_match('@^//@'      ,$url)){return 'url(https:'.$url.')';}
									   if(!preg_match('@http://@',$url))
									   {
										   //var_dump($m[0],$url);
										   $remoteBase=preg_replace('@/[^/]*$@','',$remoteUrl);
										   $remoteTop=preg_replace('@^(https?://[^/]+)(/.*)?$@','$1',$remoteUrl);
										   
										   if($url[0]==='/'){$url=$remoteTop     .$url;}
										   else             {$url=$remoteBase.'/'.$url;}
									   }
									   return 'url('.demosphere_safe_domain_proxy_create_remote_url($url).')';
								   },$css);
		file_put_contents($tmpFname,$css);
	}
	
	header('Content-Type: '.preg_replace('@\s@',' ',$headers['content-type'] ?? 'text/html'));
	readfile($tmpFname);
	unlink($tmpFname);
}

//! Extracts the remote url that is encoded in the proxy file name.
//! Also checks that the hash in the proxy file name is valid.
//! Example:
//! $proxyFname: cb28292928b6d2d74fad48cc2dc849d5cb2f4ad74f4c4ad62bc84b0700-63719a4cba.png 
//! return value: https://example.org/abc.png
function demosphere_safe_domain_proxy_remote_url($proxyFname)
{
	global $demosphere_config;
	if(!preg_match('@^(.*/)?([0-9a-f]+)-([0-9a-f]+)$@',$proxyFname,$m)){dlib_bad_request_400('demosphere_safe_domain_proxy_remote_url: invalid filename');}
	$encUrl=$m[2];
	$foundSecHash=$m[3];
	$goodSecHash=substr(hash('sha256',$encUrl.':'.$demosphere_config['secret']),0,10);
	if($foundSecHash!==$goodSecHash){return false;}
	$res=gzinflate(hex2bin($encUrl));
	return $res;
}

//! Creates a proxied url for a $remoteUrl
//! Example: 
//! $remoteUrl: https://example.org/abc.png
//! return value: http://safe.example.demosphere.net/safe-domain-proxy/cb28292928b6d2d74fad48cc2dc849d5cb2f4ad74f4c4ad62bc84b0700-63719a4cba.png
function demosphere_safe_domain_proxy_create_remote_url($remoteUrl)
{
	global $demosphere_config;

	$encUrl=bin2hex(gzdeflate($remoteUrl));
	// $secHash ensures that others cannot create new urls that use our proxy 
	$secHash=substr(hash('sha256',$encUrl.':'.$demosphere_config['secret']),0,10);
	return $demosphere_config['safe_base_url'].'/safe-domain-proxy/'.$encUrl.'-'.$secHash;
}

?>