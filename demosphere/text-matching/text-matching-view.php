<?php
/** \addtogroup text_matching 
 * @{ */
require_once 'dlib/template.php';

function text_matching_view($tmDocId=false,$isItemList=true)
{
	global $text_matching_config,$base_url,$currentPage;
	require_once 'dcomponent/dcomponent-common.php';	

	$menus=[];
	$menus[]=[['url'=>'text-matching/control-panel'       ,'title'=>t('panel'),],
			  ['url'=>'text-matching/stats'               ,'title'=>t('stats')],
			  ['url'=>'text-matching/examine'             ,'title'=>t('examine'),],
			 ];

	dcomponent_page_css_and_js($isItemList);
	$currentPage->addCssTpl('demosphere/text-matching/text-matching.tpl.css');
	text_matching_view_title($tmDocId);
	$currentPage->dcomponentMenubar=
		template_render('dcomponent/dcomponent-menubar.tpl.php',
						['dcMenus'=>$menus]);
	return ['scriptUrl'=>$base_url.'/text-matching'];
}

function text_matching_view_title($tmDocId)
{
	global $currentPage;
	if(oval($currentPage,'title')!==false){return;}
	$title='tm-';
	if(preg_match('@text-matching(/(FIXME)/\d+)?/([^/?&]+)@',$_GET['q'],$matches))
	{
		$title.=$matches[3];
	}
	if($tmDocId!==false){$title.=': '.$tmDocId;}
	if($title==='tm-'){$title=t('text-matching');}
	$currentPage->title=$title;
}

function text_matching_view_control_panel()
{
	global $text_matching_config;
	return template_render('text-matching-control-panel.tpl.php',
						   [text_matching_view(),
							'text_matching_config'=>$text_matching_config,
						   ]);

}

function text_matching_view_stats()
{
	global $currentPage;

	$totKp=db_result("SELECT COUNT(keyphrase) FROM tm_keyphrase_doc");
	$ditinctKP=db_result("SELECT COUNT(DISTINCT keyphrase) FROM tm_keyphrase_doc");
	$totDocs=db_result("SELECT COUNT(id) FROM TMDocument ");

	$avgKpPerDoc=db_result("SELECT AVG(c) FROM (SELECT COUNT(keyphrase) AS c FROM tm_keyphrase_doc GROUP BY document_id) Cts");
	$devKpPerDoc=db_result("SELECT STDDEV_POP(c) FROM (SELECT COUNT(keyphrase) AS c FROM tm_keyphrase_doc GROUP BY document_id) Cts");

	$sizes=db_one_col('SELECT document_id,COUNT(*) FROM tm_keyphrase_doc GROUP BY document_id ORDER BY COUNT(*)');
	//my_print_r($sizes);
	$nbbins=20;
	$t=array_values($sizes);sort($t);
	$min=$t[0];
	$t=array_values($sizes);rsort($t);
	$max=$t[0];
	$ct=0;
	$bin=1;
	$totDocs=count($sizes);
	$prevBinBoundary=$min;

	$bins=[];
	$totCumul=0;
	foreach($sizes as $doc=>$nbkp)
	{
		$totCumul+=$nbkp;
		$bin=floor($nbbins*($nbkp-$min)/($max-$min));
		$bin=min($bin,$nbbins-1);
		$bin=max($bin,0);
		if(!isset($bins[$bin])){$bins[$bin]=[];}
		$bins[$bin]['ct' ]=val($bins[$bin],'ct' ,0)+1;
		$bins[$bin]['tot']=val($bins[$bin],'tot',0)+$nbkp;
	}

	$cumul=0;
	$statsTable=[];
	for($bin=0;$bin<$nbbins;$bin++)
	{
		$binStart=$min+$bin	   *($max-$min)/$nbbins;
		$binEnd	 =$min+($bin+1)*($max-$min)/$nbbins;
		if(!isset($bins[$bin])){$bins[$bin]=[];}
		$ct= val($bins[$bin],'ct' ,0);
		$tot=val($bins[$bin],'tot',0);
		$cumul+=$tot;
		$l=round(2000*$ct/$totDocs);
		$line=[];

		$line['bin']=$bin;
		$line['binStart']=$binStart;
		$line['binEnd']=$binEnd;
		$line['ct']=$ct;
		$line['tot']=$tot;
		$line['cumul']=$cumul;
		$line['totCumul']=$totCumul;
		$line['l']=$l;
		$statsTable[]=$line;
	}

	return template_render('text-matching-stats.tpl.php',
						   [text_matching_view(),
							compact('totKp','ditinctKP','totDocs','avgKpPerDoc','devKpPerDoc','min','max','statsTable'),
						   ]);
}

function text_matching_view_similar_docs($refDoc)
{
	global $currentPage;
	$currentPage->addJs('lib/jquery.js');
	require_once 'dcomponent/dcomponent-common.php';	
	dcomponent_page_css_and_js(true);
	$currentPage->addCssTpl('demosphere/text-matching/text-matching.tpl.css');

	$nbRefKp=db_result('SELECT COUNT(*) FROM tm_keyphrase_doc WHERE document_id=%d',$refDoc->id);

	$nbMatches=$refDoc->findKPSimilarDocuments();
	arsort($nbMatches);
	$similarDocs=TMDocument::fetchListFromIds(array_keys($nbMatches));

	global $text_matching_config;
	$currentPage->addCssTpl('demosphere/text-matching/text-matching-info.tpl.css');

	// add js and css headers for each doctype
	foreach($text_matching_config['doctypes'] as $doctype=>$u)
	{
		text_matching_call_hook($doctype,'displayListInit');
	}

	$refTimestamps=false;
	if($refDoc->type==='event')
	{
		$event=Event::fetch($refDoc->tid,false);
		if($event!==null){$refTimestamps=[$event->startTime];}
	}

	return template_render('text-matching-similar-docs.tpl.php',
						   [
							   compact('refDoc','similarDocs','nbMatches','nbRefKp','refTimestamps'),
						   ]);

}

//! Display side by side differences between two TMDocuments
//! FIXME: think about moving all doctype (mail-import,feed-import, event ...) 
//! specific code should be moved to text matching hook for modularity (or some other solution)
function text_matching_view_hdiff($tmDoc1,$tmDoc2)
{
	global $text_matching_config,$currentPage,$base_url;
	//$currentPage->title='diff: '.$tmDoc1->tid.'/'.$tmDoc2->tid;

	text_matching_view();

	// **** top right keyphrase link 
	$kpLink='';
	$kp1=$tmDoc1->getStoredKeyphrases();
	$kp2=$tmDoc2->getStoredKeyphrases();
	$commonKp=array_intersect($kp1,$kp2);
	$accuracy1=count($kp1) ? (round(100*count($commonKp)/count($kp1))).'%' : '--';
	$accuracy2=count($kp2) ? (round(100*count($commonKp)/count($kp2))).'%' : '--';
	$kpLink.='<a id="keyphrase-diff-link" href="'.
		ent($base_url.'/text-matching/keyphrase-diff?tmDoc1='.$tmDoc1->id.'&tmDoc2='.$tmDoc2->id).'">'.$accuracy1.' ; '.$accuracy2.'</a>';

	// ** For event / event diff, use specialized functionality (diff of title, date, address and body)
	if($tmDoc1->type==='event' && 
	   $tmDoc2->type==='event' )
	{
		require_once 'demosphere/demosphere-event-revisions.php';
		return $kpLink.demosphere_event_hdiff(Event::fetch($tmDoc1->tid),
											  Event::fetch($tmDoc2->tid));
	}

	$options=[];
	require_once 'dlib/html-tools.php';

	$diffCols=[];
	for($colNb=0;$colNb<2;$colNb++)
	{
		$tmDoc=($colNb==0 ? $tmDoc1 : $tmDoc2);
		// get the html contents of both documents
		list($html,$ct)=text_matching_call_hook($tmDoc->type,'getContents',$tmDoc->tid);
		// FIXME!!!: security XSS needs complete audit/rewrite
		// cleanup and extract the body of both documents
		$html=dlib_html_cleanup_page_with_tidy($html,false);
		$headPos=strpos($html,'<body');
		$head=substr($html,0,$headPos);
		$body=substr($html,$headPos);
		// FIXME (duplicate func dlib_html_cleanup_page_with_tidy)
		$body=dlib_html_cleanup_tidy_xsl($body);
		
		$xinfo=text_matching_call_hook($tmDoc->type,'matchInfo',$tmDoc->tid);
		$url=val($xinfo,'url','badurl');
		$url=str_replace($base_url,$text_matching_config['std_base_url'],$url);


		// disable img tracking in emails (mail_import)
		if($tmDoc->type==='mail_import')
		{
			$domDoc=dlib_dom_from_html($html,$xpath);
			foreach($xpath->query("//@src") as $attr)
			{
				$attr->value=ent($base_url.'/demosphere/css/images/broken-image.png');
			}
			$body=dlib_dom_to_html($domDoc);
		}

		// Add info so that Firefox webextension can add Source: when copying from these.
		$colAttr=[];
		if($tmDoc->type==='feed_import')
		{
			$colAttr['data-demosphere-src']='article-'.$tmDoc->tid;
		}
		if($tmDoc->type==='mail_import')
		{
			$colAttr['data-demosphere-src']='message-'.$tmDoc->tid;
		}

		$diffCols[$colNb]=
			['html'=>$body,
			 'title'=>'<a href="'.ent($url).'"><span class="tmdoc-tid">'.ent('#'.$tmDoc->tid.': ').'</span>'.ent($xinfo['title']).'</a>',
			 'class'=>'tm_'.$tmDoc->type,
			 'attributes'=>$colAttr,
			 ];

		if($tmDoc->type==='event')
		{
			$currentPage->addCssTpl('demosphere/css/demosphere-event-hdiff.tpl.css');
			require_once 'demosphere/demosphere-date-time.php';
			$event=Event::fetch($tmDoc->tid);
			$diffCols[$colNb]['title']='<a href="'.ent($event->url()).'"><span class="tmdoc-tid">'.
				ent('#'.$tmDoc->tid.': ').'</span>'.ent($event->title).'</a>';
			$diffCols[$colNb]['class'].=' event-hdiff-part event-hdiff-body event-id-'.$event->id;
			$diffCols[$colNb]['save']=$base_url.'/event-hdiff-save/'.$tmDoc->tid.'?part=body&changeNumber='.$event->changeNumber;
			$diffCols[$colNb]['info']='<div class="hdiff-event-info clearfix"><div class="hdiff-event-date"><span>'.t('Date:').'</span> '.
				ent(demos_format_date('full-date-time',$event->startTime)).'</div>'.
				'<div class="hdiff-event-place">'.
				'<span>'.t('Place:').'</span>'.
				$event->usePlace()->htmlAddress().'</div></div>';
		}

	}

	// Add demosphere-htmledit support for tinymce, only if editing is actually needed
	$token='';
	if(isset($diffCols[0]['save']) || 
	   isset($diffCols[1]['save']) 	   )
	{
		require_once 'htmledit/demosphere-htmledit.php';
		$token=demosphere_htmledit_hdiff($options);
	}

	$currentPage->addCssTpl('demosphere/text-matching/text-matching.tpl.css');

	require_once 'dlib/hdiff.php';
	$hdiff=hdiff($diffCols,$options);
	$hdiff.=$token;

	return $kpLink.$hdiff;
}

//! View differences in keyphrases between two documents.
function text_matching_view_keyphrase_diff($tmDoc1,$tmDoc2)
{
	global $text_matching_config,$currentPage,$base_url;

	$tmv=text_matching_view();

	// ***************************************** keyphrases
	$wordlist1=$tmDoc1->getWordList($wlInfo1);
	$wordlist2=$tmDoc2->getWordList($wlInfo2);

	$keyphrases1=TMDocument::makeKeyphrases($wordlist1,true);
	$keyphrases2=TMDocument::makeKeyphrases($wordlist2,true);
	$kp1pos=[];
	foreach($keyphrases1 as $kp=>$info){$kp1pos[$info['wordnum']]=$kp;}
	$kp2pos=[];
	foreach($keyphrases2 as $kp=>$info){$kp2pos[$info['wordnum']]=$kp;}

	//vd($keyphrases1,$keyphrases2);

	$commonKeyphrases=[];
	foreach($keyphrases1 as $kp=>$info)
	{
		if(isset($keyphrases2[$kp])){$commonKeyphrases[]=$kp;}
	}

	$keyphrasesWithContext=[];
	for($dir=1;$dir<=2;$dir++)
	{
		$out='';
		
		$dr2=$dir==1?2:1;
		$t='wordlist'.$dir;	 $wordlistSrc  =$$t; 
		$t='wordlist'.$dr2;	 $wordlistRef  =$$t; 
		$t='wlInfo'.$dir;	 $wlInfoSrc    =$$t; 
		$t='wlInfo'.$dr2;	 $wlInfoRef    =$$t; 
		$t='kp'.$dir.'pos';	 $kpSrcPos	   =$$t; 
		$t='kp'.$dr2.'pos';	 $kpRefPos	   =$$t; 
		$t='keyphrases'.$dir;$keyphrasesSrc=$$t; 
		$t='keyphrases'.$dr2;$keyphrasesRef=$$t; 
		
		$end=false;
		$wnum=0;
		foreach($wlInfoSrc['allWords'] as $wnum0=>$word)
		{
			if(!isset($wlInfoSrc['words'][$wnum0]))
			{
				$out.='<span style="color: #888;">'.$word.'</span> ';
				continue;
			}

			if(isset($kpSrcPos[$wnum]))
			{
				$kp=$kpSrcPos[$wnum];
				if(isset($keyphrasesRef[$kp]))
				{$end=$wnum+$keyphrasesSrc[$kp]['length'];}
			}
			if($wnum<$end)
			{
				$out.='<span style="">'.$word.'</span>';
			}
			else
			{
				$out.='<span style="color: red;">'.$word.'</span>';
			}
			$out.=' ';
			$wnum++;
		}		
		$keyphrasesWithContext[]=$out;
	}
	return template_render('text-matching-keyphrase-diff.tpl.php',
						   [$tmv,
							compact('hdiff','tmDoc1','tmDoc2','keyphrases1','keyphrases2','commonKeyphrases','keyphrasesWithContext'),
						   ]);
}

//! Only for debugging
function text_matching_view_keyphrase($kpHash)
{
	global $base_url;
	require_once 'demosphere-date-time.php';
	$kpDescs=db_arrays('SELECT * FROM tm_keyphrase_doc INNER JOIN TMDocument ON tm_keyphrase_doc.document_id=TMDocument.id WHERE keyphrase=%d ORDER BY document_id ASC',$kpHash);
	$out='';
	$out.='<h2>'.ent($kpHash).' : '.ent(TMDocument::keyphraseToWordList($kpHash)).'</h2>';
	$out.='<table>';
	foreach($kpDescs as $kpDesc)
	{
		$message=null;
		if($kpDesc['type']==='mail_import'){$message=Message::fetch($kpDesc['tid'],false);}
		$out.='<tr>';
		$out.='<td>'.$kpDesc['document_id'].'</td>';
		$out.='<td>'.$kpDesc['type'].'</td>';
		if($message!==null)
		{
			$out.='<td><a href="'.ent($message->url()).'">'.$message->id.'</a></td>';
			$out.='<td><a href="'.$base_url.'/mail-import?listId='.urlencode($message->listId).'">'.ent($message->listId).'</a></td>';
			$out.='<td><a href="'.$base_url.'/mail-import?threadNb='.$message->threadNb.'">'.ent($message->threadNb).'</a></td>';
			$out.='<td>'.ent(demos_format_date('short-date-time',$message->dateFetched)).'</td>';
		}
		else
		{
			$out.='<td>'.$kpDesc['tid'].'</td>';
			$out.='<td>'.''.'</td>';
			$out.='<td>'.''.'</td>';
			$out.='<td>'.''.'</td>';
		}


		$out.='</tr>';
	}
	return $out;
}

/** @} */

?>