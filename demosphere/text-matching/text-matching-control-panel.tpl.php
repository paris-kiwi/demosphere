<h1 class="page-title">Text-matching control panel</h1>
<p></p><a href="$scriptUrl/reindex">reindex all</a></p>
<ul>
	<?foreach($text_matching_config['doctypes'] as $doctype=>$u){?>
		<li>
			<a href="$scriptUrl/reindex&doctype=$doctype">reindex $doctype</a>
		</li>
	<?}?>
</ul>
<p></p><a href="$scriptUrl/suggest-bad-words">suggest-bad-words (can be long)</a></p>
<p>doctypes:<?= implode(", ",array_keys($text_matching_config['doctypes'])) ?></p>
<p>badwords:<?= implode(", ",           $text_matching_config['bad_words'] ) ?></p>
