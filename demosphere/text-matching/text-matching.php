<?php

/** @defgroup text_matching Text Matching
 *
 * Find similar texts in different document types (events, emails, feed articles).
 * This must be done very quickly as ordinary string matching across thousands
 * of documents is impossible. Fixed sized keyphrases (lists of words) are extracted from
 * each document and are indexed in a database table (tm_keyphrase_doc) using hashes. 
 * 
 * When we get a new document that needs to be matched against existing documents,
 * its keyphrases are extracted and the resulting hashes searched for in the database.
 * 
 * For this to work, keyphrases must be extracted in a reproductible manner across
 * different documents. That is ensured by randomly
 * but reproductibly determining which words begin keyphrases.
 *
 *  @{
 */

/**
 * @file
 */


//! $text_matching_config : configuration for text_matching.
//! List of $text_matching_config keys:
//! - doctypes
//! - includes
//! - std_base_url
//! - bad_words
//! -::-;-::-
$text_matching_config;


function text_matching_add_paths(&$paths,&$options)
{
	$paths[]=['path'=>'@^text-matching(/control-panel)?$@',
			  'roles' => ['admin','moderator'],
			  'file'=>'text-matching/text-matching-view.php',
			  'function'=>'text_matching_view_control_panel',
			 ];

	$paths[]=['path'=>'text-matching/reindex',
			  'roles' => ['admin','moderator'],
			  'function'=>function()
		{
			global $base_url;
			require_once 'dcomponent/dcomponent-common.php';
			dcomponent_progress_log_start();
			text_matching_reindex(val($_GET,'doctype',false));
			dlib_message_add('reindexed ok');
			dcomponent_progress_log_end(['redirect'=>$base_url.'/text-matching/control-panel']);
		}
			 ];

	$paths[]=['path'=>'text-matching/stats',
			  'roles' => ['admin','moderator'],
			  'file'=>'text-matching/text-matching-view.php',
			  'function'=>'text_matching_view_stats',
			 ];
	$paths[]=['path'=>'text-matching/suggest-bad-words',
			  'roles' => ['admin','moderator'],
			  'file'=>'text-matching/text-matching-view.php',
			  'function'=>function()
		{
			text_matching_view(false,false);
			return ent(print_r(TMDocument::suggestBadWords(),true));
		}
			 ];

	$paths[]=['path'=>'@^text-matching/tmdocument/(\d+)/similar$@',
			  'roles' => ['admin','moderator'],
			  'file'=>'text-matching/text-matching-view.php',
			  'function'=>function($id)
		{
			$tmDoc=TMDocument::fetch($id,false);
			if($tmDoc===null){dlib_not_found_404();}
			return text_matching_view_similar_docs($tmDoc);
		},
			 ];

	$paths[]=['path'=>'text-matching/hdiff',
			  'roles' => ['admin','moderator'],
			  'file'=>'text-matching/text-matching-view.php',
			  'function'=>function()
		{
			$tmDoc1=TMDocument::fetch(val($_GET,'tmDoc1',0),false);
			$tmDoc2=TMDocument::fetch(val($_GET,'tmDoc2',0),false);
			if($tmDoc1===null || $tmDoc2===null){dlib_not_found_404();}
			return text_matching_view_hdiff($tmDoc1,$tmDoc2);
		},
			 ];

	$paths[]=['path'=>'text-matching/keyphrase-diff',
			  'roles' => ['admin','moderator'],
			  'file'=>'text-matching/text-matching-view.php',
			  'function'=>function()
		{
			$tmDoc1=TMDocument::fetch(val($_GET,'tmDoc1',0),false);
			$tmDoc2=TMDocument::fetch(val($_GET,'tmDoc2',0),false);
			if($tmDoc1===null || $tmDoc2===null){dlib_not_found_404();}
			return text_matching_view_keyphrase_diff($tmDoc1,$tmDoc2);
		},
			 ];

	$paths[]=['path'=>'@^text-matching/kp/([0-9]+)$@',
			  'roles' => ['admin'],
			  'file'=>'text-matching/text-matching-view.php',
			  'function'=>'text_matching_view_keyphrase',
			 ];

}

function text_matching_cron()
{
	ini_set('max_execution_time', max(600,ini_get('max_execution_time')));
	TMDocument::cleanupExpired();
	text_matching_update_ignored_keyphrases();
}

function text_matching_reindex($onlyDoctype=false)
{
	global $text_matching_config;
	ini_set('max_execution_time', max(600,ini_get('max_execution_time')));
	foreach($text_matching_config['doctypes'] as $doctype=>$u)
	{
		if($onlyDoctype!==false && $onlyDoctype!=$doctype){continue;}
		//echo "reindexing $doctype<br/>\n";
		db_query("DELETE FROM tm_keyphrase_doc ".
				   "USING tm_keyphrase_doc,TMDocument ".
				   "WHERE tm_keyphrase_doc.document_id=TMDocument.id ".
				   "AND TMDocument.type='".$doctype."'");
		db_query("DELETE FROM TMDocument ".
				   "WHERE TMDocument.type='".$doctype."'");
		$tids=text_matching_call_hook($doctype,'allTids');
		// Reindex all ids. 
		$time_start = microtime(true);
		foreach($tids as $tid)
		{
			//echo "indexing event ".$id."<br/>\n";
			TMDocument::reindexTid($doctype,$tid);
		}
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		dlib_message_add("Indexed ".count($tids)." ".$doctype."s in ".$time." seconds: ".($time!=0 ? count($tids)/$time : '-inf-')." docs/sec");
	}
}


//! Uses heuristics to identify keyphrases that should be ignored.
//! Typically: keyphrases that appear in email signatures or other recurring keyphrases that are not significant to user.
//! called from cron
function text_matching_update_ignored_keyphrases()
{

	// ************* mail_import

	// Ideas for future work:
	// Remove per mailing-list signatures
	// Remove personal signatures (look for keyphrases common to single FROM), 
	// The following does a very good job, so new work might not be needed

	// ** Remove all keyphrases that appear in 5 or more different 48 hour intervals.
	// Note that text_matching keeps emails for only 14 days. 5 48 hours intervals span most of the 14 days.
	$keyphraseDescs=db_arrays("SELECT keyphrase, COUNT(*) as count, COUNT(DISTINCT (Message.dateFetched DIV (3600*24*2))) AS Ct48hours  FROM ".
							  "tm_keyphrase_doc ".
							  "INNER JOIN TMDocument ON tm_keyphrase_doc.document_id=TMDocument.id ".
							  "INNER JOIN Message ON Message.id=TMDocument.tid AND TMDocument.type='mail_import' ".
							  "GROUP BY keyphrase ORDER BY Ct48hours DESC, COUNT(*) DESC");
	//text_matching_debug_query_results($keyphraseDescs);
	$ignore=dlib_array_column(array_filter($keyphraseDescs,function($d){return $d['Ct48hours']>4;}),'keyphrase');
	dlib_message_add('removing '.count($ignore)." keyphrases");
	TMDocument::addIgnoredKeyphrases($ignore);
}

function text_matching_debug_query_results($keyphraseDescs)
{
	global $base_url;
	$ct=0;
	foreach($keyphraseDescs as $keyphraseDesc)
	{
		foreach($keyphraseDesc as $k=>$v)
		{
			if($k==='keyphrase'){echo $base_url.'/text-matching/kp/'.$v.'   ';}
			else
			{echo "$k=$v   ";}
		}
		$wordList=TMDocument::keyphraseToWordList($keyphraseDesc['keyphrase']);
		echo ($wordList===false ? '(wordlist not found)' : $wordList)."\n";
		if($ct++>80){break;}
	}
}

function text_matching_debug_show_ignored()
{
	$ignored=TMDocument::getIgnoredKeyphrases();
	foreach(array_keys($ignored) as $keyphrase){$ignored[$keyphrase]=['wordList'=>'(wordList not found)','ct'=>0];}

	// Reindex all docs
	$docIds=db_one_col('SELECT id FROM TMDocument');
	foreach($docIds as $docId)
	{
		$doc=TMDocument::fetch($docId);
		$tmdocKeyphrases=TMDocument::makeKeyphrases($doc->getWordList(),true,false);
		foreach($tmdocKeyphrases as $words=>$tmdocKeyphrase)
		{
			$kp=$tmdocKeyphrase['hash'];
			if(isset($ignored[$kp]))
			{
				$ignored[$kp]['wordList']=$words;
				$ignored[$kp]['ct']++;
			}
		}
	}

	
	foreach($ignored as $keyphrase=>$info)
	{
		echo $keyphrase.'  :  '.$info['ct'].'  :  '.$info['wordList']."\n";
	}
}

/*
 compare 1 doc to around 10.000 others (around 100-800 words/doc)
 compare time must be less than 1/3 second
 
 full= 4 million words 

 => look for full sentences


 ****** alogrithm:
 ** indexing:
 extract keyphrases  from refdoc.
 add keyphrases to indexes
   
 ** searching:
 STEP 1: coarse matching
 extract keyphrases  from doc.
 look for keyphrases in indexes

 GOALS : 
 (G1) few false positives: STEP 1 should return few documents 
 (G2) few false negatives: STEP 1 must return all matching documents

 KEYPHRASE HEURISTICS:

 1 - Detect sentence begining and choose n words. Make n big for G1, n small for G2.
 Problem: reliably identify sentence beginings.
 Problem: docs with few sentences (large sentences)
 => probably unreliable

 2 - Choose keyphrase start words using arbitrary criterion (ex: first letter ascii value is in a given range).
 advantage: can modulate number of keyphrases

 parameters: number of keyphrases, keyphrase length (n)
 Nb keyphrases:
 (NK+) : many keyphrases: good for G2, but biger index size (not big problem?)
 (NK-) : few keyphrases : G2! specially for short documents
 remark: NK probably doesn't impact G1 too much, n does. (unsure)

 try many:
 feeling: $kpFrequency=8 $kpLength=8




 Evaluation procedure:
 - evaluate keyphrase extraction heuristics, by automatic tests on large realworld DB of documents. 
 Problem: large DB needs to be manually labeled. 



 STEP 2: finer matching
 evaluate overlap between doc and refdoc


 Indexes:

 keyphrase ID -> doc ID


 keyphrase ID  -> keyphrase

 (what representation for keyphrase? : big string ?)

 keyphrase MD5 -> keyphrase ID [only usefull if keyphrase rep is not indexable by mysql]


 Other data structures:
 Doc:
 Id, type, 
*/

global $text_match_keyphrase_debug;
$text_match_keyphrase_debug=false;

function text_matching_install()
{
	require_once 'TMDocument.php';
	TMDocument::createTable();

	db_query("DROP TABLE IF EXISTS tm_keyphrase_doc");
	db_query("CREATE TABLE tm_keyphrase_doc (".
			   "`keyphrase` BIGINT UNSIGNED NOT NULL default 0,".
			   "`document_id`  int(11) NOT NULL,".
			   "PRIMARY KEY  (`keyphrase`,`document_id`)".
			   ",INDEX  (`keyphrase`)".
			   ",INDEX  (`document_id`)".
			   ") DEFAULT CHARSET=utf8mb4");
}


// doctype can be 'general', hook for a non doctype specific hook
function text_matching_call_hook( /*doctype,hookName,arg1,arg2,arg3...*/ )
{
	global $text_matching_config;
	$args=func_get_args();
	$doctype=array_shift($args);
	if(isset($text_matching_config['includes'][$doctype]))
	{
		require_once $text_matching_config['includes'][$doctype];
	}
	return call_user_func_array($text_matching_config['doctypes'][$doctype],$args);
}
/** @} */

?>