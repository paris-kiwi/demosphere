<?php

function demosphere_event_repetition_form($id)
{
	require_once 'demosphere-event-multiple-edit.php';
	require_once 'demosphere-date-time.php';
	global $base_url,$demosphere_config,$currentPage,$user;
	$currentPage->addJs('lib/jquery.js');
	$currentPage->addCssTpl('demosphere/css/demosphere-event-repetition.tpl.css');
	$currentPage->addJs('demosphere/js/demosphere-event-repetition.js');

	$currentPage->title=t('Repetition manager');

	$currentEvent=Event::fetch($id,false);
	if($currentEvent===null){dlib_not_found_404();}

	// Tabs : (view, edit, repetitions...)
	require_once 'demosphere-event.php';
	demosphere_event_tabs($currentEvent,'repetition');

	if($currentEvent->getModerationStatus()==='trash')
	{
		return '<p>'.ent(t('This event is in the trash bin, it cannot have repetitions.')).'</p>';
	}

	$group=$currentEvent->useRepetitionGroup();
	if(!$group->referenceId){$group->referenceId=$currentEvent->id;}

	if($group->id!=0){$form['repetition-group-display'] = ['html'=>'<div id="group-id">'.t('Group !nb',['!nb'=>$group->id]).'</div>'];}
	$form['repetition-list-title'] = ['html'=>'<h2>'.t('Repetition list').'</h2>'];
	$form['current-eid']=['type'=>'hidden',
						  'value'=>$currentEvent->id];

	// ******** Show events sorted by date

	// firefox incorrectly restores form elements after reload. This forces reset.
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	$vars=demosphere_event_multiple_edit_setup(false);
	extract($vars);

	// We can't fetch events directly: we need to merge with timestamps of uncreated automatic repetitions.
	// We also need to slice for pager.

	// list of all event timestamps
	if(!$group->id){$eventTs=[$currentEvent->id=>$currentEvent->startTime];}
	else
	{
		$eventTs=db_one_col('SELECT id,startTime FROM Event WHERE '.
							'repetitionGroupId=%d '.
							'ORDER BY startTime ASC, id ASC ',
							$group->id);
	}

	// timestamps of uncreated events for auto repetitions
	if($group->ruleType!==0)
	{
		$notYetCreated=$group->datesNotYetCreated();
		$notYetCreated=array_combine(preg_replace('@^@','t',array_keys($notYetCreated)),$notYetCreated);
	}
	else
	{
		$notYetCreated=[];
	}

	//var_dump(array_map(function($v){return date("r",$v);},$notYetCreated));
	$tsOrEvents=$eventTs+$notYetCreated;
	// we could simply use asort() but (special case) we want to also sort by eid (when events have same startTime).
	uksort($tsOrEvents,function($a,$b)use($tsOrEvents)
		   {
			   if($tsOrEvents[$a]==$tsOrEvents[$b]){return $a-$b;}
			   else                                {return $tsOrEvents[$a]-$tsOrEvents[$b];}
		   });

	$now=time();

	// setup pager
	// we try to adapt $defaultItemsPerPage so that $firstFutureTs is before the middle of the page
	$firstFutureTs=false;
	foreach(array_values($tsOrEvents) as $n=>$ts){if($ts>=$now){$firstFutureTs=$n;break;}}
	if($firstFutureTs===false){$firstFutureTs=count($tsOrEvents)-1;}
	foreach([25,25-5,25+5] as $defaultItemsPerPage)
	{
		if($firstFutureTs%$defaultItemsPerPage<$defaultItemsPerPage/2){break;}
	}
	$pager=new Pager(['itemName'=>$group->ruleType===0 ? t('events') : t('dates'),
					  'defaultItemsPerPage'=>$defaultItemsPerPage,'nbItems'=>count($tsOrEvents)]);
	// set default pager page, so that today ($firstFutureTs) is displayed
	if(!isset($_GET['firstItem'])){$pager->firstItem=$defaultItemsPerPage*intval(floor($firstFutureTs/$defaultItemsPerPage));}
	$tsOrEvents=array_slice($tsOrEvents,$pager->firstItem,$pager->itemsPerPage,true);

	// Now, we can just fetch the events we really need
	$eids=preg_grep('@^[0-9]@',array_keys($tsOrEvents));
	if(!count($eids)){$events=[];}
	else
	{
		$events=Event::fetchPartial('SELECT '.$fields.' FROM '.$joins.' WHERE Event.id IN ('.implode(',',$eids).')');
	}

	// replace timestamps by events in $tsOrEvents
	foreach($tsOrEvents as $k=>&$t)
	{
		if(substr($k,0,1)!=='t'){$t=$events[$k];}
	}unset($t);

	$out.=template_render('dlib/pager.tpl.php',['pager'=>$pager]);
	$out.='<table class="multiple-edit">';
	foreach($tsOrEvents as $tsOrEvent)
	{
		if(is_object($tsOrEvent))
		{
			$event=$tsOrEvent;
			if($event->getModerationStatus()==='trash'){continue;}
			$out.=demosphere_event_multiple_edit_render($event,
														['repetition-ref'=>true,
														 'repetition-link'=>false,
														 'repetition-copy-link'=>true]);
		}
		else
		{
			$ts=$tsOrEvent;
			$out.='<tr class="auto-uncreated '.
				($group->maxConfirmPrior && $ts<$now+$group->maxConfirmPrior ? 'creation-pending' : '').'" data-ts="'.$ts.'">'.
				'<td class="create">[+]</td>'.
				'<td></td>'.// link
				'<td></td>'.// copy
				'<td></td>'.// ref
				'<td></td>'.// showOnFrontpage
				($demosphere_config['enable_opinions'] ? '<td></td>' : '').// opinion
				'<td></td>'.// moderationStatus
				'<td></td>'.// needsAttention
				'';

			$event=$group->useReference();
			$out.= '<td class="date weekday demosphere_timestamp" '.
				'data-demosphere-timestamp="'.
				demos_format_date('demosphere-timestamp',$ts).'" >'.ent(strftime('%a',$ts)).'</td>';
			$day  ='<td class="date day  ">'.ent(strftime('%e',$ts)).'</td>';
			$month='<td class="date month">'.ent(strftime('%b',$ts)).'</td>';
			$out.=demosphere_date_time_swap_endian($day,$month,''); 
			$out.='<td class="date year">'.(abs($ts-$now)>3600*24*30*6 ? ent(strftime('%Y',$ts)) : '').'</td>';
			$out.='<td class="time">'.(  strftime('%R',$ts)=='03:33' ?'':ent(strftime('%R',$ts))).'</td>';
			//$out.='<td></td>';// moderation message
			$out.='<td class="title"><span class="moderation-message"></span>'.$event->safeHtmlTitle().'</td>';
			$out.='<td class="place">'.ent($event->usePlace()->useCity()->getShort()).'</td>';
			//$out.='<td colspan="100">xxx: '.date('r',$ts).'</td></tr>';
			$out.='</tr>';
		}
	}
	$out.='</table>';

	$formOpts=[];

	$form['events']=['html'=>$out];
	$form['repetition-group'] = ['type'=>'hidden','value'=>$group->id];

	// *******
	$form['tabs-start'] = ['html'=>'<div id="repetitions-tabs-wrapper">'.
						   '<div id="repetitions-tabs-line">'.
						   '<h2 class="active-_manual_"><span>'.t('Add manually').'</span></h2>'.
						   '<h2 class="active-_auto_  "><span>'.t('Automatic repetitions').'</span></h2>'.
						   '</div></div>'.
						   '<div id="repetitions-tabs-bodies" >'.
						   '<div id="add-manually-body" class="active-_manual_">'
						   ];
	$form['submit-1']=['type'=>'submit',
					   'value'=>t('submit')];

	$form['add-n-repetitions'] = 
		['type' => 'textfield',
		 'title' => t('Add a number of identical repetitions'),
		 'required' => false,
		 'description' => t("Repetitions that are identical to the reference will be created. Later, you can change each (repeted) event simply by editing it."),
		];
	$form['add-daily-repetitions'] = 
		['type' => 'textfield',
		 'title' => t('Add daily repetitions until this day'),
		 'required' => false,
		 'description' => t("Creates repetitions every day, from the day of the reference event to the day you specify here (included)."),
		];
	$form['add-weekly-repetitions'] = 
		['type' => 'textfield',
		 'title' => t('Add weekly repetitions until this day'),
		 'required' => false,
		 'description' => t("Creates repetitions once per week, on the same weekday (moday, tuesday...) as the the reference event."),
		];
	$weeksInMonth=['first'  => t('first' ),
				  'second' => t('second'),
				  'third'  => t('third' ),
				  'fourth' => t('fourth' ),
				  'last'   => t('last'	),
				   ];
	$form['add-monthly-repetitions-type'] = 
		[
			'type' => 'select',
			'options' => $weeksInMonth,
		];
	$form['add-monthly-repetitions'] = 
		['type' => 'textfield',
		 'title' => t('Add monthly repetitions every !number !weekday until this day',
					  ['!number'=>'%%number%%',
					   '!weekday'=>strftime('%A',$group->useReference()->startTime)]),
		 'required' => false,
		 'description' => t("Creates repetitions once per month."),
		];

	// Extra suggestions for add-date-repetitions text area
	$suggestDates=find_dates_and_times($currentEvent->body);
	$suggestDates=array_flip($suggestDates);
	// remove date suggestions with same date as existing repetitions
	foreach($events as $event)
	{
		$t=$event->startTime;
		unset($suggestDates[$t]);
		$noTime=mktime(3,33,0,date("n",$t),date("j",$t),date("Y",$t));
		unset($suggestDates[$noTime]);
	}
	$suggestDatesOut='<ul id="repetition-suggest-dates">';
	foreach($suggestDates as $ts=>$dud)
	{
		if($ts<time()){continue;}
		$suggestDatesOut.='<li data-ts="'.$ts.'">'.
			demos_format_date('full-date-time',$ts).
			'</li>';
	}
	$suggestDatesOut.='</ul>';
	$form['add-date-repetitions'] = 
		['type' => 'textarea',
		 'title' => t('Add repetitions on the following dates'),
		 'required' => false,
		 'description' => t("One date per line. Creates repetitions for each of the entered dates. If no time is specified, the time of the reference event is used. If you want, you can also specify a title after the date, separating with :: ... for example: 5/7/2010 :: demonstration wor workers rights"),
		 'resizable'=>false,
		 'prefix'=>'<div id="repetition-suggest-dates-wrap"><p>'.t('Suggestions (click) :').'</p>'.$suggestDatesOut.'</div>',
		];
	$form['add-existing-event'] = 
		['type' => 'textfield',
		 'title' => t('Add an existing event'),
		 'required' => false,
		 'description' => t("The existing event will become part of this group of repetetitions. You can enter a link to the event or the number of the event. Example: @example",['@example'=>$base_url.'/'.$demosphere_config['event_url_name'].'/1234']),
		];
	$form['extra-options'] = ['type' => 'fieldset',
							  'title' => t('extra options'),
							  'collapsible' => true,
							  'collapsed' => true,];
	$form['remove-existing-event'] = 
		['type' => 'textfield',
		 'title' => t('Remove an existing event'),
		 'required' => false,
		 'description' => t("Remove an existing event from this group of repetetitions. You can enter a link to the event or the number of the event. Example: @example",['@example'=>$base_url.'/'.$demosphere_config['event_url_name'].'/1234']).'<br/>'.
		 t('Note: you rarely need to use this option. If you just want to delete an event, change its status to "trash".')
		 ,];
	$form['extra-options-end'] = ['type' => 'fieldset-end'];
	
	$form['submit']=['type'=>'submit',
					 'value'=>'submit'];


	// **********************  Auto repetetitions
	$form['tabs-body2'] = ['html'=>
						   '</div>'.
						   '<div id="auto-body" class="active-_auto_">'
						   ];

	require_once 'dlib/form.php';

	$form['ruleType']=['type'=>'select',
					   'title'=>t('Automatic repetition'),
					   'options'=>RepetitionGroup::getRuleTypeLabels(),
					   'default-value'=>$group->ruleType,
					   ];

	$form['ruleData-time']=['type'=>'textfield',
							'title'=>t('Time'),
							'default-value'=>val($group->ruleData,'time',date('H:i',$group->useReference()->startTime)),
							'required'=>function($v,$n,$items){return $items['ruleType']['value']!=0;},
							'wrapper-attributes'=>['class'=>['ruleData']],
							];

	$weekdays=[];
	for($i=0;$i<7;$i++)
	{
		$ts=strtotime('Monday +'.$i.' days');
		$weekdays[date('D',$ts)]=strftime('%A',$ts);
	}
	$form['ruleData-dayOfWeek'   ]=['type'=>'select',
									'title'=>t('Day of week'),
									'options'=>$weekdays,
									'default-value'=>val($group->ruleData,'dayOfWeek',date('D',$group->useReference()->startTime)),
									'wrapper-attributes'=>['class'=>['ruleData']],
									];

	$weekInMonthTemplate='(<label>$title</label>)'.
		t('!weekNb !weekDay of the month',
		  ['!weekNb'=>'<select id="edit-$cname" class="$class" $attributes>$options</select>',
		   '!weekDay'=>'<span class="weekday"></span>']);
	$form['ruleData-weekInMonth' ]=['type'=>'select',
									'title'=>t('Week in month'),
									'options'=>$weeksInMonth,
									'default-value'=>val($group->ruleData,'weekInMonth',''),
									'wrapper-attributes'=>['class'=>['ruleData']],
									'template'=>$weekInMonthTemplate,
									];

	$form['ruleData-weekInMonth1']=['type'=>'select',
									'title'=>t('First week in month'),
									'options'=>$weeksInMonth,
									'default-value'=>val($group->ruleData,'weekInMonth1',''),
									'wrapper-attributes'=>['class'=>['ruleData']],
									'template'=>$weekInMonthTemplate,
									];

	$form['ruleData-weekInMonth2']=['type'=>'select',
									'title'=>t('Second week in month'),
									'options'=>$weeksInMonth,
									'default-value'=>val($group->ruleData,'weekInMonth2',''),
									'wrapper-attributes'=>['class'=>['ruleData']],
									'template'=>$weekInMonthTemplate,
									];

	$form['ruleData-dayInMonth'  ]=['type'=>'int',
									'title'=>t('Day in month'),
									'default-value'=>val($group->ruleData,'dayInMonth',date('j',$group->useReference()->startTime)),
									'validate'=>function($v){if($v<1 || $v>31){return t('Invalid day (1-31).');}},
									'wrapper-attributes'=>['class'=>['ruleData']],
									];

	$form['organizerEmail']=['type'=>'email',
							 'title'=>t('Organizer email'),
							 'default-value'=>$group->organizerEmail,
							 'required'=>function($v,$n,$items){return $items['ruleType']['value']!=0;},
							 'description'=>t('Emails will be sent to this address regularly, requesting confirmation of upcoming dates.'),
							 ];


	$currentPage->addJsVar('ruleTypes' ,RepetitionGroup::$ruleType_['values']);
	$currentPage->addJsVar('ruleFields',RepetitionGroup::$ruleFields);
	$currentPage->addJsVar('maxConfirmPriors',['none'       =>'',
											  'daily'      =>7,
											  'weekly'     =>40,
											  'semimonthly'=>60,
											  'monthly'    =>80,
											  'dayofmonth' =>80,
											 ]);

	$currentPage->addJsVar('minConfirmPriors',['none'       =>'',
											  'daily'      =>3,
											  'weekly'     =>5,
											  'semimonthly'=>8,
											  'monthly'    =>15,
											  'dayofmonth' =>15,
											 ]);

	$logDisplay='<h4 id="group-log-title">'.t('Log:').'</h4>';
	$logDisplayUl='<ul id="group-log">';
	foreach(array_reverse($group->log) as $logEntry)
	{
		$logDisplayUl.='<li>';
		$logDisplayUl.=ent(demos_format_date('short-day-month-year',$logEntry['ts']).' '.demos_format_date('time',$logEntry['ts']).' : ');
		unset($logEntry['ts']);
		switch($logEntry['type'])
		{
		case 'confirm':
			$logDisplayUl.=t('Dates where confirmed or canceled by @email.',
						   ['@email' =>$logEntry['email'] ?? '?',]).' ';
			break;
		case 'stop':
			$logDisplayUl.=t('Stopped this automatic event after sending several emails to @email, without any confirmations.',
						   ['@email' =>$logEntry['email'],]).' ';
			if($logEntry['delay']===false){$logDisplayUl.=t('Never confirmed');}
			else
			{
				$logDisplayUl.=t('Last confirmed @nbDays days ago.',
							['@nbDays'=>round($logEntry['delay']/(3600*24)),]);
			}

			break;
		case 'email':
			$dates='';
			foreach($logEntry['events'] as $eid)
			{
				$start=db_result_check('SELECT startTime FROM Event WHERE id=%d',$eid);
				$dates.=demos_format_date('shorter-date-time',$start).', ';
			}
			$logDisplayUl.=t('Sent email to @email asking for a confirmation for the following dates: @dates',
						   ['@email' =>$logEntry['email'],
							'@dates'=>$dates,
							]);
			break;
		default:
			$logDisplayUl.=$logEntry['type'].':: ';
			unset($logEntry['type']);
			foreach($logEntry as $n=>$v){$logDisplayUl.=ent($n).':'.ent(is_array($v) ? print_r($v,true) : $v).';  ';}
		}
		$logDisplayUl.='</li>';
	}
	$logDisplayUl.='</ul>';
	if(count($group->log)!==0){$logDisplay.=$logDisplayUl;}
	else
	{$logDisplay.='<p>'.t('No emails sent yet. No confirmations made yet.').'</p>';}
	$form['log'] = ['html' => $logDisplay];

	$form['auto-extra-options'] = ['type' => 'fieldset',
								   'title' => t('extra options'),
								   'collapsible' => true,
								   'collapsed' => true,];

	$form['ruleStart'      ]=['type'=>'timestamp',
							  'title'=>t('Start'),
							  'default-value'=>$group->ruleStart,
							  'description'=>t('Automatic repetitions will start after this date (format: YYYY-MM-DD HH:MM:SS). Leave empty if it starts now.'),
							  ];

	$form['ruleEnd'        ]=['type'=>'timestamp',
							  'title'=>t('End'),
							  'default-value'=>$group->ruleEnd,
							  'description'=>t('Automatic repetitions will stop at this date (format: YYYY-MM-DD HH:MM:SS). Leave empty if it continues forever.'),
							  ];

	$form['minConfirmPrior']=['type'=>'int',
							  'title'=>t('Min confirm prior'),
							  'default-value'=>round($group->minConfirmPrior/(24*3600)),
							  'description'=>t('Emails will be sent this number of days before an unconfirmed event date.'),
							  ];

	$form['maxConfirmPrior']=['type'=>'int',
							  'title'=>t('Max confirm prior'),
							  'default-value'=>round($group->maxConfirmPrior/(24*3600)),
							  'description'=>t('The organizer can confirm event dates up to this number of days in the future.'),
							  ];

	$form['autoIsEnabled'  ]=['type'=>'checkbox',
							  'title'=>t('Enabled'),
							  'default-value'=>$group->autoIsEnabled,
							  'description'=>t('If the organizer does not confirm any dates after two emails, the event is automatically stopped after some time. You can restart it by checking this box. Please make sure you are not spamming somebody that does not want to receive emails.'),
							  ];

	if($group->id && $group->organizerEmail!=='')
	{
		$editUrl=$base_url.'/repetition-group/'.$group->id.'/confirm';
		require_once 'demosphere-self-edit.php';
		$selfEdit=demosphere_self_edit_create($group->organizerEmail,false,$group->id);
		$selfEditUrl=$editUrl.'?selfEdit='.$selfEdit['password'];
		$form['self-edit-url'] = ['html' => '<p>'.t('Self edit link:').' '.ent($selfEditUrl).'</p>'];
	}

	$form['auto-extra-options-end'] = ['type' => 'fieldset-end'];


	$form['auto-submit']=['type'=>'submit',
						  'value'=>t('submit')];

	$form['tabs-end'] = ['html'=>
						 '</div>'.
						 '<div id="auto-body">'
						 ];

	// Fix active tab (we need $items['ruleType'] value... so we have to do it in pre-render)
	$formOpts['pre-render']=function(&$items,$formOpts,$isValidationErr)
		{
			$isAuto=$items['ruleType'][$isValidationErr ? 'value' : 'default-value'];
			$items['tabs-start']['html']=str_replace('active-_manual_',($isAuto ? ''       : 'active'),$items['tabs-start']['html']);
			$items['tabs-start']['html']=str_replace('active-_auto_'  ,($isAuto ? 'active' : ''      ),$items['tabs-start']['html']);
			$items['tabs-body2']['html']=str_replace('active-_auto_'  ,($isAuto ? 'active' : ''      ),$items['tabs-body2']['html']);
		};
	$formOpts['validate']='demosphere_event_repetition_form_validate';
	$formOpts['submit']='demosphere_event_repetition_form_submit';

	$html=form_process($form,$formOpts);
	// Hack: Move monthly-repetitions-type selector into label of following item
	$html=preg_replace('@(<div id="edit-add-monthly-repetitions-type-wrapper"[^>]*>(.*)</div>)(.*)%%number%%@sU','$3$2',$html);
	return $html;
}

function demosphere_event_repetition_form_validate(&$form)
{
	global $base_url,$demosphere_config;
	require_once 'dlib/find-date-time/find-date-time.php';
	$values=dlib_array_column($form,'value');

	$currentEvent=Event::fetch($values['current-eid']);
	$group=$currentEvent->useRepetitionGroup();
	if(!$group->referenceId){$group->referenceId=$currentEvent->id;}
	
	$field='add-n-repetitions'; 
	if($group->useReference()->getModerationStatus()==='trash')
	{
		$form[$field]['error']=t('Repetition reference is an event in trash bin! You must select a new reference.');
	}
	$field='add-n-repetitions';
	if(strlen($values[$field]) && !ctype_digit($values[$field]))
	{
		$form[$field]['error']=t('must be a number');
	}
	$field='add-daily-repetitions';
	if(strlen($values[$field]) && find_date_and_time($values[$field])===false)
	{
		$form[$field]['error']=t('must be a valid date');
	}
	$field='add-weekly-repetitions';
	if(strlen($values[$field]) && find_date_and_time($values[$field])===false)
	{
		$form[$field]['error']=t('must be a valid date');
	}
	$field='add-monthly-repetitions';
	if(strlen($values[$field]) && find_date_and_time($values[$field])===false)
	{
		$form[$field]['error']=t('must be a valid date');
	}
	$field='add-date-repetitions';
	$lines=explode("\n",trim($values['add-date-repetitions']));
	if($lines[0]===''){$lines=[];}
	foreach($lines as $line)
	{
		$date=explode("::",$line);
		$date=$date[0];
		if(find_date_and_time($date)===false)
		{
			$form[$field]['error']=t('must have one valid date per line');
		}		
		if(preg_match('@::(.*$)@',$date,$matches))
		{
			$nbUnderscores=substr_count($matches[1],'_');
			if(!($nbUnderscores>=2 && ($nbUnderscores%2)==0))
			{
				$form[$field]['error']=
							   t('to publish, you must choose keywords in the title using underscores. Example : this is a title with _a keyword_ that is important' );
			}
		}
	}
	$field='add-existing-event';
	if(strlen($values[$field]))
	{
		if(!preg_match('@^('.preg_quote($base_url,'@').'/)?(/?'.$demosphere_config['event_url_name'].'/)?([0-9]+)(/|$)@',
					   trim($values[$field]),$matches))
		{
			$form[$field]['error']=t('invalid link or event number');
		}
		else
		{
			$addEvent=Event::fetch($matches[3],false);
			if($addEvent===null){$form[$field]['error']=t('added event does not exist');}
			else
			if($addEvent->repetitionGroupId==$currentEvent->repetitionGroupId &&
			   $currentEvent->repetitionGroupId!=0			   )
			{$form[$field]['error']=t('added event already in this repetition group');}
			else
			if($addEvent->id===$currentEvent->id)
			{$form[$field]['error']=t('added event is the same as the current event');}
			else
			if(!$addEvent->access('edit'))
			{
				$form[$field]['error']=t('permission denied to change that event');
			}
		}
	}
	$field='remove-existing-event';
	if(strlen($values[$field]))
	{
		if(!preg_match('@^('.preg_quote($base_url,'@').'/)?(/?'.$demosphere_config['event_url_name'].'/)?([0-9]+)(/|$)$@',
					   trim($values[$field]),$matches))
		{
			$form[$field]['error']=t('invalid link or event number');
		}
		else
		{
			$removeEvent=Event::fetch($matches[3],false);
			if($removeEvent===null){$form[$field]['error']=t('removed event does not exist');}
			else
			if($removeEvent->repetitionGroupId!=$currentEvent->repetitionGroupId)
			{$form[$field]['error']=t('removed is not in this repetition group');}
			else
			if(!$removeEvent->access('edit'))
			{
				$form[$field]['error']=t('permission denied to change that event');
			}
		}
	}

	// ****************** automatic repetitions

	$isAuto=$values['ruleType']!=RepetitionGroup::ruleTypeEnum('none');
	if($isAuto)
	{
		$dayNb=['first'  => 0,'second' => 1,'third'  => 2,'fourth' => 3,'last'   => 4,];
		if($values['ruleType']==RepetitionGroup::ruleTypeEnum('semimonthly') &&
		   $dayNb[$values['ruleData-weekInMonth1']]>=$dayNb[$values['ruleData-weekInMonth2']])
		{
			$form['ruleData-weekInMonth2']['error']=t('For semimonthly repetitions, the second week must be after the first week.');
		}
	}
}

function demosphere_event_repetition_form_submit(&$form)
{
	global $base_url,$demosphere_config;
	require_once 'dlib/find-date-time/find-date-time.php';
	$values=dlib_array_column($form,'value');

	// Reference may have changed. Find new reference.
	$currentEvent=Event::fetch($values['current-eid']);

	$group=$currentEvent->useRepetitionGroup();
	// special case: this is an event that did not have any repetitions before
	if($group->id==0)
	{
		$group->referenceId=$currentEvent->id;
		$group->save();
		$currentEvent->repetitionGroupId=$group->id;
		$currentEvent->save();
	}
	$refEvent=$group->useReference();

	$repetitions=$currentEvent->getAllRepetitions();
	$repetitions=array_flip($repetitions);

	// check if redirection is necessary (current url is a deleted event)
	//$pageEid=intval(arg(1));
	//if(!isset($repetitions[$pageEid]))
	//{
	// 	$form_state['redirect']=$demosphere_config['event_url_name'].'/'.$refEvent->id.'/repetition';
	//}

	// load all other repetitions
	foreach(array_keys($repetitions) as $eid)
	{
		$repetitions[$eid]=Event::fetch($eid);
	}

	// ****** Add repetitions ************

	$now=time();
	$dates=[];
	// Add daily repetitions
	$end=find_date_and_time($values['add-daily-repetitions']);
	$end=set_default_time($end,strtotime('23:59'));
	if($end!==false)
	{
		for($ts=$refEvent->startTime;$ts<=$end;$ts=strtotime('+1 days',$ts))
		{
			$dates[]=$ts;
		}
	}
	// Add weekly repetitions
	$end=find_date_and_time($values['add-weekly-repetitions']);
	$end=set_default_time($end,strtotime('23:59'));
	if($end!==false)
	{
		for($ts=$refEvent->startTime;$ts<=$end;$ts=strtotime('+7 days',$ts))
		{
			$dates[]=$ts;
		}
	}
	// Add monthly repetitions
	$end=find_date_and_time($values['add-monthly-repetitions']);
	$end=set_default_time($end,strtotime('23:59'));
	if($end!==false)
	{
		for($ts=$refEvent->startTime;$ts<=$end;$ts=strtotime('+7 days',$ts))
		{
			// Find first weekday of month for the month of $ts
			$first=mktime(date("H",$ts),date("i",$ts),date("s",$ts),
						  date("n",$ts),1            ,date("Y",$ts));
			$ct=0;
			for(;date('w',$first)!=date('w',$ts);
				$first=strtotime('+1 day',$first))
			{
				if(($ct++)>7){fatal("argh1");}
			}

			// Find last weekday of month for the month of $ts
			$last=mktime(date("H",$ts)  ,date("i",$ts),date("s",$ts),
						 date("n",$ts)+1,1            ,date("Y",$ts));
			$ct=0;
			do
			{
				$last=strtotime('-1 day',$last);
				if(($ct++)>7){fatal("argh2");}
			}
			while(date('w',$last)!=date('w',$ts));

			// keep this date ($ts) if it matches the condition
			switch($values['add-monthly-repetitions-type'])
			{
			case 'last'  : if($ts==$last                       ){$dates[]=$ts;}break;	
			case 'first' : if($ts==$first					   ){$dates[]=$ts;}break;	
			case 'second': if($ts==strtotime('+7 days' ,$first)){$dates[]=$ts;}break;	
			case 'third' : if($ts==strtotime('+14 days',$first)){$dates[]=$ts;}break;	
			case 'fourth': if($ts==strtotime('+21 days',$first)){$dates[]=$ts;}break;	
			}
		}
	}

	// Do not add daily, weekly or monthly repetitions 
	// that are the same as existing repetitions or that are in the past.

	foreach($repetitions as $event)
	{
		if($event->getModerationStatus()==='trash'){continue;}
		$existingDates[]=$event->startTime;
	}
	$noRepetitionsBefore=max($existingDates);
	foreach(array_keys($dates) as $k)
	{
		$ts=$dates[$k];
		if($ts<=$noRepetitionsBefore){unset($dates[$k]);}
		else
		if(array_search($ts,$existingDates)!==false)
		{// note: this condition should never happen because of $noRepetitionsBefore
			unset($dates[$k]);
			dlib_message_add(t('not adding repetition at %date, it already exists',
								 ['%date'=>demos_format_date('shorter-date-time',$ts)]));
		}
		else
		if($ts<$now)
		{
			unset($dates[$k]);
			dlib_message_add(t('not adding repetition at %date that is in the past',
								 ['%date'=>demos_format_date('shorter-date-time',$ts)]));
		}
	}

	// Add n repetitions
	$n=intval($values['add-n-repetitions']);
	if($n){$dates=array_merge($dates,array_fill(0,$n,$refEvent->startTime));}

	// add list of repetitions
	$extraInfo=[];
	$lines=explode("\n",trim($values['add-date-repetitions']));
	if($lines[0]===''){$lines=[];}
	foreach($lines as $line)
	{
		$date=explode("::",$line);
		$date=$date[0];
		$ts=find_date_and_time($date);
		// if time is undefined, use $refEvent time
		$dates[]=set_default_time($ts,$refEvent->startTime);
		if(preg_match('@::(.*$)@',$line,$matches))
		{
			$title=trim($matches[1]);
			$htmlTitle=ent($title);
			$htmlTitle=preg_replace('@_([^_]*)_@','<strong>$1</strong>',
									 $htmlTitle);
			$title=str_replace('_','',$title);
			$extraInfo[count($dates)-1]=['title'=>$title,
										 'htmlTitle'=>$htmlTitle];
		}
	}

	// all the repetitions are in $dates array. Now actually create them.
	if(count($dates)>50){dlib_bad_request_400("adding more than 50 repetitions is not reasonable, aborting");}

	foreach($dates as $k=>$date)
	{
		$newEvent=$group->createEvent($date,val($extraInfo,$k,[]));
		dlib_message_add(t("Adding event repetition at !date",['!date'=>demos_format_date('full-date-time',$newEvent->startTime)]));
	}

	// add existing event
	$field='add-existing-event';
	if(strlen($values[$field]))
	{
		preg_match('@^('.preg_quote($base_url,'@').'/)?(/?'.$demosphere_config['event_url_name'].'/)?([0-9]+)(/|$)@',
				   trim($values[$field]),$matches);
		$addEvent=Event::fetch($matches[3]);
		// Remove added event from an exisiting repetition group (if it was in one)
		$addEvent->removeFromRepetitionGroup();
		// now we can safely change the repetition group for the added event
		$addEvent->repetitionGroupId=$refEvent->repetitionGroupId;
		$addEvent->save();
	}

	// remove event from repetitions
	$field='remove-existing-event';
	if(strlen($values[$field]))
	{
		preg_match('@^('.preg_quote($base_url,'@').'/)?(/?'.$demosphere_config['event_url_name'].'/)?([0-9]+)(/|$)@',
				   trim($values[$field]),$matches);
		$removeEvent=Event::fetch($matches[3]);
		$removeEvent->removeFromRepetitionGroup();
		$removeEvent->save();
	}

	// *************************  Automatic repetitions
	$group->ruleType=$values['ruleType'];
	$group->organizerEmail=$values['organizerEmail'];
	$group->ruleData=[];
	foreach(RepetitionGroup::$ruleFields[$group->getRuleType()] as $field)
	{
		$group->ruleData[$field]=$values['ruleData-'.$field];
	}
	$group->ruleStart      =$values['ruleStart'];
	$group->ruleEnd        =$values['ruleEnd'];
	$group->minConfirmPrior=$values['minConfirmPrior']*24*3600;
	$group->maxConfirmPrior=$values['maxConfirmPrior']*24*3600;
	if(!$group->autoIsEnabled && $values['autoIsEnabled'])
	{
		$group->addLog(['type'=>'restart','email'=>$group->organizerEmail]);
	}
	$group->autoIsEnabled  =$values['autoIsEnabled'];
	$group->save();
}

function demosphere_event_repetition_copy_form($newEventId)
{
	global $demosphere_config,$currentPage,$user,$base_url;
	require_once 'dlib/diff.php';

	$now=time();

	$newEvent=Event::fetch($newEventId,false);
	if($newEvent===null){dlib_not_found_404();}

    if($newEvent->repetitionGroupId==0){dlib_bad_request_400('This event is not in a repetition group');}
    
	$isRepGroupSelfEdit=false;
	if(!$user->checkRoles('admin','moderator'))
	{
		require_once 'demosphere-self-edit.php';
		if(!demosphere_self_edit_check_group($newEvent->repetitionGroupId)){dlib_permission_denied_403();}
		$isRepGroupSelfEdit=true;
	}

	$currentPage->title=t('Repetition copy');

	if($isRepGroupSelfEdit)
	{
		$repetitions=db_one_col('SELECT id FROM Event WHERE repetitionGroupId=%d AND '.
								'moderationStatus NOT IN (%d) AND '.
								'startTime>%d ORDER BY startTime ASC',
								$newEvent->repetitionGroupId,
								Event::moderationStatusEnum('trash'),
								$now);
	}
	else
	{
		$repetitions=$newEvent->getAllRepetitions();
	}

	$currentPage->addJs('lib/jquery.js');
	$currentPage->addCssTpl('demosphere/css/demosphere-event-repetition.tpl.css');
	$currentPage->addJs('demosphere/js/demosphere-event-repetition.js');
	$currentPage->addCss('dlib/form.css');
	$currentPage->addJs('dlib/form.js');

	// Tabs : (view, edit, repetitions...)
	require_once 'demosphere-event.php';
	demosphere_event_tabs($newEvent,'repetition-copy');

	$isJustChanged=isset($_GET['just-changed']);

	// A list of fields that can be copied
	$fields=['date'=>t('Date'),'time'=>t('Time.'),'htmlTitle'=>t('Title'),
			 'body'=>t('Body'),'place'=>t('Place'),'topics'=>t('Topics'),'incompleteMessage'=>t('Incomplete')];
	// simplify for self-edit
	if($isRepGroupSelfEdit)
	{
		unset($fields['date']);
		unset($fields['incompleteMessage']);
	}

	if($isJustChanged)
	{
		// old event was stored in persistent variable when event was saved in event edit form.
		$oldEvent=variable_get($newEvent->id.'-'.hash('sha256','erc:'.session_id()),'event-repetition-copy');
		if($oldEvent===false){dlib_bad_request_400('failed to find old event in session!');}
		if($oldEvent->id!=$newEvent->id){fatal('repetition copy: strange mismatch in event id');}
		//var_dump($oldEvent,$newEvent);
	}
	else{$oldEvent=$newEvent;}

	if(isset($_POST['submit']) || 
	   isset($_POST['no-change']))
	{
		demosphere_event_repetition_copy_form_submit($newEvent,$repetitions,$fields,$isJustChanged,$isRepGroupSelfEdit);
	}

	$fieldHasChanges=[];
	foreach($fields as $field=>$unused){$fieldHasChanges[$field]=false;}

	// Which fields have changed during event submission
	$oldNewChange=[];
	foreach($fields as $field=>$name)
	{
		$oldNewChange[$field]=demosphere_event_repetition_field_changed($field,$oldEvent,$newEvent);
		// Debug spurious changes:
		//if($field==='body'){file_put_contents('/tmp/old-body',$oldEvent->body);
		//                    file_put_contents('/tmp/new-body',$newEvent->body);}
	}
	$rows=[];

	$firstRows=[];
	// If this is just changed, prepend two extra rows to display old and new 
	if($isJustChanged)
	{
		$firstRows[]='event-change-title';
		$firstRows[]='table-header';
		$firstRows[]='old';
		$firstRows[]='new';
		$firstRows[]='repetitions-title';
	}	
	else
	{
		$firstRows[]='table-header';
	}
	$repetitions=array_merge($firstRows,$repetitions);

	foreach($repetitions as $eid)
	{
		$row=[];
		$row['isOldJC']=false;
		$row['isNewJC']=false;
		if($eid==='repetitions-title' || $eid==='event-change-title' || $eid=='table-header'){$rows[]=$eid;continue;}
		if($eid=='old'){$event=$oldEvent;$row['isOldJC']=true;}
		else
		if($eid=='new'){$event=$newEvent;$row['isNewJC']=true;}
		else
		{
			$event=Event::fetch($eid);
		}

		$row['isMainRepList']=!$row['isOldJC'] && !$row['isNewJC'];
		$row['event']=$event;
		$row['startTime']=$event->startTime;
		$row['isFuture']=$event->startTime>$now;

		$cols=[];
		foreach($fields as $field=>$name)
		{
			$changed=demosphere_event_repetition_field_changed($field,$oldEvent,$event, $display,$diffDisplay);
			$cols[$field]=['title'=>cached_filter_xss($display[1],
													  ['span','br']),
						   'changed'=>$changed,
						   'oldNewChange'=>$oldNewChange[$field],
						   'diffDisplay'=>$diffDisplay===null ? false: cached_filter_xss($diffDisplay[1],['div','span','br','p']),
						   'checked'=>!$changed && $oldNewChange[$field] &&  $event->startTime>$now];
			if($changed || $oldNewChange[$field]){$fieldHasChanges[$field]=true;}
		}
		$row['cols']=$cols;
		$rows[]=$row;
	}

	require_once 'dlib/template.php';

	return template_render('templates/demosphere-event-repetition-copy-form.tpl.php',
						   [compact('rows','base_url','isJustChanged','fields','oldEvent','newEvent','fieldHasChanges','isRepGroupSelfEdit'),
							'event_url'=>$base_url.'/'.$demosphere_config['event_url_name'],
							$demosphere_config,
							//array_intersect_key($demosphere_config,array_flip(array('event_url_name')))
						   ]
						   );

	//return form_process($form);
}

function cached_filter_xss($text,$allowed=['a', 'em', 'strong', 'cite', 'code', 'ul', 'ol', 'li', 'dl', 'dt', 'dd'])
{
	require_once 'dlib/filter-xss.php';
	static $cache=[];
	$cacheKey=md5($text.'-'.implode('-',$allowed));
	$res=val($cache,$cacheKey);
	if($res===false)
	{
		$res=filter_xss($text,$allowed);
		$cache[$cacheKey]=$res;
	}
	return $res;
}

function demosphere_event_repetition_copy_form_submit($newEvent,$repetitions,$fields,$isJustChanged,$isRepGroupSelfEdit)
{
	global $demosphere_config,$base_url;

	dlib_check_form_token('event-repetition-copy-form');

	// which redirect
	if($isJustChanged){$redirect=$base_url.'/'.$demosphere_config['event_url_name'].'/'.$newEvent->id;}
	else              {$redirect=$base_url.'/'.$demosphere_config['event_url_name'].'/'.$newEvent->id.'/repetition-copy';}

	if(isset($_POST['no-change'])){dlib_redirect($redirect);}

	// If this is a self edit, automatically change the event reference to this event,
	// since this most likely is what the user wants.
	if($isRepGroupSelfEdit)
	{
		$newEvent->useRepetitionGroup()->referenceId=$newEvent->id;
		$newEvent->useRepetitionGroup()->save();
	}

	$newEventTopics=array_keys($newEvent->getTopicNames());

	foreach($repetitions as $eid)
	{
		$event=false;
		$changedFields=[];
		foreach($fields as $field=>$name)
		{
			$val=isset($_POST[$field.'-'.$eid]);
			if($val===false){continue;}
			if($event===false){$event=Event::fetch($eid);}
			$changedFields[]=$name;
			switch($field)
			{
			case 'date': 
				$t0=$event->startTime;
				$t1=$newEvent->startTime;
				$event->startTime=
					mktime(date("H",$t0),date("i",$t0),date("s",$t0),
						   date("n",$t1),date("j",$t1),date("Y",$t1));
				break;
			case 'time': 
				$t0=$event->startTime;
				$t1=$newEvent->startTime;
				$event->startTime=
					mktime(date("H",$t1),date("i",$t1),date("s",$t1),
						   date("n",$t0),date("j",$t0),date("Y",$t0));
				break;
			case 'htmlTitle': 
				$event->title=$newEvent->title;
				$event->htmlTitle=$newEvent->htmlTitle;
				break;
			case 'body': 
				$event->body=$newEvent->body;
				break;
			case 'place': 
				$event->setPlace($newEvent->usePlace());
				break;
			case 'topics':
				$event->setTopics($newEventTopics);
				if($newEvent->extraData['topic-was-set'] ?? false){$event->extraData['topic-was-set']=true;}
				break;
			case 'incompleteMessage':
				$event->incompleteMessage=$newEvent->incompleteMessage;
				break;
			default:
				fatal('demosphere_event_repetition_copy_form_submit: bad field');
			}
		}
		if($event!==false)
		{
			$event->save();
			dlib_message_add(t('<a href="!url">repetition !nid</a>: updating: @fields',
							   ['!url'=>$event->url(),
								'!nid'=>$event->id,
								'@fields'=>implode(', ',$changedFields)]));
		}
	}
	dlib_redirect($redirect);
}

//! check if this field has changed between event1 and event2.
//! Also returns array with display for this field on both of the events.
function demosphere_event_repetition_field_changed($field,$event1,$event2,
											 &$display=null,&$diffDisplay=null)
{
	require_once 'demosphere-date-time.php';
	// the display for event1
	$d1=false;
	// the display for event2
	$d2=false;
	// Used for longer fields (body,place,vocab) that require a html diff display.
	$text=false;

	// **** first check for changes : sets $changed
	switch($field)
	{
	case 'htmlTitle':
	case 'body':
		$changed=($event1->$field !== $event2->$field);
		// Re-check: for html ignore differences in the amount of whitespace
		if($changed)
		{
			$changed=
				preg_replace('@\s+@',' ',$event1->$field)!==
				preg_replace('@\s+@',' ',$event2->$field);
		}
		break;
	case 'date':
		$d1=demos_format_date('day-month-abbrev',$event1->startTime);
		$d2=demos_format_date('day-month-abbrev',$event2->startTime);
		$changed=$d1!==$d2;
		break;
	case 'time':
		$d1=strftime('%R',$event1->startTime);
		$d2=strftime('%R',$event2->startTime);
		$changed=$d1!==$d2;
		break;
	case 'place':
		require_once 'demosphere-event-edit-form.php';
		$placeChanges=$event1->usePlace()->differences($event2->usePlace());
		$changed=count($placeChanges)!==0;
		$d1=$event1->usePlace()->getCityName();
		$d2=$event2->usePlace()->getCityName();
		for($i=0;$i<2;$i++)
		{
			$event=$i==0 ? $event1 : $event2;
			$text[$i]='';
			$text[$i].=ent($event->usePlace()->getCityName()).'<br/>';
			$text[$i].=str_replace("\n",'<br/>',ent($event->usePlace()->address)).'<br/>';
			if(count(array_intersect($placeChanges,['zoom','latitude','longitude']))!==0)
			{
				$text[$i].='map-change'.$i;
			}
		}
		break;
	case 'topics':
		$t1=$event1->getTopicNames();
		$t2=$event2->getTopicNames();
		// hack : old topics are stored here (see: demosphere_event_edit_form_submit())
		if(isset($event1->extraData['topics'])){$t1=$event1->extraData['topics'];}
		if(isset($event2->extraData['topics'])){$t2=$event2->extraData['topics'];}
		$d1=implode(', ',$t1);
		$d2=implode(', ',$t2);
		$changed=$d1!==$d2;
		
		if(($event1->extraData['topic-was-set'] ?? false) !== 
		   ($event2->extraData['topic-was-set'] ?? false)    ){$changed=true;}
		break;
	case 'incompleteMessage':
		$t1=$event1->incompleteMessage;
		$t2=$event2->incompleteMessage;
		$changed=($t1 !== $t2);
		if($event1->getModerationStatus()!=='incomplete' ||
		   $event2->getModerationStatus()!=='incomplete'   ){$changed=false;}
		$d1=t('incomplete');
		$d2=t('incomplete');
		$text=[$t1,$t2];
		break;
	}

	// **** compute display values for certain fields
	switch($field)
	{
	case 'htmlTitle':
		$d1=$event1->$field;
		$d2=$event2->$field;
		break;
	case 'body':
		$d1=t('body');
		$d2=t('body');
		$text=[$event1->body,$event2->body];
		//$text=array('','');
		break;
	case 'place':
		break;
	}

	if(!isset($changed)){fatal('demosphere_event_repetition_field_changed: unknown field:'.$field);}
	
	if($text===false)
	{
		$display=[$d1,$d2];
		$diffDisplay=null;
	}
	else
	{
		$display=[];
		$diffDisplay=null;
		//echo "diff field $field,".strlen($text[0]).":".strlen($text[1])."<br/>";
		// compute diff (this might be expensive for big texts and a lot of repeating events, 
		// so we cache results)
		static $cache=[];
		$cacheKey=md5($text[0].'-'.$text[1]);
		$diffs=val($cache,$cacheKey);
		if($diffs===false)
		{
			$diffs=html_diff($text[0],$text[1]);
			$cache[$cacheKey]=$diffs;
		}
		for($i=0;$i<2;$i++)
		{
			$firstDiff=strpos($diffs[$i],'<span class="diff"');
			if(!$changed && $d1!==false){$d=($i==0 ? $d1 : $d2);}
			else
			if($firstDiff===false)
			{$d=mb_substr(dlib_fast_html_to_text(preg_replace('@</?(br|p|h)[^>]*/?>@','; ',$text[$i])),0,30);}
			else
			{
				$d=($firstDiff!=0 ? '…' : '').
					dlib_fast_html_to_text(preg_replace('@</?(br|p|h)[^>]*/?>@','; ',
												   mb_substr($diffs[$i],$firstDiff)));
			}
			//echo 'field:';var_dump($field);
			//var_dump($diffs);
			$diffDisplay[$i]='<div class="diff-wrap">'.'<span class="popup-button">[+]</span>'.
				'<div class="html-diff diff-'.$field.'">'.$diffs[$i].'</div></div>';
			$display[$i]='<span class="short">'.$d.'</span>';
		}
	}

	return $changed;
}

function demosphere_event_repetition_cron()
{
	// All active automatic groups
	$groups=RepetitionGroup::fetchList('WHERE ruleType!=0 AND (ruleStart=0 OR ruleStart<%d) AND (ruleEnd=0 OR ruleEnd>%d)  AND autoIsEnabled=1',time(),time());
	foreach($groups as $group)
	{
		$group->autoCreateEvents();
		$group->sendConfirmEmail();
	}


	demosphere_event_repetition_delete_old_auto_unpublished_events();
}

//! Automatically trashes past auto repetitions that were not confirmed.
//! This is called from cron.
function demosphere_event_repetition_delete_old_auto_unpublished_events()
{
	$ids=db_one_col('SELECT Event.id FROM Event INNER JOIN RepetitionGroup ON Event.repetitionGroupId=RepetitionGroup.id '.
					'WHERE RepetitionGroup.ruleType!=0 '.
					'AND Event.status=0 '.
					'AND Event.moderationStatus=%d '.
					"AND Event.moderationMessage='%s' ".
					'AND Event.startTime<%d',
					Event::moderationStatusEnum('waiting'),'automatic-repetition',time()-3600*24*15);
	foreach($ids as $id)
	{
		$event=Event::fetch($id);
		$event->trash();
		$event->save();
	}
}


//! Ajax create a single event, render it in multiple-edit layout, and return html.
//! Mod clicks on [+] on un-created date.
function demosphere_event_repetition_group_create_auto($groupId)
{
	dlib_check_form_token('demosphere_event_multiple_edit');
	$group=RepetitionGroup::fetch($groupId);
	$ts=(int)$_POST['ts'];
	$event=$group->createEvent($ts);
	require_once 'demosphere-event-multiple-edit.php';
	$out=demosphere_event_multiple_edit_render($event,
											   ['repetition-ref'=>true,
												'repetition-link'=>false,
												'repetition-copy-link'=>true]);
	return $out;
}

//! Displays (for admin&mod) a list of all recent repetition groups
function demosphere_event_repetition_groups()
{
	global $currentPage;
	$currentPage->addCssTpl('demosphere/css/demosphere-event-repetition.tpl.css');

	$now=time();
	$start=intval($_GET['start'] ?? strtotime('2 months ago'));

	$groupNbEv =db_one_col("SELECT repetitionGroupId,COUNT(*) FROM Event,RepetitionGroup WHERE ".
						   "Event.repetitionGroupId=RepetitionGroup.id ".
						   "GROUP BY repetitionGroupId ");

	$groups=RepetitionGroup::fetchList("FROM Event,RepetitionGroup WHERE ".
									   "Event.repetitionGroupId=RepetitionGroup.id AND ".
									   "startTime>%d ".
									   "GROUP BY repetitionGroupId ",$start);
	usort($groups,function($a,$b)use($groupNbEv)
		  {
			  $d=$b->ruleType-$a->ruleType;
			  if($d!==0){return $d;}
			  return $groupNbEv[$b->id]-$groupNbEv[$a->id];
		  });

	require_once 'demosphere-date-time.php';
	require_once 'dlib/template.php';
	return template_render('templates/demosphere-event-repetition-groups.tpl.php',
						   [compact('groups','groupNbEv')]);
}

//! A form that allows event organizers to edit their own repetitions
function demosphere_event_repetition_group_self_edit($groupId)
{
	global $user,$currentPage,$base_url;
	require_once 'demosphere-date-time.php';

	if(!$user->checkRoles('admin','moderator'))
	{
		require_once 'demosphere-self-edit.php';
		if(!demosphere_self_edit_check_group($groupId)){dlib_permission_denied_403();}
	}

	$group=RepetitionGroup::fetch($groupId,false);
	if($group===null){dlib_not_found_404();}

	$currentPage->addCssTpl('demosphere/css/demosphere-event-repetition.tpl.css');

	if($group->ruleType!=0){dlib_redirect($base_url.'/repetition-group/'.$group->id.'/confirm'.
										  (isset($_GET['event']) ? '?event='.intval($_GET['event']) : ''));}

	if(isset($_GET['event']))
	{
		$e=Event::fetch(intval($_GET['event']));
		if($e->repetitionGroupId!=$group->id){dlib_bad_request_400('event not in repetition group');}
		require_once 'demosphere-event.php';
		demosphere_event_tabs($e,'repetitions-self-edit');
	}

	$now=time();

	$events=Event::fetchList('WHERE repetitionGroupId=%d AND moderationStatus IN (%d,%d) AND startTime>%d ORDER BY startTime ASC',
							 $group->id,
							 Event::moderationStatusEnum('published'),
							 Event::moderationStatusEnum('incomplete'),
							 $now-3600*24*35
							 );

	require_once 'dlib/template.php';
	return template_render('templates/demosphere-event-repetition-group-self-edit.tpl.php',
						   [compact('group','events')]);
}

//! A form that allows event organizers to confirm dates for automatic repetitions (uses self-edit)
function demosphere_event_repetition_group_confirm($groupId)
{
	global $user,$currentPage,$base_url;
	require_once 'demosphere-date-time.php';
	require_once 'demosphere-self-edit.php';

	if(!$user->checkRoles('admin','moderator'))
	{
		if(!demosphere_self_edit_check_group($groupId)){dlib_permission_denied_403();}
	}


	$group=RepetitionGroup::fetch($groupId,false);
	if($group===null){dlib_not_found_404();}

	if($group->ruleType==0){dlib_redirect($base_url.'/repetition-group/'.$group->id.'/self-edit'.
										  (isset($_GET['event']) ? '?event='.intval($_GET['event']) : ''));}

	$currentPage->addCssTpl('demosphere/css/demosphere-event-repetition.tpl.css');

	if(isset($_GET['event']))
	{
		$e=Event::fetch(intval($_GET['event']));
		if($e->repetitionGroupId!=$group->id){dlib_bad_request_400('event not in repetition group');}
		require_once 'demosphere-event.php';
		demosphere_event_tabs($e,'repetitions-confirm');
	}


	$now=time();
	//$GLOBALS['dbgtime']=strtotime("oct 18 2015  17:00");
	////$GLOBALS['dbgtime']=time();
	//$now=$GLOBALS['dbgtime'];

	$events=Event::fetchList('WHERE repetitionGroupId=%d AND moderationStatus IN (%d,%d,%d,%d,%d) AND startTime>%d ORDER BY startTime ASC',
							 $group->id,
							 Event::moderationStatusEnum('waiting'),
							 Event::moderationStatusEnum('published'),
							 Event::moderationStatusEnum('incomplete'),
							 Event::moderationStatusEnum('rejected-shown'),
							 Event::moderationStatusEnum('rejected'),
							 $now
							 );

	$changedEvents=[];

	$items=[];
	$uncancelableDelay=3600*24*20;
	$hasUncancelable=false;
	foreach($events as $id=>$event)
	{
		if($event->status==1 && 
		   $event->getModerationStatus()==='published' && 
		   $event->startTime<$now+$uncancelableDelay){$hasUncancelable=true;continue;}
		$modStatus=$event->getModerationStatus();
		$items['event-'.$id]=
			['type'=>'checkbox',
			 'default-value'=>$modStatus!='rejected-shown' && $modStatus!=='rejected',
			 'wrapper-template'=>'$template',
			 'template'=>'<input type="checkbox" id="edit-$cname" class="$class" $attributes />',
			 'pre-submit'=>function($v,$item)use($now,&$changedEvents)
					{
						$event=Event::fetch($item['data-id']);
						$modStatus=$event->getModerationStatus();
						//if($event->id==41968){var_dump('v:',$v,'status:',$event->status,'modstatus:',$modStatus,'ok',($v && !$event->status) ||
						// 							   (!$v && ($event->status || $modStatus==='waiting')));die('pp');}
						if(( $v && (!$event->status || $modStatus!=='published')) ||
						   (!$v && ( $event->status || $modStatus==='waiting')))
						{
							if($v)
							{
								$event->status=1;
								$event->setModerationStatus('published');
								$event->moderationMessage=str_replace('automatic-repetition','',$event->moderationMessage);
								$event->moderationMessage=str_replace(t('canceled'),'',$event->moderationMessage);
								$event->logAdd('repetition confirmed');
								dlib_message_add(ent(t('The event on !date was confirmed.',
													   ['!date'=>demos_format_date('full-date-time',$event->startTime)])));
							}
							else
							{
								$event->status=0;
								$event->setModerationStatus('rejected');
								$event->moderationMessage=str_replace('automatic-repetition','',$event->moderationMessage);
								$event->moderationMessage=t('canceled').
									($event->moderationMessage!=='' ? ' ; '.$event->moderationMessage :'');
								$event->logAdd('repetition canceled');
								dlib_message_add(ent(t('The event on !date was canceled.',
													   ['!date'=>demos_format_date('full-date-time',$event->startTime)])));
							}
							//var_dump($event->id,$event->status,$event->getModerationStatus());
							//die('oo');
							$changedEvents[]=$event->id;
							$event->save();
						}

						// Update body message "Source : confirmed on ..."
						if($v && preg_match('@(<p [^p]*demosphere-source[^>]*>).*</p>@sU',$event->body,$m))
						{
							require_once 'htmledit/demosphere-htmledit.php';
							$source=preg_quote(strip_tags(t('_source_')),'@');
							$text=t('confirmation received on');
							$paragraph=$m[0];
							$paragraph=preg_replace('@'.$source.'\s*:\s*'.preg_quote($text,'@').'.*<br */>@sU','',$paragraph);
							$paragraph=str_replace($m[1],
												   $m[1].$source.': '.$text.' '.demos_format_date('approx-short-date-time',$now).'<br/>',
												   $paragraph);
							//var_dump($paragraph);die('pp');
							$body0=$event->body;
							$event->body=str_replace($m[0],$paragraph,$event->body);
							$event->body=demosphere_htmledit_cleanup_html($event->body);
							if($event->body!==$body0){$event->save();}
						}

					},
			 'data-id'=>$id,
			 ];
	}

	// message'=>
	// 						  ($lastSentEmail>0 ? 
	// 						   'Organizer has not confirmed after 2 mails. Last confirm was '.$nbDays.' days ago.' :
	// 						   'Organizer has never confirmed. 2 mails have been sent.'));

	$items['submit']=['type'=>'submit',
					  'value'=>t('Save'),
					  'submit'=>function()use($group,&$changedEvents)
			           { 
						   global $user;
						   if(count($changedEvents)===0){dlib_message_add(t('Successfully saved. No events where changed.'));}
						   $pass=demosphere_self_edit_check_group($group->id,true);
						   $selfEdit=demosphere_self_edit_get($pass);
						   $wasReEnabled=!$group->autoIsEnabled;
						   $group->autoIsEnabled=true;
						   $email=$selfEdit['email'] ?? false;
						   if($email===false){$email=$user->email;} // special case: a mod uses this form
						   $group->addLog(['type'=>'confirm','email'=>$email]);
						   $group->save();
						   if($wasReEnabled)
						   {
							   $group->autoCreateEvents();   
							   dlib_message_add(t('This repetition is enabled again. Please check for any new events below and confirm or cancel them.'));
						   }
					   },
					  ];

	if(time()<$group->ruleEnd || $group->ruleEnd==0)
	{
		$items['stop'  ]=['type'=>'submit',
						  'value'=>t('Stop'),
						  'submit'=>function()use($items,$group,&$changedEvents)
			              { 
							  $items['submit']['submit']();
							  $group->ruleEnd=time();
							  $group->save();
						  },
						 ];
	}

	if(!$group->autoIsEnabled)
	{
		$items['enable']=['type'=>'submit',
						  'value'=>t('Enable'),
						  'submit'=>function()use($items,$group,&$changedEvents)
			              { 
							  $items['submit']['submit']();
							  $group->ruleEnd=time();
							  $group->save();
						  },
						 ];
	}

	require_once 'dlib/form.php';
	$formOpts['id']='repetition-group-confirm';
	$formOpts['render-array']=true;
	$form=form_process($items,$formOpts);

	require_once 'dlib/template.php';
	return template_render('templates/demosphere-event-repetition-group-confirm.tpl.php',
						   [compact('group','events','form','hasUncancelable','uncancelableDelay')]);
}

//! Returns a list of timestamps for a repetition rule (daily, weekly, ...).
function demosphere_event_repetition_rule_dates($ruleType,$ruleData,$start,$end)
{
	require_once 'demosphere-date-time.php';
	$end=set_default_time($end,strtotime('23:59'));

	// set $start to time in $ruleData['time'] (new $start must be >= old $start)
	if(strpos($ruleData['time'],':')===false){fatal('demosphere_event_repetition_rule_dates: invalid $ruleData[time]');}
	$startT=strtotime($ruleData['time'],$start);
	if($startT>=$start){$start=$startT;}
	else               {$start=strtotime('+1 days',$startT);}

	if(isset($ruleData['dayOfWeek']))
	{
		$startDayOfWeek=strtotime($ruleData['dayOfWeek'].' '.date('H:i:s',$start),$start);
	}

	$dates=[];
	$ruleTypeName=ctype_digit((string)$ruleType) ? RepetitionGroup::$ruleType_['values'][$ruleType] : $ruleType;
	switch($ruleTypeName)
	{
	case 'none':return [];
	case 'daily': 
		for($ts=$start;$ts<=$end;$ts=strtotime('+1 days',$ts))
		{
			$dates[]=$ts;
		}
		return $dates;
	case 'weekly': 
		$start=$startDayOfWeek;
		for($ts=$start;$ts<=$end;$ts=strtotime('+7 days',$ts))
		{
			$dates[]=$ts;
		}
		return $dates;
	case 'monthly': 
		$start=$startDayOfWeek;
		$weeksInMonth=[$ruleData['weekInMonth']];
	case 'semimonthly': 
		$start=$startDayOfWeek;
		if(!isset($weeksInMonth)){$weeksInMonth=[$ruleData['weekInMonth1'],$ruleData['weekInMonth2']];}
		// Foreach week, check if it matches. If so, add to dates[]
		for($ts=$start;$ts<=$end;$ts=strtotime('+7 days',$ts))
		{
			// Find first weekday of month for the month of $ts
			$first=mktime(date("H",$ts),date("i",$ts),date("s",$ts),
						  date("n",$ts),1            ,date("Y",$ts));
			$ct=0;
			for(;date('w',$first)!=date('w',$ts);
				$first=strtotime('+1 day',$first))
			{
				if(($ct++)>7){fatal("argh1");}
			}

			// Find last weekday of month for the month of $ts
			$last=mktime(date("H",$ts)  ,date("i",$ts),date("s",$ts),
						 date("n",$ts)+1,1            ,date("Y",$ts));
			$ct=0;
			do
			{
				$last=strtotime('-1 day',$last);
				if(($ct++)>7){fatal("argh2");}
			}
			while(date('w',$last)!=date('w',$ts));

			// keep this date ($ts) if it matches the condition
			foreach($weeksInMonth as $weekInMonth)
			{
				switch($weekInMonth)
				{
				case 'last'  : if($ts==$last                       ){$dates[]=$ts;}break;	
				case 'first' : if($ts==$first					   ){$dates[]=$ts;}break;	
				case 'second': if($ts==strtotime('+7 days' ,$first)){$dates[]=$ts;}break;	
				case 'third' : if($ts==strtotime('+14 days',$first)){$dates[]=$ts;}break;	
				case 'fourth': if($ts==strtotime('+21 days',$first)){$dates[]=$ts;}break;	
				}
			}
		}
		return $dates;
	case 'dayofmonth':
		$startDay=mktime(date("H",$start),date("i",$start),date("s",$start),date("n",$start),$ruleData['dayInMonth'],date("Y",$start));
		$dates=[];
		for($monthOffset=0;;$monthOffset++)
		{
			$ts=strtotime('+'.$monthOffset.' months',$startDay);
			if($ts<$start){continue;}
			if($ts>$end  ){break;}
			// special case : example: 31 of month does not exist every month
			if(date('j',$ts)!=$ruleData['dayInMonth']){continue;}
			$dates[]=$ts;
		}
		return $dates;
	default: fatal('Invalid ruleTypeName');
	}
}


?>