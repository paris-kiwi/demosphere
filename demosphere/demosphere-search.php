<?php

const SEARCH_TYPES = ['event','mail','feed','comment'];

//! Displays a page with a search box, search options and search results.
//! Search can be performed on events, comments, email, and feed articles.
//! A separate Sphinx server is used to index events. The Sphinx server can be addressed through a proxy (for authentification on a multi-site setup)
//! Anon user and moderators see different search options and results.
function demosphere_search_page()
{
	global $currentPage,$base_url,$user;
	$isEventAdmin=$user->checkRoles('admin','moderator');

	require_once 'dlib/filter-xss.php';
	require_once 'demosphere-date-time.php';

	$currentPage->addCssTpl('demosphere/css/demosphere-search.tpl.css');

	$currentPage->title=t('Search');

	list($search,$options)=demosphere_search_parse_get_options($_GET);

	// Hack to force translation of search types
	$unused=[t('events'),t('mails'),t('feeds'),t('comments')];
	// Tell user which search types are included in main search results (displayed in pager)
	$includedTypes='';
	foreach(SEARCH_TYPES as $type){if($options['include-'.$type.'s']===true){$t=$type.'s';$includedTypes.=t($t).' + ';}}
	$includedTypes=preg_replace('@ \+ $@','',$includedTypes);
	$pager=new Pager(['itemName'=>$includedTypes,'defaultItemsPerPage'=>40,'shortDisplay'=>true]);

	$mainSearchDisplay=false;
	$foundDocs=false;
	$boxes=[];
	$nbMatches=false;
	if($search!=='')
	{
		try
		{
			// **** main search display (left column)
			$optionsLoc=$options;
			// in main display, don't include results that are in boxes
			if($optionsLoc['include-mails']==='box'){$optionsLoc['include-mails']=false;}
			if($optionsLoc['include-feeds']==='box'){$optionsLoc['include-feeds']=false;}
			$optionsLoc['limit' ]=(int)$pager->itemsPerPage;
			$optionsLoc['offset']=(int)$pager->firstItem;
			list($foundDocs,$nbMatches)=demosphere_search_results($search,$optionsLoc);
			$pager->nbItems=$nbMatches;
			$pager->checkOutOfBounds();
			$mainSearchDisplay=template_render('templates/demosphere-search-results.tpl.php',
											   [compact('foundDocs','pager','isEventAdmin','search'),
												['optionsLoc'=>$optionsLoc,'options'=>$options,'isBox'=>false]]);
			// If we don't fill the page, also show past/future event results 
			if($nbMatches<$pager->itemsPerPage && $optionsLoc['include-events'] && $optionsLoc['event-date']!=='all')
			{
				$optionsLoc['limit' ]=$pager->itemsPerPage-$nbMatches;
				$optionsLoc['offset']=0;
				$optionsLoc['event-date']=$optionsLoc['event-date']==='future' ? 'past' : 'future';
				$optionsLoc['order'     ]=$optionsLoc['event-date']==='future' ? 'date-asc' : 'date-desc';

				list($foundDocs,$nbMatches)=demosphere_search_results($search,$optionsLoc);
				$moreUrl=demosphere_search_url($search,$options,$override=['event-date'=>$optionsLoc['event-date']]);
				$mainSearchDisplay.='<hr/>'.
					'<h4 id="extra-search-title">'.
					  ent($optionsLoc['event-date']==='future' ? t('Also trying to search for future events:') : 
						                                         t('Also trying to search for past events:'));
				$mainSearchDisplay.=' <a href="'.ent($moreUrl).'">'.'['.count($foundDocs).'/'.intval($nbMatches).']</a>';
				$mainSearchDisplay.='</h4>';
				$mainSearchDisplay.=template_render('templates/demosphere-search-results.tpl.php',
												   [compact('foundDocs','isEventAdmin','search'),
													['optionsLoc'=>$optionsLoc,'options'=>$options,'isBox'=>false,'pager'=>null]]);
				if($nbMatches>count($foundDocs))
				{
					$mainSearchDisplay.='<p><a href="'.ent($moreUrl).'">'.t('more results...').' ['.count($foundDocs).'/'.intval($nbMatches).']</a></p>';
				}
			}

			// **** boxes on the right
			$boxes=[];
			foreach(['mail','feed'] as $boxType)
			{
				$box=[];
				$optionsLoc=$options;
				if($optionsLoc['include-'.$boxType.'s']!=='box'){continue;}
				// only include results for the type of this box
				$optionsLoc['include-events'  ]=false;
				$optionsLoc['include-mails'   ]=false;
				$optionsLoc['include-feeds'   ]=false;
				$optionsLoc['include-comments']=false;
				$optionsLoc['include-'.$boxType.'s']=true;
				$box['titleUrls']=demosphere_search_url($search,$optionsLoc,[],['limit','offset']);
				$optionsLoc['limit' ]=5;
				$optionsLoc['offset']=0;
				$optionsLoc['order']='date-desc';
				list($foundDocs,$box['nbMatches'])=demosphere_search_results($search,$optionsLoc);
				$box['searchResultsHtml']=template_render('templates/demosphere-search-results.tpl.php',
														  [compact('foundDocs','isEventAdmin','search'),
														   ['optionsLoc'=>$optionsLoc,'options'=>$options,'pager'=>null,'isBox'=>true]]);
				$boxes[$boxType]=$box;
			}
		}
		catch(Exception $e)
		{
			$mainSearchDisplay=false;
			// Note : this error is different for normal users and admin (debug)
			dlib_message_add('Search failed: '.ent($e->getMessage()),'error');
		}
	}

	// Add some dynamic information to help dialog for extended options
	if($isEventAdmin)
	{
		$schema=demosphere_search_proxy_call('demosphere_search_dx_describe');
		$help='';
		$help.='<p>Schema:</p>';
		$help.='<pre>'.print_r($schema,true).'</pre>';
		$help.='<p>Types:</p>';
		$help.='<pre>'.print_r(SEARCH_TYPES,true).'</pre>';

		$currentPage->addJs('function demosphere_help_hook(help,popup){'.
							'if(help.label==="extended-query-help"){popup.append('.dlib_json_encode_esc($help).');}}','inline');
	}

	return template_render('templates/demosphere-search-page.tpl.php',
						   [compact('search','isEventAdmin','options','mainSearchDisplay','nbMatches','boxes'),
						   ]);
}

//! Returns a URL for the search given by $options. 
//! For convenience, the values in $options can be changed by $override and $ignore.
function demosphere_search_url($search,array $options,array $override=[],array $ignore=[])
{
	global $base_url;
	$ignore=array_merge($ignore,['only-public-text']);
	$options=array_merge($options,$override);
	$url=$base_url.'/search?search='.urlencode($search).'&';
	foreach($options as $option=>$value)
	{
		if(count($ignore) && array_search($option,$ignore)!==false){continue;}
		$v=$value;
		if(is_bool($v)){$v=(int)$v;}
		$url.=$option.'='.urlencode($v).'&';
	}
	$url.='allset=1';
	return $url;
}

//! Renders a set of radio buttons
//! Example $name='edit-date' $option=['past'=>'Past event','future'=>'Future event'], $value='future' (currently selected value)
function demosphere_search_radio_buttons($name,array $options,$value)
{
	if(is_bool($value)){$value=(int)$value;}
	$radios='';
	$radios.='<span id="edit-'.ent($name).'" class="form-radios">';
	foreach($options as $key=>$label)
	{
		$radios.='<label class="form-radio-item '.str_replace('_','-',$name).'-'.ent($key).'">';
		$radios.='<input type="radio" name="'.ent($name).'" value="'.ent($key).'" '.
			((ctype_digit($key) ? (int)$key : $key)===$value ? 'checked="checked"' : '').'>';
		$radios.='<span>'.ent($label).'</span>';
		$radios.='</label>';
	}
	$radios.='</span>';
	return $radios;
}

//! Checks if a value is within a set of allowed values and returns value with correct type
//! Tries to find best match. Aborts if none found.
//! If it does not abort, it always returns one of the $allowedValues
function demosphere_search_checkval($v,array $allowedValues)
{
	if(is_string($v) && ctype_digit($v)){$v=(int)$v;}
	// first try strict search
	$f=array_search($v,$allowedValues,true);
	if($f!==false){return $allowedValues[$f];}
	// then try fuzzy search 
	// this can give unexpected results between strings an bools, but previous search should have caught valid string matches
	$f=array_search($v,$allowedValues);
	if($f!==false){return $allowedValues[$f];}
	dlib_bad_request_400('invalid value');
}

//! Parses information in $get (typically $_GET) and returns actual $search query and complete $options that include default values.
//! This function does security checks, returned options are safe to use. If security problems are found, fails with dlib_permission_denied_403
function demosphere_search_parse_get_options(array $get)
{
	global $demosphere_config,$user;
	$isEventAdmin=$user->checkRoles('admin','moderator');

	$search=trim(val($get,'search',''));
	$options['extended-query'  ]=demosphere_search_checkval(val($get,'extended-query'      ),[true,false]);
	$options['event-date'      ]=demosphere_search_checkval(val($get,'event-date','future' ),['past','future','all']);
	$options['event-status'    ]=demosphere_search_checkval(val($get,'event-status',$isEventAdmin ? 'all' : 1)     ,[0,1,'all']);
	$options['only-title'      ]=demosphere_search_checkval(val($get,'only-title'          ),[true,false]);

	// 'allset' is hidden input hack to distinguish "unset" from "unchecked"
	$options['include-events'  ]=demosphere_search_checkval(!isset($get['allset']) || (bool)val($get,'include-events'  ) || !$isEventAdmin,[true,false]);
	$options['include-comments']=demosphere_search_checkval(!isset($get['allset']) || (bool)val($get,'include-comments'),[true,false]);
	$options['include-mails'   ]=demosphere_search_checkval(val($get,'include-mails',$isEventAdmin ? 'box' : false),[true,false,'box']);
	$options['include-feeds'   ]=demosphere_search_checkval(val($get,'include-feeds',$isEventAdmin ? 'box' : false),[true,false,'box']);

	$options['comment-status'  ]=$isEventAdmin ? 'all' : 1;

	$isActive['mail'   ]=intval(db_result("SELECT MAX(dateFetched) FROM Message"))>strtotime("3 months ago");
	$isActive['feed'   ]=intval(db_result("SELECT MAX(lastFetched) FROM Article"))>strtotime("3 months ago");
	$isActive['comment']=$demosphere_config['comments_site_settings']!=='disabled';
	if(!$isActive['mail'    ]){$options['include-mails'   ]=false;}
	if(!$isActive['feed'    ]){$options['include-feeds'   ]=false;}
	if(!$isActive['comment' ]){$options['include-comments']=false;}
	
	// For now, order is determined automatically. We might want to add a manual option for this.
	$options['order']='date-desc';
	if(($options['include-events'  ] || $options['include-comments']) && $options['event-date']==='future')
	{
		$options['order']='date-asc';
	}

	// remove extended search special chars from ordinary query
	if(!$options['extended-query'])
	{
		if(substr_count($search,'"')%2)
		{
			$search=str_replace('"',' ',$search);
			dlib_message_add(t("You must use an even number of quotes."),'error');
		}
		$search=preg_replace('@[ \t\r\n]+@',' ',$search);
		// copied from SphinxClient::EscapeString and removed double quotes
		$from=[  '\\', '(' ,')', '|', '-', '!', '@', '~', '&', '/', '^', '$', '=', '<'];
		$to  =['\\\\','\(','\)','\|','\-','\!','\@','\~','\&','\/','\^','\$','\=','\<'];
		$search=str_replace($from,$to,$search);
	}

	if($options['extended-query'] && $options['only-title']){dlib_bad_request_400('"extended-query" option is incompatible with "only-title"');}

	if(!isset($options['offset'])){$options['offset']=0;}
	if(!isset($options['limit' ])){$options['limit' ]=10;}

	//*** Finish with security checks

	if(!$isEventAdmin)
	{
		if($options['event-status'  ]!==1     ||
		   $options['comment-status']!==1     ||
		   $options['include-mails' ]!==false ||
		   $options['include-feeds' ]!==false ||
		   $options['extended-query']!==false    )
		{
			dlib_permission_denied_403();
		}
	}

	// whether to search public texts or private texts
	$options['only-public-text']=!$isEventAdmin;

	return [$search,$options];
}


//! Returns a list containing information about each document that matched a given search.
//! Also returns the total number of matches (can be much larger than the nb of documents, when using "limit/offset").
//! Each document contains: the document's type, the actual object (Event, Message,...), snippets 
function demosphere_search_results($search,array $options)
{
	require_once 'dcomponent/dcomponent-common.php';
	require_once 'demosphere-date-time.php';
	$types=array_flip(SEARCH_TYPES);

	if(!$options['extended-query'])
	{
		// When we get here, $search is safe (special chars are escaped)
		if($options['only-title'])
		{
			$search='@title '.$search;
		}
		else
		if($options['only-public-text'])
		{
			// Typically, anon users cannot search the private text field. They can search other fields : title and public_text
			$search='@!(private_text) '.$search;
		}
	}

	//***** First step : actual search queries

	list($foundDocIds,$nbMatches)=demosphere_search_proxy_call('demosphere_search_dx_search',$search,$options);
	if(count($foundDocIds)===0){return [[],$nbMatches];}
	
	// ** Fetch Event's,Message's,Article's...  from DB
	// First regroup ids by type
	$foundIdsByType=[];
	foreach(SEARCH_TYPES as $type){$foundIdsByType[$type]=[];}
	foreach($foundDocIds as $doc ){$foundIdsByType[SEARCH_TYPES[$doc[0]]][]=(int)$doc[1];}
	// now we can actually fetch lists of objects of same type from DB
	$foundByType=[];
	foreach($foundIdsByType as $type=>$ids)
	{
		$classes=['event'=>'Event','mail'=>'Message','feed'=>'Article','comment'=>'Comment'];
		$class=$classes[$type];
		$foundDocsByType[$type]=$class::fetchListFromIds($ids,false);
	}
	// Now regroup into $foundDocs, in search result order defined by $foundDocIds
	$foundDocs=[];
	foreach($foundDocIds as $doc)
	{
		$type=SEARCH_TYPES[$doc[0]];
		if(isset($foundDocsByType[$type][$doc[1]]))
		{
			$foundDocs[]=['type'=>$type,
						  'obj'=>$foundDocsByType[$type][$doc[1]]
						 ];
		}
	}

	// Security double-check : make sure user is allowed to see each result
	foreach(array_keys($foundDocs) as $docKey)
	{
		$type=$foundDocs[$docKey]['type'];
		$obj =$foundDocs[$docKey]['obj'];
		$bad=false;
		if($options['event-status'   ]===1     && $type=='event'   && $obj->status     !=1){$bad=true;}
		if($options['comment-status' ]===1     && $type=='comment' && $obj->isPublished!=1){$bad=true;}
		if($options['include-mails'  ]===false && $type=='mail'                           ){$bad=true;}
		if($options['include-feeds'  ]===false && $type=='feed'                           ){$bad=true;}
		if($bad){unset($foundDocs[$docKey]);}
	}


	//***** Second step : sphinx queries for snippets (=excerpts) 

	$snippetOptions0 = [
		'before_match'          => '<strong>',
		'after_match'           => '</strong>',
		'chunk_separator'       => ' ... ',
		'limit'                 => 170,
		'around'                => 5,
					   ];

	foreach(['title','publicText','privateText','extra'] as $snippetType)
	{
		$snippetOptions=$snippetOptions0;
		if($snippetType==='title'){$snippetOptions['limit']=300;}
		
		// Build a list of texts that need snippets
		$texts=[];
		foreach($foundDocs as $k=>$foundDoc)
		{
			$obj =$foundDoc['obj'];
			$type=$foundDoc['type'];

			// most of the time, we just need snippets for the actually indexed text fields (title,publicText,privateText)
			if($foundDoc['type']==='comment'){$vals=demosphere_search_reindex_comment($obj,true);}
			else                             {$vals=$obj->searchReindex(true);}

			// special cases
			$special=[];
			if(($type==='event' || $type==='comment') && $snippetType==='privateText')
			{
				// events and comments have a list of info in privateText. Split them to get separate snippets.
				$special=explode('...',$vals['privateText']);
			}
			if($type==='mail' && $snippetType==='extra')
			{
				$special[]=$obj->from;
			}
			if($type==='event' && $snippetType==='extra')
			{
				$special[]=demos_format_date('full-date-time',$obj->startTime);
			}
			
			if(count($special))
			{
				$texts=array_merge($texts,$special);
				$nbSnippetsForDoc[$k]=count($special);
			}
			else 
			if($snippetType==='extra'){$nbSnippetsForDoc[$k]=0;}
			else
			{
				$texts[]=$vals[$snippetType];
				$nbSnippetsForDoc[$k]=1;
			}
		}
		// All snippets are plain text, and Sphinx will add html tags, so we need to transform the plain text to html (use entities).
		$texts=array_map('ent',$texts);
		// now actually ask Sphinx to compute snippets for all texts of this $snippetType
		$snippets=demosphere_search_proxy_call('demosphere_search_dx_snippets',$texts,$search,$snippetOptions);
		// Put the snippets back into each document
		$ct=0;
		foreach(array_keys($foundDocs) as $k)
		{
			for($i=0;$i<$nbSnippetsForDoc[$k];$i++)
			{
				$foundDocs[$k]['snippets'][$snippetType][]=$snippets[$ct++];
			}
		}
	}

	return [$foundDocs,$nbMatches];
}

//! Small form that allows admin to reindex all documents or only one type of document (event, mail, feed, comment)
//! The actual work is done in demosphere_search_rebuild_index()
function demosphere_search_rebuild_index_form()
{
	$items=[];
	$items['top']=['html'=>'<h4>Reindex only one type:</h4>'];
	foreach(SEARCH_TYPES as $type)
	{
		$items[$type]=['type'=>'submit',
					   'value'=>$type,
					   'submit'=>function($items,$activeButton){demosphere_search_rebuild_index($activeButton);}];
	}
	$items['bot']=['html'=>'<h4>Reindex all types:</h4>'];
	$items['all']=['type'=>'submit',
				   'value'=>'all',
				   'submit'=>function(){demosphere_search_rebuild_index();}];

	require_once 'dlib/form.php';
	return form_process($items);
}

//! Rebuild the whole index or optionally the index for a single type of document  (event, mail, feed, comment)
//! This can be slow (around a minute for a large site).
function demosphere_search_rebuild_index($selectType=false)
{
	require_once 'demosphere-date-time.php';
	require_once 'dcomponent/dcomponent-common.php';

	$classes=['event'=>'Event','mail'=>'Message','feed'=>'Article','comment'=>'Comment'];
	if($selectType!==false)
	{
		if(!isset($classes[$selectType])){throw new Error('demosphere_search_rebuild_index : invalid type');}
		$classes=[$selectType=>$classes[$selectType]];
	}

	demosphere_search_proxy_call('demosphere_search_dx_delete_all',$selectType);

	$start=microtime(true);

	// Avoid PHP timeout
	// 2018/11 :  47250 mails in 26 minutes on dev machine but only 5min on server (?!),  59486 events in 3.7 min
	ini_set('max_execution_time', max(3600,ini_get('max_execution_time')));

	$ct=0;
	foreach($classes as $type=>$class)
	{
		// $sqlCondition: big speedup for feed import
		$sqlCondition='1';
		if($class==='Article'){$sqlCondition="content!='' ";}

		// Don't load more than 100 documents at once (avoid out of mem errors)
		$docBatch=100;
		$nbDocs=db_result('SELECT COUNT(*) FROM '.$class.' WHERE '.$sqlCondition);
		for($i=0;$i<$nbDocs;$i+=$docBatch)
		{
			$docs=$class::fetchList("WHERE ".$sqlCondition." LIMIT %d OFFSET %d",$docBatch,$i); 
			foreach($docs as $doc)
			{
				if($type==='comment'){demosphere_search_reindex_comment($doc,false,'throw');}
				else
				{
					$doc->searchReindex(false,'throw');
				}
				$ct++;
			}
			// memory leak in imap_fetchbody()
			if($class==='Message')
			{
				mail_import_imap_get_connection(false,$x,true);
				Message::cleanupCachedFiles(true);
			}
			// not really necessary. just in case
			gc_collect_cycles(); 
		}
	}

	$end=microtime(true);
	dlib_message_add("Reindexed all $ct ".($selectType===false ? 'documents' : $selectType.'s')." in ".($end-$start)."s");
}

//! The Comment class does not have a searchReindex() method (unlike Event, Message, Feed), so we need a separate reindex function.
function demosphere_search_reindex_comment(Comment $comment,$justReturnVals=false,$error='log')
{
	// only handle comments on events (not on posts)
	if($comment->pageType!=='Event'){return;}

	$title='';

	$publicText =$comment->body;

	$privateTexts=[];
	$privateTexts[]=$comment->ip;
	$user=null;
	if($comment->userId){$user=User::fetch($comment->userId,false);}
	if($user!==null)
	{
		$privateTexts[]=$user->login;
		$privateTexts[]=$user->email;
		$privateTexts[]=$user->lastIp;
	}
	$privateText=implode('...',$privateTexts);

	if($justReturnVals){return compact('title','publicText','privateText');}

	demosphere_search_proxy_call_catch('demosphere_search_dx_reindex_document',$error,
								 $comment->id,$title,$publicText,$privateText,'comment',$comment->created,$comment->isPublished);
}

//! Called monthly, can be very slow
function demosphere_search_cron()
{
	demosphere_search_rebuild_index();
}

//**************************************************************************************
//** Functions that can be called either (d)irectly or e(x)ternally through Sphinx proxy
//**************************************************************************************

//! Search for events using sphinxql query.
//! This function should be called using demosphere_search_proxy_call()
//! note: $options must be considered as untrusted for sql injections
function demosphere_search_dx_search($search,array $options)
{
	$types=array_flip(SEARCH_TYPES);
	$sphinxdb=demosphere_search_pdx_sphinxdb();
	//vd($options);

	// avoid strange query if no document types are requested
	if($options['include-events'  ]==0 && 
	   $options['include-mails'   ]==0 &&
	   $options['include-feeds'   ]==0 &&
	   $options['include-comments']==0   ){throw new Exception('No events, mails, feeds or comments included in search');}

	// Similar to Event::today() (that is not available on proxy)
	$today=strtotime('Today 0:00');
	if((time()-$today)<3600*4){$today=strtotime('-1 days',$today);}

	// Two step: first do query COUNT(*) to get nb matches (without limit and offset), 
	// then do actual search query to get document ids (using limit and offset).
	foreach(['nbMatches','search'] as $step)
	{
		//*** SELECT
		if($step==='nbMatches'){$q="SELECT COUNT(*) ";}
		else                   {$q="SELECT id "      ;}

		// Sphinx does not allow complex conditions in "WHERE" clause. 
		// We have to put them in SELECT clause and use that condition in WHERE
		$q.=', ';
		if($options['include-events'])
		{
			$q.=' (type='.$types['event'].' ';
			if($options['event-date'  ]!=='all'){$q.=' AND date'.($options['event-date']==='past' ? '<':'>').$today;}
			if($options['event-status']!=='all'){$q.=' AND status='.intval($options['event-status']);}
			$q.=') OR ';
		}
		if($options['include-mails'   ]){$q.=' ( type='.$types['mail'   ].' ) OR ';}
		if($options['include-feeds'   ]){$q.=' ( type='.$types['feed'   ].' ) OR ';}
		if($options['include-comments'])
		{
			$q.=' ( type='.$types['comment'];
			if($options['event-date'    ]!=='all'){$q.=' AND date'.($options['event-date']==='past' ? '<':'>').$today;}
			if($options['comment-status']!=='all'){$q.=' AND status='.intval($options['comment-status']);}
			$q.=' ) OR ';
		}
		$q=substr($q,0,-3);
		$q.=' AS condition ';

		//*** FROM
		$q.="FROM ".demosphere_search_pdx_index();

		//*** WHERE
		$q.=" WHERE MATCH('".mysqli_real_escape_string($sphinxdb,$search)."') ";
		$q.=' AND condition=1 ';
		if($step==='search' && $options['order']==='date-asc' ){$q.=' ORDER BY date ASC  ';}
		if($step==='search' && $options['order']==='date-desc'){$q.=' ORDER BY date DESC ';}

		//*** LIMIT/OFFSET
		if($step==='search' && ($options['limit']!==false || $options['offset']!==false))
		{
			$q.=' LIMIT '.
				($options['offset']!==false ? intval($options['offset']).',' : '').
				($options['limit' ]!==false ? intval($options['limit' ]) : 10000000);

			// Special case: Sphinx refuses big offsets. We need to set max_matches option
			$defaultMaxMatches=1000;
			$maxNeeded=$options['offset']+$options['limit']+100;
			if($maxNeeded>$defaultMaxMatches)
			{
				$q.=' OPTION max_matches='.intval($maxNeeded).' ';
			}
		}

		// The actual query 
		//vd($q);
		$sres=demosphere_search_pdx_sphinx_query($q);

		if($step==='nbMatches'){$nbMatches=(int)mysqli_fetch_array($sres,MYSQLI_NUM)[0];}
		else
		{
			$res=[];
			while($data=mysqli_fetch_array($sres,MYSQLI_NUM))
			{
				$res[]=demosphere_search_pdx_type_id((int)$data[0],true);
			}
		}
	}
	
	return [$res,$nbMatches];
}

//! Creates snippets for each string in texts by using Sphinx "CALL SNIPPETS(...)"
function demosphere_search_dx_snippets(array $texts,$search,array $options)
{
	$sphinxdb=demosphere_search_pdx_sphinxdb();
	
	if(count($texts)===0){return [];}

	// *** Build list of strings
	$qText='';
	foreach($texts as $text)
	{
		$qText.="'".mysqli_real_escape_string($sphinxdb,$text)."',";
	}
	if(substr($qText,-1)===','){$qText=substr($qText,0,-1);}

	// *** Build list of options
	$allowedOptions=[
		'before_match'          => 'string',
		'after_match'           => 'string',
		'chunk_separator'       => 'string',
		'limit'                 => 'int',
		'around'                => 'int',
					];
	$optionsQuery='';
	foreach($options as $option=>$value)
	{
		if(!isset($allowedOptions[$option])){throw new Exception('demosphere_search_pdx_sphinxdb: invalid option');}
		if($allowedOptions[$option]==='string')
		{
			$optionsQuery.="'".mysqli_real_escape_string($sphinxdb,$value)."'";
		}
		else
		if($allowedOptions[$option]==='int')
		{
			$optionsQuery.=intval($value);
		}
		$optionsQuery.=' AS '.$option;
		if($option!==dlib_last_key($options)){$optionsQuery.=', ';}
	}
	
	// *** Build query and do it.
	$q="CALL SNIPPETS((".$qText."),'".demosphere_search_pdx_index()."','".mysqli_real_escape_string($sphinxdb,$search)."'".
		($optionsQuery!=='' ? ','.$optionsQuery : '').")";
	$sres=demosphere_search_pdx_sphinx_query($q);

	// *** Build results
	$res=[];
	while ($data=mysqli_fetch_array($sres,MYSQLI_NUM))
	{
		$res[]=$data[0];
	}
	
	return $res;
}

function demosphere_search_dx_delete_document($type,$typeId)
{
	list($typeNb,$docId)=demosphere_search_pdx_doc_id($type,$typeId);
	demosphere_search_pdx_sphinx_query('DELETE FROM '.demosphere_search_pdx_index().' WHERE id='.intval($docId));
}

function demosphere_search_dx_delete_documents($type,array $typeIds)
{
	if(count($typeIds)===0){return;}

	$docIds=demosphere_search_pdx_doc_ids($type,$typeIds);
	demosphere_search_pdx_sphinx_query('DELETE FROM '.demosphere_search_pdx_index().' WHERE id IN ('.implode(',',$docIds).')');
}

//! Add or replace a document in Sphinx index.
//! Arguments (attributes and text fields) are in the order actually used by Sphinx
function demosphere_search_dx_reindex_document($typeId,$title,$publicText,$privateText,$type,$date,$status)
{
	list($typeNb,$docId)=demosphere_search_pdx_doc_id($type,$typeId);

	$sphinxdb=demosphere_search_pdx_sphinxdb();
	$q="REPLACE INTO ".demosphere_search_pdx_index()." VALUES ( ".
		intval($docId).", ".
		"'".mysqli_real_escape_string($sphinxdb,$title)."', ".
		"'".mysqli_real_escape_string($sphinxdb,$publicText)."', ".
		"'".mysqli_real_escape_string($sphinxdb,$privateText)."', ".
		intval($typeNb).", ".
		intval($date).", ".
		intval((bool)$status)." ".
		" )";
	$sres=demosphere_search_pdx_sphinx_query($q);
}

function demosphere_search_dx_delete_all($type=false)
{
	if($type===false)
	{
		demosphere_search_pdx_sphinx_query('TRUNCATE RTINDEX '.demosphere_search_pdx_index());
	}
	else
	{
		demosphere_search_pdx_sphinx_query('DELETE FROM '.demosphere_search_pdx_index().
										  ' WHERE type='.(array_flip(SEARCH_TYPES)[$type]));
	}
}

//! Returns schema of Sphinx index
function demosphere_search_dx_describe()
{
	$res=[];
	$sres=demosphere_search_pdx_sphinx_query('DESCRIBE '.demosphere_search_pdx_index());
	while ($data=mysqli_fetch_array($sres,MYSQLI_NUM))
	{
		$res[$data[0]]=$data[1];
	}
	return $res;
}


//! Updates status for a list of documents. (used by Feed::finishArticles())
function demosphere_search_dx_set_documents_status($type,array $typeIds,$status)
{
	if(count($typeIds)===0){return;}
	$docIds=demosphere_search_pdx_doc_ids($type,$typeIds);

	$res=[];
	demosphere_search_pdx_sphinx_query('UPDATE '.demosphere_search_pdx_index().
									  ' SET status='.intval($status).' WHERE id IN ('.implode(',',$docIds).')');
}

//**************************************************************************************
//*************** Utility functions  (pdx : private dx : not callable remotely)
//**************************************************************************************

//! Sanity check to avoid common mistake.
function demosphere_search_pdx_sanity_check()
{
	global $isSphinxProxyServer,$demosphere_config;
	if(!isset($isSphinxProxyServer) && $demosphere_config['use_proxy_for_sphinx_search'])
	{
		throw new Exception('Invalid direct call to demosphere_search_pdx_... function. You must use demosphere_search_proxy_call()');
	}
}


//! Returns the name of the index (Sphinx table) for this site.
//! Example: returns "paris" for paris.demosphere.net
function demosphere_search_pdx_index()
{
	global $demosphere_config;
	demosphere_search_pdx_sanity_check();
	return str_replace('-','_',$demosphere_config['site_id']);
}

//! Returns a connection to the SphinxQl server.
//! Note: the connection has a limited lifespan
function demosphere_search_pdx_sphinxdb()
{
	global $isSphinxProxyServer;
	static $sphinxdb=false;
	static $connectionTime=false;

	demosphere_search_pdx_sanity_check();

	// Sphinx default client_timeout = 5 minutes, so we need to reconnect often
	if($sphinxdb!==false && time()>$connectionTime+60)
	{
		$ok=@mysqli_close($sphinxdb);
		if(!$ok)
		{
			// This is not a fatal error, so just generate a log entry or a warning
			if(isset($isSphinxProxyServer)){fwrite(STDERR,date('r').": WARNING: Proxy could not close connection to Sphinx server.\n");}
			else                           {dlib_message_add('Could not close connection to Sphinx server.','warning');}
		}
		$sphinxdb=false;
		$connectionTime=false;
	}
	if($sphinxdb===false)
	{
		$sphinxdb=@mysqli_connect(null,'root','','',-1,'/run/sphinxsearch/sphinxql.sock');
		if(!$sphinxdb)
		{
			throw new Exception((isset($isSphinxProxyServer) ? 'Proxy could not connect to Sphinx server: ' : 'Could not connect directly to Sphinx server: ').
								mysqli_connect_errno().' : '.mysqli_connect_error());
		}
		$connectionTime=time();
	}
	return $sphinxdb;
}

//! Execute a SQL/SphinxQl query 
function demosphere_search_pdx_sphinx_query($q)
{
	global $dlib_config,$isSphinxProxyServer;
	$sphinxdb=demosphere_search_pdx_sphinxdb();
	$qres=mysqli_query($sphinxdb,$q);
	if($qres===false)
	{
		if(isset($isSphinxProxyServer)){echo $q."\n";}
		else
		if($dlib_config['debug']){echo '<pre>'.ent($q).'</pre>';}
		throw new Exception('sphinxql query failed: '.mysqli_error($sphinxdb));
	}
	return $qres;
}

//! Return $typeNb and $docId (used by Sphinx) from $type and $typeId (used by Demosphere)
//! Example: $type='feed', $typeId=123 => $typeNb=1, $docId=200000123
function demosphere_search_pdx_doc_id($type,$typeId)
{
	$typeNb=array_search($type,SEARCH_TYPES);
	if($typeNb===false){throw new Exception("demosphere_search_pdx_doc_id: invalid type");}
	return [$typeNb,100000000*$typeNb+(int)$typeId];
}

//! Same as demosphere_search_pdx_doc_id() but works on a list of $typeIds
function demosphere_search_pdx_doc_ids($type,array $typeIds)
{
	$typeNb=array_search($type,SEARCH_TYPES);
	if($typeNb===false){throw new Exception("demosphere_search_pdx_doc_id: invalid type");}
	$res=[];
	foreach($typeIds as $typeId){$res[]=100000000*$typeNb+intval($typeId);}
	return $res;
}

//! Return $type and $typeId (used by Demosphere) from $typeNb and $docId (used by Sphinx)
//! Example: $typeNb=1, $docId=200000123 => $type='feed', $typeId=123
function demosphere_search_pdx_type_id($docId,$retNb=false)
{
	$type=(int)($docId/100000000);
	return [$retNb ? $type : SEARCH_TYPES[$type],$docId%100000000];
}


//**************************************************************************************
//*************** Communication with Sphinx proxy
//**************************************************************************************

//! Call a function either directly (ordinary function call) or externally, via a Sphinx search proxy.
//! Sphinx does not provide authentification. This is a problem on servers with several untrusted sites.
//! In that case, direct sphinx queries are prohibited (using unix domain socket file permissions) and an authenticating proxy is used.
//! The proxy allows "remote" (external) function calls for all demosphere_search_dx_... function in this file.
function demosphere_search_proxy_call()
{
	global $demosphere_config,$dlib_config;
	$args=func_get_args();
	$fctName=array_shift($args);

	//$demosphere_config['use_proxy_for_sphinx_search']=false;// debug
	try
	{
		if(!$demosphere_config['use_proxy_for_sphinx_search'])
		{
			// Simple case: no proxy, just call function
			return call_user_func_array($fctName,$args);
		}
		else
		{
			// Connect to proxy via unix domain socket
			$proxy=demosphere_search_proxy_connect();
			$call=
				[
					'sitename'=>$demosphere_config['site_id'],
					// Use the same auth token that is used for websocket auth. 
					// This is a secret, generated "password" used to prove that we are actually "sitename".
					'unhashedToken'=>hash('sha256','websocket-token:'.$demosphere_config['secret']),
					'function'=>$fctName,
					'args'=>$args,
				];
			$msg=json_encode($call);
			if($msg===false){throw new Exception("demosphere_search_proxy_call: failed encoding json");}
			// Actually ask the proxy to execute the function
			if(demosphere_search_proxy_write($proxy,$msg)===false){throw new Exception("demosphere_search_proxy_call: write to proxy failed");}
			// Read proxy's answer
			$responseRaw=demosphere_search_proxy_read($proxy);
			if($responseRaw===false){throw new Exception("demosphere_search_proxy_call: read from proxy failed");}
			socket_close($proxy);
			$response=json_decode($responseRaw,true);
			if($response===null           ){throw new Exception("demosphere_search_proxy_call: failed decoding json response");                              }
			if( isset($response['error']) ){throw new Exception("demosphere_search_proxy_call: external function call returned error : ".$response['error']);}
			// FIXME isset(null)
			//if(!isset($response['return'])){throw new Exception("demosphere_search_proxy_call: missing return value for external function call");            }
			return $response['return'];
		}
	}
	catch(Exception $e) 
	{
		// only admins/test can see full errors
		if($dlib_config['debug']){throw $e;}
		else
		{
			throw new Exception('Search system failed, please contact admin.');
		}
	}
}

//! Wrapper around demosphere_search_proxy_call(), with extra arg (after fct name) for error management.
function demosphere_search_proxy_call_catch()
{
	$args=func_get_args();
	$fctName=array_shift($args);
	$errorManagement=array_shift($args);
	array_unshift($args,$fctName);
	
	try
	{
		return call_user_func_array('demosphere_search_proxy_call',$args);
	}
	catch(Exception $e) 
	{
		switch($errorManagement)
		{
			case 'log':dlib_log($e->getMessage());break;
			default:throw($e);
		}
	}
}

//! Connects to Sphinx proxy and returns a socket.
function demosphere_search_proxy_connect()
{
	$proxySocket=@socket_create(AF_UNIX,SOCK_STREAM,0);
	if($proxySocket===false) 
	{
		throw new Exception('socket_create() failed : '.socket_strerror(socket_last_error()));
	}

	$socketFile='/run/demosphere-sphinx-proxy/demosphere-sphinx-proxy.sock';
	if(@socket_connect($proxySocket,$socketFile) === false) 
	{
		throw new Exception('Proxy server is probably down. Error: socket_connect() failed : '.socket_strerror(socket_last_error()));
	}
	return $proxySocket;
}


//! Read data from a socket. Starts reading a 12 byte string that contains the data length.
//! This function is used both by server (proxy) and client (Demosphere site).
function demosphere_search_proxy_read($msgsock)
{
	// *** First read the data's length
	$length=@socket_read($msgsock,12);
	if($length===false)
	{
		throw new Exception("socket_read() failed (for length): reason: ".socket_strerror(socket_last_error($msgsock)));
	}
	if($length==='')
	{
		throw new Exception("invalid empty read");
	}
	if(strlen($length)!==12)
	{
		throw new Exception("socket_read() invalid length");
	}
	$length=(int)$length;

	// *** Now that we know its length, we can read the actual data.
	// Since the data can be chopped into several chunks, we need to loop.
	$res='';
	while(true)
	{
		$buffer=@socket_read($msgsock,$length-strlen($res));
		if($buffer===false)
		{
			throw new Exception("socket_read() failed: reason: ".socket_strerror(socket_last_error($msgsock))."");
		}
		$res.=$buffer;
		if(strlen($res)===$length){return $res;}
		if($buffer==='')
		{
			throw new Exception("socket read. strange: missing bytes");
		}
	}
}


//! Write data into socket. 
//! Starts with a 12 byte string that contains the data length (in ascii).
//! This function is used both by server (proxy) and client (Demosphere site).
function demosphere_search_proxy_write($msgsock,$data)
{
	// *** First write 12 byt string with data length
	$data=substr(strlen($data).'            ',0,12).$data;
	// *** Then write the actual data
	// Since socket_write() might chop the data into several chunks, we need to loop until it is all sent.
	while(true)
	{
		$written=@socket_write($msgsock,$data,strlen($data));
		if($written===false)
		{
			throw new Exception("socket_write() failed while sending answer to client: reason: ".socket_strerror(socket_last_error($msgsock))."");
			return false;
		}
		$data=substr($data,$written);
		if(strlen($data)===0){return true;}
		// Just in case. Not sure if this ever really happens.
		if($written===0){fwrite(STDERR,"pause: wrote 0 bytes to client");sleep(1);}
	}
}
