<?php

require_once 'test-demosphere.php';

function test_demosphere_apache_config()
{
	global $base_url;
	$paths=['favicon.ico'=>true,
			'robots.txt'=>true,
			'.gitignore'=>false,
			'.git'=>false,
			'lib/jquery.js'=>true,
			'demosphere/demosphere-setup.php'=>false,
			'demosphere/demosphere-setup.php'=>false,
			'demosphere/css/images/edit.png'=>true,
			'demosphere/css/images/waiting.gif'=>true,
			'demosphere/mail-import/mail-import.php'=>false,
			'demosphere/mail-import/mail-import.tpl.css'=>false,
			'dlib/array-of-arrays.css'=>true,
			'dlib/database.php'=>false,
			'dlib/dlib'=>false,
			'dlib/edit.png'=>true,
			'dlib/find-date-time/find-date-time.php'=>false,
			'demosphere/install-directories/files/images/favicon.ico'=>true,
			'demosphere/install-directories/files/images/README.txt'=>false,
			'files/maps/marker.png'=>true,
			'files/maps/README.txt'=>false,
			'files/private/demosphere-general.log'=>false,
		   ];
	require_once 'dlib/download-tools.php';
	foreach($paths as $path=>$expected)
	{
		$url=$base_url.'/'.$path;
		//echo $url."\n";
		$opts=['headers'=>&$headers,'info'=>&$info];
		$res=dlib_curl_download($url,false,$opts);
		$expectedStatus=$expected;
		if($expectedStatus===true ){$expectedStatus=200;}
		else
		if($expectedStatus===false){$expectedStatus=403;}

		test_equals($info['status'],$expectedStatus);

		//var_dump($url,$opts,$res);
		//die("\n");
	}

}


?>