<?php

require_once 'test-demosphere.php';

function test_demosphere_htmledit()
{
	global $dlib_config,$base_url,$demosphere_config;
	$base_path=preg_replace('@^https?://[^/]*@','',$base_url).'/';
	$base_root=dlib_host_url($base_url);

	require_once 'htmledit/demosphere-htmledit.php';

	//***** text URL inside <a> link

	test_equals(ns(demosphere_htmledit_cleanup_html('<p> abd <a href="https://aaa.bbb">https://aaa.bbb</a> def </p>')),
				ns('<p> abd <a href="https://aaa.bbb">https://aaa.bbb</a> def </p>'));


	test_equals(ns(demosphere_htmledit_cleanup_html('<p> abd <a href="https://aaa.bbb">https://aaa.bbb</a> def </p>')),
				ns('<p> abd <a href="https://aaa.bbb">https://aaa.bbb</a> def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p> abd <a href="https://aaa.bbb">https://xxx.bbb</a> def </p>')),
				ns('<p> abd <a href="https://aaa.bbb">https://aaa.bbb</a> def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p> abd <a href="https://www.aaa.bbb">www.aa...</a> def </p>')),
				ns('<p> abd <a href="https://www.aaa.bbb">www.aaa.bbb</a> def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p> abd <a href="https://www.aaa.bbb">www.aaa...</a> def </p>')),
				ns('<p> abd <a href="https://www.aaa.bbb">www.aaa.bbb</a> def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p> abd <a href="https://aaa.bbb">12 https://aaa.bbb 34 https://xxx.bbb 56</a> def </p>')),
				ns('<p> abd <a href="https://aaa.bbb">12 https://aaa.bbb 34 https://aaa.bbb 56</a> def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p> abd <a href="https://aaa.bbb">12 https://aaa.bbb 34 https://aaa.bbbzzz 56</a> def </p>')),
				ns('<p> abd <a href="https://aaa.bbb">12 https://aaa.bbb 34 https://aaa.bbb zzz 56</a> def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p> abd <a href="https://aaa.bbb/012345678901234567890123456789">12 https://aaa.bbb 34</a> def </p>')),
				ns('<p> abd <a href="https://aaa.bbb/012345678901234567890123456789">12 https://aaa.bbb/01234567890123456789012345678… 34</a> def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p> abd <a href="https://aaa.bbb/012345678901234567890123456789">12 https://aaa.bbb/01234567890123456789012345678… 34</a> def </p>')),
				ns('<p> abd <a href="https://aaa.bbb/012345678901234567890123456789">12 https://aaa.bbb/01234567890123456789012345678… 34</a> def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p> abd <a href="https://aaa.bbb/x%20y">https://xxx.bbb</a> def </p>')),
				ns('<p> abd <a href="https://aaa.bbb/x%20y">https://aaa.bbb/x y</a> def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p> abd <a href="https://aaa.bbb/x%20y">https://aaa.bbb/x y</a> def </p>')),
				ns('<p> abd <a href="https://aaa.bbb/x%20y">https://aaa.bbb/x y</a> def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p> abd <a href="https://aaa.bbb/x%20y">https://aaa.bbb/x yz</a> def </p>')),
				ns('<p> abd <a href="https://aaa.bbb/x%20y">https://aaa.bbb/x y z</a> def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p> abd <a href="https://aaa.bbb/0123456789%200123456789%200123456789">https://aaa.bbb/0123456789 0123456789 0123456…zzz</a> def </p>')),
				ns('<p> abd <a href="https://aaa.bbb/0123456789%200123456789%200123456789">https://aaa.bbb/0123456789 0123456789 0123456… zzz</a> def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p> abd <a href="https://aaa.bbb/a?b=c&amp;d=e">https://xxx.bbb</a> def </p>')),
				ns('<p> abd <a href="https://aaa.bbb/a?b=c&amp;d=e">https://aaa.bbb/a?b=c&amp;d=e</a> def </p>'));

	//***** text URL not inside <a> link

	test_equals(ns(demosphere_htmledit_cleanup_html('<p>abd https://aaa.bbb/aax def</p>')),
				ns('<p> abd <a href="https://aaa.bbb/aax">https://aaa.bbb/aax</a> def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p>abd https://aaa.bbb/aax. def</p>')),
				ns('<p> abd <a href="https://aaa.bbb/aax">https://aaa.bbb/aax</a>. def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p>abd xhttps://aaa.bbb/aax def</p>')),
				ns('<p> abd xhttps://aaa.bbb/aax def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p>abd www.aaa.bbb def</p>')),
				ns('<p> abd <a href="https://www.aaa.bbb">www.aaa.bbb</a> def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p>abd www.aaa.bbb/xyz?a=b&amp;c=d def</p>')),
				ns('<p> abd <a href="https://www.aaa.bbb/xyz?a=b&amp;c=d">www.aaa.bbb/xyz?a=b&amp;c=d</a> def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p>abd www.aaa.bbb, def</p>')),
				ns('<p> abd <a href="https://www.aaa.bbb">www.aaa.bbb</a>, def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p>abd www.aaa.bbb/0123456789012345678901234567890123456789, def</p>')),
				ns('<p> abd <a href="https://www.aaa.bbb/0123456789012345678901234567890123456789">www.aaa.bbb/012345678901234567890123456789012…</a>, def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p>abd https://aaa.bbb/aaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxa… def</p>')),
				ns('<p> abd https://aaa.bbb/aaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxa… def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p>abd https://fr.wikipedia.org/wiki/aaaaaaaaaaaaaaaaaaaaaaaaaaaaa_(bbbbbbbbb) def</p>')),
				ns('<p> abd <a href="https://fr.wikipedia.org/wiki/aaaaaaaaaaaaaaaaaaaaaaaaaaaaa_(bbbbbbbbb)">https://fr.wikipedia.org/wiki/aaaaaaaaaaaaaaa…</a> def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p>abd https://fr.zzkipedia.org/wiki/aaaaaaaaaaaaaaaaaaaaaaaaaaaaa_(bbbbbbbbb) def</p>')),
				ns('<p> abd <a href="https://fr.zzkipedia.org/wiki/aaaaaaaaaaaaaaaaaaaaaaaaaaaaa_">https://fr.zzkipedia.org/wiki/aaaaaaaaaaaaaaa…</a>(bbbbbbbbb) def </p>'));

	//***** email 

	test_equals(ns(demosphere_htmledit_cleanup_html('<p> abd aaa@bbb.ccc def </p>')),
				ns('<p> abd aaa@bbb.ccc def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p> abd <a href="mailto:aaa@bbb.ccc">aaa@bbb.ccc</a> def </p>')),
				ns('<p> abd aaa@bbb.ccc def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p> abd <a href="https://xxx.yyy">aaa@bbb.ccc</a> def </p>')),
				ns('<p> abd aaa@bbb.ccc def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p> abd <a href="/uuuuu">aaa@bbb.ccc</a> def </p>')),
				ns('<p> abd aaa@bbb.ccc def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p> abd <a href="http://xxx.yyy">http://xxx.yyy aaa@bbb.ccc jjj</a> def </p>')),
				ns('<p> abd <a href="http://xxx.yyy">http://xxx.yyy</a> aaa@bbb.ccc jjj def </p>'));

	// *** demosphere_url_remove_tracking()

	test_equals(ns(demosphere_htmledit_cleanup_html('<p>abd https://www.facebook.com/events/123456789/?aaaaa=bbbbbb efg</p>')),
				ns('<p> abd <a href="https://www.facebook.com/events/123456789">https://www.facebook.com/events/123456789</a> efg </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p>abd http://example.org/?utm_source=123&utm_campaign=abcd def</p>')),
				ns('<p> abd <a href="http://example.org/">http://example.org/</a> def </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p>abd <img src="http://example.org/?utm_source=123" /> def</p>')),
				ns('<p> abd <img src="http://example.org/" /> def </p>'));

	// ***

	test_matches(ns(demosphere_htmledit_submit_cleanup('<p>abd <img src="data:image/png;base64,R0lGODlhAQABAPAAAJejrwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" /> def</p>')),
				 ns('@<p> abd <img src="/files/import-images/[a-z0-9]+.png"[^>]*width="1"[^>]*> def </p>@'));


	// ***

	test_equals(ns(demosphere_htmledit_submit_cleanup('<p>demosphere_dtoken_video( http://example.com/01234567890123456789012345678901234567890123456789 )</p>')),
				ns('<p> demosphere_dtoken_video( http://example.com/01234567890123456789012345678901234567890123456789 ) </p>'));

	test_equals(ns(demosphere_htmledit_submit_cleanup('<p>http://example.com/01234567890123456789012345678901234567890123456789</p>')),
				ns('<p> <a href="http://example.com/01234567890123456789012345678901234567890123456789">http://example.com/01234567890123456789012345…</a> </p>'));


	test_equals(ns(demosphere_htmledit_submit_cleanup('<p><a href="http://example.org/?a&amp;b&amp;c&amp;d&amp;e">x</a></p>')),
				ns('<p> <a href="http://example.org/?a&amp;b&amp;c&amp;d&amp;e">x</a> </p>'));

	test_matches(ns(demosphere_htmledit_submit_cleanup('<p>X</p> <p><a href="http://lafabrique.fr"><img class="floatRight image-with-handle" src="/files/import-images/resized1-61e70188ada5f3f0a479fadd8ab4da26.png" alt="http://www.demosphere.net/files/import-images/32203e36286c287cb99309ab0e5b8cf0.png" width="360" height="159" /></a></p> <p>Y</p>')),
				 ns('@^<p> X </p> <p> <a href="http://lafabrique.fr"><img class="floatRight" src="/files/import-images/resized1-61e70188ada5f3f0a479fadd8ab4da26.png" alt="http://www.demosphere.net/files/import-images/32203e36286c287cb99309ab0e5b8cf0.png" width="360" height="159"/></a> </p> <p> Y </p>$@'));

	test_equals(ns(demosphere_htmledit_submit_cleanup(
				   '<p><a href="http://example.org/a&amp;b&amp;c"><img src="'.$base_url.'/demosphere/css/images/view.png?a&amp;b&amp;c"/></a></p>')),
				ns('<p> <a href="http://example.org/a&amp;b&amp;c"><img src="/demosphere/css/images/view.png?a&amp;b&amp;c"/></a> </p>'));

	test_equals(ns(demosphere_htmledit_submit_cleanup(
				   '<p> <a href="http://example.org"><img src="'.$base_url.'/demosphere/css/images/view.png"/></a> </p>')),
				ns('<p> <a href="http://example.org"><img src="/demosphere/css/images/view.png" class="highres-checked" width="16" height="16"/></a> </p>'));

	$_POST['html']='<p>abc<br/>d<img/>ef<br/>ghi</p>';
	test_equals(ns(demosphere_htmledit_ajax_linecleanup()),ns('<p> abc </p> <p> def </p> <p> ghi </p>'));

	test_matches(ns(demosphere_htmledit_submit_cleanup('<p><img src="'.str_replace('https:','http:',$demosphere_config['safe_base_url']).'/demosphere/css/images/view.png">x</a></p>')),
				 ns('@^<p> <img src="'.$base_path.'files/import-images/[^"]+" width="16" height="16"/>x </p>$@'));


	test_equals(ns(demosphere_htmledit_submit_cleanup('<p><a href="'.$base_url.'/xyz">x</a></p>')),
				ns('<p> <a href="'.$base_path.'xyz">x</a> </p>'));

	test_equals(ns(demosphere_htmledit_submit_cleanup('<p><a href="1234">x</a></p>')),
				ns('<p> <a href="'.$base_path.'rv/1234">x</a> </p>'));


	test_equals(ns(demosphere_htmledit_submit_cleanup('<p><a href="../../abc">x</a></p>')),
				ns('<p> <a href="'.$base_path.'abc">x</a> </p>'));

	test_equals(ns(demosphere_htmledit_submit_cleanup('<p><a href="mailto:---">x</a><a href="abc">y</a></p>')),
				ns('<p> x<a href="abc">y</a> </p>'));

	test_equals(ns(demosphere_htmledit_submit_cleanup('<p title="xyz">x</p><p title="abc">y</p>')),
				ns('<p> x </p> <p> y </p>'));

	test_equals(demosphere_htmledit_cleanup_html(''),'');

	test_equals(ns(demosphere_htmledit_cleanup_html('<p>x</p><p>Source : xyz1</p><p>Source : xyz2</p><hr/><p>y</p><p>Source : xyz3</p><p>Source : xyz4</p>')),
				ns('<p> x </p> <p class="demosphere-sources"> Source : xyz1<br /> Source : xyz2 </p> <hr /> <p> y </p> <p class="demosphere-sources"> Source : xyz3<br /> Source : xyz4 </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p>x</p><p>Source : xyz</p><p>Source : xyz</p>')),
				ns('<p> x </p> <p class="demosphere-sources"> Source : xyz<br /> Source : xyz </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p class="x a y pdfPage subsection">x</p>')),
				ns('<p class="pdfPage subsection"> x </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p class="x y">x</p>')),
				ns('<p> x </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p class="floatRight">x</p><p class="floatRight"><a href="x">y<img src="/demosphere/css/images/view.png" class="pdfPage"/></a></p>')),
				ns('<p> x </p> <p class="floatRight"> <a href="x">y<img src="/demosphere/css/images/view.png" class="pdfPage" /></a> </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<p> <em>  </em><img class="floatRight" alt="z" src="/" /></p>')),
				ns('<p> <img class="floatRight" alt="z" src="/" /> </p>'));
	test_different(ns(demosphere_htmledit_cleanup_html('<p> <em>x  </em><img class="floatRight" alt="z" src="/" /></p>')),
				   ns('<p> <img class="floatRight" alt="z" src="/" /> </p>'));

	test_equals(ns(demosphere_htmledit_cleanup_html('<h2>a<img class="floatRight" alt="z" src="/" /></h2>')),
				ns('<h2> a </h2> <p> <img class="floatRight" alt="z" src="/" /> </p>'));

	//***** linewrapcleanup

	$_POST['html']='<p>aaa<strong>sss</strong>aaaax</p><h2>bbbbbbbbbbbx</h2><p>ccccccccccccx</p>';
	test_equals(ns(demosphere_htmledit_ajax_linewrapcleanup()),ns('<p> aaa<strong>sss</strong>aaaax bbbbbbbbbbbx ccccccccccccx </p>'));

	$_POST['html']='<p>aaaaaaaaaax</p><ul><li>bbbbbbbbbbbx</li></ul><p>ccccccccccccx</p>';
	test_equals(ns(demosphere_htmledit_ajax_linewrapcleanup()),ns('<p> aaaaaaaaaax </p> <ul> <li>bbbbbbbbbbbx </li> </ul> <p> ccccccccccccx </p>'));

	$_POST['html']='<p>Pour une gestion publique, transparente et<br /> démocratique de notre eau<br /> Pour 10 communes de la boucle nord du 92</p>';
	test_equals(ns(demosphere_htmledit_ajax_linewrapcleanup()),ns('<p> Pour une gestion publique, transparente et démocratique de notre eau </p> <p> Pour 10 communes de la boucle nord du 92 </p>'));

	$_POST['html']='<p>aaaaaaaaaax<br id="br1" />bbbbbbbbbbbx<br id="br2"/>ccccccccccccx</p>';
	test_equals(ns(demosphere_htmledit_ajax_linewrapcleanup()),ns('<p> aaaaaaaaaax bbbbbbbbbbbx ccccccccccccx </p>'));

	$_POST['html']='<p>aaaaaaaaaaaaaaaa</p><p>-bbbbbbbbbbbx</p><p>ccccccccccccx</p>';
	test_equals(ns(demosphere_htmledit_ajax_linewrapcleanup()),ns('<p> aaaaaaaaaaaaaaaa </p> <p> -bbbbbbbbbbbx ccccccccccccx </p>'));
}
?>