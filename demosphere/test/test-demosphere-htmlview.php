<?php

require_once 'test-demosphere.php';

function test_demosphere_htmlview()
{
	global $dlib_config,$base_url,$demosphere_config;
	$base_path=preg_replace('@^https?://[^/]*@','',$base_url).'/';
	$base_root=dlib_host_url($base_url);

	require_once 'demosphere-htmlview.php';

	test_matches(demosphere_htmlview_emails_add_mailto_and_obfuscate('<p>abc sdf@xyz.st zzz</p>'),
				'#onclick#');

	test_not_contains(demosphere_htmlview_emails_add_mailto_and_obfuscate('<p>abc sdf@xyz.st zzz</p>'),
				'sdf@xyz.st');

	test_matches(demosphere_htmlview_emails_add_mailto_and_obfuscate('<p>abc sdf@xyz.st zzz</p>'),
				'#<p>abc <a href="mailto:---" onclick[^>]*>[^<]*<span.*zzz</p>#');

	test_matches(demosphere_htmlview_emails_add_mailto_and_obfuscate('<p>abc sdf@xyz.st uuu ghj@jkl.rty zzz</p>'),
				'#<p>abc <a href="mailto:---" onclick[^>]*>[^<]*<span.* uuu <a href="mailto:---" onclick.* zzz</p>#');

	test_matches(demosphere_htmlview_emails_add_mailto_and_obfuscate('<p>abc sdf@xyz.st<span>uuu</span>ghj@jkl.rty zzz</p>'),
				'#<p>abc <a href="mailto:---" onclick[^>]*>[^<]*<span.*<span>uuu</span><a href="mailto:---" onclick.* zzz</p>#');
}
?>