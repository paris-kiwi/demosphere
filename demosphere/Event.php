<?php

//! An event displayed in the calendar. The main class used in Demosphere.
//! An event has a single start time, a single reference to a place.
//! Recurring events are represented by multiple Event's linked into a RepetitionGroup
class Event extends DBObject
{
	//! unique auto-incremented id
	public $id=0;
	static $id_           =['type'=>'autoincrement'];
	//! A simple (text-only) title. This can be computed from htmlTitle
	public $title='';
	static $title_        =[];
	//! A title with some highlighted words (<strong>). 
	public $htmlTitle='';
	static $htmlTitle_    =[];
	//! timestamp for the begining of the event (Note: time 3:33 is reinterpreted as "no time")
	public $startTime;
	static $startTime_    =['type'=>'timestamp'];
	//! The place where this event occurs (venue).
	public $placeId=0;
	static $placeId_      =['type'=>'foreign_key','class'=>'Place'];
	//! A log/history of actions that have occured on this event
	public $log;
	static $log_          =['type'=>'array'];
	//! An html description of this event. Edited using a tinymce based form. 
	//! Html is processed by functions in demosphere-htmledit.php, see demosphere_htmledit_submit_cleanup()
	public $body='';
	static $body_         =['type'=>'html'];
	//! The initial creator of this event.
	//! FIXME. Need to document this field. 
	//! Used to determine edit permissions for frontpage event creator and private event creator.
	//! Used in user profile (My events)
	//! Does not seem to be used for much else. 
	public $authorId=0;
	static $authorId_     =['type'=>'foreign_key','class'=>'User'];
	//! Is published or not published. This should be a boolean. This is a bit redundant with moderationStatus.
	public $status=0;
	static $status_       =['type'=>'enum','values'=>[0=>'not-published',1=>'published']];
	//! Show event or not on front page. In the future, if we implement user calendars, this should evolve.
	public $showOnFrontpage=true;
	static $showOnFrontpage_=['type'=>'bool'];
	//! Timestamp saying when this event was created.
	public $created;
	static $created_      =['type'=>'timestamp'];
	//! Timestamp saying when this event was last saved. This is updated automatically in save().
	public $changed;
	static $changed_      =['type'=>'timestamp'];
	//! User id of that last changed this event.
	public $changedById=0;
	static $changedById_  =['type'=>'foreign_key','class'=>'User'];
	//! Auto-incremented number counting how many times this event has been saved. This is updated automatically in save().
	//! This is more precise than "changed" to detect if event was edited by another user. (used in multiple edit).
	public $changeNumber=0;
	static $changeNumber_ =['type'=>'int'];
	//! Timestamp saying when status, startTime or Place where changed. This is updated automatically in save().
	public $lastBigChange=0;
	static $lastBigChange_=['type'=>'timestamp'];
	//! This is a bit redundant with "status".
	//! created: transient, this event has been created but not yet edited.
	//! waiting: not published yet
	//! incomplete: published, but something is missing.
	public $moderationStatus=0;
	static $moderationStatus_   =['type'=>'enum','values'=>[0=>'created',1=>'waiting',3=>'incomplete',4=>'published',5=>'rejected-shown',6=>'rejected',7=>'trash']];
	static $moderationStatusColors=[0=>'#fee',1=>'#ddf',3=>'#dfd',4=>'#6f6',5=>'#ccc',6=>'#aaa',7=>'#000'];
	//! A message explaining why moderationStatus is "incomplete" 
	public $incompleteMessage='';
	static $incompleteMessage_  =['type'=>'html'];
	//! An internal message only shown to moderators and admins.
	public $moderationMessage='';
	static $moderationMessage_  =[];

	//! This event needs attention from the moderation team. Higher values mean it's more urgent.
	public $needsAttention=0;
	static $needsAttention_     =['type'=>'enum','values'=>[0=>'ok',1=>'level-1',50=>'publication-request-waiting',100=>'level-2',110=>'new-private-event',115=>'self-edited',120=>'non-event-admin',125=>'open-published',130=>'publication-request']];
	//! Ranking of this event according to visists, relative to other events on the same day.
	//! Automatically updated in demosphere_stats_day_rankings(), using the daily_visits table.
	//! 0= not ranked; 1=most visited; 2=2nd most visited; 3=3rd most visited...
	public $dayrank=0;
	static $dayrank_            =['type'=>'int'];
	//! Automatically updated in demosphere_stats_day_rankings(), using the daily_visits table.
	public $visits=0;
	static $visits_             =['type'=>'int'];
	//! Repeating (recurring) events. 0 means not a repeating event.
	//! All events in a group point to the same RepetitionGroup.
	public $repetitionGroupId=0;
	static $repetitionGroupId_  =['type'=>'foreign_key','class'=>'RepetitionGroup'];
	//! Enable share-a-ride for this event
	public $useCarpool=2;
	static $useCarpool_         =['type'=>'enum','values'=>[0=>'no',1=>'yes',2=>'automatic']];
	//! Events can be automatically imported from other demosphere sites.
	//! This is (only?) used for Politis.
	//! see : demosphere-external-events.php and demosphere_config_description() : external_event_sources
	public $externalSourceId=0;
	static $externalSourceId_   =['type'=>'int'];
	//! Array containing data related to this event, for other modules.
	//! This is somewhat hackish, and some of it might be better-off in a standard field in this class.
	//! However, there are performance considerations. Adding too-many little used fields can be a significant performance problem.
	//! public-form: source, price, remarks, contact, tempId, ip
	//! tmp: oldEvent :: topics
	//! tmp: oldEvent :: terms
	//! seo-title
	//! seo-keywords
	//! seo-description
	//! duplicate (duplicate event redirect)
	//! external
    //! opinions-auto-publish
	//! topic-was-set
	public $extraData;
	static $extraData_          =['type'=>'array'];

 	static $dboStatic=false;

	//! Information used during select & render process, not stored in DB.
	//! topics,url,uc_selected,...
	public $render;

	// ********* 
	// ********* Create, load, save, delete, trash
	// ********* 

	function __construct(array $values=[])
	{
		global $user;
		global $demosphere_config;

		$this->authorId=$user->id;
		$this->created=time();
		$this->changed=$this->created;		
		$this->changedById=$user->id;
		$this->startTime=time();
		$this->extraData = [];
		$this->log=[]; 

		foreach($values as $name=>$value)
		{
			if(!isset($this->$name)){continue;}
			$this->$name=$value;
		}
	}

	function load()
	{
		parent::load();
	}

	function save($updateCopyPart=true)
	{
		global $user,$demosphere_config;
		require_once 'demosphere-dtoken.php';

		if($this->id){$prev=db_array('SELECT placeId,startTime,status,moderationStatus,needsAttention FROM Event WHERE id=%d',$this->id);}

		// ** remove edit permission on public form when someone else edits it.
		if(isset($this->extraData['public-form']) && 
		   $this->extraData['public-form']['status']==='new' &&
		   ($user->id!=0 && $this->authorId!=$user->id))
		{
			$this->extraData['public-form']['status']='edited';
		}

		if($this->id!=0 && $this->changedById!==$user->id)
		{
			require_once 'demosphere-event-revisions.php';
			demosphere_event_revisions_create($this->id);
		}

		$this->changed=time();
		$this->changedById=$user->id;
		$this->changeNumber++;

		// lastBigChange: only if changes in status, startTime and Place (FIXME)
		if(!isset($prev) || $this->lastBigChange===0){$this->lastBigChange=$this->changed;}
		else
		{
			if(    intval($prev['placeId'  ])!==$this->placeId         || // FIXME: check place changes in more detail (geo pos...)
			   abs(intval($prev['startTime'])  -$this->startTime)>1800 ||
				   intval($prev['status'    ])!==$this->status                 )
			{
				$this->lastBigChange=$this->changed;
			}
		}
		parent::save();
		
		require_once 'demosphere-misc.php';
		demosphere_log('Event','save',$this->id);

		TMDocument::reindexTid('event',$this->id);

		if($updateCopyPart)
		{
			demosphere_copy_part_event_update($this);
			demosphere_copy_part_index_update($this);
		}

		$this->searchReindex();

		require_once 'demosphere-place-search.php';
		demosphere_place_search_index_update(Place::fetch($this->placeId));
		// special case: this event has changed places: refresh previous place in place index 
		if(isset($prev) && intval($prev['placeId'])!==$this->placeId)
		{
			$prevPlace=Place::fetch($prev['placeId'],false);
			if($prevPlace!==null){demosphere_place_search_index_update($prevPlace);}
		}

		// tell javascript polling (multiple edit) that an event has changed
		// see: demosphere-event-multiple-edit.js : polling_for_event_change_notification()
		$notify='files/misc/event-change-notification.html';
		if(file_exists($notify) && !is_writable($notify)){unlink($notify);}// commandline user problem
		file_put_contents($notify,rand());
		if($demosphere_config['websockets_enabled'])
		{
			demosphere_websockets_send_message('event-change:'.$this->id.':'.$this->changed.':'.$this->changeNumber);
		}

		// Update opinions on this event.
		if($demosphere_config['enable_opinions'])
		{
			// Only update opinions if event has changed published/rejected and it actually does have opinions.
			if(isset($prev) &&
			   ($this->moderationStatus!=$prev['moderationStatus'] || 
				$this->needsAttention  !=$prev['needsAttention']   || 
				$this->status          !=$prev['status']               ) &&
			   db_result('SELECT COUNT(*) FROM Opinion WHERE eventId=%d',$this->id)>0)
			{
				require_once 'demosphere-opinion.php';
				demosphere_opinions_event_save($this->id);
			}
		}

		require_once 'demosphere-page-cache.php';
		demosphere_page_cache_clear_all();
	}

	function delete()
	{
		require_once 'demosphere-misc.php';
		demosphere_log('Event','delete',$this->id);

		$doc=TMDocument::getByTid('event',$this->id);
		if($doc!==null){$doc->delete();}

		require_once 'demosphere-search.php';
		demosphere_search_proxy_call_catch('demosphere_search_dx_delete_document','log','event',$this->id);

		// **** delete any remaining references to this event
		db_query('DELETE FROM event_revisions         WHERE event_id = %d',$this->id);
		db_query("DELETE FROM self_edit               WHERE event=%d     ",$this->id);
		db_query("DELETE FROM user_calendar_selection WHERE event=%d     ",$this->id);
		db_query("DELETE FROM Event_Topic             WHERE event=%d     ",$this->id);
		db_query("DELETE FROM Event_Term              WHERE event=%d     ",$this->id);
		db_query("DELETE FROM Opinion                 WHERE eventId=%d   ",$this->id);

		foreach(Comment::fetchList("WHERE pageType='Event' AND pageId=%d",$this->id) as $c)
		{
			$c->delete();
		}

		if($this->repetitionGroupId!=0){$this->removeFromRepetitionGroup();}

		// ****
		parent::delete();
		
		// ****
		require_once 'demosphere-dtoken.php';
		demosphere_copy_part_index_delete($this->id);
	}

	//! call this if you set the moderation status of this event to "trash".
	//! Note: this does NOT actually save the event. You MUST do it yourself.
	// FIXME: maybe a lot of this (index removal) should be done during save ?
	function trash()
	{
		$this->status=0;
		$this->setModerationStatus('trash');

		db_query("UPDATE Opinion SET isEnabled=0 WHERE eventId=%d",$this->id);
		
		$this->setNeedsAttention('ok');

		if($this->repetitionGroupId!=0){$this->removeFromRepetitionGroup();}

		// ****
		require_once 'demosphere-dtoken.php';
		demosphere_copy_part_index_delete($this->id);
	}

	//! permanently delete events in the trash bin that have not been changed for a long time
	static function cleanupTrashBinCron()
	{
		$del=db_one_col('SELECT id FROM Event WHERE '.
						   'moderationStatus=%d AND changed<%d',
						   Event::moderationStatusEnum('trash'),
						   time()-3600*24*7);
		foreach($del as $id)
		{
			echo "cleanup trash, deleting: ".intval($id)."\n";
			Event::fetch($id)->delete();
			// Fix log message to delete=>delete-trash
			db_query("UPDATE log SET message='delete-trash' WHERE type=0 AND message='delete' AND type_id=%d",$id);
		}
	}


	// ********* 
	// ********* 
	// ********* 

	function safeHtmlTitle()
	{
		if(empty($this->htmlTitle)){return ent($this->title);}
		//! faster alternative to very slow filter_xss($title,array('strong'))
		$res=$this->htmlTitle;
		$tag1='IJM!P*IO%J9865';
		$tag2='I(FM!P*IO%J936_';
		$res=str_replace(['<strong>','</strong>'],[$tag1,$tag2],$res);
		$res=htmlspecialchars($res, ENT_QUOTES, 'UTF-8',false);
		$res=str_replace([$tag1,$tag2],['<strong>','</strong>'],$res);
		return $res;
	}

	function getTopicNames()
	{
		return db_one_col("SELECT id,name FROM Topic,Event_Topic WHERE ".
						  "Topic.id=Event_Topic.topic AND event=%d ".
						  "ORDER BY Topic.weight ASC",$this->id);
	}
	function getTopics()
	{
		return Topic::fetchList("FROM Topic,Event_Topic WHERE ".
								"Topic.id=Event_Topic.topic AND event=%d ".
								"ORDER BY Topic.weight ASC",$this->id);
	}
	function setTopics(array $topics)
	{
		if(!$this->id){fatal('Event::setTopics: event has no id (not saved)');}
		db_query("DELETE FROM Event_Topic WHERE event=%d",$this->id);
		foreach($topics as $topic)
		{
			db_query("INSERT INTO Event_Topic (event,topic) VALUES (%d,%d)",$this->id,$topic);
		}
	}
	function getTermNames($vocab)
	{
		return db_one_col("SELECT id,name FROM Term,Event_Term WHERE ".
						  "Term.id=Event_Term.term AND event=%d AND vocab=%d",
						  $this->id,$vocab);
	}
	function getAllTerms()
	{
		return db_one_col("SELECT id,name FROM Term,Event_Term WHERE ".
						  "Term.id=Event_Term.term AND event=%d",
						  $this->id);
	}
	function setTerms($terms)
	{
		if(!$this->id){fatal('Event::setTerms: event has no id (not saved)');}
		db_query("DELETE FROM Event_Term WHERE event=%d",$this->id);
		foreach($terms as $term)
		{
			db_query("INSERT INTO Event_Term (event,term) VALUES (%d,%d)",$this->id,$term);
		}
	}

	function getRepetitionReference()
	{
		return db_result_check('SELECT referenceId FROM RepetitionGroup WHERE id=%d',$this->repetitionGroupId);
	}
	function getAllRepetitions()
	{
		if(!$this->repetitionGroupId){return [];}
		return db_one_col('SELECT id FROM Event WHERE repetitionGroupId=%d AND '.
						  'moderationStatus!=%d ORDER BY startTime ASC',
						  $this->repetitionGroupId,Event::moderationStatusEnum('trash'));
	}

	//! You need to save the event after calling this.
	function removeFromRepetitionGroup()
	{
		$group=RepetitionGroup::fetch($this->repetitionGroupId,false);
		if($group!==null){$group->eventHasBeenRemoved($this->id);}
		$this->repetitionGroupId=0;
	}

	function logAdd($message)
	{
		global $user;
		$this->log[]=['ts'=>time(),'uid'=>$user->id,'name'=>$user->login,'message'=>trim($message)];
	}

	function logRender()
	{
		require_once 'demosphere-date-time.php';
		$res='';
		$res.='<table class="event-log">';
		foreach($this->log as $logEntry)
		{
			$res.='<tr>';
			$date=demos_format_date('panel-list',$logEntry['ts']);
			$res.='<td>';
			$res.=implode('</td><td>',array_map('ent',explode('|',$date)));
			$res.='</td>';
			$res.='<td>'.ent($logEntry['name']).'</td>';
			//$desc=preg_replace_callback('@https?://[^[:space:]]*@',function'<a href="$0">$0</a>',$desc);
			$res.='<td>'.ent($logEntry['message']).'</td>';
			$res.='</tr>';
		}
		$res.='</table>';
		return $res;
	}

	function logText()
	{
		return implode("\n",dlib_array_column($this->log,'message'));
	}

	function access($op,$cuser=false)
	{
		global $user,$demosphere_config;
		$res=false;
		if($cuser===false){$cuser=$user;}

		// very special case: duplicate event: redirect
		if($op==='view' && isset($this->extraData['duplicate']))
		{
			return true;
		}

		// special case: contributor can see certain un-published events
		if($demosphere_config['enable_opinions'] && $op==='view' && $cuser->contributorLevel>0 && !$this->status)
		{
			require_once 'demosphere-opinion.php';
			// Standard case for contributors:
			if($this->getModerationStatus()==='waiting' && 
			   ($this->getNeedsAttention()==='publication-request' ||
				$this->getNeedsAttention()==='publication-request-waiting')) {return true;}

			// Very very special case for contributors (needsAttention is no longer "pub request" (example: self-edit unpublished event)
			$hasOwnOpinion=(boolean)db_result('SELECT COUNT(*) FROM Opinion WHERE eventId=%d AND userId=%d',$this->id,$cuser->id);
			if($this->getModerationStatus()==='waiting' && $hasOwnOpinion){return true;}

			// Contributor viewing a rejected event
			if($this->getModerationStatus()==='rejected')
			{
				$nbOpinions=(int)db_result('SELECT COUNT(*) FROM Opinion WHERE eventId=%d',$this->id);
				if($cuser->contributorLevel>1   && $nbOpinions>0 ){return true;}
				if($cuser->contributorLevel===1 && $hasOwnOpinion){return true;}
			}
		}

		// only event managers can access anything in rejected or trashed events
		if(($this->getModerationStatus()==='rejected' || 
			$this->getModerationStatus()==='trash'  ) &&
		   !$cuser->checkRoles('admin','moderator'))
		{
			return false;
		}

		switch($op)
		{
		case 'edit':
			$res=$cuser->checkRoles('admin','moderator') ||
				($cuser->checkRoles('frontpage event creator','private event creator') &&
				  ($cuser->id==$this->authorId));
			if($res!==true){$res=$this->canSelfEdit($cuser);}
			if($res!==true && $cuser->id!=0)
			{
				$ct=db_result('SELECT COUNT(*) FROM Place AS rplace,Place AS eplace WHERE '.
							  'rplace.id=eplace.referenceId AND eplace.id=%d AND rplace.inChargeId=%d',$this->placeId,$cuser->id);
				$res=$this->status===1 && $ct>0;
			}
			break;
		case 'delete':
			$res=$cuser->checkRoles('admin','moderator') ||
				($cuser->checkRoles('frontpage event creator','private event creator') &&
				 ($cuser->id==$this->authorId));
			break;
		case 'view': 
			$res=$this->status==1 ||
				$cuser->checkRoles('admin','moderator');
			if($res!==true){$res=$this->canSelfEdit($cuser);}
			break;
		default: 
			fatal("Event::access : unknown op : $op");
		}

		return $res;
	}

	function canSelfEdit($cuser=false)
	{
		global $user;
		if($cuser===false){$cuser=$user;}
		require_once 'demosphere-self-edit.php';
		return ($this->status!==0 || $this->getModerationStatus()==='waiting') &&
			$this->startTime>time()-3600*24*7 &&
			demosphere_self_edit_check_event($this->id,$cuser);
	}

    //! Has this Event been automatically published by contributors based on opinions.
    function isOpinionAutoPublished()
    {
        global $demosphere_config;
        return $demosphere_config['enable_opinions'] &&
               $demosphere_config['opinions_auto_publish'] &&
               $this->status===1 && 
               $this->getNeedsAttention()==='non-event-admin' &&
               isset($this->extraData['opinions-auto-publish']);
    }

	//! Can $cuser really give an opinion on this event
    function canGiveOpinion($cuser=false)
	{
		global $user;
		if($cuser===false){$cuser=$user;}
		if(!$this->access('view',$cuser)){return false;}
		$isRecentAutoPublish=$this->isOpinionAutoPublished() && $this->extraData['opinions-auto-publish']>time()-3600*24;
		return $isRecentAutoPublish || 
			($this->status==0 && 
			 $this->getModerationStatus()==='waiting' &&
			 ($this->getNeedsAttention()==='publication-request' ||
			  $this->getNeedsAttention()==='publication-request-waiting'));
	}

	static function moderationStatusLabels()
	{
		return
			[	
				0=>['short'=>t('created'	),'title'=>t('just created'          )],
				1=>['short'=>t('waiting'	),'title'=>t('waiting, not published')],
				3=>['short'=>t('incomplete' ),'title'=>t('published, incomplete' )],
				4=>['short'=>t('published'  ),'title'=>t('published'             )],
				5=>['short'=>t('rej./shown' ),'title'=>t('rejected, shown'       )],
				6=>['short'=>t('rejected'   ),'title'=>t('rejected, hidden'      )],
				7=>['short'=>t('trash'	    ),'title'=>t('trash'                 )],
			];
	}
	static function needsAttentionTitles()
	{
		return [0  =>t('ok'),
				1  =>t('needs attention level 1'),
				50 =>t('waiting publication request'),
				100=>t('needs attention level 2'),
				110=>t('new private event'),
				115=>t('has been self edited'),
				120=>t('published by non admin'),
				125=>t('open published'),
				130=>t('publication request'),
			   ];
	}

	static function today()
	{
		$res=strtotime('Today 0:00');
		if((time()-$res)<3600*4){$res=strtotime('-1 days',$res);}
		return $res;
	}
	
	//! Shortcut to avoid writing this too often
	function url()
	{
		global $demosphere_config,$base_url;
		return $base_url.'/'.$demosphere_config['event_url_name'].'/'.intval($this->id);
	}

	function shortModerationMessage()
	{
		return mb_substr(preg_replace("@\n.*$@s",'',$this->moderationMessage),0,18).
			(strpos($this->moderationMessage,"\n")!==false ? '…' :'');
	}

	function carpoolIsEnabled()
	{
		global $demosphere_config;
		// FIXME : add rules for choosing which events have carpools
		if($this->startTime<time()-3600*24*5){return false;}
		if($this->useCarpool===2){return $demosphere_config['carpool_enable_default'];}
		return $this->useCarpool===1;
	}

	//! Update information on this event in Sphinx search index.
	//! Information can also be queried ($justReturnVals=true) to display it in snippets
	//! $error can be "log" or "throw" (default=log: we dont want Sphinx server failures to crash everything)
	function searchReindex($justReturnVals=false,$error='log')
	{
		require_once 'demosphere-date-time.php';

		if($this->getModerationStatus()==='trash')
		{
			if($justReturnVals){return ['title'=>'','publicText'=>'','privateText'=>''];}
			require_once 'demosphere-search.php';
			demosphere_search_proxy_call_catch('demosphere_search_dx_delete_document',$error,'event',$this->id);
			return;
		}

		$title=$this->title;

		// ** Information that can be searched by and displayed to non moderators
		$publicText=
			demos_format_date('full-date-time',$this->startTime).' '.
			$this->usePlace()->getCityName().' '.
			$this->usePlace()->address.' '.
			dlib_fast_html_to_text($this->body);

		// ** Information that can only be searched by and displayed to moderators
		$privateTexts=[];

		// add most fields in this event's public form data
		if(isset($this->extraData['public-form']))
		{
			$vals=array_intersect_key($this->extraData['public-form'],array_flip(['contact','source','price','remarks','tempId','ip']));
			$privateTexts=array_merge($privateTexts,array_values($vals));
		}
		//  email addreses in event log
		if(preg_match_all('/([a-zA-Z0-9_.+-])+@(([a-z0-9-])+\.)+([a-z0-9]{2,8})+/i',$this->logText(),$m))
		{
			$privateTexts=array_merge($privateTexts,$m[0]);
		}
		//  auto repetition group email
		if($this->repetitionGroupId && $this->useRepetitionGroup()->organizerEmail!=='')
		{
			$privateTexts[]=$this->useRepetitionGroup()->organizerEmail;
		}
		//  moderation message
		if(trim($this->moderationMessage)!=='')
		{
			$privateTexts[]=$this->moderationMessage;
		}
		$privateTexts=array_unique($privateTexts);
		$privateText=implode('...',$privateTexts);

		if($justReturnVals){return compact('title','publicText','privateText');}

		require_once 'demosphere-search.php';
		demosphere_search_proxy_call_catch('demosphere_search_dx_reindex_document',$error,
										   $this->id,$title,$publicText,$privateText,'event',$this->startTime,$this->status);
	}

	// ********* 
	// ********* sql query helpers
	// ********* 

	static function sqlFields($options)
	{
		static $fieldGroups=false;
		if($fieldGroups===false)
		{
			$schema=self::getSchema();
			$fieldGroups=[];
			//$fieldGroups['all'][]=array_keys($schema);

			$fieldGroups['list_display']=array_diff(array_keys($schema),['body','log','extraData','created','changed','externalSourceId']);
			$fieldGroups['list_display']=array_merge($fieldGroups['list_display'],
													 ['place.id','place.referenceId','place.cityId','place.city.name','place.city.shortName']);
		}

		// process options
		$name=preg_replace('@[+-].*$@','',$options);
		preg_match_all('@([+-])([^+-]+)@',$options,$matches);
		// build initial field list
		$fields=$fieldGroups[$name];
		foreach($matches[2] as $mn=>$field)
		{
			if($matches[1][$mn]=='+'){$fields[]=$field;}
			else{$fields=array_diff($fields,[$field]);}
		}
		// Add aliases to each field
		foreach($fields as &$field)
		{
			$field=preg_replace('@\.(?=.*\.)@','__',$field);
			if(strpos($field,'.')!==false)
			{
				$field=$field.' AS '.str_replace('.','__',$field);
			}
			else{$field='Event.'.$field;}
		}
		return "\n  ".implode(",\n  ",$fields)."\n";
	}

	static function sqlJoins($name)
	{
		$joins=
			['list_display'=>
				  "Event\n".
				  " INNER JOIN Place AS place ON Event.placeId=place.id\n".
				  " INNER JOIN City  AS place__city ON place.cityId=place__city.id\n",
				  //'with_author'=>
				  //"Event\n".
				  //" INNER JOIN User  AS author ON Event.authorId=author.id\n",
				  //" INNER JOIN Place AS place  ON Event.placeId=place.id\n".
				  //" INNER JOIN City  AS place__city ON place.cityId=place__city.id\n",
			];
		
		return ' '.$joins[$name].' ';
	}

	static function sqlCreatePartial(array $row)
	{
		static $event0=false,$place0,$city0;
		if($event0===false)
		{
			$event0=(new ReflectionClass('Event'))->newInstanceWithoutConstructor();
			$place0=(new ReflectionClass('Place'))->newInstanceWithoutConstructor();
			$city0 =(new ReflectionClass('City' ))->newInstanceWithoutConstructor();
		}

		// Create a "fake" Event, and fill fields that were actually fetched
		$event=clone $event0;
		$event->dboCopyFromStringArray($row);
		// Create a "fake" Place, and fill fields that were actually fetched
		$place=clone $place0;
		$place->dboCopyFromStringArray($row,'place__');
		// Create a "fake" City , and fill fields that were actually fetched
		$city=clone $city0;
		$city->dboCopyFromStringArray($row,'place__city__');
		return [$event,$place,$city];
	}

	static function fetchPartial()
	{
		$args=func_get_args();
		$res=[];
		$qres=call_user_func_array('db_query',$args);
		while(($row=mysqli_fetch_assoc($qres))!== null)
		{
			// Create a "fake" Event, Place and City and fill fields that were actually fetched
			list($event,$place,$city)=Event::sqlCreatePartial($row);
			$event->setPlace($place);
			$place->setCity($city);
			$res[$event->id]=$event;
		}
		return $res;
	}

	static function createTable()
	{
		db_query("DROP TABLE IF EXISTS Event");
		db_query("CREATE TABLE Event (
  `id`                  int(11)       NOT NULL auto_increment,
  `title`               varchar(330)  NOT NULL,
  `htmlTitle`           varchar(330)  NOT NULL,
  `startTime`           int(11)       NOT NULL,
  `placeId`             int(11)       NOT NULL,
  `log`                 longblob      NOT NULL,
  `body`                longtext      NOT NULL,
  `authorId`            int(11)       NOT NULL,
  `status`              tinyint(1)    NOT NULL,
  `showOnFrontpage`     tinyint(1)    NOT NULL,
  `created`             int(11)       NOT NULL,
  `changed`             int(11)       NOT NULL,
  `changedById`         int(11)       NOT NULL,
  `changeNumber`        int(11)       NOT NULL,
  `lastBigChange`       int(11)       NOT NULL,
  `moderationStatus`    tinyint(2)    NOT NULL,
  `incompleteMessage`   longtext      NOT NULL,
  `moderationMessage`   longtext      NOT NULL,
  `needsAttention`      smallint(3)   NOT NULL,
  `dayrank`             smallint(2)   NOT NULL,
  `visits`              int(3)        NOT NULL,
  `repetitionGroupId`   int(11)       NOT NULL,
  `useCarpool`          smallint(3)   NOT NULL,
  `externalSourceId`    int(11)       NOT NULL,
  `extraData`           longblob      NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `startTime` (`startTime`),
  KEY `placeId` (`placeId`),
  KEY `authorId` (`authorId`),
  KEY `status` (`status`),
  KEY `showOnFrontpage` (`showOnFrontpage`),
  KEY `status_showOnFrontPage` (`status`,`showOnFrontpage`),
  KEY `created` (`created`),
  KEY `changed` (`changed`),
  KEY `changedById` (`changedById`),
  KEY `changeNumber` (`changeNumber`),
  KEY `lastBigChange` (`lastBigChange`),
  KEY `moderationStatus` (`moderationStatus`),
  KEY `needsAttention` (`needsAttention`),
  KEY `dayrank` (`dayrank`),
  KEY `visits` (`visits`),
  KEY `repetitionGroupId` (`repetitionGroupId`),
  KEY `useCarpool` (`useCarpool`),
  KEY `externalSourceId` (`externalSourceId`)
) DEFAULT CHARSET=utf8mb4");

	}

}
?>