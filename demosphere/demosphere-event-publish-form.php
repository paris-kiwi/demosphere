<?php
/**
 * @file
 * The public, click-to-edit, ajax event form.
 * Code that is common with other click-to-edit interfaces should go in demosphere-event-ajax-edit.php
 */

//! The (new) public form with an "inline" click-to edit ajax interface.
//! This function creates/loads the event object and renders it with the usual render function (demosphere_event_render()).
//! It also adds a few form elements specific to the publish form.
function demosphere_event_publish_form()
{
	global $base_url,$demosphere_config,$user,$currentPage;

	$isMobile=demosphere_is_mobile();
	demosphere_backup_site_assert();
	
	// If this is first edit, create random tempId and redirect to a page using it
	// This avoids saving empty events everytime someone visits this page (robots...).
	if(!isset($_GET['tempId']))
	{
		$tempId='n'.dlib_random_string(15);
		$_SESSION['public-form-events'][$tempId]=['eventId'=>0,
												  'time'=>time(),// for demosphere_cron_cleanup_dbsessions_daily()
												  'is-politis'=>isset($_GET['politis'])];
		dlib_redirect('publish?tempId='.$tempId);
	}

	$tempId=$_GET['tempId'];
	list($event,$error)=demosphere_event_publish_form_get_event_and_check_permissions($tempId);
	if($event===false)
	{
		switch($error)
		{
		case 'not-found':return ent(t('Error: event not found.'));
		case 'edited':   return ent(t('Error: you cannot edit this event.'));
		case 'too-old':  return ent(t('Error: you cannot edit this event. Form links are only valid for a single session.'));
		}
		fatal('demosphere_event_publish_form(): unknown error');
	}

	// *********** ok / cancel
	if(isset($_POST['ok']))
	{
		dlib_check_form_token('demosphere_event_publish_form');
		if($event->id===false){fatal('ERROR: ok button used for empty form');}
		demosphere_event_publish_form_ok($event,$tempId);
		$currentPage->addJs('var demosphere_event_publish_form_hide_buttons=true;','inline');
	}
	if(isset($_POST['cancel']))
	{
		dlib_check_form_token('demosphere_event_publish_form');
		unset($_SESSION['public-form-events'][$tempId]);
		$event->logAdd('public form: user hit cancel button: putting in trash');
		$event->trash();
		$event->save();
		return t('Your event has been deleted.');
	}


	require_once 'htmledit/demosphere-htmledit.php';
	demosphere_htmledit_add_js_css();
	$currentPage->addJs(    'lib/jquery-ui-1.12.1.custom/jquery-ui.js');
	$currentPage->addCss(   'lib/jquery-ui-1.12.1.custom/jquery-ui.min.css');
	$currentPage->addJs(    'lib/jquery-browser.js');
	$currentPage->addJs(    'dlib/merge-calls.js');
	$currentPage->addJs(    'demosphere/js/meditable.js');
	$currentPage->addJs(    'demosphere/js/demosphere-event-publish-form.js');
	$currentPage->addCssTpl("demosphere/css/demosphere-event-publish-form.tpl.css");
	$currentPage->addCssTpl("demosphere/css/meditable.tpl.css");
	$currentPage->addCssTpl("demosphere/css/demosphere-big-button.tpl.css");
	$currentPage->addCssTpl('demosphere/css/'.
						 ($isMobile ? 
						  'demosphere-calendar-mobile.tpl.css':
						  'demosphere-calendar.tpl.css'));

	$locale=dlib_get_locale_name();
	if($locale!=='en')
	{
		$currentPage->addJs( 'demosphere/translations/jquery.ui.datepicker-'.$locale.'.js'  );
	}

	$currentPage->addJsVar('demosphere_place_is_empty',$event->usePlace()->isEmpty());
	$currentPage->addJsVar('demosphere_event_publish_form_is_direct_publish',
						   ($user->checkRoles('admin','moderator','frontpage event creator','private event creator') ||
						   $demosphere_config['public_form_open_publishing']));

	require_once 'demosphere-date-time.php';
	list($fmt,$sep)=demosphere_date_time_short_day_month_year();
	$fmt=str_replace(['%d','%m','%Y'],['dd','mm','yy'],$fmt);
	$currentPage->addJsVar('demosphere_date_format',$fmt);
	$currentPage->addJsVar('demosphere_is_mobile',demosphere_is_mobile());

	$currentPage->addJsConfig('demosphere',['show_title_in_event','show_event_publish_donate_message']);


	// **** js text
	$jsTranslations=
		['phDate'			=>t('[ Click to set date ]'),
		 'phTime'			=>t('[ time ]'),
		 'phTimeManual'	=>t('Ex: 18:30'),
		 'phTitle'         =>t('[ Click to set title ]'),
		 'phFrontpageTitle'=>t('[ Click to set frontpage title ]'),
		 'phBody'			=>t('[ Description of this event ]'),
		 'phTopic'			=>t('[ click ]'),
		 'phAddress'		=>t('[ Click to enter address ]'),
		 'phCity'			=>t('[ City ]'),
		 'phPlaceSearch'	=>t('Ex: Main street'),
		 'placeErase'      =>t('erase place'),
		 'phPageTitle'     =>t('Your title for this page'),
		 'phDescription'   =>t('...Your description of this event...'),
		 'bodyClose'       =>t('Close the editor when you have finished writing.'),
		 'placeError'      =>t('You must either search and select an existing place or create a new one.'),
		 'topicDelete'     =>t('delete'),
		 'leavePageMessage'=>t('If you leave this page your event will be erased.'),
		 'editorCloseConfirm'=>t('Click on "ok" if you want to interrupt this operation.'),
		 'donateNow'       =>t('Donate now'),
		 'doNotDonate'     =>t('No thanks'),
		];

	$currentPage->addJsTranslations($jsTranslations);

	// Warning : moderators should prefer using backend form
	if($user->checkRoles('admin','moderator'))
	{
		dlib_message_add(t("This is a simplified form. Moderators should use the backend form (panel>create event)."),'warning');
	}

	// **** Top (almost empty)
	$top='';
	$top.='<form method="post" id="publish-form" data-change-number="'.intval($event->changeNumber).'" >';

	// **** Time
	// <select> that is used in JS to suggest times
	require_once 'demosphere-date-time.php';
	$timeSuggestions='<select class="time-suggestions-model" style="display: none">'.
		'<option value="manual">'.t('manual' ).'</option>';
	for($h=21;$h>=8;$h--)
	{
		foreach(['00','30'] as $m)
		{
			$ts=strtotime('Today '.$h.':'.$m);
			$timeSuggestions.='<option value="'.date('H:i',$ts).'">'.demos_format_date('time',$ts).'</option>';
		}
	}
	$timeSuggestions.='</select>';

	// **** Render event
	require_once 'demosphere-event.php';
	$eventDisplay=demosphere_event_render($event,['warnEventNotPublished'=>false,'comments'=>'','opinions'=>'']);

	if(!$event->startTime || $event->title==''){$currentPage->title=t('Publish ');}

	// #eventTitle is the $event->title added by demosphere_event_render()
	// When $demosphere_config['show_title_in_event']==='always', we want to make it seperatly editable, so we have to move it out of htmlView
	// In other cases, we want to show it only in a seperate step (frontpage title)
	// So in all cases we need to remove it.
	require_once 'dlib/filter-xss.php';
	// remove eventTitle
	$eventDisplay=preg_replace('@(<h2[^>]*id="eventTitle"[^>]*>).*?</h2>@i','',$eventDisplay);
	if($demosphere_config['show_title_in_event']==='always')
	{
		// insert it before htmlView
		$eventTitle='<h2 id="eventTitle" class="moved-eventTitle">'.filter_xss($event->htmlTitle,['strong']).'</h2>';
		$eventDisplay=preg_replace('@<div\b[^>]*\bid="htmlView"@',$eventTitle.'$0',$eventDisplay);
	}

	
	// **** Body / TinyMCE
	// $tinymceConfig: get std config and then change a few settings
	$tinymceConfig=demosphere_htmledit_js_config('edit-body',false);
	$tinymceConfig['plugins']=str_replace(',searchreplace','',$tinymceConfig['plugins']);
	$tinymceConfig['plugins']=str_replace(',imagetools','',$tinymceConfig['plugins']);
	$tinymceConfig['toolbar']="bold italic | bullist numlist | formatselect | undo redo | link unlink | image hr";
	unset($tinymceConfig['theme_advanced_toolbar_align']);
	$tinymceConfig['document_base_url']=$base_url;
	$tinymceConfig['width']='575';
	$tinymceConfig['height']='400';

	$currentPage->addJsVar('demosphereTinyMceConfig',$tinymceConfig);

	// add data-val for event body: FIXME (check if very large attribute breaks browsers)
	$pos=strpos($eventDisplay,'id="htmlView"');
	$body=demosphere_event_publish_remove_sources($event->body);
	$eventDisplay=substr($eventDisplay,0,$pos).' data-val="'.ent($body).'" '.substr($eventDisplay,$pos);

	// **** Topics

	$topics=Topic::getAllNames();
	$topics=array_combine(substr_replace(array_keys($topics),'t',0,0),
						  array_values($topics));
	$currentPage->addJsVar('demosphereTopics',$topics);
	
	// **** Bottom of page

	$bottom='';

	// frontpage title
	if($demosphere_config['show_title_in_event']!=='always')
	{
		$bottom.='<div id="frontpage">'.
			'<h2>'.t('Display on frontpage').'</h2>';
		require_once 'demosphere/demosphere-frontpage.php';
		$allEvents=[clone $event];
		require_once 'demosphere-event-list.php';
		$options=demosphere_event_list_options_defaults([]);
		$bottom.=demosphere_calendar_render($allEvents,$options,demosphere_is_mobile());
		$bottom.='</div>';
	}

	$bottom.='<div id="information">';
	$bottom.='<h2>'.t('Information').' <span id="information-subtitle">'.t('optional, but appreciated').'</span></h2>';
	$bottom.='<div id="form-items">';
	$bottom.='<div class="form-item" id="contact">'.
		'<label for="edit-contact">'.t('How can we contact you ?').'</label>'.
		'<div class="form-input">'.
		'<input id="edit-contact" name="contact" type="email" autocomplete="email" '.
		'placeholder="'.ent(t('Ex: robert@example.org')).'" '.
		'value="'.ent(val($event->extraData['public-form'],'contact','')).
		'"/><div class="publish-help-popup" style="display: none">'.t('Your email address (you, the person who is submitting this event). It will not be displayed. It will not be transmitted to a third party. We will only use it to contact you about the publication of this event. Without it, we will not be able to contact you if there is a problem.').'</div><span class="publish-help">?</span></div></div>';

	$bottom.='<div class="form-item" id="source">'.
		'<label for="edit-source">'.t('A web page with your event').'</label>'.
		'<div class="form-input">'.
		'<input id="edit-source" name="source" type="url"  pattern="^https?://.*"  '.
		'placeholder="'.ent(t('Ex: https://my-site.org/page-with-my-event.html')).'" '.
		'value="'.ent(val($event->extraData['public-form'],'source','')).
		'"/></div></div>';

	if($demosphere_config['public_form_ask_price'])
	{
		$prices=demosphere_event_publish_form_prices();
		$currentPrice=val($event->extraData['public-form'],'price');
		if($currentPrice===''){$currentPrice=false;}
		$bottom.='<div class="form-item" id="price">'.
			'<label for="edit-price">'.t('The price of the event').'</label>'.
			'<div class="form-input">'.
			'<select id="edit-price" name="price" type="url">';
		$bottom.='<option value="">'.t('Choose').'</option>';
		foreach($prices as $price=>$msg)
		{
			$bottom.='<option value="'.$price.'" '.
				($currentPrice!==false && $price==$currentPrice ? 'selected' : '').' >'.
				ent($msg).'</option>';
		}
		$bottom.='</select></div></div>';
	}

	$bottom.='<div class="form-item" id="remarks">'.
		'<label for="edit-remarks">'.t('Message to moderators').'</label>'.
		'<div class="form-input">'.
		'<textarea id="edit-remarks" name="remarks" '.
		'placeholder="'.ent(t('Ex: The exact time for this event will be confirmed in a few days.')).'">'.
		ent(val($event->extraData['public-form'],'remarks','')).
		'</textarea></div></div>';
	$bottom.='</div><!-- end form-items -->';
	$guidelines=Post::builtInUrl('publishing_guidelines');
	$bottom.='<p id="bottom-text">'.t('If you have trouble with this page, please send us your event here: !email',
								  ['!email'=>'<a href="mailto:'.ent($demosphere_config['contact_email']).'">'.ent($demosphere_config['contact_email']).'</a>']).
		'<br />'.
		'<a target="_blank" href="'.ent($guidelines).'">'.t('Publishing guidelines').'</a>'.
		'</p>';
	$bottom.='</div><!-- end information -->';

	$bottom.=dlib_add_form_token('demosphere_event_publish_form');
	$bottom.='<input name="tempId" value="'.ent($tempId).'" type="hidden"/>';
	$bottom.='</form>';
	$bottom.=$timeSuggestions;

	// Donate message
	if($demosphere_config['show_event_publish_donate_message']!=='never' && 
	   ($demosphere_config['show_event_publish_donate_message']==='debug' ||
		($event->id===0 && // first opening of event
		 ($user->id===0 || !isset($user->data['event_publish_donate_was_shown'])))))
	{
		$post=Post::fetch(Post::builtInPid('donate_message'));
		$bottom.='<div id="donate-message" data-title="'.ent($post->title).'">'.filter_xss_admin($post->body).'</div>';
		if($user->id!==0){$user->data['event_publish_donate_was_shown']=true;$user->save();}
	}

	$bottom.='<div id="buttons-line">';
	// don't show ok & cancel for published events (open publishing)
	$showOkCancel=!($demosphere_config['public_form_open_publishing'] && $event->status);
	//$bottom.=
	// 	'<button type="button" id="help" class="">'.t('Help').'</button> ';
	if($showOkCancel)
	{
		$bottom.=
			'<button type="button" id="ok"     name="ok"     class="big-button big-button-green">'.t('Ok!v',['!v'=>'']).'</button> '.
			'<button type="button" id="cancel" name="cancel" class="big-button big-button-red"  >'.t('Cancel').'</button>';
	}
	$bottom.='</div>';

	return $top.''.$eventDisplay.''.$bottom;
}

function demosphere_event_publish_form_prices()
{
	return 	[
			 0	 =>t('Free'                     ),
			 1	 =>t('Pay what you want'		),
			 7	 =>t('Full fare less than 7€'   ),
			 10  =>t('Full fare from 7€ to 10€' ),
			 15  =>t('Full fare from 10€ to 15€'),
			 1000=>t('Full fare more than 15€'  ),
			 ];
}

//! Called when user hits green "ok" button on publish form.
function demosphere_event_publish_form_ok($event,$tempId)
{
	global $base_url,$demosphere_config,$user;
	
	$out='';
	$formLink=$base_url.'/publish?id='.$tempId;

	demosphere_event_publish_form_add_source_link($event);

	$event->status=0;
	$event->showOnFrontpage=true;
	$event->setModerationStatus('waiting');
	$event->setNeedsAttention('publication-request');

	// special case: private event
	if($user->checkRoles('private event creator'))
	{
		$event->status=1;
		$event->showOnFrontpage=false;
		$event->setModerationStatus('published');
		$event->setNeedsAttention('non-event-admin');
	}

	// special case: front page publisher
	if($user->checkRoles('frontpage event creator'))
	{
		$event->status=1;
		$event->setModerationStatus('published');
		$event->setNeedsAttention('non-event-admin');
	}

	// special case: moderator or admin
	if($user->checkRoles('admin','moderator'))
	{
		$event->status=1;
		$event->setModerationStatus('published');
		$event->setNeedsAttention('ok');
	}

	// when a non admin user creates an event, auto-select it in his user cal
	if($user->id!=0 && !$user->checkRoles('admin','moderator'))
	{
		require_once 'demosphere-user-calendar.php';
		demosphere_user_calendar_set_select($event->id,$user->id,1);
	}

	if($demosphere_config['public_form_open_publishing'])
	{
		$event->status=1;
		$event->showOnFrontpage=true;
		$event->setModerationStatus('published');
		$event->setNeedsAttention('open-published');
	}

	$sendWaitMail=false;
	$selfEditUrl=false;
	if($event->status)
	{
		$out.='<h3>'.t('Your event is published!').'</h3>';
		$out.='<ul>';
		$out.='<li><a href="'.ent($event->url()).'">'.ent(t('Go to the event\'s page.')).'</a></li>';
		$out.='<li><a href="'.ent($formLink).'">'.ent(t('Go back and edit the event.')).'</a></li>';
		$out.='</ul>';
		$out.='<p>'.t('Thanks for contributing !').'</p>';
	}
	else
	{
		$out.='<h3>'.t('Your event has been saved').'</h3>';
		$out.='<p>'.t('We will read your event and publish it soon, usually in less than 24 hours.').'</p>';
		$email=val($event->extraData['public-form'],'contact','');
		$parts=dlib_email_address_parts($email);
		if(filter_var($email, FILTER_VALIDATE_EMAIL)!==false && 
		   dlib_email_check_domain($email))
		{
			$out.='<p>'.t('If there is a problem, we will contact you at @email.',['@email'=>$email]).'</p>';
			if(($demosphere_config['public_form_self_edit_link'] ?? false) && 
			   !$event->access('edit'))
			{
				$selfEdit=demosphere_self_edit_create($email,$event->id,$event->repetitionGroupId);
				$selfEditUrl=$event->url().'/edit?selfEdit='.$selfEdit['password'];
				$out.='<p>'.t('You can edit this event before and after it is published by using this link:').'<br/>'.
					'<a href="'.ent($selfEditUrl).'">'.ent($selfEditUrl).'</a></p>';
			}
			$sendWaitMail=true;
		}
		else
		{
			$out.='<p>'.t('You have not entered a valid contact email. That is ok, but if there is a problem, we will not be able to contact you.').'</p>';
		}
		$out.='<p>'.t('If you want to change something, you can continue editing it below. Changes will be automatically saved as you do them.').'</p>';
		$out.='<p>'.t('Thanks for contributing !').'</p>';
	}

	$event->logAdd("Created from publish form, contact : ".val($event->extraData['public-form'],'contact',''));
	$event->save();

	dlib_message_add($out);

	// send email to tell moderators that someone has submitted this event
	if(!$user->checkRoles('admin','moderator'))
	{
		demosphere_event_publish_form_send_email_to_moderators($event);
	}

	// Send a confirmation mail to the person requesting this publication.
	if($sendWaitMail && !isset($event->extraData['public-form']['confirmation-mail-sent']))
	{
		require_once 'lib/class.phpmailer.php';
		$mail=new PHPMailer();
		$mail->CharSet="utf-8";
		$mail->SetFrom($demosphere_config['contact_email']);
		$mail->AddAddress($event->extraData['public-form']['contact']);
		$mail->Subject=t('Your publication request on !site_name (!start_time)',
						 ['!site_name' =>$demosphere_config['site_name'],
						  '!start_time'=>demos_format_date('full-date-time',$event->startTime)]);
		$html=t('<p><i>This is an automatic message.</i></p><p>Hi,</p><p>Thanks for submiting the following event on @site_name:<br/>@start_time : @title <br/>We have received it correctly and will process it as soon as possible. If we have any questions, we can contact you.</p><p>(Ref.!event_id)</p><p>Regards,</p><p>@site_name</p>',
				['@site_name' =>$demosphere_config['site_name'],
				 '@start_time'=>demos_format_date('full-date-time',$event->startTime),
				 '@title'     =>$event->title,
				 '!event_id'  =>$event->id]);
		// hackish insert into email, but it avoids having to update all translations :-(
		if($selfEditUrl!==false)
		{
			$selfEditMessage='<p>'.t('You can edit this event before and after it is published by using this link:').'<br/>'.
				'<a href="'.ent($selfEditUrl).'">'.ent($selfEditUrl).'</a></p>';
			$html=str_replace('<p>(Ref.'.$event->id.')</p>',$selfEditMessage.'<p>(Ref.'.$event->id.')</p>',$html);
		}
		require_once 'demosphere-event-edit-form.php';
		$html=demosphere_event_edit_form_email_string_replace($html,$event,true,$event->extraData['public-form']['contact']);
		$mail->MsgHTML($html);
		$mail->AltBody=Html2Text::convert($html);
		if(!$mail->Send()) 
		{
			dlib_message_add(t("Error while sending message to @email.",['@email'=>$event->extraData['public-form']['contact']]),
							 'error');
		}
		else
		{
			dlib_message_add(t('A confirmation email was sent to you.'));
			$event->extraData['public-form']['confirmation-mail-sent']=time();
			$event->save();
		}
	}
}

// ******* send an email to tell moderators an event has been submitted
function demosphere_event_publish_form_send_email_to_moderators($event)
{
	global $demosphere_config;
	$subject=mb_substr(preg_replace('@[^\p{L}\p{Nd} -]@u','_',$event->title),0,100);
	$html='<p>'.ent(t('A user has submitted the following event using the public form:')).'</p>';
	$html.='<p><a href="'.ent($event->url()).'">'.ent($event->url()).'</a></p>';
	$html.='<h2>'.$event->htmlTitle.'</h2>';
	require_once 'demosphere-date-time.php';
	$html.='<ul>'.
		'<li>'.t('Start time:').' '.ent(demos_format_date('full-date-time',$event->startTime))."</li>".
		'<li>'.t('Contact:'   ).' '.ent(val($event->extraData['public-form'],'contact',''))."</li>".
		'<li>'.t('Web source:').' '.ent(val($event->extraData['public-form'],'source' ,''))."</li>".
		($demosphere_config['public_form_ask_price'] ? 
		 '<li>'.t('Price:'     ).' '.ent(val($event->extraData['public-form'],'price'  ,''))."</li>" : '').
		'<li>'.t('Remarks:'	  ).' '.ent(val($event->extraData['public-form'],'remarks',''))."</li>".
		'</ul><hr/>';
	$html.=$event->body;
	require_once 'demosphere-emails.php';
	demosphere_emails_notification_send('public-form',$subject,$html);
}

//! Remove all paragraphs "<p class="demosphere-sources">..</p>"
//! This is done on event body that will be shown in editor.
//! The "Source:" paragraphs are confusing for users.
function demosphere_event_publish_remove_sources($body)
{
	$body=preg_replace('@<p[^>]*class=[^>]*demosphere-sources.*</p>@sU','',$body);
	require_once 'dlib/filter-xss.php';
	$body=filter_xss_admin($body);	
	return $body;
}

//! Adds Source:  http://example.org from $event->extraData['public-form']['source']
//! This is only added to an existing <p class="demosphere-sources">...</p>.
function demosphere_event_publish_form_add_source_link(&$event)
{
	// Check for link in sources
	$source= val($event->extraData['public-form'],'source','');
	$source=trim($source);
	if(!preg_match('@^https?://[^ ]*$@i',$source)){$source=false;}
	$hasSourceLink=preg_match('@<p[^>]*class=[^>]*demosphere-sources((?!</p).)*<a[^>]*href="@s',$event->body,$matches);
	if($source!==false && !$hasSourceLink)
	{
		$shortUrl=urldecode($source);
		$shortUrl=dlib_cleanup_plain_text($shortUrl,false,false);// urldecode can make invalid utf8
		$shortUrl=dlib_truncate($shortUrl,45);
		$sourceLink=t('_source_').' : <a href="'.ent($source).'">'.ent($shortUrl).'</a><br />';
		$event->body=preg_replace('@(<p[^>]*class=[^>]*demosphere-sources[^>]*>)@',
						   '$1'."\n".$sourceLink,$event->body,1);
	}
}

//! Respond to ajax request for change number. 
//! Used to check for stale page reload / backbutton situations.
function demosphere_event_publish_get_change_number()
{
	$tempId=$_GET['tempId'];
	if(!isset($_SESSION['public-form-events'][$tempId])){return false;}
	$eventId=$_SESSION['public-form-events'][$tempId]['eventId'];
	if($eventId===0){return 0;}
	return intval(db_result('SELECT changeNumber FROM Event WHERE id=%d',$eventId));
}

//! Returns an Event and an error label.
//! The event may be either a new Empty event or an existing Event.
function demosphere_event_publish_form_get_event_and_check_permissions($tempId)
{
	global $user,$demosphere_config;
	if(!isset($_SESSION['public-form-events'][$tempId])){return [false,'not-found'];}
	$eventId=$_SESSION['public-form-events'][$tempId]['eventId'];
	if($eventId===0)
	{
		// Create empty unsaved event.
		$event=new Event();
		$event->authorId=$user->id;
		$event->startTime=0;
		$event->setPlace(Place::fetch(1));
		$event->extraData['public-form']=['status'=>'new',
										  'is-politis'=>$_SESSION['public-form-events'][$tempId]['is-politis'],];
		if($user->id!=0){$event->extraData['public-form']['contact']=$user->email;}
		$event->setModerationStatus('trash');
	}
	else
	{
		$event=Event::fetch($eventId,false);
		if($event===null){return [false,'not-found'];}
	}

	// ** permissions

	$error=false;
	// extra checks : not edited by other user, not too old
	if($event->extraData['public-form']['status']!=='new'){$error='edited';}
	if($event->created<(time()-24*3600)                  ){$error='too-old';}
	// override error if user can edit this event
	if($event->id!==0 && $event->access('edit')          ){$error=false;}

	if($error!==false){$event=false;}
	return [$event,$error];
}

//! Respond to an ajax request for updating a field in a "click to edit" page.
//! The fields are actually updated in : demosphere_event_ajax_edit_update
//! This function basically loads event,checks permissions, calls demosphere_event_ajax_edit_update and then adds
//! text & html responses that are specific to "click to edit".
function demosphere_event_publish_form_ajax()
{
	global $demosphere_config;
	dlib_check_form_token('demosphere_event_publish_form');

	// Simple semaphore locking to avoid concurrency. Semaphore is automatically released when script exits.
	// We don't have proof this ever was actually needed. But it could happen.
	$semaphoreKey=ftok(getcwd().'/'.$demosphere_config['tmp_dir'],'A');
	$semaphore=sem_get($semaphoreKey,1,0600);
	$ok=sem_acquire($semaphore);
	if(!$ok){fatal('sem_acquire failed public form');}
	
	$res=[];

	// ********** Load event 
	$tempId=$_POST['tempId'];
	list($event,$error)=demosphere_event_publish_form_get_event_and_check_permissions($tempId);
	if($event===false)
	{
		switch($error)
		{
		case 'not-found':dlib_not_found_404();
		case 'edited':  return ['errors'=>[t('Another user has edited this event, you can no longer edit it.')]];
		case 'too-old': return ['errors'=>[t('Permission denied')]];
		}
		fatal('demosphere_event_publish_form_ajax(): unknown error');
	}

	if($event->id==0)
	{
		$event->logAdd('Created in public form with tempId='.$tempId);
		$event->extraData['public-form']['tempId']=$tempId;
		$event->extraData['public-form']['ip']=$_SERVER['REMOTE_ADDR'];
		$event->save();
		$_SESSION['public-form-events'][$tempId]['eventId']=$event->id;
		$_POST['change_number']=$event->changeNumber;
	}

	// *********** 
	
	require_once 'demosphere-event-ajax-edit.php';
	list($res,$changed)=demosphere_event_ajax_edit_update($event);

	if(isset($res['errors'])){return $res;}

	// *********** Add information (like HTML/text) to $res that is specific to the public form

	if(isset($_POST['title']))
	{
		$res['html']=$event->htmlTitle;
		$res['value']=dlib_html_entities_decode_all(preg_replace('@</?strong>@','_',$event->htmlTitle),ENT_COMPAT,'UTF-8');
	}

	if(isset($_POST['placeId']))
	{
		require_once 'demosphere-place.php';
		$html=demosphere_place_render($event->usePlace(),true);
		$html=preg_replace('@^.*(<div[^>]*class="[^"]*(?<=[ "])place[ "].*)<div[^>]*id="htmlView".*$@s','$1',$html);
		$res['html']=$html;
	}

	if(isset($_POST['time']))
	{
		$ts=$event->startTime;
		if($ts==0){$res['value']='';$res['text']='';}
		else
		{
			require_once 'demosphere-date-time.php';
			$timeText=demos_format_date('full-date-time-time',$ts);
			$res['value']=date('H:i',$ts);
			$res['text']=$timeText;
		}
	}

	if(isset($_POST['date']))
	{
		require_once 'demosphere-date-time.php';
		$dateText=demos_format_date('full-date-time-date',$event->startTime);
		$res['value']=date('Y',$event->startTime)<=1970 ? '' :
			demos_format_date('short-day-month-year',$event->startTime);
		$res['text']=$dateText;
		$res['ts']=$event->startTime;
	}

	if(isset($_POST['topics']))
	{
		$out='';
		$topicList=Topic::getAllNames();
		$topics=array_unique(array_intersect(substr_replace($_POST['topics'],'',0,1),array_keys($topicList)));
		
		foreach($topics as $tid)
		{
			$out.='<a data-topic="'.$tid.'" >'.ent($topicList[$tid]).'</a>, ';
		}
		$out=preg_replace('@, $@','',$out);
		$res['html']=$out;		
	}

	if(isset($_POST['body']))
	{
		require_once 'demosphere-event.php';
		$html=demosphere_event_render($event,['warnEventNotPublished'=>false,'opinions'=>'','comments'=>'']);
		preg_match('@<div[^>]*id="htmlView"[^>]*>(.*)</div><!-- end htmlView -->@s',$html,$matches);
		$res['html']=$matches[1];

		// Remove "Source:" line from editted value.
		// It is confusing for the user. It will be automatically added by demosphere_event_ajax_edit_update()
		$res['value']=demosphere_event_publish_remove_sources($event->body);
	}

	return $res;
}

//! Ajax called function that checks for published events at a given date/time/place.
//! Returns an html message that can be displayed in a popup.
function demosphere_event_publish_form_check_duplicate()
{
	global $demosphere_config;

	if(!isset($_GET['tempId'])){dlib_bad_request_400();}
	list($event,$error)=demosphere_event_publish_form_get_event_and_check_permissions($_GET['tempId']);
	if($event===false || $event->id==0){dlib_bad_request_400();}

	$placeId=intval($_GET['placeId']);
	$date=$_GET['date'];
	$time=$_GET['time'];

	require_once 'demosphere-date-time.php';
	$ts=demosphere_date_time_parse_short($date,$time);
	if($ts===false){return ['hasDuplicate'=>false];}
	
	$placeRefId=db_result('SELECT referenceId FROM Place WHERE id=%d',$placeId);
	$duplicates=Event::fetchList('FROM Event,Place WHERE '.
								 ' Event.id!=%d AND '.
								 ' Event.placeId=Place.id AND '.
								 ' Place.referenceId=%d AND '.
								 ' Event.status=1 AND '.
								 ' Event.showOnFrontpage=1 AND '.
								 ' ABS(Event.startTime-%d)<31*60 ',$event->id,$placeRefId,$ts);
	if(count($duplicates)===0){return ['hasDuplicate'=>false];}
	$message=t('<p>The following published event(s) occur(s) at the same time and place as the one you are entering. Please make sure that your event is not already published.</p>');
	$message.='<ul>';
	foreach($duplicates as $duplicate)
	{
		$message.='<li>';
		$message.='<a target="_blank" href="'.ent($duplicate->url()).'">'.ent($duplicate->title).'</a>';
		$message.='</li>';		
	}
	$message.='</ul>';
	$message.=t('<p>If it is not a duplicate, please ignore this message.</p>');
	$message.=t('<p>If it is a duplicate and you want to change the event, please <a target="_blank"  href="!contact">contact us</a>.</p>',
				['!contact'=>ent(Post::builtInUrl('contact'))]);
	if($demosphere_config['comments_site_settings']==='enable' ||
	   $demosphere_config['comments_site_settings']==='pre-mod' )
	{
		$message.=t('<p>If it is urgent, you can also leave a comment on the event\'s page.</p>');
	}

	return ['hasDuplicate'=>true,
			'title'=>t('Duplicate ?'),
			'message'=>$message];
}


?>