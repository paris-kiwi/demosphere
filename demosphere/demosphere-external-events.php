<?php

//! manage external event sources 
//! download external events
function demosphere_external_events_cron()
{
	require_once 'demosphere-event-list.php';
	require_once 'dlib/filter-xss.php';
	global 	$demosphere_config,$custom_config;

	if(!isset($demosphere_config['external_event_sources'])){return;}
	$sources=$demosphere_config['external_event_sources'];

	$today=Event::today();

	// fetch list of events for each external site
	$res=[];
	foreach($sources as $source)
	{
		echo "*** ".$source['name']."<br/>\n";
		//if($source['name']!='Rennes'){continue;}$source['url']='https://paris.local';// testing
		//if($source['name']!='Paris'){continue;}$source['url']='https://paris.local';// testing
		if($source['type']!='demosphere'){continue;}

		// ******* fetch json from remote site
		$url=$source['url'].'/event-list-json?url&fullUrls&place__address&visits&place__latitude&place__longitude&place__zoom&selectStartTime='.$today;
		$json=@file_get_contents($url);
		//echo 'json:'.$url.':';var_dump($json);
		if($json===false){continue;}
		$remoteResponse=json_decode($json);
		if($remoteResponse===NULL){continue;}
		$remoteEvents=$remoteResponse->events;

		$remoteById=array_flip(dlib_object_column($remoteEvents,'id'));

		// ******* get local, existing db-saved events for this source
		$localEvents=Event::fetchList('WHERE startTime>%d AND externalSourceId=%d',
									  $today,$source['id']);
		$localByRemoteId=[];
		foreach($localEvents as $k=>$e)
		{
			// FIXMEx: skip very old (demosphere v1 events: nid not id)
			if(!isset($e->extraData['external']['remoteId'])){continue;}
			$localByRemoteId[$e->extraData['external']['remoteId']]=$k;
		}

		// ******* existing local events that no longer exist on remote source
		foreach($localEvents as &$localEvent)
		{
			// FIXMEx: skip very old (demosphere v1 events: nid not id)
			if(!isset($localEvent->extraData['external']['remoteId'])){continue;}
			if(isset($remoteById [$localEvent->extraData['external']['remoteId']])){continue;}

			if($localEvent->status)
			{
				echo "source:".ent($source['name'])."(".$source['id'].") remote event ".
					intval($localEvent->extraData['external']['remoteId']).
					" disappeared, trashing local:".
					$localEvent->id."<br/>\n";
				$localEvent->trash();
				$localEvent->save();
			}
		}
		unset($localEvent);

		// ******* add new local events or update existing local events
		// from remote events
		foreach($remoteEvents as $remote)
		{
			$resetStatuses=false;
			if(! isset($localByRemoteId [$remote->id]))
			{
				echo "source:".ent($source['name'])."(".$source['id']."): ".
					"found new remote event: remoteid=".intval($remote->id)."<br/>\n";
				$event=new Event();
				$event->placeId=1;
				$event->save();
				$event->externalSourceId=$source['id'];
				$event->authorId=0;//FIXME (maybe 0 is ok?)
				$resetStatuses=true;
			}
			else
			{
				$k=$localByRemoteId[$remote->id];
				$event=&$localEvents[$k];
				echo "found existing event id=".$event->id." for remoteid=".intval($remote->id)."\n";
				// Resuscitate disappeared event
				if($event->getModerationStatus()==='trash')
				{
					echo "source:".ent($source['name'])."(".$source['id'].") : ".
						"Resuscitating event that reapeared: id=".$event->id." remoteid=".intval($remote->id)."<br/>\n";
					$resetStatuses=true;
				}
				else
				// If this event is unchanged, just skip it, don't save (save is very slow)
				{
					$place=Place::fetch($event->placeId);
					//var_dump($event);die('oo');

					if($event->title		==$remote->title		             &&
					   // FIXME temp: until all sites have migrated (5/9/2016) from start_time to startTime & html_title to htmlTitle
					   $event->startTime	==(isset($remote->startTime) ? $remote->startTime : $remote->start_time)   &&
					   $place->address		==$remote->place__address	         &&
					   abs($place->latitude  -$remote->place__latitude) <.000001 &&
					   abs($place->longitude -$remote->place__longitude)<.000001 &&
					   $place->zoom			==$remote->place__zoom			       )
					{
						// Just update visits, with simple sql query
						if($event->visits!=$remote->visits)
						{
							db_query('UPDATE Event SET visits=%d WHERE id=%d',$remote->visits,$event->id);
						}
						continue;
					}
					else
					{
						echo "event has changed, updating\n";
					}
				}
			}

			if($resetStatuses)
			{
				$event->status           =$source['event_status'];
				$event->moderationStatus=$source['event_moderation_status'];
				$event->needsAttention  =$source['event_needs_attention'];
			}

			$event->title			=$remote->title;
			$event->body			='<p>'.t('External:').' '.filter_xss_check_url($remote->url).'</p>';
			// FIXME temp: until all sites have migrated (5/9/2016) from start_time to startTime & html_title to htmlTitle
			$event->startTime		=(isset($remote->startTime) ? $remote->startTime : $remote->start_time);
			$place=new Place();
			$place->setCity(City::findOrCreate($remote->place__city__getShort));
			$place->address	    =$remote->place__address;
			$place->latitude	=$remote->place__latitude;
			$place->longitude	=$remote->place__longitude;
			$place->zoom		=$remote->place__zoom;
			$existingPlaces=Place::find(['cityId'   =>$place->cityId,
										 'address'	 =>$place->address,
										 'latitude' =>$place->latitude,
										 'longitude'=>$place->longitude,
										 'zoom'	 =>$place->zoom		 ],1);
			if(count($existingPlaces)){$place=Place::fetch($existingPlaces[0]);}
			$event->setPlace($place);

			$event->htmlTitle		 =filter_xss($remote->htmlTitle,['strong']);
			$event->visits           =intval($remote->visits);
			$event->extraData['external']['remoteId']=$remote->id;
			$event->extraData['external']['url'     ]=$remote->url;
			//echo "remote:";var_dump($remote);
			//echo "event before save:";var_dump($event);
			$event->save();
			if(isset($custom_config['demosphere_external_events_postsave']))
			{
				$custom_config['demosphere_external_events_postsave']($event,$remote,$source);
			}
			//die("iii\n");
			//fatal('iii');
		}
	}
}

// ************************************************************************
// ********* shared info between Demosphere sites
// ************************************************************************


//! Fetch a list of demosphere sites from tech.demosphere.net
//! This is meant to be called from cron.
function demosphere_slm_sites_update()
{
	$json=@file_get_contents('https://tech.demosphere.net/slm/sites/json');
	if($json!==false)
	{
		$sites=json_decode($json,true);
		variable_set('slm_sites','',$sites);
	}
}

//! Return public information on this site.
//! Is called by tech.demosphere.net to centralize data.
function demosphere_site_info()
{
	global $demosphere_config;
	$res=array_intersect_key($demosphere_config,
		 array_flip(['map_center_latitude','map_center_longitude','map_zoom','contact_email','event_url_name','place_url_name','locale',]));
	return $res;
}

?>