<?php
/**
 * @file
 * Code to update event fields through ajax.
 * This code is used by click-to-edit interfaces,
 * notably: the public "publish" form : demosphere-event-publish-form.php
 * and multiple edit forms (pending events, and others).
 */

//! Update a field of an event through an ajax call.
//! This function is currently used for "publish" (public form) and multiple edit.
//! If errors are detected, the events's fields might be changed in the browser, but the event is not saved.
function demosphere_event_ajax_edit_update($event)
{
	global $demosphere_config,$user;

	$changed=[];
	$errors=[];
	$isStale=false;
	$res=[];

	if($event->changeNumber>$_POST['change_number'])
	{
		$errors[]='This event has been modified elsewhere. Please reload/refresh this page.'.
			' '.$event->changeNumber.'>'.$_POST['change_number'];
		$res['debug']=$event->changeNumber.' / '.$_POST['change_number'];
		dlib_log_error(dlib_last($errors));
		$isStale=true;
	}

	// ***********
	// ***********
	// ***********

	// **** title
	if(isset($_POST['title']))
	{
		$title=$_POST['title'];
		if($user->checkRoles('admin','moderator'))
		{
			$nbUnderscores=substr_count($title,'_');
			if(($nbUnderscores%2)===1)
			{$errors[]=t('You must use two underscores around each keyword in the title.');}
			if($demosphere_config['require_keyword_in_event_title'] && $event->status!=0 && $nbUnderscores===0)
			{$errors[]=t('You must use underscores to select a keyword in the title.');}
		}

		// Enforce title length (but only in public form, not in multiple edit)
		if(isset($_POST['dlib-form-token-demosphere_event_publish_form']))
		{
			require_once 'demosphere-event.php';
			$cityForTitleLength=$_POST['cityForTitleLength'] ?? '';
			if($cityForTitleLength===''){$cityForTitleLength='XXXXXXXXXXXXXX';}
			if(!demosphere_event_title_length_is_ok($title,$cityForTitleLength))
			{
				$errors[]=t('Too long !');
			}
		}

		if(count($errors)===0)
		{
			$changed['title']=true;
			$title=dlib_cleanup_plain_text($title,false);
			$title=preg_replace('@\s+@',' ',$title);
			$event->title=str_replace('_','',$title);
			$htmlTitle=ent($title);
			$htmlTitle=preg_replace('@_([^_]*)_@','<strong>$1</strong>',
									 $htmlTitle);
			$event->htmlTitle=$htmlTitle;
		}
	}

	// **** moderationStatus
	if(isset($_POST['moderationStatus']) && 
	   !$user->checkRoles('admin','moderator')){$errors[]='permission denied';}
	else
	if(isset($_POST['moderationStatus']))
	{
		$moderationStatus=intval($_POST['moderationStatus']);
		$isPublished=
			$moderationStatus==Event::moderationStatusEnum('incomplete'  ) ||
			$moderationStatus==Event::moderationStatusEnum('published'	  ) ||
			$moderationStatus==Event::moderationStatusEnum('rejected-shown');

		if($isPublished){demosphere_event_ajax_edit_validate_publish($event,$errors);}

		if(count($errors)==0)
		{
			$schema=Event::getSchema();
			$changed['moderationStatus']=$schema['moderationStatus']['values'][$_POST['moderationStatus']];
			$oldModerationStatus=$event->moderationStatus;
			$oldNeedsAttention  =$event->needsAttention;
			$event->moderationStatus=$moderationStatus;
			$event->status=$isPublished ? 1:0;
			if($event->getModerationStatus()==='rejected-shown'){$event->showOnFrontpage=false;}
			
			// *** certain modstatus changes imply needs attention changes
			// see also: demosphere_event_edit_form_submit()

			// auto change to "ok", when "publication request" is "published"
			if($oldNeedsAttention  ==Event::needsAttentionEnum('publication-request') &&
			   ($event->getModerationStatus()==='published' ||
				$event->getModerationStatus()==='incomplete'))
			{
				$event->setNeedsAttention('ok');
			}
			// auto change to "publication-request-waiting", when "publication request" is "rejected"
			if($oldModerationStatus==Event::moderationStatusEnum('waiting') &&
			   $oldNeedsAttention  ==Event::needsAttentionEnum('publication-request') &&
			   ($event->getModerationStatus()==='trash' ||
				$event->getModerationStatus()==='rejected'))
			{
				$event->setNeedsAttention('publication-request-waiting');
			}

			// events that where rejected-shown and change status are reshown on frontpage to avoid confusion
			if($event->getModerationStatus()!='rejected-shown' && 
			   $oldModerationStatus    ==Event::moderationStatusEnum('rejected-shown')){$event->showOnFrontpage=true;}
			// trashed events need special processing
			if($event->getModerationStatus()==='trash')
			{
				$event->trash();
			}
		}
		else
		{
			$res['resetModerationStatus']=$event->moderationStatus;
		}
		$res['showOnFrontpage'        ]=$event->showOnFrontpage;
		$res['needsAttention']=$event->needsAttention;
	}

	// **** needsAttention
	if(isset($_POST['needsAttention']) && 
	   !$user->checkRoles('admin','moderator')){$errors[]='permission denied';}
	else
	if(isset($_POST['needsAttention']))
	{
		$changed['needsAttention']=intval($_POST['needsAttention']);
		
		$event->needsAttention=$_POST['needsAttention'];
	}

	// **** moderationMessage
	if(isset($_POST['moderationMessage'])&& 
	   !$user->checkRoles('admin','moderator')){$errors[]='permission denied';}
	else
	if(isset($_POST['moderationMessage']))
	{
		$changed['moderationMessage']=true;
		$event->moderationMessage=$_POST['moderationMessage'];
	}

	// **** topics
	if(isset($_POST['topics']))
	{
		$oldTopics=array_keys($event->getTopicNames());
		$isOthers=false;
		if($_POST['topics']==''){$topics=[];}
		else
		{
			if(array_search('t0',$_POST['topics'])===false)
			{
				$topics=array_unique(array_intersect(substr_replace($_POST['topics'],'',0,1),
													 array_keys(Topic::getAllNames())));
			}
			else
			{$topics=[];$isOthers=true;}// others/none
		}
		sort($topics);
		sort($oldTopics);
		
		if(count($topics)===0 && $demosphere_config['topics_required']==='mandatory' && $event->status!=0)
		{
			$errors[]=t('You must choose a topic');
		}
		else
		if($oldTopics!=array_values($topics) ||
		  ($isOthers && !($event->extraData['topic-was-set'] ?? false) ))
		{
			$changed['topics']=true;
			$event->setTopics($topics);
			$event->extraData['topic-was-set']=true;
		}
	}

	// **** placeId
	if(isset($_POST['placeId']))
	{
		$place=Place::fetch(intval($_POST['placeId']));
		$event->setPlace($place);
		$changed['placeId']=true;
		if(function_exists('custom_demosphere_event_public_form_submit'))
		{
			custom_demosphere_event_public_form_submit($event);
		}
	}

	// **** place_erase
	if(isset($_POST['place_erase']))
	{
		$event->setPlace(Place::fetch(1));
		$changed['place_erase']=true;
	}

	// **** time
	if(isset($_POST['time']))
	{
		$time=trim($_POST['time']);
		if($time===''){$time='3:33';}
		require_once 'demosphere-date-time.php';
		$parsedTime=demosphere_date_time_parse_time($time);
		if($parsedTime===false)
		{
			$errors[]=t('Invalid time format. Please use hh:mm');
		}
		else
		{
			$changed['time']=true;
			$event->startTime=strtotime($parsedTime['hours'].':'.sprintf('%02d',$parsedTime['minutes']),
										 $event->startTime==0 ? strtotime('Jan 5 1970') : $event->startTime);
		}
	}

	// **** date
	if(isset($_POST['date']))
	{
		$date=trim($_POST['date']);
		require_once 'demosphere-date-time.php';
		$date=demosphere_date_time_parse_short($date,($event->startTime===0 ? false : date('H:i',$event->startTime)));
        // sanity check
        if($date>time()+3600*24*365*2){$date=false;}
		if($date===false)
		{
			$errors[]=t('Invalid date format.');
			$date=$event->startTime;
		}
		else
		{
			$changed['date']=true;
			$event->startTime=$date;
		}
	}

	// **** body
	if(isset($_POST['body']))
	{
		require_once 'htmledit/demosphere-htmledit.php';
		$body=$_POST['body'];
		// document cleanup generates dlib messages that can be confusing
		$oldMessages=val($_SESSION,'dlib_messages');
		$body=demosphere_htmledit_submit_cleanup($body);
		$newMessages=val($_SESSION,'dlib_messages');
		// FIXME : display messages to user
		if($oldMessages===false){unset($_SESSION['dlib_messages']);}else{$_SESSION['dlib_messages']=$oldMessages;}
		// Add Source : messsage received on... line if no sources present
		if(!preg_match('@<p[^>]*class=[^>]*demosphere-sources@',$body) &&
		   trim($body)!='')
		{
			require_once 'demosphere-date-time.php';
			$body.='<p class="demosphere-sources">';
			$body.=t('_source_').' : '.t('message received on !date',
										 ['!date'=>demos_format_date('approx-short-date-time',time())]);
			$body.="</p>";
		}
		$event->body=$body;
		// Check for link in sources
		demosphere_event_publish_form_add_source_link($event);
		$changed['body']=true;
	}

	// **** place->address
	if(isset($_POST['address']))
	{
		$place=$event->usePlace();
		if($place->isEmpty())
		{
			$place=new Place();
			$event->setPlace($place);
		}
		else 
		// if this place is used elsewhere, create a variant
		if($place->isUsedElsewhere($event->id))
		{
			$oldPlace=$place;
			$place=$oldPlace->createVariant();
			$event->setPlace($place);
			$place->referenceId=$oldPlace->referenceId;
		}
		// set address
		$place->address=Place::cleanupAddress($_POST['address']);
	}

	// **** city
	if(isset($_POST['city']))
	{
		$place=$event->usePlace();
		if($place->isEmpty())
		{
			$place=new Place();
			$event->setPlace($place);
		}
		else 
		// if this place is used elsewhere, create a variant
		if($place->isUsedElsewhere($event->id))
		{
			$oldPlace=$place;
			$place=$oldPlace->createVariant();
			$event->setPlace($place);
			$place->referenceId=$oldPlace->referenceId;
		}

		// ** now process city
		$cityName=trim(dlib_cleanup_plain_text($_POST['city'],false));
		// If this is a known city, just use it
		$cityId=City::findId($cityName);
		if($cityId!==false){$place->cityId=$cityId;}
		else
		{
			// This is an unknown city.
			// If the city previously used by his place is only used by this place, just replace the text for it.
			$ct=db_result("SELECT COUNT(*) FROM Place WHERE cityId=%d and id!=%d",
						  $place->cityId,$place->id);
			if($ct==0){$place->useCity()->name=$cityName;}
			else
			{
				// otherwise, create a new city
				$place->setCity(City::findOrCreate($cityName));
			}
		}
		if(function_exists('custom_demosphere_event_public_form_submit'))
		{
			custom_demosphere_event_public_form_submit($event);
		}
	}

	// ***********
	// *********** public form extra data
	// ***********
	// FIXME: This function is supposed to be general, these fields are specific to public form

	// **** source
	if(isset($_POST['source']))
	{
		$event->extraData['public-form']['source']=$_POST['source'];
		$changed['source']=true;
	}

	// **** price
	if(isset($_POST['price']))
	{
		$event->extraData['public-form']['price']=$_POST['price'];
		$changed['price']=true;
	}

	// **** remarks
	if(isset($_POST['remarks']))
	{
		$event->extraData['public-form']['remarks']=$_POST['remarks'];
		$changed['remarks']=true;
	}

	// **** contact
	if(isset($_POST['contact']))
	{
		$event->extraData['public-form']['contact']=$_POST['contact'];
		$changed['contact']=true;
	}

	// ***********
	// *********** repetitions form
	// ***********
	// FIXME: This function is supposed to be general, these fields are sepcific to repetitions form

	// ********** repetitionGroupId: repetitions form: change repetition reference
	if(isset($_POST['repetition_ref']))
	{
		db_query('UPDATE RepetitionGroup SET referenceId=%d WHERE id=%d',$event->id,$event->repetitionGroupId);
	}

	// ***********
	// *********** Save event and update logs
	// ***********
	if(count($errors)){$res['errors']=$errors;}
	else
	{
		$event->save();
		// Log changes to both event log
		// Avoid adding too many log entries.
		$lastLog=val($_SESSION,'demosphere_event_ajax_edit_update_lastlog');
		$log=[time(),md5($event->id.':::'.implode(':',array_keys($changed)))];
		if($lastLog[1]!==$log[1] || $log[0]>$lastLog[0]+60*5)
		{
			$_SESSION['demosphere_event_ajax_edit_update_lastlog']=$log;
			$logMsg='';
			foreach($changed as $field=>$msg){$logMsg.=$field.($msg!==true ? ':'.$msg :'').',';}
			$event->logAdd('changed: '.$logMsg);
			$nt='t';
			$event->save();
		}
	}
	$res['change_number']=$isStale ? $_POST['change_number'] : $event->changeNumber;
	return [$res,$changed];
}

//! For now this is only called when moderation status changes to a published state.
//! FIXME: should think of a more general validation approach.
//! Much of this is copied from demosphere_event_edit_form validate
function demosphere_event_ajax_edit_validate_publish($event,&$errors)
{
	global $demosphere_config;

	if(trim($event->usePlace()->address)=='')
	{
		$errors[]=t('to publish, you must fill the address field');
	}

	// only allow no keywords in titles of un-published events
	if($demosphere_config['require_keyword_in_event_title'] && strpos($event->htmlTitle,'<strong>')===false)
	{
		$errors[]=t('In the title, you must choose an important word that will be displayed in a different color on the frontpage. To select a word in the title, put an undescore before and after the word. The undescore is located on the top right part of your keyboard (above the minus sign). Example : this is a title with _a keyword_ that is important' );
	}

	// only allow unset city in un-published events
	if($event->usePlace()->cityId==0)
	{
		$errors[]=t('To publish, you must fill the city field' );
	}

	// only allow unset topic in un-published events
	if(count($event->getTopics())==0 && 
	   ($demosphere_config['topics_required']==='mandatory' || 
		($demosphere_config['topics_required']==='check-entered' && !($event->extraData['topic-was-set'] ?? false))
	   ))
	{
		$errors[]=t('To publish, you must select a topic.' );
	}

	// FIXME: ajax form has no way of setting departement (used by politis)
	//if(function_exists('custom_ajax_edit_validate_publish'))
	//{
	// 	$hookErrors=custom_ajax_edit_validate_publish($event);
	// 	$errors=array_merge($errors,$hookErrors);
	//}

}

?>