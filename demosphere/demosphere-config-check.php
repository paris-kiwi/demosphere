<?php
//! Displays a page with configuration errors.
//!The real work is done in demosphere_config_check()
function demosphere_config_check_page()
{
	global $currentPage;
	$currentPage->title=t('Configuration check');
	$out='';
	$out.='<h1 class="page-title">'.t('Configuration check').'</h1>';
	$req=[];
	demosphere_config_check('runtime',$req);

	$ok=count($req)==0;
	foreach($req as $name=>$val)
	{
		$out.='<h4>'.$val['severity'].':'.ent($name).'</h4>';
		$out.='<p>'.$val['description'].'</p>';
	}

	if(!variable_get('demosphere_config_check','') && $ok)
	{
		$out.=t('Configuration used to be broken but now seems ok.');
	}
	else
	if($ok){$out.=t('Configuration seems ok.');}
	
	variable_set('demosphere_config_check','',$ok);

	return $out;
}
//! Called from demosphere_cron() to set the demosphere_config_check variable.
//!The real work is done in demosphere_config_check()
function demosphere_config_check_cron()
{
	$req=[];
	demosphere_config_check('runtime',$req);
	$ok=true;
	foreach($req as $val){if($val['severity']==='ERROR'){$ok=false;}}
	variable_set('demosphere_config_check','',$ok);
}

/** Checks many aspects of configuration and fills an array with any errors.
 *
 * This is similar to @see http://api.drupal.org/api/function/hook_requirements
 * @param phase can be 'install' or 'runtime'. Some checks are different depending on phase.
 * @param the array that will be filled 
 */
function demosphere_config_check($phase,&$req,$args=false)
{
	global $base_url,$demosphere_config;
	$t=function_exists('t') ? 't' : function($s){return $s;};

	$v=explode('.',phpversion());
	if(intval($v[0])<5 || (intval($v[0])==5 && intval($v[1])<4))
	{
		_req_error('phpversion',$req,$t('Your PHP version is too old. You need PHP 5.4 or newer to run Demosphere.'));
	}

	if(!function_exists('imap_open'))
	{
		_req_error('imap',$req,$t('Your PHP installation does not seem to have the <strong>imap</strong> extension. You need to install imap for php. For example, if you are admin on certain ubuntu or debian systems you can type: apt-get install php5-imap'));
	}

	if(!function_exists('tidy_repair_string'))
	{
		_req_error('tidy',$req,$t('Your PHP installation does not seem to have the <strong>tidy</strong> extension. You need to install tidy. For example, if you are admin on certain ubuntu or debian systems you can type: apt-get install php5-tidy'));
	}
	if(!class_exists('XSLTProcessor'))
	{
		_req_error('xsl',$req,$t('Your PHP installation does not seem to have the <strong>xsl</strong> extension. You need to install xsl. For example, if you are admin on certain ubuntu or debian systems you can type: apt-get install php5-xsl'));
	}
	if(!class_exists('Normalizer'))
	{
		_req_error('xsl',$req,$t('Your PHP installation does not seem to have the <strong>intl</strong> extension. You need to install intl. For example, if you are admin on certain ubuntu or debian systems you can type: apt-get install php5-intl'));
	}
	if(!function_exists('curl_init'))
	{
		_req_error('curl',$req,$t('Your PHP installation does not seem to have the <strong>curl</strong> extension. You need to install curl. For example, if you are admin on certain ubuntu or debian systems you can type: apt-get install php5-curl'));
	}

	if(!extension_loaded('imagick'))
	{
		_req_error('imagick',$req,$t('Your PHP installation does not seem to have the <strong>imagick</strong> extension. You need to install imagick. For example, if you are admin on certain ubuntu or debian systems you can type: apt-get install php5-imagick'));
	}

// 	exec('which wget',$unused,$cmdRet);
// 	if($cmdRet)
// 	{
// 		_req_error('wget',$req,$t('Your system does not seem to have the <strong>wget</strong> command. You need to install wget. For example, if you are admin on certain ubuntu or debian systems you can type: apt-get install wget'));
// 	}

	exec('which convert',$unused,$cmdRet);
	if($cmdRet)
	{
		_req_error('imagemagickcl',$req,$t('Your system does not seem to have the <strong>convert</strong> command. You need to install command-line ImageMagick. For example, if you are admin on certain ubuntu or debian systems you can type: apt-get install imagemagick'));
	}

	exec('which pdfimages',$unused,$cmdRet);
	if($cmdRet)
	{
		_req_error('pdfutils',$req,$t('Your system does not seem to have the <strong>pdfimages</strong> command. You need to install command-line xpdf-utils or poppler-utils. For example, if you are admin on certain ubuntu or debian systems you can type: apt-get install xpdf-utils (or poppler-utils)'));
	}

	if($phase==='install'){demosphere_config_check_install($req,$args);}
	if($phase==='runtime'){demosphere_config_check_runtime($req);}

	// mail import settings
	require_once 'mail-import/mail-import.php';
	mail_import_config_check($phase,$req);
	// feed import data dir
	require_once 'feed-import/feed-import.php';
	feed_import_config_check($phase,$req);
}

function demosphere_config_check_install(&$req,$args)
{
}
function demosphere_config_check_runtime(&$req)
{
	global $demosphere_config,$base_url;
	// sanity check for demosphere_config values
	//if($demosphere_config['std_base_url']!=$base_url)
	//{
	// 	_req_error('std_base_url',$req,'demosphere-config: '.
	// 			   'std_base_url is set to '.$demosphere_config['std_base_url'].
	// 			   ' but should be '.$FIXME); information from demosphere_sites() is no longer available
	//}

	// infoboxes
	$infoboxes=$demosphere_config['front_page_infoboxes'];
	foreach($infoboxes as $box) 
	{
		if(Post::fetch($box['pid'],false)===null)
		{
			_req_error('infobox-'.$box['pid'],$req,
					   'config problem: infobox pid is set to '.
					   '<a href="'.$base_url.'/post/'.$box['pid'].'">'.$box['pid'].'</a>'.
					   ' but that Post does not exist. You probably need to '.
					   '<a href="'.$base_url.'/post/add">create</a> '.
					   '(<a href="https://paris.demosphere.net/post/'.$box['pid'].'">example</a>) '.
					   'that Post and/or change the config file.');
		}
	}

	// data dirs in files directory
	demosphere_config_check_file('files2',$req,'files/docs');
	demosphere_config_check_file('files3',$req,'files/maps');

	$data='files/private';
	$locale=dlib_get_locale_name();
	demosphere_config_check_file('filesdata',$req,$data);

	// word frequencies
	$wordFrequencies=variable_get('word_freq','',[]);
	if(count($wordFrequencies)==0)
	{
		$url=$base_url.'/maintenance/update-place-search-word-freq';
		_req_error('word frequencies',$req,'Word frequencies are not set. Place searches will not return reliable results. '.
				   'You should visit the following page once you have enough events to build reliable statistics (the page load can take a few minutes).<br/>'.
				   '<a href="'.ent($url).'">'.ent($url).'</a>');
	}

	// using default translations that should be customized
	$refCities=['es'=>['siteId'=>'valladolid','city'=>'valladolid'],
				'fr'=>['siteId'=>'paris'     ,'city'=>'paris'      ]];
	$badCity='paris';
	if(isset($refCities[$locale]))
	{
		$badCity=$refCities[$locale]['city'];
		if($refCities[$locale]['siteId']===$demosphere_config['site_id']){$badCity='NONEXISTINGTRANSLATION1234565';}
	}
	$badTranslations=db_one_col("SELECT id,translation FROM translations WHERE (".
								"LOWER(CONVERT(translation  USING utf8mb4)) LIKE '%%%s%%' OR ".
								"translation LIKE '%%FIXME%%' ) AND translation=msgstr",
								$badCity);
	foreach($badTranslations as $id=>$translation)
	{
		_req_error('translation-'.$id,$req,t('You are using the default translation for: !translation.<br/>This translation needs to be !link changed here</a>',['!translation'=>'<em>'.ent($translation).'</em>','!link'=>'<a href="'.$base_url.'/translation-backend/translation/'.$id.'/edit">']).'<br/>');
	}

	// MX domain 
	if($demosphere_config['hosted'])
	{
		require_once 'demosphere-emails.php';
		$domain=demosphere_emails_hosted_domain();
		if($domain!==false)
		{
			if(checkdnsrr($domain,"MX")===false)
			{
				_req_error('domain-no-mx',$req,t('DNS configuration for the domain @domain does not contain an MX entry. You will not be able to receive email. Please contact the server admin.',['@domain'=>$domain]));
			}
			else
			{
				getmxrr($domain,$mxhosts);
				$mxip=gethostbyname($mxhosts[0]);
				$serverIp=$_SERVER["SERVER_ADDR"];
				if($mxip!==$serverIp)
				{
					_req_error('domain-mx-ip',$req,t('The DNS MX field for @domain resolves to @mxip which is different than the server\'s IP @serverIp. You will not be able to receive email. Please contact the server admin.',['@domain'=>$domain,'@mxip'=>$mxip,'@serverIp'=>$serverIp]));
				}
			}
		}
	}

	// check sphinx search setup 
	require_once 'demosphere-search.php';
	try{demosphere_search_proxy_call('demosphere_search_dx_describe');}
	catch(Exception $e)
	{
		_req_error('sphinxsearch',$req,t('Sphinxsearch is not working, please contact server admin.').' Error: '.ent($e->getMessage()));
	}
}


function demosphere_config_check_file($name,&$req,$path,$type='exists,writable')
{
	if(strpos($type,'exists'  )!==false && !file_exists($path))
	{
		_req_error($name.'-exists',$req,
				   '"'.$name.'" : config problem : file or dir does not exist : '.$path);
	}
	if(strpos($type,'writable')!==false && !is_writable($path))
	{
		_req_error($name.'-writable',$req,
				   '"'.$name.'" : config problem : file or dir is not writable : '.$path);
	}	
}

function _req_error($name,&$req,$message)
{
	$req[$name]['description'] = $message;
	$req[$name]['severity'] = 'ERROR';
}

?>