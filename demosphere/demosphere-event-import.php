<?php

//! Creates an event and redirects to the event edit form. 
//! Called from mail-import and feed-import via dcomponent_import_to_demosphere().
//! $dataSrc is used to add "Source:" line and logging (examples: 'article-123' or 'message-8765') 
function demosphere_event_import_create_event(string $title,string $body,$dataSrc=false)
{
	$src=demosphere_event_import_src($dataSrc);
	$title=demosphere_event_import_cleanup_title($title);
	$body =demosphere_event_import_cleanup_body($body);
	$body.=demosphere_event_import_source_paragraph($src);

	$event=new Event(['title'=>$title,'htmlTitle'=>ent($title),'body'=>$body,'startTime'=>0]);
	demosphere_event_import_log_event_creation($event,$src,false);
	$event->placeId=1;
	$event->save();
	dlib_redirect($event->url().'/edit');
}

//! Returns an array with information about the source.
//! $dataSrc='article-123' or 'message-8765'
//! Empty array is returned on failure.
function demosphere_event_import_src($dataSrc)
{
	require_once 'dcomponent/dcomponent-common.php';
	if(!preg_match('@^(article|message|page)-([0-9]+)$@',$dataSrc,$m)){return [];}
	$type=     $m[1];
	$id  =(int)$m[2];
	$res=[
			'type'=>$type,
			'id'  =>$id,
		 ];
	switch($type)
	{
	case 'article':
		$article=Article::fetch($id,false);
		if($article===null){return [];}
		$res['url']=$article->url;
		$email=$article->useFeed()->email;
		if($email!=''){$res['email']=$email;}
		break;
	case 'message':
		$message=Message::fetch($id,false);
		if($message===null){return [];}
		$res['source']=$message->getSourceOrDefault();
		$res['email' ]=$message->from;
		break;
	case 'page':
		$page=Page::fetch($id,false);
		if($page===null){return [];}
		$res['url']=$page->url;
		break;		
	}
	return $res;
}

function demosphere_event_import_src_ajax()
{
	return demosphere_event_import_src($_GET['dataSrc'] ?? '');
}

//! Encode a $src array into a $dataSrc string.
//! ['type'=>...,'id'=>..., ...]  => 'message-123'
function demosphere_event_import_data_src(array $src)
{
	if(isset($src['type'])){return $src['type'].'-'.$src['id'];}
	return false;
}


//! Add information to an Event's log saying how / from where it was created.
function demosphere_event_import_log_event_creation(Event $event,array $src,bool $isSelection,$srcUrl=false)
{
	global $base_url;

	$log=false;
	switch($src['type'] ?? false)
	{
	case false:$log='imported selection from '.$srcUrl;
		break;
	case 'message':
		require_once 'mail-import/mail-import.php';
		$message=Message::fetch($src['id']);
		$log='imported '.($isSelection ? 'selection from ':'').'email '.$src['id']."\n".
			$message->url()."\n".
			'MailFrom:'.$message->from;
		break;
	case 'article':
		$log='imported '.($isSelection ? 'selection from ':'').'feed article '.intval($src['id']).' from '.$src['url'];
		break;
	case 'page':
		$log='imported '.($isSelection ? 'selection from ':'').'pagewatch page '.intval($src['id']).' from '.$src['url'];
		break;
	}
	if(isset($src['email']) && !isset($message))
	{
		$log.="\n"."Alt email: ".$src['email'];
	}
	if($log!==false){$event->logAdd($log);}
}

//! Removes the "file" part at the end of a url.
//! https://aaa.bb/ccc/ddd/eee => https://aaa.bb/ccc/ddd
function demosphere_event_import_baseurl(string $url)
{
	preg_match('@(https?://)([^/]+)(/[^/]*)*@',$url,$matches);
	if(isset($matches[3])){$url=substr($url,0,-strlen($matches[3]));}
	return $url;
}

function demosphere_event_import_guess_title(string $contents,string $url)
{
	global $demosphere_config;
	$site=$url;
	$site=preg_replace('@https?://(www[.]|)([^/]*).*@','$2',$site);
	// this is an import from own site
	// can be mail, feed or, less commonly, another page
	if(strpos($url,$demosphere_config['safe_base_url'])===0)
	{
		if(preg_match('@/mail-import/message/[0-9]+@',$url)){$site='mail';}
		if(preg_match('@/feed-import/article/[0-9]+@',$url)){$site='feed';}
	}
	$title=$contents;
	$title=t("selection:").' '.$site.' : '.$title;
	$title=demosphere_event_import_cleanup_title($title);
	return $title;
}

function demosphere_event_import_cleanup_title(string $title)
{
	// we only want simple text in the title
	$title=dlib_fast_html_to_text($title);
	$title=dlib_cleanup_plain_text($title);
	$title=preg_replace("@[\s]+@s"," ",$title);
	$title=trim($title);
	$title=dlib_truncate($title,95);
	if(strlen($title)==0){$title=t('(no title)');}
	return $title;
}

//! Cleanup html when importing
//! Most of the real work is done by demosphere_htmledit_submit_cleanup()
function demosphere_event_import_cleanup_body(string $body,$url=false,$htmlBaseHref=false)
{
	global $demosphere_config;

	// determine if we are importing from demosphere (reimporting)
	$isDemosphere=strpos($url,$demosphere_config['std_base_url' ])===0 || 
		          strpos($url,$demosphere_config['safe_base_url'])===0;
	$isDemosphere=$isDemosphere && !(strpos($url,'/mail-import')!==false ||
									 strpos($url,'/feed-import')!==false ||
									 strpos($url,'/page-watch' )!==false	   );

	// if reimport from demosphere remove hr part numbers
	if($isDemosphere){$body=preg_replace('@<div class="textNav".*</div>@sU','',$body);}

	// fix bullets, including spip <img puce> => ul/li
	require_once 'htmledit/demosphere-htmledit.php';
	$bullets=demosphere_htmledit_list_bullets();
	unset($bullets[array_search('-',$bullets)]); // remove "-" ... too many false positives
	$bullets=implode('|',$bullets);
	// fix bullets, including spip <img puce> => ul/li
	$body=preg_replace('@<(br|p)[^>]*>\s*(<strong[^>]*>\s*)?(<img[^>]*\bpuce\b[^>]*>|'.$bullets.')(.*)(?=(</(p|h[0-9])>|<br\b[^>]*>))@sU',
					   "\n".'<ul><li>$2$4</li></ul>'."\n",$body);

	// get clean html before we do more regex parsing
	try
	{
		require_once 'dlib/html-tools.php';
		$body  =dlib_html_cleanup_tidy_xsl($body);
	}
	catch(Exception $e){$body=t('Import failed (invalid document in cleanup)');}

	// fix relative/absolute urls (adds host url)
	if($url!==false || $htmlBaseHref!==false)
	{
		require_once 'dlib/html-tools.php';
		$body=dlib_html_links_to_urls($body,$htmlBaseHref===false ? $url : $htmlBaseHref);
	}
	// add "subsection" classname to all hr that are not from demosphere
	if(!$isDemosphere){$body=preg_replace('@<hr\b@','<hr class="subsection"',$body);}
	// call the standard demosphere html cleanup function
	require_once 'htmledit/demosphere-htmledit.php';
	$body=demosphere_htmledit_submit_cleanup($body);
	// remove multiple (more than 2) <br/>
	$body=preg_replace("@(<br\s*/?>\s*){3,}@",
					   "<br/>\n<br/>\n",$body);

	$body=dlib_html_cleanup_tidy_xsl($body);

	return $body;
}

//! Returns "Source:" paragraph 
function demosphere_event_import_source_paragraph(array $src=[],$url=false)
{
	$res='';

	$srcUrl=$src['url'] ?? $url;

	// data-demosphere-paste is used  by js  in  demosphere-event-edit-form to
	// recognize copy/pasted data from emails. 
	$dataSrc=demosphere_event_import_data_src($src);

	$res.='<p '.($dataSrc!==false ? 'data-demosphere-paste="'.ent($dataSrc).'" ' : '').'>';
	$res.=t('_source_').' : ';
	if(isset($src['source']))
	{
		// $src['source'] can contain html
		require_once 'dlib/filter-xss.php';
		$res.=filter_xss_admin($src['source']);
	} 
	else
	if($srcUrl!==false)
	{
		$shortUrl=urldecode($srcUrl);
		$shortUrl=dlib_cleanup_plain_text($shortUrl,false,false);// urldecode can make invalid utf8
		$shortUrl=dlib_truncate($shortUrl,45);
		$res.='<a href="'.ent($srcUrl).'">'.ent($shortUrl).'</a></p>';
	}
	$res.='</p>';
	
	return $res;
}

//! Ajax query from webextension to create an event from a selection. 
//! The event is then opened in a new tab by the webextension.
function demosphere_webextension_create_event()
{
 	$srcUrl   =$_POST['srcUrl'];
 	$selection=$_POST['selection'];
	// create title and cleanup
	$title=demosphere_event_import_guess_title($selection,$srcUrl);
	$dataSrc  =$_POST['dataSrc'];
	$base     =$_POST['base'] ?? '';
	if($base===''){$base=false;}
	
	$src  =demosphere_event_import_src($dataSrc);
	$body =demosphere_event_import_cleanup_body($selection,$srcUrl,$base);
	$body.=demosphere_event_import_source_paragraph($src,$srcUrl);

	// create event
	$event=new Event(['title'=>$title,
					  'htmlTitle'=>ent($title),
					  'body'=>$body,
					  'startTime'=>0]);
	$event->placeId=1;
	demosphere_event_import_log_event_creation($event,$src,true,$srcUrl);
	$event->save();
	return ['eventId'=>$event->id,'eventUrl'=>$event->url()];
}


//! Ajax request from webextension : cleanup selection for copy/paste .
//! Adds "Source:" and imports images.
function demosphere_webextension_selection_cleanup()
{
	$srcUrl  =$_POST['srcUrl'];
	$contents=$_POST['selection'] ?? '';
	$dataSrc =$_POST['dataSrc']   ?? '';
	$base    =$_POST['base']      ?? '';
	if($base===''){$base=false;}

	$src=demosphere_event_import_src($dataSrc);
	$res=demosphere_event_import_cleanup_body($contents,$srcUrl,$base);
	$res.=demosphere_event_import_source_paragraph($src,$srcUrl);
	return $res;
}

//! Ajax request from webextension : highlight future dates and keywords in page.
function demosphere_webextension_highlight()
{
	require_once 'demosphere-common.php';
	$str=$_POST['contents'];
	// use tag to avoid highlighting several times
	if(strpos($str,"demosphere-is-highlighted-tag")!==false){return $str;}
	$res=highlight_future_dates_and_keywords($str);
	$res.='<div id="demosphere-is-highlighted-tag"></div>';
	return $res;
}

//*******************************************************
//****** temp documents 
//****** (used for finding similar docs and diff, notably using selection from extension )
//*******************************************************


//! Display a tmpdoc and documents similar to it
//! This is just a wrapper around text_matching_view_similar_docs()
function demosphere_tmpdoc(int $id)
{
	$tmpdoc=variable_get($id,'tmpdoc');
	if($tmpdoc===false){dlib_not_found_404();}
	
	require_once 'text-matching/text-matching.php';
	require_once 'text-matching/text-matching-view.php';
	$tm=TMDocument::getByTid('tmpdoc',$id);
	return text_matching_view_similar_docs($tm);
}

//! Create a tmpdoc. This is called through Ajax by webextension.
function demosphere_tmpdoc_add()
{
	$url=$_POST['url'] ?? false;
	if(!isset($_POST['html'])){dlib_bad_request_400();}
	$html=$_POST['html'];

	// *** html cleanup
	// Note that filter_xss is used at display time, so this is more about appearances and "extra safety".
	require_once 'dlib/html-tools.php';
	$html=dlib_html_links_to_urls($html,$url!==false ? $url : 'https://url.invalid' );
	// big cleanup
	$html=dlib_html_cleanup_tidy_xsl($html);
	$doc=dlib_dom_from_html($html,$xpath);
	// Extra: Remove all attributes except href
	foreach($xpath->query("//@*") as $attr){if($attr->name!=='href'){$attr->ownerElement->removeAttribute($attr->name);}}
	// Extra: Remove images
	foreach($xpath->query("//img") as $img){$img->parentNode->removeChild($img);}
	$html=dlib_dom_to_html($doc);
	$html=dlib_tidy_repair_string($html,dlib_html_cleanup_tidy_xsl_config(),"utf8");
	// Just in case
	require_once 'dlib/filter-xss.php';
	$html=filter_xss_admin($html);

	// Save tmpdoc and add it to TM index
	$id=1+intval(db_result("SELECT MAX(CAST(name as SIGNED)) FROM variables WHERE type='tmpdoc'"));
	$doc=[
		'url'=>$url,
		'html'=>$html,
		'created'=>time(),
		];
	variable_set($id,'tmpdoc',$doc);
	$tm=new TMDocument('tmpdoc',$id);
	$tm->save();
	$tm->addToIndex();
	return $id;
}


function demosphere_tmpdoc_text_matching_hook(string $op,$arg1=null,$arg2=null)
{
	global $demosphere_config,$base_url,$currentPage;

	// operations that don't need $tmpdoc
	switch($op)
	{
	case 'allTids': return array_keys(variable_get_all('tmpdoc'));
	case 'displayListInit':
		$currentPage->addJs('demosphere/dcomponent/dcomponent-dcitems.js');
		return;
	}

	$id=intval($arg1);
	$tmpdoc=variable_get($id,'tmpdoc');

	switch($op)
	{
	case 'isExpired':
		return $tmpdoc===false || $tmpdoc['created']<time()-3600*24;
	case 'matchInfo': 
		return ['class'=>'',
				'title'=>'tmpdoc'.$id,
				'url'  =>$base_url.'/tmpdoc/'.$id,
			   ]; 
	case 'getContents':
		if($tmpdoc===false){return false;}
		return [$tmpdoc['html'],'html'];
	case 'displayInList': 
		if($tmpdoc===false){echo '<p>tmpdoc '.$id.' does not exist</p>';return;}
		echo template_render('text-matching/text-matching-similar-doc.tpl.php',
							 [
								 'content' => $tmpdoc['html'],
								 'type'    => 'tmpdoc',
								 'typeId'  => $id,
								 'title'   => t('temporary document').' - '.$id,
								 'titleUrl'=> $tmpdoc['url']!==false ? $tmpdoc['url'] : $base_url.'/tmpdoc/'.$id,
							 ]
							);
		return;
	}
}

?>