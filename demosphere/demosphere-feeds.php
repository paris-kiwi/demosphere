<?php
// ***************************************************
// *************  RSS feeds	 **************
// ***************************************************

/**
 * Displays an rss feed of future events.
 * This is called from path /events.xml (see demosphere_paths), with optional arguments:
 * http//example.com/events.xml?example_argument=example-value
 *	- ?terms=4  (replace depreciated ?stopic )
 *	- ?title_city=	   (0 or 1 to display city in title)
 *	- ?nb_items=
 *	- ?userCalendar=   (user customized calendar)
 *	etc..
 */
function demosphere_feeds_rss() 
{
	global $base_url,$demosphere_config;
	require_once 'demosphere-event-list.php';

	// "stopic" and selectTerm are deprecated. instead use "selectTopic"
	// stopic: 'x' separated list of terms (tid's)

	$topicNames=Topic::getAllNames();
	
	$rawOpts=$_GET;

	// replace old (deprecated option names)
	$rename=['stopic'=>'selectTopic',
			 'lat_lng_dist'=>'nearLatLng',
			 'nb_items'=>'limit',
			 'perso'=>'userCalendar',
			 'start_time'=>'useEventDateNotPub',
			 'title_location'=>'title_city',
			 'terms'=>'selectTerms',
			];
	foreach($rename as $old=>$new)
	{
		if(isset($rawOpts[$old])){$rawOpts[$new]=$rawOpts[$old];unset($rawOpts[$old]);}
	}

	// Backwards compatibility for selectTerms => selectTopic & selectCityId
	if(isset($rawOpts['selectTerms']))
	{
		$rawOpts['selectTerms']=str_replace('x'    ,',',$rawOpts['selectTerms']);
		$rawOpts['selectTerms']=str_replace('-and-',',',$rawOpts['selectTerms']);
		if(strpos($rawOpts['selectTerms'],',')!==false){dlib_bad_request_400('selectTerms is no longer supported');}

		// distinguish between old terms that become new terms, topic or city
		$tid=intval($rawOpts['selectTerms']);
		$vocab=db_result_check('SELECT vocab FROM Term WHERE id=%d',$tid);
		if($vocab!==false){$rawOpts['selectVocabTerm']=''.$tid;}
		else
		if(isset($topicNames[$tid])){$rawOpts['selectTopic' ]=''.$tid;}
		else                        {$rawOpts['selectCityId']=''.$tid;}
		unset($rawOpts['selectTerms']);
	}

	// Backwards compatibility for selectTermsExpression => selectVocabTermOr
	if(isset($rawOpts['selectTermsExpression']))
	{
		$rawOpts['selectVocabTermOr']=$rawOpts['selectTermsExpression'];
		unset($rawOpts['selectTermsExpression']);
	}

	// parse all generic options
	$parseRes=demosphere_event_list_parse_getpost_options($rawOpts);
	$rawOpts=$parseRes['notParsed'];
	$options=$parseRes['options'];

	// time_format: integer: which strftime format defined in config:rss_time_formats
	$timeFormatNb=val($_GET,'time_format',1);
	if(!ctype_digit((string)$timeFormatNb) || !isset($demosphere_config['rss_time_formats'][$timeFormatNb])){dlib_bad_request_400('invalid time_format');}
	$timeFormat=$demosphere_config['rss_time_formats'][$timeFormatNb];
	$startTimeInTitle=$timeFormatNb!=0;

	// useEventDateNotPub: 0/1 on whether or not to replace pubDate by event's startTime
	$useEventDateNotPubConf=$demosphere_config['rss_use_eventdate_instead_of_pubdate'];
	$useEventDateNotPub=(boolean)$useEventDateNotPubConf;
	if(isset($_GET['useEventDateNotPub'    ])){$useEventDateNotPub=(boolean)$_GET['useEventDateNotPub'];}
	if(isset($_GET['use_event_date_not_pub'])){$useEventDateNotPub=(boolean)$_GET['use_event_date_not_pub'];}

	// title_city: 0/1 on whether or not to display event city in title
	$cityInTitle=(boolean)val($_GET,'title_city',true);

	// always limit the number of events in the feed
	if(!isset($options['limit'])){$options['limit']=30;}

	// endAfter (like endTime, but relative to startTime). Requested by commingues.demosphere.net
	if(isset($rawOpts['endAfter'])){$options['endTime']=val($options,'selectStartTime',Event::today())+intval($rawOpts['endAfter']);}

	$useDescription=isset($_GET['useDescription']);

	$badOpts=array_diff(array_keys($rawOpts),['time_format','rss_time_formats','useEventDateNotPub','use_event_date_not_pub','title_city','limit','endAfter','useDescription',]);
	if(count($badOpts)){dlib_bad_request_400('Unknown options: '.implode(',',$badOpts));}

	// rebuild url of this channel 
	$channelLink=$base_url."/events.xml?time_format=$timeFormatNb".
		"&useEventDateNotPub=".($useEventDateNotPub ? 1 : 0).
		"&title_city=".($cityInTitle ? 1 : 0);
	foreach($options as $name=>$val)
	{
		$channelLink.='&'.$name.'='.(is_array($val) ? implode(',',$val): $val);
	}

	if($useDescription){$options['body']=true;}
	$options['created'         ]=true;
	$options['lastBigChange' ]=true;
	$options['place__zoom'     ]=true;
	$options['place__latitude' ]=true;
	$options['place__longitude']=true;
	$events=demosphere_event_list($options);

	require_once 'demosphere-date-time.php';
	$out=template_render('templates/demosphere-feeds-rss.tpl.php',
						 [compact('channelLink','events','topicNames','useEventDateNotPub','startTimeInTitle','timeFormat','cityInTitle','useDescription'),
						  'templateHtmlEntitiesFunction'=>'xent']);
	return ['out'=>$out,
			'headers'=>['Content-Type'=>'application/rss+xml; charset=utf-8'],
		   ];
}


/** 
 * Returns html for a small pure js gui that builds a url of a rss or ical feed.
 *
 * Use with 
 * "demosphere_dtoken_rss_feed_url_builder" or
 * "demosphere_dtoken_ical_feed_url_builder".
 * The dtoken can be inserted into any Event or Post.
 * $type can be 'ical' or 'rss'
 */
function demosphere_feeds_rss_url_builder($type)
{
	global $demosphere_config,$base_url,$currentPage;

	require_once 'demosphere-date-time.php';
	require_once 'demosphere-event-list-form.php';
	// rss feed builder needs extra js/css
	$currentPage->addJs ('feed_builder_type="'.$type.'";','inline');
	$currentPage->addJs ("lib/jquery.js");
	$currentPage->addCssTpl('demosphere/css/demosphere-event-list-form.tpl.css');
	$currentPage->addJs ("demosphere/js/demosphere-event-list-form.js");
	$currentPage->addCssTpl('demosphere/css/demosphere-feeds-rss-url-builder.tpl.css');
	$currentPage->addJs ("demosphere/js/demosphere-feeds-rss-url-builder.js");

	$currentPage->addCss('lib/leaflet/leaflet.css');
	$currentPage->addJs('lib/leaflet/leaflet.js');

	$examplePubTime=strtotime("5 days ago 10am");
	$exampleEventTime=strtotime("+10 days 20:00");


	$currentPage->addJs ("demosphere/js/demosphere-event-list-form.js");
	$currentPage->addCssTpl('demosphere/css/demosphere-event-list-form.tpl.css');
	
	$out='';
	$out.='<div class="rss-feed-url-builder">';
	require_once 'dlib/form.php';
	$out.=form_render(demosphere_feeds_rss_url_builder_form($type),
					  ['id'=>'rss-url-builder-form']);
	$out.='</div>';


	$out.='<p><strong>'.t('Feed url:').'</strong><br/>';
	$out.='<a href="" id="rss-url"></a></p>'."\n";
	if($type=='rss')
	{
		$out.='<p id="rss-feed-example">';
		$out.='<strong>'.t('Example:').'</strong><br/>';
		$out.='<span id="example-title-time"></span>';
		$out.='<span id="example-title-city">'.t('New York').' - </span>';
		$out.='<span id="example-title">'.t('protest against low salaries').'</span>';
		$out.='<br/>';
		$out.=t('date published: ').
			'<span id="example-pubdate">'.
			demos_format_date('full-date-time',$examplePubTime).
			'</span>';	
		$out.='<span id="example-pubdate-startime">'.
			demos_format_date('full-date-time',$exampleEventTime).
			'</span>';	
		$out.='</p>';
	}
	return $out;
}
/**
 * Builds a drupal form for the feed url builder.
 */
function demosphere_feeds_rss_url_builder_form($type)
{
	global $demosphere_config,$currentPage;
	$items=[];
	$values=[];
	
	$exampleEventTime=strtotime("+10 days 20:00");

	$timeFormats=[];
	foreach($demosphere_config['rss_time_formats'] as $nb=>$format)
	{
		$timeFormats[$nb]=($nb==0 ? ent($format):
			 (strpos($format,'|')===0 ? 
			  demos_format_date(substr($format,1),$exampleEventTime) : 
			  strftime($format,$exampleEventTime)));
	}

	$items['time-format']=
		['type' => 'select',
		 'title' => t('date/time format in the title'),
		 'options' => $timeFormats,
		 'default-value'=>1,
		];

	$items['use-event-date-not-pub']=
		['type' => 'checkbox',
		 'title' => t("replace published date by event's date"),
		 'default-value' => $demosphere_config['rss_use_eventdate_instead_of_pubdate'],
		];

	$items['title-city']=
		['type' => 'checkbox',
		 'title' => t('should the city / district be shown in the title?'),
		 'default-value' => true,
		];

	if($type==='rss')demosphere_event_list_form_item('limit'   ,$values,$items);
	demosphere_event_list_form_item('select-topic'             ,$values,$items);
	demosphere_event_list_form_item('select-city-id'           ,$values,$items);
	demosphere_event_list_form_item('select-fp-region'         ,$values,$items);
	demosphere_event_list_form_item('select-place-reference-id',$values,$items);
	demosphere_event_list_form_item('near-lat-lng'             ,$values,$items);
	demosphere_event_list_form_item('more-options'             ,$values,$items);
	demosphere_event_list_form_item('map-shape'                ,$values,$items);
	demosphere_event_list_form_item('select-vocab-term'        ,$values,$items);
	demosphere_event_list_form_item('select-vocab-term-or'     ,$values,$items);
	if($type!=='rss')demosphere_event_list_form_item('limit'   ,$values,$items);
	demosphere_event_list_form_item('max-nb-events-per-day'    ,$values,$items);
	demosphere_event_list_form_item('most-visited'			   ,$values,$items);
	$items['more-options-end']=['html'=>'</div></div>'];

	$items['limit']['default-value']=($type==='ical' ? '' : 30);

	if($type==='ical')
	{
		unset($items['time-format']);
		unset($items['use-event-date-not-pub']);
		unset($items['title-city']);
	}

	foreach($items as $name=>$formItem)
	{
		if(isset($formItem['default-value']))
		{$defaults[$name]=$formItem['default-value'];}
	}

	$currentPage->addJsVar('rss_builder_defaults',$defaults);
	$currentPage->addJsConfig('demosphere',['map_center_latitude','map_center_longitude','map_zoom','mapbox_access_token']);

	return $items;
}

// ***************************************************
// *************  calendar format files and feeds ****
// ***************************************************

function demosphere_feeds_icalendar()
{
	$calendar=demosphere_feeds_icalendar_object();
	//$calendar->returnCalendar();
	// This is copied from vcalendar::returnCalendar() because we need those headers
	// this is an autogenerated filename (example: 20130311163815.ics). What is it for? (seems necessary)
    $filename = $calendar->getConfig( 'filename' ); 
	return ['out'=>$calendar->createCalendar(),
			'headers'=>['Content-Disposition'=>'attachment; filename="'.$filename.'"',
						'Content-Type'=>'text/calendar; charset=utf-8']];
}

function demosphere_feeds_icalendar_object($singleEvent=false)
{
	global $base_url,$demosphere_config;
	require_once 'lib/iCalcreator/iCalcreator.php';
	// Warning: iCalcreator sets timezone in toplevel php!!!! :-( !!!
    // FIXME: this no longer seems to be the case for 2.22.1
	// we reset timezone here:
	date_default_timezone_set($demosphere_config['timezone']);

	if($singleEvent===false)
	{
		$rawOpts=$_GET;
		// replace old (deprecated option names)
		$rename=['perso'=>'userCalendar',];
		foreach($rename as $old=>$new)
		{
			if(isset($rawOpts[$old])){$rawOpts[$new]=$rawOpts[$old];unset($rawOpts[$old]);}
		}

		// parse all generic options
		require_once 'demosphere-event-list.php';
		$parseRes=demosphere_event_list_parse_getpost_options($rawOpts);
		$options=$parseRes['options'];
	}
	$calendar = new vcalendar(['unique_id'=>'demosphere.net', // product id (not unique id for this feed)
							   'TZID'=>$demosphere_config['timezone'],
							  ]);
	$calendar->setProperty("method", "PUBLISH"); 
	if($singleEvent===false)
	{
		$calendar->setProperty("X-WR-CALNAME",$demosphere_config['site_name']); 
		$calendar->setProperty("X-WR-CALDESC",$demosphere_config['site_slogan']);
		$tmp=$rawOpts;
		unset($tmp['q']);
		$uuid=md5($base_url.':'.serialize($tmp)); 
		$calendar->setProperty( "X-WR-RELCALID", $uuid );
	}
	$calendar->setProperty("X-WR-TIMEZONE",$demosphere_config['timezone']);

	if($singleEvent!==false)
	{
		$events=[$singleEvent];
	}
	else
	{
		// fetch events from database
		require_once 'demosphere-event-list.php';
		$options['place__address']=true;
		$events=demosphere_event_list($options);
	}

	foreach($events as $event)
	{
		$calEvent = new vevent();
		$hasTime=date('G',$event->startTime).':'.date('i',$event->startTime) != '3:33';
		$calEvent->setDtstart(['timestamp'=>$event->startTime],
		 					  $hasTime ? false : ["VALUE"=>"DATE"]);
		if($hasTime)
		{
			$calEvent->setDtend(['timestamp'=>$event->startTime+3600]);
		}

		$calEvent->setDtstamp(['timestamp'=>$event->startTime]);
		$calEvent->setUid($event->url());
		$calEvent->setDescription(t('more info here: !url',['!url'=>$event->url()]));
		$calEvent->setSummary($event->title);
		$calEvent->setLocation($event->usePlace()->getCityName().", \n".
							   str_replace(["\r","\n"],[""," \n"],$event->usePlace()->address));
		if($event->usePlace()->zoom!==0)
		{
			$calEvent->setGeo($event->usePlace()->latitude,
							  $event->usePlace()->longitude);
		}
		$calEvent->setUrl($event->url());
		$calendar->addComponent($calEvent);
	}
	return $calendar;
}

// ***************************************************
// *************  Sitemap for search engines ****
// ***************************************************

function demosphere_feeds_sitemap()
{
	global $demosphere_config,$custom_config;
	$out='';
	$out.='<?xml version="1.0" encoding="UTF-8"?>'."\n".
		'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
	// *** frontpage
	$out.=demosphere_feeds_sitemap_url('',time(),'hourly',1);

	// ********** events **********
	require_once 'demosphere-event-list.php';
	$events=Event::fetchList('SELECT %no[log,body,extraData] '.
							 'FROM Event '.
							 'WHERE startTime>%d AND status=1 AND '.
							 'showOnFrontpage=1 '.
							 'ORDER BY startTime ASC ',Event::today());
	// when events become too old.  Add  a 1 day delay to avoid google
	// complaining that sitemap containts redirects.
	global $demosphere_config;

	foreach($events as $event)
	{
		//print_r($event);
		$loc=$demosphere_config['event_url_name'].'/'.$event->id;
		$priority=1;
		// old events will soon be archived, so keep checking 
		$changefreq='weekly';

		$changefreq='daily' ;
		// change frequency
		if($event->startTime > time()+20*24*3600){$changefreq='weekly';}

		$out.=demosphere_feeds_sitemap_url($loc,$event->changed,$changefreq,$priority);
	}

	// ********** posts **********
	// Only publish posts that have a path
	$posts=Post::fetchList('WHERE status>0');
	$paths=db_one_col('SELECT src,dest FROM path_alias');
	foreach($posts as $post)
	{
		$loc='post/'.$post->id;
		if(isset($paths[$loc])){$loc=$paths[$loc];}
		else{continue;}
		$out.=demosphere_feeds_sitemap_url($loc,$post->changed,'monthly',.5);
	}
	if(isset($custom_config['sitemap'])){$custom_config['sitemap']($out);}
	$out.='</urlset>'."\n";
	header('Content-Type: application/xml; charset=utf-8'); 
	echo $out;	
}

function demosphere_feeds_sitemap_url($loc,$lastmod=false,$changefreq=false,$priority=false)
{
	global $base_url;
	$out='';
	$out.='<url>'."\n";
	$out.='  <loc>'.$base_url.'/'.xent($loc)."</loc>\n";
	if($lastmod   !==false)
	{
		$out.="  <lastmod>".xent(date(/*'Y-m-d'*/'c',$lastmod))."</lastmod>\n";
	}
	if($changefreq!==false)
	{
		$out.='  <changefreq>'.$changefreq."</changefreq>\n";
	}
	if($priority  !==false)
	{
		$out.='  <priority>'.$priority."</priority>\n";
	}
	$out.="</url>\n";
	return $out;
}
?>