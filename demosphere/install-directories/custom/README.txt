This directory contains PHP and css files that are used to customize a specific site in a multi-site installation.
On multi-site installations, sites will share a single copy of the demosphere, lib and dlib directories.

Files:
- custom.php
- any css file with the same name as a css file in demosphere will be automatically used.

FIXME: add more doc
