Directory for uploaded images  and derived images.

Images are automatically uploaded when links are used in site contents.
Derived images: pdf page icons, resized images

Do not delete these files.

FIXME:
Setup a procedure for checking which images are no longer referenced by site contents.
Then delete images.
