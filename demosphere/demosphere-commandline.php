<?php

function demosphere_commandline_config()
{
	global $commandline_config,$dlib_config;
	$dlib_config['is_commandline']=true;
	$commandline_config=[];
	$commandline_config['status' ]='demosphere_commandline_status';
	$commandline_config['install']=function($opts)
		{
			ini_set("include_path",ini_get("include_path").':'.dirname(__FILE__));
			require_once 'demosphere-install.php';
			demosphere_install($opts);
		};
	$commandline_config['setup' ]=function(&$args)
		{
			global $demosphere_config,$dlib_config;
			require_once 'demosphere-setup.php';
			demosphere_setup_part1();
			demosphere_setup_part2();
			if(array_search('--debug',$args)!==false){demosphere_setup_debug('on');}
		};
	
	$commandline_config['php_eval_init' ]=function()
		{
			// require all php files, so that the user doesn't have to do it in cmd line
			require_once 'demosphere-all.php';
			demosphere_all();
			return ['demosphere_config'];
		};

	$commandline_config['help' ]=function()
		{
			echo "  cache-clear      clear all caches (page & css)\n";
			echo "  page-cache       0/1 : enable/disable page cache (clears all caches too)\n";
			echo "  css-compression  0/1 : enable/disable css compression  (clears all caches too)\n";
		};
	$commandline_config['exec' ]=function($command,$args)
		{
			global $demosphere_config;
			switch($command)
			{
			case 'cache-clear': 
			    require_once 'demosphere-common.php';
				$out=demosphere_cache_clear();
				echo str_replace('<br/>',"\n",$out);
			    break;
			case 'css-compression':  
			    require_once 'demosphere-common.php';
			    $demosphere_config["compress_css"]=$args[0]==1;
				echo 'css compression '.($args[0]==1 ? 'enabled':'disabled')."\n";
				variable_set("demosphere_config","",$demosphere_config);
			    demosphere_page_cache_clear_all();
			    break;
			case 'page-cache':  
			    require_once 'demosphere-common.php';
			    $demosphere_config["page_cache_enable"]=$args[0]==1;
				echo 'page cache '.($args[0]==1 ? 'enabled':'disabled')."\n";
				variable_set("demosphere_config","",$demosphere_config);
			    demosphere_page_cache_clear_all();
			    break;
			default: return false;
			}
			return true;
		};
	$commandline_config['update_db_init']=function()
		{
			require_once 'demosphere-update-db.php';
			require_once 'demosphere-config-form.php';
			require_once 'dlib/translation-backend.php';
			// update translations from .po files
			$lang=dlib_get_locale_name();
			echo "Update translations from files ($lang)\n";
			echo translation_po_file_import("demosphere/translations/demosphere.".$lang.".po");
			echo translation_po_file_import("dlib/translations/dlib.".$lang.".po");

			return 'demosphere_';
		};

}

function demosphere_commandline_status()
{
	global $db_config,$demosphere_config;
	echo dlib_string_pad("site_id:"      ).$demosphere_config['site_id']."\n";
	echo dlib_string_pad("std_base_url:" ).$demosphere_config['std_base_url']."\n";
	echo dlib_string_pad("locale:" ).$demosphere_config['locale']."\n";
	echo dlib_string_pad("safe_base_url:").$demosphere_config['safe_base_url']."\n";
	echo dlib_string_pad("cron url:").$demosphere_config['std_base_url'].'/cron?cron_key='.variable_get('key','cron',0)."\n";
	echo dlib_string_pad("cron last started:").date('r',variable_get('last','cron',0))."\n";
	echo dlib_string_pad("cron last end ok:" ).date('r',variable_get('end_ok','cron',0))."\n";
	echo dlib_string_pad("page cache:" ).($demosphere_config['page_cache_enable'] ? 'on':'off')."\n";
	echo dlib_string_pad("css compression:" ).($demosphere_config['compress_css'] ? 'on':'off')."\n";

	//var_dump($demosphere_config);
}

?>