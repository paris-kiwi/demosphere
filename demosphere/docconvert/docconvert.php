<?php
/** @defgroup docconcert Docconvert
 *  @{
 */

//! $docconvert_config : configuration options for docconvert.
//! List of $docconvert_config keys:
//! - log
//! - tmp_dir
//! - dir_url
//! -::-;-::-
$docconvert_config;

require_once 'dlib/tools.php';


//! All web requests for document conversion go through this function.
//! This script  provides a user interface  for converting word/rtf/pdf/images/...
//! documents into clean html that can be used for demosphere events and posts.
//! **** Example session ***
//! browser: Link to doc is pasted into editor
//! browser: Paste event handler fires
//! browser: docconvert_find_docs_in_text: finds links
//! server: action=async-get-url-types 
//! server: checks cache for url type
//! dochost: get header for doc type
//! server: cache doc type
//! browser: link is tagged as office document : docconvert-link, docconvert-type-office
//! browser: user selects document, info & buttons displayed : docconvert_show_doc
//! browser: user clicks on doc action : extract-contents: docconvert_command
//! browser: docconvert_command
//! server: action=async-command... DD::convertCommand 
//! server: DD::get: checks cache if doc exists for url
//! server: DD::get: downloads doc from url
//! dochost: returns doc file
//! server: DD::convertCommand: this is office doc, 
//! server: DD::extractOffice: for html & pdf: exec unoconv that connects to libreoffice server that runs in a Docker container
//! libreoffice server: writes result in /var/local/demosphere/libreoffice/tmp...
//! server: DD::extractOffice: copy files from /var/local/demosphere/libreoffice/tmp... and  cleanup html
//! browser: docconvert_command: insert html into editor
//! browser: submit
//! server: demosphere_event_edit_form_submit : finds private links,
//! server: demosphere_event_submit_fix_private_link: demosphere_download_file
//! server: demosphere_event_submit_fix_private_link: file copied to files/docs
function docconvert_add_paths(&$urls,&$options)
{
	$urls[]=
		['path'=>'docconvert/async-command',
		 'roles'=>true,
		 'output'=>'json',
		 'function'=>function()
			{
				docconvert_log("docconvert: async-command: ".$_POST['url'],'debug');
				$doc=DocconvertDoc::get('url',$_POST['url']);
				if($doc===false)
				{
					docconvert_log("async-command failed to convert doc for: ".$_POST['url']);
					return ['html'=>false,
							'error'=>t('document download failed')];
				}
				return $doc->convertCommand($_POST['command'],$_POST['type']);
			},];

	$urls[]=
		['path'=>'docconvert/async-get-url-types',
		 'roles'=>true,
		 'output'=>'json',
		 'function'=>function()
			{
				if(!is_array($_POST['urls'] ?? false)){dlib_bad_request_400();}
				docconvert_log("docconvert: async-get-url-types : nb urls: ".count($_POST['urls']),'debug');
				$types=docconvert_get_url_types($_POST['urls'],isset($_GET['nocache']));
				return ['types'=>$types];
			},];
	$urls[]=
		['path'=>'@^docconvert/doc/([a-f0-9]+)/view-file$@',
		 'roles'=>true,
		 'output'=>false,
		 'function'=>function($docid)
			{
				docconvert_log("docconvert: view-file: ".$docid,'debug');
				$doc=new DocconvertDoc($docid);
				$doc->viewFile(intval($_GET['file']));
			},];

	$urls[]=
		['path'=>'docconvert/submit-upload',
		 'roles'=>true,
		 'output'=>false,
		 'function'=>'docconvert_submit_upload',
		];

	$urls[]=
		['path'=>'docconvert/pdf-select',
		 'roles'=>['admin','moderator'],
		 'file'=>'demosphere/docconvert/docconvert-pdf-select.php',
		 'function'=>'docconvert_pdf_select',
		];

	$urls[]=
		['path'=>'docconvert/pdf-select-image',
		 'output'=>false,
		 'roles'=>['admin','moderator'],
		 'file'=>'demosphere/docconvert/docconvert-pdf-select.php',
		 'function'=>'docconvert_pdf_select_image',
		];

	$urls[]= 
		['path'=>'docconvert/pdf-select-selection',
		 'output'=>'json',
		 'roles'=>['admin','moderator'],
		 'file'=>'demosphere/docconvert/docconvert-pdf-select.php',
		 'function'=>'docconvert_pdf_select_selection',
		];
}

//! This is "displayed" when user presses submit in the upload popup.
//! The "display" is just a javascript that fires a function in the main page,
//! so that the main page gets the url of the uploaded doc.
function docconvert_submit_upload()
{
	docconvert_log("docconvert: docconvert_submit_upload",'debug');
	dlib_check_form_token('docconvert_upload_form',$_REQUEST);
	//var_dump($_POST);
	$doc=DocconvertDoc::get('form');
    $editorId=$_REQUEST['docconvert-editor-id'];
	if(!preg_match('@^[a-z0-9_-]+$@',$editorId)){dlib_bad_request_400();}
	if($doc===false)
	{
		echo '<script type="text/javascript">'.
			'window.parent.tinyMCE.get("'.$editorId.'").docconvert.upload_failed();'.
			'</script>';
		return;
	}
	$url=$doc->srcFileUrl();
	$doc->save();// file url
	echo '<script type="text/javascript">'.
		'window.parent.tinyMCE.get("'.$editorId.'").docconvert.upload_success('.dlib_json_encode_esc($url).',false);'.
		'</script>';
}

function php_ini_return_bytes($val) {
    $val = trim($val);
    $last = strtolower($val[strlen($val)-1]);
	$val=substr($val,0,-1);
    switch($last) {
        // The 'G' modifier is available since PHP 5.1.0
        case 'g':
            $val *= 1024;
        case 'm':
            $val *= 1024;
        case 'k':
            $val *= 1024;
    }

    return $val;
}
//! Returns an array with a simple type ('pdf','office','image',false) for each url.
//! Notice that this uses demosphere_get_content_type which 
//! automatically recognizes certain private local files.
function docconvert_get_url_types(array $urls,bool $nocache=false)
{
	require_once 'dlib/mail-and-mime.php';
	require_once 'demosphere-misc.php';
	$unique=array_flip($urls);
	foreach(array_keys($unique) as $url)
	{
		$cached=docconvert_cache($url);
		$type=false;
		if(!$nocache && isset($cached['type'])){$type=$cached['type'];}
		else
		{
			$mime=demosphere_get_content_type($url,
					   ['curl'=>[CURLOPT_TIMEOUT=>($nocache ? 15 : 5)]]);
			$type=dlib_mime_to_simple_type($mime);
			$cached['type']=$type;
			docconvert_cache($url,$cached);
		}
		$unique[$url]=$type;
	}
	$res=[];
	foreach($urls as $k=>$url){$res[$k]=$unique[$url];}
	return $res;
}

//! Caches document-ids and link types.
//! link types: avoids doing multiple GET content-type requests.
//! document-id's: avoids recreating a new document for a request of an already 
//! processed document.
//! Returns an array with information (type, ts, docid) on the url. 
//! @param the url 
//! @param if false, this is a cache read. Otherwise an array with type and/or docid
function docconvert_cache($url,$value=false)
{
	global $docconvert_config;
	static $cache=false;
	$cacheFile=$docconvert_config['tmp_dir'].'/cached-urls';
	if($cache===false)
	{
		// cache-urls sometimes (very very rarely) does fails to unserialize (maybe concurrency)
		// It might be better to use  variable_set/get instead of a file ... benchmark shows that db is 5x slower than file :-( 
		// For now, we just disable the warning, and check for parse errors.
		if(file_exists($cacheFile)){$cache=@unserialize(@file_get_contents($cacheFile),['allowed_classes'=>false]);}
		if($cache===false){$cache=[];}
	}
	if($value===false)
	{
		if(isset($cache[$url])){return $cache[$url];}
		return [];
	}
	else
	{
		$cache[$url]=$value;
		$cache[$url]['ts']=time();
		file_put_contents($cacheFile,serialize($cache));
	}
}

//! A document (pdf,image, office) from which we want to extract information.
//! Each document is stored in a seperate temporary directory, with its files.
//! Directory names and ids are computed from md5 of the doc's url (or random if no url).
//! DocconvertDoc fields are stored serialized in the "saved" file.
//! The main document filename is "fetched-src".
class DocconvertDoc
{
	//! ids are computed from md5 of the doc's url (or randndom if no url).
	var $id;
	//! full path of the directory of this DocconvertDoc (this is computed, not stored)
	var $dir;
	//! the original url of this DocconvertDoc (false if it was a file upload)
	var $url;
	//! full path of the main file. ex:files/private/tmp/doc-a23.../fetched-src
	//! (this is computed, not stored)
	var $src;
	//! A filename extension that we have computed for the original document (ex:"doc" "pdf" )
	//! This is determined by us (the orignal is often wrong and untrusted)
	var $srcExtension;
	//! Original filename of this document (ex: my-pretty-flyer.pdf).
	//! We DO NOT use this name in docconvert for any of our files 
	var $srcOrigFname;
	//! Http headers from downloading the original file.
	//! (or just content-type for non url docs)
	var $srcHeaders;
	//! Computed mime type of original doc (ex: application/pdf).
	//! This is supposed to be the most reliable mime type we can figure out.
	var $srcMimeType;
	//! filename of pdf file (ex: src.pdf)
	var $pdf;
	//! filename of html extracted from office (ex: office-html.html)
	var $officeHtml;
	//! filename of text extracted from pdf (ex: pdf-text.html)
	var $pdfText;
	//! prefix of filename of images extracted from pdf (ex: pdf-image)
	var $pdfImages;
	//! A list of files that have links to them.
	//! The tmp directory where our files reside should not be accessible by the webserver 
	//! for security reasons. When we need to link to a file, we use fileUrl()
	//! that registers the file and returns a link to it.
	var $registeredFiles=[];

	static $savedFields=['url','srcExtension','srcOrigFname','srcHeaders','srcMimeType','pdf','officeHtml','pdfText','pdfImages','registeredFiles'];

	//! If no id, that creates a new doc. Creator needs to set things up.
	//! If id is set, loads existing doc.
	function __construct($id=false)
	{
		if($id===false){return;}

		global $docconvert_config;
		$this->id=$id;
		$this->dir=$docconvert_config['tmp_dir'].'/doc-'.$id;
		if(!file_exists($this->dir))
		{
			throw new Exception("Failed to find document dir");
		}
		$this->src=$this->dir.'/fetched-src';
		$saved=unserialize(file_get_contents($this->dir.'/saved'),['allowed_classes'=>false]);
		foreach(self::$savedFields as $f){$this->$f=$saved[$f];}
	}

	//! Save doc by serializing fields into a file called "saved".
	public function save()
	{
		$saved=[];
		foreach(self::$savedFields as $f){$saved[$f]=$this->$f;}
		file_put_contents($this->dir.'/saved',serialize($saved));
	}

	//! FIXME : some of this is old and was made for an upload form displayed in an iframe. Remove it.
	//! Creates a DocconvertDoc from either a form submission or a url.
	//! It will autodetect existing DocconvertDoc if it has the same url.
	//! @param either 'form', 'url' or 'test'
	//! @param url (if srctype is 'url')
	public static function get($srcType,$url=false)
	{
		global $docconvert_config;

		docconvert_log('DocconvertDoc::get: srcType:'.$srcType.' url:'.$url,'debug');

		// FIXME: this doesn't seem to be used anymore
		//if($srcType==='form'){$url=val($_POST,'url');}

		if($srcType==='form' && (!isset($_FILES["docconvertupload"]['error']) ||
								 $_FILES["docconvertupload"]['error']!==UPLOAD_ERR_OK))
		{
			docconvert_log('tl:DocconvertDoc::get: failed upload 1 : error nb: '.$_FILES["docconvertupload"]['error'].': error message:'.file_upload_error_message($_FILES["docconvertupload"]['error']));
			return false;
		}

		// FIXME: can this really happen ? Isn't first error check enough ?
		if($srcType==='form' && !isset($_FILES["docconvertupload"]['name']))
		{
			docconvert_log('tl:DocconvertDoc::get: failed upload 2 : no name in FILES');
			return false;
		}

		if($url!==false)
		{
			$cached=docconvert_cache($url);
			if(isset($cached['docid']))
			{
				$dir=$docconvert_config['tmp_dir'].'/doc-'.$cached['docid'];
				//docconvert_log('tl:DocconvertDoc::get: cached:'.$dir);
				if(file_exists($dir))
				{
					//docconvert_log('tl:DocconvertDoc::get: cachedok');
					return new DocconvertDoc($cached['docid']);
				}
				else
				{
					//docconvert_log('tl:DocconvertDoc::get: stale cache, removing');
					unset($cached['docid']);
					docconvert_cache($url,$cached);
				}
			}
		}

		$id=md5($url!==false ? $url : 'doc'.rand());
		$dir=$docconvert_config['tmp_dir'].'/doc-'.$id;
		if(file_exists($dir)){return new DocconvertDoc($id);}
		$doc=new DocconvertDoc();
		$doc->id=$id;
		$doc->dir=$dir;
		docconvert_log('tl:DocconvertDoc::get: creating new doc id:'.$doc->id.' for url:'.$url. 'dir:'.$doc->dir,'debug');
		if(!mkdir($doc->dir)){fatal("DocconvertDoc::get mkdir failed");}
		$doc->src=$doc->dir.'/fetched-src';
		$doc->url=$url;

		if($srcType==='test'){copy($url,$doc->src);}
		else
		if($url!==false)// download from url
		{
			require_once 'demosphere-misc.php';
			$info=demosphere_download_file($url,$doc->src);
			if($info===false){rmdir($doc->dir);return false;}
			$doc->headers=$info['headers'];
			$doc->srcOrigFname=$info['filename'];
			$cached['docid']=$doc->id;
			docconvert_cache($url,$cached);
		}
		else
		{
			// this is an uploaded file
			if($_FILES['docconvertupload']['error'])
			{
				echo file_upload_error_message($_FILES['docconvertupload']['error'])."<br/>";
				dlib_bad_request_400("aborted");
			}
			$doc->srcOrigFname=$_FILES["docconvertupload"]['name'];
			$ok=move_uploaded_file($_FILES["docconvertupload"]['tmp_name'],$doc->src);
			if($ok===false){fatal('move upload failed');}
		}

		require_once 'dlib/mail-and-mime.php';
		$doc->srcMimeType=dlib_finfo($doc->src);
		$doc->srcExtension=dlib_mime_to_extension($doc->srcMimeType);
		if($doc->srcExtension===false)
		{
			dlib_message_add('Docconvert: no filename extension found for file:"'.ent($doc->srcOrigFname).'" : found mime type:"'.ent($doc->srcMimeType).'"','warning');
		}

		// create a copy of the orginal file, using extension (necessary for some programs)
		if(!copy($doc->src,$doc->src.'.'.$doc->srcExtension)){fatal('failed copy');}

		if(dlib_mime_to_simple_type($doc->srcMimeType)==='pdf'){$doc->pdf='fetched-src.pdf';}

		$doc->save();
		return $doc;
	}

	//! Simply output a raw file.
	//! Documents (images, dowloaded files, pdfs..) are all in directories
	//! that are not directly accesible by http. 
	public function viewFile($fileNb)
	{
		header('Content-Type: '.$this->fileType($fileNb));
		$fname=$this->getRegisteredFname($fileNb);
		header("Content-Length: " . filesize($fname));
		readfile($fname);
		exit;	
	}

	//! Returns the mime type of a registered file.
	public function fileType($fileNb)
	{
		$fname=$this->getRegisteredFname($fileNb);
		require_once 'dlib/mail-and-mime.php';
		// special case for original src file: use already computed mime type 
		// that was computed using src filename extension.
		if(preg_match('@/fetched-src$@',$fname)){return $this->srcMimeType;}
		$mime=dlib_finfo($fname);
		return $mime;
	}

	//! Copy a registered file (identified by its number) to a destination.
	public function copyFile($fileNb,$dest)
	{
		copy($this->getRegisteredFname($fileNb),$dest);
	}

	//! Returns the full path of a registered file, and checks if it's ok.
	private function getRegisteredFname($fileNb)
	{
		$fileNb=intval($fileNb);
		if(!isset($this->registeredFiles[$fileNb]))
		{
			throw new Exception("Failed to find registered file");
		}
		return $this->dir.'/'.$this->registeredFiles[$fileNb];
	}
	//! Returns a url for the main file in the doc.
	public function srcFileUrl(){return $this->fileUrl('fetched-src');}

	//! Returns a url for a file in the doc. The file is registered for future access
	//! using this url. It is identified by a number.
	private function fileUrl($file)
	{
		global $base_url;
		$n=count($this->registeredFiles);
		$this->registeredFiles[]=$file;
		return $base_url.'/docconvert/doc/'.$this->id.'/view-file?'.
			'file='.$n;
	}
	//! Converts a document and returns information on it .
	public function convertCommand($command,$type=false)
	{
		global $docconvert_config;
		$error=false;
		require_once 'dlib/mail-and-mime.php';
		if($type===false){$type=dlib_mime_to_simple_type($this->srcMimeType);}
		docconvert_log('DocconvertDoc::convertCommand: src mime: '.$this->srcMimeType.' simple type:'.$type,'debug');

		if($type==="office")
		{
			$officeHtml=$this->extractOffice();
			if($officeHtml===false)
			{return ['html'=>false,'error'=>t('extract from office document failed')];}
		}

		if($command=="preview-pages" || $command=="preview-pages-1" || $command=="preview-pages-2" || $command==="extract-contents")
		{
			$pdfFname=$this->dir.'/'.$this->pdf;
			$options=[];
			// preview-pages automatically chooses between 1 and 2 page images
			if($command=="preview-pages")
			{
				if(pdf_nb_pages($pdfFname)===1){$options['sizeFactor']=2.0113;$options['maxPages']=1;}
			}
			if($command=="preview-pages-1"){$options['sizeFactor']=2.0113;$options['maxPages']=1;}
			$pages=pdf_page_images($pdfFname,$this->dir.'/pdf-page-image',$options);
			if($pages===false){$error=t('pdf page preview failed');$pages=[];}
			$pdfPages='';
			$pdfPages.='<p class="floatRight mceNonEditable composite-doc-container doc-type-pdf" data-mce-contenteditable="true">'.
				'<a href="'.ent($this->fileUrl($this->pdf)).'" class="composite-doc">';

			foreach($pages as $page)
			{
				$pdfPages.='<img class="pdfPage" src="'.
					ent($this->fileUrl(basename($page))).'" />';
			}
			$pdfPages.="</a></p> ";
		}

		$html='';
		switch($command)
		{
		case "preview-pages":
		case "preview-pages-1":
		case "preview-pages-2":
			$html=$pdfPages;
			break;
		case "extract-contents":
			if($type==="pdf")   
			{
				$html=$this->extractPdfText();
				if($html===false){$error=t("extract pdf text failed");}
			}
			if($type==="office"){$html=$officeHtml;}
			$html.=$pdfPages;
			break;
		case "extract-pdf-images":
			$images=$this->extractPdfImages(800);
			$html='';
			if($images===false){$error=t('pdf image extract failed');$images=[];}
			else
			if(count($images)==0){$error=t('no pdf images found');}
			foreach($images as $image)
			{
				$html.='<p><img src="'.ent($this->fileUrl($image)).'"/></p>';
			}
			break;
		default: $error="bad convertCommand";
		}

		$this->save();// necessary for fileUrl(...)

		return ['html'=>$html,'error'=>$error];
	}

	//! Convert office documents to pdf and html.
	//! LibreOffice runs as a sever (may be in Docker) and we use the unoconv command line client to convert.
	//! We use a temp dir that is writable by the LibreOffice server.
	public function extractOffice()
	{
		global $docconvert_config;
		docconvert_log('tl:extractOffice: ','debug');

		$html=false;
		$this->pdf='src.pdf';

		// FIXME: make tmp dir configurable ?
		// Libreoffice runs as a sever inside Docker. We need to give it files and read result files using a shared directory.
		$officeTmp=dlib_make_tmp_dir('/var/local/demosphere/libreoffice','tmp-',0770);
		if($officeTmp===false){return false;}
		chmod($officeTmp,0770);
		copy($this->src,$officeTmp.'/src');

		// convert the document to each format
		foreach(['html','pdf'] as $format)
		{
			$file=$officeTmp.'/converted.'.$format;
			$cmd='unoconv '.
				'--connection \'socket,host=127.0.0.1,port=2225,tcpNoDelay=1;urp;StarOffice.ComponentContext\' '.
				'-f '.escapeshellarg($format).' '.
				'-o '.escapeshellarg($file).' '.
				escapeshellarg($officeTmp.'/src');
			$cmdRet=dlib_logexec($cmd,$docconvert_config['log']);
			if($cmdRet!=0 || !file_exists($file))
			{
				docconvert_log('extractOffice failed');
				// 8/2018 debug;  dlib_logexec('rm -r '.$officeTmp);
				return false;
			}
			if($format==='pdf' ){copy($file,$this->dir.'/'.$this->pdf);}
			if($format==='html')
			{
				// cleanup html and fix image references
				$html=file_get_contents($file);
				require_once 'dlib/html-tools.php';
				$html=dlib_html_cleanup_tidy_xsl($html);
				$dc=$this;
				$html=preg_replace_callback('@(<img[^>]* src=["\'])(converted_html_[a-zA-Z0-9]{1,30}[.][a-zA-Z0-9]{1,30})([\'"])@',
									  function($match)use($dc,$officeTmp)
									  {
										  $fname=$match[2];
										  $tmpFile=$officeTmp.'/'.$fname;
										  if(!file_exists($tmpFile)){return $match[1].'dc-extract-office-image-not-found'.$match[3];}
										  $imgDir=$dc->dir.'/office-html-images';
										  if(!file_exists($imgDir)){mkdir($imgDir);}
										  copy($tmpFile,$imgDir.'/'.$fname);
										  $url=$dc->fileUrl('office-html-images/'.$fname);
										  return $match[1].ent($url).$match[3];
									  },
									  $html);
				require_once 'lib/htmLawed.php';
				$html = htmLawed($html,['safe'=>1,'deny_attribute'=>'style']);
				$this->officeHtml='office-html.html';
				file_put_contents($this->dir.'/'.$this->officeHtml,$html);
			}
		}
		$this->save();
		dlib_logexec('rm -r '.$officeTmp);
		return $html;
	}

	function extractPdfText()
	{
		global $docconvert_config;
		docconvert_log('tl:extractPdfText: ','debug');
		$this->pdfText='pdf-text.html';
		// use unix command to extract text from pdf document (very unreliable)
		$cmdRet=dlib_logexec('pdftotext -enc UTF-8 '.
						$this->dir.'/'.$this->pdf.' '.
						$this->dir.'/'.$this->pdfText,
						$docconvert_config['log']);
		if($cmdRet!=0){return false;}
		$res=file_get_contents($this->dir.'/'.$this->pdfText);
		// convert text to html (this is unreliable)
		if(substr($res,-1)===chr(12)){$res=substr($res,0,-1);}
		$res=str_replace(chr(12),"\nDCPDFPAGEBREAK\n",$res);
		$res='<p>'.str_replace(['DCPDFPAGEBREAK',"\n"],
							   ['<hr class="subsection"/>','</p><p>'],
							   ent($res)).'</p>';
		$res=str_replace('<p></p>','',$res);
		require_once 'dlib/html-tools.php';
		$res=dlib_html_cleanup_tidy_xsl($res);
		require_once 'lib/htmLawed.php';
		$res = htmLawed($res,['safe'=>1,'deny_attribute'=>'style']);
		file_put_contents($this->dir.'/'.$this->pdfText,$res);
		$this->save();
		return $res;
	}

	function extractPdfImages($maxWidth=false)
	{
		global $docconvert_config;
		$this->pdfImages='pdf-image';
		// use unix command to extract images from pdf document
		$cmdRet=dlib_logexec('pdfimages '.$this->dir.'/fetched-src.pdf '.
							 $this->dir.'/'.$this->pdfImages,
							 $docconvert_config['log'],
							 "extract images from pdf: pdfimages failed");
		if($cmdRet!=0){return false;}
		$scanRes=dlib_find_files_with_basename($this->dir,$this->pdfImages.'-');
		// get largest images (some pdfs have thousands...)
		$files=[];
		$found=false;
		foreach($scanRes as $file)
		{
			if(preg_match('@^'.$this->pdfImages.'-[0-9]+.[a-z]*$@s',$file))
			{
				$files[$file]=filesize($this->dir.'/'.$file);
			}
		}
		arsort($files);
		$res=[];
		foreach($files as $file=>$size)
		{
			$jpgFile=dlib_remove_filename_extension($file).".jpg";
			dlib_logexec('convert '.
						 $this->dir.'/'.escapeshellarg($file).' '.
						 "-geometry '".intval($maxWidth)."x>' ".
						 $this->dir.'/'.escapeshellarg($jpgFile),
						 $docconvert_config['log'],
						 "extract images from pdf: jpg failed");
			$res[]=$jpgFile;
		}

		$this->save();
		return $res;
	}

	//! Remove documents after one day.
	static function cleanupOld()
	{
		global $docconvert_config;
		$dir=$docconvert_config['tmp_dir'];
		$scanRes=scandir($dir);
		foreach($scanRes as $dirEntry)
		{
			if(!preg_match('@^doc-[a-f0-9]{20,}@',$dirEntry)){continue;}
			if(filemtime($dir.'/'.$dirEntry)<time()-3600*24)
			{
				//echo "deleting old doc:".$dir.'/'.$dirEntry."<br/>\n";
				system("rm -rf ".$dir.'/'.$dirEntry);
			}
		}
	}

}

function docconvert_log($message,$level=false)
{
	global $docconvert_config;
	if($level==='debug'){return;}
	dlib_log($message,$docconvert_config['log']);
}

function docconvert_js_config()
{
	global $docconvert_config,$currentPage;
	$maxsize=php_ini_return_bytes(ini_get('upload_max_filesize'));
	$maxsize=min($maxsize,100*1024*1024);
	$maxsize=max($maxsize,  1*1024*1024);

	$text=[
		'view_document'=>t('view document'),
		'page_icons'=>t('page icons'),
		'pdf_select'=>t('select pdf parts'),
		'extract_contents'=>t('extract contents'),
		'extract_pdf_images'=>t('extract pdf images'),
		'insert_image'=>t('insert image'),
		'page_icons'=>t('page icons'),
		'prev'=>t('prev'),
		'next'=>t('next'),
		'documents'=>t('Documents'),
		'upload'=>t('upload document'),
		'upload-button'=>t('add document'),
		'upload-add'=>t('pdf, image, word, odt ... <br />Max size: @maxsize Megabytes',
						['@maxsize'=>round($maxsize/(1024*1024),2)]),
		'refresh'=>t('refresh'),
		'uploading_'=>t('uploading...'),
		'wait_for_cmd'=>t('Please wait. Previous command has not finished.'),
		'click_inside'=>t("Please click inside the editor before you upload. The uploaded link will be inserted there."),
		'image'=>t('image'),
		'resize_image'=>t('resize image'),
		'loading'=>t('loading...'),
		'source'=>t('_source_'),
		'file_too_large'=>t("Your file is too large :"),
		'upload_failed'=>t('File upload failed'),
		'still_uploading'=>t('File is still being uploaded.'),
		'docconvert_not_finished'=>t('Document processing has not finished.'),
		'uploaded_document'=>t('uploaded document'),
		  ];

	$currentPage->addJsTranslations($text);
	$currentPage->addJs('demosphere/docconvert/docconvert.js');
	require_once 'dlib/css-template.php';
	$currentPage->addJs('window.docconvert = window.docconvert || {};'.
						'docconvert.editor_css="'.css_template('demosphere/docconvert/docconvert-editor.tpl.css').'";'.
						'docconvert.upload_maxsize='.$maxsize.';'.
						'docconvert.upload_form_token="'.dlib_get_form_token('docconvert_upload_form').'";',
						'inline');
}


// ***************************************************
// ************** helper functions *******************
// ***************************************************


//! Converts a pdf document into images (one image per page)
function pdf_page_images($src,$resBase,$options=[])
{
	global $docconvert_config;
	$defaults=['format'=>"png",						 
			   'baseSize'=>"177x350",
			   'sizeFactor'=>1,
			   'rotate'=>false,
			   'justGetFilenames'=>false,
			   'maxPages'=>2];
	$options=array_merge($defaults,$options);
	extract($options);

	if(!$justGetFilenames)
	{
		$size=explode("x",$baseSize);
		$width =round($size[0]*$sizeFactor);
		$height=round($size[1]*$sizeFactor);
		// approx density  based on complex : 72dpi->600pix *3 for quality
		$density=round(max($width,$height)/2); 
		
		// delete existing file
		if(file_exists("$resBase.$format")){unlink("$resBase.$format");}

		// ImageMagick is doing strange things to colors and taking very long
		// Switch to gm
		$cmdRet=dlib_logexec('gm convert -density '.$density.' '.
							 escapeshellarg($src).
							 ($maxPages!==false ? "'[".implode(',',range(0,$maxPages-1))."]'":'').
							 ' '.
							 ($rotate ? "-rotate $rotate " : '').
							 "-geometry $width"."x$height +adjoin $resBase-%d.$format",
							 $docconvert_config['log']);
		if($cmdRet!=0){return false;}
	}
		
	$pages=[];
	if(file_exists("$resBase.$format")){$pages[]="$resBase.$format";}
	else
	{
		for($i=0;$i<1000;$i++)
		{
			$page="$resBase-$i.$format";
			if(file_exists($page)){$pages[]=$page;}
		}
	}

	// After switch to debian-7 Wheezy convert pdf produces transparent images, 
	// we want white bg, not transparent.
	//if(!$justGetFilenames)
	//{
	// 	foreach($pages as $page)
	// 	{
	// 		$cmdRet=dlib_logexec('convert '.escapeshellarg($page).' -flatten '.escapeshellarg($page));
	// 	}
	//}

	return $pages;
}

function pdf_nb_pages($fname)
{
	exec('/usr/bin/pdfinfo '.escapeshellarg($fname), $output,$ret);
	if($ret!=0){return false;}
	$lines=preg_grep('@^Pages:\s*([0-9]+)@',$output);
	if(count($lines)==0){return false;}
	$line=array_pop($lines);
	preg_match('@^Pages:\s*([0-9]+)@',$line,$m);
	return intval($m[1]);
}

function file_upload_error_message($error_code) 
{
    switch ($error_code) {
        case UPLOAD_ERR_INI_SIZE:
            return 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
        case UPLOAD_ERR_FORM_SIZE:
            return 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        case UPLOAD_ERR_PARTIAL:
            return 'The uploaded file was only partially uploaded';
        case UPLOAD_ERR_NO_FILE:
            return 'No file was uploaded';
        case UPLOAD_ERR_NO_TMP_DIR:
            return 'Missing a temporary folder';
        case UPLOAD_ERR_CANT_WRITE:
            return 'Failed to write file to disk';
        case UPLOAD_ERR_EXTENSION:
            return 'File upload stopped by extension';
        default:
            return 'Unknown upload error';
    }
}

/** @} */

?>