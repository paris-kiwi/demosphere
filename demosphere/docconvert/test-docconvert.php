<?php

function docconvert_web_test()
{
	global $docconvert_config,$base_url;
	require_once 'dlib/html-tools.php';
	$tdir='demosphere/docconvert/test/web-test';
	$docconvert_config['dir_url']=$base_url.'/demosphere/docconvert';
	$docconvert_config['log']=$tdir.'/docconvert.log';
	$docconvert_config['tmp_dir']=$tdir;
	docconvert_test_basic($tdir.'/..');
}

function docconvert_test_basic($testDocDir)
{
	global $docconvert_config,$base_url;
	// purge all caches
	$tmp=$docconvert_config['tmp_dir'];
	vd($tmp);
	system("cd $tmp;rm -rf doc-* cached-urls docconvert.log");

	require_once 'demosphere-misc.php';
	require_once 'dlib/download-tools.php';
	// test curl_get_header
	$h=dlib_curl_get_header('http://sdfsdf.jgnsdfgsdgfdf.fdfdfgfdg.com');
	test_equals($h,false);
	$h=dlib_curl_get_header('http://demoselenium');
	test_assert(is_array($h));
	test_equals($h['content-type'],'text/html; charset=UTF-8');
	
	// test demosphere_get_content_type
	$h=demosphere_get_content_type('http://sdfsdf.jgnsdfgsdgfdf.fdfdfgfdg.com');
	test_equals($h,false);
	$h=demosphere_get_content_type('http://demoselenium');
	test_equals($h,'text/html; charset=UTF-8');
	$h=demosphere_get_content_type($base_url.'/docconvert/doc/xyz/view-file?file=0');	
	test_equals($h,false);
	
	$f=demosphere_download_file('http://sdfsdf.jgnsdfgsdgfdf.fdfdfgfdg.com',$tmp.'/f');

	// now create a document and test
	$doc=DocconvertDoc::get('url','http://demoselenium/demosphere/css/fp-chop.png');

	$id=$doc->id;
	// this should fail gracefully: unregistered file
	$h=demosphere_get_content_type($base_url.'/docconvert/doc/'.$id.'/view-file?file=0');	
	test_equals($h,false);

	$url=$doc->srcFileUrl();
	$doc->save();
	// this url should be recognized as a private docconvert file
	$private=demosphere_private_file($url);
	test_equals($private['type'],'docconvert');
	// this should succeed: registered src file (and private)
	$h=demosphere_get_content_type($url);	
	test_equals($h,'image/png');

	// test bad url
	$doc=DocconvertDoc::get('url','http://sdfsdf.jgnsdfgsdgfdf.fdfdfgfdg.com');
	test_equals($doc,false);

	// test pdf without images
	$doc=DocconvertDoc::get('test',$testDocDir.'/test1.pdf');
	$id=$doc->id;
	test_equals($doc->srcExtension,'pdf');
	test_equals($doc->srcMimeType,'application/pdf');
	$cmdRes=$doc->convertCommand('extract-contents','pdf');
	test_contains($cmdRes['html'],'ceci est un joli essai');
	test_contains($cmdRes['html'],ent($base_url.'/docconvert/doc/'.$id.'/view-file'));
	$cmdRes=$doc->convertCommand('extract-pdf-images','pdf');
	test_different($cmdRes['error'],false);


	// test pdf page icons
	$doc=DocconvertDoc::get('test',$testDocDir.'/test1.pdf');
	$cmdRes=$doc->convertCommand('preview-pages','pdf');
	test_equals($cmdRes['error'],false);
	test_equals(substr_count($cmdRes['html'],'class="pdfPage"'),1);

	// test pdf page icons (limit to 2)
	$doc=DocconvertDoc::get('test',$testDocDir.'/test3.pdf');
	$cmdRes=$doc->convertCommand('preview-pages','pdf');
	test_equals($cmdRes['error'],false);
	test_equals(substr_count($cmdRes['html'],'class="pdfPage"'),2);

	// test pdf (with images)
	$doc=DocconvertDoc::get('test',$testDocDir.'/test2.pdf');
	$cmdRes=$doc->convertCommand('extract-pdf-images','pdf');
	test_equals($cmdRes['error'],false);

}

?>