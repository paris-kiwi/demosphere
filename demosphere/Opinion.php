<?php

//! An opinion a contributor (User) gives on an Event.
//! Each Opinion affects the contributor's reputation.
//! A contributor can only give one Opinion for each Event.
class Opinion extends DBObject
{
	public $id=0;
	static $id_		   =['type'=>'autoincrement'];
	//! The contributor
	public $userId=0;
	static $userId_	   =['type'=>'foreign_key','class'=>'User'];
	//! The event that is the object of this opinion.
	public $eventId=0;
	static $eventId_   =['type'=>'foreign_key','class'=>'Event'];
	//! Last time the contributor changed this event (using the form).
	public $time=0;
	static $time_	   =['type'=>'timestamp'];
	//! An optional comment the contributor can make when giving his opinion.
	public $text='';
	static $text_	   =[];
	//! A grade between -5 to +5. 
	//! This is the main information of an opinion. 
	//! Ratings of all opinions on an Event are averaged to compute a rating for that Event.
	//! The average is weighted using the reputation of the contributors (Users), not the reputation of the Opinions.
	public $rating=0;
	static $rating_	   =['type'=>'int'];
	//! The reputation that this opinion would give to the contributor if it where the only one.
	//! It is computed by comparing the rating to ratings by other users and to the final publish/no-publish decision
	//! Currently reputation can only be 0 or 1 (this might or might not change in future developments).
	//! Reputation and weight are determined by calcReputation()
	public $reputation=0;
	static $reputation_ =['type'=>'float'];
	//! How much the $reputation will influence the total reputation of the *user* (not the event).
	//! The total reputation of a user is based on the weighted average of the reputations of his opinions @see demosphere_opinion_user_reputation()
	public $weight=0;
	static $weight_ =['type'=>'float'];
	//! A reference to the article in the publishing guidelines on which this opinion is based.
	//! Only set for negative ratings.
	public $pubGuideline  ='';
	static $pubGuideline_ =[];

	//! An optional comment from moderators on this opinion.
	//! This comment is invisible.
	public $modComment='';
	static $modComment_=[];
	//! An optional reputation adjustment by the moderator on this Opinion
	//! It is added to the reputation.
	public $modReputation=0;
	static $modReputation_ =['type'=>'float'];

	//! Opinions can be enabled/disabled.
	//! We want to keep a history of Opinions, so they should not be deleted.
	//! If they are disabled, their reputation will not count. 
	//! This can happen for example, when an event has been unpublished because it is canceled, not because it is inappropriate.
	public $isEnabled=1;
	static $isEnabled_ =['type'=>'bool'];

	static $dboStatic=false;

	//! Computes the reputation and weight of this opinion, and returns them (but does not set them).
	//! Optionally returns an array with data about this computation.
	//! Does not change anything in db.
	//! Reputation and weight are computed by comparing :
	//! * This opinion's "rating"
	//! * any other rating by other users.
	//! * This event's published/rejected status
	function calcReputation($retAllData=false)
	{
		global $demosphere_config;
		require_once 'demosphere-opinion.php';

		$event=$this->useEvent();
		// Have moderators made a publish/reject decision ?
		$publishingDecisionMadeByMods=
			($event->status==1 && !$event->isOpinionAutoPublished()) ||
			$event->getModerationStatus()==='rejected-shown' ||
			$event->getModerationStatus()==='rejected';
		
		$modsAgree=true; // Do moderators agree among themselves (including publish/reject decision)
		$modsRatingCt=0; // Total nb of moderator ratings (publish/reject decision counts as .01)
		$modsRatingTot=false; // Used to compute average moderator rating
		// publishing descision is counted as a very small weighted rating
		if($publishingDecisionMadeByMods){$modsRatingTot=$event->status ? .01 : -.01;$modsRatingCt=.01;}

		$reputation=0;
		$display='';

		// *** Compare this rating to each other opinion on this event 

		$isFirst=null;
		$isBeforeMod=null;
		$dissentBeforeMod=null;
		$otherOpinions=db_arrays_keyed('SELECT userId,rating,time FROM Opinion WHERE '.
									  'eventId=%d AND userId!=%d AND isEnabled ORDER BY time ASC',
									  $this->eventId,$this->userId);
		$nonModBeforeUs=null; // only used to compute $dissentBeforeMod
		foreach($otherOpinions as $userId=>$otherOpinion)
		{
			$otherRating=(int)$otherOpinion['rating'];
			$oUser=User::fetch($userId);
			// If other opinion is given by a moderator
			if($oUser->checkRoles('admin','moderator'))
			{
				if($modsAgree){$modsAgree=$modsRatingCt===0 ? true : ($otherRating===0 || (($otherRating>0) === ($modsRatingTot>0)));}
				$modsRatingTot+=$otherRating;
				$modsRatingCt+=1;
				if($isBeforeMod===null){$isBeforeMod=$this->time<$otherOpinion['time'];}
				// Did we dissent before this mod
				if($dissentBeforeMod===null)
				{
					$dissentBeforeMod=
						$otherOpinion['time']>$this->time && // if mod has given opinion before us, then we are not dissenting before mod
						$this->rating!==0 && // dissent only if real opinion (>0 or <0)
						$nonModBeforeUs!==null && $nonModBeforeUs!==($this->rating>0); // dissent only if disagree with previous non-mod
				}
			}
			else
			{
				// if this is a non-mod opinion, before ours
				if($otherOpinion['time']<$this->time && $dissentBeforeMod===null && $this->rating!==0)
				{
					if($otherRating===0){$dissentBeforeMod=false;} // a non mod is undecided before us, so we are not really dissenting
					else
					if($nonModBeforeUs===null){$nonModBeforeUs=$otherRating>0;}// found first non-mod opinion before us
					else					  
					// other non-mods disagree between themselves before us, so we are not really dissenting
					if($nonModBeforeUs!==($otherRating>0)){$dissentBeforeMod=false;} 
				}
			}
			if($isFirst===null){$isFirst=$this->time<$otherOpinion['time'];}
		}
		if($isFirst===null){$isFirst=true;}
		// Mod publish/reject decision is pretty much like a mod rating (for $isBeforeMod and $dissentBeforeMod)
		if($publishingDecisionMadeByMods)
		{ 
			if($isBeforeMod===null){$isBeforeMod=true;}
			if($dissentBeforeMod===null)
			{
				$dissentBeforeMod=$this->rating!==0 && $nonModBeforeUs!==null && $nonModBeforeUs!==($this->rating>0);
			}
		}
		if($dissentBeforeMod===null){$dissentBeforeMod=false;}

		// If this rating is made by a moderator, do not compute reputation and weight
		$isEventAdmin=$this->useUser()->checkRoles('admin','moderator');
		if($isEventAdmin)
		{
			$display='Moderator';
			$reputation=0;
			$weight=0;

			if(!$retAllData){return [$reputation,$weight];}
			return ['publishingDecisionMadeByMods'=>$publishingDecisionMadeByMods,
					'modsAgree'=>$modsAgree,
					'agreesWithMods'=>true,
					'reputation'=>$reputation,
					'weight'=>$weight,
					'display'=>$display,
				   ];
		}

		$hasValidReputation=true;

		// Standard case : mods have rated or decided
		if($modsRatingCt>0)
		{
			// mods have only given publish/unpublish decision, but no ratings
			if($modsRatingCt<.1)
			{
				if($this->rating===0)
				{
					$reputation=0;
					$weight=1;
					$display.="Neutral. Mods decided.\n";
				}
				else
				{
					$reputation=($this->rating>0) === ($modsRatingTot>0) ? 1 : 0;
					if($reputation==1){$weight=1;$display.="Agrees with mod decision. weight=1\n";}
					else
					{
						$modsFakeRating=($modsRatingTot>0) ? 1 : -1;
						list($reputation,$weight)=self::ratingDistance($this->rating,$modsFakeRating);
						$display.="Disagrees with mod decision: weight=ratingDistance(".$this->rating.",$modsFakeRating)=".round($weight,3)."\n";
					}
				}
			}
			else
			// Mods have rated (might have decided too)
			{
				$modsRatingAvg=$modsRatingTot/$modsRatingCt;
				list($reputation,$weight)=self::ratingDistance($this->rating,$modsRatingAvg,$d);
				$display.="Mods have rated:	 modsRatingAvg=".round($modsRatingAvg,3).
					" weight=ratingDistance(".$this->rating.",".round($modsRatingAvg,3).")=".round($weight,3)."\n";
			}
		}
		else
		// Special case: mods have not rated nor decided: but event has been auto-published
		// (this is a situation where non-mods rate themselves)
		if($event->isOpinionAutoPublished())
		{
            $avgRating=demosphere_opinion_event_rating_average($event->id)['average'];
			list($reputation,$weight)=self::ratingDistance($this->rating,$avgRating,$d);
			$weight/=4;
			$display.="Auto-published: avgRating=".round($avgRating,3)." weight=ratingDistance(".$this->rating.",$avgRating)/4=".round($weight,3)."\n";
		}
		else
		{
			// Reputation cannot be determined yet (no decision, no mod rating, no autopublish)
			$reputation=1;
			$weight=0;
			$hasValidReputation=false;
		}
		
		// Modulate good reputation
		if($hasValidReputation && $reputation>0)
		{
			if($dissentBeforeMod===true)
			{
				$weight+=5;
				$display.="Dissent before mods! weight=weight+5=".round($weight,3)."\n";
			}

			if($isFirst===true)
			{
				$weight+=.5;
				$display.="First opinion on event and correct: weight=weight+.5=".round($weight,3)."\n";
			}

			if($isBeforeMod===false)
			{
				$weight/=3;
				$display.="Agrees after mod: weight=weight/3=".round($weight,3)."\n";
			}

			// events where everybody agrees dont add too much rep
			if(count($otherOpinions)>0)
			{
				$tmp=dlib_array_column($otherOpinions,'rating');
				$tmp[]=$this->rating;
				$minRating=count($tmp) ? min($tmp) : false;
				$maxRating=count($tmp) ? max($tmp) : false;

				$d=$maxRating-$minRating;
				$consensualAdj=$d<=3 ? 1/(4-$d) : 1+($d-3)/3;
				if($consensualAdj!=1)
				{
					$weight0=$weight;
					$weight*=$consensualAdj;
					$display.="Consensual adj=".round($consensualAdj,3).
						"  (min:$minRating max:$maxRating) weight=".round($consensualAdj,3)."*weight=".round($weight,3)."\n";
				}
			}

			// Rating one's own event does not add rep (unless rejected)
			if(!$isEventAdmin && $this->userId==$event->authorId)
			{
				$display.="Rating own event: weight=0\n";
				$weight=0;
			}
		}
		
		// old wording of "0" rating was ambigous and made it seem like "I'm not sure" 
		if($hasValidReputation && $reputation==0 && $this->rating===0 && $this->time<strtotime('Jul 3 2016'))
		{
			$weight/=3;
			$display.="Wording for rating 0 was ambigous 'Im not sure' weight=".round($weight,3)."\n";
		}

		if($hasValidReputation && !$modsAgree)
		{
			$display.="Mods disagree among themselves: weight=0\n";
			$weight=0;
		}

		if(!$hasValidReputation)
		{
			$display="Cannot determine reputation.\n";
		}

		if($this->modReputation!=0)
		{
			$r=$reputation>0.5 ? $weight : -$weight;
			$r+=$this->modReputation;
			$reputation=$r>=0 ? 1 : 0;
			$weight=abs($r);
			$display.="Mod manually adjusted reputation: ".round($this->modReputation,3).": reputation=$reputation weight=".round($weight,3)."\n";
		}

		if(!$this->isEnabled)
		{
			$reputation=0;
			$weight=0;
			$display.="This opinion is DISABLED => weight=0\n";
		}

		if(!$retAllData){return [$reputation,$weight];}

		$agreesWithMods=null;
		if($publishingDecisionMadeByMods && $modsAgree && $this->rating!==0)
		{
			$agreesWithMods=($this->rating>0) === ($event->status==1);
		}

		$display.="reputation=".round($reputation,3)." weight=".round($weight,3)."\n";

		return ['publishingDecisionMadeByMods'=>$publishingDecisionMadeByMods,
				'modsAgree'=>$modsAgree,
				'agreesWithMods'=>$agreesWithMods,
				'reputation'=>$reputation,
				'weight'=>$weight,
				'display'=>$display,
				];
	}

	function save()
	{
		list($this->reputation,$this->weight)=$this->calcReputation();
		parent::save();
		require_once 'demosphere-opinion.php';
		// Changing an opinion can change the reputation of all opinions on this event.
		// For example, when a moderator gives an opinion, other contributors reputiation is affected.
		demosphere_opinions_update_all_reputations_for_event($this->eventId);
	}

	function delete()
	{
		parent::delete();
		require_once 'demosphere-opinion.php';
		// Delete an opinion can change the reputation of all opinions on this event.
		demosphere_opinions_update_all_reputations_for_event($this->eventId);
	}

	//! Compares two ratings and returns reputation and weight.
	//! Optionally returns $d (ex: for display).
	static function ratingDistance($rating1,$rating2,&$d=null)
	{
		$d=abs($rating2-$rating1);
		// overlapping "0" is quite strong : add +1.5 to distance
		if($rating2!=0 && $rating1!=0 && (($rating2>0)!==($rating1>0))){$d+=1.5;}
		$reputation=$d<=2.5 ? 1 : 0;
		$weight=self::weightFct($d);
		return [$reputation,$weight];
	}

	static function plotWeightFct()
	{
		for($d=0;$d<=10;$d+=.1)
		{
			echo sprintf("%01f : %01f \n",$d,self::weightFct($d));
		}
	}

	static function weightFct($d)
	{
		global $demosphere_config;
		require_once 'demosphere-opinion.php';

		return linear_by_parts_fct($d,[
			[ 0	 ,1.5],
			[ 1.5,1	 ],
			[ 2	 ,0	 ],
			[ 2.5,0	 ],
			[ 4	 ,$demosphere_config['opinions_disagree_weight']	   ], 
			[ 6	 ,$demosphere_config['opinions_disagree_weight']*1.5   ],
			// going too high can cause a rating mistake/misunderstanding to have a too long term impact on the users rep
			[20	 ,$demosphere_config['opinions_disagree_weight']*1.5+10],
		]);
	}

	static function createTable()
	{
		db_query("DROP TABLE IF EXISTS Opinion");
		db_query("CREATE TABLE Opinion (
  `id` int(11) NOT NULL auto_increment,
  `userId`	int(11) NOT NULL,
  `eventId` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `text` longtext NOT NULL,
  `rating`	float NOT NULL,
  `weight`	float NOT NULL,
  `reputation`	float NOT NULL,
  `pubGuideline` longtext NOT NULL,
  `modComment` longtext NOT NULL,
  `modReputation`  float NOT NULL,
  `isEnabled`  tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `userId` (`userId`),
  KEY `eventId` (`eventId`),
  KEY `time` (`time`),
  KEY `rating` (`rating`),
  KEY `weight` (`weight`),
  KEY `reputation` (`reputation`),
  KEY `modReputation` (`modReputation`),
  KEY `isEnabled` (`isEnabled`)
) DEFAULT CHARSET=utf8mb4");
	}
}

?>