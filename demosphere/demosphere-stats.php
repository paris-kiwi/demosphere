<?php

//! Updates daily_visits table from raw_apache_log.
//! Called from cron.
//! Remembers information from last parse in several daily_visits variables.
//! Algorithm: A full list of all valid "hits" for today (and sometimes yesterday) is kept in a variable.
//! At each run of this function, new hits are added to the hits variable.
//! The per-event statistics are deleted and completely re-created at the end of this function.
//! Why: 
//! - the log file does not always contain a full day (logrotate), and this function runs before 23h59
//! - parsing the log file is very time consuming, we need to restart where we left, not re-parse whole file
//! - a table with all hits in for long periods of time would be too large
//! Note: all of this assumes 1 ip = 1 person. This assumption is not always true, specially on mobile
function demosphere_stats_daily_visits_update()
{
	global $demosphere_config;

	// no more than 15 events per IP (otherwise probably robot)
	$limitHits=15;
	$now=time();

	// **** load data from last parse
	$skipIps   =variable_get('skipIps'    ,'daily_visits',[]);
	$uaIsRobot =variable_get('uaIsRobot'  ,'daily_visits',[]);
	$hits      =variable_get('hits'       ,'daily_visits',[]);
	$lastUpdate=variable_get('lastUpdate' ,'daily_visits',['file-start'=>false,'position'=>false,'ts'=>0]);
	$mobileCt  =variable_get('mobileCt'   ,'daily_visits',[]);
	$uniqueIps =variable_get('uniqueIps'  ,'daily_visits',[]);

	// *** update data from last parse
	$skipIps=array_filter($skipIps,function($ts)use($now){return $ts>$now-3600*24*7;});

	// Skip mod and admin ips
	require_once 'demosphere-user.php';
	foreach(demosphere_user_admin_ips(3600*24*30) as $ip){$skipIps[$ip]=$now;}

	// For which days are we keeping data (today, and sometimes yesterday)
	$todayTs    =strtotime('3:33');
	$yesterdayTs=strtotime('Yesterday 3:33');
	// testing
	//$todayTs=strtotime('July 9 2014 3:33');
	//$yesterdayTs=strtotime('July 8 2014  3:33');
	$daysOfInterest=[$yesterdayTs,$todayTs];
	// If we have finished processing yesterday's logs, then ignore them
	if($lastUpdate['ts']>$todayTs){$daysOfInterest=[$todayTs];unset($hits[$yesterdayTs]);}

	// List of recent event ids. Needed to detect hits on old events that are likely to be bots.
	$recentEvents=array_flip(db_one_col('SELECT id FROM Event WHERE status=1 AND startTime>%d',$todayTs-3600*24*30*3));

	// make big regular expression to find robot user agents
	require_once 'demosphere-robots-regexp.php';
	$robotsREList=demosphere_robots_regexp();
	$robotsRE="/(".implode('|',$robotsREList).")/iS";

	$fname=$demosphere_config['raw_apache_log'];
	//$fname='paris-access.log';
	//$fname='paris-access.log.1';
	//$fname='test.log';
	//$fname='/tmp/log';
	$file=fopen($fname,'r');
	if($file===false){fatal("demosphere_stats_daily_visits_update: failed to read log file");}

	// **** Check if we can skip the beginning of file (already parsed)
	$line=fgets($file);
    if(trim($line)===''){return;}// empty file: no visits
	$lineParts=explode(' ',$line);
	$dateTime=str_replace('/',' ',substr($lineParts[3],1,11)).' '.substr($lineParts[3],13);
	$firstTs=strtotime($dateTime);
	if($lastUpdate['file-start']===$firstTs){fseek($file,$lastUpdate['position']);} // old file
	else{$lastUpdate['file-start']=$firstTs;fseek($file,0);}// new file

	$startTs=$lastUpdate['ts'];
	$ts=$startTs;
	$ipLooksAtPageRessources=[];
	$ipLooksAtOldEvents=[];

	// Parse file, line by line
	while(($line = fgets($file))!==false)
	{
		//echo "parsing:".trim($line)."\n";
		$lineParts=explode(' ',trim($line));
		$ip=$lineParts[0];
		$page=$lineParts[6];
		
		// anybody fetching robots.txt is a robot => bad ip
		if($page==="/robots.txt"){$skipIps[$ip]=$now;}	
		// skip bad ip's
		if(isset($skipIps[$ip])){continue;}

		// We are only interested by event pages
		if(strpos($page,'/'.$demosphere_config['event_url_name'])!==0)
		{
			if(preg_match('@^/(files|demosphere/css|demosphere/js)@',$page)){$ipLooksAtPageRessources[$ip]=true;}
			continue;
		}
		// Ignore event editors => we don't want to have mods in stats
		if($lineParts[5]==='"POST' && strpos($page,'/edit')!==false){$skipIps[$ip]=$now;continue;}
		if(!preg_match('@^/'.preg_quote($demosphere_config['event_url_name'],'@').'/([0-9]+)($|\?)@',$page,$pageMatches)){continue;}
		$eventId=(int)$pageMatches[1];

		// parse date/time
		$dateTime=str_replace('/',' ',substr($lineParts[3],1,11)).' '.substr($lineParts[3],13);
		$ts=strtotime($dateTime);
		$dayTs=strtotime('3:33',$ts);
		if(array_search($dayTs,$daysOfInterest)===false){continue;}// Strange. Old log file?

		// Too many hits = robot or moderator
		if(isset($hits[$dayTs][$ip]) && count($hits[$dayTs][$ip])>$limitHits){$skipIps[$ip]=$now;}

		// ignore hits before $startTs (this should not happen, as we fseek to good position in log file)
		if($ts<=$startTs){continue;}

		// Hits on old events (likely robot, needs more info).
		 if(!isset($recentEvents[$eventId])){$ipLooksAtOldEvents[$ip]=true;}

		// check user agents for robots
		$ua=substr(implode(' ',array_splice($lineParts,11)),1,-1);
		if(!isset($uaIsRobot[$ua]))
		{
			// This preg_match with a huge regexp is slow 
			$uaIsRobot[$ua]=(bool)preg_match($robotsRE,$ua);
		}
		if($uaIsRobot[$ua]===true){$skipIps[$ip]=$now;continue;}

		$hits[$dayTs][$ip][$eventId]=demosphere_detect_mobile($ua);
	}

	// ips that look at old events and at no CSS/images/... are deemed to be robots
	foreach($ipLooksAtOldEvents as $ip=>$unused)
	{
		if(!isset($ipLooksAtPageRessources[$ip]) && !isset($skipIps[$ip])){$skipIps[$ip]=$now;}
	}

	//print_r($skipIps);
	//print_r(preg_grep('@1@',$uaIsRobot,PREG_GREP_INVERT));

	// 2nd pass is necessary to remove hits that were added before $ip was put into $skipIps .
	foreach(array_keys($hits) as $dayTs)
	{
		$hits[$dayTs]=array_diff_key($hits[$dayTs],$skipIps);
	}

	// *** unique ips
	foreach($daysOfInterest as $dayTs)
	{
		if(isset($hits[$dayTs])){$uniqueIps[$dayTs]=count($hits[$dayTs]);}
	}

	// switch from $hits to $events
	$events=[];
	foreach($daysOfInterest as $dayTs)
	{
		if(!isset($hits[$dayTs])){continue;}
		$mobileCt[$dayTs]=['mobile'=>0,'tot'=>0];
		foreach($hits[$dayTs] as $ip=>$dayEvents)
		{
			// Limit number of hits per IP per day to 3 (random selection)
			if(count($dayEvents)>5){$dayEvents=array_flip(array_rand($dayEvents,5));}
			foreach($dayEvents as $eid=>$isMobile)
			{
				if(!isset($events[$eid][$dayTs])){$events[$eid][$dayTs]=0;}
				$events[$eid][$dayTs]++;
			}
			if($isMobile){$mobileCt[$dayTs]['mobile']++;}
			$mobileCt[$dayTs]['tot']++;
		}
	}

	// Only keep visits for a little more than a month
	db_query('DELETE FROM daily_visits WHERE day<%d',$now-3600*24*35);
	
	// delete whole day for considered days
	foreach($daysOfInterest as $dayTs)
	{
		db_query('DELETE FROM daily_visits WHERE day=%d',$dayTs);
	}

	// add results into db 
	$sql='';
	foreach($events as $eid=>$days)
	{
		foreach($days as $day=>$count)
		{
			//echo "$eid $day $count\n";
			$sql.=sprintf('(%d,%d,%d),',$eid,$day,$count);
		}
	}
	if($sql!==''){db_query('INSERT INTO daily_visits (event,day,hits) VALUES '.substr($sql,0,-1));}


	//echo "hits:\n";print_r($hits);
	//echo "hits days:\n";print_r(array_keys($hits));	
	//echo "lastUpdate:\n";print_r($lastUpdate);
	//echo "skipIps:\n";print_r($skipIps);
	//echo "uaIsRobot:\n";print_r($uaIsRobot);

	// *** update information for next parse
	$lastUpdate['position']=filesize($fname);
	$lastUpdate['ts']=$ts;
	// Purge hits for old days 
	foreach(array_keys($hits) as $dayTs)
	{
		if($dayTs===$todayTs){continue;}
		// special case: keep yesterday's hits if we haven't finished parsing them
		if($dayTs===$yesterdayTs && $lastUpdate['ts']<$todayTs){continue;}
		unset($hits[$dayTs]);
	}
	if(count($uaIsRobot)>1000){$uaIsRobot=array_slice($uaIsRobot,count($uaIsRobot)-1000,NULL,true);}
	unset($mobileCt[$yesterdayTs]);

	// *** save information for next parse
	variable_set('lastUpdate','daily_visits',$lastUpdate);
	variable_set('hits'      ,'daily_visits',$hits);
	variable_set('skipIps'   ,'daily_visits',$skipIps);
	variable_set('uaIsRobot' ,'daily_visits',$uaIsRobot);
	variable_set('mobileCt'  ,'daily_visits',$mobileCt);
	variable_set('uniqueIps' ,'daily_visits',$uniqueIps);
}

//! Figure out which events are most viewed for each day and save it to event table.
function demosphere_stats_day_rankings()
{
	global $demosphere_config;

	db_query("SET time_zone = '%s'",date("P"));
	$events=db_arrays("SELECT Event.id AS id,SUM(hits) AS visits,DATE(FROM_UNIXTIME(startTime)) AS sqlday ".
					  "  FROM Event,daily_visits ".
					  "  WHERE Event.id=daily_visits.event AND ".
					  "    Event.startTime>%d ".
					  "  GROUP BY Event.id ".
					  // Event.status DESC: special case when a very visited event was unpublished
					  "  ORDER BY sqlday ASC,Event.status DESC,SUM(hits) DESC",
					  Event::today());

	$day=false;
	$rank=false;
	foreach($events as &$event)
	{
		if($day!==$event['sqlday']){$day=$event['sqlday'];$rank=1;}
		$event['rank']=$rank;
 		db_query("UPDATE Event SET dayrank=%d,visits=%d WHERE id=%d",
				 $event['rank'],$event['visits'],$event['id']);
		$rank++;
	}
	unset($event);
}

//! Parse awstats database (/var/lib/awstats...), compute stats for today and store in variable "awstats-last".
//! Stats computed: todays vists, yesterdays, today's predicted.
function demosphere_stats_awstats_cron()
{
	global $demosphere_config;
	if($demosphere_config['awstats_name']===false){return;}

	$tmpDir=$demosphere_config['tmp_dir'];

	require_once 'demosphere-user.php';
	$ips=demosphere_user_admin_ips(3600*24*30);
	file_put_contents("files/private/awstats-skip-ips",implode("\n",$ips));

	// **** fetch and parse awstats data file
	$name=$demosphere_config['awstats_name'];
	if($name===false){return;}
	$awstatsFile='/var/lib/awstats-'.$name.'/';
	$awstatsFile.='awstats'.date('mY').'.'.$name.'.txt';
	if(!file_exists($awstatsFile)){echo "demosphere_stats_awstats_cron: awstats file not found, aborting:".ent($awstatsFile)."\n";return;}
	$awstats=awstats_parse($awstatsFile,['DAY','GENERAL','TIME']);
	if($awstats===false)
	{
		echo "demosphere_stats_awstats_cron: awstats file is invalid:".ent($awstatsFile)."\n";
		// ignore errors on first day of month (empty stats)
		if(date('n')!=1){fatal('awstats_parse failed');}
        return;
	}

	$lastUpdate=$awstats['GENERAL']['LastUpdate']['value'];
	$lastUpdateTs=strtotime(preg_replace('@([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})@','$1-$2-$3 $4:$5:$6',$lastUpdate));

	// *********** todays visits	
	$visits=isset($awstats['DAY'][date('Ymd')]) ? 
		intval(   $awstats['DAY'][date('Ymd')]['Visits']) : 0;

	// *********** yesterdays visits

	$yesterdaysVisists=isset($awstats['DAY'][date('Ymd',strtotime('yesterday 10:00'))]) ? 
		intval(              $awstats['DAY'][date('Ymd',strtotime('yesterday 10:00'))]['Visits']) : 0;


	// *********** parse hourly table, necesary for predicting todays vists
	// use last month's (LM) hourly stats (current month is incomplete and biased)
	$awstatsLM=false;
	if(date('j')<15)
	{
		$awstatsFileLM='/var/lib/awstats-'.$name.'/';
		$awstatsFileLM.='awstats'.date('mY',strtotime('last month')).'.'.$name.'.txt';
		if(file_exists($awstatsFileLM))
		{
			$awstatsLM=awstats_parse($awstatsFileLM,['DAY','GENERAL','TIME']);
		}
	}
	if($awstatsLM===false){$awstatsLM=$awstats;}

	foreach($awstatsLM['TIME'] as $hour=>$hdata)
	{
		$hourly[$hour]=intval($hdata['Pages']);
	}

	// *********** predict todays total vists, 
	// using todays current visits and hourly statistics
	$total=0;
	$h=substr($lastUpdate,8,2);
	foreach($hourly as $hour => $nb)
	{
		if($h==$hour)
		{
			$c=intval(preg_replace('@^0*@','',substr($lastUpdate,10,2)))/60.0;
			$estTotal=$total+$c*$nb; /* $c*$nb adds a fraction of this hour */
		}
		$total+=$nb;
	}
	$predicted=intval($estTotal!=0 ? $visits*$total/$estTotal : -1);

	// ********** save results
	variable_set('awstats-last','',['ts'=>$lastUpdateTs,
									'visits'=>$visits,
									'predicted'=>$predicted,
									'yesterday'=>$yesterdaysVisists]);
}

function demosphere_stats_daily_visits_fetch()
{
	global $demosphere_config;
	$good_token=substr(hash("sha256",$demosphere_config["secret"].":"."qMr3q4GWgwxyGNWw"),0,20);
	if(val($_GET,'token')!==$good_token){dlib_permission_denied_403();}
	if(isset($_GET['kini-ev-visits']))
	{
		return db_one_col("SELECT DATE_FORMAT(FROM_UNIXTIME(MIN(startTime)),'%%Y-%%m'), ROUND(SUM(visits)/1.86873186904268545379) FROM Event WHERE startTime<%d GROUP BY DATE_FORMAT(FROM_UNIXTIME(startTime),'%%Y-%%m') ORDER by startTime ASC",strtotime("last day of previous month 23:59"));
	}

	return 	variable_get('uniqueIps'  ,'daily_visits',[]);
}

function demosphere_stats_daily_visits_install()
{
	variable_delete(false,'daily_visits');
	db_query("DROP TABLE IF EXISTS daily_visits");
	db_query("CREATE TABLE daily_visits (
  `event` int(11) NOT NULL,
  `day` int(11) NOT NULL,
  `hits` int(11) NOT NULL,
  PRIMARY KEY  (`day`,`event`),
  KEY `day` (`day`),
  KEY `event` (`event`)
) DEFAULT CHARSET=utf8mb4");

}

// *************************************************************
// user and widget stats
// *************************************************************


//! Display information about registered users
//! FIXME: This is just a begining, lots of other info should be displayed
function demosphere_stats_users()
{
	global $currentPage;
	require_once 'dlib/template.php';

	$currentPage->title=t('Users stats');
	$currentPage->addJs('lib/jquery.js');
	$currentPage->addJs('dlib/form.js');
	$currentPage->addCss('dlib/form.css');

	$users=User::fetchList('WHERE 1=1');

	$stats=['admins'=>[],
			'moderators'=>[],
			'ordinary'=>[],
			'active'=>[],
			'email'=>[],
			'email-settings'=>['how-often-days'=>0],
		   ];
	$now=time();
	require_once 'demosphere-email-subscription.php';
	$emailDefaults=demosphere_email_subscription_default_values();

	foreach($users as $uid=>$cuser)
	{
		$priv=false;
		if($cuser->checkRoles('admin','moderator')){$stats['moderators'][]=$cuser;$priv=true;}
		if($cuser->checkRoles('admin'            )){$stats['admins'    ][]=$cuser;$priv=true;}
		if(!$priv){$stats['ordinary'][]=$cuser;}
		if($now-$cuser->lastAccess<3600*24*30*3){$stats['active'][]=$cuser;}
		// email stats
		if(isset($cuser->data['demosphere_email_subscription']))
		{
			$email=$cuser->data['demosphere_email_subscription'];
			if(val($email,'active'))
			{
				$stats['email'][]=$cuser;
				foreach(['select-topic','select-city-id','select-fp-region','select-place-reference-id','near-lat-lng-use-map','map-shape','most-visited','how-often-days','start-time','how-many-events','select-recent-changes','urgent-events'] as $k)
				{
					if($email[$k]!=$emailDefaults[$k]){$stats['email-settings'][$k]=val($stats['email-settings'],$k,0)+1;}
				}
			}
		}
	}

	$sentEmails=variable_get('email_subscription_sent_count','',0);

	$publicFormContacts=[];
	$extraData=db_one_col("SELECT extraData FROM Event WHERE extraData LIKE '%%\"public-form\";%%@%%'");
	foreach($extraData as $e)
	{
		$a=unserialize($e,['allowed_classes'=>false]);
		$contact=val(val($a,'public-form',[]),'contact');
		if(!preg_match('/([^\s]+@[^\s]+)/',$contact,$m)){continue;}
		$publicFormContacts[$m[1]]=val($publicFormContacts,$m[1],0)+1;
	}
	arsort($publicFormContacts);

	//var_dump($stats['email-settings']);
	$authored=db_one_col("SELECT authorId,COUNT(*) FROM Event GROUP BY authorId");
	//var_dump($authored);

	return template_render('templates/demosphere-user-stats.tpl.php',
						   compact('stats','authored','sentEmails','publicFormContacts'));

}


//! This are very basic stats of widget usage, directly parsed from log file
function demosphere_stats_widget()
{
	global $demosphere_config,$currentPage;
	require_once 'demosphere-date-time.php';

	// log start time
	$lines=[];
	exec("head -n 1 ".escapeshellarg($demosphere_config['raw_apache_log'])." | cut -d ' ' -f 4",$lines,$ret);
	//var_dump($lines,$ret,$demosphere_config['raw_apache_log']);
	$p=explode(':',substr($lines[0],1));
	preg_match('@^\[(..)/(...)/(....):(..):(..):(..)$@',$lines[0],$m);
	$start=strtotime($m[2].' '.$m[1].' '.$m[3].' '.$m[4].':'.$m[5]);

	// log end time
	$lines=[];
	exec("tail -n 1 ".escapeshellarg($demosphere_config['raw_apache_log'])." | cut -d ' ' -f 4",$lines,$ret);
	$p=explode(':',substr($lines[0],1));
	preg_match('@^\[(..)/(...)/(....):(..):(..):(..)$@',$lines[0],$m);
	$end=strtotime($m[2].' '.$m[1].' '.$m[3].' '.$m[4].':'.$m[5]);

	$lines=[];
	exec("grep -E 'GET /widget-(js|html)' ".escapeshellarg($demosphere_config['raw_apache_log']).' | '.
		 "cut -d ' ' -f 1,7,11 | sort -u  | cut -d ' ' -f 2,3 | ". // unique IPS
		 "sed -r 's@\"(https?://[^/]*).*\$@\\1@g' | ". // only referer base url, not full url
		 "sort | uniq -c | sort -r -g | awk '{print \$1 \" \" \$2 \" \" \$3}' " // group and count 
		 ,$lines,$ret);

	$out='';
	$out.='Log starts at: '.ent(demos_format_date('full-date-time',$start)).'<br/>';
	$out.='Log ends at: '.  ent(demos_format_date('full-date-time',$end  )).'<br/>';
	$out.='<table id="widget-stats">';
	$out.='<tr><th>ip/day</th><th>unique ips</th><th></th><th></th></tr>';
	foreach($lines as $line)
	{
		$cols=explode(" ",$line);
		$visits=intval($cols[0]);
		$widgetPath=$cols[1];
		$referer=$cols[2];
		$url=preg_match('@^https?://[a-z0-9.-]+$@i',$referer) ? $referer : false;
		$out.='<tr>';
		$out.='<td>'.round(24*3600*$visits/($end-$start),2).'</td>';
		$out.='<td>'.ent($visits).'</td>';
		$out.='<td>'.ent($widgetPath).'</td>';
		if($url===false){$out.='<td>'.ent($referer).'</td>';}
		else
		{$out.='<td><a href="'.ent($url).'">'.ent($url).'</a></td>';}
		$out.='</tr>'."\n";
	}
	//echo $out;
	$currentPage->addCssTpl('demosphere/css/demosphere-stats.tpl.css');
	return $out;
}

// *************************************************************
// Email bounces stats
// *************************************************************

//! Display email bounces
function demosphere_stats_email_bounces()
{
	global $demosphere_config,$base_url,$currentPage;
	if(!$demosphere_config['hosted']){fatal('demosphere_stats_email_bounces: not on hosted demosphere site');}
	$currentPage->addCssTpl('demosphere/css/demosphere-stats.tpl.css');
    require_once 'demosphere-emails.php';
	$bounces=demosphere_emails_bounces_parse();
	$now=time();
	$rows=[];
	foreach($bounces as $email=>$tss)
	{
		$row=[];
		$uids=db_one_col("SELECT id FROM User WHERE email='%s'",$email);
		$cuser=User::fetch(val($uids,0,0),false);
		$subs=[];
		$row['email']=$email;
		$row['all']=count($tss);
		if($cuser!==null)
		{
			$subs=val($cuser->data,'demosphere_email_subscription',[]);
			$row['user']=$cuser;
			$row['active' ]=val($subs,'active');
			$row['24h-tot']=val($subs,'how-often-days') ? round((1   )/val($subs,'how-often-days'),1) : 0;
			$row['30d-tot']=val($subs,'how-often-days') ? round((30  )/val($subs,'how-often-days')) : 0;
			$row['90d-tot']=val($subs,'how-often-days') ? round((30*3)/val($subs,'how-often-days')) : 0;
		}
		$row['24h']=count(array_filter($tss,function($ts)use($now){return $ts>$now-3600*24*1   ;}));
		$row['30d']=count(array_filter($tss,function($ts)use($now){return $ts>$now-3600*24*30  ;}));
		$row['90d']=count(array_filter($tss,function($ts)use($now){return $ts>$now-3600*24*30*3;}));
		$rows[]=$row;
	}

	usort($rows,function($a,$b){return $a['30d']===$b['30d'] ? 0 : ($a['30d']>$b['30d'] ? -1 : 1);});

	$out='';
	$out.='<h1 class="page-title">'.t('Email bounces').'</h1>';
	$out.='<p>'.t('Number of email bounces (errors) received. Users that have a lot of bounces are automatically unsubscribed.').'</>';
	$out.='<table id="email-bounces">';
	$out.='<tr><th>user</th><th>subs.</th><th>email</th><th>last 24h</th><th>last 30d</th><th>last 90d</th><th>all</th></tr>';
	foreach($rows as $row)
	{
		$cuser=val($row,'user');
		$out.='<tr>';
		if($cuser===false){$out.='<td>-</td><td>-</td>';}
		else
		{
			$out.='<td><a href="'.$base_url.'/user/'.$cuser->id.'">'.ent($cuser->login).'</a></td>';
			$out.='<td><a href="'.$base_url.'/email-subscription-form?uid='.$cuser->id.'">'.(val($row,'active') ? '✓' : 'x' ).'</td>';
		}
		$out.='<td>'.ent($row['email']).'</td>';
		$out.='<td>'.$row['24h'].'/'.val($row,'24h-tot','-').'</td>';
		$out.='<td>'.$row['30d'].'/'.val($row,'30d-tot','-').'</td>';
		$out.='<td>'.$row['90d'].'/'.val($row,'90d-tot','-').'</td>';
		$out.='<td>'.$row['all'].'</td>';
		$out.='</tr>';
	}
	return $out;
}


// *************************************************************
// Stats utility functions
// *************************************************************

//! Parse an awstats data file
function awstats_parse($fname,$sections=false)
{
	require_once 'dlib/parse-file.php';
	$awstats=file_get_contents($fname);
	// check for empty stats : this happens at the begining of each month
	if(preg_match('@^FirstTime $@m',$awstats)){return false;}

	$res=[];
	static $allSectionsSchemas=
		['MAP'           =>['schema'=>['name','value'],'key'=>'name',],
		 'GENERAL'       =>['schema'=>['name','value'],'key'=>'name',],
		 'TIME'          =>['schema'=>['Hour','Pages','Hits','Bandwidth','Not viewed Pages','Not viewed Hits','Not viewed Bandwidth'],'key'=>'Hour',],
		 'DOMAIN'        =>['schema'=>['Domain','Pages','Hits','Bandwidth'],'key'=>'Domain',],
		 'CLUSTER'       =>['schema'=>['Cluster ID','Pages','Hits','Bandwidth'],'key'=>'Cluster ID',],
		 'LOGIN'         =>['schema'=>['Login','Pages','Hits','Bandwidth','Last visit'],'key'=>'Login',],
		 'ROBOT'         =>['schema'=>['Robot ID','Hits','Bandwidth','Last visit','Hits on robots.txt'],'key'=>'Robot ID',],
		 'WORMS'         =>['schema'=>['Worm ID','Hits','Bandwidth','Last visit'],'key'=>'Worm ID',],
		 'EMAILSENDER'   =>['schema'=>['EMail','Hits','Bandwidth','Last visit'],'key'=>'EMail',],
		 'EMAILRECEIVER' =>['schema'=>['EMail','Hits','Bandwidth','Last visit'],'key'=>'EMail',],
		 'FILETYPES'     =>['schema'=>['Files type','Hits','Bandwidth','Bandwidth without compression','Bandwidth after compression'],'key'=>'Files type',],
		 'OS'            =>['schema'=>['OS ID','Hits'],'key'=>'OS ID',],
		 'BROWSER'       =>['schema'=>['Browser ID','Hits'],'key'=>'Browser ID',],
		 'SCREENSIZE'    =>['schema'=>['Screen size','Hits'],'key'=>'Screen size',],
		 'UNKNOWNREFERER'=>['schema'=>['Unknown referer OS','Last visit date'],'key'=>'Unknown referer OS',],
		 'UNKNOWNREFERERBROWSER'=>['schema'=>['Unknown referer Browser','Last visit date'],'key'=>'Unknown referer Browser',],
		 'ORIGIN'        =>['schema'=>['Origin','Pages','Hits'],'key'=>'Origin',],
		 'SEREFERRALS'   =>['schema'=>['External page referers','Pages','Hits'],'key'=>'External page referers',],
		 'PAGEREFS'      =>['schema'=>['Search keyphrases','Number of search'],'key'=>'Search keyphrases',],
		 'SEARCHWORDS'   =>['schema'=>['Search keywords','Number of search'],'key'=>'Search keywords',],
		 'KEYWORDS'      =>['schema'=>['Search keywords','Number of search'],'key'=>'Search keywords',],
		 'ERRORS'        =>['schema'=>['URL with 404 errors','Hits','Last URL referer'],'key'=>'URL with 404 errors',],
		 'SIDER_404'     =>['schema'=>['Host','Pages','Hits','Bandwidth','Last visit date','[Start date of last visit]','[Last page of last visit]'],'key'=>'Host',],
		 'VISITOR'       =>['schema'=>['Host','Pages','Hits','Bandwidth','Last visit date','[Start date of last visit]','[Last page of last visit]'],'key'=>'Host',],
		 'DAY'           =>['schema'=>['Date','Pages','Hits','Bandwidth','Visits'],'key'=>'Date',],
		 'SESSION'       =>['schema'=>['Session range','Number of visits'],'key'=>'Session range',],
		 'SIDER'         =>['schema'=>['URL','Pages','Bandwidth','Entry','Exit'],'key'=>'URL',],
		];
	if($sections===false){$schemas=$allSectionsSchemas;}
	else
	{
		$schemas=array_intersect_key($allSectionsSchemas,array_flip($sections));
	}

	foreach($schemas as $section=>$options)
	{
		$start=strpos($awstats,"\nBEGIN_".$section.' ');
		if($start===false){return false;}
		$startD=strpos(substr($awstats,$start+1),"\n");
		$start+=$startD+2;
		$end=strpos($awstats,"\nEND_".$section."\n")+1;
		if($end  ===false){return false;}
		$data=substr($awstats,$start,$end-$start);

		$options['rawData']=$data;
		$options['separator']=' ';
		$options['trim']=true;
		// FIXME: this is a strange way of handling stuff.
		if(count(preg_grep('@\[@',$options['schema']))){$options['failOnError']=false;}
		$parsed=parse_file(false,$options);
		if($parsed===false){return false;}
		$res[$section]=$parsed;
	}
	return $res;
}


?>