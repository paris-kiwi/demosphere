<?php
/**
 * @file
 * All functions needed to implement the control panel, entry
 * point is: demosphere_control_panel()
 *
 * This file is included directly by the drupal menu system.
 */

/** 
 * A control panel providing important information, like stats and user activty 
 * as well as links to common tasks.
 *
 * This function is called by the drupal menu system.
 * The control panel is made up of "blocks". Thes
 * Blocks are made of html that can be edited in a post.
 * Blocks in the post are separated by horizontal lines (<hr>).
 * The html uses a lot of ptokens (special words starting with "dpr_").
 * These ptokens are replaced with text generated by calling php functions.
 * Ptokens can have arguments, for example  dpr_block(width=230px)
 * The actual html is stored in a Post that an admin can manually edit.
 * Each block is configurable : it can be closed/opened by the user and moved around.
 * This is persistent (saved using ajax), so that each user will keep his settings.
 * To create a new ptoken: add the ptoken's name to the $ptokens array in demosphere_control_panel(),
 * and then create your ptoken function that returns the ptoken's html.
 */
function demosphere_control_panel()
{
	global $base_url,$user,$currentPage;
	global $demosphere_config;
	$isLoggedIn=$user->id!=0;
	require_once 'dlib/filter-xss.php';

	$currentPage->title=t('panel');

	$userPrefs=$user->data;
	$userPrefs=val($userPrefs,'demosphere_control_panel_prefs',[]);

	$currentPage->bodyClasses[]='unPaddedContent';
	$currentPage->bodyClasses[]="nonStdContent"; 

	$currentPage->addCssTpl('demosphere/css/demosphere-panel.tpl.css');
	$currentPage->addJs ('lib/jquery.js');
	$currentPage->addJs ('demosphere/js/demosphere-panel.js');

	// Get the html for the control panel.
	$post=Post::fetch(Post::builtInPid('control_panel_data'));

	// get saved user preference data (that was set using ajax)
	$blockJsData=val($userPrefs,'blocks',[]);

	// split into blocks
	$body=$post->body;
	$body=filter_xss_admin($post->body);
	$blocks=preg_split('@<hr[^>]*>@',$body);

	// replace all ptokens in all blocks
	$ptokens=['block','most_viewed_today','online_users',
			  'recent_users','recent_changes',
			  'nb_comments_today','carpools','nb_published_future_events','nb_published_events','nb_needs_attention',
			  'nb_incomplete_events','nb_events_mstatus','awstats','demosphere_config',
			  'config_checks','username','uid',
			  'email_subscribers','cache_status',
			  'opinions','software_forum'];

	foreach(array_keys($blocks) as $nbBlock)
	{
		$block=&$blocks[$nbBlock];
		foreach($ptokens as $ptoken)
		{
			$fullPtoken='dpr_'.$ptoken;
			// find and replace all occurences of this ptoken inside the current block
			while(($pos=strpos($block,$fullPtoken))!==false)
			{
				preg_match('@^'.$fullPtoken.'(\([^)]*\))?@',substr($block,$pos),$matches);
				$ptokenSize=strlen($matches[0]);
				$block=substr($block,0,$pos).''.substr($block,$pos+$ptokenSize);
				// parse arguments for this ptoken
				$info=['block'=>&$block,'pos'=>$pos,'nbBlock'=>$nbBlock];
				$args0=count($matches)>1 ? explode(",",substr($matches[1],1,-1)) : [];
				$args=[];
				foreach($args0 as $a)
				{
					$t=explode('=',$a);
					if(count($t)!=2){dlib_message_add('bad ptoken argument');continue;}
					$args[trim($t[0])]=trim($t[1]);
				}
				//$args=array_merge($args,$info);
				// call ptoken function with its arguments
				$fctName='demosphere_panel_ptoken_'.$ptoken;
				$ret=$fctName($info,$args);
				// if ptokens mangage their own replacements, they must return false
				if($ret!==false)
				{$block=substr($block,0,$pos).''.$ret.''.substr($block,$pos);}
			}
		}
	}
	// add user preferences to js
	$currentPage->addJsVar('blocks',$blockJsData);

	// now generate html for control panel
	$output='<h1 class="page-title">'.t('Control panel');
	if($user->checkRoles('admin'))
	{
		$output.='<a class="title-link" href="'.
			$base_url.'/control-panel-data">('.t('edit').')</a>';
	}
	$output.='<a class="title-link" href="'.$base_url.'/control-panel/reset-preferences">'.
		'('.t('reset blocks').')</a>';
	$output.='</h1>';
	$output.='<div id="closed-blocks"><div id="closed-blocks-bg"></div></div>';
	$output.='<div id="panel-blocks">'.implode("\n",$blocks).'</div>';
	return $output;
}

/** Saves blocks stauts (opened/closed) and order into the user's data.
 *
 * This is called in async-js. Block information is in json (js) format.
 */
function demosphere_panel_ajax_save()
{
	global $user;
	// validate json data (security). FIXME: this is ugly.
	if(!preg_match('@^\[(\{"nb":[0-9]+,"closed":(true|false)\},?)+\]$@',$_POST['data']))
	{
		fatal("demosphere_panel_ajax_save syntax error");
	}
	// decode json into a php array
	$received=json_decode($_POST['data'],true);
	// save it into user data
	$user->data['demosphere_control_panel_prefs']['blocks']=$received;
	$user->save();
	echo 'demosphere_panel_ajax_save ok';
}

function demosphere_panel_reset_preferences()
{
	global $user,$base_url;
	$user->data['demosphere_control_panel_prefs']=[];
	$user->save();
	dlib_redirect($base_url.'/control-panel');
}

/** This is a ptoken that sets information for the current block.
 *
 * usage example: dpr_block(width=230px,background-color=f00,class=toto,title-color=0f0) 
 * The folowing information can be set:
 *  - width: example 200px
 *  - user-access: calls the user_access function to check if this block should
 *    be open or closed by default. Example: 
 * 
 */
function demosphere_panel_ptoken_block(&$info,$args)
{
	global $user;
	$width     =val($args,'width'      	   );
	$access	   =val($args,'user-access'	   ,true);
	$class	   =val($args,'class'		   );
	$titleColor=val($args,'title-color'	   );
	$bgColor   =val($args,'background-color');

	$block=&$info['block'];
	// validate block syntax and argument syntax
	$message='BAD BLOCK '.$info['nbBlock'].':';
	if(strpos($block,'<h2')===false)
	{return $message.'no h2 title found';}
	if($width!=='auto' && !preg_match('@^[0-9]+(px|%|em|ex)$@',$width))
	{return $message.'invalid width';}
	if($class     !==false && preg_match('@[^0-9a-z_-]@',$class))
	{return $message.'invalid class';}
	if($titleColor!==false && preg_match('@[^0-9a-f]@'  ,$titleColor))
	{return $message.'invalid title-color';}
	if($bgColor   !==false && preg_match('@[^0-9a-f]@'  ,$bgColor))
	{return $message.'invalid background-color';}

	// FIXME: this is ok for now, but we should simplify "user-access=administer users" to "adminOnly in all panels"
	if($access!==true){$access=$user->checkRoles('admin');}

	preg_match('@^(.*)<h2([^>]*)>([^>]*)</h2>(.*)$@Us',$block,$matches);
	$block='<div id="block-'.$info['nbBlock'].'" '.
		'class="panel-block '.
		($class!==false ? $class.' ' :'').
		($access ? '': 'closed').'" '.
		'style="'.($width!==false ? 'width: '.$width.';' :'').
		($bgColor!==false ? 'background-color: #'.$bgColor.';' : '').
		'" >'.
		'<h2 class="panel-block-title" '.
		'style="'.($titleColor!==false ? 'background-color: #'.$titleColor.';':'').'"'.
		'>'.$matches[3].'</h2>'.
		'<div class="panel-block-contents" '.
		'>'.$matches[4].
		'</div></div>';
	// New policy (2/2011) : completely remove blocks that are not accessible
	if(!$access){$block='';}

	return false;
}

function demosphere_panel_ptoken_online_users(&$info,$args)
{
	global $base_url;
	$users=db_arrays("SELECT id,login FROM User WHERE lastAccess>%d",time()-60*15);
	$out='<ul class="online_users">';
	foreach($users as $user)
	{
		$out.='<li><a href="'.$base_url.'/user/'.$user['id'].'">'.ent($user['login']).'</a></li>';
	}
	$out.='</ul>';
	return $out;
}
function demosphere_panel_ptoken_username(&$info,$args)
{
	global $user;
	return $user->name;
}
function demosphere_panel_ptoken_uid(&$info,$args)
{
	global $user;
	return $user->id;
}

function demosphere_panel_ptoken_cache_status(&$info,$args)
{
	global $demosphere_config,$base_url;
	return '<a href="'.ent($base_url.'/configure/misc-config#compress_css'     ).'">css</a>:  '.($demosphere_config["compress_css"     ] ? 'on':'off').', '.
		   '<a href="'.ent($base_url.'/configure/misc-config#page_cache_enable').'">page</a>: '.($demosphere_config["page_cache_enable"] ? 'on':'off');
}

function demosphere_panel_ptoken_recent_users(&$info,$args)
{
	global $base_url;
	require_once 'demosphere-date-time.php';
	$n=val($args,'n',7);
	// List last access for $n users:
	$output='<table>';
	$users=db_arrays('SELECT id,login,role,lastAccess FROM User ORDER BY lastAccess DESC LIMIT %d', 
						   intval($n));
	foreach($users as $user) 
	{
		if($user['lastAccess']==0){continue;}
		$isRecent=$user['lastAccess']>time()-60*15;
		$output.= '<tr class="'.($isRecent ? 'recent-user' : '').' role-'.str_replace(' ','-',$user['role']).'">'.
			'<td><a href="'.$base_url.'/user/'.intval($user['id']).'">'.ent($user['login']).'</a></td>'.
			'<td>'.
			demos_format_date('relative-short',$user['lastAccess']).'</td></tr>';
	}
	$output.= '</table>';  
	return $output;
}


/** Recently changed events & posts.
 *
 * We use the log table to find edits, and then fetch information on users 
 * and events. 
 */
function demosphere_panel_ptoken_recent_changes(&$info,$args)
{
	global $base_url,$demosphere_config;
	$n=val($args,'n',10);
	$output='';
	$output.='<table class="recent-changes-table">';
	
	$pager=new Pager(['defaultItemsPerPage'=>$n,'prefix'=>'log']);

	// for profiling : 
	//db_query("UPDATE log SET message='dfsdf".mt_rand()."' WHERE id=1");
	//$start = microtime(true);

	// FIXME: (11/2016): the log table is not really used. 
	// Currently all of its information is also carried by Event::changed and Post::changed
	// We need to either drop the log table or make some better use of it 

	// We only want one log entry for each Event or Post (the most recent one)
	// The simple query is way too slow. 
	// The GROUP BY clause forces it to group on all items of this very large table.
	// To make it much faster we use $beginId, which restricts search to recent log items.
	// $beginId is determined heuristically (almost always ok), and is increased if we did not get enough results.
	$maxId=db_result("SELECT MAX(id) FROM log");
	$mult=300;
	do
	{
		$beginId=$maxId-$pager->itemsPerPage*$mult;
		$logIds=db_one_col("SELECT SQL_CALC_FOUND_ROWS MAX(log.id) FROM ".
						   "log LEFT OUTER JOIN Event ON log.type_id=Event.id AND log.type=0 ".
						   "WHERE log.id>=%d AND ".
						   "(log.type!=0 OR Event.moderationStatus!=7) ".
						   "GROUP BY IF(type IN (0,1),type+100*type_id,log.id) ORDER BY MAX(log.id) DESC ".
						   $pager->sql(),
						   $beginId);
		$mult*=5;
	}
	while($beginId>0 && count($logIds)<$pager->itemsPerPage);
	$pager->foundRows();

	//$end = microtime(true);
	//echo "Time:".($end-$start)."s\n";
    // Left join is needed to choose single l1 that has max id for group (using "GROUP BY type,type_id" is no longer allowed in default mysql config)
    $logs=db_arrays("SELECT l1.* FROM log l1 LEFT JOIN log l2 ON ".
                    "     (l1.type=l2.type AND l1.type_id=l2.type_id AND l1.id<l2.id) WHERE ".
                    "     l2.id IS NULL AND l1.id IN (".implode(',',$logIds).") ".
                    "ORDER BY l1.id DESC");

	$eids=[];
	$pids=[];
	foreach($logs as $log)
	{
		if($log['type']==0){$eids[]=$log['type_id'];}// type=0=Event
		else               {$pids[]=$log['type_id'];}
	}

	$events=[];
	if(count($eids))
	{
		$events=Event::fetchList('SELECT %no[body,extraData] FROM Event WHERE id IN ('.implode(',',$eids).')');
	}
	$posts=[];
	if(count($pids))
	{
		$posts=Post::fetchList('SELECT %no[body] FROM Post WHERE id IN ('.implode(',',$pids).')');
	}

	// display the table
	$users=[];
	$ct=0;
	foreach($logs as $log)
	{
		if($log['type']==0)// 0=Event
		{
			$event=val($events,$log['type_id'],null);
			$post=null;
			$both=$event;
			// ignore trashed events
			if($event!==null && $event->getModerationStatus()==='trash'){continue;}
			// ignored events auto deleted from trash
			if($event===null && $log['message']==='delete-trash'){continue;}
		}
		else
		{
			$event=null;
			$post=val($posts,$log['type_id'],null);
			$both=$post;
		}

		$uid=$log['user_id'];
		if(!$uid){$luser=null;}
		else
		{
			if(!isset($users[$uid])){$users[$uid]=User::fetch($uid,false);}
			$luser=$users[$uid];
		}
		$output.= '<tr class="'.((($ct++)%2) ? 'even' : 'odd').' '.ent($log['type']).'">';
		$eventLog='';
		if($event)
		{
			$eventLog=$event->logRender();
		}
		$output.='<td class="log"><span class="button">'.($event ? '+' : '').'</span>'.
			'<div class="contents">'.$eventLog.'</div></td>';
		$url='';
		$title=false;
		if($event){$url=$event->url();$title=$event->title;}
		if($post ){$url=$base_url.'/post/'.$post->id;$title=$post->title;}
		$output.='<td class="title">';
		if($title===false){$output.=t('[deleted]');}
		else{$output.='<a href="'.ent($url).'">'.(trim($title)==='' ?  '[...]' : ent($title)).'</a>';}
		$output.='</td>';
		$output.='<td class="date">'.demos_format_date('relative-short',$log['timestamp']).'</td>';
		$output.='<td class="username">';
		if($luser)
		{
			$output.='<a href="'.$base_url.'/user/'.$luser->id.'">'.ent($luser->login).'</a>';
		}
		$output.='</td>';
		if($ct>=$pager->itemsPerPage){break;}
	}
	$output.= '</table>';
	$output.=template_render('dlib/pager.tpl.php',['pager'=>$pager]);
	return $output;
}

function demosphere_panel_ptoken_nb_comments_today(&$info,$args)
{
	return db_result("SELECT COUNT(*) FROM Comment WHERE created > %d",Event::today());
}

function demosphere_panel_ptoken_carpools(&$info,$args)
{
	return db_result("SELECT COUNT(*) FROM Carpool,Event WHERE Carpool.eventId=Event.id AND Event.startTime > %d",Event::today());
}

function demosphere_panel_ptoken_nb_published_future_events(&$info,$args)
{
	return db_result("SELECT COUNT(*) as count FROM Event WHERE ".
					 "startTime>=%d AND ".
					 "status=1",time());
}

function demosphere_panel_ptoken_nb_published_events(&$info,$args)
{
	return db_result("SELECT COUNT(*) as count FROM Event WHERE status=1");
}

function demosphere_panel_ptoken_nb_needs_attention(&$info,$args)
{
	$level=val($args,'level',2);
	$recent=val($args,'recent',5000);
	return db_result("SELECT COUNT(*) FROM Event WHERE needsAttention>=%d AND ".
					 "startTime<%d",
					 Event::needsAttentionEnum('level-'.$level),
					 time()+$recent*3600*24);	
}

function demosphere_panel_ptoken_nb_events_mstatus(&$info,$args)
{
	$mstatus=val($args,'mstatus','waiting');
	$recent =val($args,'recent' ,5000);
	return db_result("SELECT COUNT(*) FROM Event WHERE moderationStatus=%d AND ".
					 ($mstatus!=="created" ?  "startTime>".Event::today()." AND " : '').
					 "startTime<%d",
					 Event::moderationStatusEnum($mstatus),
					 time()+$recent*3600*24);
}

function demosphere_panel_ptoken_nb_incomplete_events(&$info,$args)
{
	$recent=val($args,'recent',5000);
	return db_result("SELECT COUNT(*) FROM Event WHERE ".
					 "moderationStatus=%d AND ".
					 "startTime>%d AND startTime<%d",
					 Event::moderationStatusEnum('incomplete'),
					 Event::today(),
					 time()+$recent*3600*24);
}

function demosphere_panel_ptoken_awstats(&$info,$args)
{
	global $demosphere_config;
	$output='';
	$awstatsLast=variable_get('awstats-last');
	if($demosphere_config['awstats_name']===false || $awstatsLast===false){return '';}
	if($awstatsLast['ts']<time()-3600*4){return 'awstats not updated, problem?';}
	
	return '<span class="awstats">'.t('( awstats: visits: @v predicted: @p yesterday: @y )',
									  ['@v'=>$awstatsLast['visits'],
									   '@p'=>$awstatsLast['predicted'],
									   '@y'=>$awstatsLast['yesterday'],]
				   ).'</span>';
}

//! Display most viewed events for today.
//! This information is directly available in the daily_visits table 
//! @see demosphere_stats_daily_visits_update() 
function demosphere_panel_ptoken_most_viewed_today(&$info,$args)
{
	global $demosphere_config,$base_url;
	$output='';

	$dayTs=strtotime('3:33');
	$totalHits=db_result('SELECT SUM(hits) FROM daily_visits WHERE day=%d',$dayTs);

	$pager=new Pager(['defaultItemsPerPage'=>20,'prefix'=>'stats']);
	$eventHits=db_one_col('SELECT SQL_CALC_FOUND_ROWS event,hits FROM daily_visits WHERE day=%d ORDER BY hits DESC,event ASC '.$pager->sql(),$dayTs);
	$pager->foundRows();

	// retreive info for events in stat file
	$eventInfo=[];
	if(count($eventHits)>0)
	{
		$events=Event::fetchList('SELECT %no[body,extraData] FROM Event WHERE id IN ('.implode(',',array_keys($eventHits)).')');
	}

	// ******** Display extra stats information 
	// that was gathered during log file parsing in demosphere_stats_daily_visits_update() 
	$mobileCt  =variable_get('mobileCt'   ,'daily_visits',[]);
	$uniqueIps =variable_get('uniqueIps'  ,'daily_visits',[]);
	$todayTs=strtotime('3:33');
	$output.='<span id="panel-stats-summary">';
	$output.='<span>'.t("ev. views:").' '.$totalHits.'</span> ';
	$output.='<span>'.t("uniq. ips:").' '.val($uniqueIps,$todayTs,0).'</span> ';
	$mobileToday=val($mobileCt,$todayTs,['mobile'=>0,'tot'=>0]);
	$output.='<span>'.t("mobile:").' '.($mobileToday['tot']==0 ? '0' : round(100*$mobileToday['mobile']/$mobileToday['tot'],1)).'%</span> ';
	$output.='</span>';

	// ******* Display a table with stats (and titles)
	$output.='<table id="panelEventList">';
	$subTotal=0;
	$dhits=[];
	foreach($eventHits as $eid=>$hits)
	{
		$subTotal+=$hits;
		$event=val($events,$eid,null);

		$output.='<tr class="'.
			($event===null || $event->startTime<time()-3600*24*4 ? 'oldEvent' : '').'">';

		$output.="<td>".intval(100*$hits/$totalHits)."%</td>";

		$output.="<td>$hits</td>";

		$date=demos_format_date('panel-list',$event===null ? 0 : $event->startTime);
		$output.='<td>';
		$output.=implode('</td><td>',array_map('ent',explode('|',$date)));
		$output.='</td>';

		$title=$event===null ? t('[deleted]') : $event->htmlTitle;
		$output.='<td><a href="'.($event===null ? '' : $event->url()).'">'.
			filter_xss($title,['strong']).'</a></td>';

		$output.="</tr>\n";
	}
	$output.="</table>\n";
	$output.=template_render('dlib/pager.tpl.php',['pager'=>$pager]);

	return $output;
}

function demosphere_panel_ptoken_config_checks(&$info,$args)
{
	global $base_url;
	$output='';
	$ok=variable_get('demosphere_config_check',false);
	if(!$ok)
	{
		$output.='<span class="warning">'.
			'<a href="'.$base_url.'/config-check" class="warning">'.
			t('config problem !').'</a></span>';
	}
	return $output;
}

function demosphere_panel_ptoken_email_subscribers()
{
	global $base_url,$user,$demosphere_config;
	require_once 'demosphere-email-subscription.php';
	$ct=db_result("SELECT COUNT(*) FROM User WHERE data LIKE '%%demosphere_email_subscription%%active\";b:1%%'");
	$sent=variable_get('email_subscription_sent_count','',0);
	$bounces=variable_get('email_bounces_past_24h','',0);

	if($user->checkRoles('admin'))
	{
		$output='<span title="'.ent(t("email subscriptions: errors in past 24h : sent in past 24h / total subscribers")).'">'.
			($demosphere_config['hosted'] ? '<a href="'.$base_url.'/stats/email-bounces">'.intval($bounces).'</a> : ' : '').
			'<a href="'.$base_url.'/stats/users">'.intval($sent).' / '.intval($ct).'</a>'.
			'</span>';
	}
	else{$output=$ct;}

	return $output;
}

function demosphere_panel_ptoken_demosphere_config(&$info,$args)
{
	global $demosphere_config;
	$allowed=['event_url_name','place_url_name','places_url_name','debug'];
	$name=$args['name'];
	if(array_search($name,$allowed)===false)
	{
		return 'this config name is not allowed, please ask server admin to add it to demosphere-panel.php';
	}
	return ent($demosphere_config[$name]);
}

function demosphere_panel_ptoken_opinions()
{
	global $demosphere_config,$base_url;
	if(!$demosphere_config['enable_opinions']){return '';}
	$nbPastWeek=db_result("SELECT COUNT(*) FROM Opinion WHERE time > %d",time()-3600*24*7);
	$out='';
	$out.='<li><a href="'.ent($base_url).'/opinions">'.ent(t('opinions:')).'</a> '.$nbPastWeek.'</li>';
	return $out;
}

function demosphere_panel_ptoken_software_forum()
{
	global $demosphere_config,$base_url;
	if(!$demosphere_config['hosted']){return '';}
	$nbPastWeek=db_result("SELECT COUNT(*) FROM Opinion WHERE time > %d",time()-3600*24*7);
	$out='';
	$out.='<li class="forum">'.
		'<div class="forum-line1"><a href="https://software.demosphere.net/?site-id='.$demosphere_config['site_id'].'">'.t('forum').'</a>';
	$last=variable_get('last_post','software_forum');
	if($last!==false && $last['post_time']>time()-3600*24*7)
	{
		require_once 'demosphere-date-time.php';
		require_once 'dlib/filter-xss.php';
		$postUrl='https://software.demosphere.net/viewtopic.php?f='.intval($last['forum_id']).
			'&t='.intval($last['topic_id']).
			'&site-id='.$demosphere_config['site_id'];
		$out.=' : <a class="forum-username" href="'.ent($postUrl).'">'.
			//ent(demos_format_date('relative-short',$last['post_time'])).' : '.
			'<span>'.ent($last['username']).'</span>'.
			'</a>'.
			'</div>'.
			'<div class="forum-subject"><a href="'.ent($postUrl).'">'.
			filter_xss($last['post_subject'],[]).
			'</a>'.
			'</div>';
	}
	else{$out.='</div>';}
	$out.='</li>';
	return $out;
}
function demosphere_panel_ptoken_software_forum_update()
{
	require_once 'demosphere-misc.php';
	$demosauth=demosphere_remote_demosauth_cmd('fetch-recent-posts');
	$url='https://software.demosphere.net/?demosauth='.$demosauth;
	require_once 'dlib/download-tools.php';
	$json=dlib_curl_download($url);
	if($json===false){return;}
	$reply=json_decode($json,true);
	if(!is_array($reply) || !isset($reply['post_id'])){return;}
	variable_set('last_post','software_forum',$reply);
}

?>