<?php

//! A description of available demosphere-tokens.
//! @see demosphere_dtokens()
function demosphere_dtoken_list()
{
	global $custom_config;
	$list= 
		[
			  'video'=>
			  ['name'=>dt('Embed video'),'desc'=>dt("Insert a video inside the contents. Only Youtube, Dailymotion and Vimeo videos are supported for now (contact the developpers for others). This token takes one argument: the url to the video (this can be a simple link to the page that displays the video). Examples: demosphere_dtoken_video(https://www.youtube.com/watch?v=flz6shD3g2s) demosphere_dtoken_video(https://www.dailymotion.com/video/x10v63n_noam-chomsky-at-left-forum_news) demosphere_dtoken_video(https://vimeo.com/65840348)"),'insert'=>dt('demosphere_dtoken_video(link-to-your-video-here)')],
			  'copy_part'=>
			  ['name'=>dt('Copy part'),'desc'=>dt('The contents of a page can be divided into parts, using horizontal lines (hr). Sometimes you want to use the same part on several different pages. One page is considered the "reference". Other pages are copied from it. Example: If the reference part is on https://example.org/event/1234, put demosphere_dtoken_copy_part(1234) in that page, and in several pages. Each time the part on https://example.org/event/1234 is changed, all pages with demosphere_dtoken_copy_part(1234) will be updated.'),'insert'=>'demosphere_dtoken_copy_part(#!eid)'],
			  'demosphere_sites'       =>['name'=>dt('Demosphere sites'),'desc'=>'Displays a list of sites that use Demosphere.','cache'=>3600*24],
			  'login_logout'=>
			  ['name'=>dt('Login/logout link'),'desc'=>dt('A simple link to the login page if a user is not logged-in. A link to the logout page if the user is logged-in.'),],
			  'number_of_users'        =>['name'=>dt('Number of users'),'desc'=>'',],
			  'number_of_published_events'=>
			  ['name'=>dt('Number of published events'),'desc'=>'',],
			  'number_of_published_future_events'=>
			  ['name'=>dt('Number of published future events'),'desc'=>'',],
			  'years_since_first_event'=>['name'=>dt('Years since first event'),'desc'=>'Number of years your calendar has existed.',],
			  'new'                    =>['name'=>dt('new! icon'),'desc'=>'A small icon with an exclamation point. (frontpage only)',],
			  'quick_email_subscribe'  =>['name'=>dt('Quick email subscribe'),'desc'=>'',],
			  'rss_feed_url_builder'   =>['name'=>dt('Custom RSS link builder GUI'),'desc'=>'',],
			  'ical_feed_url_builder'  =>['name'=>dt('Custom iCal link builder GUI'),'desc'=>'',],
			  'feed_import_public_list'=>['name'=>dt('List of feed-import feeds'),'desc'=>'',],
			  'pagewatch_public_list'  =>['name'=>dt('List of page-watch pages'),'desc'=>'',],
			  'logout_usercal_url'     =>['name'=>dt('Logout link'),'desc'=>'',],
			  'email_icon'             =>['name'=>dt('Email icon'),'desc'=>'',],
			  'target_blank'           =>['name'=>dt('Target blank'),'desc'=>'links open in another window',],
			  'anchor_name'            =>['name'=>dt('Anchor name'),'desc'=>'',],
			  'anchor_link'            =>['name'=>dt('Anchor link'),'desc'=>'',],
			  ];
	if(isset($custom_config['dtoken_list'])){$list=array_merge($list,$custom_config['dtoken_list']);}
	return $list;
}

//! Returns a json object with a json description of all dtokens as well as an 
//! html description that is used for a popup dialog in the backend editor.
function demosphere_dtoken_list_json()
{
	$dtokens=demosphere_dtoken_list();
	$out='';
	$out.='<div id="dtoken-popup" style="display: none;">';
	$out.='<dl>';
	foreach($dtokens as $tag=>$token)
	{
		$out.='<dt data-token="'.ent($tag).'">'.ent(t($token['name'])).'</dt>';
		$out.='<dd>'.ent(t($token['desc'])).'</dd>';
	}
	$out.='</dl>';
	$out.='</div>';
	return ['list'=>$dtokens,
			'popup'=>$out];
}

//! Replaces predefined special words in events or other content by php-generated content.
//! This is named "Magic words" in some CMS's.
function demosphere_dtokens($text)
{
	if(strpos($text,'demosphere_tag_'   )===false && // old, deprecated name
	   strpos($text,'demosphere_dtoken_')===false){return $text;}
	// this is used by install profile to copy remote pages (make sure no security problems?)
	if(isset($_GET['disable-demosphere-dtoken'])) {return $text;} 

	$dtokens=demosphere_dtoken_list();

	// Do not accept dtokens inside an HTML tag (XSS)
	if(preg_match('@<[^>]*demosphere_(dtoken|tag)_('.implode('|',array_keys($dtokens)).')\(@s',$text))
	{
		fatal('dtoken inside HTML tag is not allowed');
	}

	return preg_replace_callback('@demosphere_(dtoken|tag)_('.implode('|',array_keys($dtokens)).')'.
								 '(\(([^)]*)\))?@',
								 'demosphere_dtokens_regexp_cb',$text);
}
function demosphere_dtokens_regexp_cb($matches)
{
	// FIXME: arg is not used, and a better format (like panel_tags) should be used
	$arg=val($matches,4,false);
	$dtoken=$matches[2];

	$fct='demosphere_dtoken_'.$dtoken;
	if(!function_exists($fct)){fatal('dtoken function does not exist');}

	// check for cached version if caching requested
	$useCache=false;
	$list=demosphere_dtoken_list();
	$info=$list[$dtoken];
	if(isset($info['cache']))
	{
		$useCache=$dtoken.'-'.md5($arg);
		$cached=variable_get($useCache,'dtoken');
		if($cached!==false && (time()-$cached['last'])<$info['cache'])
		{return $cached['value'].'.';}
	}

	// actually call the dtoken function
	$out=$fct($arg);

	if($useCache)
	{
		variable_set($useCache,'dtoken',['value'=>$out,'last'=>time()]);
	}
	return $out;
}
function demosphere_dtokens_named_args($rawText)
{
	$rawText=trim($rawText);
	if(strlen($rawText)===0){return [];}
	$re='([a-z]+)=([^,]+),';
	if(!preg_match('@^('.$re.')+$@',$rawText.',') ||
	   preg_match_all('@'.$re.'@',$rawText.',',$matches)===false)
	{
		dlib_message_add('Invalid arguments to dtoken: '.ent($rawText),'error');
		return false;
	}
	return array_combine($matches[1],$matches[2]);
}

/* DToken functions */

function demosphere_dtoken_number_of_users()
{
	return db_result("SELECT COUNT(*) FROM User");
}
function demosphere_dtoken_number_of_published_events()
{
	return db_result("SELECT COUNT(*) FROM Event WHERE status!=0");
}
function demosphere_dtoken_number_of_published_future_events()
{
	return db_result("SELECT COUNT(*) FROM Event WHERE ".
							  "status!=0 AND ".
							  "startTime>%d",time());
}
function demosphere_dtoken_feed_import_public_list()
{
	return Feed::publicList();
}
function demosphere_dtoken_pagewatch_public_list()
{
	global $currentPage;
	return Page::publicList();
}

function demosphere_dtoken_login_logout()
{
	global $base_url,$user;
	$isLoggedIn=$user->id!=0;
	return '<a class="dtoken-login-logout" href="'.
		$base_url.'/'.($isLoggedIn ? 'logout' : 'user').'">'.
		($isLoggedIn ? t('log out') :t('login')).'</a>';
}

function demosphere_dtoken_years_since_first_event()
{
	// id<3000 : hack, to speedup very slow query
	$first=db_result("SELECT MIN(startTime) FROM Event WHERE Event.status=1 AND Event.showOnFrontpage=1 AND id<3000");
	$first=floor((time()-$first)/(365*24*3600));
	return $first;
}

function demosphere_dtoken_rss_feed_url_builder()
{
	require_once 'demosphere-feeds.php';
	return demosphere_feeds_rss_url_builder('rss');
}
function demosphere_dtoken_ical_feed_url_builder()
{
	require_once 'demosphere-feeds.php';
	return demosphere_feeds_rss_url_builder('ical');
}

function demosphere_dtoken_quick_email_subscribe()
{
	require_once 'demosphere-email-subscription.php';
	require_once 'dlib/form.php';
	list($items,$opts)=demosphere_email_subscription_quick_form();
	// This page cant be cached!! (form token depends on session id)
	demosphere_page_cache_is_cacheable(null,null);
	return form_process($items,$opts);
}
//! The copy part dtoken does nothing on display (it just erases itself).
//! Everything is done on Event submission.
function demosphere_dtoken_copy_part()
{
	return '';
}

function demosphere_dtoken_new()
{
	return '<span class="new"></span>';
}
function demosphere_dtoken_email_icon()
{
	return '<span class="email"></span>';
}


function demosphere_dtoken_anchor_name($arg)
{
	if(!preg_match('@^[a-z][a-z0-9-]*$@i',$arg)){return 'Invalid name for anchor. Must be simple letters or numbers.';}
	return '<a class="dtoken-anchor" id="dtoken-anchor-'.$arg.'"></a>';
}

function demosphere_dtoken_anchor_link($arg)
{
	if(!preg_match('@^([a-z][a-z0-9-]*),(.*)$@i',$arg,$m)){return 'Invalid syntax. Use anchor name followed by a comma and then by the link text.';}
	return '<a class="dtoken-anchor-link" href="#dtoken-anchor-'.$m[1].'">'.$m[2].'</a>';
}


function demosphere_dtoken_video($url)
{
	// recognize video type from url:

	$out=false;
	// Youtube
	if(preg_match('@(youtube|youtu.be).*(v=|v/|embed/)([a-zA-Z0-9_-]{9,})@',$url,$matches))
	{
		$out='<iframe width="480" height="390" src="https://www.youtube.com/embed/'.ent($matches[3]).'" frameborder="0" allowfullscreen></iframe>';
	}
	else
	// Dailymotion
	if(preg_match('@dailymotion.*(embed/video/x|video/x|swf/x)([a-z0-9]{4,})@',$url,$matches))
	{
		$out='<iframe frameborder="0" width="560" height="245" src="https://www.dailymotion.com/embed/video/x'.$matches[2].'"></iframe>';
	}
	else
	// Vimeo
	if(preg_match('@vimeo.com/([0-9]{4,})@',$url,$matches))
	{
		$out='<iframe src="https://player.vimeo.com/video/'.$matches[1].'" width="576" height="325" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
	}
	if($out===false){return t('[failed to embed video: bad url]');}

	return $out;
}


function demosphere_dtoken_demosphere_sites($rawArgs)
{
	$args=demosphere_dtokens_named_args($rawArgs);
	if($args===false){return 'error';}
	require_once 'dlib/download-tools.php';
	$lang=val($args,'lang',dlib_get_locale_name());
	
	$sites=dlib_curl_download('https://tech.demosphere.net/slm/sites/json');
	$sites=json_decode($sites,true);
	$out='';
	require_once 'dlib/filter-xss.php';
	foreach($sites as $name=>$site)
	{
		if($lang!=='all' && $site['language']!==$lang){continue;}
		$out.='<a href="'.filter_xss_check_url($site['url']).'">'.ent($site['name']).'</a>, ';
	}
	$out=preg_replace('@, $@','',$out);
	return $out;
}

function demosphere_dtoken_target_blank($html)
{
	$html=preg_replace('@<a\b@','<a target="_blank" ',$html);
	require_once 'dlib/filter-xss.php';
	$html=filter_xss_admin($html);
	return $html;
}

// ***************************************************
// *************  event copy part 
// ***************************************************

/**
 * When an event is saved, update the demosphere_dtoken_copy_part's.
 *
 * Copy part tokens allow the editor to maintain similar parts of text
 * across different events. Each event, including the copy-reference event,
 * contains a token like this "demosphere_dtoken_copy_part(1234)". Where 1234 is the copy-reference event id.
 * There are two ways to define a part:
 *  - As the text from the previous to the next <hr>
 *  - As the text between a start and end tag:
 *    demosphere_dtoken_copy_part(1234,start)this is the text demosphere_dtoken_copy_part(1234,end)
 * This function is called from event->save()
 */
function demosphere_copy_part_event_update($event)
{
	global $base_url;

	// Quick check to see if there are any copy tokens and avoid costly processing.
	if(strpos($event->body,'demosphere_dtoken_copy_part')===false){return;}

	// Find all events ids in copy tokens
	if(!preg_match_all('@demosphere_dtoken_copy_part\(([0-9]+)[,\)]@',$event->body,$matches))
	{
		dlib_message_add(t('invalid demosphere_dtoken_copy_part'),'error');
		return;
	}
	$copyEids=array_unique(array_map('intval',$matches[1]));
	//echo '$copyEids';var_dump($copyEids);

	// Process tokens for each eid in this Event
	foreach($copyEids as $copyEid)
	{
		// Two cases: 1) this token is a self-reference 2) this token references another Event
		if($copyEid==$event->id)
		{
			$copyRef=$event;
			// Self-reference : We are saving the copy reference!
			// Update all events containing this token.
			$refPart=demosphere_copy_part_get_part($copyRef->body,$copyRef->id);
			if($refPart===false){fatal('failed extracting dtoken copy part');}

			// we get the list of events using the index:
			$index=variable_get('demosphere_copy_part_index','',[]);
			$destEids=val($index,$copyRef->id,[]);
			foreach($destEids as $destEid)
			{
				if($destEid==$copyRef->id){continue;}// this should not happen
				$destEvent=Event::fetch($destEid,false);
				// stale reference: dest no longer exists
				if($destEvent===null){continue;}
				demosphere_copy_part_copy($copyRef,$destEvent,$refPart);
			}
		}
		else
		{
			// We are saving an event that contains a reference to another Event.
			// Update this part (just in case it was changed).
			$copyRef=Event::fetch($copyEid,false);
			if($copyRef===null)
			{
				dlib_message_add(t('demosphere_dtoken_copy_part could not find reference:').
								   $copyEid,'error');
				continue;
			}
			$refPart=demosphere_copy_part_get_part($copyRef->body,$copyRef->id);
			if($refPart===false)
			{
				dlib_message_add(t('demosphere_dtoken_copy_part not found in reference:').
								   $copyEid,'error');
				continue;
			}
			$ok=demosphere_copy_part_copy($copyRef,$event,$refPart);
			if(!$ok){dlib_message_add('demosphere_dtoken_copy_part: failed extracting dtoken',
										'error');}
		}
		//var_dump($matches);fatal('oo');
	}
}

//! Copies a part from $copyRef to the matching part in $destEvent.
//! The token is identified by $copyRef->id.
//! @param $refPart a description of the reference part obtained by demosphere_copy_part_get_part().
//! @return false on failure, true otherwise
function demosphere_copy_part_copy(&$copyRef,&$destEvent,$refPart)
{
	global $base_url;
	$destPart=demosphere_copy_part_get_part($destEvent->body,$copyRef->id);
	// bad destination
	if($destPart===false){return false;}

	$content=$refPart['content'];

	// Special cases:
	// if we are copying form an "hr" part to a "start/end" block, remove the token
	if($refPart['tokenEnd']===false && $destPart['tokenEnd']!==false)
	{
		$content=str_replace('demosphere_dtoken_copy_part('.$copyRef->id.')','',$content);
	}
	// if we are copying form an a "start/end" block to a "hr" part, add the token
	if($refPart['tokenEnd']!==false && $destPart['tokenEnd']===false)
	{
		$content='demosphere_dtoken_copy_part('.$copyRef->id.')'.$content;
	}

	$before=$destEvent->body;
	$destEvent->body=
		substr($destEvent->body,0,$destPart['start']).
		$content.
		substr($destEvent->body,$destPart['end']);

	// only save and display message if there actually is a change.
	if($destEvent->body!==$before)
	{
		dlib_message_add(t("dtoken_copy_part: copying part from !link1 to !link2",
						   ['!link1'=>'<a href="'.$copyRef  ->url().'">'.$copyRef  ->id.'</a>',
							'!link2'=>'<a href="'.$destEvent->url().'">'.$destEvent->id.'</a>']
							 ));
		$destEvent->save(false);
	}
	return true;
}

//! Returns a description (start,end,content...) of a part matched by the token for eid.
//! @param $html: usually $event->body
//! @param $eid: the id of the event containing the token we are searching (demosphere_dtoken_copy_part(eid...))
function demosphere_copy_part_get_part($html,$eid)
{
	if(!preg_match_all('@demosphere_dtoken_copy_part\('.intval($eid).'(,(start|end))?\)@',
					   $html,$tokenMatches,PREG_OFFSET_CAPTURE))
	{
		return false;
	}
	if(count($tokenMatches[0])>2){return false;}
	$isWholePart=count($tokenMatches[0])===1;

	if(!$isWholePart)
	{
		if(!is_array($tokenMatches[2][0]) ||  $tokenMatches[2][0][0]!=='start' ||
		   !is_array($tokenMatches[2][1]) ||  $tokenMatches[2][1][0]!=='end'     ){return false;}

		$tokenStart      =$tokenMatches[0][0][1];
		$tokenStartLength=strlen($tokenMatches[0][0][0]);
		$start=$tokenStart+$tokenStartLength;
		$end  =$tokenMatches[0][1][1];
		$tokenEnd=$end;
		$tokenEndLength=strlen($tokenMatches[0][1][0]);
	}
	else
	{
		$tokenStart      =$tokenMatches[0][0][1];
		$tokenStartLength=strlen($tokenMatches[0][0][0]);
		$tokenEnd=false;
		$tokenEndLength=false;

		preg_match_all('@<hr(?![^>]*subsection)[^>]*>@',$html,$partSeps,
					   PREG_PATTERN_ORDER|PREG_OFFSET_CAPTURE);
		//echo "partSeps";var_dump($partSeps);
		$partNb=count($partSeps[0]);
		foreach($partSeps[0] as $n=>$partSep)
		{
			if($partSep[1]>$tokenStart){$partNb=$n;break;}
		}

		$start=$partNb==0 ? 0:
			$partSeps[0][$partNb-1][1] + strlen($partSeps[0][$partNb-1][0]);
		$end  =$partNb>=count($partSeps[0]) ? strlen($html) :
			$partSeps[0][$partNb][1];
	}
	$content=substr($html,$start,$end-$start);
	return ['start'=>$start,
			'end'=>$end,
			'tokenStart'=>$tokenStart,
			'tokenStartLength'=>$tokenStartLength,
			'tokenEnd'=>$tokenEnd,
			'tokenEndLength'=>$tokenEndLength,
			'content'=>$content,
		   ];
}


/**
 * Updates the index of copy parts.
 *
 * This index is necessary to know which events (X) reference another event (R).
 * If we update that event (R), we need to update all events (X).
 * Without this index we would need to search all existing event bodies for copy tokens...
 * Note: we don't care about stale references in the index (it's just a tiny performance issue).
 * and we want to keep things simple.
 * Called from Event::(delete|save).
 */
function demosphere_copy_part_index_update($event)
{
	// Quick check to see if there are any copy tokens and avoid costly processing.
	if(strpos($event->body,'demosphere_dtoken_copy_part')===false){return;}

	if(!preg_match_all('@demosphere_dtoken_copy_part\(([0-9]+)[,\)]@',$event->body,$matches)){return;}

	$index=variable_get('demosphere_copy_part_index','',[]);

	foreach(array_map('intval',$matches[1]) as $referencedEid)
	{
		if($referencedEid==$event->id){continue;}
		$index[$referencedEid][]=$event->id;
		$index[$referencedEid]=array_unique($index[$referencedEid]);
	}
	variable_set('demosphere_copy_part_index','',$index);
}
function demosphere_copy_part_index_delete($eid)
{
	$index=variable_get('demosphere_copy_part_index','',[]);
	if(isset($index[$eid]))
	{
		unset($index[$eid]);
		variable_set('demosphere_copy_part_index','',$index);
	}
}

?>