<?php

//! returns html for a given $place. 
//! This is used both in main event view and single place page view
function demosphere_place_render($place,$showPlaceLinks=true,$mapUrl=false,&$placeHeightEst=false)
{
	global $demosphere_config,$base_url,$currentPage,$user;
	$isEventAdmin=$user->checkRoles('admin','moderator');

	$currentPage->addCssTpl('demosphere/css/demosphere-place.tpl.css');

	$hasMapImage=$place->zoom!=0;
	if($mapUrl===false)
	{
		require_once 'demosphere-event-map.php';
		$mapUrl=demosphere_get_map_url($place);
	}
	$placeUrl=$base_url.'/'.$demosphere_config['place_url_name'].'/'.intval($place->referenceId);
	$referencePlace=$place->getReference();
	$placeHeightEst+=24+4;// h3 height+ 4px padding
	// filter: replace newlines with <br>
	$addressText=$place->htmlAddress();
	$placeHeightEst+=(1+substr_count($addressText,'<br />'))*19;
	// add extra wraped lines to $placeHeightEst
	$alines=explode('<br />',$addressText);
	foreach($alines as $n=>$aline)
	{
		$aline=dlib_cleanup_plain_text(trim($aline));
		$totWidth=340;
		$widthEst=mb_strlen($aline)*7.9;
		//require_once 'dlib/html-tools.php';
		//$widthEst=dlib_html_estimate_display_width($aline,36.3);
		$placeHeightEst+=19*floor($widthEst/$totWidth);
		//echo mb_strlen($aline)." : ".($widthEst)." : ".floor($widthEst/$totWidth).' : <span id="t'.$n.'">'.ent($aline)."</span><br/>";
	}

	$mapImage=false;
	if($hasMapImage)
	{
		require_once 'demosphere-event-map.php';
		$mapImage=demosphere_event_get_map_image($place->latitude,
										   $place->longitude,
										   $place->zoom);
		$placeHeightEst+=$mapImage['height'];
	}
	else
	{
		$placeHeightEst+=12;
		if($isEventAdmin){$placeHeightEst+=20;}
	}
	$placeHeightEst=max(45,$placeHeightEst);

	// place-links
	$nbEventsAtPlace=db_result('SELECT COUNT(*) FROM Event,Place WHERE placeId=Place.id AND Event.status=1 AND Place.referenceId=%d',$place->referenceId);

	$placeHeightEst+=10;// place links
	$placeHeightEst+=4+1;// place bottom padding 3 + border

	return template_render('templates/demosphere-place.tpl.php',
						   [compact('isEventAdmin','hasMapImage','place','placeUrl','mapUrl','mapImage','referencePlace','nbEventsAtPlace','showPlaceLinks'),
						   ]);
}


function demosphere_place_view($placeId)
{
	global $base_url,$demosphere_config,$user,$currentPage;

    // Robots using tor have been crawling this page a lot. Strange...
    // Robot script is set up (through tor) on 14/Sep/2015:16:39:51 (referer is old event: /rv/12650 ??)
    // Robot script is manually adjusted (through tor) on 14/Sep/2015:17:12:18
    require_once 'dlib/mail-and-mime.php';
    $ip=$_SERVER['REMOTE_ADDR'];
    dlib_throttle_add('robot-crawling-place-view-fast::'.$ip);
    dlib_throttle_add('robot-crawling-place-view-slow::'.$ip);
	if(dlib_throttle_count('robot-crawling-place-view-fast::'.$ip,40 )>15 ||
       dlib_throttle_count('robot-crawling-place-view-slow::'.$ip,180)>30)
    {
		dlib_abort("Too many requests",false,429);
    }

    $isEventAdmin=$user->checkRoles('admin','moderator');
	$place=Place::fetch($placeId,false);
	if($place===null){dlib_not_found_404();}
	$isPublic=db_result("SELECT COUNT(*) FROM Event WHERE placeId=%d AND Event.status=1",$place->id)>0;
	if(!$isEventAdmin && !$isPublic){dlib_permission_denied_403();}


	demosphere_place_tabs($place,'view');

	$currentPage->title=t('Place : !placetitle',['!placetitle'=>$place->nameOrTitle()]);
	$currentPage->bodyClasses[]="notFullWidth";
	$currentPage->bodyClasses[]='unPaddedContent';
	$currentPage->bodyClasses[]="nonStdContent"; 
	$currentPage->addCssTpl('demosphere/css/demosphere-place-view.tpl.css');
	$currentPage->addCssTpl('demosphere/css/demosphere-htmlview.tpl.css');
	if($isEventAdmin){$currentPage->addJs('lib/jquery.js');}

	$placeHeightEst=$place->hasDescription ? -30 : 0;
	$placeHtml=demosphere_place_render($place,false,false,$placeHeightEst);

	if($place->hasDescription)
	{
		$currentPage->robots=[];
	}
	
	// non admins are redirected to reference place, 
	// unless this non ref place has a description (unlikely)
	if(!$user->checkRoles('admin','moderator') && $place->id!=$place->referenceId && !$place->hasDescription)
	{
		if($place->id!=$place->referenceId && !$place->hasDescription){dlib_redirect($place->getReference()->url());}
	}

	$description=false;
	if($place->description!='')
	{
		$description=$place->description;
		require_once 'demosphere-dtoken.php';
		$description=demosphere_dtokens($description);
		require_once 'demosphere-htmlview.php';
		$description=demosphere_htmlview_body($description,$placeHeightEst);
	}

	$isReference=$place->id===$place->referenceId;

	$futureEvents=Event::fetchList('SELECT %no[log,body,extraData] '.
								   'FROM Event,Place WHERE Event.placeId=Place.id AND '.
								   ($isEventAdmin ? 'moderationStatus!=7' : 'status=1').' AND '.
								   'showOnFrontpage=1 AND '.
								   'startTime>%d AND '.
								   ($isReference ? 'Place.referenceId=%d ' : 'Place.id=%d ').
								   'ORDER BY startTime ASC ',
								   time(),$place->id);

	$pastEvents  =Event::fetchList('SELECT %no[log,body,extraData] '.
								   'FROM Event,Place WHERE Event.placeId=Place.id AND '.
								   ($isEventAdmin ? 'moderationStatus!=7' : 'status=1').' AND '.
								   'showOnFrontpage=1 AND '.
								   'startTime<%d AND '.
								   ($isReference ? 'Place.referenceId=%d ' : 'Place.id=%d ').
								   'ORDER BY startTime DESC ',
								   time(),$place->id);
	$events=array_merge($futureEvents,[false],$pastEvents);

	$placeRdf=demosphere_place_rdf($place);

	require_once 'demosphere-date-time.php';
	require_once 'dlib/filter-xss.php';
	require_once 'dlib/template.php';
	return template_render('templates/demosphere-place-page.tpl.php',
						   [compact('place','base_url','events','pastEvents','placeHtml','futureEvents','description','isEventAdmin','placeRdf'),
							array_intersect_key($demosphere_config,array_flip(['place_url_name']))]);
}

//! Returns an array ready for LD+JSON conversion describing the place. 
//! Used for Google structured data display.
function demosphere_place_rdf($place,$addContext=true)
{
	$placeRdf=
		[
			'@type'=>'Place', 
			'name'=>$place->nameOrTitle(),
			'url' =>$place->url(),
			'address'=>
			[
				'@type'=>'PostalAddress',
				'addressLocality'=>$place->getCityName(),
				'streetAddress'  =>$place->address, // FIXME html or text ? => clearly should be text
			],
			// FIXME: description
			// FIXME: image
		];
	if($addContext){$placeRdf['@context']="http://www.schema.org";}

	if($place->zoom!==0)
	{
		$placeRdf['geo']=
			[
				'@type'=>'GeoCoordinates',
				'latitude' =>$place->latitude,
				'longitude'=>$place->longitude,
			];
	}
	return $placeRdf;
}


function demosphere_place_variants($refId)
{
	global $currentPage,$demosphere_config;
	$refPlace=Place::fetch($refId,false);
	if($refPlace===null){dlib_not_found_404();}

	$currentPage->title=t('Variants:').' '.$refPlace->nameOrTitle();
	$currentPage->addCssTpl('demosphere/css/demosphere-place-admin.tpl.css');

	demosphere_place_tabs($refPlace,'variants');

	if($refPlace->id!=$refPlace->referenceId){dlib_redirect($refPlace->useReference()->url().'/variants');}

	if(isset($_POST['submit']))
	{
	 	dlib_check_form_token('demosphere_place_variants');
		$destId=intval($_POST['destination']);
		$dest=Place::fetch($destId);
		if($dest->referenceId!==$refPlace->id){dlib_bad_request_400('Invalid destination.');}
	 	foreach($_POST as $k => $v)
	 	{
	 		if(substr($k,0,7)!='delete-'){continue;}
	 		$pid=intval(substr($k,7));
			if($pid===$destId){continue;}
			$place=Place::fetch($pid);
			if($place->referenceId!==$refPlace->id){dlib_bad_request_400('Place is not variant.');}
	 		db_query('UPDATE Event SET placeId=%d WHERE placeId=%d',$destId,$pid);
			$place->delete();
	 		dlib_message_add(ent(t('Place @nb was deleted.',['@nb'=>$pid])));
	 	}
		dlib_redirect($refPlace->url().'/variants');
	}

	$nbEvents=db_one_col('SELECT '.
						 'Place.id,'.
						 '(SELECT COUNT(*) FROM Event WHERE Event.placeId=Place.id) AS ct '.
						 'FROM Place WHERE '.
						 'Place.referenceId=%d '.
						 'ORDER BY id=%d DESC, ct DESC, Place.id ASC',$refPlace->id,$refPlace->id);
	$places=Place::fetchListFromIds(array_keys($nbEvents));
	require_once 'dlib/template.php';
	return template_render('templates/demosphere-place-variants.tpl.php',
						   [compact('refPlace','nbEvents','places')]);
}

//! When the same place/address has been entered several times, we might want to merge 
function demosphere_place_merge($mergeToId)
{
	global $base_url,$demosphere_config,$currentPage;
	require_once 'demosphere-place-search.php';

	$currentPage->bodyClasses[]="place-merge";
	$currentPage->addCssTpl('demosphere/css/demosphere-admin.tpl.css');
	$currentPage->addCssTpl('demosphere/css/demosphere-place-admin.tpl.css');

	$mergeTo=Place::fetch($mergeToId,false);
	if($mergeTo===null){dlib_not_found_404();}
	// we only merge to reference places.

	$currentPage->title=t('Merge: ').$mergeTo->nameOrTitle();

	demosphere_place_tabs($mergeTo,'merge');

	if($mergeTo->id!=$mergeTo->referenceId){dlib_redirect($mergeTo->useReference()->url().'/merge');}
	
	if(isset($_POST['submit']))
	{
		dlib_check_form_token('demosphere_place_merge');
		foreach($_POST as $k => $v)
		{
			if(substr($k,0,3)!='cb-'){continue;}
			$pid=intval(substr($k,3));
			$place=Place::fetch($pid);
			// update all places that have references to $place
			// Note that this includes current place (pid)
			db_query('UPDATE Place SET referenceId=%d WHERE referenceId=%d',$mergeTo->id,$place->referenceId);
			$place->referenceId=$mergeTo->id;// needed for index update
			demosphere_place_search_index_update($place);
			dlib_message_add('Merged place <a href="'.ent($place->url()).'">'.ent($pid).'</a>');
		}
		demosphere_place_search_index_update($mergeTo);
		dlib_redirect($mergeTo->url().'/merge');
	}

	// Find places with similar text
	$text=$mergeTo->getCityName().' '.$mergeTo->address;
	$similarPlaces=demosphere_place_search_long_search($text,50);

	// Find places that are geographically nearby
	$nearby=db_one_col(
		'SELECT p1.id,'.demosphere_place_sphere_distance_sql().' AS distance '.
		'FROM Place AS p1,Place AS p2 '.
		'WHERE p2.id=%d AND p1.referenceId!=p2.referenceId '.
		'AND p1.referenceId=p1.id '.
		'AND p1.zoom!=0 '.
		'AND p2.zoom!=0 '.
		'HAVING distance<50 ORDER BY distance ASC',$mergeTo->id);

	// Build info for display
	$pids=array_unique(array_merge([$mergeTo->id],
								   array_keys($nearby),
								   array_keys($similarPlaces)));

	$nbEvents=db_one_col('SELECT Place.referenceId,COUNT(*) FROM Event,Place WHERE '.
						 'Place.id=Event.placeId AND '.
						 'Place.referenceId IN ('.implode(',',$pids).') '.
						 'GROUP BY Place.referenceId');

	$places=Place::fetchListFromIds($pids);

	require_once 'dlib/template.php';
	return template_render('templates/demosphere-place-merge.tpl.php',
						   [compact('mergeTo','places','nbEvents','nearby')]);
}

//! Return HTML display of differences between two places (city and address only).
function demosphere_place_diff($place1,$place2)
{
	$res='';
	$words1=preg_split("@[ ]@",preg_replace("@([^\p{L}\p{Nd} ])@iu",' |$1 ',$place1->getCityName()."\n\n".$place1->address));
	$words2=preg_split("@[ ]@",preg_replace("@([^\p{L}\p{Nd} ])@iu",' |$1 ',$place2->getCityName()."\n\n".$place2->address));
	$simple1=[];
	foreach($words1 as $word)
	{
		$simple1[dlib_simplify_text($word)]=true;
	}		
	$words1=array_flip($words1);
	foreach($words2 as $sn=>$word)
	{
		$w=str_replace('|','',$word);
		if($w==''){$w=' ';}
		if($w=="\n"){$w='#';}
		$class='';
		$simple=dlib_simplify_text($word);
		if(!isset($words1[$word   ])){$class.='smalldiff ';}
		if(!isset($simple1[$simple])){$class.='bigdiff ';}
		$res.='<span class="'.ent($class).'">'.ent($w).'</span> ';

	}
	return $res;
}

//! Display a list of all places used in demosphere.
function demosphere_place_json($placeId)
{
	global $user;
	$place=Place::fetch($placeId,false);
	if($place===null){dlib_not_found_404();}

	// check permissions for non-mods 
	if(!$user->checkRoles('admin','moderator'))
	{
		$nbPublic=db_result('SELECT COUNT(*) FROM Event WHERE placeId=%d AND status=1 AND showOnFrontpage=1',$place->id);
		if($nbPublic==0){dlib_permission_denied_403();}
	}

	// throttle to avoid crawling 
	require_once 'dlib/mail-and-mime.php';
	$ip=$_SERVER['REMOTE_ADDR'];
	dlib_throttle_add('demosphere_place_json::'.$ip);
	if(dlib_throttle_count('demosphere_place_json::'.$ip,50)>15*60){dlib_abort("Too many requests",false,429);}

	$placeFields=['id','referenceId','city','address','latitude','longitude','zoom',];
	$res=array_intersect_key((array)$place,array_flip($placeFields));
	$res['city']=$place->getCityName();
	return $res;
}

//! Display a list of all places used in demosphere.
function demosphere_places()
{
	global $demosphere_config,$base_url,$user,$currentPage;
	require_once 'demosphere-event-list.php';

	$options['hasDescription']=(bool)val($_GET,'hasDescription');
	$options['selectFpRegion']=val($_GET,'selectFpRegion');
	$options['cityId']=val($_GET,'cityId');


	$currentPage->title=t('List of places');

	// select with sort order
	// FIXME: Are all places with description really public?
	$sql='SELECT Place.referenceId,';
	$sql.='(SELECT COUNT(*) FROM Event,Place as p2 WHERE Event.placeId=p2.id AND Place.referenceId=p2.referenceId AND Event.status=1) AS ct ';
	$sql.='FROM Place WHERE ';
	if($options['hasDescription']){$sql.=' Place.hasDescription!=0 AND ';}
	else
	if(!$user->checkRoles('admin','moderator'))
	{
		$sql.='EXISTS (SELECT * FROM Event WHERE Event.placeId=Place.id AND Event.status=1) AND ';
	}
	if($options['cityId'])
	{
		$sql.=' Place.cityId!='.intval($options['cityId']).' AND ';
	}
	$sql=preg_replace('@(AND|WHERE) $@','',$sql);
	$sql.=' GROUP BY Place.referenceId ORDER BY ct DESC';
	$order=db_one_col($sql);

	// apply non sql options/filters
	$ct=0;
	$okPids=[];
	foreach($order as $pid=>$ct)
	{
		// *** selectFpRegion rule
		if(val($options,'selectFpRegion')!==false)
		{
			$place=Place::fetch($pid);
			$region=$demosphere_config['frontpage_regions'][$options['selectFpRegion']];
			if(isset($region['lat']) && $region['lat']!==false)
			{
				$d=$place->distance($region['lat'],$region['lng']);
				if($d==false || $d>$region['distance']){continue;}
			}
			else
			if(isset($region['cityId']) && $region['cityId']!==false)
			{
				if(intval($region['cityId'])!==intval($place->cityId)){continue;}
			}
			else{fatal("config problem: bad selectFpRegion, please contact admin");}
		}
		$okPids[]=$pid;
	}

	// add city(region) names for links
	$regions=$demosphere_config['frontpage_regions'];
	if($regions===false){$regions=[];}
	foreach($regions as $urlName=>&$region)
	{
		if(isset($region['cityId']) && $region['cityId']!==false)// if empty city name : use city name
		{
			$city=City::fetch($region['cityId'],false);
			if($city===null){dlib_message_add('Invalid city in frontpage regions','error');continue;}
			if(strlen(val($region,'name'))==0){$region['name']=$city->name;}
		}
	}
	unset($region);

	require_once 'dcomponent/dcomponent-common.php';	
	$pager=new Pager(['itemName'=>t('places'),'defaultItemsPerPage'=>60,'nbItems'=>count($okPids)]);
	$places=[];
	$okPids=array_slice($okPids,$pager->firstItem,$pager->itemsPerPage);
	$places=Place::fetchListFromIds($okPids);


	$currentPage->bodyClasses[]="places";
	$currentPage->addCssTpl('demosphere/css/demosphere-places.tpl.css');
	//var_dump($places);
	require_once 'dlib/template.php';
	return template_render('templates/demosphere-places.tpl.php',
						   [compact('places','base_url','order','pager','options','regions'),
							'siteName'=>$demosphere_config['site_name'],
							array_intersect_key($demosphere_config,array_flip(['place_url_name','places_url_name','frontpage_regions']))]);
}

//! Displays a list of refrences places that are close to each other. 
//! These might be candidates for merging.
function demosphere_place_by_proximity()
{
	global $demosphere_config,$currentPage;
	$pager=new Pager(['defaultItemsPerPage'=>100]);

	$currentPage->addCssTpl('demosphere/css/demosphere-place-admin.tpl.css');

	// Compute latitude and longitude changes for 100m in each direction. This is needed for bounding box speedup.
	$dlat=.1;
	$dlng=.1;
	$ddlat=demosphere_place_sphere_distance($demosphere_config['map_center_latitude']+$dlat,
											$demosphere_config['map_center_longitude'],
											$demosphere_config['map_center_latitude'],
											$demosphere_config['map_center_longitude']);
	$ddlng=demosphere_place_sphere_distance($demosphere_config['map_center_latitude'],
											$demosphere_config['map_center_longitude']+$dlng,
											$demosphere_config['map_center_latitude'],
											$demosphere_config['map_center_longitude']);
	$dlat100m=100*$dlat/$ddlat;
	$dlng100m=100*$dlng/$ddlng;

	$proximities=db_arrays("SELECT SQL_CALC_FOUND_ROWS p1.id AS id1,p2.id AS id2,".
						   demosphere_place_sphere_distance_sql()." AS distance ".
						   "FROM Place as p1,Place as p2 ".
						   "WHERE p1.id=p1.referenceId AND p2.id=p2.referenceId AND p1.id>p2.id AND p1.zoom!=0 AND p2.zoom!=0 ".
						   // 3x speedup using bounding box 
 						   "AND p1.latitude<p2.latitude+".  $dlat100m." ".
 						   "AND p1.latitude>p2.latitude-".  $dlat100m." ".
 						   "AND p1.longitude<p2.longitude+".$dlng100m." ".
 						   "AND p1.longitude>p2.longitude-".$dlng100m." ".
						   "AND EXISTS (SELECT * FROM Event AS e1 WHERE e1.placeId=p1.id AND e1.status=1) ".
						   "AND EXISTS (SELECT * FROM Event AS e2 WHERE e2.placeId=p2.id AND e2.status=1) ".
 						   "ORDER BY distance ASC, p1.id ASC, p2.id ASC ".
						   $pager->sql()
						  );
	$pager->foundRows();

	$out='';
	$out.='<h1 class="page-title">'.t('Places by proximity').'</h1>';
	$out.='<p>'.t('A list of places sorted by distance in meters. Places that are close to each other might need to be merged.').'</p>';
	$out.=template_render('dlib/pager.tpl.php',['pager'=>$pager]);
	$out.='<table id="proximity">';
	foreach($proximities as $proximity)
	{
		$p1=Place::fetch($proximity['id1']);
		$p2=Place::fetch($proximity['id2']);
		$out.='<tr>';
		$out.='<td>'.round($proximity['distance']).'m</td>';
		$out.='<td><a href="'.ent($p1->url()).'">'.ent($p1->nameOrTitle()).'</a></td>';
		$out.='<td><a href="'.ent($p2->url()).'">'.ent($p2->nameOrTitle()).'</a></td>';
		$out.='</tr>';
	}
	$out.='</table>';
	return $out;
}

//! Returns a distance in meters
function demosphere_place_sphere_distance($lat1,$lng1,$lat2,$lng2)
{
    return 6371000*acos(cos(deg2rad($lat1))*cos(deg2rad($lat2))*cos(deg2rad($lng2)-deg2rad($lng1))+
						sin(deg2rad($lat1))*sin(deg2rad($lat2)));
}

function demosphere_place_sphere_distance_place($place1,$place2)
{
	return demosphere_place_sphere_distance($place1->latitude,$place1->longitude,
											$place2->latitude,$place2->longitude);
}

//! Returns sql statement for distance between two points
function demosphere_place_sphere_distance_sql($lat1='p1.latitude',$lng1='p1.longitude',$lat2='p2.latitude',$lng2='p2.longitude')
{
	// FIXME: use ST_Distance_Sphere after switch to Mariadb >= 10.2 (Debian 10)
	$distSql="IF(".$lng1."=".$lng2." AND ".$lat1."=".$lat2.", 0,GREATEST(.0001,".
		"6371000 * acos( cos( radians(".$lat1.") )  * cos( radians( ".$lat2." ) )  * cos( radians( ".$lng2." ) - radians(".$lng1.") )  + ".
		"                sin( radians(".$lat1.") )  * sin( radians( ".$lat2." ) ) ) ))";
	//$distSql="ST_Distance_Sphere(POINT(".$lng1.",".$lat1."),POINT(".$lng2.",".$lat2."))";
	return $distSql;
}

//! Called once a day (in the middle of the night)
function demosphere_place_cron()
{
	require_once 'demosphere-place-search.php';
	Place::cleanupUnusedPlaces();
	Place::mergeDuplicates();
	City::cleanupUnusedCities();
	demosphere_place_search_index_rebuild();
	require_once 'demosphere-event-map.php';
	demosphere_event_map_refresh_cron();
}

function demosphere_place_tabs($place,$active)
{
	global $user,$base_url,$currentPage,$demosphere_config;

	if(!$user->checkRoles('admin','moderator') && 
	   !($user->id!=0 && $user->id==$place->inChargeId)){return;}

	$tabs=[];
	$tabs['view' ]=['text'=>t('View')      ,'href'=>$place->url()        ];
	$tabs['edit' ]=['text'=>t('Edit place'),'href'=>$place->url().'/edit'];
	if($user->checkRoles('admin','moderator'))
	{
		$nbVariants=db_result('SELECT COUNT(*) FROM Place WHERE referenceId=%d',$place->referenceId);
		if($nbVariants>1)
		{
			$tabs['variants']=['text'=>t('Variants').' ('.$nbVariants.')','href'=>$place->url().'/variants'];
		}
		$tabs['merge']=['text'=>t('Merge'),'href'=>$place->url().'/merge'];
	}
	if(isset($tabs[$active])){$tabs[$active]['active']=true;}
	$currentPage->tabs=$tabs;
}

//! Setup for class information displayed/edited by dbobject-ui
function demosphere_place_class_info(&$classInfo)
{
	global $base_url;
	$classInfo['tabs']=function(&$tabs,$place)
		{
			if($place)
			{
				$tabs['view'      ]['text']=t('Admin: view');
				$tabs['edit'      ]['text']=t('Admin: edit');
				$tabs['user-view']=['href'=>$place->url()        ,'text'=>t('View')];
				$tabs['user-edit']=['href'=>$place->url().'/edit','text'=>t('Edit')];
			}
			unset($tabs['add']);
		};
	$classInfo['list_top_text']=
		'<h1 class="table-render-title">Place</h1>'.
		t('Unused places are automatically deleted after a month. Places only used in unpublished events (rejected), are automatically deleted after 3 months. New places are automatically created when you type them in an event form.').
		'<br/>(<a href="'.ent($base_url.'/place/by-proximity').'">'.t('Places by proximity').'</a>)';
}

?>