<?php

//! Is it ok to cache this page?
//! If called with $srcPath=null, $desc=null, it will disable caching for the current page.
//! At startup demosphere_page_cache_serve_page() also checks for uncacheable pages and tells this function if the page is uncacheable.
//! FIXME : this is hackish ... stores state in static var :-(
function demosphere_page_cache_is_cacheable($srcPath,$desc)
{
	global $user,$demosphere_config;
	static $disabled=false;
	if($srcPath===null){$disabled=true;return;}

	$res=!$disabled &&
		$user->id==0 &&
		val($desc,'cache') && 
		$demosphere_config['page_cache_enable'];

	// Session messages are displayed and emptied before this is called.
	// So caching is manually disabled before message display.

	return $res;
}

//! Output (echo) page cached in db or return false.
//! Note: this returns any page in the db, regardless of $demosphere_config['page_cache_enable'] (not setup yet).
function demosphere_page_cache_serve_page($url)
{
	$isUncacheable=false;
	// don't use cached pages for logged-in users (note: $user is not setup yet)
	if(!$isUncacheable && isset($_SESSION['uid']) && $_SESSION['uid']!=0){$isUncacheable=true;}
	// don't use cached pages if dlib_message pending
	if(!$isUncacheable && isset($_SESSION['dlib_messages']) && count($_SESSION['dlib_messages'])!=0){$isUncacheable=true;}
	// don't use cached pages for selfEdit
	if(!$isUncacheable && isset($_SESSION['selfEdit']) && count($_SESSION['selfEdit']))
	{
		$eventIds=db_one_col('SELECT event FROM self_edit WHERE password IN ('.
							 implode(',',array_map(function($s){return "'".db_escape_string($s)."'";},$_SESSION['selfEdit'])).")");
		foreach($eventIds as $eventId)
		{
			if(strpos($url,(string)$eventId)!==false){$isUncacheable=true;break;}
		}
	}
	// don't use cached pages for pages where user has a comment
	if(!$isUncacheable && isset($_SESSION['hasComments']))
	{
		$sessionN=dbsessions_get_n();
		$max=5; // Performance: avoid slow queries ... just make everything uncacheable if user has lots of comments
		$comments=db_arrays('SELECT pageType,pageId,created FROM Comment WHERE sessionN=%d LIMIT %d',$sessionN ?? -1,$max+1);
		if(count($comments)>$max){$isUncacheable=true;$comments=[];}
		foreach($comments as $comment)
		{
			if($comment['pageType']==='Post' && $comment['created']>time()-3600*24*30*2){$isUncacheable=true;break;}
			else
			if(strpos($url,$comment['pageId'])){$isUncacheable=true;break;}
		}
	}

	// don't use cached pages if this is a POST request
	$method=$_SERVER['REQUEST_METHOD'];
	if($method!='GET' && $method!='HEAD' && $method!='OPTIONS'){$isUncacheable=true;}

	// Don't use cached pages if url is too long to fit into varchar
	// FIXME: maybe we should use url hashes instead of actual urls. This is pretty rare, so it might not be worth it.
	if(mb_strlen($url)>191){$isUncacheable=true;}

	if($isUncacheable)
	{
		// The previous reasons not to serve a previously cached page also apply to not caching this current page.
		demosphere_page_cache_is_cacheable(null,null);
		return false;
	}

	// Load page from database cache
	$cachedPage=demosphere_page_cache_get($url);
	// Not found in cache
	if($cachedPage===null){return false;}

	// **** Ok, we really can serve this page from cache

	$createdTs=$cachedPage['created'];
	$createdDate=gmdate(DATE_RFC1123,$createdTs);
	// Send standard cached page headers
	demosphere_page_cache_headers($createdTs);

	// Add headers saved in cached page (this is typically just: Content-Type) 
	foreach($cachedPage['headers'] as $name=>$val){header($name.': '.$val);}

	// Check if the contents of this page really needs to be sent. 
	// If it has not changed, just send "304 Not Modified", without page contents (much faster).
    if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) || isset($_SERVER['HTTP_IF_NONE_MATCH'])) 
	{
        if(val($_SERVER,'HTTP_IF_MODIFIED_SINCE') == $createdDate || 
		   str_replace('"', '', stripslashes(val($_SERVER,'HTTP_IF_NONE_MATCH',''))) == $createdTs)
		{
            header('HTTP/1.1 304 Not Modified');
			header('ETag: "'.$createdTs.'"');
            return true;
        }
    }

	// FIXME: performance: does storing in db uncompressed suck?

	// Send cached page contents
	echo $cachedPage['page'];
	return true;
}

//! Http headers for cached pages.
function demosphere_page_cache_headers($createdTs)
{
	$createdDate=gmdate(DATE_RFC1123,$createdTs);
	header('ETag: "'.$createdTs.'"');
    header('Last-Modified: '.$createdDate);
	// Browser must check if etag (or last modified) is ok. 
	// In other words: browser must make a request, browser cannot simply use locally saved page without checking
	header("Expires: Thu, 19 Nov 1981 08:52:00 GMT");
	header("Cache-Control: must-revalidate");

	// Copied from demosphere_uncached_page_headers()
	if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=="on")
	{
		header("Strict-Transport-Security: max-age=63072000; includeSubdomains;");
	}

	//header("X-Generator: test-demosphere-is-cached"); 
}

//! Caching is based on $fullUrl not on $srcPath for 2 reasons:
//! $srcPath has no get args (/events.xml?a=b...) 
//! $srcPath is not domain dependent (example.org / mobile.example.org)
function demosphere_page_cache_save($url,$page,array $headers,$created)
{
	db_query("REPLACE INTO page_cache VALUES ('%s','%s','%s',%d)",$url,$page,serialize($headers),$created);
}

function demosphere_page_cache_get($url)
{
	$cachedPage=db_array("SELECT * FROM page_cache WHERE url='%s'",$url);
	if($cachedPage!==null){$cachedPage['headers']=unserialize($cachedPage['headers'],['allowed_classes'=>false]);}
	return $cachedPage;
}

function demosphere_page_cache_clear_all()
{
    // Innodb consistently takes 170ms to truncate an empty table (!!!) :-( ... use DELETE instead 
	db_query("DELETE FROM page_cache"); 
	//db_query("TRUNCATE page_cache");
}

function demosphere_page_cache_clear($like)
{
	global $demosphere_config;
	$likes=[$like];

	// If this is a full url, make sure we clear for both desktop site and mobile site
	$stdBase  =$demosphere_config['std_base_url'];
	$stdMobile=preg_replace('@^https?://(www\.)?@','https://mobile.',$stdBase);
	if(strpos($like,$stdBase)===0)
	{
		$likes[]=$stdMobile.substr($like,strlen($stdBase));
	}
	if(strpos($like,$stdMobile)===0)
	{
		$likes[]=$stdBase.substr($like,strlen($stdMobile));
	}

	foreach($likes as $l)
	{
		db_query("DELETE FROM page_cache WHERE url LIKE '%s'",str_replace('%','%%',$l));	
	}
}

function demosphere_page_cache_install()
{
	db_query("DROP TABLE IF EXISTS page_cache");
	db_query("CREATE TABLE page_cache (
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `page` longblob NOT NULL,
  `headers` text NOT NULL,
  `created` int(11) NOT NULL,
  PRIMARY KEY  (`url`),
  KEY `created` (`created`)
) DEFAULT CHARSET=utf8mb4");

}

?>