<?php

// ************************* General event list form functions (for all fields)

/**
 * Creates standard dlib form array(s) for a given form-item name.
 *
 * Example: $fname='map-shape';$values=[] 
 * ----> $form['map-shape']=['type'=>'textfield','description'=>....];
 *
 * @param $fname: the name of the item, in form-item syntax (example: map-shape)
 * @param $values: a list of current or default values for all form items 
 *     ($form_state['values'] for a standard drupal form).
 * @param $form: the result form array where items will be added.
 */
function demosphere_event_list_form_item($fname,$values,&$form)
{
	global $demosphere_config;
	$form[$fname]=[];
	$f=&$form[$fname];
	$default=val($values,$fname,'');
	switch($fname)
	{
	case 'map-shape'   : demosphere_event_list_form_map_shape	  ($values,$form);break;
	case 'near-lat-lng': demosphere_event_list_form_near_lat_lng($values,$form);break;
	case 'more-options': 
		$f=['html'=>'<div id="more-options"><a href="javascript:void(0);">'.
			t('More options').'</a><div id="more-options-content" style="display:none;">',];
		break;
	case 'more-options-fs': 
		$f=['type' => 'fieldset',
			'title' => t('More options'),
			'collapsible' => true,
			'collapsed' => true,];
		break;
	case 'limit': 
		$f=['type'=>'textfield',
			'title'      =>t('Maximum number of events'),
			'default-value'=>$default,
			'description'=>t('Only select this number of events. The first events are chosen. If you leave this blank, an unlimited number of events will be shown.'),];
		break;
	case 'select-topic': 
		$topics=Topic::getAllNames();
		$topics=dlib_array_insert_assoc($topics,dlib_first_key($topics),0,t('(all)'));
		if($demosphere_config['topics_required']!=='mandatory')
		{
			$topics=dlib_array_insert_assoc($topics,dlib_last_key($topics),'others','-'.t('others').'-',true);
		}
		$f=['type' =>'select',
			'title'=>t('Topic'),
			'options'=>$topics,
			'default-value'=>$default,
			'description'=>t('Only show events having this topic.'),];
		break;
	case 'select-vocab-term': 
		$f=['type' =>'textfield',
			'title'=>t('Term'),
			'default-value'=>$default,
			'wrapper-attributes'=>['class'=>[count($demosphere_config['term_vocabs'])==0 ? 'no-vocabs':'']],
			'description'=>t('Only show events having this term (number).'),];
		break;
	case 'select-vocab-term-or': 
		$f=['type' =>'textfield',
			'title'=>t('Term list'),
			'default-value'=>$default,
			'wrapper-attributes'=>['class'=>[count($demosphere_config['term_vocabs'])==0 ? 'no-vocabs':'']],
			'description'=>t('Only show events having at least one of these terms (Example: 12-54-21-55).'),];
		break;
	case 'select-city-id': 
		$cities=db_one_col("SELECT City.id,City.name FROM City,Place,Event WHERE ".
						   "City.id=Place.cityId AND ".
                           "Place.id=Event.placeId AND ".
						   "Event.status=1 ".
						   "GROUP BY City.id ".
						   "ORDER BY COUNT(*) DESC ".
						   "LIMIT 30");
		$cities=demosphere_sort_clean($cities);
		if(count($cities)===0){$cities=[t('(all)')];}
		else
		{$cities=dlib_array_insert_assoc($cities,dlib_first_key($cities),0,t('(all)'));}
		$f=['type' =>'select',
			'title'=>t('City / district'),
			'options'=>$cities,
			'default-value'=>$default,
			'description'=>t('Only show events that happen in this city / district.'),];
		break;
	case 'select-fp-region': 
		if(count($demosphere_config["frontpage_regions"])===0){$f=['type'=>'data','value'=>false];break;}
		$f=['type' =>'select',
			'title'=>t('Regions'),
			'options'=>[0=>t('(all)')]+dlib_array_column($demosphere_config["frontpage_regions"],"name"),
			'default-value'=>$default,
			'description'=>t('Only show events that happen in this region.'),];
		break;
	case 'select-place-reference-id': 
		$f=demosphere_event_list_form_select_place($default);
		break;
	case 'most-visited': 
		$mostVisitedList=['--'];
		foreach([5,10,15,20,25,30,35,45,60,80,100,130,160] as $n){$mostVisitedList[$n]=$n;}
		$f=['type' =>'select',
			'title'=>t('Most visited events'),
			'options'=>$mostVisitedList,
			'default-value'=>$default,
			'description'=>t('Show the most visited events, limited to this number of events.'),];
		break;
	case 'max-nb-events-per-day': 
		$maxNbEventsPerDayList=[0=>'--'];
		for($i=1;$i<20;$i++){$maxNbEventsPerDayList[$i]=$i;}
		$f=['type'=>'select',
			'title'=>t('Max number of events per day'),
			'options'=>$maxNbEventsPerDayList,
			'default-value'=>$default,
			'description'=>t('Limit the number of events displayed for each day. The most visited events for each day are chosen.'),];
		break;
	case 'select-recent-changes':
		$selectRecentChangesList=[0=>'--'];
		require_once 'demosphere-date-time.php';
		foreach([6,24,2*24,3*24,4*24,5*24,7*24,15*24,30*24] as $h)
		{
			$selectRecentChangesList[$h*3600]=demos_format_date('relative-short-full',time()-$h*3600);
		}
		$f=['type'=>'select',
			'title'=>t('New events since'),
			'options'=>$selectRecentChangesList,
			'default-value'=>$default,
			'description'=>t('Only show events that have been recently published or significantly changed.'),];
		break;
	case 'order-by-last-big-changes':
		$f=['type'=>'checkbox',
			'title'=>t('Recent changes first'),
			'default-value'=>(bool)$default,
			'description'=>t('Show events ordered from the most recently published (or changed) to the oldest. This is useful if you want to keep up with recently published events.'),];
		break;
	default: fatal('demosphere_event_list_form_item: unknown form item:'.$fname);
	}
	if($f===[]){unset($form[$fname]);}
	if(isset($form[$fname]) && !isset($form[$fname]['required'])){$form[$fname]['required']=false;}
}



/**
 * Validate a form-field of a given name.
 *
 * Example: $fname='map-shape',$values=array('lmn-opq'=>123,'map-shape'=>'abc',...)
 * ---> $form['map-shape']['error']='unkown map shape abc'
 *
 * This function relies on demosphere_event_list_parse_getpost_option() for simple fields.
 * Other, more complex, fields may have specific validation code here.
 * Note: this function is meant for form-fields (string values), but
 * also accepts simple values (bool, int) that are allready parsed.
 * @param $fname form-field-name: example "near-lat-lng-use-map"
 * @param $value the val(s) of the form field(s). 
 *        (an array with all values for $action='drupal-form', 
 *         a single value otherwise). Note "all values" is just for convenience.
 * @param $action 'drupal-form': this is a standard drupal form: uses form_set_error
 *          'simple': custom validation: returns array($ok,$value,$message)
 * @return depends on $action 
 */
function demosphere_event_list_form_item_validate($fname,$value,$action='drupal-form')
{
	if($action==='drupal-form'){$value=$value[$fname];}
	$message=false;
	$value=trim($value);
	switch($fname)
	{
	case "near-lat-lng-use-map":
		$ok=true;
		$value=(bool)$value;
		break;
	case "near-lat-lng-distance":
		$ok=$value==='' || preg_match('@^[0-9]*([.][0-9]+)?$@',$value)!==0;
		$message='bad distance format. good example: 12.456';
		$value=(float)$value;
		break;
	case "near-lat-lng-latitude-and-longitude":
		$ok=$value==='' || preg_match('@^-?[0-9]*([.][0-9]+)?,-?[0-9]*([.][0-9]+)?$@',$value)!==0;
		$message='bad latitude,longitude format';
		if($ok){$value=implode(',',array_map('floatval',explode(',',$value)));}
		break;
	case "select-topic":
	case "select-city-id":
	case "select-fp-region":
	case "select-place-reference-id":
		if($value==='0'){$value=false;}
	default:
		if($value==='' || $value===false){$value=false;$ok=true;break;}// empty text fields are ok
		// use demosphere_event_list_parse_getpost_option for simple fields:
		$gname=str_replace(' ','',ucwords(str_replace('-',' ',$fname)));
		// lcfirst only exists in php > 5.3
		$gname=mb_strtolower(substr($gname,0,1)).substr($gname,1);
		list($status,$rvalue)=demosphere_event_list_parse_getpost_option($gname,(string)$value);
		//echo 'validate: after parse:';var_dump(array($gname,$value,$status,$rvalue));
		switch($status)
		{
		case 'unknown':fatal('unknown option: form-name:'.$fname.' get-name:'.$gname);break;
		case 'error':  $message=$rvalue;$ok=false;break;
		case 'ok':     $value  =$rvalue;$ok=true ;break;
		}
	}

	if($message===false){$message='invalid entry:'.$fname;}
	if($ok){$message=false;}

	switch($action)
	{
	case 'drupal-form':
		if(!$ok){$form[$fname]['error']=$message;}
		return $ok;
	case 'simple':
		return [$ok,$value,$message];
	default: fatal('invalid action');
	}
}

/**
 * This function converts a form-field value into an internal (parsed ) value.
 *
 * Example: $iname='mapShape',$values=array('lmn-opq'=>123,'map-shape'=>'abc',...)
 * ---> $res['mapShape']='abc'
 *
 * This function always uses demosphere_event_list_parse_getpost_option() for actual parsing.
 * So it basically converts form-fields into getpost-syntax and then uses that function.
 * Note: this function is meant for form-fields (string values), but
 * also accepts simple values (bool, int) that are allready parsed.
 * @param $iname: std option name: example "nearLatLng" (internal name)
 * @param $values: the values of all form fields
 * @return false if there is an error, true otherwise
 */
function demosphere_event_list_form_item_parse($iname,$values,&$res,$dieOnError=true)
{
	switch($iname)
	{
	case 'nearLatLng':
		// don't parse disabled nearLatLng
		if(!$values['near-lat-lng-use-map']){$res['nearLatLng']=false;return true;}
		$gvalue=$values['near-lat-lng-latitude-and-longitude'].','.
			$values['near-lat-lng-distance'];
		break;
	default:
		$fname=mb_strtolower(preg_replace('@([A-Z])@','-$1',$iname));
		if(!isset($values[$fname])){fatal('ERROR:Could not find form-item:'.$fname);}
		$gvalue=$values[$fname];
		if($gvalue==='' || $gvalue===false){$res[$iname]=false;return true;}
	}

	list($status,$resValue)=demosphere_event_list_parse_getpost_option($iname,(string)$gvalue);

	switch($status)
	{
	case 'unknown':fatal('unknown option: form-name:'.$fname.' get-name:'.$gname);break;
	case 'error':  if($dieOnError){fatal('Error parsing '.$iname.' in demosphere_event_list_form_item_parse:'.$resValue.' ; ');}return false;
	case 'ok':     $res[$iname]=$resValue;return true;
	}
}

/**
 * This function converts an internal (parsed) value into one or more form-field values.
 *
 * Example: $iname="mapShape",$value="abcd" ---> $res['map-shape']="abcd"
 *
 * @param $iname: std option name: example "nearLatLng" (internal name)
 * @param $value: the fully parsed, internaly stored value.
 * @return (no return value)
 */
function demosphere_event_list_form_item_values_from_parsed_value($iname,$value,&$res)
{
	$fname=mb_strtolower(preg_replace('@([A-Z])@','-$1',$iname));
	switch($iname)
	{
	case 'nearLatLng':
		// don't parse disabled nearLatLng
		if($value===false)
		{
			$res['near-lat-lng-use-map']=false;
			$res['near-lat-lng-latitude-and-longitude']='';
			$res['near-lat-lng-distance']='';
		}
		else
		{
			$res['near-lat-lng-use-map']=true;
			$res['near-lat-lng-latitude-and-longitude']=$value['lat'].','.$value['lng'];
			$res['near-lat-lng-distance']=$value['distance'];
		}
		break;
	default:
		$res[$fname]=$value;
		if($value===false){$res[$fname]='';}
	}	
}

// ************************* map shape

/**
 * Creates form items for a map shape (named geographical region).
 *
 * Only called from demosphere_event_list_form_item().
 * See demosphere_mapshape_get()
 */
function demosphere_event_list_form_map_shape($values,&$form)
{
	require_once 'demosphere-event-map.php';
	$publicShapes=demosphere_mapshape_get_public();
	$options='<option value="0">'.t('-predefined-').'</option>';
	foreach($publicShapes as $shape){$options.='<option>'.ent($shape).'</option>';}
	$form['map-shape']=
		['type' => 'textfield',
		 'title' => t('Name of region'),
		 'description' => t('You can select predefined regions, or create your own region. The region you create will be given a name that will be entered here. A region is a shape (for example a polygon) drawn on a map.'),
		 'field-suffix'=>(count($publicShapes) ? 
						  '<select id="map-shape-publiclist">'.$options.'</select>' :'').
		 ' <a id="map-shape-link" href="javascript:void(0);">'.t('create your own region').'</a><div id="map-shape-popup-container" ></div>',
		 'default-value'=>val($values,'map-shape',''),
		];
}

function demosphere_event_list_form_map_shape_upload_popup()
{
	//echo '<style type="text/css">input{width:10em;}</style>';
	$items=demosphere_event_list_form_map_shape_upload_form();
	require_once 'dlib/form.php';
	$form=form_process($items);
    return '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><style type="text/css">form{margin-bottom:0;}</style><head>'.
		'<body>'.$form.'<body></html>';
}

function demosphere_event_list_form_map_shape_upload_form()
{
	$items['map-shape-upload']=
		['type' => 'file',
		 'size' => 20,
		 'title' => t('Your map region file'),
		 'description' => t('This file can be a simple KML file containing a polygon or just a simple text file with cordinates of a polygon (long,lat long,lat long,lat ...).'),
		];	
	$items['submit']=['type' => 'submit',
					  'value'=>t('Upload file'),
					  'submit'=>'demosphere_event_list_form_map_shape_upload_form_submit'];

	return $items;
}

function demosphere_event_list_form_map_shape_upload_form_submit($items)
{
	require_once 'demosphere-event-map.php';

	if(!isset($_FILES['map-shape-upload']['name']) ||
	   strlen($_FILES['map-shape-upload']['name'])===0)
	{
		dlib_bad_request_400(t('You did not upload a file!'));
	}

	// determine unique name for this shape
	$name=$_FILES['map-shape-upload']['name'];
	$name=preg_replace('@.kml$@','',$name);
	if($name===''){$name='noname';}
	$name0=$name;
	$exists=demosphere_mapshape_get($name);
	for($i=0;$i<15;$i++)
	{
		if(demosphere_mapshape_get($name)===false){break;}
		$name=$name0.'-'.mt_rand(0,round(pow(10,$i+1)));
	}
	if(demosphere_mapshape_get($name)!==false){fatal('failed to make unique name');}

	// parse KML data 
	$data=file_get_contents($_FILES['map-shape-upload']['tmp_name']);
	$coords=demosphere_mapshape_parse_kml_polygon($data);
	if($coords===false){dlib_bad_request_400('KML file parse failed');}
	$shape=['type'=>'polygon',
			'isPublic'=>false,
			'coordinates'=>$coords,
		   ];
	//var_dump($shape);
	demosphere_mapshape_save($name,$shape);
	global $base_url;
	if(!isset($items['submit']['redirect'])){dlib_redirect($base_url.'/upload-map-shape-finished?name='.urlencode($name));}
}
function demosphere_event_list_form_map_shape_upload_finished()
{
	global $base_url;
	echo 'upload ok';
	echo '<script type="text/javascript" src="'.$base_url.'/lib/jquery.js"></script>';
	echo '<script type="text/javascript"> $(document).ready(function() {'.
		'window.parent.demosphere_event_list_form_map_shape_upload_finished('.dlib_json_encode_esc($_GET['name']).');});'.
		'</script>';
}


// ************************* 
// ************************* choose circle on map (neat-lat-lng)
// ************************* 

/**
 * Creates form items to interactively draw a geographical region defined by a circle on a map.
 *
 * Only called from demosphere_event_list_form_item()
 */
function demosphere_event_list_form_near_lat_lng($values,&$form)
{
	$isMobile=demosphere_is_mobile();

	$map='<div id="near-lat-lng-map-wrapper">'.
		 '<p id="near-lat-lng-disabled">'.t('map disabled').'</p>'.
		 '<div id="near-lat-lng-map"></div></div>';

	$form['near-lat-lng-start'] = ['html'=>'<div id="near-lat-lng">'];

	if(!$isMobile){$form['near-lat-lng-map'] = ['html'=>$map];}

	$form['near-lat-lng-formitems-start'] = ['html'=>'<div id="near-lat-lng-formitems">'];

	$form['near-lat-lng-use-map'] = 
		['type' => 'checkbox',
		 'title' => t('Select events near a place, by positioning the place on a map'),
		 'description' => t('If you select this option, only events that are inside a circle will be included. You can move the center of the circle on the map, and also change the size of the circle.'),
		 'default-value'=>val($values,'near-lat-lng-use-map'),
		];

	if($isMobile){$form['near-lat-lng-map'] = ['html'=>$map];}

	$form['near-lat-lng-distance'] = 
		['type' => 'textfield',
		 'title' => t('Distance'),
		 'required' => false,
		 'description' => t('Only events that happen inside the red circle will be included. This distance is the radius of the circle. It is expressed in kilometers. You can use the + and - buttons to enlarge or shrink the circle.'),
		 'default-value'=>val($values,'near-lat-lng-distance'),
		];
	$form['near-lat-lng-latitude-and-longitude'] = 
		['type' => 'textfield',
		 'title' => t('Latitude, Longitude'),
		 'required' => false,
		 'description' => t('The center of the circle. Move the red marker on the map to set this field.'),
		 'default-value'=>val($values,'near-lat-lng-latitude-and-longitude'),
		];
	global $demosphere_config;
	//$cityVid=$demosphere_config['city_vocabulary_id'];
	//$vocabs=taxonomy_get_vocabularies();
	$form['near-lat-lng-city-warning'] = 
		['html'=>'<div id="near-lat-lng-and-city-warning" style="display:none">'.
		 t('Warning: you have selected both a city/district and a place on a map. Only events that meet both of these rules will be displayed.').
		 '</div>'];

	$form['near-lat-lng-formitems-end'] = ['html'=>'</div>'];
	$form['near-lat-lng-end'          ] = ['html'=>'</div>'];

}


// ************************* 
// ************************* Place select
// ************************* 

//! Creates a select box form for references places
function demosphere_event_list_form_select_place($default)
{
	// Fetch a list of terms for this vocab, sorted by most used order.
	// Note : this is not always pretty (user might expect alphabetical order...)
	$pids=db_one_col('SELECT Place.referenceId,COUNT(*) AS ct FROM Event,Place WHERE Event.status=1 AND Place.id=Event.placeId AND Event.startTime>%d GROUP BY Place.referenceId ORDER BY ct DESC LIMIT 30',time()-3600*24*365);

	// First item in list (not selected) :
	$options=[0=>t('(all)')];
	// build list of options
	foreach($pids as $pid=>$ct)
	{
		$options[$pid]=Place::fetch($pid)->makeName();
	}

	return
		['type' => 'select',
		 'title' => t('Place'),
		 'options' => $options,
		 'default-value'=>$default,
		 'description' => t('Select events at this place'),
		];
}


//! Sorts an array of strings using correct order when numerical values are encountered.
//! Accents and trailing space (trim) are also removed for sort order.
function demosphere_sort_clean($list)
{
	$order=$list;
	$nlist=[];
	foreach($list as $k=>$v){$nlist[$k]=[$k,$v];}

	$order=preg_replace_callback('@(?<![0-9])([0-9]+)@',
								 function($matches){return sprintf("%010d",$matches[1]);},
								 $order);
	foreach(array_keys($order) as $k)
	{
		$order[$k]=trim($order[$k]);
		$order[$k]=dlib_remove_accents($order[$k]);
		$order[$k]=preg_replace('@^([( -])@','zzzzz$1',$order[$k]);
	}
	array_multisort($order,SORT_ASC,$nlist);
	$list=count($nlist)===0 ? [] : array_combine(dlib_array_column($nlist,0),dlib_array_column($nlist,1));
	return $list;
}

?>