<?php

//! Post an event to another web site by faking an browser session using curl.
//! This is called when user submits the backend form.
//! It is configured in $demosphere_config['post_on_other_site']
//! Currently supported remote site types: demosphere
//! Can be customized in site's 'custom" module to add new types.
function demosphere_event_post_on_other_site($site,$event)
{
	require_once 'dlib/html-tools.php';
	switch($site['type'])
	{
	case 'demosphere':
		return demosphere_event_post_on_other_site_demosphere($site,$event);
	default:
		if(function_exists('custom_post_on_other_site'))
		{
			return custom_post_on_other_site($site,$event);
		}
		else{fatal("demosphere_event_post_on_other_site: invalid site type".$site['type']);}
	}
}

//! Handler to post on other demosphere sites.
//! This uses curl to submit a simple form provided on other site on https://other-site.org/post-on-other-site
//! The approach used here can be reused for many other sites.
function demosphere_event_post_on_other_site_demosphere($site,$event)
{
	global $demosphere_config,$base_url;
	require_once 'dlib/download-tools.php';
	// setup cookie file (for sessions)
	$cookies = tempnam($demosphere_config['tmp_dir'], "demosevent-submit-other-site-"); 

	// ****** optionally log into other demosphere site
	if(strlen($site['user']))
	{
		$loginPage=dlib_download_and_clean_html_page($site['url'].'/user',false,['cookiejar'=>$cookies]);
		$post=demosphere_form_values_from_html($loginPage);
		print_r($post);
		if(!isset($post['pass'])){dlib_message_add('submit to other site: login page load failed','error');return;}
		$post['name']=$site['user'];
		$post['pass']=$site['password'];
		$loginResPage=dlib_download_and_clean_html_page($site['url'].'/user',false,['cookiejar'=>$cookies,'postfields'=>$post]);
		//file_put_contents('/tmp/loginResPage',$loginResPage);
	}

	// ****** download post-on-other-site form and retreive all form elements
	$formPage=dlib_download_and_clean_html_page($site['url'].'/post-on-other-site',false,['cookiejar'=>$cookies]);
	//file_put_contents('/tmp/formPage',$formPage);
	$post=demosphere_form_values_from_html($formPage);
	//echo '<pre>';print_r($post);die('ii');
	if(val($post,'form-id')!=='post-on-other-site' ){dlib_message_add('submit to other site: form page load failed','error');return;}

	// ****** fill in form elements with values from $event
	$post['city'      ]=$event->usePlace()->getCityName();
	$post['address'   ]=$event->usePlace()->address;
	$post['latitude'  ]=$event->usePlace()->latitude;
	$post['longitude' ]=$event->usePlace()->longitude;
	$post['zoom'      ]=$event->usePlace()->zoom;

	// FIXME: add html title support
	$post['title'    ]=$event->title;
	$post['startTime']=strftime('%Y-%m-%d %T',$event->startTime);
	// replace relative urls by absolute for images and links 
	$body=preg_replace('@(<(img|a)[^>]*(src|href)=")/@','$1'.$base_url.'/',$event->body);
	$sourceLink=t('_source_').' : <a href="'.ent($event->url()).'">'.
			ent(mb_substr($event->url(),0,45,'UTF-8')).'</a>';
	$post['body'     ]=$body.'<p>'.$sourceLink.'</p>';

	$post['contact'   ]=$demosphere_config['contact_email'];
	$post['source'    ]=$event->url();
	$post['remarks'   ]=t('Automatically posted from !url',['!url'=>$base_url]);

	// FIXME: the hash does not add any security, it just makes it a tiny bit more difficult to spam.
	// Anyways, this is just equivalent of posting on the public form, so there are no security issues.
	$post['challenge' ]=hash('sha256','7f98872fcc038ab:'.$post['dlib-form-token-post-on-other-site']);

	//echo '<pre>';print_r($post);die('ii');

	// ****** submit form
	$eventSubmittedPage=
		dlib_download_and_clean_html_page($site['url'].'/post-on-other-site',
										  false,['cookiejar'=>$cookies,'postfields'=>$post]);
	//file_put_contents('/tmp/eventSubmittedPage',$eventSubmittedPage);

	// ****** check if ok, and retreive url of submitted event
	if(preg_match('@demosphere_event_post_on_other_site_form#success#([^#]+)#@',$eventSubmittedPage,$matches))
	{
		dlib_message_add('Successfully submitted event on other site: '.
						 '<a href="'.ent($matches[1]).'">'.ent($matches[1]).'</a>');
	}
	else
	{dlib_message_add('submit to other site: submit failed','error');return;}

	unlink($cookies);
}

//! This form is only meant to be used by another remote demosphere to post events on this site.
//! It is very basic : it is not meant for human interaction.
function demosphere_event_post_on_other_site_form()
{
	require_once 'dlib/form.php';

	$event=new Event();
	list($objForm,$unused)=form_dbobject($event);

	$items['title'     ]=$objForm['title'];
	$items['startTime']=$objForm['startTime'];
	$items['body'      ]=$objForm['body'];

	$place=new Place();
	list($placeObjForm,$unused)=form_dbobject($place);
	$items['address'  ]=$placeObjForm['address']; 
	$items['latitude' ]=$placeObjForm['latitude']; 
	$items['longitude']=$placeObjForm['longitude'];
	$items['zoom'     ]=$placeObjForm['zoom'];
	$items['city'     ]=['type'=>'textfield'];

	$items['contact'  ]=['type'=>'textfield'];
	$items['source'   ]=['type'=>'textfield'];
	$items['remarks'  ]=['type'=>'textfield'];
	$items['challenge']=['type'=>'textfield'];

	$items['save']=
		['type'=>'submit',
		 'submit'=>function($items)
			{
				$values=dlib_array_column($items,'value');

				if($values['challenge']!==hash('sha256','7f98872fcc038ab:'.dlib_get_form_token('post-on-other-site')))
				{
					dlib_permission_denied_403('bad challenge response',false);
				}

				$place=new Place();
				$place->address  =$values['address']; 
				$place->latitude =$values['latitude']; 
				$place->longitude=$values['longitude'];
				$place->zoom 	 =$values['zoom'];
				$place->cityId 	 =City::findOrCreate($values['city'])->id;
				$place->save();

				$event=new Event();
				$event->title     =$values['title'];
				$event->htmlTitle=ent($values['title']);
				$event->startTime=$values['startTime'];
				$event->placeId=$place->id;
				require_once 'htmledit/demosphere-htmledit.php';
				$event->body      =demosphere_htmledit_submit_cleanup($values['body']);
				$event->setModerationStatus('waiting');
				$event->setNeedsAttention('publication-request');

				$event->extraData['public-form']=
				['contact'=>$values['contact'],
				 'source' =>$values['source' ],
				 'price'  =>$values['price'  ] ?? false,
				 'remarks'=>$values['remarks'],
				 'status' =>'edited',
				];

				$event->save();
				require_once 'demosphere-event-publish-form.php';
				demosphere_event_publish_form_send_email_to_moderators($event);
				echo 'demosphere_event_post_on_other_site_form#success#'.ent($event->url()).'#';
				die();
			}];

	return form_process($items,['id'=>'post-on-other-site']);
}


//! Returns an array with all form element values from the given html.
//! FIXME: this still needs work and testing (checkboxes, select, textarea...)
function demosphere_form_values_from_html($html)
{
	require_once 'dlib/html-tools.php';
	$d=dlib_dom_from_html($html,$xpath);
	$post=[];
	foreach($xpath->query("//input|//textarea|//select") as $node)
	{
		if($node->getAttribute('name')!=='')
		{
			$value=false;
			switch($node->nodeName)
			{
			case 'input': $value=$node->getAttribute('value');break;
			case 'textarea': $value='';break;//FIXME
			}
		
			$post[(string)$node->getAttribute('name')]=(string)$value;
		}
	}
	return $post;
}

?>