// namespace
(function() {
window.demosphere_htmledit = window.demosphere_htmledit || {};
var ns=window.demosphere_htmledit;// shortcut

//! ***********************************************
//! ****  Tincymce plugin for backend html editor (used for posts and events)
//! ***********************************************
tinymce.PluginManager.add('demosphere', function(ed)
{
	var ebody=null;

	// Exports that are specific to this editor instance
	ed.demosphere_htmledit={};
	ed.demosphere_htmledit.move_down=move_down;
	ed.demosphere_htmledit.rebuild_content_info=rebuild_content_info;
	ed.demosphere_htmledit.add_hr_handles=add_hr_handles;
	ed.demosphere_htmledit.add_empty_paragraphs=add_empty_paragraphs;

	demosphere_htmledit_setup();

	function demosphere_htmledit_setup()
	{
		var plugin=this;
		var titles=ed.getParam('demosphere_button_titles');

		// We need to wait for both isInit and isRendered before calling our init_hook()
		// (the order changes between normal and inline Tinymce - ex: hdiff)
		var isRendered=false;
		var isInit=false;

	    // Note, tinymce does not support "shift+f3" as shortcut. 
	    // FIXME: no up/down case  shortcut for now. (using an event listener too slow?)



		// *******************
		// Add postrender to "bold" button which (we asume) is always present (unlike moderator-only buttons)
		// This might be hackish (reverse engineered, didn't find a way to add a "post-toolbar-render" event).
		ed.buttons.bold.onpostrender=function(a,b,c,d)
		{
			//console.log('bold button onpostrender');
			if(isInit && !isRendered){init_hook();}
			isRendered=true;
		};

		// *******************
		ed.addCommand('demosBigHr', function() 
		{
			ed.execCommand('mceInsertContent', false,'<hr/>');
			add_hr_handles();
		});

		ed.addButton('bighr', {title: titles['bighr'], 
							   cmd  : 'demosBigHr'
							   });
		// *******************
		ed.addCommand('demosSmallHr', function() 
		{
			ed.execCommand('mceInsertContent', false, '<hr class="subsection" />');
			add_hr_handles();
		});
		ed.addButton('smallhr', {title: titles['smallhr'], 
								 cmd  : 'demosSmallHr'
								 });

		// *******************
		ed.addCommand('demosDowncase', function() 
		{
			var range=ed.selection.getRng();
			iterate_range_text(range,function(text,foundNonInline)
			{
				text=text.toLowerCase();
				if($('html').attr('lang')=="el")
				{
					// FIXME: ES6 supports Regex character classes : wait until 6/2018
					text=text.replace(/σ(?![ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩΆΈΉΊΌΎΏΪΫαβγδεζηθικλμνξοπρσςτυφχψωάέήίόύώϊϋΐΰa-z])/gi,"ς");
				}
				return text;
			});
		});
		ed.addButton('downcase', {title: titles['downcase'], 
								  cmd  : 'demosDowncase'
								  });

		// *******************
		var upcaseShift=false;
		$('body').on('mousedown','.button-upcase',function(e){upcaseShift=e.shiftKey;});
		ed.addCommand('demosUpcase', function() 
		{
			var range=ed.selection.getRng();
			// iterates throught text elements, keeping track of previous breaks (line breaks, or trailing non-letter in previous text).
			var pendingBreak=true;
			iterate_range_text(range,function(text,foundNonInline)
			{
				if(foundNonInline){pendingBreak=true;}
				if(upcaseShift)
				{
					return text.toUpperCase();
				}
				// FIXME: ES6 supports Regex character classes : wait until 6/2018
				text=text.replace(
					new RegExp("([^a-zñäâàáåãéèëêòóôõöøìíîïùúûüýçΐ-ϿA-ZÑÄÂÀÁÅÃÉÈËÊÒÓÔÕÖØÌÍÎÏÙÚÛÜÝÇ]"+(pendingBreak ? '|^' : '')+")"+
							     "[a-zñäâàáåãéèëêòóôõöøìíîïùúûüýçΐ-Ͽ]","g"),
					function (match){return match.toUpperCase();});
				if(text.length)
				{
					pendingBreak=/[^a-zñäâàáåãéèëêòóôõöøìíîïùúûüýçΐ-Ͽ]$/i.test(text);
				}
				return text;
			});
		});
		ed.addButton('upcase', {title: titles['upcase'], 
								cmd  : 'demosUpcase'
							   });

		// *******************
		ed.addCommand('demosTidy', function() 
		{
			if(typeof ed.getBody().spellcheck!=='undefined'){ed.getBody().spellcheck=0;}
			cleanup_content_info();
			var html=ed.getContent();
			$.post(base_url+"/htmledit/tidy", 
				   {
					   html: html,
					   isArticle: $('#edit-body').parents('form').is('#Post') ? 1 : 0
				   },
				   function(data)
				   {
					   if(data.length == 0 || data == '0') 
					   {alert("An error occurred.");return;}
					   ed.setContent(data);
					   rebuild_content_info();
					   if(typeof ed.getBody().spellcheck!=='undefined'){ed.getBody().spellcheck=true;}
				   });	

		});
		ed.addButton('tidy', {title: titles['tidy'], 
							  cmd  : 'demosTidy'
							 });

		// *******************
		ed.addCommand('demosLineCleanup', function() 
		{
			cleanup_content_info();
			var sel = ed.selection.getContent();
			var isSelection=sel.length!=0;
			var source= isSelection ? sel : ed.getContent();
			var bookmark=isSelection? ed.selection.getBookmark() : false;

			$.post(base_url+"/htmledit/linecleanup", 
				   {html: source},
				   function(data)
				   {
					   if(data.length = 0 || data == '0') 
					   {alert("An error occurred.");return;}
					   if(isSelection)
					   {
						   // 3 vars used for the Tinymce bug workaround
						   var edoc=ed.getDoc();
						   var bmStart=edoc.getElementById(bookmark.id+'_start');
						   var bmStartPrev=bmStart.previousSibling;

						   ed.selection.setContent(data);

						   // Tinymce bug workaround: sometimes setContent deletes <span id="..._start"> that is used as bookmark
						   // To reproduce bug: select a paragraph bottom up until the limit of the previous paragraph
						   if(edoc.getElementById(bookmark.id+'_start')===null && bmStart!==null && bmStartPrev!==null)
						   {
							   var bmSpan=edoc.createElement('span');
							   bmSpan.setAttribute('id',bookmark.id+'_start');
							   bmStartPrev.parentNode.insertBefore(bmSpan,bmStartPrev.nextSibling);
						   }

						   ed.selection.moveToBookmark(bookmark);
					   }
					   else{ed.setContent(data);}

					   rebuild_content_info();
				   });	
		});
		ed.addButton('linecleanup', {title: titles['linecleanup'], 
							  cmd  : 'demosLineCleanup'
							  });


		// *******************
		ed.addCommand('demosLineWrapCleanup', function() 
		{
			cleanup_content_info();
			var sel = ed.selection.getContent();
			var isSelection=sel.length!=0;
			var source= isSelection ? sel : ed.getContent();
			var bookmark=isSelection? ed.selection.getBookmark() : false;

			$.post(base_url+"/htmledit/linewrapcleanup", 
				   {html: source},
				   function(data)
				   {
					   if(data.length = 0 || data == '0') 
					   {alert("An error occurred.");return;}
					   if(isSelection)
					   {
						   ed.selection.setContent(data);
						   ed.selection.moveToBookmark(bookmark);
					   }
					   else{ed.setContent(data);}

					   rebuild_content_info();
				   });	
		});
		ed.addButton('linewrapcleanup', {title: titles['linewrapcleanup'], 
							  cmd  : 'demosLineWrapCleanup'
							  });


		// *******************
		ed.addCommand('demosListCleanup', function() 
		{
			var sel = ed.selection.getContent();
			if(sel.length==0)
			{
				alert("you must select paragraphs that will be joined into a clean list");
				return;
			}
			var bookmark=ed.selection.getBookmark();

			$.post(base_url+"/htmledit/listcleanup", 
				   {html: sel},
				   function(data)
				   {
					   if(data.length = 0 || data == '0') 
					   {alert("An error occurred.");return;}
					   ed.selection.setContent(data);
					   ed.selection.moveToBookmark(bookmark);
				   });	
		});
		ed.addButton('listcleanup', {title: titles['listcleanup'], 
							  cmd  : 'demosListCleanup'
							  });

		// *******************
		ed.addCommand('demosImageRight', function() 
		{
			var img=ed.selection.getNode();
			if(img.tagName.toUpperCase()!=='IMG')
			{
				img=$(ed.getBody()).find('img')[0];
				if(typeof img==='undefined'){alert("first click on the image you want to float right");return;}
			}
			$(img).toggleClass("floatRight");
		});
		ed.addButton('imageright', {title: titles['imageright'], 
									cmd  : 'demosImageRight'
								   });

		// *******************
		ed.addCommand('demosParagraphRight', function() 
		{
			var p=false;
			if(ed.selection.getNode().tagName.toUpperCase()==='P'){p=ed.selection.getNode();}
			else
			{
				p=$(ed.selection.getNode()).parents("p").eq(0);
				if(p.length===0){alert("not in a paragraph");return;}
			}
			
			$(p).toggleClass("floatRight");
			$(p).toggleClass("floatRightText",$(p).is('.floatRight') && !$(p).is('.composite-doc-container'));
		});
		ed.addButton('paragraphright', {title: titles['paragraphright'], 
									cmd  : 'demosParagraphRight'
									});

		// *******************
		ed.addCommand('demosDToken', function() 
		{
			dtoken_popup();
		});
		if($('body').hasClass('eventAdmin'))
		{
			ed.addButton('dtoken', {title: titles['dtoken'], 
									cmd  : 'demosDToken'
									});
		}

		// ******************* custom insert

		$.each(demosphere_config["editor_custom_insert"],function()
	    {
			//add_my_plugin(wym,"custominsert_"+this['name'],this['icon'],this);
			var name="custominsert_"+this['name'];
			var icon=this['icon'];
			var html=this['html'];
			ed.addCommand('demos_'+name, function() 
			{
				ed.execCommand('mceInsertContent', false,html);
			});
			ed.addButton(name, {title: titles[name], 
								cmd  : 'demos_'+name,
								image: base_url+'/files/images/'+icon});
		});

		// *********** init callback
	    ed.on('init',function(e){
			//console.log('ed.on.init');
			if(isRendered && !isInit){init_hook();}
			isInit=true;
		});

	    //ed.on('postRender',function(e){
		// 	//console.log('ed.on.postrender');
		//});

		// *********** paste callback
	    ed.on('paste'           ,function(e){paste_hook(e);       });
 		ed.on('PastePostProcess',function(e){paste_postprocess(e);});
	}

	function init_hook()
	{
		//console.log('init_hook:',ed);
		var titles=ed.getParam('demosphere_button_titles');

		//console.log('demosphere_htmledit.init');
		ebody=$(ed.getBody());

		// Shortcuts are added in tinymce after plugins. 
		// So we need to remove/override default shortcuts here.
		ed.shortcuts.remove('meta+shift+1');
		ed.shortcuts.remove('meta+shift+2');
		ed.shortcuts.remove('meta+shift+3');
		ed.shortcuts.remove('meta+shift+4');
		ed.shortcuts.remove('meta+shift+5');
		ed.shortcuts.remove('meta+shift+6');

		ed.addShortcut('meta+shift+1', '', ['FormatBlock', false, 'p']);
		ed.addShortcut('meta+shift+2', '', ['FormatBlock', false, 'h2']);
		ed.addShortcut('meta+shift+3', '', ['FormatBlock', false, 'h3']);
		ed.addShortcut('meta+shift+4', '', ['FormatBlock', false, 'h4']);

		$.each(titles,function(name){$('.mce-i-'+name).addClass('demosphere-htmledit-button');});


		// Tinymce4 does not add classes to identify each button :-( 
		// So we do it manually:
		$('.mce-toolbar .mce-btn .mce-ico[class*="mce-i-"]').each(function()
																  {
																	  var name=this.className.match(/\bmce-i-([\w-]+)/)[1];
																	  var re=/images\/button-([\w-]+)\.png/;
																	  var style=this.getAttribute('style');
																	  if(name==='none' && style.match(re)!==null){name=style.match(re)[1];}
																	  this.parentNode.parentNode.className+=" button-"+name;
																  });
		$('.mce-toolbar-grp .mce-listbox').addClass('button-formatselect');

		// add class to formats list popup. Very ugly. :-(
		$('.button-formatselect').click(function()
										{
											if($('.formatselect-menu').length!==0){return;}
											window.setTimeout(function()
															  {
																  var menu=$('.mce-menu');
																  if(menu.find('.mce-menu-item').eq(1).text().match(/2$/)!==null &&
																	 menu.find('.mce-menu-item').eq(2).text().match(/3$/)!==null && 
																	 menu.find('.mce-menu-item').eq(3).text().match(/4$/)!==null    )
																  {
																	  menu.addClass('formatselect-menu');
																  }
															  },0);
										});

		// The right column with the documents menu
		$(ed.getContainer()).append('<div class="htmleditor-area-right"/>');


		// browser specific: 
		if($.browser.msie)
		{
			// IE fails if min-height is added to paragraphs in CSS.
			ebody.addClass('IE');
		}

		// detect specific browser support for css 
		if (!('WebkitBorderImage' in document.body.style) &&
			!('MozBorderImage'    in document.body.style) &&
			!('borderImage'       in document.body.style)    ) 
		{
			ebody.addClass('noBorderImageSupport');
		}

		// 8/2019: tmp fix: shift click on link => open in new window 
		// This used to work, and then stopped.
		ebody.on('click','a',function(e)
				 {
					 if(e.shiftKey) 
					 {
						 window.open($(this).attr('href'),'_blank',"menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes,toolbar=yes,personalbar=yes");
						 e.preventDefault();
					 } 
				 });

		if(typeof docconvert                    !=='undefined'){docconvert.docconvert_init(ed);}
		if(typeof demosphere_event_publish_form !=='undefined'){demosphere_event_publish_form.tinymce_init(ed);}

		add_empty_paragraphs();
		rebuild_content_info("htmledit-init");
		wait_for_editor(ed.id,null,true);
	}


	function cleanup_content_info()
	{
		// remove docconvert buttons
		ebody.find('.docconvert-link-button').remove();
	}

	function rebuild_content_info(type)
	{
		add_hr_handles();
		add_image_handles();
		add_image_menu();
		if(typeof demosphere_event_edit_form!=='undefined')
		{
			demosphere_event_edit_form.rebuild_content_info();
		}

		if(typeof ed.docconvert!=='undefined'){ed.docconvert.rebuild_content_info();}
	}

	function add_hr_handles()
	{
		// hr handles not supported on ie<9
		if($.browser.msie && parseInt($.browser.version, 10)<9){return;}

		ebody.find('.hr-handles').remove();
		ebody.find('hr').removeClass("hr-with-handle");

		ebody.find('hr').each(function()
								 {
									 var hr=$(this);
									 // add class for handles
     								 hr.addClass('hr-with-handle');

									 // also add empty paragraphs if the hr is beside another hr, a document, or an image
									 if(hr.parent().is('body'))
									 {
										 var prev=hr.prev();
										 if(prev.length==0 || 
											prev.is('hr') ||
										    prev.is('.composite-doc-container')  ||
											(prev.is('p') && $.trim(prev.text())=='' && prev.find('img').length!=0))
										 {
											 hr.before('<p>&nbsp;</p>');
										 }

										 var next=hr.next();
										 if(next.is('hr') ||
										    next.is('.composite-doc-container') ||
											(next.is('p') && $.trim(next.text())=='' && next.find('img').length!=0) ||
											// special case (strange) : invisible caret between hr and doc
											(
												next.next().is('.composite-doc-container') && 
												$.trim(next.text())=='' && 
												next.is('p') && 
												next.attr('data-mce-caret')==='before' &&
												next.attr('data-mce-bogus')==='all' 
											)
										   )
										 {
											 hr.after('<p>&nbsp;</p>');
										 }
									 }
									 hr.append('<span class="hr-handles mceNonEditable">'+
											   '<span class="hr-handle hr-handle-resize"></span>'+
											   '<span class="hr-handle hr-handle-close" ></span></span>');
								 });

		// avoid mozilla resizing handles 
		ebody.find('.hr-handles').each(function(){this.contentEditable=false;});

		ebody_on('mousedown','.hr-handle-resize',function(e)
				 {
					 if(e.which!=1){return;}
					 e.preventDefault();e.stopPropagation();
					 $(this).parents('hr').toggleClass("subsection");
				 });

		ebody_on('mousedown','.hr-handle-close',function(e)
				 {
					 if(e.which!=1){return;}
					 e.preventDefault();e.stopPropagation();
					 $(this).parents('hr').remove();
				 });
	}


	function add_image_handles()
	{
		//console.log('add_image_handles');
		// not supported on ie<9
		if($.browser.msie && parseInt($.browser.version, 10)<9){return;}

		ebody.find('.image-handles').remove();
		ebody.find('img').removeClass("image-with-handle");

		ebody.find('img').each(function()
		{
			//console.log('considering image:',this);
			var img=$(this);

			// If an image is not loaded yet, re-call add_image_handles after it is loaded
			if(this.naturalWidth === 0 && !img.hasClass('addImgHandlesWait'))
			{
				img.addClass('addImgHandlesWait')
				img.on('load',function(){add_image_handles();});
			}
			img.removeClass('addImgHandlesWait');
			
			// no handles for small images
			if(img.width()<64 || img.height()<40){return;}
			// no handles for pdfPages 
			if(img.hasClass('pdfPage')){return;}

			// add class for handles
			img.addClass('image-with-handle');

			img.after('<span class="image-handles mceNonEditable" data-mce-contenteditable="false"><span class="image-handle-down"></span><span class="image-handle-right"></span></span>');
		});

		ebody_on('mousedown','.image-handle-right',function(e)
				 {
					 if(e.which!=1){return;}
					 e.preventDefault();e.stopPropagation();
					 $(this).parent().prev().toggleClass("floatRight");
				 });

		ebody_on('mousedown','.image-handle-down',function(e)
				 {
					 if(e.which!=1){return;}
					 e.preventDefault();e.stopPropagation();
					 move_down($(this).parent().prev(),$(this));
				 });

	}

	// Small helper function to add delegated handlers only once in editor body
	function ebody_on(eventType,selector,handler)
	{
		var label=(selector+'--'+eventType).replace(/[^a-z0-9-]/i,'_');
		if(!ebody.hasClass(label))
		{
			ebody.addClass(label);
			ebody.on(eventType,selector,handler);
		}
	}

	// Move something (image or composite doc) down to to the bottom of this part of the document.
	function move_down(obj,removeHandle)
	{
		// The paragraph, h2... where obj is contained
		var container=obj;
		if(!container.parent().is('body')){container=container.parentsUntil('body').last();}
		// Find first hr (or end of document)
		var end=container.nextAll('hr').not('.subsection').first();
		// Now build a list of containers before the hr/end 
		var prev;
		if(end.length!==0){prev=end.prevUntil(container);}
		else
		{
			var last=ebody.children().last();
			prev=last.prevUntil(container).get();
			prev.unshift(last[0]);
			prev=$(prev);
		}

		// Go up that list to find first place where we can insert image
		prev.each(function()
				  {
					  var p=$(this);
					  //if(p.hasClass('demosphere-sources')){return true;} (editing can add extra lines with invalid demosphere-sources)
					  if(p.hasClass('floatRight')){return true;}
					  if((new RegExp("^\\s*"+escapeRegExp(t('source'))+"\\s*:")).test(p.text())){return true;}
					  if(p.find('img').length!==0 && p.find('img').height()>20){return true;}
					  if($.trim(p.text()).length===0 && p.find('img,hr').length===0){return true;}

					  // insert obj after the found position
					  if(!obj.parent().is('body'))
					  {
						  var newP=$('<p></p>');
						  newP.append(obj);
						  p.after(newP);
						  //  cleanup old container
						  if($.trim(container.html())===''){container.remove();}
						  if(typeof removeHandle!=='undefined'){removeHandle.parent().remove();}
					  }
					  else
					  {
						  p.after(obj);
					  }
					  rebuild_content_info();
					  return false;
				  });
	}

	// Add right menu displaying actions when user clicks on image.
	function add_image_menu()
	{
		ebody.find('.use-image-menu').removeClass('use-image-menu');
		ebody.find('img').not('.pdfPage').addClass('use-image-menu');

		// Only add handlers if not already added
		if(ebody.hasClass('image-menu-handlers')){return;}
		ebody.addClass('image-menu-handlers');
		

		// Click anywhere in editor body removes image menu
		ebody.on('click',function(e)
					{
						if(typeof e.originalEvent.imageMenuAdded!=='undefined'){return;}
						$('.htmleditor-area-right .image-menu').remove();
					});

		// Add image menu when user clicks on image
		ebody.on('click','.use-image-menu',function(e)
					{
						if(e.which!=1){return;}
						e.preventDefault();

						// tell click not to remove this menu that has just been created.
						e.originalEvent.imageMenuAdded=true;

						var img=$(this);
						var imageMenu=$('<ul class="image-menu">');

						// Menu item: revert resized image
						var src=img.prop('src');
						var m=src.match('^(.*)/(resized[0-9]*-)+([^/]*)$');
						var revertTo=false;
						if(m!==null)
						{
							revertTo=m[1]+'/'+m[3];
							img.attr('data-revert-src',revertTo);
							imageMenu.append($('<li class="resized-image-menu-item">').text(t('resized_image')));
						}

						// Menu item: OCR
						if($('body').hasClass('eventAdmin'))
						{
							imageMenu.append($('<li>').append(
								$('<a target="_blank">').text(t('img_ocr')).prop('href',
																				 base_url+'/docconvert/pdf-select?'+
																				 "type=image&"+
																				 "url="+encodeURIComponent(revertTo===false?src:revertTo))));
						}
						
						// remove docconvert menu (only if it is not also selected)
						if(typeof ed.docconvert!=='undefined' && 
						   img.parents('.docconvert-current').length===0
						  )
						{
							ed.docconvert.unselect_doc();
						}

						$('.htmleditor-area-right .image-menu').remove();
						$('.htmleditor-area-right').append(imageMenu);
					});

		// Double-click on image restores it to original size
		ebody.on('dblclick','.use-image-menu',function(e)
					{
						if(e.which!=1){return;}
						var img=$(this);

						img.removeAttr('width');
						img.removeAttr('height');

						var revertTo=img.attr('data-revert-src');
						if(typeof revertTo==='undefined'){return;}
						e.preventDefault();

						img.attr('src',revertTo);
						img.attr('data-mce-src',revertTo);
						img.removeAttr('data-revert-src');
						$('.resized-image-menu-item').remove();
					});

	}


	function paste_hook(event)
	{
		// Workaround tinymce bug. Check if bug report is fixed and then remove this code.
		// http://www.tinymce.com/develop/bugtracker_view.php?id=5029

		// Hack: onPaste hook is called before paste, we need to process after paste
		window.setTimeout(function()
						  {
							  // remove all top level textNodes containing only whitespace
							  var children=ed.getBody().childNodes;
							  for(var i=0,l=children.length;i<l;i++)
							  {
								  var node=children[i];
								  // remove text nodes containing only space
								  if(node.nodeType==3 && /* Node.TEXT_NODE */ 
									 node.nodeValue.match(/^\s*$/))
								  {
									  ed.getBody().removeChild(node);
									  i--;
									  l--;
								  }
							  }

						  },100);

		window.setTimeout(function()
						  { 
							  rebuild_content_info();
						  }, 500);	
	}


	// tinymce paste plugin hook
	// FIXME: figure out if we can merge with "paste_hook"
	function paste_postprocess(e) 
	{
		debug_log('post-paste before:',e.node,e);
		var n=$(e.node);
		// Note: remove styles is done by paste plugin (paste_remove_styles option)

		// Remove all style attributes 
		// (Note, this used to be done in paste plugin config, but does not seem to work in tinymce4)
		n.find('*[style]').removeProp("style");

		// Remove / replace disallowed html tags
		n.find('iframe').remove();
		n.find('h1'   ).changeElementType('h2');
		n.find('h5,h6').changeElementType('h4');
		n.find('dl').changeElementType('ul');
		n.find('div,table,tr,tbody,address,blockquote,center,pre,article,section,header').changeElementType('p');
		n.find('*').not('a,br,li,p,ul,ol,img,h2,h3,h4,strong,em,hr').changeElementType('span');

		// replace multiple br's by spliting into paragraphs
		var brs=/<br *\/?>((\s|&nbsp;)*<br *\/?>)+/mg;
		if(n.html().match(brs))
		{
			n.html(n.html().replace(brs,'<p>'));
		}

		// remove empty paragraphs
		n.find('p').each(function()
						 {
							 if($(this).html().match(/^(\s|&nbsp;)*$/)){$(this).remove();}
						 });


		// mark pasted composite-doc for future processing in docconvert_rebuild_content_info
		n.find('.composite-doc').addClass('pasted-composite-doc');
		// IE removes br (!!)
		if($.browser.msie){n.find('.composite-doc img:first-child').before('<br />');}
		n.find('.composite-doc-is-setup').removeClass('composite-doc-is-setup');
		//console.log('post-paste after:',n.html());
		e.node=n[0];

		// Facebook CDN blocks images sometimes, so we use proxy.
		n.find('img').each(function()
		{
			var img=$(this);
			if(img.prop('src').indexOf('fbcdn.net')==-1){return;}
			
			$.get(base_url+'/image-proxy-ajax-remote-url-encode',
				  {remoteUrl:img.prop('src')},
				  function(response)
				  {
					  if(!response.hasOwnProperty('proxiedUrl')){return;}
					  ebody.find('img').each(function()
					  {
						  if($(this).prop('src')!==img.prop('src')){return;}
						  // strange: directly changing existing image src url, fails. We need to create a new image.
						  // (Maybe DOM considers that the image is in a "failed" state?)
						  var newImg=$('<img>');
						  newImg.attr('data-mce-src',response.proxiedUrl);
						  newImg.attr('src',response.proxiedUrl);
						  $(this).after(newImg);
						  $(this).remove();
						  add_image_handles();
					  });
				  });
		});
		
		debug_log('post-paste after:',e.node,e);
	}


	function dtoken_popup()
	{
		if(typeof demosphere_dtoken_data!=='undefined')
		{
			$("#dtoken-popup").toggle();
			return;
		}

		$.get(base_url+'/dtoken-list-json',
			  {},// strange: jquery needs this 
			  function(data)
			  {
				  demosphere_dtoken_data=data;
				  $('body').append(demosphere_dtoken_data.popup);
				  var offset=$(".button-dtoken").offset();
				  $("#dtoken-popup").css('left',offset.left);
				  $("#dtoken-popup").css('top',offset.top+20);
				  $('#dtoken-popup dt').click(function(e)
											  {
												  var tag=$(this).attr('data-token');
												  var html='demosphere_dtoken_'+tag;
												  if(typeof demosphere_dtoken_data.list[tag].insert!=='undefined')
												  {
													  html=demosphere_dtoken_data.list[tag].insert;
													  html=html.replace('#!eid',$('#edit-js-event-id').val());
												  }
												  html=" "+html+" ";
												  ed.execCommand('mceInsertContent', false,html);
												  $("#dtoken-popup").hide();
											  });
				  $("#dtoken-popup").show();
			  },'json');
		return;
	}
	
	//! Add empty paragraphs at the begining or end of text if there is a document or image
	function add_empty_paragraphs()
	{
		var first=ebody.children().first();
		if((first.find('img').length>0 && 
			$.trim(first.text())=='') ||
		   first.hasClass('floatRight') ||
	       first.is('hr')
		  )
		{
			ebody.prepend('<p>&nbsp;</p>');
		}

		var last=ebody.children().last();
		if((last.find('img').length>0 && 
			$.trim(last.text())=='') ||
		   last.hasClass('floatRight') ||
	       last.is('hr')
		  )
		{
			ebody.append('<p>&nbsp;</p>');
		}
	}


});



//! Node tree manipulation: splits the parent of splitPos at splitPos.
//! This function will recursivelly split parents up to stopAt (optional).
//! Returns the container of splitPos (or one of is parents).
function split_after(splitPos,stopAt)
{
	debug_log("split_after:",splitPos.tagName,splitPos);
	var container=splitPos.parentNode;
	if(stopAt && container===stopAt){return container;} // No container!!
	var newNode=document.createElement(container.tagName);
	container.parentNode.insertBefore( newNode, container.nextSibling);

	for(var n=splitPos.nextSibling;n;n=after)
	{
		var after=n.nextSibling;
		debug_log("sib:",n);
		newNode.appendChild(n);
	}
	if(stopAt && container.parentNode!==stopAt)
	{
		return split_after(container,stopAt);
	}
	return container;
}

//! Calls "callback" for all nodes (of any type, elemnts, text, ...) in the range.
function iterate_range(range,callback)
{
	var iterator=document.createNodeIterator(range.commonAncestorContainer,NodeFilter.SHOW_ALL);
	var hasStarted=false;
	while(iterator.nextNode()) 
	{
		var current=iterator.referenceNode;
		if(!hasStarted)
		{
			if(range.startContainer.nodeType===Node.TEXT_NODE)
			{
				if(current!==range.startContainer){continue;}
			}
			else
			{
				// If startContainer is not text node, 
				// the startOffset is the number of child nodes between the start of the startContainer and the boundary point of the Range.
				// https://developer.mozilla.org/en-US/docs/Web/API/Range/endOffset
				if(range.startOffset<range.startContainer.childNodes.length)
				{
					if(current!==range.startContainer.childNodes[range.startOffset]){continue;}
				}
				else
				{
					if(current===range.startContainer.childNodes[range.startContainer.childNodes.length-1]){hasStarted=true;}
					continue;
				}
			}
		}
		hasStarted=true;
		if(range.endContainer.nodeType!==Node.TEXT_NODE)
		{
			// https://developer.mozilla.org/en-US/docs/Web/API/Range/endOffset
			// the endOffset is the number of child nodes between the start of the endContainer and the boundary point of the Range. 
			var isEndChildCase=range.endOffset>=range.endContainer.childNodes.length;
			var cmp=current.compareDocumentPosition(range.endContainer.childNodes[isEndChildCase ? range.endContainer.childNodes.length-1 : range.endOffset]);
			if((cmp===0 && !isEndChildCase) || (cmp&2)){break;}
		}
		callback(current);
		if(range.endContainer.nodeType===Node.TEXT_NODE && current===range.endContainer){break;}
	}
}

//! Calls "callback" with text from text elements in the range.
//! callback(text,foundNonInline,node) => newText
//! foundNonInline says if iteration has crossed a non-inline (eg. block) container (not always needed)
//! node: current node (not always needed)
//! callback should return the new text (modified or not).
function iterate_range_text(range,callback)
{
	// The start node text and end node texts are split at these positions.
	// We need to keep these, as changing text in node can actually change values returned by range.
	var startOffset=range.startOffset;
	var endOffset  =range.endOffset;
	var foundNonInline=false;
	iterate_range(range,function(node)
	{
		if(!foundNonInline){foundNonInline=node.nodeType===Node.ELEMENT_NODE ? getComputedStyle(node).display!=='inline' : false;}
		if(node.nodeType!==Node.TEXT_NODE){return;}
		var oldVal=node.nodeValue;
		var text=oldVal.substring((node===range.startContainer ? startOffset : 0        ),
								  (node===range.endContainer   ? endOffset   : undefined));
		var newText=callback(text,foundNonInline,node);
		if(newText!==text)
		{
			var newVal=
				(node===range.startContainer ? node.nodeValue.substring(0,startOffset) : '')+
				newText+
				(node===range.endContainer   ? node.nodeValue.substring(endOffset)     : '');
			node.nodeValue=newVal;
			if(node===range.startContainer){range.setStart(node,startOffset);}
			if(node===range.endContainer  ){range.setEnd(  node,endOffset  );}
		}
		foundNonInline=false;
	});
}

$.fn.changeElementType = function(newType) 
{
    var attrs = {};
	if(this.length===0){return;}
	
    $.each(this[0].attributes, function(idx, attr) 
    {
        attrs[attr.nodeName] = attr.nodeValue;
    });
	
    this.replaceWith(function() 
    {
        return $("<" + newType + "/>", attrs).append($(this).contents());
    });
};

function debug_log()
{
 	//console.log.apply(null,arguments);
}

// http://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex/6969486#6969486
function escapeRegExp(str) 
{
	return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

//! For other modules that need to acces an editor after it is fully initialized
function wait_for_editor(id,callback,mode)
{
	if(typeof wait_for_editor.callbacks==="undefined")
	{
		wait_for_editor.callbacks=[];
		wait_for_editor.ok_ids=[];
	}
	if(typeof mode==="undefined")
	{
		wait_for_editor.callbacks.push({callback:callback,id:id});
	}
	else
	{
		wait_for_editor.ok_ids.push(id);
	}

	// Call callbacks that have an ok id (and disable them)
	for(var i=0;i<wait_for_editor.callbacks.length;i++)
	{
		var cb=wait_for_editor.callbacks[i];
		var cid=cb.id;
		if(wait_for_editor.ok_ids.indexOf(cid)!==-1)
		{
			// lazy disable (avoid complex code)
			cb.id='0 invalid';
			// actually call callback
			cb.callback(tinyMCE.get(cid));
		}
	}
}


// Exports:
window.demosphere_htmledit.split_after=split_after;
window.demosphere_htmledit.wait_for_editor=wait_for_editor;

// end namespace wrapper
}());
