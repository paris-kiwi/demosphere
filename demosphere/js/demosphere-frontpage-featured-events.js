
function frontpage_featured_events()
{
	if(typeof $==='undefined'){return;}
	if(frontpage_featured_events.ok){return;}
	frontpage_featured_events.ok=true;
	// reset event handler on edit link to avoid reloading js and css
	$('#edit-featured span').get(0).onclick=null;
	$('#edit-featured').click(function(){$('.featured-select,.featured-deselect').toggle();});

	// add a "+" button to each event
	$('.dcal tr td:first-child').prepend('<span class="featured-select" style="position:absolute;">+</span>');

	// add an "X" button for each featured event
	var addDeselect=function(){$('#featured-events li').prepend('<span class="featured-deselect" style="position:absolute;">X</span>');};
	addDeselect();

	$("body").on('mousedown','.featured-select,.featured-deselect',function(e)
	{
		if(e.which!=1){return;}
		e.preventDefault();

		var isSelect=$(this).is('.featured-select');
		// find eid, do ajax call and reload page
		var link=$(this).parents('tr,li').eq(0).find('td:not(.c0,.c1,.cx,.c3),h2').find('a');
		var eid=link.attr('href').replace(/^[^?]*\/([0-9]+).*$/,'$1');

		$.post(base_url+'/event-set-featured',
			   {
				   eid:eid,
				   value: isSelect ? 1:0,
				   'dlib-form-token-demosphere_event_set_featured':demosphere_event_set_featured_form_token
			   },
			   function(response)
			   {
				   $('#featured-events').remove();
				   if(response.html)
				   {
					   $('#edit-featured').after(response.html);
					   addDeselect();
				   }
			   });
	});
}
