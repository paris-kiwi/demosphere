
// Returns the width of a text computed bu renering in a temporary span.
// Optionally uses css properties.
function html_text_width(text,css)
{
	var lengthSpan=$('#html-length-check');
	if(lengthSpan.length===0)
	{
		lengthSpan=$('<span id="html-length-check"/>');
		$('body').append(lengthSpan);
	}
	lengthSpan.attr("style","");
	lengthSpan.css({display: 'none',position: 'absolute','font-size': '13px','font-weight': ''});
	if(typeof css!=='undefined'){lengthSpan.css(css);}
	lengthSpan.text(text);
	return lengthSpan.width();
}
