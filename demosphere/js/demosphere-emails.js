// namespace
(function() {
window.demosphere_emails = window.demosphere_emails || {};
var ns=window.demosphere_emails;// shortcut

$(document).ready(function()
{
	// Auto-open yellow config buttons
	window.setTimeout(function()
	{
		$("#config-main-button").mousedown();
	},1);

	array_of_arrays("#aliases",
	{
		editCallback:function(form,row)
		{
			// Set class of edit form using "type" field
			form[0].className=form[0].className.replace(/type-[^ ]*/g,"");
			var type=form.find(".aofa-field-type").val();
			form.addClass("type-"+(type=='' ? 'none' : type));
			// readonly
			form.find(".aofa-field-type").attr("readonly",true);
			if(type==='moderators' || type==='contributors' /* || type==='contact' */){form.find(".aofa-field-comments").attr("readonly",true);}
		}
	});
	
	array_of_arrays("#mailboxes");

	// Add "type" row value as class for each row
	$("td.aofa-col-type"  ).each(function(){$(this).parents("tr").addClass("row-"+$(this).text());});
	// Buttons in readonly rows are hidden by CSS. We also need to disable click.
	$('.aofa-table .row-readonly .edit'  ).unbind('click');
	$('.aofa-table .row-readonly .delete').unbind('click');
});


// end namespace wrapper
}());
