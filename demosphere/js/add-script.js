// Dynamically add a <script>, optionally using an async attribute or calling a callback when the script has executed.
// Dynamically added scripts are executed by the browser after document is parsed. 
// This is later then it would with <script src="..."> tags.
// Scripts added with as=falsy  are executed in the order add_script() was called.
// You can use wait_for_script() to execute inline JS code between (as=falsy) scripts added by add_script().
// Scripts added with add_script() might finish loading after DOMContentLoaded.
// If you do not use jQuery, use wait_document_ready() to wait for it all.
// If you do use jQuery, $(document).ready() works fine. We delay it until all scripts are finished.
function add_script(u,as,cb)
{
	var s=document.createElement("script");
	s.type="text/javascript";
	if(u.indexOf("http")!==0  && u.indexOf("//")!==0){u=base_url+(u.indexOf("/")!==0 ? "/" : "")+u;}

	s.async=as ? "async" : false;
	s.src=u;

	var finish=false;

	if(as){finish=cb;}
	else
	{
		var so={is_loading:true, url:u, wfs_callbacks:[]};
		add_script.scripts.push(so);
		if(typeof $!=="undefined")$.holdReady(true);
		finish=function()
		{
			so.is_loading=false;
			if(cb)cb();
			while(so.wfs_callbacks.length)so.wfs_callbacks.shift()();
			wait_document_ready();
			if(typeof $!=="undefined")$.holdReady(false);
		};
	}

	if(finish)
	{
		// IE<9
		if(s.readyState)
		{
			s.onreadystatechange=function()
			{
				if(s.readyState=="loaded" || s.readyState=="complete"){s.onreadystatechange=null;finish();}
			};
		}
		else
		// Other browsers
		{s.onload=function(){finish();};}
	}

	document.getElementsByTagName('head')[0].appendChild(s);
}
add_script.scripts=[];

// Do something after the previous script added with add_script has finished (and before doc ready).
// This is used to wrap inline JS and execute it in correct order.
// FIXME: this is used very little, but makes every single page larger.
function wait_for_script(cb)
{
	var last=add_script.scripts[add_script.scripts.length-1];
	if(!last || !last.is_loading)cb();
	else  last.wfs_callbacks.push(cb);
}

// When scripts are added with add_script, DOMContentLoaded might fire before scripts have finished loading.
// This function waits for both document and scripts. 
// FIXME: this is used very little, but makes every single page larger.
function wait_document_ready(f)
{
	var self=wait_document_ready;
	if(typeof f==='function')self.callbacks.push(f);

	if(!('document_is_ready' in self))
	{
		self.document_is_ready=document.readyState!=='loading';
		document.addEventListener('DOMContentLoaded', function(){self.document_is_ready=true;self();},false);
	}

	if(!self.document_is_ready)return;
	for(var i=0;i<add_script.scripts.length;i++){if(add_script.scripts[i].is_loading)return;}
	
	while(self.callbacks.length)self.callbacks.shift()();
}
wait_document_ready.callbacks=[];
