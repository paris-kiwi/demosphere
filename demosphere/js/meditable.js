// namespace
(function() {
window.meditable = window.meditable || {};
var ns=window.meditable;// shortcut


// MEditable: click to edit functionality.
// User can click on something displayed on the page to edit it. 
// The element is replaced by an HTML input (input[text], textarea, select).
// When focus leaves (user clicks elsewhere), the input is removed and replaced by the typed text.
// Changes (typed text) are immediately sent to server as they are typed (at every key press).
// An instance of the MEditable class is attached to each meditable DOM element.
// Two modes exist (determined by the presence of data-val attribute during MEditable creation):
// - Simple mode: the displayed text is the actual value that is edited.
// - DataValMode : the displayed text is not the same as the edited text.
//   Example: a date is typed in input as "1/2/2012" but displayed as "February 2nd, 2012"
//   The edited value is stored in an attribute called "data-val"
//   The ajax response returns the text "February 2nd, 2012"
//   that is stored  in MEditable.responseHtml/MEditable.responseText  and displayed  when the
//   editable is closed.
// For the select input type, both of the above modes still hold.
// An extra attribute "data-selected" contains the actually selected value.
// If data-selected is initially not set, it will be copied either from data-val (DataValMode) 
// or guessed from the currently displayed text.
// 
// Customization: lots of options : see doc in code for defaultSettings
//
// Keeping track of change numbers: these are used server-side to detect if someone else (or some other window) changed the data.
//    more info in: MEditable.ajax_update()
//
// Ajax request (post):
// - name: "example" which field has been changed
// - "example": value ; the new value for the changed field
// to add custom post fields for all requests change meditable.MEditable.defaultSettings.updateData
// 
// Ajax response :
// - change_number (required): an increasing integer for each valid change
// - errors (only if error): an array of error messages that will be displayed in a popup
// - html   (data-val mode): html that will be displayed in element
// - text   (data-val mode): text that will be displayed in element
// - value  (data-val mode): actual value stored in data-val attribute, and edited in input
//
// Usage example: $('#my-element').meditable({name: 'age', placeHolder:'Click to set age'});
//
function MEditable(editEl,options)
{
	var self=this;

	// Unique id for each MEditable (used for identifying & removing event handlers)
	this.id=MEditable.idCount++;
	// The html element that is editable
	this.editEl=editEl;
	// The input element (input[text] or select or textarea)
	this.input=false;
	// Mode: true if the edited and displayed values are different.
	this.isDataValMode=editEl.attr('data-val')!==undefined;
	// Saved ajax response with text that will be displayed in editEl on close
	// (only used in DataValMode)
	this.responseText=false;
	// Saved ajax response with html that will be displayed in editEl on close
	// (only used in DataValMode)
	this.responseHtml=false;
	// Customized settings for this instance of MEditable 
	this.settings=false;
	// The last value found for this meditable.
	// This is used to determine if we really need to send an ajax update.
	this.lastValue=false;

	// *****************
	// Called when user clicks on meditable element.
	// We create the input element and set up editing.
	this.beginEditing=function()
	{
		debug_log('beginEditing');
		var editEl=self.editEl;
		var settings=self.settings;
		var name=self.settings.name;

		// Just in case something went wrong: first close all other open meditables
		$('.meditable.editing').each(function()
									 {
										 debug_log('forcing close of open meditable:',this);
										 $(this).data('meditable').endEditing();
									 });

		editEl.addClass('editing');

		// actually create the input element and fill it 
		if(settings.inputType==='select' || settings.inputType==='selectmenu')
		{
			self.input=$('<select></select>');
			if(settings.multipleSelect){self.input.prop('multiple',true);}
			$.each(settings.selectOptions,function(value,text)
		    {
				self.input.append($('<option/>').attr("value",value).text(text));
			});
		}
		else
		if(settings.inputType==='textarea'){self.input=$('<textarea></textarea>');}
		else                               {self.input=$('<input type="text" />');}
		self.input.addClass('meditable-input');
		var input=self.input;

		var width0 =editEl.width();
		var height0=editEl.height();
		debug_log('found w,h:',width0,height0);

		// remove placeholder text for empty meditable 
		editEl.find('.meditable-placeholder').remove();

		// Select: in DataValMode data-selected is initially copied from data-val
		if((settings.inputType==='select'  || 
			settings.inputType==='selectmenu') && self.isDataValMode)
		{
			editEl.attr('data-selected',editEl.attr('data-val'));
		}

		// Fill input from either editEl html or data-val (if available)
		if(self.isDataValMode)
		{
			input.val(editEl.attr('data-val'));
			if(settings.multipleSelect){input.val(editEl.attr('data-val').split(','));}
		}
		else                  
		{
			if((settings.inputType==='select' || 
				settings.inputType==='selectmenu') && 
			    editEl.attr('data-selected')!==undefined)
			{
				var val=editEl.attr('data-selected');
				if(settings.multipleSelect){val=val.split(',');}
				self.input.val(val);
			}
			else
			{
				input.val(html_to_text(editEl.html()));
				// Select: ensure that data-selected is always set
				if(settings.inputType==='select' || 
				   settings.inputType==='selectmenu'){editEl.attr('data-selected',input.val());}
			}
		}

		// In data-val mode, save html contents so that we can re-display it if user
		// closes meditable without changing it.
		if(self.isDataValMode){this.responseHtml=editEl.html();}

		if(editEl.attr('tabindex')){input.attr('tabindex',editEl.attr('tabindex'));}
		editEl.html(input);

		// Set size
		if(settings.inputType!=='select' && 
		   settings.inputType!=='selectmenu'){input.outerWidth(width0);}
		if(settings.inputType==='textarea'  ){input.outerHeight(height0);}

		// Special case: this editEl is a link or is included in a link: disable link
		input.parents('a').each(function()
        {
			$(this).attr('data-meditable-bu-href',$(this).attr('href'));
			$(this).removeAttr('href');
		});

		if(settings.inputType==='select')
		{
			// Used in 2 cases: auto-open select hack (for all browsers) AND multipleSelect on all browsers
			if(!window.hasOwnProperty('demosphere_is_selenium'))
			{
				// simulate select dropdown by changing select 
				// into a multiline select with position:absolute
				input.css('position','absolute');
				input.css('z-index','100');
				// add extra margin on top and manage clicks on this empty space
				input.find('option').first().css('margin-top','17px'); // does not work on chrome
				input[0].size=input[0].length+1;
				// FIXME: This is no longer needed on Firefox 56 and Chrome 61 (probably earlier too).
				// It is a problem on IE (ok on Edge)
				input.on('mousedown',function(e)
				{
					if(e.target===this && !($.browser.msie && $.browser.versionNumber<=11))
					{
						e.stopPropagation();
						input.hide();
						self.endEditing();
					}
				});
				// workaround: close even if it doesnt change (select already selected option)
				// hide() makes the ui feel faster
				input.click(function(e){input.hide();self.endEditing();});
			}
		}

		// jQuery-ui selectmenu replaces <select> with it's own HTML/CSS/JS menu. 
		if(settings.inputType==='selectmenu')
		{
			input.selectmenu(
				{
					position: {collision: "flip"},
					classes: {"ui-selectmenu-menu"  : "meditable-selectmenu "+name+"-meditable-selectmenu-menu",
							  "ui-selectmenu-button": "meditable-selectmenu-button "+name+"-meditable-selectmenu-button"},
					select: function(){input.change();},
					change: function(){input.change();},
					close:  function(){self.endEditing();}
				});
			input.selectmenu("instance").button.outerWidth(width0);
			input.selectmenu("instance").button.outerHeight(height0);
			input.selectmenu("open");
		}

		settings.inputSetup(self,input);

		// force focus on this input 
		window.setTimeout(function()
						  {// ie bug
							  if(input.is(':visible') && input.css('visibility')!=='hidden'){input.focus();}
						  },1);

		if(settings.inputType==='select' ||
		   settings.inputType==='selectmenu')
		{
			input.on('change',function(e)
			{
				debug_log('select: on change:',input.val());
				if(self.isDataValMode)
				{
					editEl.attr('data-val',self.getValue());
					if(self.getValue()===null){editEl.attr('data-val','');}
				}
				self.endEditing();
			});
		}

		self.lastValue=self.getValue();

		// update using ajax each time a key is un-pressed
		input.keypress(function(e)
		{
			if(e.keyCode == '13' && settings.inputType==='textfield')
			{e.stopPropagation();e.preventDefault();self.endEditing();}
		});
		input.keyup(function(e)
		{
			self.update();
		});
	};

	// *****************
	// Return the value inside the input element.
	// This can be overridden by custom function (for example when using tinymce).
	this.getValue=function()
	{
		var input=self.input;
		var res;
		if(self.settings.getValue!==false){res=self.settings.getValue(self);}
		else{							   res=input.val();}
		return res;
	};

	// *****************
	// Send the current value to server with ajax call.
	// We first check that the value has really changed to avoid too many calls.
	this.update=function()
	{
		debug_log('update');
		var editEl=self.editEl;
		var input=self.input;
		var settings=self.settings;

		var updateData=$.extend({},self.settings.updateData);
		var name=self.settings.name;
		updateData.name=name;
		updateData[name]=self.getValue();		

		if(self.settings.postUrl===false){return;}
		if(self.settings.preUpdateHook!==false && self.settings.preUpdateHook(self,updateData)===false){return;}

		// avoid calling ajax update if value has not changed (ex:arrow keys)
		if(self.lastValue==updateData[name]){return;}
		self.lastValue=updateData[name];

		debug_log('updateData:',updateData);
		// If update is called several times before the first one finishes, then
		// only keep the last call. Limit to once every 200ms
		merge_async_calls(name,'call',200,true,function()
        {
			MEditable.ajax_update(updateData,editEl,self.settings)
				.always(function()
				{
					merge_async_calls(name,'end');
				})
				.done(function(response)
				{
					// save values from ajax response
					var c=0;
					if(response.hasOwnProperty('value')){c=1;editEl.attr('data-val' ,response.value);}
					if(response.hasOwnProperty('text' )){c=1;self.responseText=response.text;}
					if(response.hasOwnProperty('html' )){c=1;self.responseHtml=response.html;}
					// sanity checks
					if(c && !self.isDataValMode)
					{alert("ERROR: found value,text or html in ajax response ... "+
						   "but we are not in dataValMode. Make sure the data-val attribute is set at startup.");}
					if(!c && self.isDataValMode){alert("ERROR: meditable isDataValMode, but no value in ajax response");}

					if(self.settings.updateHook!==false){self.settings.updateHook(self,response);}
				});
		});
	};

	// *****************
	// finish editing (remove input form), for example, when focus leaves this element
	this.endEditing=function(e)
	{
		debug_log('endEditing');
		if(!self.settings.endEditingHook(e,self)){return;}
		var editEl=self.editEl;
		var input =self.input;
		// avoid multiple endEditing calls
		if(!editEl.hasClass("editing")){debug_log('detected multiple endEditing');return;}
		editEl.addClass("endEditing");
		editEl.removeClass("editing");
		// Send value to server (generally, this will do nothing since value has already been sent)
		self.update();
		// wait popup, if closing takes too long
		var wait=$('<div class="meditable-waiting"></div>');
		var waitId=window.setTimeout(function()
		{
			$('body').append(wait);
			wait.offset(editEl.offset());
		},500);

		// We must wait until all ajax updates are over before closing this input
		wait_for_condition(
			function()
			{
				var status=merge_async_calls(self.settings.name,'status');
				return status===undefined  || status==='idle';
			})
			.done(function()
			{
				var settings=self.settings;
				debug_log('endEditing step2');
				window.clearTimeout(waitId);
				wait.remove();

				// Special case: this editEl is a link or is included in a link: re-enable link
				input.parents('a[data-meditable-bu-href]').each(function()
				{
					$(this).attr('href',$(this).attr('data-meditable-bu-href'));
					$(this).removeAttr('data-meditable-bu-href');
				});

				editEl.css('width','');

				if(settings.endEditingPreRemoveHook!==false){settings.endEditingPreRemoveHook(self);}

				debug_log(self.input);
				// set text/display of editEl with the new edited value
				if(self.responseText!==false){editEl.text(self.responseText);}
				else
				if(self.responseHtml!==false){editEl.html(self.responseHtml);}
				else
				{
					if(settings.inputType==='select'  ||
					   settings.inputType==='selectmenu')
					{
						var vals=input.find("option:selected").map(function(){return $(this).text();}).get();
						editEl.text(vals.join(', '));
					}
					else
					if(settings.inputType==='textarea'){editEl.html(text_to_html(self.getValue()));}
					else 					           {editEl.text(self.getValue());}
				}
				if(settings.inputType==='select'  ||
				   settings.inputType==='selectmenu'  ){editEl.attr('data-selected',self.getValue());}
				self.input.remove();

				// if new display is empty, optionally add placeholder
				self.setPlaceholderIfNeeded();

				self.input=false;
				if(settings.endEditingOverHook!==false){settings.endEditingOverHook(self);}
				editEl.removeClass("endEditing");
			});
	};

	// *****************
	this.setPlaceholderIfNeeded=function()
	{
		var editEl=self.editEl;
		var settings=self.settings;
		if(settings.placeHolder!==false && 
		   (editEl.attr('data-val')==='' ||
			$.trim(editEl.text()).length===0))
		{
			editEl.html('');
			editEl.append($('<span class="meditable-placeholder"></span>').text(settings.placeHolder));
		}
	};
	
	// *****************
	// *****************  Constructor code
	// *****************

	// Set custom options: use defaultSettings overridden by options
	this.settings={};
	$.extend(this.settings,MEditable.defaultSettings,options);
	// also merge updateData fields
	this.settings.updateData={};
	$.extend(this.settings.updateData,MEditable.defaultSettings.updateData,options.updateData);

	editEl.addClass('meditable');

	if(this.settings.changeNumberElement===false)
	{
		this.settings.changeNumberElement=editEl.parents('[data-change-number]').eq(0);
	}

	this.setPlaceholderIfNeeded();

	// **** user clicks on this editable
	editEl.on('mousedown',function(e)
	{
		debug_log('editel mousedown');		
		// click to edit, only with left mouse button, and no modifier
		if(e.type!=='focus' && (e.which!=1 || e.ctrlKey || e.metaKey || e.shiftKey)){return;} 
		// do not re-edit if this field is already being edited
		if(editEl.hasClass('editing'   )){return;}
		if(editEl.hasClass('endEditing')){return;}
		e.stopPropagation();
		e.preventDefault();
		self.beginEditing();
	});

	// ******* finish editing (remove input form) when focus leaves this element
	$('body').on('mousedown.meditable'+self.id,function(e)
	{
		//debug_log('meditable body mousedown for:',editEl,editEl.parents('body'));
		// Cleanup handlers for meditables that have been deleted (removed from DOM)
		if(editEl.parents('body').length===0)
		{
			$('body').unbind('mousedown.meditable'+self.id);
			return;
		}
		if(!editEl.hasClass("editing")    ){return;}
		// Selectmenu click in menu
		if(e && $(e.target).parents('.meditable-selectmenu').length){return;}
		// Ignore orphaned targets
		// place holder (or other contents) can disappear during event hanlding
		if(e.target!==document.body && 
		   $(e.target).parents('body').length===0){return;}
		if(editEl[0]===e.target                  ){return;}
		if($(e.target).parents().filter(editEl).length!=0){return;}
		self.endEditing(e);
	});
};

// *****************
// ***************** static values and methods
// *****************

MEditable.defaultSettings=		
{
	// Extra fields that will be sent in ajax update post requests.
	// You can set fields common to all meditables on a page 
	// simply by changing meditable.MEditable.defaultSettings.updateData.
	// Note: new fields are correctly merged with existing defaults.
	updateData  : {},
	// When an element is empty, you can show  this text.
	placeHolder : false,
	// The url where ajax update post requests will be sent.
	// false means that no ajax updates will be performed.
	postUrl     : false,
	// The type of input field to use, when user clicks on meditable.
	// accepted types are : textarea, textfield, select, selectmenu (jQuery ui)
	inputType   : 'textfield',
	// Hook that is called when input has already been created.
	// Use this to setup custom interactions (example: datepicker, tinymce, autocomplete suggestions...)
	inputSetup  : function(meditable,input){},
	// An array describing select option items used when inputType=='select'
	// Array is simply a list of value/text items.
	selectOptions : false,
	// When input type is "select", whether to use multiple select or not
	multipleSelect : false,
	// hook called before an ajax request. If hook returns false, the request is not performed.
	// preUpdateHook(meditable)
	preUpdateHook  : false,
	// hook called at the end of an ajax request, once values have been set.
	// updateHook(meditable,response)
	updateHook  : false,
	// hook called at the end of an ajax request, once values have been set.
	// Called immediately after user does some action (clicks elsewhere) that might
	// cause this meditable to end editing. If the hook returns false, then editing does not end.
	// This is used often to avoid closing the meditable when user clicks on something related to the meditable.
	// Example: datepicker, autocomplete.
	// function(meditable){return !e || $(e.target).parents('ul.ui-autocomplete').length==0;}
	endEditingHook    : function(event,meditable){return true;},
	endEditingPreRemoveHook : false,
	endEditingOverHook      : false,
	// Return the value inside the input element. getValue(meditable)
	// Example tinymce.
	getValue : false,
	// Element common to a group of meditables refering to a same object that keeps change_number value
	// If false, defaults to first parent of editEl that has attribute data-change-number.
	changeNumberElement : false
};
	 
MEditable.idCount=0;

MEditable.displayErrors=function(errors,element)
{
	if(errors===undefined)
	{$('#meditable-error').hide();return false;}

	// create error div	
	var editError=$('#meditable-error');
	if(editError.length==0)
	{
		editError=$('<div id="meditable-error"></div>');
		$('body').append(editError);
		editError.on('mousedown',function(){editError.hide();});
	}

	// add error text
	if(errors.length==1)
	{
		editError.html('');
		editError.append($('<p>').text(errors[0]));
	}
	else
	{
		editError.html('<ul/>');
		$.each(errors,function(k,v){editError.find('ul').append($('<li/>').text(v));});
	}

	var offset=$(element).offset();
	editError.css('left',(offset.left-2)+'px');
	var top=offset.top-editError.height()-20;
	if(top<0){top=offset.top+30;}
	editError.css('top' ,top+'px');
	editError.show();
	return true;
};

//! This is used by MEditable to make its ajax calls. 
//! It should also anywhere else that needs to make ajax updates on the same data.
//! It takes care of locking, change-number and error/fail display.
//! editEl: used to determine the position on screen to display an error, and to determine changeNumberElement if it is not set in options and settings.
//! options: {postUrl,changeNumberElement,updateData}. Missing items are copied from to MEditable.defaultSettings. 
MEditable.ajax_update=function ajax_update(data,editEl,options)
{
	editEl=$(editEl);
	if(options===undefined){options={};}
	if(!options.hasOwnProperty('postUrl'            )){options.postUrl            =MEditable.defaultSettings.postUrl;}
	if(!options.hasOwnProperty('changeNumberElement')){options.changeNumberElement=MEditable.defaultSettings.changeNumberElement;}
	if(!options.hasOwnProperty('updateData'         )){options.updateData         =MEditable.defaultSettings.updateData;}
	if(options.changeNumberElement===false){options.changeNumberElement=editEl.parents('[data-change-number]').eq(0);}

	// In rare conditions user will edit an ajax field before another one is finished.
	// This may cause change numbers to get out of order and block the interface into an "edited elsewhere" error.
	// To avoid this, we use data-update-lock before any ajax call. 
	// This enforces that no ajax call is made before the previous one is over.
	// This is an overkill, but it is simpler to implement than other solutions to detect if another user/window has edited this object.
	return wait_for_condition(function(){return options.changeNumberElement.attr('data-update-lock')!=='L';})
		.then(function()
		{
			options.changeNumberElement.attr('data-update-lock','L');
			var updateData=$.extend(data,
									{
										change_number:options.changeNumberElement.attr('data-change-number')
									},
									options.updateData);
			var url=options.postUrl;
			// This is only meant as extra info to help for debugging. It makes server logs easier to understand.
			url+='?name='+encodeURIComponent(data.name)+'&changeNumber='+updateData.change_number;
			return $.post(url,updateData,undefined,'json')
				.then(function(response)
				{
					options.changeNumberElement.attr('data-update-lock','');
					if(MEditable.displayErrors(response.errors,editEl))
					{
						// Very special case : first edit has errors. Change number 0=>1
						if(options.changeNumberElement.attr('data-change-number')==='0'){options.changeNumberElement.attr('data-change-number',1);}
						return $.Deferred().reject('response has error',response);
					}
					if(!response.hasOwnProperty('change_number')){alert('ERROR: bad ajax response (change_number not found)');}
					options.changeNumberElement.attr('data-change-number',response.change_number);
					return $.Deferred().resolve(response);
				},
			    function(jqXHR, textStatus, errorThrown)
				{
					alert('An error has occured: ajax error:'+textStatus+':'+errorThrown);
					options.changeNumberElement.attr('data-update-lock','');
					return $.Deferred().reject('ajax request failed',jqXHR);
				});
		});
};

// *****************
// ***************** Jquery Extension *****************
// *****************
$.fn.meditable = function( options ) 
{  
	// make each element editable 
	return this.each(function() 
	{
		var editEl=$(this);
		if(editEl.data('meditable')!==undefined){throw new Error("Recreating meditable on same element");}
		var med=new MEditable(editEl,options);
		editEl.data('meditable',med);
	}); 
};

// *****************
// ***************** helper functions
// *****************

//! Returns a jQuery promise that resolves when the condition function polls to true.
//! The condition function may return the following values:
//!  true  => resolves
//!  false => tries again later
//!  other => rejects
function wait_for_condition(condition,delay)
{
	if(delay===undefined){delay=200;}
	var deferred=$.Deferred();
	(function wait()
	{
		var c=condition();
		if(c==true ){deferred.resolve();}
		else
		if(c!=false){deferred.reject();}
		else
		{
			setTimeout(wait,delay);
		}
	})();
	return deferred.promise();
}

function html_to_text(html)
{
	// textContent/innerText : browsers don't transform closing block tags to \n in consistent manner.
	// Firefox: </p> => newline; Chrome: </p>=> ""
	html=html.replace(/(<br[^>]*>|<\/(address|article|aside|audio|blockquote|canvas|dd|div|dl|fieldset|figcaption|figure|footer|form|h[1-6]|header|hgroup|hr|noscript|ol|output|p|pre|section|table|tfoot|ul|video)>)[\n]?/gi,'XHXHXHXHXUHOUIHLOIH');
	html=html.replace(/[ \n\r]+/gi,' ');
	html=html.replace(/XHXHXHXHXUHOUIHLOIH/g,'\n');
	html=html.replace(/<[^>]*>/g,'');	
	// remove html entites
	var tmp = document.createElement("DIV");
	tmp.innerHTML=html;
	var res=tmp.textContent||tmp.innerText;
	if(typeof res==='undefined'){res='';}
	return res;
}

function text_to_html(text)
{
	var tmp=$('<div></div>');
	tmp.text(text);
	return tmp.html().replace(/\n/g, '<br />');
}


function debug_log()
{
 	//console.log.apply(console,arguments);
}

// Exports:
window.meditable.MEditable=MEditable;
window.meditable.html_to_text=html_to_text;
window.meditable.text_to_html=text_to_html;
// Global exports:
window.wait_for_condition=wait_for_condition;

// end namespace wrapper
}());
