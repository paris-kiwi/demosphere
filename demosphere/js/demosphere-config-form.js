$(document).ready(function()
{
	var fragment=window.location.href.match(/#(.*)$/);
	if(fragment!==null)
	{
		$('.form-item-wrapper.'+fragment[1]).addClass('fragment-selected');
	}
	
	if(typeof demosphereColorPresets!=='undefined')
	{
		$.each(demosphereColorPresets,function(name,colorPreset)
		{
			$('#color-presets').find('[value='+name+']').css('border-left-color' ,colorPreset.color_palette_fg);
			$('#color-presets').find('[value='+name+']').css('background-color'  ,colorPreset.color_palette_bg);
			$('#color-presets').find('[value='+name+']').css('border-right-color',colorPreset.color_palette_bg2);
		});
		$('#color-presets').change(function()
		{
			var colorPreset=demosphereColorPresets[$(this).val()];
			$('.color[id^="edit-color-palette-"]').val('');
			$.each(colorPreset,function(name,val)
			{
				$('#edit-'+name.replace(/_/g,'-')).val(val);
			});
			$('.useJscolor .form-color.color').each(function(){this.color.importColor();});
		});
	}

	var initialColors=$('#edit-color-palette-fg,#edit-color-palette-bg,#edit-color-palette-bg2').serialize();
	$('#configure-form').submit(function()
	{
		var newColors=$('#edit-color-palette-fg,#edit-color-palette-bg,#edit-color-palette-bg2').serialize();
		if(newColors!==initialColors)
		{
			alert(t('colors-changed-warning'));
		}
	});

});

