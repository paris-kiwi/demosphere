(function() {
window.demosphere_help = window.demosphere_help || {};
var ns=window.demosphere_help;// shortcut

/*
$(document).ready(function()
{
	var data=ns.help_data;
	if(data.length==0){return;}
	var where=$("#eventAdminMenubar");
	var isAdmin=true;
	if(where.length==0)
	{
		where=$("#header");
		isAdmin=false;
	}
	where.append('<div class="main">?</div>');
	$(".main,.demosphere-help-extra-button").mousedown(main);
});
*/

//! Called when user clicks on main (top) config or help button
//! This starts by loading needed js and css
function main_button(event,helpOrConfig,jqPath)
{
	//console.log('main_button entry',helpOrConfig,typeof $,ns.data);
	if(typeof ns.data==='undefined')
	{
		load_info(helpOrConfig,jqPath);
		// load_info will call main_button again when everything is ready
		return;
	}
	//console.log('main_button real!',helpOrConfig);

	if(event!==null){event.preventDefault();}

	var show=!$('body').hasClass('show-'+helpOrConfig);
	$('body').toggleClass('show-'+helpOrConfig)
	$(".edit_link").toggle();
	
	// in case something moved in the page
	if(show && event!==null){reposition_buttons();}
	if(typeof ns.repositionPoll==='undefined')
	{
		ns.repositionPoll=window.setInterval(function(){reposition_buttons();},1000);
	}

	// add translate link in adm menu 
	if(helpOrConfig==='config' && $('#text-main-button').length===0 && $('body').hasClass('role-admin'))
	{
		var textButton=$('<a id="text-main-button">');
		textButton.attr("title","text translations on this page");
		textButton.attr("href",base_url+'/translation-backend?search='+encodeURIComponent(window.location.href));
		$('#menubar-right-buttons').prepend(textButton);
	}

}

//! Load jquery, jquery-ui, css and json help+config data, and then call main_button
function load_info(helpOrConfig,jqPath)
{
	//console.log('load_info');
	// add css here?
	if(typeof $=='undefined')
	{
		add_script('/'+jqPath,false,function(){load_info_jquery_ok(helpOrConfig);});
	}
	else{load_info_jquery_ok(helpOrConfig);}
}

//! Second step of load_info, once jquery is loaded
function load_info_jquery_ok(helpOrConfig)
{
	//console.log('load_info_jquery_ok');
	
	if(!$('link[href*="/lib/jquery-ui-"]').length)
	{
		$('head').append('<link rel="stylesheet" type="text/css" href="'+
						 base_url+'/lib/jquery-ui-1.12.1.custom/jquery-ui.min.css" />');
		add_script('lib/jquery-ui-1.12.1.custom/jquery-ui.js');
	}

	$.getJSON(base_url+'/help-json',
			  {url:window.location.href}, 
			  function( data ) 
			  {
      			  ns.data=data;
				  create_buttons();
				  main_button(null,helpOrConfig);
			  });
}

//! Creates html for both help and config buttons. They are initially hidden.
function create_buttons()
{
	var body=$('body');
	for(var t=0;t<2;t++)
	{
		var helpOrConfig=t ? 'config' : 'help';
		var items=ns.data[helpOrConfig];
		for(var i=0,l=items.length;i<l;i++)
		{
			var desc=items[i];
			var button=$('<div id="'+helpOrConfig+'-'+i+'" class="'+helpOrConfig+'-button '+
						 (desc.hasOwnProperty('label') ? desc['label'] : '')+'">'+
						 '</div>');

			if(typeof desc.title !== 'undefined'){button.attr('title',$.trim(desc.title));}

			// mouse button action
			if(helpOrConfig==='help'){button.mousedown(help_popup  );}
			else                     {config_button_setup(button,desc);}

			if(desc.htmlSelect=='page'){button.addClass("page");}

			body.append(button);
		}
	}
	reposition_buttons();
	$('#menubar-right-buttons').prepend($('<a id="edit-help">').text('edit help').attr('href',ns.data.helpEditUrl));
	
}

function val(a,def)
{
	if(typeof a==='undefined'){return typeof def==='undefined' ? false : def;}
	return a;
}

function reposition_buttons()
{
	$('.button-no-pos').removeClass('button-no-pos');
	var pageCt=0;
	for(var t=0;t<2;t++)
	{
		var helpOrConfig=t ? 'config' : 'help';
		if(!$('body').hasClass('show-'+helpOrConfig)){continue;}
		var items=ns.data[helpOrConfig];
		for(var i=0,l=items.length;i<l;i++)
		{
			var desc=items[i];
			var button=$('#'+helpOrConfig+'-'+i);

			var element=$(desc.htmlSelect).first();
			if(element.length===0 || !element.is(':visible'))
			{
				if(element.length===0 && !val(desc.noerror))
				{
					$('#'+helpOrConfig+'-main-button').addClass('error');
				}
				button.css('right',0);
				button.css('top',($('.button-no-pos').length*30+100)+'px');
				button.addClass('button-no-pos');
				continue;
			}
			offset=element.offset();
			offset.top -=11;
			offset.left-=11;

 			// apply x/y offsets and positions
			if(typeof desc.xoffset!=='undefined'){offset.left+=desc.xoffset;}
			if(typeof desc.yoffset!=='undefined'){offset.top +=desc.yoffset;}
			if(typeof desc.center !=='undefined'){offset.left+=element.width()/2;}				
			if(typeof desc.right  !=='undefined'){offset.left+=element.width()	;}				
			if(typeof desc.middle !=='undefined'){offset.top +=element.height()/2;}				
			if(typeof desc.bottom !=='undefined'){offset.top +=element.height()	;}				
			
			button.offset(offset);

/*
  FIXME: change these in help desc
			if($.inArray('after' ,options)!=-1){$(desc.htmlSelect).after(  button);}
			else
			if($.inArray('before',options)!=-1){$(desc.htmlSelect).before( button);}
			else
			if($.inArray('append',options)==-1){$(desc.htmlSelect).prepend(button);}
			else                               {$(desc.htmlSelect).append( button);}
*/
		}
	}
}

function help_popup(e)
{
	e.preventDefault();
	e.stopPropagation();
	var data=ns.data.help;
	var button=this;//e.currentTarget; ie problem
	var i=parseInt($(button).attr('id').match(/help-([0-9]*)/)[1]);
	var id="help-dialog-"+i;
	var popup=$('#'+id);
	// create the popup button if it doesn't exist yet
	if(!popup.length)
	{
		popup=$('<div id="'+id+'" class="help-dialog">'+data[i].contents+'</div>');
		if(typeof demosphere_help_hook==='function'){demosphere_help_hook(data[i],popup);}
		popup.attr('title',data[i].title);
		$('body').append(popup);
		popup.dialog({ autoOpen: false, width: 500,height: 400,
						  dialogClass: "dialog"
						}
					);
		popup.css("display","inherit");
	}
	// open/close the popup
	if(popup.dialog('isOpen')){popup.dialog('close');}
	else                      {popup.dialog('open');}
}

function config_button_setup(button,desc)
{
	if(typeof desc.link !== 'undefined')
	{
		var link=$('<a>');
		button.append(link);
		link.attr('href',desc.link);
	}
}



function config_click(e)
{
	//console.log('config_click');
	//e.preventDefault();
	//e.stopPropagation();
}

// Exports:
window.demosphere_help.main_button=main_button;

// end namespace wrapper
}());
