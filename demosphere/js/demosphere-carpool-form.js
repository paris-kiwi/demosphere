// namespace
(function() {
window.demosphere_carpool_form = window.demosphere_carpool_form || {};
var ns=window.demosphere_carpool_form;// shortcut

$(document).ready(function()
{
	// Show / hide "hide email" checkbox when user enters an email in "contact" 
	$('#edit-contact').keyup(function()
	{
		var contact=$(this).val();
		var isEmail=(contact.indexOf('@')!==-1);
		$('#edit-hideEmail-wrapper').toggle(isEmail);
		if(!isEmail){$('#edit-hideEmail').prop('checked', false);}
	}).keyup();
});

// end namespace wrapper
}());
