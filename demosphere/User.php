<?php
class User extends DBObject
{
	public $id=0;
	static $id_        =['type'=>'autoincrement'];
	public $login;
	static $login_     =[];
	public $password;
	static $password_  =[];
	//! email can be empty, in that case $data['email-verify']['email'] is set 
	public $email='';
	static $email_     =['type'=>'email'];
	//! Users in Demosphere only have a single role (this might change in the future).
	public $role=0;
	static $role_      =['type'=>'enum','values'=>[0=>'none',1=>'admin',2=>'moderator',3=>'frontpage event creator',4=>'private event creator']];
	public $created;
	static $created_   =['type'=>'timestamp'];
	public $lastLogin;
	static $lastLogin_ =['type'=>'timestamp'];
	public $lastAccess;
	static $lastAccess_=['type'=>'timestamp'];
	public $lastIp;
	static $lastIp_=[];
	//! Contributors can give opinions on unpublished events.
	//! 0 : not a contributor. 1: std contributor (cannot see other users' opinions) 2+: can see other opinions
	public $contributorLevel =0;
	static $contributorLevel_=['type'=>'int'];

	//! Array containing data related to this user, for other modules.
	//! This is somewhat hackish, and some of it might be better-off as standard fields in this class.
	//! However, there are performance considerations. Adding too-many little used fields can be a performance problem.
	//! A better alternative might be to create DBObject classes for each one of them.
	//! demosphere_email_subscription
	//! demosphere_widget_config
	//! event_publish_donate_was_shown
	//! demosphere_control_panel_prefs
	//! calendar
	//! forgot-password
	//! opinion-options
	//! emails-received-options
	//! email-verify
	public $data=[];
	static $data_      =['type'=>'array'];

	const EMAIL_VERIFY_DELAY = 3600*24*2;

	static $dboStatic=false;

	function __construct()
	{
		$this->created=time();
	}

	//function save()
	//{
	// 	if($this->data===null){$this->data=array();}
	//}

	function delete()
	{
		db_query("UPDATE Post  SET authorId=0 WHERE authorId=%d",$this->id);
		db_query("UPDATE Event SET authorId=0 WHERE authorId=%d",$this->id);
		db_query("UPDATE Event SET changedById=0 WHERE changedById=%d",$this->id);
		db_query("UPDATE Comment SET userId=0 WHERE userId=%d",$this->id);
		db_query("DELETE FROM user_calendar_selection WHERE user=%d",$this->id);
		db_query("UPDATE event_revisions SET changed_by_id=0 WHERE changed_by_id=%d",$this->id);
		db_query("DELETE FROM Opinion WHERE userId=%d",$this->id);
		db_query("UPDATE Carpool SET userId=0 WHERE userId=%d",$this->id);
		db_query("DELETE FROM dbsessions WHERE uid=%d",$this->id);
		db_query("UPDATE Place SET inChargeId=0 WHERE inChargeId=%d",$this->id);

		parent::delete();
	}

	function setHashedPassword($rawPassword)
	{
		$this->password=crypt($rawPassword,'$6$rounds=5000$'.dlib_random_string(15).'$');
	}

	function checkPassword($rawPassword)
	{
		// Special case: very old md5 hashes without salt were re-hashed 
		if(strpos($this->password,'rehash$')===0)
		{
			$rawPassword=md5($rawPassword);
			$goodHash=preg_replace('@^rehash\$@','$',$this->password);
		}
		else
		{
			$goodHash=$this->password;
		}

		// sanity check, this should never happen
		if(strpos($goodHash,'$6$')!==0){return false;}

		// normal passwords using salt
		$salt=explode('$',$goodHash);
		array_pop($salt);
		$salt=implode('$',$salt);
		return crypt($rawPassword,$salt)===$goodHash;
	}

	//! Is this user's role in the list given as arg ($roles)
	function checkRoles($roles)
	{
		return array_search($this->getRole(),
							is_array($roles)? $roles : func_get_args()) 
			!==false;
	}

	static function fetchByLogin($login,$exceptionOnFail=true)
	{
		$id=db_result_check("SELECT id FROM User WHERE login='%s'",trim($login));
		return User::fetch($id,$exceptionOnFail);
	}

	//! Returns a list of email-verify data for users that haven't yet clicked on the email link to verify their address
	static function emailVerifyPending()
	{
		$res=[];
		$allData=db_one_col("SELECT id,data FROM User WHERE email=''");
		foreach($allData as $uid=>$serialized)
		{
			$data=unserialize($serialized,['allowed_classes'=>false]);
			if(isset($data['email-verify'])){$res[$uid]=$data['email-verify'];}
		}
		return $res;
	}

	//! Find uid of user that has a given email.
	//! Returns false if not found
	static function searchByEmail($email,$checkPending=true,$excludeUid=false)
	{
		$uid=db_result_check("SELECT id FROM User WHERE email='%s' AND id!=%d",$email,$excludeUid);
		if($uid===false)
		{
			$verifyPending=dlib_array_column(User::emailVerifyPending(),'email');
			if($excludeUid!==false){unset($verifyPending[$excludeUid]);}
			$uid=array_search($email,$verifyPending);
		}
		return $uid;
	}

	static function createTable()
	{
		db_query("DROP TABLE IF EXISTS User");
		db_query("CREATE TABLE User (
  `id` int(11) NOT NULL auto_increment,
  `login` varchar(250)  NOT NULL,
  `password` varchar(250)  NOT NULL,
  `email` varchar(250)  NOT NULL,
  `role` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `lastLogin` int(11) NOT NULL,
  `lastAccess` int(11) NOT NULL,
  `lastIp` varchar(100) NOT NULL,
  `contributorLevel` int(11) NOT NULL,
  `data` longblob NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `login` (`login`),
  KEY `email` (`email`),
  KEY `role` (`role`),
  KEY `created` (`created`),
  KEY `lastLogin` (`lastLogin`),
  KEY `lastAccess` (`lastAccess`),
  KEY `lastIp` (`lastIp`),
  KEY `contributorLevel` (`contributorLevel`)
) DEFAULT CHARSET=utf8mb4");

	}
}

?>