<?php

//! A proposal to share a ride to a specific event.
class Carpool extends DBObject
{
	public $id=0;
	static $id_         =['type'=>'autoincrement'];

	//! Carpools are always related to a specific event, that gives destination place and date/time.
	public $eventId=0;
	static $eventId_   =['type'=>'foreign_key','class'=>'Event'];

	//! A user id. 0 for un-registered user.
	public $userId=0;
	static $userId_    =['type'=>'foreign_key','class'=>'User'];

	//! Whether this Carpool is passenger or a driver
	public $isDriver=true;
	static $isDriver_   =['type'=>'bool'];
	//! A name or pseudonym (auto-filled with user login for logged-in users)
	public $name;
	static $name_       =[];
	//! Email or phone number (auto-filled with user email for logged-in users)
	public $contact;
	static $contact_    =[];
	//! Privacy protection, when hidden, can be contacted through contact form
	public $hideEmail;
	static $hideEmail_ =['type'=>'bool'];
	//! Departure place
	public $from;
	static $from_       =[];
	//! Departure timestamp (can be 0 = not set)
	public $time;
	static $time_       =['type'=>'timestamp'];
	public $comment;   
	static $comment_    =[];
	//! Wheter this is a request for a one-way or round-trip.
	public $isRoundTrip=true;
	static $isRoundTrip_  =['type'=>'bool'];

	//! Special case: avoid sending several emails  with edit link to same user.
	public $editLinkWasSentTo;
	static $editLinkWasSentTo_=[];

 	static $dboStatic=false;

	function __construct()
	{
	}

	function canEdit($cuser)
	{
		global $demosphere_config;
		$ok=false;
		// admin+mod can edit any carpool
		if($cuser->checkRoles('admin','moderator')){$ok=true;}
		// user can edit his own carpool
		if($this->userId!=0 && $this->userId==$cuser->id){$ok=true;}
		// carpool self edit link
		$auth=substr(hash('sha256',$this->id.':carpool:'.$demosphere_config['secret']),0,10);
		if(val($_GET,'auth')===$auth){$ok=true;}
		// carpool in session
		if(isset($_SESSION['carpools']) && array_search($this->id,$_SESSION['carpools'])!==false){$ok=true;}
		return $ok;
	}

	static function createTable()
	{
		db_query("DROP TABLE IF EXISTS Carpool");
		db_query("CREATE TABLE Carpool (
  `id` int(11) NOT NULL auto_increment,
  `eventId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `isDriver` TINYINT(1) NOT NULL,
  `name` varchar(330) NOT NULL,
  `contact` varchar(330) NOT NULL,
  `hideEmail` TINYINT(1) NOT NULL,
  `from` longtext NOT NULL,
  `time` int(11) NOT NULL,
  `comment` longtext NOT NULL,
  `isRoundTrip` TINYINT(1) NOT NULL,
  `editLinkWasSentTo` varchar(330) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `eventId` (`eventId`),
  KEY `userId` (`userId`),
  KEY `isDriver` (`isDriver`),
  KEY `hideEmail` (`hideEmail`),
  KEY `time` (`time`),
  KEY `isRoundTrip` (`isRoundTrip`)
) DEFAULT CHARSET=utf8mb4");
	}

}
?>
