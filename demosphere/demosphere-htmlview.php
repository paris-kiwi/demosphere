<?php

function demosphere_htmlview_body($body,$rightColOffset=0)
{
	global $base_url,$demosphere_config,$currentPage;

	$body=demosphere_htmlview_punctuation_nbsp($body);
	$isMobile=demosphere_is_mobile();

	// for localhost tests, change image urls to main site
	if(strpos($base_url,'paris.local')!==false)
	{
		$body=str_replace('src="/files/import-images/',
						  'src="https://paris.demosphere.net/files/import-images/',$body);
	}
	// chop text into parts (divided by hr's) and add 
	// page label links. Puts each part in a div.
	$parts=preg_split('@<hr */?>@',$body);
	$nbParts=count($parts);
	$body='';
	$rightObjects=[];
	$rightObjectsParts=[];
	$objCt=0;
	$maxParts=$isMobile ? 10 : 20;
	$increment=intval(floor($nbParts/$maxParts))+1;// very rare, more than 20 parts (ex: help Post)
	foreach($parts as $nPart=>$part)
	{
		$body.='<div id="textPart'.$nPart.'" class="textPart'.
			($nPart==count($parts)-1 ? ' lastPart' : '').'">';
		// add text navigation (parts links)
		$r='';
		if($nPart!=0){$r.='<hr/>';}
		$r.='<div class="textNav">';
		if($nPart==0 && !$isMobile){$r.=t('@nbParts parts',["@nbParts"=>$nbParts]).' : ';}
		for($j=0;$j<$nbParts;$j+=$increment)
		{
			if($nPart!=$j)
			{
				$r.='<a href="#p'.($j+1).'">'.($j+1).'</a> ';
			}
			else
			{
				$r.='<a class="anchor" id="p'.($j+1).
					'">'.($j+1).'</a> ';
			}
		}
		$r=trim($r);
		$r.='</div><!-- end textNav -->';
		if(count($parts)>1){$body.=$r;}

		$r=$part;
		// add class to first block element (so we can reduce top margin)
		$r=preg_replace('@^(\s*(<meta[^>]*>)?\s*)<(p|h[2-4]|ul)@s',
						'$1<$3 class="openingBlock" ',$r);

		// add class to last block element (so we can reduce bot margin)
		if(preg_match('@</(p|h[2-4]|ul)>\s*$@s',$r,$m))
		{
			// FIXME, There HAS to be a simpler way to do this
			$p=strrpos($r,'<'.$m[1]);
			$r=substr($r,0,$p).'<'.$m[1].
				str_replace('"class="',' ',
							' class="closingBlock"'.
							preg_replace('@^([^>]*)(class="[^"]*")@','$2 $1',
										 substr($r,$p+strlen('<'.$m[1]))));
		}

		// Find right objects in each part, and compute their position.

		// add id's to all rightObject's
		$r=preg_replace_callback('@(<[^>]* )(class="[^"]*floatRight)@',
								 // '"$1 id=\"rightObject".($objCt++)."\" $2"'
								 function($m)use(&$objCt){return $m[1].' id="rightObject'.($objCt++).'" '.$m[2];},$r);
		// fetch the list of all the rightObjects
		preg_match_all('@<[^>]*id="(rightObject[0-9]+)"[^>]*>@',
					   $r,$matches);

		// extract information for each right object
		$rightObjectsParts[$nPart]=[];
		foreach($matches[0] as $nm=>$match)
		{
			$id=$matches[1][$nm];
			preg_match('@<([^ ]+)@',$match,$m);
			$type=$m[1];			
			if($type=='p')
			{
				if(preg_match('@class="[^"]*composite-doc-container@',$match)){$type='composite-doc';}
				if(preg_match('@class="[^"]*floatRightText@'         ,$match)){$type='floatRightText';}
			}
			preg_match('@width="([0-9]+)"@',$match,$m);
			$w=val($m,1,false);
			preg_match('@height="([0-9]+)"@',$match,$m);
			$h=val($m,1,false);
			
			$offset=0;

			if($type=='composite-doc' || $type=='p'/* old */)
			{
				$h=260;
				// try to find height of pdf page 
				$rest=substr($r,strpos($r,$match));
				$para=substr($rest,0,strpos($rest,'</p>'));
				if(preg_match('@pdfPage[^>]*height="([0-9]+)"@',$para,$paraMatches) ||
				   preg_match('@height="([0-9]+)"[^>]*pdfPage@',$para,$paraMatches))
				{
					$h=$paraMatches[1]+14+7;// top & bot margin
					$offset=14;
				}
			}
			if($type=='floatRightText')
			{
				$h=260;
				if(preg_match('@data-float-right-text-height="([0-9]+)"@',$match,$m)) 
				{
					$h=$m[1]+15*2;// 2 lines margin
					$offset=15;
				}
			}

			$rightObjectsParts[$nPart][]=count($rightObjects);
			$rightObjects[]=['nPartObj'=>$nPart,
							 'id'=>$id,
							 'type'=>$type,
							 'h'=>$h,
							 'offset'=>$offset];
		}
		$body.=$r;
		$body.='</div><!-- end textPart -->';
	}

	// compute positions for rightObjects, 
	$css='';
	foreach($rightObjectsParts as $nPart=>$objParts)
	{
		$nbPartObjs=0;
		if($nPart==0){$top=$rightColOffset;}
		else{$top=0;}
		foreach($objParts as $n=>$nobj)
		{
			$obj=$rightObjects[$nobj];
			$css.='#htmlView #'.$obj['id'].' {top:'.($top+$obj['offset']).'px;';
			if($type==='floatRightText'){$css.='height:'.$obj['h'].'px;';}
			$css.='}'."\n";
			$top+=$obj['h']+4;
			$nbPartObjs++;
		}
		if($top>0)
		{
			$css.='#textPart'.$nPart.' {min-height:'.$top.'px;}';
		}
	}
	if(!$isMobile){$currentPage->addCss($css,'inline');}



	// htmlawed is 3x slower than filter_xss_admin.
	require_once 'dlib/filter-xss.php';
	$body=filter_xss_admin($body);

	// add zoom js for resize images
	// Image zoom: on mobile, just navigate to image on click
	if($isMobile)
	{
		$imageClickJsOk=false;
		$body=preg_replace_callback('@(<img[^>]*\b)(src="[^">]*/resized[0-9]*-[^>]*>)@',
				function($match)use($imageClickJsOk,$currentPage)
				{
					if(!$imageClickJsOk)
					{
						$imageClickJsOk=true;
						$currentPage->addJS('function demosphere_htmlview_mobile_image_click(event,image){'.
											'    if(image.parentNode.tagName!=="A")'.
											'    {'.
											'        window.location=image.getAttribute("src").replace(/\\/(resized[0-9]*-)+/,"/");'.
											'    }'.
											'}','inline');
					}
					return $match[1].' onmousedown="demosphere_htmlview_mobile_image_click(event,this)" '.$match[2];
				},$body);
	}
	else
	{
		$body=preg_replace_callback('@<img[^>]*src="[^">]*/resized[0-9]*-[^>]*>@',
									'demosphere_htmlview_image_zoom_re_cb',$body);
	}

	if(!isset($_GET['disable-demosphere-dtoken']))
	{
		$body.='<div style="display:none" id="text-image-zoom">'.
			t('zoom').'</div>';
		$body.='<div style="display:none" id="text-image-link">'.
			t('link').'</div>';
		$body.='<div style="display:none" id="text-image-zoom-title">'.
			t('click to zoom on image').'</div>';
		$body.='<div style="display:none" id="text-image-zoom-popup-close">'.
			t('click to close image').'</div>';
	}

	$body=demosphere_htmlview_emails_add_mailto_and_obfuscate($body);
	return $body;
}

function demosphere_htmlview_image_zoom_re_cb($matches)
{
	require_once 'demosphere-common.php';
	$res=$matches[0];
	$res=str_replace('<img','<img onmouseover="var img=this;
   window.demosphere_htmlviewcb=function(){hover_resized_image(event,img);};
   if(typeof $!==\'function\'){
   add_script(\''.demosphere_cacheable('lib/jquery.js').'\');}
   add_script(\''.demosphere_cacheable('demosphere/js/demosphere-htmlview.js').'\');" ',
					 $res);

	$res=str_replace('class="','class="zoomableImage ',$res);
	if(strpos($res,'class="')===false){$res=str_replace('<img','<img class="zoomableImage" ',$res);}
	return $res;
}

function demosphere_htmlview_punctuation_nbsp($html)
{
	if(dlib_get_locale_name()=='fr')
	{
		$html=preg_replace('@(\w|>) ([;:!»?])@u','$1 $2',$html);
		$html=preg_replace('@(«) (\w)@u','$1 $2',$html);
		$html=preg_replace('@(\d) (€|euro)@u','$1 $2',$html);
	}
	return $html;
}

//! Finds text emails in HTML and adds <a href="mailto:..."></a>.
//! It also obfuscates the emails, making them difficult to parse for spammers.
//! This is DOM-based should be XSS safe.
function demosphere_htmlview_emails_add_mailto_and_obfuscate($body)
{
	require_once 'dlib/html-tools.php';
	$doc=dlib_dom_from_html($body,$xpath);

	// https://www.regular-expressions.info/email.html
	$emailRegex='[A-Z0-9._%+-]{1,64}\@[A-Z0-9.-]{1,125}\.[A-Z]{2,63}';

	// Merge adjacent text nodes.
	$doc->normalizeDocument();

	// Find text emails that are not in an <a>
	foreach($xpath->query("//text()[not(ancestor::a) and contains(.,'@')]") as $textNode)
	{
		$text=$textNode->wholeText;

		if(!preg_match_all('@'.$emailRegex.'@iu',$text,$matches, PREG_OFFSET_CAPTURE)){continue;}

		$currentTextNode=$textNode;
		$currentText='';
		$prevPos=0;
 		foreach($matches[0] as $nMatch=>$m)
 		{
			$email=$m[0];
			$pos=$m[1];

			$currentText.=substr($text,$prevPos,$pos-$prevPos);
			$prevPos=$pos;
			$currentTextNode->nodeValue=$currentText;
			
			// Create <a> element
			$onclick='this.href=\'mailto:'.str_replace('@','helQ1ydQc5jje5xBacm',$email).'\'.replace(\'helQ1ydQc5jje5xBacm\',\'@\');return true;';
			$a=$doc->createElement('a');
			$a->setAttribute('href'   ,'mailto:---');
			$a->setAttribute('onclick',$onclick);

			// Build obfuscated contents of <a>
			$emailLength=mb_strlen($email);
			for($i=0;$i<$emailLength;$i++)
			{
				$c=mb_substr($email,$i,1);
				if(($i%8)==0 || $c==='@')
				{
					$span=$doc->createElement('span',$c);
					$span->setAttribute('class','xsd');
					$a->insertBefore($span,null);
				}
				else
				{
					if(($i%8)==4)
					{
						$span=$doc->createElement('span','ex');
						$span->setAttribute('style','display:none');
						$a->insertBefore($span,null);
					}
					$n=$doc->createTextNode($c);
					$a->insertBefore($n,null);
				}
			}

			$nextTextNode=$doc->createTextNode('');
			$currentText='';
			$currentTextNode->parentNode->insertBefore($a           ,$currentTextNode->nextSibling);
			$a              ->parentNode->insertBefore($nextTextNode,$a->nextSibling);
			$currentTextNode=$nextTextNode;

			$prevPos+=strlen($email);
 		}
		$currentText.=substr($text,$prevPos);
		$currentTextNode->nodeValue=$currentText;
	}

	// Merge adjacent text nodes.
	$doc->normalizeDocument();

	$res=dlib_dom_to_html($doc);
	return $res;
}

?>