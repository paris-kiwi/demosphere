<?php
error_reporting(E_STRICT | E_ALL);

// launch command line test if called from command line
if(!isset($_SERVER['SERVER_NAME']) && isset($argv) &&
   basename($argv[0])===basename(__FILE__))
 {html_selector_command_line_test();return;}

function html_selector_command_line_test()
{
	setlocale(LC_TIME, "fr_FR.utf8");
    ini_set("include_path", ini_get("include_path").PATH_SEPARATOR."..".PATH_SEPARATOR."../..");
	require_once 'html-selector/html-selector.php';
	require_once 'dlib/tools.php';
	require_once 'dlib/testing.php';
	require_once 'demosphere.module';
	function t($a){return $a;}
 	global $db_params,$html_selector_config,$demosphere_config,$dlib_config,$template_config;

	$html_selector_config=
		[
			'script_url'  =>'http://demolocal/html-selector',
			'base_url'    =>'http://demolocal/demosphere/html-selector',
			'tmp_dir'=>'/var/www/common/demosphere/html-selector/test',
		];
	$template_config['dir']='/var/www/common/demosphere/html-selector/test';

	$demosphere_config['highlight_keywords']=['@(testkeyword)@i'];
	$demosphere_config['future_date_offset']=3600*4;
	$demosphere_config['future_date_today_with_no_time']=false;
	global $html_selector_is_test;
	$html_selector_is_test=true;

	db_setup();
	html_selector_config_check_fast();
	//html_selector_test_configure_content_selector_ajax_guess_selector();
	html_selector_test();
}

function html_selector_web_test()
{
 	global $html_selector_config,$base_url;

	$html_selector_config=
		[
			'script_url'  =>$base_url.'/html-selector',
			'base_url'    =>$base_url.'/demosphere/html-selector',
			'log_file'=>'demosphere/html-selector/test/web-test/html-selector.log',
			'tmp_dir'=>'demosphere/html-selector/test/web-test',
		];
	html_selector_test();
}

function html_selector_test()
{
	global $html_selector_config;

	$dir=$html_selector_config['tmp_dir'];
	system('rm -f '.$dir.'/html-selector-editor-*');
	
	$e=HtmlSelectorEditor::create(false,'test-title',[]);
	$fname=$dir.'/html-selector-editor-'.$e->id;
	test_assert(file_exists($fname));
	
	// **** find common class
	$e->pages=[['html'=>'<div class="a b c x d">a</div>',],
			   ['html'=>'<div class="x a f g h">a</div>',],
			   ['html'=>'<div class="i f k l x">a</div>',],
			  ];
	$dbg=false;

	$path=[(object)['tagName'=>'div','id'=>'','className'=>'a b c x d',],];
	$res=$e->guessSelector($path,$dbg);
	test_equals($res['selector'],'.x');

	// **** no common class, revert to common tagname
	$e->pages=[['html'=>'<div class="a b c d">a</div>',],
			   ['html'=>'<div class="a f g h">a</div>',],
			   ['html'=>'<div class="i f k l">a</div>',],
			  ];
	$path=[(object)['tagName'=>'div','id'=>'','className'=>'a b c d',],];
	$res=$e->guessSelector($path,$dbg);
	test_equals($res['selector'],'div');

	// ****
	$e->pages=[['html'=>'<div><div id="content"><div>x</div></div><div>a</div></div>',],
			   ['html'=>'<div><div id="content"><div>y</div></div><div>b</div></div>',],
			   ['html'=>'<div><div id="content"><div>z</div></div><div>c</div></div>',],
			  ];
	$path=[(object)['tagName'=>'div','id'=>''       ,'className'=>'',],
		   (object)['tagName'=>'div','id'=>'content','className'=>'',],
		   (object)['tagName'=>'div','id'=>''       ,'className'=>'',]];
	$res=$e->guessSelector($path,$dbg);
	test_equals($res['selector'],'#content div');

	// ****
	$e->pages=[['html'=>'<p><div id="content"><p><div class="c">x</div></p></div><div>a</div></p>',],
			   ['html'=>'<p><div id="content"><p><div>y</div></p></div><div class="c">b</div></p>',],
			   ['html'=>'<p><div id="content"><p><div>z</div></p></div><div>c</div></p>',],
			  ];
	$path=[(object)['tagName'=>'div','id'=>''       ,'className'=>'c',],
		   (object)['tagName'=>'div','id'=>'','className'=>'',],
		   (object)['tagName'=>'div','id'=>'content'       ,'className'=>'',],
		   (object)['tagName'=>'p','id'=>''       ,'className'=>'',],];
	$res=$e->guessSelector($path,$dbg);
	test_equals($res['selector'],'#content div');

	//fatal("stop");
}


?>