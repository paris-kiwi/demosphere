<?php
/** @defgroup html_selector Html Selector
 *  @{
 */


//! $html_selector_config : configuration for html_selector.
//! List of $html_selector_config keys:
//! - tmp_dir
//! - dir_url
//! -::-;-::-
$html_selector_config;


require_once 'dlib/tools.php';
require_once 'dlib/template.php';

function html_selector_add_paths(&$paths,&$options)
{
	$paths[]=['path'=>'@^html-selector/editor/([a-z0-9]+)/guess-selector$@',
			  'roles' => ['admin','moderator'],
			  'output'=>'json',
			  'function'=>function($id)
		{
			$editor=HtmlSelectorEditor::load($id);
			$path=json_decode($_POST['path'],true);
			return html_selector_node_path_to_selector_for_pages($editor->pages,$path);
		},
			 ];
	
	$paths[]=['path'=>'@^html-selector/editor/([a-z0-9]+)/refresh-test-pages$@',
			  'roles' => ['admin','moderator'],
			  'output'=>'json',
			  'function'=>function($id)
		{
			$editor=HtmlSelectorEditor::load($id);
			return $editor->refreshTestPages();
		},
			 ];

	$paths[]=['path'=>'@^html-selector/editor/([a-z0-9]+)$@',
			  'roles' => ['admin','moderator'],
			  'function'=>'html_selector_view_editor',
			 ];

	$paths[]=['path'=>'@^html-selector/editor/([a-z0-9]+)/safe-html$@',
			  'output'=>false,
			  'access' => 'demosphere_safe_domain_check', 
			  'function'=>'html_selector_view_safe_html',
			 ];

	
}


function html_selector_js_config()
{
	global $html_selector_config,$base_url;
	$js='';
	$js.='html_selector_config={'.
		'script_url:"'.$base_url.'/html-selector",'.
		'base_url:"'.$html_selector_config['dir_url'].'"'.
		'};';
	return $js;
}

function html_selector_view_safe_html($editorId)
{
	global $demosphere_config,$base_url;
	require_once 'dcomponent/dcomponent-common.php';

	$editor=HtmlSelectorEditor::load($editorId);

	if(isset($_GET['disabled'])){echo "disabled";return;}
	$page=$editor->pages[intval($_GET['page'])];
	$html=$page['html'];
	if(isset($_GET['testSelector']))
	{
		if(!isset($editor->selector)){echo 'ERROR: no selector defined';return;}
		require_once 'dlib/html-tools.php';
		$allSelectors=array_merge($editor->selector,Article::$unwantedPageParts);
		$html=dlib_html_apply_selectors($html,$allSelectors);
		// strip javascript to avoid js errors and strange interactions
		$html=preg_replace('@<(script|SCRIPT).*</(script|SCRIPT)>@sU','',$html);
		$html=preg_replace('@<(script|SCRIPT)[^>]*/>@','',$html);
		$html=preg_replace('@(on(click|load|blur|focus|mouseover|'.
						   'mousemove|mouseout|resize|scroll)=["\'])@','x$1',$html);
		echo $html;
	}
	else
	{
		// strip javascript to avoid js errors and strange interactions
		$html=preg_replace('@<(script|SCRIPT).*</(script|SCRIPT)>@sU','',$html);
		$html=preg_replace('@<(script|SCRIPT)[^>]*/>@','',$html);
		$html=preg_replace('@(on(click|load|blur|focus|mouseover|'.
						   'mousemove|mouseout|resize|scroll)=["\'])@','x$1',$html);

		$cpage=new HtmlPageData();
	
		// Add CSS and JS for interactive selector
		$cpage->addCssTpl('demosphere/html-selector/html-selector-select.tpl.css');
		$cpage->addJs('window.demosphere_t={'.
					 'grow:"'.t('grow').'",'.
					 'shrink:"'.t('shrink').'"'.
					 '};'.
					 'base_url="'.ent($base_url).'";'.
					 'window.demosphere_url="'.$demosphere_config['std_base_url'].'";','inline');
		require_once 'demosphere/demosphere-common.php';
		$cpage->addJs(demosphere_js_shorten(file_get_contents('demosphere/js/add-script.js')),'inline',['prepend'=>true,'use-compat'=>false]);
		$cpage->addJs('demosphere/html-selector/html-selector-select.js','file',['use-compat'=>false]);
		$insert =$cpage->renderCss();
		$insert.=$cpage->renderJs();
		$html=str_ireplace('</head>',$insert.'</head>',$html);
		echo $html;
	}
}

function html_selector_view_editor($editorId)
{
	global $html_selector_config,$base_url,$currentPage;

	$editor=HtmlSelectorEditor::load($editorId);

	require_once 'dcomponent/dcomponent-common.php';

	// **********
	$currentPage->addCssTpl('demosphere/html-selector/html-selector.tpl.css');
	// translatable strings for javascript
	$currentPage->addJs(html_selector_js_config(),'inline');
	$currentPage->addJsTranslations([
										'newRow'=>        t('Extra selector item'),
										'addRow'=>        t('save selector item'),
										'noEdit'=>        t('No open edit form. Please edit a selector item.'),
										'failedSelector'=>t('Invalid selector. Try the grow button.'),
										'pathTitle'=>     t('Path from selected element to top:'),
									]);
	$currentPage->addJs('lib/jquery.js');
	$currentPage->addJs('demosphere/html-selector/html-selector.js');

	// Create information for array_of_arrays form element (for multi-item selector)
	$types=['CSS'  =>t('CSS selector'  ),
			'XPATH'=>t('XPATH selector'),
			'ERASE'=>t('Erase regex'   ),
			'CSS-REMOVE'=>t('CSS remove'   ),
			'XPATH-REMOVE'=>t('XPATH remove'   ),
		   ];
	$aofaColumns=[['type',t('sel. type'),'select','values'=>$types,],
				  ['arg' ,t('sel. description')]];

	require_once 'dlib/form.php';
	$items=[];
	$items['open']=['html'=>'<div id="open-selector-aofa">+</div>'];
	$items['selector']=
		['type'=>'array_of_arrays',
		 'aofa-options'=>$aofaColumns,
		 'default-value'=>$editor->selector,
		 'attributes'=>['class'=>['custom-init']],
		 'validate'=>function($selectors)
			{
				require_once 'dlib/html-tools.php';
				foreach($selectors as $sel)
				{
					$arg=$sel['arg'];
					switch($sel['type'])
					{
					case 'ERASE':
						if(!preg_match('@^([^\\sa-zA^C0-9\\\\]).*(\1)[a-zA-Z]*$@',$arg)){return 'Invalid regexp syntax';}
						if(@preg_replace($arg,'','')===null){return 'Invalid regexp: test failed';}
						break;
					case 'CSS':
					case 'CSS-REMOVE':
						$arg=dlib_css_selector_to_xpath($arg);
						if($arg===false){return 'Invalid CSS selector';}
					case 'XPATH':
					case 'XPATH-REMOVE':
						try{dlib_html_select_xpath('',$arg);}
						catch(Exception $e)
						{
							return 'Invalid CSS selector or XPATH';
						}
						break;
					}
				}
			}
		];
	$items['wrap-1']=['html'=>'<div id="current-and-button">'];
	$items['current-selector']=['html'=>'<span id="current-selector">&nbsp;</span>'];
	$items['save-selector']=
		['type'=>'submit',
		 'value'=>t('save'),
		 'submit'=>function($items)use($editor)
			{
				// When user hits form "save" button, set the htmlContentSelector and save this feed.
				$selector=$items['selector']['value'];
				$editor->selector=$selector;
				$editor->save();
				html_selector_call_callback($editor->finishedCallback,$editor->selector);
				return;
			}
		];
	$items['wrap-1-end']=['html'=>'</div>'];

	$formOpts=['id'=>'selector-form'];
	if(count($editor->selector)==0 || (count($editor->selector)==1 && $editor->selector[0]['type']==='CSS'))
	{
		$formOpts['attributes']=['class'=>['closed-aofa']];
	}
	$form=form_process($items,$formOpts);

	$iframeUrl=dcomponent_safe_domain_url('html-selector/editor/'.$editor->id.'/safe-html?page=0&x&y&rand='.mt_rand());
	$html=template_render('html-selector.tpl.php',
						  [compact('aofaColumns','form','iframeUrl','editor'),
						   'scriptUrl'=>$base_url.'/html-selector',
						  ]);
	if($editor->displayCallback!==false){$html=html_selector_call_callback($editor->displayCallback,$html);}
	return $html;
}

function html_selector_call_callback()
{
	$arguments=func_get_args();
	$callback=array_shift($arguments);
	//var_dump($callback);
	//var_dump($arguments);
	if(isset($callback['require_once'])){require_once $callback['require_once'];}
	if(isset($callback['data'])){array_unshift($arguments,$callback['data']);}
	// security: only callback function with "callback" in their name
	if(strpos($callback['function'],'callback')===false){dlib_permission_denied_403();}
	return call_user_func_array($callback['function'],$arguments);
}

//! Data used in the HTML selector interface
//! The GUI allows a user to graphically determine part of a web page (or of a list of pages) 
//! The result is a "selector" (a list of XPATH, CSS and REGEXP operations recognized by dlib_html_apply_selectors())
//! This data is serialized and stored in tmp file
class HtmlSelectorEditor
{
	//! A list of operations recognized by dlib_html_apply_selectors()
	var $selector;
	//! A hash that uniquely identifies the GUI for this selector
	var $id;
	//! A list of html pages: [['url'=>'https://example.org/123','html'=>'<div></div>'],['url'=>'...','html'=>'<div></div>'...]]
	var $pages;
	
	//! Optional callback that will change things in displayed html
	var $displayCallback=false;
	//! Function that is called when user has finished building selector.
	var $finishedCallback;
	//! Optional, customizable texts that will be displayed in interface.
	var $texts;
	//! Optionaly add extra electors after user determined selector
	var $appendSelectors=[];

	//! Load existing HtmlSelectorEditor or create new one
	static function create($selector,$urls,$finishedCallback,$options=[])
	{
		require_once 'dlib/download-tools.php';
		global $html_selector_config;

		if(!isset($options['title'])){fatal('HtmlSelectorEditor::create: no title');}
		if($selector===false){$selector=[];}

		// *** Load if it already exists
		$id=hash('sha256',session_id().':'.$options['title'].':'.serialize($urls));
		$res=self::load($id);
		if($res!==false)
		{
			$res->selector=$selector;
			$res->save();
			return $res;
		}
		
		// *** This is a new editor, create it
		$res=new HtmlSelectorEditor();

		$res->texts=['title'       =>val($options,'title'),
					 'tab-selector'=>val($options,'tab-selector',
										 t('Choose area')),
					 'main-help'   =>val($options,'main-help',
										 t('Click inside the area of a page you want to choose. Then click on the "grow" button until you get a valid selection that is large enough to fit all of the area of interest. You can check the extracted area in the "Selected result" tab. Don\'t forget to save.')),
					];
		if($res->texts['title']===false){fatal('HtmlSelectorEditor::create: no title');}
	
		$res->displayCallback =val($options,'display-callback');
		$res->finishedCallback=$finishedCallback;

		$res->appendSelectors =val($options,'append-selectors',[]);

		$res->id=$id;
		$res->selector=$selector;
		$res->pages=[];
		foreach($urls as $url)
		{
			dcomponent_progress_log("downloading :".$url);
			require_once 'demosphere-safe-domain.php';
			$page['url']=$url;
			$page['html']=dlib_download_and_clean_html_page($url,false);
			// We need to proxy non-https CSS, otherwise browser will block mixed content
			$page['html']=preg_replace_callback('@(<link[^>]*href\s*=\s*["\'])([^"\' ]+)([^>]*)@i',function($m)
												{
													if(!preg_match('@\bcss\b@i'       ,$m[0]) && 
													   !preg_match('@\bstylesheet\b@i',$m[0])    ){return $m[0];}
													$url=$m[2];
													if(preg_match('@^//@',$url)){$m[1].'https:'.$m[2].$m[3];}
													return $m[1].demosphere_safe_domain_proxy_create_remote_url($m[2]).'?fix-css-urls'.$m[3];
												},$page['html']);
			
			$res->pages[]=$page;
		}
		$res->save();
		return $res;
	}

	function save()
	{
		global $html_selector_config;
		$fname=$html_selector_config['tmp_dir'].'/html-selector-editor-'.$this->id;
		return file_put_contents($fname,serialize($this));
	}

	static function load($id)
	{
		global $html_selector_config;
		$fname=$html_selector_config['tmp_dir'].'/html-selector-editor-'.$id;
		if(!file_exists($fname)){return false;}
		return unserialize(file_get_contents($fname),['allowed_classes'=>['HtmlSelectorEditor']]);
	}

	//! This is Ajax-called when the user has entered a new selector.
	//! The selector is saved for future calls to display test pages.
	//! This function returns urls to the safe-domain test pages. The urls are changed
	//! to force refresh. 
	function refreshTestPages()
	{
		require_once 'dlib/html-tools.php';
		require_once 'dcomponent/dcomponent-common.php';

		$selector=json_decode($_POST['selector'],true);

		// save selector for future calls to display test pages.
		$this->selector=$selector;
		$this->save();

		// Compute urls for test page iframes (and also raw html for selector debuging)
		$resPages=[];
		foreach($this->pages as $nb=>$page)
		{
			$iframeUrl=dcomponent_safe_domain_url('html-selector/editor/'.$this->id.'/safe-html?'.
												  'page='.intval($nb).'&'.
												  'testSelector'.'&'.
												  'refresh='.md5(serialize($selector)));
			$html=$page['html'];
			$allSelectors=array_merge($this->selector,$this->appendSelectors);
			$html=dlib_html_apply_selectors($html,$allSelectors);
			$resPages[]=['url'=>$iframeUrl,'html'=>$html];
		}
		// a text version of this selector just for display
		$textSel=[];
		foreach($selector as $line){$textSel[]=implode(':',$line);}
		$textSel=implode(' ; ',$textSel);
		if(count($this->appendSelectors)){$textSel.=' (and additional selectors)';}

		// return urls
		return ['pages'=>$resPages,'selector'=>$textSel];
	}
	
}


//! This runs a heuristic algorithm to determine a CSS selector that selects the first node in $path on all pages.
//! The returned selector will match a unique element on all pages.
//! This function can guess simple selectors (#example). 
//! It can also find parent selectors if they are needed to make selector unique (#main article)
//! $pages : Example [['html'=>'<div>...</div>'],['html'=>'<div>...</div>'],['html'=>'<div>...</div>']]
//!    Each page can also have:
//!      'cacheName' : allows html_selector_check_is_unique_selector_for_pages() to cache results (big speedup)
//!      'doc' and 'xpath' : DOMDocument and its DOMXPath (if not present, will be created)
//! $path: Example: [['tagName'=>'article','className'=>'post'],['tagName'=>'div','id'=>'main'],['tagName'=>'div']]
function html_selector_node_path_to_selector_for_pages($pages,$path,$dbg=false)
{
	require_once 'dlib/html-tools.php';
	$dbg=false;

	// Create doc & xpath for each page
	if(!isset(dlib_first($pages)['doc']))
	{
		foreach(array_keys($pages) as $k)
		{
			try 
			{
				$pages[$k]['doc'  ]=dlib_dom_from_html($pages[$k]['html'],$xpath);
				$pages[$k]['xpath']=$xpath;
			}
			catch (Exception $e) 
			{
				// FIXME: this error condition is not handled (displayed on client)
				return ['selector'=>false,'error'=>'dlib_dom_from_html failed'];
			}
		}
	}

	// Valid HTML docs have <html> and <body>. In that case remove <html>
	$tagNames=dlib_array_column($path,'tagName');
	if(count($path)>3 && $tagNames[count($path)-3]=='body' && $tagNames[count($path)-2]=='html')
	{
		unset($path[count($path)-2]);
	}
	if($tagNames[count($path)-1]=='#document')	
	{
		unset($path[count($path)-1]);
	}
	$path=array_values($path);
	
	
	// Algorithm to determine a CSS selector.
	// Start from the target node ($path[0]) and move up the node path until we find a node  ($path[$bestUnique]) that has a unique selector.
	// Start again from the target node and move up to the previously found node.
	// Repeat until we build a unique selector the target node, or that no unique selector is found.
	// Each pass adds an element to $selector
	// During each pass we test selectors for id, class, and tagName
	// $bestUnique=0 means we're over
	$selector='';
	$found=false;
	for($bestUnique=count($path);$bestUnique>0;)
	{
		// find the first pathPart that is unique on all pages
		if($dbg)echo "**** Searching at : ".ent($selector)." (try bestUnique=$bestUnique)<br/>\n";
		$found=false;
		for($n=0;$n<$bestUnique;$n++)
		{
			$pathPart=$path[$n];
			if($dbg)echo "part:$n<br/>\n";
			$tests=[];
			$isUniqueTagName=array_search($pathPart['tagName'],['body','html'])!==false;
			// add test for id
			if(isset($pathPart['id']) && $pathPart['id']!=='' && !$isUniqueTagName){$tests[]='#'.$pathPart['id'];}
			// add tests for each class
			if(isset($pathPart['className']) && $pathPart['className']!=='' && !$isUniqueTagName)
			{
				foreach(preg_split('@\s+@',$pathPart['className']) as $class)
				{
					$tests[].='.'.$class;
				}
			}
			// add test for tagName
			$tests[]=$pathPart['tagName'];
			if($dbg){echo"tests:".ent(implode(' ; ',$tests))."\n";}
			// Check if adding each one of the tests to the current selector results in
			// one unique selected element for all pages.
			foreach($tests as $test)
			{
				$sel=trim($selector.' '.$test);
				if($dbg)echo "doing test:".ent($test)." ::: ".ent($sel)."<br/>\n";
				if(html_selector_check_is_unique_selector_for_pages($pages,$sel,$dbg))
				{
					if($dbg)echo "found unique: $sel<br/>\n";
					$selector=$sel;
					$bestUnique=$n;
					$found=true;
					break;
				}
				else{if($dbg){echo "not unique<br/>\n";}}
			}
		}
		// we failed if we were not able to uniquely identify any path part in current iteration
		if(!$found){break;}
	}
	if(!$found){$selector=false;}
	if($dbg)echo "selector:".ent($selector)."<br/>\n";

	return ['selector'=>$selector];
}

//! Check if the given selector identifies one (and only one) element on each page.
//! This is used in the heuristic algorithm to determine a good selector.
function html_selector_check_is_unique_selector_for_pages($pages,$selector,$dbg)
{
	static $cache=[];
	$cacheName=val(dlib_first($pages),'cacheName');
	if($cacheName!==false && isset($cache[$cacheName][$selector])){return $cache[$cacheName][$selector];}
	
	$res=true;
	foreach($pages as $n=>$page)
	{
		$xpathSel=dlib_css_selector_to_xpath($selector);
		$selected=$page['xpath']->evaluate($xpathSel);
		if($selected===false){fatal('invalid xpath');}
		if($dbg)echo "sel:'".ent($selector)."' page:$n nb:".$selected->length."<br/>\n";
		if($selected->length!==1){$res=false;break;}
	}
	if($cacheName!==false){$cache[$cacheName][$selector]=$res;}
	return $res;
}


function html_selector_config_check_fast()
{
	global $html_selector_config;

	if(!isset($html_selector_config)){fatal("html_selector_config_check_fast: no config!");}

	if(!isset($html_selector_config['tmp_dir']))
	{fatal("html_selector_config_check_fast: tmp_dir not set");}

}

?>