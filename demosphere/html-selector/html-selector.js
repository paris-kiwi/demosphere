//! Javascript for the html_selector editor form.
$(document).ready(function()
{
	var aofa=array_of_arrays('#edit-selector',
							 {
								 rowFormUseTable:false,
								 text:{newRow   :t('newRow')   ,
									   addRow   :t('addRow')   }
							 });

	$('.raw-html-button').click(function(){$(this).parents('div').eq(0).find('.raw-html').toggle();resize_frames();});
	
	// simple emulation of jquery ui tabs
	$("#tabs a").click(function(e){e.stopPropagation();e.preventDefault();});
	$("#tabs a").mousedown(function(e)
	{
		e.stopPropagation();
		e.preventDefault();
		var id=$(this).attr('href');
		$("#tabs li").removeClass('active');
		$(this).parent().addClass('active');
		$("#tabs a").each(function()
		{
			var oid=$(this).attr('href');
			if(oid===id){$(oid).addClass(   'active');}
			else		{$(oid).removeClass('active');}
		});
		resize_frames();		
	});


	$('#open-selector-aofa').text($('#selector-form').hasClass('closed-aofa') ? '+' : '-');
	$('#open-selector-aofa').click(function()
	{
		$('#selector-form').toggleClass('closed-aofa');
		$(this).text($('#selector-form').hasClass('closed-aofa') ? '+' : '-');
		resize_frames();
	});

	window.addEventListener("message",function(e){receive_iframe_messages(e,aofa);}, false);

	$(document).on('aofachanged',function(e){refresh_test_pages(aofa);});

	// wait for iframe ready message
	$('#current-selector').addClass('waiting');

	refresh_test_pages(aofa);
});

$(window).load(function()
{
	resize_frames();
	$(window).resize(function(){resize_frames();});
});

function resize_frames()
{
	var iframe=$('.tab-body.active iframe');
	$('.tab-body.active iframe').height($(document.body).height()-iframe.offset().top-5);
}

//! Refresh the contents of the "test page" tabs, for example after a new selector has been entered.
//! An ajax request is used to fetch new "src" urls for each iframe.
function refresh_test_pages(aofa)
{
	aofa.updateJsonData();
	$.post(html_selector_config.script_url+'/editor/'+window.location.href.match(/\/editor\/([0-9a-z]+)([\/?]|$)/)[1]+'/refresh-test-pages',
		   {selector: $(aofa.formItem).val()},
		   function(data)
		   {
			   var i=0;
			   $('.test-page').each(function()
			   {
				   var page=data.pages[i];
				   $(this).find('iframe').attr('src',page.url);
				   $(this).find('.raw-html').text(page.html);
				   $(this).find('.page-selector').text(data.selector);
				   i++;
			   });
		   },'json');
}

//! Receive a message from the "Find article" iframe.
//! When user clicks on an element inside the "Find article" tab, the js inside the 
//! tab builds a full description of the path of the clicked element. For security reasons
//! the iframe can't call functions in this js. So it uses a message, sending the json encoded
//! path.
function receive_iframe_messages(e,aofa)
{
	//console.log("main window receive_iframe_messages:",e.origin,e.data,e);
	var std_base_host=dcomponent_config.safe_base_url.replace(/^(https?:\/\/[^\/]*)\/.*$/,'$1');
	if (e.origin != std_base_host){return;}

	// Wait for iframe to tell us it is ready.
	// Then give it setup information (initial selector, options)
	if(e.data.indexOf('ready:')==0)
	{
		$('#current-selector').removeClass('waiting');
		var aofaCss=selector_aofa_get_main_css(aofa,false);
		if(aofaCss!==false)
		{
			$('#current-selector').text(aofaCss.text());
			$('#tab-selector iframe')[0].contentWindow.postMessage(JSON.stringify(
				{
					initialSelector: aofa!==false ? aofaCss.text() : ''
				}),dcomponent_config.safe_base_url);
		}
	}

	if(e.data.indexOf('selectpath:')!==0){return;}
	var path=JSON.parse(e.data.substring('selectpath:'.length));
	//console.log('received:',path);

	var current=$('#current-selector');

	current.removeClass('error').html('&nbsp;');

	current.addClass('waiting');
	$.post(html_selector_config.script_url+'/editor/'+window.location.href.match(/\/editor\/([0-9a-z]+)([\/?]|$)/)[1]+'/guess-selector',
 	       {
			   path: JSON.stringify(path)
		   },
		   function(data)
		   {
			   if(data.selector!==false)
			   {
				   // The selector is ok. Remove error messages/colors and show test pages.
				   current.removeClass('waiting error');
				   current.text(data.selector);
				   animate_bgcolor(current,"ffff00","eeeeee",{duration:1000},true);
				   var aofaCss=selector_aofa_get_main_css(aofa);
				   aofaCss.text(data.selector);
				   refresh_test_pages(aofa);
			   }
			   else
			   {
				   // the selector is NOT ok. Show error messages/colors.
				   current.text(t('failedSelector'));
				   current.removeClass('waiting');
				   current.addClass('error');
			   }
		   },'json');
}

//! Returns jQuery list with the <td> of the first CSS selector in aofa table.
//! If no CSS selector is found it will either create a row (if create===true), otherwise it will return false.
function selector_aofa_get_main_css(aofa,create)
{
	var res=false;
	aofa.table.find('tr td:nth-child(4)').each(function()
	{
		if($(this).text()===aofa.columns[0].values.CSS){res=$(this).next();return false;}
	});
	if(res===false && (typeof create==='undefined' || create))
	{
		var tr=aofa.createNewRow();
		tr.find('td:nth-child(4)').text(aofa.columns[0].values.CSS);
		tr.find('td:nth-child(5)').text('body');
		res=tr.find('td:nth-child(5)');
	}
	return res;
}
