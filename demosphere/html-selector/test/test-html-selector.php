<?php

function test_html_selector()
{
	test_html_selector_node_path_to_selector_for_pages();
}

function test_html_selector_node_path_to_selector_for_pages()
{
	require_once 'dlib/testing.php';
	require_once 'demosphere/html-selector/html-selector.php';

	$pages=[['html'=>'<div><div id="main"><article>z</article></div><div id="comments"><article>c</article></div><div>'],
			['html'=>'<div><div id="main"><article>z</article></div><div>'],
		   ];
	$res=html_selector_node_path_to_selector_for_pages($pages,[['tagName'=>'article'],
															   ['tagName'=>'div','id'=>'main'],
															   ['tagName'=>'div'],
															  ],true);
	test_equals($res,['selector'=>'#main article']);

}


?>