<div id="configureContentSelector">
<? /* Get/create the data for this feed.
    configureContentSelectorData can produce output text. We create a div that
    will be hidden as soon as the output is over. */ ?>
<div id="fetch-page-output">
	FIXME (used to be output of page download)
</div>
<? // hide the output text ?>
<script type="text/javascript">
	document.getElementById("fetch-page-output").style.display="none";
</script>

<? // Extra information (path details table) and description (help text) ?>
<p class="main-help">$editor->texts['main-help']</p>

<? // Tab buttons ?>
<ul id="tabs">
	<li class="active"><a href="#tab-selector">$editor->texts['tab-selector']</a></li>
	<? foreach($editor->pages as $nb=>$page){  ?>
		<li><a href="#tab-test-<?: $nb+1 ?>">
			_(Selected)<?: count($editor->pages)>1 ? ' '.($nb+1) : '' ?>
		</a></li>
	<?}?>
</ul>

<? // Main tab ("Find Article") ?>
<div id="tab-selector" class="tab-body active">
	<?= $form ?>
	<iframe src="$iframeUrl" >_(error displaying iframe)</iframe>
</div>

<? // Tabs for test pages ?>
<? // (that display results of applying selector on several pages of this feed) ?>
<? foreach($editor->pages as $nb=>$page){ ?>
	<div id="tab-test-<?: $nb+1 ?>" class="tab-body test-page">
		(<a href="$page['url']" target="_blank">_(original page)</a> , 
		<a href="javascript:void(0);" class="raw-html-button">_(raw html)</a>)
		&nbsp;&nbsp;selector:<span class="page-selector"></span><br/>
		<div class="raw-html" style="display:none"></div>
		<iframe src="" class="" >_(no display)</iframe>
	</div>
<?}?>
</div>

