<?php

function demosphere_event_send_by_email_form($id)
{
	global $base_url,$currentPage,$demosphere_config;

	$event=Event::fetch($id,false);
	if($event===null){dlib_not_found_404();}
	if(!$event->access('view')){dlib_permission_denied_403();}

	$currentPage->title=t('Email an event to someone');

	$currentPage->bodyClasses[]="notFullWidth";
	$currentPage->addCssTpl("demosphere/css/demosphere-event-send-by-email.tpl.css");

	$formOpts=[];
	$items=[];
	$items['event']=['type'=>'data','data'=>$event];

	$items['title']=
		['html'=>'<h2 class="send-by-email">'.t('Send an email with this event to somebody:').'</h2><div id="send-by-email-required-inputs">'];
	$items['recipients']=
		['type'=>'textfield',
		 'title'=>t('recipient\'s address'),
		 'size'=>40,
		 'required'=>	true,
		 'attributes'=>['autocomplete'=>'email'],
		];
	$items['sender-email']=
		['type'=>'email',
		 'check-dns'=>true,
		 'title' => t('your adress'),
		 'required'=>	true,
		 'size'  => 40,];
	
	$items['submit']=
		['type'=>'submit',
		 'value'=>t('Send the email'),
		 'redirect'=>$event->url(),
		];
	
	$items['optional'  ]=
		['html'=>
		 '</div><p id="send-by-email-optional">'.
		 t('if you want, you can also fill in the following fields :').'</p>'];
	
	$items['sender-name' ]=
		['type'=>'textfield',
		 'title' =>	 t('your name'),
		 'size'=>	40,];
	
	$items['message']
		=['type'=>'textarea',
		  'title'=>t('a message'),
		  'rows'=>5,];

	$items['example-title']=
		['html'=>'<h3 id="example-title">'.t('The email that will be sent will look like this').'</h3>'];

	require_once 'dlib/template.php';
	require_once 'demosphere-event-map.php';
	require_once 'demosphere-date-time.php';
	$items['example-body'] = 
		['html'=>
		 template_render('templates/demosphere-event-send-by-email-html.tpl.php',
						 [compact('base_url','event'),
						  'site_name'=>$demosphere_config['site_name'],
						  'isSampleMail'=>true,
						  'event'=>$event,
						  'senderEmail'=>t('your.address@example.org'),
						  'senderName'=>t('[your name]'),
						  'message'=>t('[the message]'),
						  'html_message'=>t('[the message]'),
						  'nameOrEmail'=>t('[your name]'),
						  'place'=>$event->usePlace(),
						  'city'=>$event->usePlace()->getCityName(),
						  'map_url'=>demosphere_get_map_url($event->usePlace(),$event),
						 ])];
	$items['private'  ]=
		['html'=>'<p id="send-by-email-private">'.
		 t('We respect privacy concerns and we will not keep any email addresses '.
		   'or use them for any other purpose.').'</p>'];
	$items['spamcheck'] =['type'=>'spamcheck',
						  'spamcheck-delay'=>2];

	$items['submit1']=['type'=>'submit',
					   'value'=>t('Send the email'),
					   'redirect'=>$event->url(),];

	$formOpts['id']='send-by-email-form';
	$formOpts['validate']='demosphere_event_send_by_email_form_validate';
	$formOpts['submit'  ]='demosphere_event_send_by_email_form_submit';
	require_once 'dlib/form.php';
	return form_process($items,$formOpts);
}

function demosphere_event_send_by_email_get_recipients($recipients)
{
	$recipients = trim($recipients);
	$recipients = str_replace(["\r\n", "\n", "\r"], ',', $recipients);
	$recipients = str_replace(' ', '', $recipients);
	$recipients = explode(',', $recipients);
	foreach(array_keys($recipients) as $k)
	{
		$recipients[$k]=trim($recipients[$k]);
	}
	return $recipients;
}

function demosphere_event_send_by_email_form_validate(&$items)
{
	$event=$items['event']['data'];
	$values=dlib_array_column($items,'value');
	// ** recipients
	$recipients=demosphere_event_send_by_email_get_recipients($values['recipients']);
	foreach($recipients as $email)
	{
		$v=form_validate_email($email,['check-dns'=>true]);
		if($v!==null)
		{
			$items['recipients']['error']=val($items['recipients'],'error','').'<br/>'.$v;
		}
	}

	// Avoid spamming (too many emails sent from same IP)
	require_once 'dlib/mail-and-mime.php';
	$ip=$_SERVER['REMOTE_ADDR'];
	if((dlib_throttle_count('event-send-by-email::'.$ip,3600*24)+count($recipients))>4)
	{
		$items['recipients']['error']=t('Please do not send a large number of emails with this service.');
	}
	
	$ok=true;
	$msg='';
	require_once 'dlib/mail-and-mime.php';
	$ip=$_SERVER['REMOTE_ADDR'];
	if(dlib_check_ip_in_stopforumspam($ip)===true){$ok=false;$msg.=':stopforumspam';}
	$dnsbl=dlib_check_dnsbl($ip);
	if($dnsbl!==false){$ok=false;$msg.=':'.$dnsbl;}
	if(array_search($ip,['95.215.0.27','195.2.240.106','46.161.41.34','46.161.41.32','94.242.246.24','31.172.30.3'])!==false)
	{
		$ok=false;
		$msg.=':bad-ip-list';
	}

	if(!$ok)
	{
		dlib_message_add(t('Your IP address is not allowed to send emails.'),'error');
		demosphere_event_send_by_email_log($event->id,$values,'rejected:'.$msg);
	}

	return $ok;
}

function demosphere_event_send_by_email_form_submit($items,$formOptions,$activeButton)
{
	global $base_url,$demosphere_config;
	$values=dlib_array_column($items,'value');
	$event=$items['event']['data'];

	require_once 'dlib/template.php';
	require_once 'lib/class.phpmailer.php';
	require_once 'demosphere-event-map.php';

	$recipients=demosphere_event_send_by_email_get_recipients($values['recipients']);

	$values['message']=trim($values['message']);

	require_once 'demosphere-feeds.php';
	$calendar=demosphere_feeds_icalendar_object($event);
	$calendar->setMethod("REQUEST");
	$ics=$calendar->createCalendar();

	foreach($recipients as $to) 
	{
		require_once 'dlib/mail-and-mime.php';
		$ip=$_SERVER['REMOTE_ADDR'];
		dlib_throttle_add('event-send-by-email::'.$ip);

		$mail=new PHPMailer();
		$mail->CharSet="utf-8";
		$mail->SetFrom($demosphere_config['email_subscription_from']);
		$mail->AddReplyTo($values['sender-email']);
		$mail->AddAddress($to);
		$mail->Subject=$values['sender-name']!=='' ? 
			t('!name has sent you an event from !site',
			  ['!name' => $values['sender-name'],
			   '!site' => $demosphere_config['site_name'],
			   '!date' => demos_format_date('shorter-date-time',$event->startTime),
			   '!title'=> $event->title,
			  ])	  :
			t('an event has been sent to you from !site',
			  ['!site' => $demosphere_config['site_name'],
			   '!date' => demos_format_date('shorter-date-time',$event->startTime),
			   '!title'=> $event->title,
			  ]
			  );

		$vars=[
			'base_url'=>$base_url,
			'site_name'=>$demosphere_config['site_name'],
			'isSampleMail'=>false,
			'event'     =>$event,
			'senderEmail'   =>$values['sender-email' ],
			'senderName'    =>$values['sender-name'  ],
			'message'  =>$values['message'],
			'html_message'  =>str_replace(["\n\r","\n","\r"],
										  ["<br/>","<br/>","<br/>",],
										  $values['message']),
			'nameOrEmail'=>$values['sender-name']!=='' ? $values['sender-name'] : $values['sender-email'],
			'place'=>$event->usePlace(),
			'city'=>$event->usePlace()->getCityName(),
			'map_url'=>demosphere_get_map_url($event->usePlace(),$event),
								
			  ];
		$html=template_render('templates/demosphere-event-send-by-email-html.tpl.php',$vars);
		$text=template_render('templates/demosphere-event-send-by-email-text.tpl.php',$vars);
		$text=html_entity_decode($text,ENT_QUOTES,"UTF-8");

		$mail->MsgHTML($html);
		$mail->AltBody = $text;
		$mail->AddStringAttachment($ics,'event-'.$event->id.'.ics','base64','text/calendar');

		//var_dump($mail);die('oo');
		if(!$mail->Send()) 
		{
			dlib_message_add(t("Error while sending to @to.",['@to'=>$to])."<br/>".
							   ent($mail->ErrorInfo),'error');
		}
		else 
		{
			dlib_message_add(t("Event successfully sent to @to",['@to'=>$to]));
		}
		demosphere_event_send_by_email_log($event->id,$values,'ok',$to);
	}
	dlib_message_add(t("Thanks for helping us spread Démosphère events !"));
}

function demosphere_event_send_by_email_log($eventId,$values,$status,$to='')
{
	global $demosphere_config;
	if(is_writable('/var/local/demosphere/send-mail-spam-check.log'))
	{
		file_put_contents('/var/local/demosphere/send-mail-spam-check.log',
						  '##-##-# '.$_SERVER['REMOTE_ADDR'].' '.date('r').' '.$demosphere_config['site_id'].' '.$status.' '.$eventId.' '.$to."\n".
						  'email:'.  $values['sender-email']."\n".
						  'name:'.   $values['sender-name' ]."\n".
						  'message:'.$values['message'     ]."\n",
						  FILE_APPEND);
	}
}


?>