<?php

//! $dcomponent_config : configuration for dcomponent.
//! List of $dcomponent_config keys:
//! - dir_url
//! - safe_base_url
//! -::-;-::-
$dcomponent_config;

function dcomponent_page_css_and_js($isItemList=true)
{
	global $dcomponent_config,$currentPage;

	// Firefox sometimes (?) incorrectly restores frame contents after reload. This forces reset.
	// All pages in dcomponents have dynamic content, so caching is not usefull.
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");

	$currentPage->addJs('lib/jquery.js');
	$currentPage->addJsConfig('dcomponent',['safe_base_url']);
	$currentPage->bodyClasses[]='dcomponent';
	$currentPage->addCssTpl('demosphere/dcomponent/dcomponent-common.tpl.css');

	if($isItemList)
	{
		$currentPage->addJs    ('demosphere/dcomponent/dcomponent-dcitems.js');
		$currentPage->addCssTpl('demosphere/css/demosphere-highlight.tpl.css');
		$currentPage->addJs    ('demosphere/js/demosphere-event-near-date.js');
		$currentPage->addCssTpl('demosphere/text-matching/text-matching-info.tpl.css');
		$currentPage->addJsTranslations(['Are you sure you want to un-finish this item?'=>t('Are you sure you want to un-finish this item?')]);
	}

	$logBox=dcomponent_progress_log_box();
	if($logBox!=''){$currentPage->dcomponentLogBox=$logBox;}
}

//! Begin an operation that wants to display its progress.
//! Several output options are available: 
//! * html: log messages are sent to an empty html page. 
//!         the program should redirect on dcomponent_progress_log_end
//!         towards a page that will redisplay the log messages in a box.
//! * message: log messages are displayed with dlib_message_add 
function dcomponent_progress_log_start($type='html')
{
	global $dcomponent_progress_log_info;
	if(isset($dcomponent_progress_log_info)){fatal("dcomponent_progress_log_start: already started");}

	$dcomponent_progress_log_info=
		['type'      =>$type,
		 'messages'  =>[],
		 'boxDisplay'=>'simple'];
	
	switch($type)
	{
	case 'html':
		echo '<html><head>'.
			'<style type="text/css">'.
			'.progress-log{white-space: pre-wrap;font-family: monospace;padding-left:2em;}'.
			'.progress-log.error{background-color: #fcc;}'.
			'.progress-log.warn {background-color: #220;}'.
			'</style>';
			'</head><body>';
			break;
	case 'text':
	case 'messages':
		break;
	default: fatal('dcomponent_progress_log_start: invalid type');
	}
}

//! Display a log message according to type chosen in dcomponent_progress_log_start()
//! $isRawHtml: 
function dcomponent_progress_log($message,$level='status',$isRawHtml=false)
{
	global $dcomponent_progress_log_info;
	// if progress log is not started, this is text output
	if(!isset($dcomponent_progress_log_info)){dcomponent_progress_log_start('text');}

	$info=&$dcomponent_progress_log_info;

	switch($info['type'])
	{
	case 'text':
		if($level!=='status'){echo $level.":\n";}
		echo $message."\n";
		break;
	case 'html':
		$info['messages'][]=['message'=>$message,'level'=>$level,'isRawHtml'=>$isRawHtml];
		echo '<div class="progress-log '.$level.'">'.($isRawHtml ? $message : ent($message)).
			"</div>".
			'<script>window.scrollTo(0,document.body.scrollHeight);</script>';
		for($i=0;$i<10000;$i++){echo " ";};
		flush();
		break;
	case 'messages':
		dlib_message_add($message,$level);
		break;
	}
}

function dcomponent_progress_log_end($options=[])
{
	global $dcomponent_progress_log_info;
	if(!isset($dcomponent_progress_log_info)){fatal("dcomponent_progress_log: not started");}

	$dcomponent_progress_log_info= 
		array_merge($dcomponent_progress_log_info,
					array_intersect_key($options,array_flip(['boxDisplay'])));

	// redirect to url, add information so that target page will show also show the messages
	if(isset($options['redirect']))
	{
		$id=mt_rand();
		$_SESSION['progress-log-'.$id]=$dcomponent_progress_log_info;
		$url=$options['redirect'];
		$url=$url.(strpos($url,'?')===false ? '?' : '&').'progress-log='.$id;
		dcomponent_js_redirect($url);
	}
}
function dcomponent_progress_log_box()
{
	$id=val($_GET,'progress-log');
	if($id===false){return '';}
	$info=val($_SESSION,'progress-log-'.$id);
	if($info===false){return '';}
	$out='';
	if($info['boxDisplay']==='fold')
	{
		$out.='<span id="progress-log-button" onmousedown="$(\'#progress-log-box\').toggle();">+/- '.t('log').'</span>';
	}
	$out.='<div id="progress-log-box" class="'.$info['boxDisplay'].' progress-box-pre">';
	foreach($info['messages'] as $m)
	{
		$out.='<div class="progress-log '.$m['level'].'">'.($m['isRawHtml'] ? $m['message'] : ent($m['message']))."</div>";
	}
	$out.='</div>';
	unset($_SESSION['progress-log-'.$id]);
	return $out;
}

function dcomponent_js_redirect($url)
{
	echo '<script type="text/javascript">//<![CDATA['."\n".
		'window.location="'.$url.'";'."\n".
		'//]]>'."\n".
		'</script>';
	echo 'redirecting to <a href="'.ent($url).'">'.ent($url).'</a>...';
	exit(0);
}

function dcomponent_current_url()
{
	$pageURL = (val($_SERVER,'HTTPS') == "on") ? "https://" : "http://";
	if ($_SERVER["SERVER_PORT"] != "80")
	{
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} 
	else 
	{
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}

//! Add javascript and css used inside frames in feed-import, mail-import, page-watch
//! Returns headers to be inserted in page <head>
//! $csp: 
//!   mail-import and feed-import use Content-Security-Policy. Inline js is not allowed.
//!   $csp can be false (disable csp), true (enable csp, std options), or 'allow-inline-css'
function dcomponent_dcitem_inside_frame_js_and_css($dcompName,$csp=false)
{
	global $base_url,$currentPage;

	//$page=new HtmlPageData();

	$currentPage->addCssTpl('demosphere/css/demosphere-highlight.tpl.css');
	// CSP breaks dynamic <script> loading so we set 'use-compat'=>false, and abandon old browsers. We could workaround, but it's not worth it.
	$currentPage->addJs    ('demosphere/js/demosphere-event-near-date.js' ,'file',['use-compat'=>false]);
	$currentPage->addJs    ('demosphere/dcomponent/dcitem-inside-frame.js','file',['use-compat'=>false]);

	// Content-Security-Policy
	// Enforce blocking of external requests with Content-Security-Policy:
	// https://developer.mozilla.org/en-US/docs/Security/CSP/Introducing_Content_Security_Policy
	// Note that the iframe that displays this page also has a sandbox attribute (experimental)
	// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe
	if($csp!==false)
	{
		global $demosphere_config;

		// Urls that this page is allowed to access
		$selfUrls=[
			         $demosphere_config['std_base_url'],
					 $demosphere_config['safe_base_url'],
				  ];

		$policy='';
		// "default" security policy: only allow connections to same site and sites listed in $selfUrls
		// "default" can be overriden by other Content-Security-Policy directives (style-src, ...)
		$policy.="default-src 'self' ".implode(' ',$selfUrls).'; ';
		if($csp==='allow-inline-css')
		{
			// "style-src" overrides "default-src" for CSS
			// "unsafe-inline" allows inline CSS, which may lead to js execution (?) ... but XSS isn't a problem on safe-domain.
			// "default-src" still blocks any requests outside of our domain. So there should be no privacy issues with spammers (Message).
			// For example: background-image requests in Message are blocked (actually tested)
			$policy.="style-src 'self' 'unsafe-inline' ".implode(' ',$selfUrls);
		}

		if(headers_sent()){fatal('Headers already sent, so we can\'t add Content-Security-Policy. Aborting.');}
		header('Content-Security-Policy: '.$policy);

		// reset 
		$res='';
		// When we use CSP, we can't use inline css or js (CSP version 2 allows it, using hashes. That might be nice.)
		$currentPage->addJs ('mail-import/js-config','file',['cache-checksum'=>false,'use-compat'=>false]);
		if($csp!=='allow-inline-css'){$currentPage->css=array_filter($currentPage->css,function($c){return $c['location']!=='inline';});}
		$currentPage->js =array_filter($currentPage->js ,function($c){return $c['location']!=='inline';});
		$res.=$currentPage->renderJs();
		$res.=$currentPage->renderCss();
	}
	else
	{
		// Page-watch does not use CSP.
		$res='';
		$currentPage->addJsConfig('demosphere',['std_base_url']);
		$vars=$currentPage->variables();
		$res.=$vars['css'];
		$res.=$vars['js'];
	}
	return $res;
}


function dcomponent_url_to_name($url,$uniqTable=false,$uniqCol='name')
{
	if(preg_match('@https://[^/]*\bfacebook\.[a-z]{2,6}/([^/?#]+)@',$url,$m))
	{
		$name=$m[1];
		$name=urldecode($name);
		$name=preg_replace('@-[0-9]{3,}$@','',$name);
	}
	else
	if(preg_match('@https://[^/]*\btwitter\.com/([a-zA-Z0-9_]{1,15})@',$url,$m))
	{
		$name=$m[1];
		$name=str_replace('_',' ',$name);
	}
	else
	{


		$name=$url;
		$name=preg_replace('@^https?://@','',$name);
		$name=preg_replace('@^www[.]@','',$name);
		$name=preg_replace('@/.*$@','',$name);
		$hostingPlatforms=['lautre[.]net','ouvaton[.]org',
						   'espiv[.]net','canalblog[.]com','espivblogs[.]net','squat[.]gr',
						   'wordpress[.]com','free[.]fr','over-blog[.][a-z]{2,3}',
						   'blogspot[.]com',
						  ];
		$name=preg_replace('@[.]('.implode('|',$hostingPlatforms).')$@i','',$name);
		if(mb_strlen($name)>22)
		{
			$name=preg_replace('@[.][a-z]{2,4}$@','',$name);
		}
	}
	$name=preg_replace('@[^\pL0-9.-]@iu','_',$name);
	$name=substr($name,0,40);

	// avoid same name : add number to name
	if($uniqTable!==false)
	{
		for($i=0;;$i++)
		{
			$check=$name.($i ? '-'.$i : '');
			$ct=db_result("SELECT COUNT(url) FROM `".$uniqTable."` WHERE ".$uniqCol."='%s'",
						  $check);
			if($ct==0){$name=$check;break;}
		}
	}
	return $name;
}


// *********** Interactions with demosphere *******/

//! Creates an event and redirects to its edit form.
function dcomponent_import_to_demosphere($title,$body,$dataSrc)
{
	require_once 'demosphere-event-import.php';
	demosphere_event_import_create_event($title,$body,$dataSrc);
}

// finds links to events inside contents
function dcomponent_find_demosphere_event_references($contents)
{
	global $demosphere_config;
	$nids=[];

	$r=preg_match_all('@('.preg_quote($demosphere_config['std_base_url'],'@').'/'.$demosphere_config['event_url_name'].'/([0-9]+)|'.
					  '\(Ref[.]([0-9]+)\))@',
					  $contents,$matches);
	if($r==0){return [];}		
	foreach(array_merge($matches[2],$matches[3]) as $nid)
	{
		if($nid!=''){$nids[]=intval($nid);}
	}
	$res=[];
	foreach(array_unique($nids) as $nid)
	{
		$res[$nid]=$demosphere_config['std_base_url'].'/'.$demosphere_config['event_url_name'].'/'.$nid;
	}
	return $res;
}

function dcomponent_tell_demosevents_we_found_reference($eventIds,$message)
{
	if(count($eventIds)==0){return;}
	$logs=db_one_col("SELECT id,log FROM Event WHERE id IN (".implode(',',array_map('intval',$eventIds)).")");
	foreach($logs as $id=>$log)
	{
		$entries=unserialize($log,['allowed_classes'=>false]);
		$entries[]=['ts'=>time(),'uid'=>0,'name'=>'mail','message'=>trim($message)];
		db_query("UPDATE Event SET log='%s' WHERE id=%d",serialize($entries),$id);
	}
}

// *********** These are wrappers around demosphere functions *******/
// They are not called directly to help keep track of dependencies on demosphere.
// Ideally, all dcomponents should be independent from demosphere.
// Making a dcomponent really independent means reimplementing / copying these functions.
// Note : 08/2016 : there are too many interactions between mail-import and Demosphere. We are no longer respecting this "proxy" idea.

function dcomponent_check_roles($roles)
{
	global $user;
	return $user->checkRoles($roles);
}
function dcomponent_safe_domain_url($b,$c=false)
{
	require_once 'demosphere-safe-domain.php';
	return demosphere_safe_domain_url($b,$c);
}
function dcomponent_config_check_file($a,&$b,$c)
{
	return demosphere_config_check_file($a,$b,$c);
}
function dcomponent_highlight($html,$moreInfo=false,$addStyle=true,$addJS=false)
{
	require_once 'demosphere-common.php';
	return highlight_future_dates_and_keywords($html,$moreInfo,$addStyle,$addJS);
}
function dcomponent_format_date($a,$b)
{
	require_once 'demosphere-date-time.php';
	return demos_format_date($a,$b);
}

function dcomponent_search_proxy_call_catch()
{
	require_once 'demosphere-search.php';
	$args=func_get_args();
	return call_user_func_array('demosphere_search_proxy_call_catch',$args);
}

function dcomponent_browser_download($url,$selector=false,$wait=0,$options=[])
{
	require_once 'demosphere-misc.php';
	return demosphere_browser_download($url,$selector,$wait,$options);
}

function dcomponent_browser_screenshot($url,$width=false,$height=false,$options=[])
{
	require_once 'demosphere-misc.php';
	return demosphere_browser_screenshot($url,$width,$height,$options);
}

function dcomponent_browser_screenshot_selected($url,$selector,$width=false,$height=false,$options=[])
{
	require_once 'demosphere-misc.php';
	return demosphere_browser_screenshot_selected($url,$selector,$width,$height,$options);
}

function dcomponent_url_clean_facebook($url)
{
	require_once 'demosphere-common.php';
	return demosphere_url_clean_facebook($url);
}

function dcomponent_url_remove_tracking($url)
{
	require_once 'demosphere-common.php';
	return demosphere_url_remove_tracking($url);
}

function dcomponent_url_remove_tracking_html($html)
{
	require_once 'demosphere-common.php';
	return demosphere_url_remove_tracking_html($html);
}

?>