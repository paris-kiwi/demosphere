
$(document).ready(function() 
{
	add_popup_event_to_highlighted_dates();

	// Lazy load iframes only when they are visibile to avoid overloading browser (and server).
	// Inspired by https://github.com/luis-almeida/unveil/blob/master/jquery.unveil.js
	var iframes=$(".dcitem iframe").not('iframe[src]');
	var win=$(window);
	var threshold=350;
	$(window).on('scroll',function()
	{
		// This is called on every scroll. It has to be fast.
		var removed=false;
		if(iframes.length)
		{
			var wtop = win.scrollTop();
			var wbot = wtop + win.height();
		}
		iframes.each(function()
		{
			var iframe=$(this);
			var ftop = iframe.offset().top;
			var fbot = ftop + iframe.height();
			if(fbot >= wtop - threshold && ftop <= wbot + threshold)
			{
				// add sandbox here to avoid error message in console (see dcitem.tpl.php)
				iframe.attr('sandbox',iframe.attr('sandbox')+" allow-same-origin");
				iframe.attr('src',iframe.attr('data-src'));
				removed=true;
			}
		});
		if(removed){iframes=$(".dcitem iframe").not('iframe[src]');}
	}).trigger('scroll');


	// "Prev/Next"  buttons that apears on  feed import, mail import and page watch pages.  
	$('.cycleButtons .prev,.cycleButtons .next').mousedown(function(event)
    {
		event.preventDefault();
		event.stopPropagation();
		var iframe=$(this).parents(".dcitem").eq(0).find(".cycleContainer").get(0);
		// To communicate with other window in iframe we need to use messages
		var ret=iframe.contentWindow.postMessage($(this).hasClass('prev') ? 'prev' : 'next', "*");
	});

	$(".dcitem .frame-resize-buttons a").click(dcomponent_frame_resize);
	$(".dcitem.single iframe").load(function()
	{
		this.contentWindow.postMessage("getheight:"+$(this).parents(".dcitem").attr("id"),"*");
	});
	window.addEventListener("message",function(e)
	{
		//console.log("gotresponse:",e.origin,e.data,e);

		// The iframe now has the "sandbox" attribute. So the origin of the message is now null. 
		// We cannot verify that the message is comming from that frame... 
		// but that is not a problem, since nothing senitive is being done with that data.
		// Anyways, the iframe's content is untrusted... so checking the origin provides no security: we know we are getting untrustworthy data.
		//var safe_base_host=dcomponent_config.safe_base_url.replace(/^(https?:\/\/[^\/]*)\/.*$/,'$1');
		//if (e.origin != safe_base_host){return;}

		// Sanity check (avoid js warnings)
		if(typeof e.data!=='string'){return;}

		// This parsing IS security sensitive. For example, if it were done incorrectly, the iframe could inject a malicious "id".
		var parse=e.data.match(/^dcitem-height:((message|article|page)[0-9]+):([0-9]+)$/);
		if(parse===null){return;}
		var id=parse[1];
		var h =parseInt(parse[3]);
		$("#"+id+" .main-line").css('height',(Math.max(h+50,265))+"px");
	}, false);

	// FIXME: mail import specific?
	$(".infoPart.privacy").click(function()
    {
		var iframe=$(this).parents(".dcitem").find("iframe");
		iframe.attr("src",iframe.attr("data-demospriv"));
	});

	// Display overflowing contents in a popup when user hovers on it.
	// This js adds/removes the popup class and wrapps the original in a span (overflow-box). 
	// It also sets positions, sizes and color so that the popup looks exactly like the original.
	$('.overflow-popup').hover(
		function()
		{
			var wantedWidth=this.scrollWidth;
			if(this.offsetWidth<this.scrollWidth)
			{
				var overflowEl=$(this);
				// The original element should not move: lock its current size before its content becomes "position: absolute".
				overflowEl.width (overflowEl.width ());
				overflowEl.height(overflowEl.height());
				// Wrap its contents in a span (only the first time)
				var box=overflowEl.children('.overflow-box');
				if(box.length===0)
				{
					overflowEl.wrapInner('<span class="overflow-box"/>');
					box=overflowEl.children('.overflow-box');
					
					// Hack to try to determine the background color (if this fails, you need to do it manually in CSS)
					var bgColor=false;
					$(overflowEl.parents().addBack().get().reverse()).each(function()
                    {
						var bg=$(this).css('background-color');
						if(bg!=='transparent' && !(/ *rgba\( *0 *, *0 *, *0 *, *0*\) */.test(bg)))
						{
							bgColor=$(this).css('background-color');
							return false;
						}
					});
					//console.log(bgColor);
					bgColor=bgColor.replace(/rgb(\( *[0-9]+ *, *[0-9]+ *, *[0-9]+ *)\)/,'rgba$1'+',.9)');
					//console.log(bgColor);
					box.css('background-color',bgColor);
				}
				overflowEl.addClass('popup');
				var overflowEloffset=overflowEl.offset();
				box.width(Math.min(wantedWidth+10,$(window).width()-overflowEloffset.left));
			}
		},
		function(){$(this).removeClass('popup');}
	);
	
	// hackish :-( See css .tmInfo comments for explanations.
	$('.dcitem .text-matching .tmInfo').css('max-width',$(window).width()-200); // need to do this first, otherwise maxWidth will be influenced by this el.
	var maxWidth=$('.dcitem .dc-middle').width();
	$('.dcitem .text-matching .tmInfo').css('max-width',maxWidth);
});

function dcomponent_finish_dcitem(button,id,finishUrl)
{
	var mdiv=$(button).parents('.dcitem,.message-list-compact tr');
	var finish=true;
	if(mdiv.hasClass('pending')){return;}
	if(mdiv.hasClass('finished'))
	{
		if(mdiv.hasClass('page') || !confirm(t('Are you sure you want to un-finish this item?'))){return;}
		finish=false;
	}
	mdiv.addClass('pending');
	$.post( finishUrl,{finish:finish},
			function(data)
			{
				if(data=="finished-ok-"+id)
				{
					mdiv.toggleClass('finished',finish);
					mdiv.removeClass('pending');
					mdiv.find("iframe").css("height","");
				}
				else{alert("finish-failed");}
			});
}

function dcomponent_frame_resize(e)
{
	e.preventDefault();
	e.stopPropagation();
	var dcitem=$(e.currentTarget).parents(".dcitem").eq(0);
	var mainLine=dcitem.find(".main-line");
	var h=mainLine.height();
	var minHeight=0;
	if(dcitem.find(".info-left li").length)
	{
		dcitem.find(".info-left li").last( ).offset().top+dcitem.find(".info-left li").last( ).outerHeight()-
		    dcitem.find(".main-line").offset().top;
	}
	var newHeight=$(e.currentTarget).text()=="+" ? 
					2*h : 
					Math.max(minHeight,Math.round(h/2));
	mainLine.height(newHeight);
}
