<?php
require_once 'demosphere-frontpage.php';

function demosphere_map()
{
	global $user,$base_url,$demosphere_config,$currentPage;

	$isLoggedIn=$user->id!=0;
	$isEventAdmin=$user->checkRoles('admin','moderator');

	$currentPage->addCssTpl('demosphere/css/demosphere-frontpage.tpl.css');
	$currentPage->addCssTpl('demosphere/css/demosphere-common.tpl.css');
	$currentPage->addCssTpl('demosphere/css/demosphere-map.tpl.css');
	$currentPage->addJs ('lib/jquery.js');
	$currentPage->addJs( 'lib/jquery-ui-1.12.1.custom/jquery-ui.js');
	$currentPage->addCss('lib/jquery-ui-1.12.1.custom/jquery-ui.min.css');

	$currentPage->addJsTranslations([
										'overlap'         =>t('overlap'         ),
										'title'	        =>t('title'           ),
										'keywords'        =>t('keywords'        ),
										'day'		        =>t('day'	          ),
										'time'	        =>t('time'	          ),
										'displayed events'=>t('displayed events'),
										'more information'=>t('more information'),
									]);

	$currentPage->title=t('interactive map');
	$currentPage->addJsVar('demosphereTopics',array_keys(Topic::getAllNames()));

	$currentPage->showPageTopForEventAdmin=true;
	$currentPage->bodyClasses[]='notFullWidth';
	$currentPage->bodyClasses[]='unPaddedContent';
	$currentPage->bodyClasses[]="nonStdContent"; 

	$currentPage->wrapTop=demosphere_frontpage_topic_selector([],'map');

	// ***** location shortcuts
	$locationShortcuts='';
	if(count($demosphere_config['map_location_shortcuts'])!==0)
	{
		$locationShortcuts='<select>';
		$locationShortcuts.='<option value="'.
			ent(json_encode(['lat' =>$demosphere_config['map_center_latitude'],
							 'lng' =>$demosphere_config['map_center_longitude'],
							 'zoom'=>$demosphere_config['map_zoom']])).'">'.
				ent(t('(shortcuts)')).'</option>';
		foreach($demosphere_config['map_location_shortcuts'] as $k=>$shortcut)
		{
			$locationShortcuts.='<option value="'.ent(json_encode($shortcut)).'">'.
				ent($shortcut['name']).'</option>';
		}
		$locationShortcuts.='</select>';
	}

	// CSS for topic colors
	foreach(array_reverse(Topic::getAll()) as $topic)
	{	
		$css=demosphere_frontpage_infobox_maplink_eventmarker_css($topic->id,$topic->color,true);
		$currentPage->addCss($css,'inline');
	}

	// Determine slider size: 1 day, 1 week or 1 month, depending on number of events
	foreach(['day','week','month'] as $interval)
	{
		$nbEvents[$interval]=db_result('SELECT COUNT(*) FROM Event WHERE status=1 AND showOnFrontpage=1 AND startTime>%d AND startTime<%d',
									   Event::today(),strtotime('+1 '.$interval,Event::today()));
	}
	$initialSliderNbDays=1;
	if($nbEvents['week' ]<20){$initialSliderNbDays=7;}
	if($nbEvents['month']<20){$initialSliderNbDays=round((strtotime('+1 month',Event::today())-Event::today())/(3600*24));}

	require_once 'demosphere/demosphere-event-map.php';
	$currentPage->addJs(demosphere_google_map_api_url(),'external');
	$currentPage->addJsConfig('demosphere',['map_center_latitude','map_center_longitude','map_zoom',]);
	$currentPage->addJsVar('demosphere_map_locale_data',demosphere_map_locale_data());
	$currentPage->addJsVar('initialSliderNbDays',$initialSliderNbDays);
	$currentPage->addJs('demosphere/js/demosphere-map.js');
	return template_render('templates/demosphere-map.tpl.php',
						   [compact('locationShortcuts'),]);
}

function demosphere_map_locale_data()
{
	$res=[
		  'shortWeekDays'=>[],
		  'longWeekDays'=>[],
		  'months'=>[]
		  ];
	for($i=0;$i<7;$i++)
	{
		$res['shortWeekDays'][$i]=strftime('%a',strtotime('+'.$i.' days',strtotime('sunday')));
		$res['longWeekDays' ][$i]=strftime('%A',strtotime('+'.$i.' days',strtotime('sunday')));
	}
	for($i=0;$i<12;$i++)
	{
		$res['months' ][$i]=strftime('%B',strtotime('+'.$i.' months',strtotime('January 1')));
	}
	return $res;
}


?>
