<?php

//! Displays a popup when user clicks on higlighted date/time
//! This function needs to be very fast, so many things are non-standard.
function demosphere_event_near_date()
{
	global $base_url,$demosphere_config,$user;
	if(!isset($_GET['date'])){return "<a>invalid request, missing date!</a>";}
	$date=$_GET['date'];
	$parse=isset($_GET['parse']) && $_GET['parse']=='true';
	$id=isset($_GET['id']) ? intval($_GET['id']) : false;
	$source=val($_GET,'source','event');
	$locale=setlocale(LC_TIME,"0");
	if(strpos($locale,'en_US')===0){$locale='en_US';}
	else                           {$locale=preg_replace('@[._].*$@','',$locale);}

	// Check if user can access  
	if(isset($_GET['srcUrl']))
	{
		// This is meant to be called from feed-import or mail-import items displayed in safe domain
		// We re-use the safe domain url and its token to check if the client has the correct IP (REMOTE_ADDR)
		// Note, this is a bit hackish and needs a cleanup. 
		// It copies code from demosphere_safe_domain() and it should be in demosphere-safe-domain.php
		preg_match('@[&?]sdsession=([a-z0-9-]*)&sdtoken=([0-9a-f-]+)@',$_GET['srcUrl'],$matches);
		$found_login=$matches[1];
		$found_token=$matches[2];
		$found_url=$_GET['srcUrl'];
		$found_url=preg_replace('@^https?://[^/]*@','',$found_url);
		$found_url=preg_replace('@[&?]sdtoken=[a-z0-9-]*$@'  ,'',$found_url);
		$found_url=preg_replace('@[&?]sdsession=[a-z0-9-]*$@','',$found_url);
		$found_ip=$_SERVER['REMOTE_ADDR'];
		$sessions=variable_get('demosphere_safe_domain_sessions','',[]);
		$session=val($sessions,$found_login);
		if($session===false){dlib_permission_denied_403('demosphere_safe_domain: failed',false);}
		$good_token=hash('sha256',$session['passwd'].':'.$found_ip.':'.$found_url);
		if($found_token!==$good_token){dlib_permission_denied_403('demosphere_safe_domain: failed (2)',false);}
		// ok: now we have validated this request.
		// Send CORS headers authorizing cross site request.
		header('Access-Control-Allow-Origin: '.$demosphere_config['safe_base_url']);
		header('Access-Control-Allow-Credentials: true');
	}

	$out='';
	if($source=='plugin')
	{
		require_once 'dlib/css-template.php';
		$out.='<link rel="stylesheet" type="text/css" media="all" href="'.
			css_template('demosphere/css/demosphere-admin.tpl.css').'" />';
	}

	$out.='<h1 class="eventsNearDatePopupTitlebar">X</h1>';
	$out.='<ul>';

	if($parse)
	{
		require_once 'dlib/find-date-time/find-date-time.php';
		$ts=find_date_and_time($date);
	}
	else
	{
		$ok=preg_match('@^ *(?P<year>[0-9]{4})-(?P<month>0?[0-9]|1[0-2])-(?P<day>0?[0-9]|[12][0-9]|3[01])(?P<time>[ T]'.
					   '(?P<hour>0?[0-9]|1[0-9]|2[0-3]):(?P<minute>[0-5][0-9]))? *$@',$date,$matches);
		if($ok===0 || !checkdate($matches['month'],$matches['day'],$matches['year']))
		{
			$out.="<a>invalid date!</a>";
			return $out;
		}
		if(!isset($matches['time'])){$matches['hour']='3';$matches['minute']='33';}
		$ts=mktime($matches['hour'],$matches['minute'],0, $matches['month'],$matches['day'],$matches['year']);
	}
	if($ts===false){$out.="<a>invalid date!!</a>";return $out;}
	$undefTime=(date('G i',$ts)=='3 33');
	//$out.="bonjour-$date<br/>".date('r',$ts)."<br/>";var_dump($undefTime);
	$ts=intval($ts);
	if($source=='event')
	{
		$tsStart=$ts-60*35;
		$tsEnd  =$ts+60*35;
	}
	else
	{
		$tsStart=$ts-60*35;
		$tsEnd  =$ts+60*35;
	}
	if($undefTime) // show all day's events
	{
		$tsStart=$ts-12780;
		$tsEnd  =$ts+73620;
	}

	$isEventAdmin=$isEventAdmin=$user->checkRoles('admin','moderator');

	// *********** fetch events from database
	$published=!$isEventAdmin ? "AND Event.status = 1" : '';
	$query=
<<<SQLQUERY
SELECT
 Event.id       AS id,
 Event.status,
 Event.moderationStatus,
 Event.needsAttention,
 Event.moderationMessage,
 Event.htmlTitle,
 Event.title,
 Event.startTime,
 Event.showOnFrontpage,
		(SELECT IF(shortName = '',name,shortName) FROM City WHERE City.id=Place.cityId LIMIT 1) AS short_city
FROM
 Event,
 Place
WHERE
 Place.id = Event.placeId AND
 Event.moderationStatus!=7 AND
 Event.startTime >=$tsStart AND
 Event.startTime <=$tsEnd 
 $published
ORDER BY startTime ASC
SQLQUERY;

	$ct=0;
	$query_result=db_query($query);
	while ($event = db_fetch_object($query_result))
	{
		// don't display the event that is being edited
		if($event->id==$id){continue;} 
		$eurl=$demosphere_config['std_base_url'].'/'.$demosphere_config['event_url_name'].'/'.$event->id;
		$out.='<li class="eventsNearDatePopupLine'.($event->status ? '':' notPublished').' '.
			($event->showOnFrontpage ? '' : 'private ').
			'status-'.$event->moderationStatus.' '.
			'nattention-level-'.(!$event->needsAttention ? 0:1+floor($event->needsAttention/100)).'">';

		$time=strftime((date('z',$ts)!=date('z',$event->startTime) ? 
						($locale=='en_US' ? '%m/%d' : '%d/%m ') : '').
					   '%R', $event->startTime);
		$time=str_replace('03:33',' - ',$time);

		$out.=($isEventAdmin ? '<a class="deventTime" href="'.$eurl.'/edit" target="_blank">' :
			  '<span class="deventTime">').
			$time.
			($isEventAdmin ? '</a>' : '</span>');
		if($isEventAdmin && !$event->showOnFrontpage){$out.=' - <span class="deventPrivate">P</span> ';}
		if($isEventAdmin && strlen($event->moderationMessage))
		{
			$out.=' <span class="deventMessage">['.
				htmlentities(mb_substr(preg_replace("@\n.*$@s",'',$event->moderationMessage),0,18).
							 (strpos($event->moderationMessage,"\n")!==false ? '…' :'')).
				']</span> ';
		}
		$out.='<a href="'.$eurl.'" target="_blank">'.
			" - ".(strlen($event->htmlTitle) ? $event->htmlTitle : $event->title).
			"&nbsp;&nbsp;".
 			"<span class=\"stpCity\">".
			$event->short_city."</span>".
			"</a>";
		$out.='</li>';
		$ct++;
	}
	$out.='</ul>';
	if($ct==0)
	{
		// translation is slow, avoid it in french & english
		if($undefTime)
		{
			switch($locale)
			{
			case 'fr'   : $out.="Pas d'évènement trouvé le ".strftime('%d/%m', $ts);break;	
			case 'en_US': $out.="No events found for ".strftime('%m/%d', $ts);break;	
			default     : $out.=t("No events found for @date",['@date'=>strftime('%d/%m', $ts)]);break;	
			}
		}
		else
		{
			switch($locale)
			{
			case 'fr'   : $out.="Pas d'évènement trouvé autour de ".strftime('%d/%m %R', $ts);break;	
			case 'en_US': $out.="No events found around ".strftime('%m/%d %R', $ts);break;	
			default     : $out.=t("No events found around @time",['@time'=>strftime('%d/%m %R', $ts)]);break;	
			}
		}
	}
	return $out;
}

?>