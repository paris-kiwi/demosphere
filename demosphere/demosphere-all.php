<?php

function demosphere_all()
{
	// find -iname  "*.php" | grep -E -v '(#|test|/files/|\.tpl\.|log-remove-robots\.php|demosphere-install\.php|/extra-strings\.php|/migrate\.php|^\./[^/]*\.php|/custom/|^\./lib/)' | sed "s@^\./@require_once '@" | sed "s@\$@';@" 

	// move to begining:
	require_once 'dlib/DBObject.php';

	require_once 'dlib/form.php';
	require_once 'dlib/diff.php';
	require_once 'dlib/find-date-time/find-date-time-ca.php';
	require_once 'dlib/find-date-time/find-date-time-es.php';
	require_once 'dlib/find-date-time/find-date-time-gl.php';
	require_once 'dlib/find-date-time/find-date-time-en.php';
	require_once 'dlib/find-date-time/find-date-time-fr.php';
	require_once 'dlib/find-date-time/find-date-time.php';
	require_once 'dlib/find-date-time/find-date-time-it.php';
	require_once 'dlib/find-date-time/find-date-time-pt.php';
	require_once 'dlib/find-date-time/find-date-time-el.php';
	require_once 'dlib/find-date-time/find-date-time-de.php';
	require_once 'dlib/translation-backend.php';
	require_once 'dlib/dlib-debug.php';
	require_once 'dlib/dlib-cron.php';
	require_once 'dlib/database.php';
	require_once 'dlib/array-of-arrays.php';
	require_once 'dlib/tools.php';
	require_once 'dlib/parse-file.php';
	require_once 'dlib/sprite.php';
	require_once 'dlib/download-tools.php';
	require_once 'dlib/dbtable-ui.php';
	require_once 'dlib/HtmlPageData.php';
	require_once 'dlib/template.php';
	require_once 'dlib/hdiff.php';
	require_once 'dlib/comments/comments.php';
	require_once 'dlib/comments/comments-notifications.php';
	require_once 'dlib/comments/Comment.php';
	require_once 'dlib/comments/IpWhois.php';
	require_once 'dlib/DBObject.php';
	require_once 'dlib/parse-php.php';
	require_once 'dlib/translation.php';
	require_once 'dlib/dlib-paths.php';
	require_once 'dlib/dbsessions.php';
	require_once 'dlib/mail-and-mime.php';
	require_once 'dlib/dbobject-ui.php';
	require_once 'dlib/html-tools.php';
	require_once 'dlib/filter-xss.php';
	require_once 'dlib/commandline-tools.php';
	require_once 'dlib/Pager.php';
	require_once 'dlib/css-template.php';
	require_once 'dlib/table-tools.php';
	require_once 'dlib/variable.php';
	require_once 'demosphere/page-watch/page-watch.php';
	require_once 'demosphere/page-watch/Page.php';
	require_once 'demosphere/page-watch/page-watch-view.php';
	require_once 'demosphere/demosphere-map.php';
	require_once 'demosphere/demosphere-config-check.php';
	require_once 'demosphere/demosphere-feeds.php';
	require_once 'demosphere/demosphere-search.php';
	require_once 'demosphere/Term.php';
	require_once 'demosphere/demosphere-post.php';
	require_once 'demosphere/docconvert/docconvert.php';
	require_once 'demosphere/demosphere-place-search.php';
	require_once 'demosphere/demosphere-event-send-by-email.php';
	require_once 'demosphere/demosphere-widget.php';
	require_once 'demosphere/demosphere-event-revisions.php';
	require_once 'demosphere/demosphere-dtoken.php';
	require_once 'demosphere/demosphere-event-ajax-edit.php';
	require_once 'demosphere/demosphere-htmlview.php';
	require_once 'demosphere/demosphere-place.php';
	require_once 'demosphere/demosphere-misc.php';
	require_once 'demosphere/demosphere-robots-regexp.php';
	require_once 'demosphere/demosphere-city.php';
	require_once 'demosphere/demosphere-event.php';
	require_once 'demosphere/demosphere-cron.php';
	require_once 'demosphere/demosphere-opinion.php';
	require_once 'demosphere/demosphere-frontpage.php';
	require_once 'demosphere/demosphere-paths.php';
	require_once 'demosphere/demosphere-path-alias.php';
	require_once 'demosphere/RepetitionGroup.php';
	require_once 'demosphere/feed-import/Article.php';
	require_once 'demosphere/feed-import/feed-import-view.php';
	require_once 'demosphere/feed-import/feed-import.php';
	require_once 'demosphere/feed-import/feed-import-form.php';
	require_once 'demosphere/feed-import/Feed.php';
	require_once 'demosphere/feed-import/FakeFeed.php';
	require_once 'demosphere/demosphere-setup.php';
	require_once 'demosphere/demosphere-event-near-date.php';
	require_once 'demosphere/Event.php';
	require_once 'demosphere/demosphere-update-db.php';
	require_once 'demosphere/demosphere-event-edit-form.php';
	require_once 'demosphere/demosphere-built-in-pages.php';
	require_once 'demosphere/Place.php';
	require_once 'demosphere/demosphere-topics.php';
	require_once 'demosphere/html-selector/html-selector.php';
	require_once 'demosphere/demosphere-event-import.php';
	require_once 'demosphere/demosphere-all.php';
	require_once 'demosphere/demosphere-page.php';
	require_once 'demosphere/User.php';
	require_once 'demosphere/demosphere-date-time.php';
	require_once 'demosphere/demosphere-event-repetition.php';
	require_once 'demosphere/Carpool.php';
	require_once 'demosphere/City.php';
	require_once 'demosphere/demosphere-event-map.php';
	require_once 'demosphere/demosphere-event-post-on-other-site.php';
	require_once 'demosphere/text-matching/TMDocument.php';
	require_once 'demosphere/text-matching/text-matching.php';
	require_once 'demosphere/text-matching/text-matching-view.php';
	require_once 'demosphere/htmledit/demosphere-htmledit.php';
	require_once 'demosphere/demosphere-user.php';
	require_once 'demosphere/demosphere-terms.php';
	require_once 'demosphere/demosphere-event-list-form.php';
	require_once 'demosphere/Post.php';
	require_once 'demosphere/demosphere-email-subscription.php';
	require_once 'demosphere/demosphere-help.php';
	require_once 'demosphere/demosphere-config-form.php';
	require_once 'demosphere/mail-import/mail-import.php';
	require_once 'demosphere/mail-import/MimeMessage.php';
	require_once 'demosphere/mail-import/Message.php';
	require_once 'demosphere/mail-import/mail-import-view.php';
	require_once 'demosphere/mail-import/MailRule.php';
	require_once 'demosphere/demosphere-event-multiple-edit.php';
	require_once 'demosphere/demosphere-stats.php';
	require_once 'demosphere/demosphere-page-cache.php';
	require_once 'demosphere/demosphere-event-publish-form.php';
	require_once 'demosphere/dcomponent/dcomponent-common.php';
	require_once 'demosphere/demosphere-maintenance.php';
	require_once 'demosphere/demosphere-emails.php';
	require_once 'demosphere/demosphere-common.php';
	require_once 'demosphere/Topic.php';
	require_once 'demosphere/demosphere-user-calendar.php';
	require_once 'demosphere/demosphere-commandline.php';
	require_once 'demosphere/demosphere-panel.php';
	require_once 'demosphere/demosphere-carpool.php';
	require_once 'demosphere/demosphere-event-list.php';
	require_once 'demosphere/demosphere-safe-domain.php';
	require_once 'demosphere/Opinion.php';
	require_once 'demosphere/demosphere-external-events.php';


	// **************************** manually added
	require_once 'demosphere/test/test-demosphere.php';
	require_once 'dlib/testing.php';

	// **************************** manually added from lib/
	require_once 'lib/class.phpmailer.php';
	require_once 'lib/sphinxapi.php';
	require_once 'lib/iCalcreator/iCalcreator.php';
	require_once 'lib/htmLawed.php';
	//require_once 'lib/iCalUtilityFunctions.class.php';

}
?>