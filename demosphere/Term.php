<?php

class Term extends DBObject
{
	public $id=0;
	static $id_           =['type'=>'autoincrement'];
	public $vocab=0;
	static $vocab_        =['type'=>'int'];
	public $name;
	static $name_         =[];
	public $description;
	static $description_  =[];

 	static $dboStatic=false;

	function __construct($vocab)
	{
		$this->vocab=$vocab;
	}

	function delete()
	{
		db_query("DELETE FROM Event_Term WHERE term=%d",$this->id);
		parent::delete();
	}

	function save()
	{
		parent::save();
		demosphere_page_cache_clear_all();
	}

	static function getAll($vocab,$where=false)
	{
		return Term::fetchList('WHERE vocab=%d',$vocab);
	}

	static function getAllNames($vocab)
	{
		return db_one_col("SELECT id,name FROM Term WHERE vocab=%d",$vocab);
	}

	static function parse($string,$vocab)
	{
		$res=[];
		foreach(explode(',',$string) as $name)
		{
			$name=trim($name);
			$id=db_result_check("SELECT id FROM Term WHERE LOWER(name)=LOWER('%s') AND vocab=%d",$name,$vocab);
			if($id===false)
			{
				$term=new Term($vocab);
				$term->name=$name;
				$term->save();
				$id=$term->id;
			}
			$res[]=$id;
		}
		return $res;
	}


	static function createTable()
	{
		db_query("DROP TABLE IF EXISTS Term");
		db_query("CREATE TABLE Term (
  `id` int(11) NOT NULL auto_increment,
  `vocab` TINYINT(4) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `vocab` (`vocab`),
  KEY `name` (`name`)
) DEFAULT CHARSET=utf8mb4");

		db_query("DROP TABLE IF EXISTS Event_Term");
		db_query("CREATE TABLE Event_Term (
  `event` int(11) NOT NULL,
  `term` int(11) NOT NULL,
  PRIMARY KEY  (`event`,`term`),
  KEY `event` (`event`),
  KEY `term` (`term`)
) DEFAULT CHARSET=utf8mb4");
	}

}
?>
