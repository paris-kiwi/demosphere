<h1 class="page-title">_(Stats)</h1>
<table id="feed-stats" class="gray-table sortable js-sort">
	<tr class="sticky-table-top">
		<th>id</th>
		<th>_(Name)</th>
        <th>_(Articles)</th>
        <th>_(Finished)</th>
        <th>_(Error art.)</th>
		<th>_(Last OK)</th>
        <th>_(Most recent)</th>
	</tr>
	<? foreach($feeds as $id=>$feed){?>
		<tr>
			<td>$id</td>
			<td><a href="$scriptUrl/feed/$id">$feed->name</a></td>
	        <td>$stats[$id]['nbArticles']</td>
		    <td>$stats[$id]['nbFinished']</td>
			<td>$stats[$id]['nbBad']</td>
			<td data-sortable="$feed->lastOk"            ><?: dcomponent_format_date('relative-short',$feed->lastOk           ) ?></td>
		    <td data-sortable="$stats[$id]['mostRecent']"><?: dcomponent_format_date('relative-short',$stats[$id]['mostRecent'])?></td>
		</tr>
	<?}?>
</table>