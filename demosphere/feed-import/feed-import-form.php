<?php

//! Multi-step form to create a new Feed
function feed_import_form()
{
	global $user;
	require_once 'feed-import/feed-import-view.php';
	require_once 'dlib/form.php';
	feed_import_view();
	

	$step=val($_GET,'step','start');
	switch($step)
	{
	case 'no-selector': return feed_import_form_no_selector();
	}
	$guessed=false;

	$formOpts=['id'=>'create-feed-form'];

	$items=[];
	$items['page-title']=['html'=>'<h1 class="page-title">'.t('Create feed').'</h1>'];

	$items['url']=['type'=>'url',
				  'title'=>t('Url:'),
				  'required'=>true,
				  'description'=>t('Please enter either:').
				   '<ul>'.
				   '   <li>'.t('the url of a site that has a feed (Example: http://example.over-blog.com)').'</li>'.
				   '   <li>'.t('the url of a feed (Example: http://example.over-blog.com/rss-articles.xml)').'</li>'.
				   '   <li>'.t('the url of a Facebook page (Example: https://www.facebook.com/example/)').'</li>'.
				   '   <li>'.t('the url of a Twitter page (Example: https://twitter.com/Example)').'</li>'.
				   '   <li>'.t('the url of an iCalendar/ics (Example: http://www.google.com/calendar/ical/example%40gmail.com/public/basic.ics)').'</li>'.
				   '</ul>',
				 ];

	$items['advanced-begin']=
		[
			'type' => 'fieldset',
			'title' => t('Advanced:'),
			'collapsible'=> true,
			'collapsed'  => true,
		];

	$items['fake-feed-type']=
		[
			'title'=> t('Special feed type'),
			'type' => 'select',
			'options'=>['automatic'=>t('Automatic'),'custom-code'=>'custom-code','links-in-page'=>'links-in-page','page-parts'=>'page-parts'],
		];
	if(!$user->checkRoles('admin')){unset($items['fake-feed-type']['custom-code']);}

	$items['advanced-end']=['type' => 'fieldset-end',];


	$items['submit']=['type'=>'submit',
					 'value'=>t('Create'),
					 'submit'=>function($items)use(&$guessed)
		             {
						 global $base_url;
						 $feedUrl=$guessed['url'];
						 $feed=false;

						 $values=dlib_array_column($items,'value');
						 if($values['fake-feed-type']!=='automatic')
						 {
							 $feed=new Feed('');
							 if($values['fake-feed-type']==='page-parts'){$feed->getContentFromFeedXml=true;}
							 $feed->name=dcomponent_url_to_name($values['url'],'Feed');
							 $feed->save();
							 $fakeFeed=new FakeFeed($feed->id,$values['url'],FakeFeed::typeEnum($values['fake-feed-type']));
							 $fakeFeed->save();
							 if($values['fake-feed-type']==='links-in-page')
							 {
								 dlib_redirect($base_url.'/feed-import/fake-feed/'.$fakeFeed->id.'/lip-html-selector-editor');
							 }
							 else
							 {
								 dlib_redirect($feed->link().'/edit');
							 }
						 }
						 else
						 if($guessed['type']==='icalendar')
						 {
							 $feed=feed_import_form_icalendar_create($feedUrl);
						 }
						 else
						 if($guessed['type']==='facebook')
						 {
							 $feed=feed_import_form_facebook_create($feedUrl);
						 }
						 else
						 if($guessed['type']==='twitter')
						 {
							 $feed=feed_import_form_twitter_create($feedUrl);
						 }
						 else
						 if(val($guessed,'getContentFromFeedXml'))
						 {
							 $feed=new Feed($feedUrl);
							 $feed->getContentFromFeedXml=true;
							 $feed->save();
							 $feed->update();
							 $feed->updateArticles();
						 }
						 else
						 {
							 echo '<div style="position: fixed;background-color: green;color: white;padding: .2em;margin-left: 50%;font-size: 20px;">'.
							 t('Studying site layout... please wait').'</div>';
							 feed_import_form_progress("======== guess_selector start:");
							 $report=['debug'=>true];
							 $selector=feed_import_form_guess_selector($feedUrl,$report);
							 if(count($report['errors'])){lvd($report['errors']);}
							 feed_import_form_progress("======== guess_selector finished");
							 if($selector===false)
							 {
								 dcomponent_progress_log_end(['redirect'=>$base_url.'/feed-import/form?step=no-selector&feedUrl='.urlencode($feedUrl),
															  'boxDisplay'=>'fold']);
							 }
							 else
							 {
								 require_once 'dcomponent/dcomponent-common.php';
								 $feed=new Feed($feedUrl);
								 $feed->htmlContentSelector=[['type'=>'CSS','arg'=>$selector]];
								 $feed->save();
								 $feed->update();
								 $feed->updateArticles();
							 }
						 }
						 if($feed!==false)
						 {
							 if(isset($guessed['name']))
							 {
								 $feed->name=$guessed['name'];
								 $feed->save();
							 }
							 dlib_message_add(t('Automatic feed setup successfully completed, please check below if articles are correctly downloaded.'));
							 dcomponent_progress_log_end(['redirect'=>$feed->link(),'boxDisplay'=>'fold']);
						 }
						 else
						 {
							 dlib_message_add(t('Feed creation failed.').($guessed['type']!=='normal' ? ' ('.$guessed['type'].')' : ''));
							 dlib_redirect($base_url.'/feed-import/form');
						 }
					 }
					 ];

	$formOpts['validate']=function(&$items)use(&$guessed)
		{
			if($items['fake-feed-type']['value']==='automatic')
			{
				$guessed=feed_import_form_guess_feed_type($items['url']['value']);
				if(isset($guessed['error'])){$items['url']['error']=$guessed['error'];}
			}
		};


	return form_process($items,$formOpts);
}

//! Multi-step form to create a Feed : this is step 2, when we failed to automatically guess a selector. 
function feed_import_form_no_selector()
{
	$items=[];
	$items['top-msg']=['html'=>t('Failed to automatically find which part of the pages of this site contain the articles of interest (selector). If you want to create this feed, you will have to do it manually.'),
					 ];
	$items['cancel']=['type'=>'submit',
					 'value'=>t('Cancel'),
					 'redirect'=>'/feed-import/feed',
					 ];

	$items['create']=['type'=>'submit',
					 'value'=>t('Create'),
					 'submit'=>function($items)
		             {
						 $feedUrl=$_GET['feedUrl'];
						 $feed=new Feed($feedUrl);
						 $feed->save();
						 dlib_message_add(t('New feed successfully created. You should now: <ol><li>optionally edit any necessary information on this page and save it</li><li>configure the content selector ("Manual" option inside the "edit" tab)</li><li>update the feed to make sure it is working OK ("update" button in the "view" tab)</li></ol>'));
						 dlib_redirect($feed->link()."/edit");
					 }
					 ];

	return form_process($items);	
}


//! Runs guess_selector and displays debug information.
//! Intended for testing/debugging from web interface.
function feed_import_form_feed_try_auto_selector($id)
{
	require_once 'feed-import/feed-import-view.php';
	$feed=Feed::fetch($id);
	feed_import_view();
	$report=['debug'=>true];
	$selector=feed_import_form_guess_selector($feed->url,$report);
	feed_import_form_progress("======== found:");
	lvd($selector);
	if(count($report['errors'])){lvd($report['errors']);}
	if(dlib_debug_use_html()){dcomponent_progress_log_end(['redirect'=>$feed->link()]);}
}

//! Fetches a Facebook page id and creates a Feed.
//! This is temp, we should try to use the Facebook graph API
//! FIXME: move some of this into FakeFeed::createFacebook()
function feed_import_form_facebook_create($pageUrl)
{
	$html=dlib_curl_download($pageUrl,false);
	if(preg_match('@<meta[^>]*\bproperty="al:[^>":]*:url"[^>]*\bcontent="fb://(page|group)/\?id=([0-9]+)"@',$html,$m))
	{
		$facebookId=$m[2];
	}
	else
	if(preg_match('@{"pageID":"([0-9]+)","pageName"@',$html,$m))
	{
		$facebookId=$m[1];
	}
	else
	{
		dlib_message_add(t('Failed to find Facebook id for this page.'),'error');
		return false;
	}

	// FIXME: sometimes pages don't have og:url. Try to understand why.
	if(preg_match('@<meta[^>]*\bproperty="og:url"[^>]*\bcontent="(https?://[^/]*facebook[^"]*)"@',$html,$m))
	{
		$cleanUrl=dlib_html_entities_decode_all($m[1]);
		// Facebook bug ? This url can contain invalid characters ! (example: accented caracters)
		$cleanUrl=preg_replace_callback('@[^A-Za-z0-9._~:/?#\[\]\@!$&\'()*+,;=`.-]@u',function($m){return urlencode($m[0]);},$cleanUrl);
	}
	else
	{
		dlib_message_add(t('Failed to find clean url for this Facebook page.'),'warning');
		$cleanUrl=$pageUrl;
	}
	$feed=new Feed('');
	$feed->getContentFromFeedXml=true;
	$feed->name=dcomponent_url_to_name($cleanUrl,'Feed');
	$feed->htmlContentSelector=[['type' => 'CSS-REMOVE','arg'=>'.name']];
	$feed->save();
	$fakeFeed=new FakeFeed($feed->id,$cleanUrl,FakeFeed::typeEnum("facebook"));
	$fakeFeed->fbId=$facebookId;
	$fakeFeed->save();
	$feed->update();
	$feed->updateArticles();
	return $feed;
}


function feed_import_form_twitter_create($pageUrl)
{
	if(!preg_match('@https://[^/]*\btwitter\.com/([a-zA-Z0-9_]{1,15})@',$pageUrl,$m))
	{
		dlib_message_add(t('Failed to find Twitter username for this page.'),'error');
		return false;
	}
	$cleanUrl='https://twitter.com/'.$m[1];
	$twitterName=$m[1];

	$feed=new Feed('');
	$feed->getContentFromFeedXml=true;
	$feed->name=dcomponent_url_to_name($twitterName,'Feed');
	$feed->save();
	$fakeFeed=new FakeFeed($feed->id,$cleanUrl,FakeFeed::typeEnum("twitter"));
	$fakeFeed->save();
	$feed->update();
	$feed->updateArticles();
	return $feed;
}


//! Creates a FakeFeed to manage an iCalendar
//! FIXME: move this into FakeFeed::createIcalendar()
function feed_import_form_icalendar_create($pageUrl)
{
	$feed=new Feed('');
	$feed->getContentFromFeedXml=true;
	$feed->name=dcomponent_url_to_name($pageUrl,'Feed');
	$feed->save();
	// this is a wild guess that is often wrong (for example for google cals). 
	// but there is no url to the src site in a google ical
	$ffurl=dlib_host_url($pageUrl);
	$fakeFeed=new FakeFeed($feed->id,$ffurl,FakeFeed::typeEnum("icalendar"));
	$fakeFeed->iCalUrl=$pageUrl;
	$fakeFeed->save();
	$feed->update();
	$feed->updateArticles();
	return $feed;
}



//*************************************************************
//! Guess best selector for feed
//*************************************************************

//! Guess the best selector for the main content/article in the pages of a feed.
//! This downloads all pages and uses a long and fairly complex algorithm to determine an optimal selector.
//! $report is used to gather information that might be usefull for debuging
function feed_import_form_guess_selector($feedUrl,&$report=[])
{
	if(!isset($report['debug'])){$report['debug']=false;}
	$report['errors']=[];
	$pages=feed_import_form_fetch_pages($feedUrl,$feedXml,$report);
	if($pages===false){return false;}
	$siteInfo=feed_import_form_site_info($pages,$feedXml);	
	$report['siteInfo']=$siteInfo;
	$selector=feed_import_form_guess_selector_from_pages($pages,$siteInfo,$report);
	return $selector;
}

//! Downloads at most 10 pages from the feed and returns an array of pages with the html, DOM tree, feed text...
function feed_import_form_fetch_pages($feedUrl,&$feedXml,&$report)
{
	global $custom_config;
	require_once 'dlib/download-tools.php';
	require_once 'dlib/html-tools.php';
	require_once 'dlib/diff.php';
	$curl_download=val($custom_config,'feed_import_form_curl_download','dlib_curl_download');
	$feedXml=$curl_download($feedUrl,false,['timeout'=>20,'headers'=>&$getHeaders]);
	if($feedXml===false || $feedXml===''){$report['errors'][]='Feed download failed';return false;}
	$feedXml=Feed::cleanupFeedXml($feedXml,$feedUrl,$getHeaders!==null ? val($getHeaders,'content-type','') : false);

	require_once 'feed-import/feed-import.php';
	$items=feed_import_feed_items($feedXml,$feedUrl);
	if($items===false){return false;}
	//lvd($items);
	$pages=[];
	$ct=0;
	foreach($items as $item)
	{
		$page=[];
		$page['url'  ]=$item['link'];
		feed_import_form_progress("fetch page:".$page['url']);
		$download_and_clean_html_page=val($custom_config,'feed_import_form_download_and_clean_html_page','dlib_download_and_clean_html_page');
		$page['html' ]=$download_and_clean_html_page($page['url'],false,['timeout'=>20]);
		if($page['html' ]===false || $page['html']==='')
		{
			$report['errors'][]='empty or failed html for '.$page['url'];
			continue;
		}
		// remove <script> tags which will pollute text
		$page['html' ]=dlib_html_select_xpath($page['html'],'//script',true);
		$page['doc'  ]=dlib_dom_from_html($page['html'],$xpath);
		$page['xpath']=$xpath;
		$page['feedText']=false;
		$page['feedWords']=[];

		$desc=feed_import_feed_item_description($item['xml'],'bestField');
		// skip pages with no description
		if($desc===false){continue;}
		$page['feedText']=dlib_simplify_text($desc,true);
		$page['feedWords']=feed_import_form_html_to_words($desc);
		// skip pages with very few words
		if(count($page['feedWords'])<4){continue;}
		$pages[]=$page;
		if($ct++>=10-1){break;}
	}
	if(count($pages)<4)
	{
		$report['errors'][]='Found '.count($items).' items in feed. '.
			'Only '.count($pages).' have valid descriptions and contents, aborting.';
		return false;
	}
	//lvd($pages);
	$cacheName=md5(implode('::',dlib_array_column($pages,'url')));
	foreach($pages as &$page){$page['cacheName']=$cacheName;}
	unset($page);

	return $pages;
}

//! Determines information (CMS) about a site from data in pages.
function feed_import_form_site_info($pages,$feedXml)
{
	$siteInfo=[];
	$siteInfo['generator']=false;
	$siteInfo['cms']=false;

	// *** Look for generator in feed xml
	$xml=simplexml_load_string($feedXml);
	if($xml!==false)
	{
		$feedGenerator=isset($xml->channel->generator) ? $xml->channel->generator : false;
		if($feedGenerator!==false)
		{
			$siteInfo['generator']=(string)$feedGenerator;
			foreach(array_reverse(['spip','wordpress','blogger','drupal','canalblog','joomla','tumblr','dotclear','overblog']) as $cms)
			{
				if(preg_match('@\b'.$cms.'\b@i',(string)$feedGenerator))
				{
					$siteInfo['cms']=$cms;
				}
			}
		}
	}

	// *** Look for generator in page html <meta>
	foreach(dlib_first($pages)['xpath']->query("//meta") as $meta)
	{
		if(strtolower($meta->getAttribute('name'))==='generator')
		{
			$siteInfo['generator']=$meta->getAttribute('content');
			break;
		}
	}
	foreach(array_reverse(['spip','wordpress','blogger','drupal','canalblog','joomla']) as $cms)
	{
		if(preg_match('@\b'.$cms.'\b@i',$siteInfo['generator'])){$siteInfo['cms']=$cms;}
	}

	// *** Last resort: look for cms by looking characterstic stuff in html (example: CMS specific urls)
	if($siteInfo['cms']===false && preg_match('@over-blog\.(com|org)/@'       ,dlib_first($pages)['url' ])){$siteInfo['cms']='over-blog';}
	if($siteInfo['cms']===false && preg_match('@assets.over-blog\.(com|org)/@',dlib_first($pages)['html'])){$siteInfo['cms']='over-blog';}
	if($siteInfo['cms']===false && preg_match('@id="ja-login"@'               ,dlib_first($pages)['html'])){$siteInfo['cms']='joomla';}
	if($siteInfo['cms']===false && preg_match('@/sites/all/modules@'          ,dlib_first($pages)['html'])){$siteInfo['cms']='drupal';}
	if($siteInfo['cms']===false && preg_match('@wp-content/themes@'           ,dlib_first($pages)['html'])){$siteInfo['cms']='wordpress';}
	return $siteInfo;
}

//! Implements the alogrithm that finds the best selector for a list of pages.
//! Returns a string with a CSS selector
function feed_import_form_guess_selector_from_pages($pages,$siteInfo,&$report)
{
	//*** Find candidate selectors by looking in pages html for text from feed
	$candidateSelectors=feed_import_form_candidate_selectors($pages,$siteInfo,$report);
	arsort($candidateSelectors);
	$candidateSelectors=array_keys($candidateSelectors);

	//*** Add common selectors
	$wellKnownSelectors=['.post'=>1,'.entry-content'=>.8,'#content'=>1,'#contenu'=>1,'.body'=>1,'.entry'=>1,'.contenuArticle'=>1,'#article'=>1,'.post-body'=>.7,'.article'=>1,'.texte'=>.5,'.post-entry'=>.5,'.post-content'=>.5,'.contenu-principal'=>.3,'.node'=>1];
	// performance: remove commonSelectors that do not match anything
	foreach(array_keys($wellKnownSelectors) as $selector)
	{
		$found=false;
		foreach($pages as $page)
		{
			$selected=$page['xpath']->evaluate(dlib_css_selector_to_xpath($selector));
			if($selected!==false && $selected->length!=0){$found=true;break;}
		}
		if($found===false){unset($wellKnownSelectors[$selector]);}
	}
	//lvd($wellKnownSelectors);
	//die('pp');

	$candidateSelectors=array_unique(array_merge($candidateSelectors,array_keys($wellKnownSelectors)));
	$report['candidateSelectors']=$candidateSelectors;
	if($report['debug'])lvd('$candidateSelectors:',$candidateSelectors);
	if(count($candidateSelectors)===0){$report['errors'][]='No candidate selectors found.';return false;}

	//*** Evaluate each selector for several criteria
	$evals=[];
	foreach($candidateSelectors as $selector)
	{
		if($report['debug'])feed_import_form_progress("\n=====****:$selector");
		$eval=feed_import_form_evaluate_selector($selector,$pages,$siteInfo,$report);
		$evals[$selector]=$eval;
	}

	//***  add size rank to favor selectors that select more contents
	$sizes=dlib_array_column($evals,'medianSize');
	asort($sizes);
	foreach(array_keys($sizes) as $n=>$selector)
	{
		//if($report['debug'])feed_import_form_progress( "size adj: $selector:".$sizes[$selector]." : ".(($n/count($sizes))*.2)."\n");
		$evals[$selector]['sizeRank']=$n;
	}

	//*** boost for selectors in $wellKnownSelectors
	foreach($evals as $selector=>$eval)
	{
		$evals[$selector]['wellKnown']=val($wellKnownSelectors,$selector,0);
	}

	//*** custom rules
	foreach($evals as $selector=>$eval)
	{
		$evals[$selector]['custom']=0;
	}
	if(isset($evals['.texte'  ]) && isset($evals['#content'])){$evals['.texte'  ]['custom']=-.5;}
	if(isset($evals['.article']) && isset($evals['#article'])){$evals['#article']['custom']+=.1;}

	$report['evals']=$evals;


	//*** Compute final score for each selector from data in $evals
	$selScores=[];
	if($report['debug'])feed_import_form_progress( "*** final scores");
	foreach($evals as $selector=>$eval)
	{
		$selScores[$selector]=
			$eval['medianFeedSelSimilarity']/(.5+$eval['medianCommonRatio'])+
			.2*($eval['sizeRank']/count($evals))+
			.2*$eval['wellKnown']+
			$eval['custom'];
	}

	arsort($selScores);
	
	foreach($selScores as $selector=>$score)
	{
		$eval=$evals[$selector];
		if($report['debug'])feed_import_form_progress( sprintf('%30s',$selector)." : ".
													 " score: ".sprintf('%3.3f',$score).
													 " feed:".  sprintf('%3.3f',$eval['medianFeedSelSimilarity']).
													 ' common:'.sprintf('%3.3f',$eval['medianCommonRatio']).
													 ' : size:'.sprintf('%2d'  ,$eval['sizeRank']).
													 ' : wellKnown:'.sprintf('%1.2f',$eval['wellKnown']).
													 ' : custom:'.sprintf('%1.2f',$eval['custom'])
												   );
	}

	if($report['debug'])lvd($selScores);
	$report['selScores']=$selScores;
	//feed_import_form_evaluate_selector([['type'=>'CSS','arg'=>'.post']],$pages,$siteInfo);

	if(dlib_first($selScores)<1.5){return false;}
	return dlib_first_key($selScores);
}

//! Searches for the feed's texts (descriptions) in the html of pages.
//! Returns a list of selectors that match places in all pages that contain the texts.
//! This relies on feed's texts (<description>..</description> or similar) which is not always available or accurate.
function feed_import_form_candidate_selectors($pages,$siteInfo,&$report)
{
	$okSelectors=[];
	foreach($pages as $n=>$page)
	{
		if($report['debug'])feed_import_form_progress( "**************** feed_import_form_candidate_selectors page $n");
			//if($n===0){continue;}

		//*** Search for feed text in html (recursive search in dom)
		if($report['debug'])lvd('feedText:',$page['feedText']);
		$searchWords=$page['feedWords'];
		$foundNodes=feed_import_form_find_text_in_dom($page['doc'],$searchWords);

		//*** Build a list of acceptable CSS selectors from nodes that contain the feed's text
		foreach($foundNodes as $node)
		{
			//** build "path" of this node (from node to top of doc)
			$nodePath=[];
			for($n=$node;$n!=null;$n=$n->parentNode)
			{
				$id   =method_exists($n,'getAttribute') ? $n->getAttribute('id'   ): '';
				$class=method_exists($n,'getAttribute') ? $n->getAttribute('class'): '';
				// invalid id or class can cause problems later (invalid selectors will crash xpath queries)
				if(!preg_match('@^[0-9a-z_-]+$@i',$id   )){$id   ='';}
				if(!preg_match('@^[0-9a-z_\s-]+$@i',$class)){$class='';}
				$nodePathPart=['tagName'=>$n->nodeName];
				if($id   !==''){$nodePathPart['id'       ]=$id   ;}
				if($class!==''){$nodePathPart['className']=$class;}
				$nodePath[]=$nodePathPart;
			}
			//** Check if there is an acceptable CSS selector
			for($i=0;$i<count($nodePath);$i++)
			{
				require_once 'demosphere/html-selector/html-selector.php';
				$selector=html_selector_node_path_to_selector_for_pages($pages,array_slice($nodePath,$i));
				if($selector['selector']===false){continue;}
				//if($report['debug'])feed_import_form_progress( "ok candidate:".$selector['selector']);
				//lvd('selected content:',$node->textContent);
				$okSelectors[$selector['selector']]=1+val($okSelectors,$selector['selector'],0);
			}
		}
	}
	return $okSelectors;
}

//! Computes the following measures that can be used to compute a score for a selector:
//! - similarity between feed texts and selected text in pages
//! - amount of text common to all selected text in pages
//! - size of selected text in pages
function feed_import_form_evaluate_selector($selector,$pages,$siteInfo,$report)
{
	$unwantedPageParts=Article::$unwantedPageParts;

	// *** performance: remove $unwantedPageParts that do not match anything and save that for future calls
	static $cache=[];
	$cacheName=val(dlib_first($pages),'cacheName');
	if($cacheName!==false)
	{
		if(isset($cache[$cacheName])){$unwantedPageParts=$cache[$cacheName];}
		else
		{
			foreach(array_keys($unwantedPageParts) as $k)
			{
				$found=false;
				foreach($pages as $page)
				{
					$selected=$page['xpath']->evaluate(dlib_css_selector_to_xpath(dlib_array_column($unwantedPageParts,'arg')[$k]));
					if($selected!==false && $selected->length!=0){$found=true;break;}
				}
				if($found===false){unset($unwantedPageParts[$k]);}
			}
			$cache[$cacheName]=$unwantedPageParts;
		}
	}

	//*** select 
	$selected=[];
	foreach($pages as $n=>$page)
	{
		//if($report['debug'])feed_import_form_progress( "feed_import_form_evaluate_selector:page $n dlib_html_apply_selectors");
		$selHtml=dlib_html_apply_selectors($page['html'],array_merge([['type'=>'CSS','arg'=>$selector]],$unwantedPageParts));
		$selected[$n]=['html'=>$selHtml,
					   'text'=>dlib_simplify_text($selHtml,true),
					   'words'=>feed_import_form_html_to_words($selHtml),
					  ];
	}	

	$common=$selected[0]['words'];
	$scores=[];
	foreach($pages as $n=>$page)
	{
		require_once 'dlib/diff.php';
		$feedWords=$page['feedWords'];
		$selWords =$selected[$n]['words'];
		//lvd($feedWords,$selWords);
		
		$lcs      =feed_import_form_lcs($feedWords,$selWords)[0];
		$commonPos=feed_import_form_lcs($common   ,$selWords)[0];
		//$lcs      =longest_common_sequence_dp($feedWords,$selWords);
		//$commonPos=longest_common_sequence_dp($common   ,$selWords);
		$common=[];
		foreach($commonPos as $a=>$b){$common[]=$selWords[$b];}
		//foreach($commonPos as $a=>$b){$common[]=$b;}
		//lvd('feed desc:',$page['feedText'],'page text:',$selected[$n]['text'],'lcs:',$lcs);
		//lvd($page);
		if($report['debug'])
		{
			//lvd('$feedWords:',$feedWords,'$selWords:',$selWords,'lcs:',$lcs);
		}

		$scores[$n]=[
			'feedSelSimilarity' =>count($lcs)/(float)count($feedWords),
			'feedSelCommonWords'=>count($lcs),
			'feedWords'         =>count($feedWords),
			'selWords'          =>count($selWords),
			];
		if($report['debug'])
		{
			//lvd('scores for :'.$page['url'],$scores[$n]);
		}
		//die('pp');
	}

	foreach($pages as $n=>$page)
	{
		if(count($common)>1000){$scores[$n]['commonRatio']=1;}// performance
		$selWords =$selected[$n]['words'];
		$lcs=feed_import_form_lcs($common,$selWords)[0];
		// min(150,) : avoids that a very long text hides common content
		//$scores[$n]['commonRatio']=min(150,count($lcs))/min(150,count($selWords));
		$scores[$n]['commonRatio']=count($lcs)/100;
	}

	$feedSelSimilarities=dlib_array_column($scores,'feedSelSimilarity');
	sort($feedSelSimilarities);
	$medianFeedSelSimilarity=$feedSelSimilarities[floor(count($feedSelSimilarities)/2)];

	$commonRatios=dlib_array_column($scores,'commonRatio');
	sort($commonRatios);
	$medianCommonRatio=$commonRatios[floor(count($commonRatios)/2)];

	$sizes=dlib_array_column($scores,'selWords');
	sort($sizes);
	$medianSize=$sizes[floor(count($sizes)/2)];

	if($report['debug'])
	{
		//lvd('common:',implode(' ',$common));
		//lvd('scores:',$scores);
	}
	if($report['debug'])
	{
		$out='';
		foreach(array_values($common) as $n=>$w){$out.=explode(':',$w)[0].' ';if($n>50){$out.='...';break;}}
		feed_import_form_progress('words common to all pages: ('.count($common).') '.$out);
	}
	if($report['debug'])feed_import_form_progress(" ### medianFeedSelSimilarity:$medianFeedSelSimilarity  medianCommonRatio:$medianCommonRatio medianSize:$medianSize");
	return compact('medianFeedSelSimilarity','medianCommonRatio','scores','medianSize');
}



//*************************************************************
// Utility functions that might, someday, be usefull elsewhere
//*************************************************************

//! Wrapper around longest_common_sequence functions.
//! Adds caching and also choosest fastest algo according to list size.
function feed_import_form_lcs($list1,$list2)
{
	static $cache=[];
	$cs=md5(implode(':',$list1).';;;'.implode(':',$list2));
	if(isset($cache[$cs])){return $cache[$cs];}
	//echo "lcsfast:".count($list1).'x'.count($list2).":".(count($list1)*count($list2))."\n";

	if(count($list1)*count($list2)<0){$res=longest_common_sequence          ($list1,$list2);}
	else                             {$res=longest_common_sequence_unix_diff($list1,$list2);}
	$cache[$cs]=$res;
	return $res;
}

//! Splits a text into simplified words. Pairs of successive words are concatenated, this makes matching more robust.
//! Example: "Hello! my name is Tom" => ['hello:my','my:name','name:is','is:tom','tom:']
function feed_import_form_html_to_words($html)
{
	$words=explode(' ',dlib_simplify_text($html,true));
	// concat each 2 consecutive words to avoid bad lcs matches on simple common words
	$n=count($words);
	$res=[];
	foreach($words as $i=>$word)
	{
		$res[]=$word.':'.($i+1<$n ? $words[$i+1] : '');
	}
	return $res;
}

//! Recursivelly search for nodes that contain some text.
//! $words : a text to match, processed by feed_import_form_html_to_words()
//! Returns an array of DOMNode's
function feed_import_form_find_text_in_dom($node,$words)
{
	//echo "==================\n";
	//dlib_dom_print($node);
	if(count($words)===0){return [];}
	$nodeWords=feed_import_form_html_to_words($node->textContent);

	// performance: quick check before very long lcs 
	if(count(array_unique(array_intersect($nodeWords,$words)))<=2){return [];}

	$lcs=feed_import_form_lcs($nodeWords,$words)[0];
	//$lcs=longest_common_sequence_dp($nodeWords,$words);
	$matchRatio=count($lcs)/count($words);
	// No match if ratio too low or if two words or less.
	// However, if more than 20 words match.. then consider it a match anyways
	if(($matchRatio<.5 || count($lcs)<=2) && count($lcs)<20){return [];}
	//vd('ct',count($lcs),count($words),$matchRatio);
	$foundChildren=[];
	foreach($node->childNodes as $child)
	{
		if($child->childNodes===null){continue;}
		$childRes=feed_import_form_find_text_in_dom($child,$words);
		$foundChildren=array_merge($foundChildren,$childRes);
	}
	if(count($foundChildren)){return $foundChildren;}
	return [$node];
}

//! Tries to guess what type of Feed/FakeFeed should be created for a given url.
//! Returns an array describing the guessed type and some extra info that can be used when creating the Feed.
function feed_import_form_guess_feed_type($url)
{
	require_once 'dlib/download-tools.php';
	$opts['headers']=&$getHeaders;
	$opts['timeout']=20;
	$html=dlib_curl_download($url,false,$opts);

	$res=['url'=>$url,
		  'type'=>'normal',
		 ];

	if(preg_match('@^text/calendar\b@',$getHeaders['content-type'])){$res['type']='icalendar';return $res;}

	if(preg_match('@https://[^/]*\bfacebook\.[a-z]{2,6}/.+@',$url) ){$res['type']='facebook'; return $res;}

	if(preg_match('@https://[^/]*\btwitter\.com/([a-zA-Z0-9_]{1,15})@',$url,$m)){$res['type']='twitter';return $res;}

	if(preg_match('@^(text/html|application/xhtml(\+xml)?)@',$getHeaders['content-type']))
	{
		$feedUrl=feed_import_form_rss_link_in_html($url,$html);
		if($feedUrl!==false)
		{
			$res['url']=$feedUrl;
			return $res;
		}
		return ['error'=>t('Unable to find feed in page.')];
	}
	// Assume default is std xml/rss/atom feed (but we didn't really check it)
	return $res;
}


//! Searches for the first feed link <link rel="alternate" ...> inside an html page.
function feed_import_form_rss_link_in_html($url,$html)
{
	require_once 'dlib/html-tools.php';
	$doc=dlib_dom_from_html($html,$xpath);
	foreach($xpath->query("//link[@rel='alternate']") as $link)
	{
		if(preg_match('@^application/(rss|atom)(\+xml)?@i',$link->getAttribute('type')))
		{
			$feedUrl=$link->getAttribute('href');
			if(strpos($feedUrl,'/')===0)
			{
				if(!preg_match('@^https?://[^/]*@i',$url,$htmlUrlParts)){return false;}
				$base=$htmlUrlParts[0];
				$feedUrl=$base.$feedUrl;
			}
			else
			if(!preg_match('@^https?://@i',$feedUrl))
			{
				$dirUrl=$url;
				if(substr_count($dirUrl,'/')>2){$dirUrl=preg_replace('@/[^/]*$@','',$dirUrl);}
				$feedUrl=$dirUrl.'/'.$feedUrl;
			}
			return $feedUrl;
		}
	}
	return false;
}

//*************************************************************
//! temp testing
//*************************************************************

// Problems:
// * A lot of feeds do not have descriptions, or descriptions are too short. This includes CMS's. 
//    Examples: http://www.attac93sud.fr/spip.php?page=backend     
// * Some pages don't have id, classes or identificable tags. 
//         http://www.educationpopulaire93.fr/spip.php?page=backend&id_mot=1
//         http://deboulonneurs.org/spip.php?page=backend&id_rubrique=7
// * Pages with article footer without class/tags:
//         http://www.droitsdevant.org/spip.php?page=backend

// Misc: pdf or image only site: 
//        http://www.cnt-f.org/fedeptt/spip.php?page=backend
//        http://www.sudrailpse.org/site/feed/
// Sites without valid pages :
//       http://hopitalpourtous.blogspot.com/feeds/posts/default
// Spip site that has descriptions in feed but doesnt display contents on page (!!) http://www.survie-paris.org/spip.php?page=backend
// Spip site with "hot" article inside normal article https://lundi.am/spip.php?page=backend
// Spip site with a lot of content that changes on every page: http://www.oclibertaire.lautre.net/spip.php?page=backend

function feed_import_form_eval_all_feeds($startId=0)
{
	$reportFname='files/private/feed-import-guess-eval.json';
	$feeds=db_one_col('SELECT id,url FROM Feed WHERE id>=%d ORDER BY id ASC',$startId);
	$reports=[];
	if($startId===0){file_put_contents($reportFname,'');}
	foreach($feeds as $id=>$feedUrl)
	{
		if(strpos($feedUrl,'/fake-feed/')!==false){continue;}
		echo "\n\n\nXXXXXXXXXXXX $id:$feedUrl XXXXXXXXXXXXXXXXXXXXXXX\n\n";
		$start=microtime(true);
		$report=[];
		$selector=feed_import_form_guess_selector($feedUrl,$report);
		$report['id']=$id;
		$report['url']=$feedUrl;
		$report['time']=microtime(true)-$start;
		file_put_contents($reportFname,json_encode($report)."\n",FILE_APPEND);
	}
}

function feed_import_form_parse_reports()
{
	$reportFname='files/private/feed-import-guess-eval.json';
	$reportsRaw=trim(file_get_contents($reportFname));
	foreach(explode("\n",$reportsRaw) as $reportJson)
	{
		$reports[]=json_decode($reportJson,true);
		//lvd($report);
	}
	
	foreach($reports as $report)
	{
		if(!isset($report['selScores']))
		{
			echo sprintf('%3d',$report['id']).' : '."failed"." : ".
				implode('|',$report['errors']).' : '.
				$report['url'].

				"\n";
			continue;
		}
		$feed=Feed::fetch($report['id']);
		$score=dlib_first($report['selScores']);
		echo sprintf('%3d',$report['id']).' : '.
			($score<1.5 ? 'failed' : 'ok    ').' : '.
			sprintf('%3.3f',$score).' : '.
			sprintf('%20s',dlib_first_key($report['selScores'])).' : '.
			sprintf('%20s',count($feed->htmlContentSelector) ? dlib_first($feed->htmlContentSelector)['arg'] : '').' : '.
			sprintf('%10s',$report['siteInfo']['cms']).' : '.
			$report['url'].
			"\n";
		//if($report['id']==607){lvd($report);}
	}
}

function feed_import_form_update_reports()
{
	$reportFname='files/private/feed-import-guess-eval.json';
	$reportsRaw=trim(file_get_contents($reportFname));
	file_put_contents($reportFname.'.n','');
	foreach(explode("\n",$reportsRaw) as $reportJson)
	{
		$report0=json_decode($reportJson,true);
		echo "xxx ".$report0['url']."\n";
		$pages=feed_import_form_fetch_pages($report0['url'],$feedXml,$unused);
		if($pages!==false)
		{
			$siteInfo=feed_import_form_site_info($pages,$feedXml);	
			$report0['siteInfo']=$siteInfo;
			lvd($report0['siteInfo']);
		}
		file_put_contents($reportFname.'.n',json_encode($report0)."\n",FILE_APPEND);
	}
}


//! Wrapper around dcomponent_progress_log() that auto-starts.
function feed_import_form_progress($message,$level='status',$isRawHtml=false)
{
	static $hasStarted=false;
	if(!$hasStarted)
	{
		$isHtml=dlib_debug_use_html();
		dcomponent_progress_log_start($isHtml ? 'html' : 'text');
		$hasStarted=true;
	}
	dcomponent_progress_log($message,$level,$isRawHtml);
} 

//! var_dump alternative vd() that uses feed_import_form_progress() for output.
function lvd()
{
	ob_start();
	call_user_func_array('vd',func_get_args());
	$out=ob_get_contents();
	ob_end_clean();
	feed_import_form_progress($out,'status',true);
}

?>