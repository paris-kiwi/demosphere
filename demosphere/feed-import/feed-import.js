$(document).ready(function()
{
	// Feed manager page: Move pager and search, since we know there is empty space (rotated titles)
	$('body').on('demos:table-rotate-finished',function(e,table)
	{
		var margin=parseInt(table.css('margin-top'));
		var wrap=$('<div class="pager-wrap"></div>');
		var pager =table.parents('.table-render-wrapper').find('.pager');
		pager.before(wrap);
		wrap.append(pager);
		table.css('margin-top',0);
		wrap.css('margin-top',margin+'px');
		var pagerY=pager.offset().top;
		$('.page-title').offset({top:pagerY-40,left:0});
	});

	// Feed errors page: Hide error buttons
	$('#feed-errors .actions input').click(function(e)
	{
		var button=$(this);
		var feed=button.parents('.feed');
		$.post(feedImportScriptUrl+'/feed/'+feed.attr('data-id')+'/hide-errors',
			   {days:button.attr('data-duration')},
			   function(response)
			   {
				   feed.addClass('hide-errors');
				   feed.find('.until span').text(response.hideLabel);
				   feed.toggleClass('hide-errors',response.hide);
			   });
	});

	// Feed errors page: Show/hide error log
	$('#feed-error-log-fold>button').click(function(e)
	{
		$(this).parent().toggleClass('hidden');
	});

	// Feed view page : Show/hide code 
	$('#info-custom-code>span').mousedown(function(e)
	{
		$("#info-custom-code>pre").toggle();
	});

	// Feed edit page: If user clicks on the debug button with unsaved data, force save and make debug page wait for save to be over

	function feed_edit_serialized_form(){return $('#feed-edit').serialize().replace(/(^|&)lip-filter=[^&]*/,'');}

	$('#fake-feed-debug-link').click(function(e)
    {
		if($('#feed-edit').data('serialized')!==feed_edit_serialized_form())
		{
			var token=Math.floor(Math.random()*1000000);
			$('#fake-feed-debug-link').attr('href',
											$('#fake-feed-debug-link').attr('href').replace(/\?.*/,'')+'?wait-for-save='+token);
			$('#edit-fakeFeed-debug-wait-for-save').val(token);
			$('#edit-save').click();
		}
	});
	// Feed edit page: middle click is not caught by "click" ... simulate it.
	// https://stackoverflow.com/questions/20154901/why-does-middle-click-not-trigger-click-in-several-cases
	$('#fake-feed-debug-link').mousedown(function(e) 
	{
        if(e.which===2){$(this).data('down',true);}
    });
	$('#fake-feed-debug-link').mouseup(function(e) 
	{
        if(e.which===2 && $(this).data('down'))
		{
			$(this).data('down',false);
			$(this).click();
		}
    });

	// Feed edit page: If user clicks on Manual or Automatic selector links, warn of unsaved
	$('#selector-manual,#selector-auto').click(function(e)
    {
		if($('#feed-edit').data('serialized')!==feed_edit_serialized_form())
		{
			alert(t('unsaved-in-feed-edit-form'));
			e.preventDefault();
		}
	});

	// Feed edit page: FakeFeed only know lipLinkRegexp. 
	// This provides a simplified interface so that users can enter a simpler field "Only keep links starting with:".
	// They can optionally enter lipLinkRegexp
	$('#edit-fakeFeed-lipLinkRegexp').on('keyup change',function()
	{
		var ffUrl=$.trim($('#edit-fakeFeed-url').val());
		var ffHostUrl=ffUrl.replace(/^(https?:\/\/[^\/]+).*$/,'$1');
		var regexp=$.trim($(this).val());
		var isValidRegexp=/^@.*@[a-zA-Z]*$/.test(regexp);
		var isComplexRegexp=/[.:?{}\[\]\*\+()]/.test(regexp.replace(/^@\^?(.*)@[a-zA-Z]*/,'$1').replace(/\\([.:?])/g,'X'));
		var regexpToUrl=regexp.replace(/^@\^?(.*)@[a-zA-Z]*/,'$1').replace(/\\([.:?-])/g,'$1');

		if(regexp==='')
		{
			regexpToUrl=ffHostUrl;
			regexp='@^'+preg_quote(ffHostUrl)+'@';
			$('#edit-fakeFeed-lipLinkRegexp').val(regexp);
			isValidRegexp=true;
			isComplexRegexp=false;
		}
		if(!isValidRegexp || isComplexRegexp)
		{
			$('#lipLinkStarts').val('');
			$('#lipLinkStarts').prop('disabled',true);
			$('[name="lip-filter"]').eq(0).prop('checked',false);
			$('[name="lip-filter"]').eq(1).prop('checked',true);
		}
		else
		{
			$('#lipLinkStarts').val(regexpToUrl);
			$('#lipLinkStarts').prop('disabled',false);
		}

		if(!isValidRegexp){return;}

		if($('[name="lip-filter"]').eq(0).prop('checked')===false &&
		   $('[name="lip-filter"]').eq(1).prop('checked')===false  )
		{
			$('[name="lip-filter"]').eq(0).prop('checked',true)
		}

		if($('[name="lip-filter"]').eq(1).prop('checked')){$('#edit-fakeFeed-lipLinkRegexp').parent().show();}
	}).change();
	$('[name="lip-filter"]').eq(1).change(function()
	{
		$('#edit-fakeFeed-lipLinkRegexp').parent().show();
	});
	$('[name="lip-filter"]').eq(0).change(function()
	{
		if(!$(this).is(':checked')){return;}
		$('#edit-fakeFeed-lipLinkRegexp').parent().hide();
		if($('#lipLinkStarts').prop('disabled')===true)
		{
			$('#edit-fakeFeed-lipLinkRegexp').val('');
			$('#edit-fakeFeed-lipLinkRegexp').change();
		}
	});


	$('#lipLinkStarts').on('keyup change',function()
	{
		$('#edit-fakeFeed-lipLinkRegexp').val('@^'+preg_quote($(this).val())+'@');
	});
	$('#lipLinkStarts,#edit-fakeFeed-lipLinkRegexp').focus(function(){$(this).parents('li').eq(0).find('[name="lip-filter"]').click();});

	$('#edit-getContentFromFeedXml').change(function()
	{
		$('#selector-item').toggle(!$('#edit-getContentFromFeedXml').is(':checked'));
	}).change();

	// Do this after all js has changed form items
	$('#feed-edit').data('serialized',feed_edit_serialized_form());

});

// Quote regular expression characters plus an optional character  
// http://phpjs.org/functions/preg_quote
function preg_quote(str,delimiter) 
{
    return (str + '').replace(new RegExp('[.\\\\+*?\\[\\^\\]$(){}=!<>|:\\' + (delimiter || '') + '-]', 'g'), '\\$&');
}
