<?php
/** \addtogroup feed_import 
 * @{ */

/**
 * An Article is a feed item, it is associated to a Feed object.
 *
 * An Article is fully dowloaded from the web site 
 * (it is not only the short summary found in the feed). 
 * Articles contents is highlighted an higlighted information is cached for
 * faster retreival. After a user has checked the an article for interesting content
 * it is flaged as "finished" and will no longer appear in lisitings.
 * It is kept a while before being actually deleted.
 */
class Article extends DBObject
{
	//! unique autoincremented ID. 
	// $guid could be used, but it is too long to be a primary index for mysql (and it might not be reliable / cross-site)
	public $id=0;
	static $id_           =['type'=>'autoincrement'];
	//! unique identifier (provided by the feed).
	public $guid;
	static $guid_          =[];
	//! The html page of this article (provided by the feed).
	public $url;
	static $url_          =['type'=>'url'];
	//! The feed to which this article belongs.
	public $feedId=0;
	static $feedId_       =['type'=>'foreign_key','class'=>'Feed'];
	//! title for this article
	public $title;
	static $title_          =[];
	//! true: has been downloaded and parsed ok.
	public $downloadStatus;
	static $downloadStatus_=['type'=>'bool'];
	//! Last time this article was succesfully fetched (downloaded)
	public $lastFetched;
	static $lastFetched_  =['type'=>'timestamp'];
	//! Time at which this article was created in this software.
	public $created;
	static $created_      =['type'=>'timestamp'];
	//! the last xml feed item/entry found for this article.
	public $fullXml;
	static $fullXml_      =[];
	//! Last update date as appears in rss/atom feed.
	public $pubDate;
	static $pubDate_      =['type'=>'timestamp'];
	//! xhtml content fetched and parsed from  url.
	public $content;
	static $content_      =[];
	//! Highlighting can be long (dates/times), so we cache it here.
	public $highlightedContent;
	static $highlightedContent_=[];
	/** If  the content matches  some keywords  or future  dates. 
     *
	 * Note: dates that were "future" during	 parsing might	no	longer be
	 * future when  user actually reads	the content, so	 second check
	 * when reading is necessary.
	 */
	public $isHighlighted;
	static $isHighlighted_=['type'=>'bool'];
	public $highlightInfo;
	static $highlightInfo_=['type'=>'array'];
	//! If the article contains highlighted future dates, this is first future date.
	//! Note this needs to be updated regularly.
	public $firstFutureHLDate=0;
	static $firstFutureHLDate_=['type'=>'timestamp'];
	//! dlib_simplify_text()'ied version of this article to speedup searches in serchterm feeds.
	//! Also used by screenshot to find long article.
	public $simpleText;
	static $simpleText_=[];
	//! if the user has finished using the content.
	public $finishedReading;
	static $finishedReading_=['type'=>'timestamp'];
	//! Misc data for statistics, debuging and experimental features.
	//! used for 'errors'
	public $data;
	static $data_=['type'=>'array'];

	//! information for database load / save in DBObject.
	static $dboStatic=false;

	//!
	static $unwantedPageParts=[		
		['type'=>'CSS-REMOVE','arg'=>'.jp-relatedposts'], // wordpress "similar" articles
		['type'=>'CSS-REMOVE','arg'=>'#jp-relatedposts'], // wordpress "similar" articles
		['type'=>'CSS-REMOVE','arg'=>'div.wpa'], // wordpress ??
		['type'=>'CSS-REMOVE','arg'=>'.sd-social'],
		['type'=>'CSS-REMOVE','arg'=>'.sd-sharing-enabled'],
		['type'=>'CSS-REMOVE','arg'=>'#hierarchie'], // SPIP breadcrumb
		['type'=>'CSS-REMOVE','arg'=>'.breadcrumb'], // SPIP?
		['type'=>'CSS-REMOVE','arg'=>'.social'], // 
		['type'=>'CSS-REMOVE','arg'=>'.m-social-networks'], // overblog
		['type'=>'CSS-REMOVE','arg'=>'.post-footer'],
		['type'=>'CSS-REMOVE','arg'=>'.itemfooter'],//canalblog.com
		['type'=>'CSS-REMOVE','arg'=>'.newsAuthor,.newsCmd'],// SPIP bottom
		['type'=>'CSS-REMOVE','arg'=>'#contenu .outil'], // SPIP print
		['type'=>'CSS-REMOVE','arg'=>'.buttons img'], // joomla print
		['type'=>'CSS-REMOVE','arg'=>'a[href*="javascript"][title*="imprimable"]'], // SPIP print
		['type'=>'CSS-REMOVE','arg'=>'a[href*="javascript"][title*="Imprimer"][title*="article"]'], // SPIP print
		['type'=>'CSS-REMOVE','arg'=>'a.repondre[href*="forum"]'], // SPIP comment link
		['type'=>'CSS-REMOVE','arg'=>'.views-field-created'], // blogger date created
		['type'=>'CSS-REMOVE','arg'=>'a[name="fb_share"]'], // SPIP share
		['type'=>'CSS-REMOVE','arg'=>'.cartouche small .published,.cartouche small .author'], // SPIP date+author
		['type'=>'CSS-REMOVE','arg'=>'a[href*="spipdf_article"]'], // SPIP print
		['type'=>'CSS-REMOVE','arg'=>'#commentform'], // eklablog comments
		['type'=>'CSS-REMOVE','arg'=>'#articles-recents-auteur'], // SPIP author articles
		['type'=>'CSS-REMOVE','arg'=>'#mots-cles_associes'], // SPIP related articles (keywords)
		['type'=>'CSS-REMOVE','arg'=>'.formulaire_forum'], // SPIP comment
		['type'=>'CSS-REMOVE','arg'=>'.forum-repondre'], // SPIP comment
		['type'=>'CSS-REMOVE','arg'=>'.post-tags'], // wordpress
		['type'=>'CSS-REMOVE','arg'=>'.relatedposts'], // wordpress
		['type'=>'CSS-REMOVE','arg'=>'.sharedaddy'],
		['type'=>'CSS-REMOVE','arg'=>'.comments-link'], // wordpress
		['type'=>'CSS-REMOVE','arg'=>'.tags'], // wordpress
		['type'=>'CSS-REMOVE','arg'=>'.entry-footer'], // wordpress
		['type'=>'CSS-REMOVE','arg'=>'.entry-utility'], // wordpress
		['type'=>'CSS-REMOVE','arg'=>'.tags-link'], // wordpress
		['type'=>'CSS-REMOVE','arg'=>'.cat-link'], // wordpress
		['type'=>'CSS-REMOVE','arg'=>'.cat-links'], // wordpress
		['type'=>'CSS-REMOVE','arg'=>'.permalink-bookmark'], // wordpress
		['type'=>'CSS-REMOVE','arg'=>'.comments'], // wordpress? (and probably others)
		['type'=>'CSS-REMOVE','arg'=>'#comments'], // canalblog (and probably others)
		['type'=>'CSS-REMOVE','arg'=>'.comments-feed'], // SPIP comments
		['type'=>'CSS-REMOVE','arg'=>'.comment-form'], // SPIP comments
		['type'=>'CSS-REMOVE','arg'=>'.comment-entry'], // wordpress
		['type'=>'CSS-REMOVE','arg'=>'.comment-respond'], // wordpress
		['type'=>'CSS-REMOVE','arg'=>'.share'],
		['type'=>'CSS-REMOVE','arg'=>'.entry-meta'], // WordPress
		['type'=>'CSS-REMOVE','arg'=>'.post-meta'],
		['type'=>'CSS-REMOVE','arg'=>'.nav-previous'],
		['type'=>'CSS-REMOVE','arg'=>'.nav-next'],
		['type'=>'CSS-REMOVE','arg'=>'.postmetadata'], // WordPress
		['type'=>'CSS-REMOVE','arg'=>'.crp_related'], // WordPress
		['type'=>'CSS-REMOVE','arg'=>'.rubrique-tag'], // SPIP
		['type'=>'CSS-REMOVE','arg'=>'.action-links'], // Drupal
		['type'=>'CSS-REMOVE','arg'=>'.arbo'], // SPIP
		['type'=>'CSS-REMOVE','arg'=>'#socialtags'],
		['type'=>'CSS-REMOVE','arg'=>'.ob-Shares'], // overblog
		['type'=>'CSS-REMOVE','arg'=>'.ob-Related'], // overblog
		['type'=>'CSS-REMOVE','arg'=>'.juiz_sps_links'], // Juiz Social Post Sharer (wordpress plugin)
		['type'=>'CSS-REMOVE','arg'=>'.comment-respond'], 
		['type'=>'CSS-REMOVE','arg'=>'.twitter-share-button'],
		['type'=>'CSS-REMOVE','arg'=>'.addtoany_share_save_container'],
		['type'=>'CSS-REMOVE','arg'=>'.yarpp-related'],
		['type'=>'CSS-REMOVE','arg'=>'.no-display'], // rare spip
		['type'=>'CSS-REMOVE','arg'=>'.hidden'], // 
		['type'=>'CSS-REMOVE','arg'=>'.linkr-bm'], 
		['type'=>'CSS-REMOVE','arg'=>'.ob-ShareBar'],  // overblog share
		['type'=>'CSS-REMOVE','arg'=>'.toolbar'], 
		['type'=>'CSS-REMOVE','arg'=>'.synved-social-container'], 
		['type'=>'CSS-REMOVE','arg'=>'.entry-categories'], 




							  ]; 


	function __construct($guid,$url,$feedId)
	{
		$this->guid=$guid;
		// some feeds (overblog) may add spurious spaces around url 
		$url=trim($url);
		// very rare: some feeds (ex:www.thechangebook.org) include unencoded unicode in urls
		$url=preg_replace_callback('@[^a-zA-A0-9;/?:\@=&$_.!+*\'(),~%#-]@u',function($v){return urlencode($v[0]);},$url);
		$this->url=$url;
		$this->feedId=$feedId;
		$this->downloadStatus=false;
		$this->created=time();
		$this->data=['errors'=>[]];
	}

	function load()
	{
		parent::load();
		if(!is_array($this->data)){$this->data=['errors'=>[]];}
	}

	// $hintFeedId is only used to speedup the sql query (very slow!)
	public static function guidToId($guid,$hintFeedId=false)
	{
		if($hintFeedId!==false)
		{
			$id=db_result_check("SELECT id FROM Article WHERE ".
								  "feedId=%d AND guid='%s'",$hintFeedId,$guid);
		}
		else
		{
			$id=db_result_check("SELECT id FROM Article WHERE ".
								  "guid='%s'",$guid);
		}
		//echo "guid:$guid hintFeedId:$hintFeedId = id:$id<br/>";
		return $id;
	}

	function save()
	{
		parent::save();
		TMDocument::reindexTid('feed_import',$this->id);
		$this->searchReindex();
	}

	function delete()
	{
		parent::delete();
		require_once 'dcomponent/dcomponent-common.php';
		dcomponent_search_proxy_call_catch('demosphere_search_dx_delete_document','log','feed',$this->id);
		TMDocument::reindexTid('feed_import',$this->id);
	}

	public function setFullXml($item)
	{
		// use dom to create well formed xml document (simplexml does not)
		$doc = new DOMDocument('1.0','utf-8');
		$doc->formatOutput = true;
		$domnode = dom_import_simplexml($item);
		if($domnode===false){throw new Exception("XML parse for this article failed (1)");}

		$domnode = $doc->importNode($domnode, true);
		$domnode = $doc->appendChild($domnode);
		//echo "dom save:".$doc->saveXML()."\n";
		$this->fullXml=$doc->saveXML();
		if(strlen($this->fullXml)===0){throw new Exception("XML parse for this article failed (2)");}
	}
	public function setFinishedReading($time=false,$finish=true)
	{
		if($finish)
		{
			$this->finishedReading=$time!==false ? $time : time();
		}
		else
		{
			// "un"-finish
			$this->finishedReading=0;
		}
	}

	public function getId(){return $this->id;}
	public function getHighlightInfo(){return $this->highlightInfo;}

	public function needsUpdate(){return $this->downloadStatus==0;}

	public function update()
	{
		dcomponent_progress_log("Article::update:".$this->id);
		//feed_import_log("Article::update:".$this->id);
		$this->fetchAndCleanupContent();
		$this->highlightContent();
		$this->save();
		TMDocument::reindexTid('feed_import',$this->id);
		$this->searchReindex();
	}

	public function reset()
	{
		$this->downloadStatus=0;
		$this->lastFetched=0;
		$this->pubDate=0;
		$this->content=false;
		$this->highlightedContent=false;
		$this->simpleText=false;
		$this->finishedReading=false;
		$this->highlightInfo=false;
		$this->firstFutureHLDate=0;
	}

	private function fetchAndCleanupContent()
	{
		require_once 'dlib/html-tools.php';
		global $feed_import_config;
		dcomponent_progress_log("*** article>fetchAndCleanupContent:".ent($this->url));
		$now=time();
		$feed=Feed::fetch($this->feedId);
		$fakeFeed=$feed->getFakeFeed();
		$selector=$feed->htmlContentSelector;

		if(!$feed->getContentFromFeedXml)
		{
			if(strlen($this->url)===0){$this->failDownload("Article download failed: empty url!");}
			// normal case: actually download html for article
			// Get fixCharset from htmlContentSelector (FIXME: is this really used?)
			$fixCharset=false;
			foreach($selector as $s){if($s['type']=='fixCharset'){$fixCharset=$s['arg'];break;}}
			// trim : tmp fix (fixed in constructor)
			$html=$this->downloadHtml(trim($this->url),$info,$fixCharset);
		}
		else
		{
			// special case: use html directly from feed/article xml.
			$xml=simplexml_load_string($this->fullXml);
			require_once 'feed-import.php';
			$html=feed_import_feed_item_description($xml);
			$html='<html><head><meta charset="utf-8" /></head><body>'.$html;
			$info=[];
		}

		$fullHtml=$html;

		// tidy chokes on some blogspot html containing complex js
		if(strpos($this->url,'.blogspot.com')!==false)
		{
			$html=preg_replace('@<script.*</script>@Us','',$html);
		}
		try{$html=dlib_html_cleanup_page_with_tidy($html,false);}
		catch (Exception $e) 
		{
			$this->failDownload("html content select crashed with the following message:\n".
								$e->getMessage());
		}
		// FIXME: move this to cleanup_html_page_with_tidy ?
		// cleanup windows-1252 encoding mistaken as iso
		$html=str_replace("","€",$html);
		$html=str_replace("","...",$html);
		$html=str_replace('',"'",$html);
		$html=str_replace("","-",$html);// should be a dot
		$html=str_replace("","-",$html);
		$html=str_replace("","-",$html);
		$html=str_replace("","oe",$html);

		if(strlen($html)==0){$this->failDownload('html preproc and cleanup gives empty result');}

		// Remove common unwanted page parts (mostly added by CMS, blog platforms...)
		// share links, related pages, comments ...
		$manualSelectors=$selector;
		$selector=array_merge($selector,self::$unwantedPageParts);

		if(!$feed->getContentFromFeedXml && count($selector))
		{
			//echo "xpath:".ent($selector)."<br/>\n";
			//file_put_contents('/tmp/src.html',$html); // DEBUG
			$html0=$html;
			$html=dlib_html_apply_selectors($html,$selector);
			//file_put_contents('/tmp/txpath.html',$this->content);
			//fatal('test xpath');
			if(strlen($html)==0)
			{
				$redirect=val($info,'redirect');

				// If a selector fails on a link that is redirecting, it is probably a redirect to another site.
				// In that case, we try to use the description in the feed's XML (which might be absent)
				if($redirect!==false)
				{
					$xml=simplexml_load_string($this->fullXml);
					require_once 'feed-import.php';
					$html=feed_import_feed_item_description($xml);
				}
				
				if(strlen($html)==0)
				{
					$this->failDownload("html content select failed :\n".
										"url:".ent($this->url)."\n".
										"selector:".
										ent(implode("\n         ", 
													array_map(function($v){return implode('|',$v);},$manualSelectors)))."\n".
										'(auto remove: '.implode(';',dlib_array_column(array_slice($selector,count($manualSelectors)),'arg')).')'."\n".
										"html length before select:".strlen($html0)."\n".
										($redirect!==false ?
										 "Redirect detected:\n".$redirect."\n".
										 "This article might have redirected to a page on another site.\n".
										 "In that case, just check the link, and then finish this article.\n"
										 : "").
										"Log of download:\n".val($info,'log','empty')."\n");
				}
			}
		}
		// In all cases, from here on, we should have only content (no <html>,<head>,<body> tags)
		// This is needed when getContentFromFeedXml
		$html=preg_replace('@^.*<body\b[^>]*>@is','',$html);
		$html=preg_replace('@</body>.*$@is','',$html);

		// Simplify href & src urls and remove trackers (before proxying images)
		$html=dcomponent_url_remove_tracking_html($html);

		// Cache images locally to protect moderator's privacy (and avoid https errors)
		require_once 'demosphere-misc.php';
		$lastView=variable_get('feed-import-last-view-articles');
		$imageProxyDlNow=$lastView!==false && $lastView>time()-3600*24*5;
		$html=demosphere_image_proxy($html,$imageProxyDlNow);


		$sXml=simplexml_load_string($this->fullXml);
		if($sXml===false){trigger_error('Article::fetchAndCleanupContent fullXml parse failed: strange: Article id='.$this->id);}

		// Special case: fakefeed adds start time in xml
		$startTimeInXml=(string)($sXml->children("https://demosphere.net/custom/")->start);
		if($startTimeInXml!=='')
		{
			$html='<p class="start-time-in-xml">'.
				ent(dcomponent_format_date('full-date-time'.(date('G i',$startTimeInXml)=='0 00' ? '-date' : ''),$startTimeInXml)).
				'</p>'.
				$html;
		}

		// *** title

		// Default is RSS/Atom title. That usually is OK.
		$this->title=trim(dlib_cleanup_plain_text($sXml->title,true,true));
		$this->title=mb_substr($this->title,0,100);

		// If not, first try to find a title tag in the HTML <head>
		if($this->title==='')
		{
			if(preg_match('@<title[^>]*>(.*)</title>@Us',$fullHtml,$matches))
			{
				$this->title=trim($matches[1]);
				$this->title=html_entity_decode($this->title,ENT_COMPAT,'UTF-8');
				$this->title=preg_replace('@[ \n\t]+@',' ',$this->title);
			}
		}

		// Try extracting title from beginning of text
		if($this->title==='')
		{
			$this->title=dlib_fast_html_to_text($html);
			$this->title=preg_replace('@\s+@',' ',$this->title);
			$this->title=$feed->name.': '.mb_substr($this->title,0,80);
		}

		if($this->title===''){$this->title=t('(no title)');}

		// add h2 title to article, if it is not already there (easier for keywords and selection) 
		$simpleText =' '.dlib_simplify_text($html,true).' ';
		$simpleTitle=trim(dlib_simplify_text($this->title));
		$titles     =dlib_simplify_text(dlib_html_select_css($html,'h1,h2,h3,h4'),true);
		if($simpleTitle!=='' &&
		   strpos(trim($simpleText),$simpleTitle)!==0 &&
		   (strpos($titles         ,$simpleTitle)===false  || 
			strpos($titles         ,$simpleTitle)>150 ))
		{
			$html='<h2 class="demosphere-feed-import-title">'.ent($this->title)."</h2>\n".$html;
		}


		$this->content=$html;
		$this->highlightedContent=false;
		$this->simpleText=$simpleText;
		$this->highlightInfo=false;
		$this->downloadStatus=true;

		// Check if this is a duplicate Fakebook article.
		if($fakeFeed!==null && $fakeFeed->getType()==='facebook')
		{
			$existing=db_result("SELECT MIN(id) FROM Article WHERE id!=%d AND feedId=%d AND lastFetched>%d AND simpleText='%s'",
								$this->id,$feed->id,time()-3600*24*10,$this->simpleText);
			if($existing!==null)
			{
				// We can't just delete this article, otherwise it will be automatically re-created.
				$this->title='Facebook duplicate of '.$existing;
				$this->content='';
				$this->simpleText='';
				$this->finishedReading=$now;
			}
		}

		$dc=$sXml->children('http://purl.org/dc/elements/1.1/'); 
		//foreach($dc as $k=>$kw){echo "$k => $kw<br/>";}
		$this->pubDate=strtotime(isset(  $sXml->pubDate)   ? $sXml->pubDate :
								 (isset( $sXml->published) ? $sXml->published: 
								  (isset($dc->date)      ? $dc->date : 
								   false)));
		//echo 'pubdate:';
		//var_dump($this->pubDate);
		$this->lastFetched=$now;
	}

	//! This downloads HTML from given url, fixes charset related problems and reports errors.
	//! Note: fct should be static, but needs $this for error reporting.
	function downloadHtml($url,&$info,$fixCharset=false)
	{
		global $feed_import_config;
		require_once 'dlib/html-tools.php';
		require_once 'dlib/download-tools.php';

		$html=false;
		$getHeaders=true;
		// FIXME: we should only accept html content types
		// (but are content types always set?)
		// FIXME: in that case we could use download_and_clean_html_page
		require_once 'dlib/download-tools.php';
		// very special case: rare sites need cookies during redirects (language)
		$cookieFile=tempnam($feed_import_config['tmp_dir'],"feed-import-cookies-");
		$html=dlib_download_html_page($url,false,['log'=>$feed_import_config['log_file'],
												  'headers'=>&$getHeaders,
												  'info'=>&$info,
												  'timeout'=>300,
												  'allowNonHtmlContentTypes'=>true,
												  'cookiejar'=>$cookieFile,
												  ]);
		unlink($cookieFile);
		if($html===false)
		{
			$this->failDownload("Article download content failed\n".
								"Log of download:\n".val($info,'log','empty')."\n");
		}
		if($html==="")
		{
			$this->failDownload("Article download content failed (empty result)\n".
								"Log of download:\n".val($info,'log','empty')."\n");
		}
		if(strpos(val($getHeaders,'content-type',''),'application')!==false)
		{
			$this->failDownload("download: bad content type, aborting. Found: \n".
								ent($getHeaders['content-type']).
								(preg_match('@(image|pdf)@',$getHeaders['content-type'])?
								 "\n"."This does not seem to be a serious error.\n".
								 "It just means that an image or pdf document was found in this feed,\n".
								 "instead of an ordinary html page.\n".
								 "You should just click on the link\n".
								 "to check if this image or pdf contains an interesting event,\n".
								 "and then finish this article (click on the red cross)."
								 :'')."\n".
								"Log of download:\n".val($info,'log','empty')."\n"
								);
		}

		// Replace images by an <img>
		if(strpos(val($getHeaders,'content-type',''),'image')!==false)
		{
			$html='<img src="'.ent(u($url)).'"/>';
		}

		//  Overblog "lazy-load" images use data-src instead of src. Some Wordpress sites use data-lazy-src
		$html=preg_replace_callback('@<img[^>]*data(-lazy)?-src="[^">]*"[^>]*>@',function($m)
		{
			$res=$m[0];
			if(strpos($res,' data-lazy-src=')!==false)
			{
				$res=str_replace(' src=',' old-src=',$res);
				$res=str_replace(' data-lazy-src=',' src=',$res);
				return $res;
			}
			if(strpos($res,' src=')!==false){return $res;}
			$res=str_replace('data-src=','src=',$res);
			return $res;
		},$html);

		// FIX: < ? xml version=\"1.0\" encoding=\"utf-8\" ? >
		$html=preg_replace('@^(\s*<\?xml[^>]*)\\"@','"',$html);

		$contentType=$getHeaders['content-type'] ?? false;
		// Optionally override charset
		if($fixCharset!==false){$contentType='text/html; charset='.$fixCharset;}

		$html=dlib_html_convert_page_to_utf8($html,$contentType);

		return $html;
	}

	private function failDownload($message)
	{
		if(count($this->data['errors'])>10){array_shift($this->data['errors']);}
		$this->data['errors'][]=
			['time'=>time(),
			 'message'=>$message,
			];
		$this->downloadStatus=0;
		$this->content=null;
		$this->save();
		$fullMsg="Download or select failed for article ".$this->id." : '".$this->guid."'\n".
			"url:'".ent($this->url)."'\n".
			$message;
		feed_import_log($fullMsg);		
		throw new Exception($fullMsg);
	}

	// Checks content for future dates and keywords and sets highlightedContent, highlightInfo, firstFutureHLDate.
	// also updates simpleText
	function highlightContent()
	{
		global $feed_import_config;
		$hinfo=dcomponent_highlight($this->content,true,false,false);
		$this->highlightedContent=$hinfo['content'];
		unset($hinfo['content']);

		$this->isHighlighted=count($hinfo['timestamps'])>0 || 
			count($hinfo['keywordMatches'])>0;
		$hinfo['timestamps']=array_unique($hinfo['timestamps']);
		sort($hinfo['timestamps']);
		$hinfo['keywordCount']=count($hinfo['keywordMatches']);
		$t=$hinfo['keywordMatches'];
		foreach($t as $k=>$kw)
		{$hinfo['keywordMatches'][$k]=mb_strtolower(dlib_remove_accents($kw));}
		$hinfo['keywordMatches']=array_unique($hinfo['keywordMatches']);
		$this->highlightInfo=$hinfo;
		$this->firstFutureHLDate=
			count($hinfo['timestamps'])>0 ? min($hinfo['timestamps']) : false;

		return $this->highlightedContent;
	}

	public function reHighlightIfNeeded()
	{
		$limit=time()+3600*4;
		if($this->firstFutureHLDate<$limit)
		{
			$this->highlightContent();
			$this->save();
		}
	}

	public function viewData($isSingle=false)
	{
		global $feed_import_config,$base_url;
		$this->reHighlightIfNeeded();
		$res=[];
		$feed=Feed::fetch($this->feedId);
		$res['feed']=$feed;
		$res['classes']=($this->finishedReading ? 'finished ' : '').
			($this->downloadStatus ? '' : 'bad ').
			($isSingle ? 'single ' :'');

		$res['title']=$this->title;
		$res['finishUrl']='/article/'.$this->id.'/finish';
		$res['titleUrl']=$base_url.'/feed-import/article/'.$this->id.'/view-single';
		$res['events']=dcomponent_find_demosphere_event_references($this->content);

		$res['type']='article';

		$res['alink']=$this->url;

		$res['iframeUrl']=dcomponent_safe_domain_url('feed-import/article/'.$this->id.'/display-html');
		$res['highlightInfo']=$this->highlightInfo;
		$res['isHighlighted']=$this->isHighlighted;
		$thisDoc=TMDocument::getByTid('feed_import',$this->id);
		$res['textMatching']=$thisDoc===null?'':
			$thisDoc->displaySimilarDocInfo(false,$this->highlightInfo['timestamps']);
		return $res;
	}

	public function displayHtml()
	{
		global $feed_import_config;
		// This must be first as it adds http headers (Content-Security-Policy)
		$htmlHead=dcomponent_dcitem_inside_frame_js_and_css('feed-import',true);

		// If this article is bad, display the error message instead of article contents.
		if($this->downloadStatus==0)
		{
			echo "<h2>Error:</h2>";
			if(count($this->data['errors'])>0)
			{
				$error=$this->data['errors'][count($this->data['errors'])-1];
				echo '<strong>'.t('Last error').'</strong> : '.
					dcomponent_format_date('full-date-time',$error['time']).' : '.
					'<pre>'.ent($error['message']).'</pre>';
			}
			return;
		}

		global $base_url;
		$this->reHighlightIfNeeded();
		echo '<!DOCTYPE html>';
		echo '<html>';
		echo '<head>';
		echo  $htmlHead;
		echo '<link type="text/css" rel="stylesheet" href="'.ent($feed_import_config['dir_url']).'/feed-import-content.css">';
		echo '</head>';
		echo '<body data-demosphere-src="article-'.$this->id.'">';
		$old=time()-$feed_import_config['delete_old_articles'];
		if($this->highlightedContent==='' && 
		   ($this->lastFetched    >0 && $this->lastFetched    <$old ||
			$this->finishedReading>0 && $this->finishedReading<$old))
		{
			echo "This is an old article. It's content has been deleted.";
		}
		else
		{
			echo $this->highlightedContent;
		}
		echo '</body></html>';
	}
	
	function getFeed(){return Feed::fetch($this->feedId);}

	function getTmIndexContent()
	{
		return $this->content;// FIXME add title
	}
	function textMatchInfo()
	{
		global $base_url;
		$feed=Feed::fetch($this->feedId);
		$xml=simplexml_load_string($this->fullXml);
		return ['class'=>
				($this->finishedReading ? 'old' : 'recent').' '.
				'',
				'title'=>$this->title,
				'url'=>$base_url.'/feed-import/article/'.$this->id.'/view-single',
			   ];
	}

	function searchReindex($justReturnVals=false,$error='log')
	{
		require_once 'dcomponent/dcomponent-common.php';
		// special case: invalid article
		if(!$this->downloadStatus || $this->content==='')
		{
			if($justReturnVals){return ['title'=>'','publicText'=>'','privateText'=>''];}
			dcomponent_search_proxy_call_catch('demosphere_search_dx_delete_document',$error,'feed',$this->id);
			return;
		}

		$title=$this->title;
		$publicText='';
		$privateText=dlib_fast_html_to_text($this->content);

		if($justReturnVals){return compact('title','publicText','privateText');}

		dcomponent_search_proxy_call_catch('demosphere_search_dx_reindex_document',$error,
										   $this->id,$title,$publicText,$privateText,'feed',$this->lastFetched,$this->finishedReading==0);
	}

	public static function cleanupOld()
	{
		global $feed_import_config;
		if($feed_import_config['delete_old_articles']>0)
		{
			// we can't  just delete the articles:  Otherwise we loose
			// the  gids,  that say  "this  article  has already  been
			// downloaded"
			// This is only an issue for feeds that heven't changed for a long time.
			// So we just empty out the contents that takes space in the db

			$old=time()-$feed_import_config['delete_old_articles'];
			$ids=db_one_col("SELECT id FROM Article ".
						"WHERE (lastFetched>0 AND lastFetched<%d) OR ".
						"(finishedReading>0 AND finishedReading<%d)",
						$old,$old);
			if(count($ids)===0){return;}
			$q=db_query("UPDATE Article SET ".
						"content='',fullXML='',highlightedContent='',simpleText='',".
						"highlightInfo='',".
						"finishedReading=IF(finishedReading=0, %d,finishedReading) ".
						"WHERE id IN (".implode(',',$ids).")",
						time());
			
			dcomponent_search_proxy_call_catch('demosphere_search_dx_delete_documents','log','feed',$ids);
		}
	}

	//! Automatically finish articles after a while. The delay depends on the article's status.
	static function finishOldUnreadArticles()
	{
		$ids=db_one_col("SELECT id FROM Article WHERE ".
						"finishedReading=0 AND (".
						"    (created    <%d AND downloadStatus=0) OR ".
						"    (lastFetched<%d AND downloadStatus=1 AND firstFutureHLDate=0 AND isHighlighted=0) OR ".
						"    (lastFetched<%d AND downloadStatus=1 AND firstFutureHLDate=0 AND isHighlighted=1) OR   ".
						"    (lastFetched<%d AND downloadStatus=1)".
						")",
						strtotime("7 days ago"), // error articles
						strtotime("3 days ago"), // no  future dates, no  keywords
						strtotime("6 days ago"), // no  future dates, has keywords
						strtotime("4 months ago")// has future dates, has keywords
					   );
		if(count($ids)===0){return;}
		db_query("UPDATE Article SET finishedReading=%d WHERE id IN (".implode(',',$ids).")",time());

		// We need to reindex them for search ...
		foreach($ids as $id)
		{
			$article=Article::fetch($id);
			$article->searchReindex();
		}
	}

	// Rehighlight: some dates may no longer be in the future
	static function reHighlightIfNeededAll()
	{
		$articles=Article::fetchList("WHERE ".
									 "downloadStatus!=0 AND ".
									 "firstFutureHLDate!=0 AND ".
									 "firstFutureHLDate<%d ",
									 time()+3600*4);
		foreach($articles as $article)
		{
			$article->reHighlightIfNeeded();
		}
	}

	static function createTable()
	{
		db_query("DROP TABLE IF EXISTS Article");
		db_query("CREATE TABLE Article (
  `id` int(11) NOT NULL auto_increment,
  `guid` text  NOT NULL,
  `url` text  NOT NULL,
  `feedId` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `downloadStatus` TINYINT(1) NOT NULL,
  `lastFetched` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `pubDate` int(11) NOT NULL,
  `content` longtext  NOT NULL,
  `highlightedContent` longtext  NOT NULL,
  `simpleText` longtext  NOT NULL,
  `finishedReading` int(11) NOT NULL,
  `isHighlighted` TINYINT(1) NOT NULL,
  `highlightInfo` longtext  NOT NULL,
  `firstFutureHLDate` int(11)      NOT NULL,
  `fullXml` longblob NOT NULL,
  `data` longblob NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `guid` (`guid`(200)),
  KEY `feedId` (`feedId`),
  KEY `downloadStatus` (`downloadStatus`),
  KEY `lastFetched` (`lastFetched`),
  KEY `created` (`created`),
  KEY `pubDate` (`pubDate`),
  KEY `finishedReading` (`finishedReading`),
  KEY `isHighlighted` (`isHighlighted`),
  KEY `firstFutureHLDate` (`firstFutureHLDate`)
) DEFAULT CHARSET=utf8mb4;");
	}

}

/** @} */

?>