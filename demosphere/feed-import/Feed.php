<?php
/** \addtogroup feed_import 
 * @{ */


/**
 * A Feed object represents an  RSS or Atom feed.  
 * 
 * It does not contain individual articles  (that's the Article class).  
 * The  feed's XML file is downloaded  and stored as is 
 * (it  is not parsed into  PHP arrays or other). 
 * Parsing is done when needed using SimpleXML.  
 */
class Feed extends DBObject
{
	//! unique autoincremented ID. 
	public $id=0;
	static $id_           =['type'=>'autoincrement'];
	//! A human readable name for this feed.
	public $name;
	static $name_         =[];
	//! The URL of the RSS/Atom feed. Empty for FakeFeeds
	public $url;
	static $url_          =['type'=>'url'];
	//! Whether the last attempt resulted in a fully updated feed.
	//! (including XML download, parse and starting article updates).
	//! This does not say if the article updates actually succeeded, but only that they were tried.
	public $status=false;
	static $status_       =['type'=>'bool'];
	//! When was the last time $status was set to 1.
	public $lastOk=0;
	static $lastOk_       =['type'=>'timestamp'];
	//! When was the last time we called the update() function (even if it didn't finish)
	public $lastUpdated=0;
	static $lastUpdated_  =['type'=>'timestamp'];
	//! Timestamp that says until when errors from this feed are temporarily disabled.
	public $hideErrorsUntil;
	static $hideErrorsUntil_ =['type'=>'timestamp'];
	//! the last xml found for this feed
	public $fullXml;
	static $fullXml_      =[];
	//! An  xpath expression  matching  the content  in  html pages  of
	//! articles from this feed.
	public $htmlContentSelector=[];
	static $htmlContentSelector_=['type'=>'array'];
	//! Normally the user is warned if a feed contains no entries. 
	public $ignoreEmptyFeedError;
	static $ignoreEmptyFeedError_=['type'=>'bool'];
	//! Some feeds show https links but have invalid certificates. If this is selected
	//! https://example.org/article123 will be fixed to http://example.org/article123
	public $fixHttpsToHttp;
	static $fixHttpsToHttp_=['type'=>'bool'];
	//! Normally, feed import downloads articles from links in feeds. 
	//! With this option, feed import directly uses the html that is embeded in the feed's xml, without downloading the articles.
	public $getContentFromFeedXml;
	static $getContentFromFeedXml_=['type'=>'bool'];
	//! Optional user provided comment for this feed
	public $comment;
	static $comment_      =[];
	//! Optional user provided email associated to this feed. Helpfull for sending email after importing an event.
	public $email;
	static $email_        =['type'=>'email'];
	//! Error messages
	public $errors=[];
	static $errors_       =['type'=>'array'];
	//! Misc data for statistics, debugging and experimental features.
	//! List: screenshot information, pastRedirect, oldRedirectedUrls, (...?)
	public $data;
	static $data_         =['type'=>'array'];

	//! information for database load / save in DBObject.
	static $dboStatic=false;

    //! arguments: $load (load from database?), $id/$url
	function __construct($url)
	{
		$this->url=$url;
		require_once 'dcomponent/dcomponent-common.php';
		$this->name=dcomponent_url_to_name($url,'Feed');
	}

	function load()
	{
		parent::load();
	}

	//! Destroy  this feed and it's articles.  
	function delete()
	{
		require_once 'feed-import.php';
		feed_import_log("deleting feed and articles for feed ".$this->id);

		$ids=db_one_col("SELECT id FROM Article WHERE feedId=%d",$this->id);
		dcomponent_search_proxy_call_catch('demosphere_search_dx_delete_documents','log','feed',$ids);

		db_query("DELETE FROM Article  WHERE feedId=%d",$this->id);
		db_query("DELETE FROM FakeFeed WHERE feedId=%d",$this->id);		
		parent::delete();
	}

	public function link()
	{
		global $base_url;
		return $base_url.'/feed-import/feed/'.$this->id;
	}
	
	//! Returns a visitable page for this feed (not the url of a feed).
	//! $this->url:http://example.org/feed => http://example.org
	function guessSitePage()
	{
		$hostUrl=dlib_host_url($this->url);

		// FakeFeed's have a url, and normal Feed's should have a url in their <channel><link>
		$res=false;
		$fakeFeed=$this->getFakeFeed();
		if($fakeFeed!==null){$res=$fakeFeed->url;}
		else
		{
			if(trim($this->fullXml)!=='')
			{
				$xml=simplexml_load_string($this->fullXml);
				if($xml!==false)
				{
					if($xml->channel->link!==null)
					{
						// Check for invalid rss (link should be html website according to spec)
						if(((string)$xml->channel->link)!==$this->url){$res=(string)$xml->channel->link;}
					}
				}
			}
		}
		// If no valid url was found, use $hostUrl
		if(!preg_match('@^https?://[^/]+\.[^/]+@',$res)){$res=$hostUrl;}
		$res=preg_replace('@^(https?://[^/]*)/$@','$1',$res);
		$res=preg_replace('@^(https?://[^/]*)\?@','$1/?',$res);
		return $res;
	}

	//! Adds an error to this feed's error log and throws an exception.
	function error($label,$data=[])
	{
		$error['time']=time();
		$error['label']=$label;
		$error+=$data;

		// remove errors older than a month to save space
		$this->errors=array_filter($this->errors,function($e){return $e['time']>time()-3600*24*30;});
		
		array_unshift($this->errors,$error);

		$this->status=false;
		if(isset($error['hide_errors']) && $this->lastOk>0)
		{
			$this->hideErrorsUntil=max($this->hideErrorsUntil,$this->lastOk+$error['hide_errors']);
		}

		$this->save();

		require_once 'feed-import-view.php';
		$messages=feed_import_view_error_messages($label,$error,$this);
		$fullMsg="Error for feed feedId=".$this->id." : '".$this->name." : ".$error['label']." : ".$messages['short_message'].
			(isset($error['info']) ? " : http:".$error['info']['status'] : '').
			"' \n".
			$messages['message']."\n".
			(isset($messages['details']) ? $messages['details']."\n" : '').
			(isset($error['info']) && isset($error['info']['log']) ? "\n".$error['info']['log'] : '')
			;
		require_once 'feed-import.php';
		feed_import_log($fullMsg);
		$e=new Exception($fullMsg);
		$e->demosphere_error_label=$label;
		$e->demosphere_is_feed_error=true;
		throw $e;
	}

	//! Download, cleanup and validate XML feed.
	//! If successful, this object is saved to DB. 
	function update($verbose=true)
	{
		global $feed_import_config;
		require_once 'feed-import.php';
		feed_import_log("Feed::update:".$this->id);
		dcomponent_progress_log('******* Feed::update:'.intval($this->id).' : '.date('H:i').' : '.ent($this->name).' : '.ent($this->url));
		$this->lastUpdated=time();
		// update() function can abort before save()
		db_query('UPDATE Feed SET lastUpdated=%d WHERE id=%d',$this->lastUpdated,$this->id);

		// ***** Fetch XML, either by downloading $this->url or from FakeFeed
		$fakeFeed=$this->getFakeFeed();
		if($fakeFeed===null)
		{
			list($feedtext,$contentType)=$this->downloadNormalFeed();
		}
		else
		{
			try
			{
				$feedtext=$fakeFeed->feedXml($verbose ? false: null);
			}
			catch(Exception $e)
			{
				if(!isset($e->demosphere_is_fakefeed_error)){throw($e);}
				$this->error($e->getMessage(),$e->demosphere_data);
			}
			$contentType=false;
			if(strlen(trim($feedtext))==0){$this->error('ff_empty_file');}
		}


		$feedtext0=$feedtext;
		$feedtext=Feed::cleanupFeedXml($feedtext,$this->url,$contentType);

		if(trim($feedtext)==''){$this->error('empty_after_cleanup');}

		$this->fullXml=$feedtext;

		if(preg_match('@<html@si',$feedtext)){$this->error('feed_is_html');}

		// Make sure this feed parses OK
		$xml=simplexml_load_string($feedtext);
		if($xml===false)
		{
			$brokenFeedFile0=tempnam($feed_import_config['tmp_dir'],"broken-xml-feed-0-");
			file_put_contents($brokenFeedFile0,$feedtext0);
			$brokenFeedFile1=tempnam($feed_import_config['tmp_dir'],"broken-xml-feed-1-");
			file_put_contents($brokenFeedFile1,$feedtext);
			$this->error('xml_parse',['extra'=>dlib_show_xml_errors('',true,false),]);
		}

		// Check for valid feeds with no items
		$items=$this->getFeedItems();
		if(count($items)===0)
		{
			$last=$this->lastArticleTs();
			if($last!==null && $last< time()-3600*24*30*3)
			{
				$this->error('no_items_longtime',['last'=>$last]);
			}			
		}

		// *** Check if Feed url has been consistently redirecting for a month, and has ok Articles since. In that case, update url.
		if($fakeFeed===null)
		{
			if(!isset($dlInfo['redirect']) || !preg_match('@^https?://@',$dlInfo['redirect'])){unset($this->data['pastRedirect']);}
			else
			{
				$newUrl=$dlInfo['redirect'];
				if(!isset($this->data['pastRedirect']) || $this->data['pastRedirect']['newUrl']!==$newUrl)
				{
					$this->data['pastRedirect']=['newUrl'=>$newUrl,'ts'=>time()];
				}
				else
				{
					if($this->data['pastRedirect']['ts']<time()-3600*24*31)
					{
						$okArticlesSinceRedirect=db_result('SELECT COUNT(*) FROM Article WHERE feedId=%d AND downloadStatus!=0 AND lastFetched>%d',
														   $this->id,$this->data['pastRedirect']['ts']);
						if($okArticlesSinceRedirect>0)
						{
							$this->data['oldRedirectedUrls'][]=$this->url;
							$this->url=$newUrl;
							unset($this->data['pastRedirect']);
						}
					}
				}
			}
		}

		$this->save();
		return $xml;
	}

	//! Downloads the RSS/Atom url for a normal (not FakeFeed) Feed.
	//! Most of this function is about error handling and reporting.
	function downloadNormalFeed()
	{
		global $feed_import_config;
		require_once 'dlib/download-tools.php';
		$timeout=180;
		$feedtext=dlib_curl_download($this->url,false,['log'=>$feed_import_config['log_file'],
													   'headers'=>&$getHeaders,
													   'info'=>&$dlInfo,
													   'timeout'=>$timeout]);

		if($dlInfo['status']==0 && $dlInfo['curl_errno']===CURLE_OPERATION_TIMEDOUT)
		{
			$this->error('timeout',[
							 'hide_errors'=>2*24*3600,
							 'download'=>$dlInfo,
								   ]);
		}
			
		// Common case: check if whole site is down.
		// For that, we download the site's "front" page. 
		// If both the feed and the page are down ... then we guess that it's the whole site.
		if($dlInfo['status']>=400 || $dlInfo['status']===0)
		{
			$siteUrl=$this->guessSitePage();
			$siteHtml=dlib_curl_download($siteUrl,false,['log'=>$feed_import_config['log_file'],
														 'info'=>&$siteDlInfo,
														 'timeout'=>180]);
			if($siteDlInfo['status']===0 || 
			   $siteDlInfo['status']>=400)
			{
				$realUrl=$this->url;
				$realSiteUrl=$siteUrl;
				if(isset($dlInfo[    'redirect'])){$realUrl    =$dlInfo[    'redirect'];}
				if(isset($siteDlInfo['redirect'])){$realSiteUrl=$siteDlInfo['redirect'];}
				if(dlib_host_url($realUrl)===dlib_host_url($realSiteUrl))
				{
					$error['short_message']=t('Site is down.');
					// A site that is down is the most common problem. 
					// Most of the time, there is nothing the user can do about it.
					// In rare cases, the site has moved and user has to update the url.
					// => hide errors one week
					$this->error('site_down',[
									             'hide_errors'=>7*24*3600,
												 'download'=>$dlInfo,
												 'site_status'=>false,
												 'site_url'=>$siteUrl,
												 'site_download'=>$siteDlInfo,
											 ]);
				}
				else
				{
					// We were unable do find a reliable url for the site
					$this->error('feed_failed_no_site',['download'=>$dlInfo,]);
				}
			}
			else
			{
				// Check if the site page points to new feed url
				require_once 'feed-import-form.php';
				$newFeedUrl=feed_import_form_rss_link_in_html($siteUrl,$siteHtml);
				if($newFeedUrl===$this->url){$newFeedUrl=false;}

				$this->error('feed_failed_site_ok',[
								                       'download'=>$dlInfo,
													   'site_status'=>true,
													   'site_url'=>$siteUrl,
													   'site_download'=>$siteDlInfo,
													   'new_feed_url'=>$newFeedUrl
												   ]);
			}
		}

		if($feedtext===false)
		{
			$this->error('dl_failed_unknown',['download'=>$dlInfo,]);
		}
		if(trim($feedtext)==='')
		{
			$this->error('dl_empty_file',['download'=>$dlInfo,]);
		}
		return [$feedtext,val($getHeaders,'content-type')];
	}

	//! Returns ts of latest article of this feed with ok status.
	//! Returns null if no articles.
	function lastArticleTs()
	{
		return db_result('SELECT MAX(lastFetched) FROM Article WHERE feedId=%d AND downloadStatus!=0',$this->id);
	}

	//! Workaround common problems found in real-life feeds and clean them up, including with tidy
	static function cleanupFeedXml($feedtext,$url=false,$dlContentType=false)
	{
		$feedtext=trim($feedtext);
		// workaround for php bug in namespaces
		$feedtext=preg_replace("@xmlns=.http://[^'\"]*.@",'',$feedtext);

		// Garbage at begining of feed. (for example php error messages)
		if(!preg_match('@^<\?xml@',$feedtext) &&
		   preg_match('@<\?xml@',$feedtext))
		{
			$feedtext=preg_replace('@^.*<\?xml@sU','<?xml',$feedtext);
		}

		// Add missing xml declaration, using http content-type encoding
		if(!preg_match('@^<\?xml@',$feedtext))
		{
			if($dlContentType!==false && preg_match('@charset=([a-z0-9-]+)@i',$dlContentType,$m))
			{$charset=$m[1];}
			else
			{$charset='UTF-8';}
			$feedtext='<?xml version="1.0" encoding="'.$charset.'"?>'."\n".
				$feedtext;
		}

		// add encoding="utf-8" to opening xml tag if no encoding is specified
		if(preg_match( '@^[ \n]*<\?xml@si',$feedtext) &&
		   !preg_match('@^[ \n]*<\?xml[^>]*encoding=["\']([^"\']*)["\']@si',$feedtext))
		{
			$feedtext=preg_replace('@^<[?]xml @','<?xml encoding="UTF-8" ',$feedtext);
		}

		// fix all feed xml using tidy (we have tested that this does not break any good feeds).
		require_once 'dlib/html-tools.php';
		$feedtext=dlib_xml_convert_to_utf8($feedtext);
		$config = [
			          'doctype'          => 'strict',
					  'indent'           => true,
					  'output-xml'       => true,
					  'input-xml'        => true,
					  'numeric-entities' => true,
					  'wrap'             => 0
				  ];
		$feedtext=dlib_tidy_repair_string($feedtext,$config,"utf8");

		// very special: feed contains ascii 0 char
		$feedtext=str_replace(chr(0),'',$feedtext);

		// tidy doesn't remove stray content after end of rss tag
		// (for example, bad feeds may display php errors after feed).
		$feedtext=preg_replace('@(</rss>).*$@s','$1',$feedtext);

		return $feedtext;
	}

	//! returns a list of items in this feed  array(array('xml','link'(url),'guid')...)
	function getFeedItems()
	{
		require_once 'feed-import.php';
		$self=$this;
		return feed_import_feed_items($this->fullXml,$this->url,$this->ignoreEmptyFeedError,
									  function($msg)use($self){$self->error('xml_parse_2',['extra'=>$msg]);},
									  $this->fixHttpsToHttp);
	}

	//! Creates (or  loads, if it exists) and  updates (downloads) each
	//! article referenced by this feed. The actual work is done in the
	//! Article class.
	//! Special case: it also updates any error articles that are not in the feed (this can happen if articles have errors for a long time).
	function updateArticles($verbose=true)
	{
		$status=['ignored'=>0,'updated'=>0,'error'=>0];
		if(count($this->htmlContentSelector)===0 && !$this->getContentFromFeedXml)
		{
			$this->error('no_selector');
		}
		$updatedIds=[];
		//dcomponent_progress_log("*** getFeedItems ".ent($this->name));
		$items=$this->getFeedItems();
		foreach($items as $item)
		{
			if($verbose){dcomponent_progress_log("*** updating article guid:".ent($item['guid']));}
			$articleId=Article::guidToId($item['guid'],$this->id);
			// create new Article or load if already exists
			if($articleId===false)
			{
				$article=new Article($item['guid'],$item['link'],$this->id);
				$article->save();
			}
			else
			{
				$article=Article::fetch($articleId);
				if(!$article->needsUpdate())
				{
					$status['ignored']++;
					if($verbose){dcomponent_progress_log("no update needed");}
					continue;
				}
			}
			if(!$verbose){dcomponent_progress_log("*** updating article guid:".ent($item['guid']));}
			$article->setFullXml($item['xml']);
			$article->save();
			$updatedIds[]=$article->id;
			try
			{
				$article->update();
				$status['updated']++;
			}
			catch (Exception $e)
			{
				$status['error']++;
				dcomponent_progress_log('Feed '.$this->id.
										' : article update failed:'.ent($e->getMessage()));
				continue;
			}
		}

		//! Special case: update any error articles that are no longer in the feed (this can happen for old error articles).
		$errIds=db_one_col('SELECT id FROM Article WHERE feedId=%d AND downloadStatus=0 AND finishedReading=0 '.
						   (count($updatedIds) ?  'AND id NOT IN ('.implode(',',$updatedIds).')' :''),
						   $this->id);
		foreach($errIds as $errId)
		{
			$article=Article::fetch($errId);
			if(!$article->needsUpdate()){dcomponent_progress_log("no update needed");continue;}
			try
			{
				$article->update();
				$status['updated']++;
			}
			catch (Exception $e)
			{
				$status['error']++;
				dcomponent_progress_log('Feed '.$this->id.
										' : article update (second pass for err. articles) failed:'.ent($e->getMessage()));
				continue;
			}
		}

		$this->lastOk=time();
		$this->status=true;
		$this->save();
		return $status;
	}

	function finishArticles()
	{
		$ids=db_one_col("SELECT id FROM Article WHERE ".
						"Article.feedId=%d AND ".
						"Article.finishedReading=0 ",
						$this->id);
		if(count($ids)===0){return 0;}

		$now=time();
		db_query("UPDATE Article SET Article.finishedReading=%d WHERE id IN (".implode(',',$ids).")",$now);

		dcomponent_search_proxy_call_catch('demosphere_search_dx_set_documents_status','log','feed',$ids,0);
		return count($ids);
	}

	function screenshotNeedsUpdate()
	{
		if(!isset($this->data['screenshot'])){return true;}
		$cssSelector=false;
		if($this->usesCssSelector())
		{
			$cssSelector=$this->htmlContentSelector[0]['arg'];
		}
		if($this->data['screenshot']['feedUrl' ]!==$this->url             ){return true;}
		if($this->data['screenshot']['selector']!==$cssSelector           ){return true;}
		if($this->data['screenshot']['siteUrl' ]!==$this->guessSitePage() ){return true;}
		// Feed failed because no articles found, try again ... this is cheap (no screenshot made).
		// This happens, for example, after feed creation, before first update.
		if($this->data['screenshot']['url']===false                       ){return true;}
		// No settings have changed. If there was an error, don't insist, wait for 8h.
		if(val($this->data['screenshot'],'error',0)> time()-3600*8        ){return false;}
		if(isset($this->data['screenshot']['error'])                      ){return true;}
		// Refresh screenshot after 2 months
		if($this->data['screenshot']['time']       < time()-3600*24*30*2  ){return true;}
		return false;
	}
	function screenshotFile(){return "files/import-images/feed-import-feed-".$this->id.".png";}
	function screenshotImgUrl()
	{
		global $base_url;
		return $base_url.'/'.$this->screenshotFile().'?'.
			substr(md5(filemtime($this->screenshotFile())),0,6);
	}
	function screenshotUpdate()
	{
		global $feed_import_config;
		unset($this->data['screenshot']);

		$fakeFeed=$this->getFakeFeed();
		$cssSelector=false;
		if($this->usesCssSelector())
		{
			$cssSelector=$this->htmlContentSelector[0]['arg'];
		}

		// Data used by screenshotNeedsUpdate(). This needs to be set before any errors.
		$this->data['screenshot']['feedUrl' ]=$this->url;
		$this->data['screenshot']['siteUrl' ]=$this->guessSitePage();
		$this->data['screenshot']['selector']=$cssSelector;
		$this->data['screenshot']['time'    ]=time();
		$this->data['screenshot']['url'     ]=false;

		if($cssSelector===false)
		{
			$url=$this->guessSitePage();
			$this->data['screenshot']['url']=$url;
			$bigFile=dcomponent_browser_screenshot($url,1000,1300,['log'=>$feed_import_config['log_file']]);
			if($bigFile===false){$this->data['screenshot']['error']=time();return false;}
		}
		else
		{
			// Use url of a recently downloaded article. Try to find an article that has more text to avoid small selected area.
			// We also need to make sure it is in current feed (in case the feed's url has changed, for example, after edit form)
			$currentUrls=dlib_array_column($this->getFeedItems(),'link');
			if(count($currentUrls)===0){$this->data['screenshot']['error']=time();return false;}
			$url=db_result_check("SELECT url FROM Article WHERE feedId=%d AND downloadStatus=1 AND ".
								 "url IN ('".implode("','",array_map('db_escape_string',$currentUrls))."') ".
								 "ORDER BY LEAST(LENGTH(simpleText),500) DESC, lastFetched DESC LIMIT 1",$this->id);
			$this->data['screenshot']['url']=$url;
			if($url===false){$this->data['screenshot']['error']=time();return false;}
			$screenshotData=dcomponent_browser_screenshot_selected($url,$cssSelector,1000,1300,['log'=>$feed_import_config['log_file']]);
			if($screenshotData===false){$this->data['screenshot']['error']=time();return false;}
			$bigFile=$screenshotData['file'];
		}
		$resWidth=150;
		$ret=dlib_logexec("convert ".escapeshellarg($bigFile).
						  " -geometry ".$resWidth." ".escapeshellarg($this->screenshotFile()),$feed_import_config['log_file']);
		unlink($bigFile);
		if($ret!=0){unlink($this->screenshotFile());$this->data['screenshot']['error']=time();return false;}
		// Save selector box coordinates in data.
		if(isset($screenshotData))
		{
			foreach(['top'=>'y0','left'=>'x0','right'=>'x1','bottom'=>'y1'] as $pos=>$coord)
			{
				$this->data['screenshot']['selbox'][$coord]=$resWidth*$screenshotData[$pos]/1000;
			}
		}
		return true;
	}

	function usesCssSelector()
	{
		return !$this->getContentFromFeedXml &&
			val(val($this->htmlContentSelector,0,[]),'type')==='CSS';
	}


	function getFakeFeed()
	{
		$id=$this->getFakeFeedId();
		return FakeFeed::fetch($id,false);
	}

	function getFakeFeedId()
	{
		return db_result_check("SELECT id FROM FakeFeed WHERE feedId=%d",$this->id);
	}

	function stats()
	{
		static $now=false;
		if($now===false){$now=time();}
		$nbArticles=db_result('SELECT COUNT(id) FROM Article '.
								'WHERE feedId=%d',$this->id);
		$nbFinished=db_result('SELECT COUNT(id) FROM Article '.
								'WHERE feedId=%d AND '.
								'finishedReading!=0',$this->id);
		$nbBad     =db_result('SELECT COUNT(id) FROM Article '.
								'WHERE feedId=%d AND '.
								'finishedReading=0 AND '.
								'downloadStatus=0',$this->id);
		$mostRecent=(int)db_result('SELECT MAX(lastFetched) FROM Article '.
								'WHERE feedId=%d AND '.
								'downloadStatus!=0',$this->id);
		return compact('nbArticles','nbFinished','nbBad','mostRecent');
	}

	//! Returns true if we should skip updating this feed to save time.
	function throttle()
	{
		$delay=0;
		$stats=$this->stats();

		// Feed has been in error for a few days
		if(!$this->status && $this->lastOk<strtotime('-3 days'  )){$delay=24*3600;}
		// No articles for more than a few months
		if(           $stats['mostRecent']<strtotime('-3 months')){$delay=24*3600;}

		return time()<$this->lastUpdated+$delay-3600;
	}

	//! Returns an html table with all available feeds.
	static function publicList()
	{
		$out='';
		$out.='<span class="feed-public-list">';
		$feeds=Feed::fetchList('WHERE 1 ORDER BY id DESC');
		$ct=0;
		foreach($feeds as $feed)
		{
			$url=$feed->guessSitePage();
			$out.= '<span><a href="'.ent(u($url)).'">'.ent($feed->name).'</a>, </span>';
		}
		$out.='</span>';
		return $out;
	}

	//! Update feeds. This should be called regularly from a cron job.
	static function updateAll($isCron=false)
	{
		ini_set('max_execution_time', max(1200,ini_get('max_execution_time')));
		require_once 'feed-import.php';
		feed_import_log("updateAll");

		$lastViewed=variable_get('feed-import-last-view-articles');
		if($lastViewed<(time()-3600*24*30*4))
		{
			dcomponent_progress_log('***Feed::updateAll : abort: feed-import not viewed for a long time');
			return;
		}

		dcomponent_progress_log('***Feed::updateAll***');
		$feedIds=db_one_col("SELECT id FROM Feed");
		foreach($feedIds as $id)
		{
			$feed=Feed::fetch($id);
			if($isCron && $feed->throttle()){continue;}
			try
			{
				$feed->update(!$isCron);
				if(!$isCron){dlib_show_xml_errors("update feed rss/xml ".$feed->name);}
				// ***
				$feed->updateArticles(!$isCron);
				if(!$isCron){dlib_show_xml_errors("update feed articles ".$feed->name);}

				if($feed->screenshotNeedsUpdate()){$feed->screenshotUpdate();$feed->save();}
			}catch (Exception $e) 
			 {
				 dcomponent_progress_log('feed update failed:'.ent($e->getMessage()));
				 continue;
			 }
			if(!$isCron){dlib_show_xml_errors("updateAll");}
			else        {libxml_clear_errors();}
			flush();
		}
	}

	static function createTable()
	{
		db_query("DROP TABLE IF EXISTS Feed");
		db_query("CREATE TABLE Feed (
  `id` int(11) NOT NULL auto_increment,
  `url` TEXT  NOT NULL,
  `name` varchar(1000)  NOT NULL,
  `status` TINYINT(1) NOT NULL,
  `lastOk` int(11) NOT NULL,
  `lastUpdated` int(11) NOT NULL,
  `hideErrorsUntil` int(11) NOT NULL,
  `htmlContentSelector` longblob  NOT NULL,
  `fullXml` longblob NOT NULL,
  `comment` longtext NOT NULL,
  `email` varchar(250) NOT NULL,
  `errors` longblob NOT NULL,
  `data` longblob NOT NULL,
  `ignoreEmptyFeedError` TINYINT(1) NOT NULL,
  `fixHttpsToHttp` TINYINT(1) NOT NULL,
  `getContentFromFeedXml` TINYINT(1) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `status` (`status`),
  KEY `lastOk` (`lastOk`),
  KEY `hideErrorsUntil` (`hideErrorsUntil`)
) DEFAULT CHARSET=utf8mb4;");
	}
}


/** @} */

?>