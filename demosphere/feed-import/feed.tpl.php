<div id="feed-blocks">

	<div id="info-block">
		<ul>
			<? if($fakeFeed===null){?>
				<li class="url-info">
					<span>_(Feed url:)</span>
					<a class="url" href="<?: u($feed->url) ?>">$feed->url</a>
				</li>
				<li class="url-info">
					<span>_(Site url:)</span>
					<a class="url" href="<?: u($feed->guessSitePage()) ?>">$feed->guessSitePage()</a>
				</li>
			<?}else{?>
				<li>_(Special feed:) <strong>$fakeFeed->getType()</strong><li>
				<li class="url-info">
					<span>_(Url:)</span>
					<a class="url" href="<?: u($fakeFeed->url) ?>">$fakeFeed->url</a>
				</li>
				<? if($fakeFeed->getType()==='custom-code'){?>
					<li><div id="info-custom-code"><span>[code]</span><pre>$fakeFeed->ccPhpCode</pre></div></li>
				<?}?>
				<? if($fakeFeed->getType()==='icalendar'){?>
					<li><a class="url" href="<?: u($fakeFeed->iCalUrl)?>">$fakeFeed->iCalUrl</a></li>
				<?}?>
				<? if($fakeFeed->getType()==='page-parts'){?>
					<?if($fakeFeed->ppCssSelector!==''){?>
						<li>_(Parts of page defined by:) $fakeFeed->ppCssSelector</li>
					<?}?>
					<?if($fakeFeed->ppSplitRegex!==''){?>
						<li>_(Parts of page split by:) $fakeFeed->ppSplitRegex</li>
					<?}?>
				<?}?>
			<?}?>
			<li>_(Latest article:) <?: dcomponent_format_date('relative-short',$latestArticle) ?></li>
		</ul>
	</div>

	<div id="status-block">
		<div id="status-line">		
			<a <?= !$feed->status   ? 'href="'.ent($feed->link().'/errors').'"' : '' ?> class="indicator status-<?: intval($feed->status) ?>">
				feed
			</a>
			<a <?= $nbErrorArticles ? 'href="'.ent($feed->link().'/errors').'"' : '' ?> class="indicator status-<?: intval($nbErrorArticles==0) ?>">
				 <?: $nbErrorArticles>0 ? $nbErrorArticles : '' ?> articles
			</a>
		</div>
		<? if(!$feed->status && count($feed->errors)){?>
			<ul>
				<li>
					<a id="error-message" href="$feed->link()/errors">
						<?: feed_import_view_error_message($feed,'short_message') ?>
					</a>
				</li>
				<li>_(Feed down since:) <?: dcomponent_format_date('relative-short-full',$feed->lastOk) ?></li>
				<? if($feed->hideErrorsUntil>time()){?>
					<li>_(Hide errors until:) <?: dcomponent_format_date('relative-short-full',$feed->hideErrorsUntil) ?></li>
				<?}?>
			</ul>
		<?}?>
	</div>


	<? $screenshotBlock=feed_import_view_screenshot($feed); ?>
	<? if($screenshotBlock!==''){?>
		<div id="screenshot-block">
			<?= $screenshotBlock ?>
		</div>
	<?}?>
	<div id="latest-finished">
		<h4>_(Latest finished:)</h4>
		<table>
			<? foreach($latestFinished as $article){?>
				<tr>
					<td><?:dcomponent_format_date('relative-short',$article->finishedReading) ?></td>
					<td><a href="$scriptUrl/article/$article->id/view-single"><?: $article->title==='' ? t('no title') : $article->title?></a></td>
				</tr>
			<?}?>
		</table>
		<?if(!count($latestFinished)){?>
			_(None)
		<?}else{?>
			<a href="$scriptUrl/view-recently-finished-articles?feedId=$feed->id">_([more])</a>
		<?}?>
	</div>

</div><!-- end feed-blocks -->


<div id="feed-view-articles">
	<div id="feed-view-pager-line">
		<?= render('dlib/pager.tpl.php') ?>
		<div id="feed-action-line">
			<a  id="update-feed-link" class="top-button icon-add"  href="$feed->link()/update">_(Update feed)</a>
			<? if(count($articles)){?>
				<a id="finish-feed-articles" class="top-button icon-add" href="$scriptUrl/article/finish-confirm/feed?feedId=$feed->id">
					<?: t('Finish !nb articles',['!nb'=>$pager->nbItems]) ?>
				</a>
			<?}?>
			<a  id="delete-feed-link" class="top-button icon-add"  href="$feed->link()/delete">_(Delete feed)</a>
		</div>
	</div>
	<? foreach($articles as $item){ ?>
		<? extract($item->viewData()) ?>
		<?= render('dcomponent/dcitem.tpl.php',
				render_parts('article-item.tpl.php')); ?>
	<?}?>
</div>

