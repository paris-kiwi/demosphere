<?php


function test_feed_import()
{
	require_once 'dlib/testing.php';
	require_once 'feed-import/feed-import.php';
	require_once 'dcomponent/dcomponent-common.php';
	//test_feed_import_feed();
	test_feed_import_form();
}

function test_feed_import_feed()
{
	$f1=new Feed('http://paris.local/events.xml');
	$f1->htmlContentSelector=[['type'=>'CSS','arg'=>"#ideventMain"]];
	$f1->save();
	$tf1=Feed::fetch($f1->id);
	test_equals($f1->url,$tf1->url);

	try
	{
		$f1->update();
		$f1->updateArticles();
	}
	catch (Exception $e) 
	{
		echo "exception:$e\n";
		test_assert(false);
	}
}


function test_feed_import_form()
{
	require_once 'dlib/testing.php';
	require_once 'feed-import/feed-import-form.php';

	// *** test 

	//$doc=dlib_dom_from_html('<div>hi there, how <p id="xx2">are you ? <span id="xx2">I am fine and you?</p><p> Nice day, isnt it?</p></div><div>zz</div>');
	$doc=dlib_dom_from_html('<div>hi there, how <p id="xx2">are you ? <span id="xx2">I am fine and you?</p><p> Nice day, isnt it?</p></div><div>zz</div>');
	$search=feed_import_form_html_to_words('I am fine and you.');
	//dlib_dom_print($doc);
	$found=feed_import_form_find_text_in_dom($doc,$search);
	//lvd('found res:',$found);
	//foreach($found as $node)
	//{
	// 	lvd('found:');
	// 	dlib_dom_print($node);
	//}
	test_equals(count($found),1);	
	test_equals($found[0]->getAttribute('id'),'xx2');	

}

?>