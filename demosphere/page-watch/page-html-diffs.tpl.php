<table id="page-html-diffs">
	<tr>
		<th>t0 remove</th>
		<th>t1 insert</th>
	</tr>
	<? foreach($diffs as $diff){ ?>
		<tr>
			<td style="width:50%;">
				<?= str_replace("\n",'<br/>',ent($diff['remove']))?>
			</td>
			<td>
				<?= str_replace("\n",'<br/>',ent($diff['insert']))?>
			</td>
		</tr>
	<?}?>
</table>
