<?php

//! Returns html that displays help and config buttons, only if they are actually present on current page.
//! Actual help data is only fetched by Ajax, so there is not even loaded by this function.
//! This is called on many pages, so we need to keep it lightweight.
function demosphere_help_buttons($onlyHelp=false)
{
	global $currentPage;
	$res=[];
	$hasConfig=false;
	if(!$onlyHelp){$hasConfig=demosphere_config_is_on_this_page();}
	$hasHelp=demosphere_help_is_on_this_page();
	if($hasConfig){$res['config']='<span id="config-main-button" onmousedown="dhelp(event,\'config\')" title="'.ent(t('configuration for items on this page')).'"></span>';}
	if($hasHelp  ){$res['help'  ]='<span id="help-main-button"   onmousedown="dhelp(event,\'help\')"   title="'.ent(t('help for items on this page')).'"></span>';}
	if($hasConfig || $hasHelp)
	{
		require_once 'demosphere-common.php';
		$helpJs=demosphere_cacheable('demosphere/js/demosphere-help.js');
		$jqJs=demosphere_cacheable('lib/jquery.js');
		$currentPage->addJs('function dhelp(e,h){e.preventDefault();'.
							'if(typeof demosphere_help==="undefined"){add_script("'.$helpJs.'",false,'.
							' function(){demosphere_help.main_button(null,h,"'.$jqJs.'");});}'.
							'else{demosphere_help.main_button(e,h);}}',
							'inline');
		$currentPage->addCssTpl('demosphere/css/demosphere-help.tpl.css');
	}
	return $res;
}

//! Check if there help is available on this page.
//! This is called on each page, so we need to keep it lightweight.
function demosphere_help_is_on_this_page()
{
	global $user;
	//$url=demosphere_path_alias_src($_GET['q']); This would be better, but adds extra sql request to all pages
	$url=$_SERVER['REQUEST_URI'];
	$selectors=variable_get('help_path_selectors');
	if($selectors===false)
	{
		$post=Post::fetch(Post::builtInPid('help_data'));
		demosphere_help_parse_data($post->body);
		$selectors=variable_get('help_path_selectors');
	}

	// check if this page url matches at least one selector
	foreach($selectors as $selector)
	{
		if(preg_match($selector[0],$url))
		{
			$options=val($selector,1,[]);
			if(val($options,'eventAdminOnly') && !$user->checkRoles('admin','moderator')){continue;}
			return true;
		}
	}
	return false;
}

function demosphere_config_is_on_this_page()
{
	global $user;
	$url=$_SERVER['REQUEST_URI'];
	$mod=$user->checkRoles('admin') ? '' : '_moderator';
	$selectors=variable_get('config_path_selectors'.$mod,'',[]);

	// ** if $selectors is too old, rebuild it
	$mtime=array_shift($selectors);
	if($mtime<filemtime(__FILE__))
	{
		demosphere_config_buttons();
		$selectors=variable_get('config_path_selectors'.$mod);
		array_shift($selectors);
	}

	// check if this page url matches at least one selector
	foreach($selectors as $selector)
	{
		if(preg_match($selector,$url)){return true;}
	}
	return false;
}

function demosphere_help_json()
{
	global $base_url,$user;
	$isAdmin=$user->checkRoles('admin');
	$isEventAdmin=$user->checkRoles('admin','moderator');
	$url=str_replace($base_url,'',$_GET['url']);
	$url=preg_replace('@#.*$@','',$url);// remove fragment

	$help  =variable_get('help_data');
	$config=demosphere_config_buttons();
	// build result array with items having a pathSelect that matches $url
	$out=['help'=>[],'config'=>[]];
	foreach(['help','config'] as $type)
	{
		$data=$type=='help' ? $help : $config;
		foreach($data as $item)
		{
			if(!preg_match($item['pathSelect'],$url)){continue;}
			if($type==='config' && !$isAdmin)
			{
				if(!$isEventAdmin || !isset($item['moderator'])){continue;}
			}
			if($type==='help' && !$isEventAdmin && val($item,'eventAdminOnly')){continue;}
			$out[$type][]=$item;
		}
	}
	$out['helpEditUrl']=$base_url.'/post/'.Post::builtInPid('help_data');
	return $out;
}

function demosphere_help_parse_data($helpPostBody)
{
	global $demosphere_config;
	require_once 'dlib/filter-xss.php';
	$helpPostBody=filter_xss_admin($helpPostBody, 
								   ['a','em','strong','cite','code','ul','ol','li','dl','dt','dd',
									'h1','h2','h3','h4','h5','h6','img','p','hr','b','i','br']);
	$parts=preg_split('@<hr\b[^>]*>@i',$helpPostBody);
	$help=[];
	$helpPathSelectors=[];
	foreach($parts as $n=>$part)
	{
		$ok=preg_match('@^.*HELPDESC;([^<]+)</p>([\t\n\r ]*<h2>([^<]*)</h2>)?(.*)$@s',
					   $part,$matches);
		//var_dump($matches);
		if(!$ok)
		{dlib_message_add("help : failed to parse part $n");continue;}
		$desc=$matches[1];
		$desc=dlib_html_entities_decode_all($desc);
		$desc=trim($desc);
		$dparts=preg_split('@(?<!\\\);@',$desc);
		$dparts=str_replace('\;',';',$dparts);
		if(count($dparts)<2)
		{dlib_message_add("help : bad desc format on part $n",'error');continue;}
		if(!preg_match('@^(.).*(\1)[ixADSUXJu]*$@',$dparts[0]))
		{dlib_message_add("help : invalid path regexp for part $n",'error');continue;}
		$pathSelect=array_shift($dparts);
		// Hack for changing event path names
		$pathSelect=preg_replace('@\bevent\b@',$demosphere_config['event_url_name'],$pathSelect);
		$desc=[
			   'pathSelect'=>$pathSelect,
			   'htmlSelect'=>array_shift($dparts),
			   'title'     =>filter_xss_admin($matches[3]),
			   'contents'  =>filter_xss_admin($matches[4]),
			  ];
		foreach($dparts as $dpart)
		{
			if(trim($dpart)===''){continue;}
			if(!preg_match('@^([^=]+)(=(.*))?$@',$dpart,$m)){dlib_message_add('bad option:'.ent($dpart),'error');continue;}
			$name=$m[1];
			$value=val($m,3,true);
			if(array_search($name,['xoffset','yoffset','noerror','eventAdminOnly',
								   'center','right','middle','bottom','label'])===false){dlib_message_add('bad option name:'.ent($name),'error');continue;}
			if($name==='label' && !preg_match('@^[a-z][a-z0-9_-]+$@i',$value)){dlib_message_add('invalid label name:'.ent($value),'error');continue;}
			$desc[$name]=$value;
			if(is_numeric($value)){$desc[$name]=0+$value;}
		}

		$sel=[$pathSelect];
		$selOptions=array_intersect_key($desc,array_flip(['eventAdminOnly']));
		if(count($selOptions)){$sel[]=$selOptions;}
		$helpPathSelectors[]=$sel;
		$help[]=$desc;
	}
	//var_dump($help);

	// Use seperate $help and $pathSelectors arrays for performance:
	// This way we can quickly load selectors and check if th page url matches
	// without having to load all the large help data.
	variable_set('help_data','',$help);
	variable_set('help_path_selectors','',array_unique($helpPathSelectors,SORT_REGULAR));
}


function demosphere_config_buttons()
{
	global $demosphere_config,$base_url;
	$event=$demosphere_config["event_url_name"];

	$buttons=
	[
	 // ********** misc-config
	 [
	  'pathSelect'=>'@^/$@',
	  'htmlSelect'=>'#secondary-links',
	  'config'=>'secondary_links',
	 ],
	 [
	  'pathSelect'=>'@^/publish\b@',
	  'htmlSelect'=>'#main',
	  'config'=>'public_form_open_publishing',
	 ],
	 [
	  'pathSelect'=>'@^/publish\b@',
	  'htmlSelect'=>'#main',
	  'config'=>'show_event_publish_donate_message',
	  'yoffset'=>24,
	 ],
	 [
	  'pathSelect'=>'@^/publish\b@',
	  'htmlSelect'=>'#information',
	  'config'=>'public_form_ask_price',
	  'yoffset'=>50,
	 ],
	 [
	  'pathSelect'=>'@^/control-panel@',
	  'htmlSelect'=>'a[href*="cache-clear"]',
	  'config'=>'page_cache_enable',
	  'yoffset'=>24,
	 ],
	 [
	  'pathSelect'=>'@^/control-panel@',
	  'htmlSelect'=>'a[href*="cache-clear"]',
	  'config'=>'compress_css',
	  'yoffset'=>24,
	  'xoffset'=>120,
	 ],
	 [
	  'pathSelect'=>'@^/$@',
	  'htmlSelect'=>'.dr1 .cx,.dr2 .cx',
	  'config'=>'dayrank_enable',
	  'xoffset'=>-25,
	  'noerror'=>true,
	 ],
	 [
	  'pathSelect'=>'@^/$@',
	  'htmlSelect'=>'.dr1 .cx,.dr2 .cx',
	  'config'=>'dayrank_min_visits',
	  'yoffset'=>17,
	  'xoffset'=>-25,
	  'noerror'=>true,
	 ],
	 [
	  'pathSelect'=>'@^/(control-panel)?$@',
	  'htmlSelect'=>'#eventAdminMenubar>ul',
	  'yoffset'=>7,
	  'config'=>'admin_menu',
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/[0-9]+$@',
	  'htmlSelect'=>'#demosShareLink',
	  'config'=>'share_links',
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/[0-9]+$@',
	  'htmlSelect'=>'#eventFrontpageTitle',
	  'config'=>'show_title_in_event',
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/[0-9]+$@',
	  'htmlSelect'=>'#commentLink',
	  'config'=>'comments_site_settings',
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/[0-9]+$@',
	  'htmlSelect'=>'#carpoolLink',
	  'config'=>'carpool_enable_default',
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/[0-9]+$@',
	  'htmlSelect'=>'#topics',
	  'link'=>'backend/topic',
	  'title'=>t('topics'),
	  'noerror'=>true,
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/[0-9]+$@',
	  'htmlSelect'=>'#miscEventInfo .dayrank',
	  'config'=>'dayrank_enable',
	  'noerror'=>true,
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/[0-9]+$@',
	  'htmlSelect'=>'#Opinion',
	  'config'=>'opinions_pub_guidelines',
	  'noerror'=>true,
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/[0-9]+$@',
	  'htmlSelect'=>'#Opinion',
	  'config'=>'opinions_disagree_weight',
	  'noerror'=>true,
	  'xoffset'=>25,
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/[0-9]+$@',
	  'htmlSelect'=>'#Opinion',
	  'config'=>'opinions_auto_publish',
	  'noerror'=>true,
	  'xoffset'=>50,
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/[0-9]+$@',
	  'htmlSelect'=>'#Opinion',
	  'config'=>'opinions_reputation_buildup',
	  'noerror'=>true,
	  'xoffset'=>75,
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/[0-9]+/repetition$@',
	  'htmlSelect'=>'#edit-organizerEmail',
	  'config'=>'auto_repetition_confirm_email_subject',
	  'noerror'=>true,
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/[0-9]+/repetition$@',
	  'htmlSelect'=>'#edit-organizerEmail',
	  'config'=>'auto_repetition_confirm_email_body',
	  'noerror'=>true,
	  'xoffset'=>-25,
	 ],

	 // ********** backend-form
	 [
	  'pathSelect'=>'@^/'.$event.'/([0-9]+/edit|add)@',
	  'htmlSelect'=>'#edit-title',
	  'config'=>'require_keyword_in_event_title',
	  'xoffset'=>20,
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/([0-9]+/edit|add)@',
	  'htmlSelect'=>'#citySelect',
	  'config'=>'common_cities',
	  'xoffset'=>20,
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/([0-9]+/edit|add)@',
	  'htmlSelect'=>'.mce-toolbar .mce-btn-group:last-child',
	  'config'=>'editor_custom_insert',
	  'xoffset'=>50,
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/([0-9]+/edit|add)@',
	  'htmlSelect'=>'#edit-post-on-other-site',
	  'config'=>'post_on_other_site',
	  'noerror'=>true,
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/([0-9]+/edit|add)@',
	  'htmlSelect'=>'#edit-topics',
	  'link'=>'backend/topic',
	  'title'=>t('topics'),
	  'noerror'=>true,
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/([0-9]+/edit|add)@',
	  'htmlSelect'=>'#icomplete-message-select',
	  'config'=>'incomplete_messages',
	  'yoffset'=>10,
	  'noerror'=>true,
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/([0-9]+/edit|add)@',
	  'htmlSelect'=>'#moderation-message-select',
	  'config'=>'moderation_messages',
	  'yoffset'=>10,
	  'noerror'=>true,
	 ],
	 // ********** frontpage
	 [
	  'pathSelect'=>'@^/$@',
	  'htmlSelect'=>'#fp-logo',
	  'link'=>'browse/images',
	  'title'=>t('Logo'),
	  'center'=>true,
	  'yoffset'=>20,
	 ],
	 [
	  'pathSelect'=>'@^/$@',
	  'htmlSelect'=>'#fp-big-name',
	  'config'=>'big_site_name',
	  'center'=>true,
	  'bottom'=>true,
	  'yoffset'=>50,
	 ],
	 [
	  'pathSelect'=>'@^/$@',
	  'htmlSelect'=>'#slogan',
	  'config'=>'site_slogan',
	  'center'=>true,
	  'xoffset'=>-30,
	 ],
	 [
	  'pathSelect'=>'@^/$@',
	  'htmlSelect'=>'#slogan',
	  'config'=>'site_mission',
	  'xoffset'=>25,
	  'yoffset'=>25,
	 ],
	 [
	  'pathSelect'=>'@^/$@',
	  'htmlSelect'=>'#sidebar',
	  'config'=>'front_page_infoboxes',
	  'right'=>true,
	  'xoffset'=>10,
	 ],
	 [
	  'pathSelect'=>'@^/$@',
	  'htmlSelect'=>'#calendar',
	  'config'=>'frontpage_regions',
	  'yoffset'=>-20,
	 ],
	 [
	  'pathSelect'=>'@^/$@',
	  'htmlSelect'=>'#calendar',
	  'config'=>'front_page_limit_nb_events',
	  'bottom'=>true,
	  'center'=>true,
	  'yoffset'=>20,
	 ],
	 [
	  'pathSelect'=>'@^/$@',
	  'htmlSelect'=>'#topicSelect',
	  'link'=>'backend/topic',
	  'title'=>t('topics'),
	  'right'=>true,
	  'middle'=>true,
	 ],
	 [
	  'pathSelect'=>'@^/$@',
	  'htmlSelect'=>'#options',
	  'config'=>'frontpage_options',
	 ],
	 [
	  'pathSelect'=>'@^/$@',
	  'htmlSelect'=>'#slogan',
	  'link'=>'post/'.intval($demosphere_config['frontpage_announcement']).'/edit',
	  'title'=>t('announcement'),
	  'xoffset'=>200,
	  'yoffset'=>20,
	  'moderator'=>true,
	 ],
	 [
	  'pathSelect'=>'@^/$@',
	  'htmlSelect'=>'#slogan',
	  'link'=>'post/'.intval($demosphere_config['frontpage_messages']).'/edit',
	  'title'=>t('messages'),
	  'xoffset'=>220,
	  'yoffset'=>40,
	  'moderator'=>true,
	 ],

	 // ********** maps
	 [
	  'pathSelect'=>'@^/map$@',
	  'htmlSelect'=>'#header',
	  'config'=>'map_center_latitude',
	  'middle'=>true,
	  'xoffset'=>300,
	  'yoffset'=>20,
	 ],
	 [
	  'pathSelect'=>'@^/map$@',
	  'htmlSelect'=>'#header',
	  'config'=>'map_center_longitude',
	  'middle'=>true,
	  'xoffset'=>322,
	  'yoffset'=>20,
	 ],
	 [
	  'pathSelect'=>'@^/map$@',
	  'htmlSelect'=>'#header',
	  'config'=>'map_zoom',
	  'middle'=>true,
	  'xoffset'=>344,
	  'yoffset'=>20,
	 ],
	 [
	  'pathSelect'=>'@^/map$@',
	  'htmlSelect'=>'#optionTitle',
	  'config'=>'map_show_option_list',
	  'center'=>true
	 ],
	 [
	  'pathSelect'=>'@^/map$@',
	  'htmlSelect'=>'#header',
	  'config'=>'map_location_shortcuts',
	  'bottom'=>true,
	  'xoffset'=>50,
	  'yoffset'=>-15,
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/([0-9]+/edit|add)@',
	  'htmlSelect'=>'#map-links',
	  'config'=>'extra_map_links',
	  'middle'=>true,
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/[0-9]+$@',
	  'htmlSelect'=>'.mapimage',
	  'config'=>'event_map_static_url',
	  'noerror'=>true,
	  'center'=>true,
	  'xoffset'=>-40,
	  'yoffset'=>20,
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/[0-9]+$@',
	  'htmlSelect'=>'.mapimage',
	  'config'=>'event_map_link',
	  'noerror'=>true,
	  'center'=>true,
	  'xoffset'=>-15,
	  'yoffset'=>20,
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/[0-9]+$@',
	  'htmlSelect'=>'.mapimage',
	  'config'=>'event_map_add_marker',
	  'noerror'=>true,
	  'center'=>true,
	  'middle'=>true,
	 ],

	 // *********** pages FIXME

	 // *********** emails
	 [
	  'pathSelect'=>'@^/'.preg_quote(Post::builtInUrl('contact'),'@').'$@',
	  'htmlSelect'=>'#content',
	  'config'=>'contact_email',
	  'right'=>true,
	  'yoffset'=>20,
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/([0-9]+/edit|add)@',
	  'htmlSelect'=>'#eventEmails',
	  'config'=>'email_senders',
	  'yoffset'=>25,
	 ],
	 [
	  'pathSelect'=>'@^/'.$event.'/([0-9]+/edit|add)@',
	  'htmlSelect'=>'#eventEmails',
	  'config'=>'emails',
	 ],
	 //[
	 // 'pathSelect'=>'@^/user/([0-9]+/emails-received-options)@',
	 // 'htmlSelect'=>'#edit-contact',
	 // 'config'=>'contact_email',
	 // 'xoffset'=>200,
	 //],
	 //[
	 // 'pathSelect'=>'@^/email-aliases@',
	 // 'htmlSelect'=>'#aliases-section .row-contact td:nth-child(4)',
	 // 'config'=>'contact_email',
	 // 'right'=>true,
	 // 'xoffset'=>-10,
	 // 'yoffset'=>10,
	 //],

	// ********** feed-import
	 [
	  'pathSelect'=>'@^/feed-import(/view-articles)?@',
	  'htmlSelect'=>'.pager',
	  'config'=>'feed_import_default_articles_per_page',
	  'xoffset'=>10,
	 ],


	];
	
	// frontpage infoboxes
	foreach($demosphere_config['front_page_infoboxes'] as $box)
	{
		$buttons[]=
		[
		 'pathSelect'=>'@^/$@',
		 'htmlSelect'=>'#infobox'.$box['pid'],
		 'link'=>'post/'.$box['pid'].'/edit',
		 'right'=>true,
		 'moderator'=>true,
		 ];
	}


	if(!$demosphere_config['hosted'])
	{
		$buttons=
			array_merge($buttons,
		[
		 [
		  'pathSelect'=>'@^/control-panel@',
		  'htmlSelect'=>'.stats-block',
		  'config'=>'raw_apache_log',
		  'center'=>true,
		  ],
		 [
		  'pathSelect'=>'@^/map$@',
		  'htmlSelect'=>'#map',
		  'config'=>'gmap_key',
		  'yoffset'=>50,
		  ],
		 ]);
	}			

	// ****** add information from config form 
	require_once 'demosphere-config-form.php';
	$config=demosphere_config_description();
	foreach($buttons as &$item)
	{
		if(!isset($item['config'])){continue;}
		$configName=$item['config'];
		$configDesc=$config[$item['config']];
		$item['desc']=$configDesc;
		if(!isset($item['title'])){$item['title']=$configDesc['title'];}
		if(!isset($item['link' ])){$item['link']=$base_url.'/configure/'.$configDesc[':section'].'#'.$configName;}
	}
	unset($item);

	// ****** post process and create selectors var
	$selectors=[];
	$selectorsMod=[];
	foreach($buttons as &$item)
	{
		if( isset($item['link' ]) && strpos($item['link'],'http')!==0)
		{
			$item['link']=$base_url.'/'.$item['link'];
		}
		if(isset($item['moderator'])){$selectorsMod[]=$item['pathSelect'];}
		$selectors[]=$item['pathSelect'];
	}
	unset($item);

	$selectors   =array_unique($selectors);
	$selectorsMod=array_unique($selectorsMod);
	array_unshift($selectors   ,time());
	array_unshift($selectorsMod,time());
	variable_set('config_path_selectors'          ,'',$selectors);
	variable_set('config_path_selectors_moderator','',$selectorsMod);
	return $buttons;
}

?>