// All demosphere code is Copyright 2007 - 2010 by the original authors.
// http://demosphere.net
// Licensed under the GNU Affero GPL v3 - http://www.gnu.org/licenses
// **************************************************************
// **** An interface to input / edit an array of arrays
// **************************************************************
// It replaces an existing textarea, and on submit fills it with the json encoded
// edited array. 
// **
// The textarea's content should be the json encoded content array
// The textarea should have a data-aofa attribute with the column info:
// 	$data=json_encode(array(array(colname,title,['int'|'float'|'textarea'|...],...),
//                          array(colname,title,['int'|'float'|'textarea'|...],...),
//                               ...));
// (use class=false for assoc array index column)
// ** PHP Example :
// (ent=htmlentities) 
//
// $columns=array(array(false,'login',),
//                array('name','Name',),
//                array('age','Age','int','js_validation'=>'parseInt(v)<100'),
//                array('sex','Sex','select','values'=>array('m'=>'male','f'=>'female')),
//                array('story','A short story','textarea'),);
// $content=array('john'=>array('name'=>'John Doe','age'=>20,'story'=>'bla bla bla'),
//                'tom' =>array('name'=>'Tom Xyz','age'=>25,'story'=>'ha ha ha'),);
// 		echo '<textarea data-aofa="'.ent(json_encode($columns)).'">'.
//			ent(json_encode($content)).
//			'</textarea>';
// **
// Note: this uses the JSON object (works on firefox, for other browser you will need
// a compatible JSON library) 
// **
// Returns the ArrayOfArrays object (if mutliple where objects created, returns the last one).
// **************************************************************
function array_of_arrays(selector,options)
{
	var res=false;
	if(typeof selector==='string'){selector=$(selector);}
	selector.filter('textarea,input[type=text]').each(function()
	{
		if($(this).hasClass('array_of_arrays_is_init')){return;}
		res=new ArrayOfArrays(this,options);
		$(this).addClass('array_of_arrays_is_init');
	});
	return res;
}

function ArrayOfArrays(item,options)
{
	this.init(item,options);
}

ArrayOfArrays.prototype=
{
	// options/settings given at setup. 
	// Options are:
	// * singleLine: true/false : only show form on startup if 0 or 1 rows. 
	//           With this option, ArrayOfArrays looks like a standard (single element) form 
	//           unless the user adds new items. Only then is the table displayed.
	// * text: {newRow:'...',addRow:'',changeRow:''} : text displayed in buttons or elsewhere
	options: false,
	// a descriptions of columns. This is is extracted from the json encoded 
	// information in the dat-aofa attribute of of the textarea 
	columns: false,
	// whether or not this is an associative array (first col is actually the key)
	assocCol: false,
	// the textarea in the original form, 
	// that is a placeholder for all the ArrayOfArrays stuff (it is hidden on init)
	formItem: false,
	// Div wraper for all of this ArrayOfArrays : table+ msg+add new button
	div : false,
	// The table that is displayed with all the ArrayOfArrays content
	table: false,
	// The button on which the user can click to add a new row
	newRowButton: false,
	// False if we are not currently in single line mode.
	// Otherwise, it is the single line table row (tr)
	singleLine: false,
	// In Single line mode we create a dud empty row if the initial content array is empty.
	// We need to keep track of it to remove it if it has not changed, and return empty result
	singleLineEmptyRow: false,
	// The form for editing a row. Only one form can exist at a time.
	rowForm: false,

	init: function(formItem,options)
	{
		var aofa=this;
		// All the information (array columns and initial contents) 
		// is json encoded in the form element
		$(formItem).parents('form').eq(0).submit(function(e){
													 //e.stopPropagation();e.preventDefault();
													 aofa.updateJsonData();
												 });
		//console.log('formItem:',formItem,$(formItem).val());
		var initContent=JSON.parse($(formItem).val());
		//console.log('initContent:',initContent);
		aofa.columns=JSON.parse($(formItem).attr('data-aofa'));
		// If values is an assoc arrray, the column with name = false is actually the key.
		$.each(aofa.columns,function(n,desc)
			   {if(desc[0]===false){aofa.assocCol=n;}});

		if(aofa.columns===false){return;}
		aofa.formItem=formItem;

		aofa.options=typeof options==='undefined' ? {} : options;
		if(typeof aofa.options.singleLine     ==='undefined'){aofa.options.singleLine=false;}

		var text={
			newRow:'Add new',
			addRow:'add row',
			changeRow:'ok',
			cancel:'cancel',
			mustSaveMsg:"The data has been edited. Don't forget to save this form.",
			confirmRowDelete:"Are you sure you want to delete this line?"
		};
		if(typeof array_of_arrays_translations!=='undefined'){text=$.extend(text,aofa.options.text,array_of_arrays_translations);}
		if(typeof aofa.options.text           !=='undefined'){text=$.extend(text,aofa.options.text);};
		aofa.options.text=text;

		// hide the unused original form item (we create our own table and form items)
		$(formItem).hide();
		//$(formItem).parents('div').eq(0).find('.grippie').remove();

		// create the table that displays the array of data
		var table=$('<table class="aofa-table">'+
					'<tr><td></td><td></td><td></td></tr></table>');
		aofa.table=table;
		// add table headers
		$.each(aofa.columns,function(unused,desc)
			   {
				   table.find('tr').append($('<th></th>').text(desc[1]).addClass('aofa-col-'+(desc[0]==false ? '-key' : desc[0])));
			   });

		// create a button for adding new rows to the table
		var newRowButton=
			$('<a class="aofa-new-row-button" href="javascript:void(0)"></a>').
				text(aofa.options.text.newRow);
		aofa.newRowButton=newRowButton;
		newRowButton.click(function(e)
		{
			// in single line mode, adding new row will also click on (hidden) ok button.
			if(aofa.singleLine!==false)
			{
				aofa.rowForm.find('.aofa-change-row').trigger('click');
				aofa.singleLine=false;
				aofa.div.removeClass('singleLine');
				aofa.singleLineEmptyRow=false;
			}
			aofa.openRowForm(false);
		});

		// use initial content to create each row in the table
		var lineNum=0;
		$.each(initContent,function(lineKey,line)
		{
			var row=$('<tr></tr>');
			// add the delete and edit buttons at the begining of each row
			aofa.addRowButtons(row,newRowButton);

			// add columns
			$.each(aofa.columns,function(colNb,desc)
			{
				var type=typeof desc[2]==='undefined' ? 'text' : desc[2];
				var val=colNb===aofa.assocCol ? lineKey : line[desc[0]];
				// Select displays human readable, not actual value
				if(type==='select'){val=desc.values[val];}
				if(typeof val==='undefined'){val='';}
				if(typeof val==='boolean'){val=val? '1':'';}
				var td=$('<td></td>');
				td.text(val);
				td.addClass('aofa-type-'+(typeof type==='undefined' ? 'undefined' : type));
				td.addClass('aofa-col-'+(desc[0]==false ? '-key' : desc[0]));
				row.append(td);
			});
			table.append(row);
			lineNum++;
		});
		aofa.div=$('<div class="aofa"></div>');
		$(formItem).after(aofa.div);
		aofa.div.append(table);
		aofa.div.append(newRowButton);
		aofa.div.append($('<div class="aofa-must-save-msg"></div>').text(aofa.options.text.mustSaveMsg));

		// in singleLine mode, directly open a row form, if there are zero or one existing rows
		if(aofa.options.singleLine && initContent.length<2)
		{
			aofa.div.addClass('singleLine');
			// if there are no rows yet, create an empty one
			if(initContent.length===0)
			{
				aofa.singleLineEmptyRow=aofa.createNewRow();
			}
			aofa.singleLine=table.find('tr:nth-child(2)');
			aofa.openRowForm(aofa.singleLine);
			aofa.rowForm.find('input[type=button]').hide();
			if(aofa.singleLineEmptyRow!==false)
			{
				aofa.rowForm.find('input,textarea,select').change(function()
				{
					aofa.singleLineEmptyRow=false;
				});
			}
		}
	},

	// Display a form to edit a row, or to add a new row
	// @param where: where the rowForm should be appended (false for default)
	// @param editRow: what table row we are editing (false for new row)
	openRowForm: function (editRow)
	{
		var aofa=this;
		if(this.rowForm!==false){alert('You can only edit one row at a time.');return;}
		if(editRow!==false)
		{
			if($(editRow).hasClass("editing")){alert("already editing");return;}
			$(editRow).addClass("editing");
		}
		// create big div for the edit row form
		var rowForm=$('<div class="aofa-row-form"></div>');
		this.rowForm=rowForm;
		// create a ul for all input elements
		var rowFormItems=$('<ul class="aofa-form-items"></ul>');

		// create field inputs
		$.each(aofa.columns,function(pos,desc)
		{
			var field;
			var type=typeof desc[2]==='undefined' ? 'text' : desc[2];
			switch(type)
			{
			case 'textarea':
				field=$('<textarea class="aafield"></textarea>');
				break;
			case 'text':
				field=$('<input class="aafield" type="text"/>');
				break;
			case 'int':
				field=$('<input class="aafield" type="text"/>');
				field.keypress(function(e){setTimeout(function()
			    {field.toggleClass('invalid',field.val().match(/[^0-9]/)!==null);
				},1);});
				break;
			case 'float':
				field=$('<input class="aafield" type="text"/>');
				field.keypress(function(e){setTimeout(function()
				{field.toggleClass('invalid',field.val().match(/^(-?[0-9]*[.][0-9]+|-?[0-9]+|)$/)===null);
				},1);});
				break;
			case 'select':
				field=$('<select class="aafield"></select>');
				$.each(desc.values,function(value,title)
				{
					var opt=$('<option></option>');
					opt.val(value);
					opt.text(title);
					field.append(opt);
				});
				break;
			default: alert('bad type:'+type);return;
			}

			// optional custom js validation function
			if(typeof desc.js_validation!=='undefined')
			{
				field.keypress(function(e){setTimeout(function()
				{
					var v=field.val();
					field.toggleClass('invalid',!eval(desc.js_validation));
				},1);});
			}
			
			// if this is an exsiting row, set field to existing value
			if(editRow!==false)
			{
				var el=$(editRow).find('td').eq(pos+3);
				var val=el.text();
				if(type==='textarea'){field.text(val);}
				else
				if(type==='select')
				{
					var rval=false;
					$.each(desc.values,function(k,v){if(v==val){rval=k;};});
					field.val(rval);
				}
				else{field.val(val);}
			}
			field.addClass('aofa-field-'+(desc[0]==false ? '-key' : desc[0]));
			var item=$('<li></li>');
			item.addClass('aofa-form-item-'+(desc[0]==false ? '-key' : desc[0]));
			item.append($('<label></label>').text(desc[1]));
			item.append($('<span></span>').append(field));
			rowFormItems.append(item);
		});
		rowForm.append(rowFormItems);
		// handler for "Add row" (ok) button
		var submitNewRow=$('<input type="button"/>').val(aofa.options.text.addRow);
		//console.log('submitNewRow',submitNewRow);
		submitNewRow.click(function()
		{
			if($('.aofa-row-form .invalid').length){alert('invalid value');return;}
			aofa.updateRowFromForm(aofa.createNewRow());
		});
		if(editRow===false){rowForm.append(submitNewRow);}

		// handler for "Change row" (ok) button
		var changeRow=$('<input type="button" class="aofa-change-row"/>').
			val(aofa.options.text.changeRow);
		changeRow.click(function(e)
		{
			if($('.aofa-row-form .invalid').length){alert('invalid value');return;}
			aofa.updateRowFromForm(editRow);
		});
		if(editRow!==false){rowForm.append(changeRow);}

		// handler for "Cancel" button
		var cancel=$('<input type="button">').val(aofa.options.text.cancel);
		cancel.click(function()
				  {
					  rowForm.remove();
					  aofa.rowForm=false;
					  if(editRow!==false){$(editRow).removeClass("editing");}
				  });
		// Return key in aofa form should not submit whole form, just emulate "ok" button.
		rowForm.find('input').keypress(function(e)
		{
			var key = (e.keyCode ? e.keyCode : e.which);
			if(key==13 /* DOM_VK_RETURN */)
			{
				e.preventDefault();
				e.stopPropagation();
				// emulate "ok" button
				changeRow.click();
			}
		});

		rowForm.append(cancel);
		rowForm.css('left',$(editRow!==false ? editRow : aofa.newRowButton).position().left);
		rowForm.css('top' ,$(editRow!==false ? editRow : aofa.newRowButton).position().top);
		aofa.table.after(rowForm);

		if(aofa.options.hasOwnProperty('editCallback')){aofa.options.editCallback(rowForm,editRow);}
		return rowForm;
	},

	createNewRow: function()
	{
		var aofa=this;
		var newRow=$('<tr></tr>');
		aofa.addRowButtons(newRow);
		$.each(aofa.columns,function(colNb,desc)
	    {
			newRow.append($('<td></td>').
						  addClass('aofa-type-'+aofa.columns[colNb][2]));
		});
		aofa.table.append(newRow);
		return newRow;
	},

	// Update a row in the main contents table from the data the user has
	// entered in the input form (add/edit row).
	updateRowFromForm: function(row,leaveOpen)
	{
		var aofa=this;
		var form=aofa.rowForm;
		$.each(aofa.getRowFormValues(form),function(n,val)
			   {
				   $(row).find('td').eq(n+3).text(val);
			   });
		if(typeof leaveOpen!=='undefined' && leaveOpen){return;}
		$(row).css("background-color","yellow");
		animate_bgcolor(row,"ffff00","eee5e0",{duration:1000},true);
		form.remove();
		aofa.rowForm=false;
		$(row).removeClass("editing");
		$(row).addClass("edited");
		aofa.div.find('.aofa-must-save-msg').show();
		// fire custom event
		$.event.trigger({
							type: "aofachanged",
							row: row
						});
	},

	// Returns an array of values from the data the user has
	// entered in an input form (add/edit row).
	getRowFormValues: function(rowForm)
	{
		var res=[];
		rowForm.find('.aafield').each(function()
		{
			var val=false;
			if(this.tagName==='SELECT')
			{
				val=$(this).find('option:selected').text();
			}
			else{val=$(this).val();}
			res.push(val);
		});		
		return res;
	},

	// add the up,down, delete and edit buttons at the begining of a row for the main content table
	addRowButtons: function (row)
	{
		var aofa=this;
		var buttons=
			$('<td class="updown"><span class="up"></span><span class="down"></span></td>'+
			  '<td class="delete"><span></span></td>'+
			  '<td class="edit"  ><span></span></td>');
		row.append(buttons);
		buttons.find(".up"    ).click(function()
		{
			var tr=$(this.parentNode.parentNode);
			if(tr.prev().prev().length){tr.prev().before(tr);}
			aofa.div.find('.aofa-must-save-msg').show();
		});
		buttons.find(".down"  ).click(function()
		{
			var tr=$(this.parentNode.parentNode);
			tr.next().after(tr);
			aofa.div.find('.aofa-must-save-msg').show();
		});
		// delete
		$(buttons[1]).click(function()
		{
			if(!confirm(aofa.options.text.confirmRowDelete)){return;}
			$(this.parentNode).remove();
			aofa.div.find('.aofa-must-save-msg').show();
		});
		// edit
		$(buttons[2]).click(function(e){aofa.openRowForm(e.currentTarget.parentNode);});
	},

	// Called when the user submits the the form that contains this ArrayOfArrays.
	// This creates a json encoded version of the content displayed in the table.
	updateJsonData: function()
	{
		var aofa=this;
		if(aofa.columns===false){return;}
		if(aofa.singleLine!==false)
		{
			aofa.updateRowFromForm(aofa.singleLine,true);
		}

		// Read values from html table into result array
		var res=aofa.assocCol===false ? [] : {};
		aofa.table.find('tr:gt(0)').each(function()
		{
			var row=$(this).find('td:gt(2)');
			var rowRes={};
			var rowKey=false;
			$.each(aofa.columns,function(colNb,desc)
			{
				var type=typeof desc[2]==='undefined' ? 'text' : desc[2];
				var val=row.eq(colNb).text();
				// special case for select type: 
				if(type==='select')
				{
					$.each(desc.values,function(ok,ov)
					{
						if(ov===val){val=ok;return;}
					});
				}

				if(colNb===aofa.assocCol){rowKey=val;}
				else
				{rowRes[desc[0]]=val;}
				//console.log("colNb:",colNb,'desc',desc[0],'val:',val,'rowRes:',rowRes);
			});
			if(rowKey===false){res.push(rowRes);}else{res[rowKey]=rowRes;}
		});
		//console.log('resxx0:',res[0]);
		// put json encoded result array into textarea
		$(aofa.formItem).val(JSON.stringify(res));
		//console.log("res:",res);
		//console.log("final val:",$(aofa.formItem).val());
	}
};

// progressively change the background color of an element (el) from color1 to color2
// color format "ff00ff"
// FIXME: shouldn't we use css transitions for this?
function animate_bgcolor(el,color1,color2,options,reset)
{
	var r1=parseInt(color1.substr(0,2),16);
	var g1=parseInt(color1.substr(2,2),16);
	var b1=parseInt(color1.substr(4,2),16);
	var r2=parseInt(color2.substr(0,2),16);
	var g2=parseInt(color2.substr(2,2),16);
	var b2=parseInt(color2.substr(4,2),16);

	el=$(el);
	el.css('background-color',"#"+color1);

	if(typeof options==="undefined"){options={};}
	if(typeof reset==="undefined"){reset=false;}
	if(reset){options.complete=function(){el.css('background-color','');};}
	// FIXME : we used to use an non-existant css property, but jquery (1.9) now requires a valid property
	// We use "top", and assume it does not affect the object. But that is hackish.
	el.css('top','0%');
	options.step=function()
	{
		//console.log(el,el.css('top'));
		var v=parseInt(el.css('top').replace('%',''));
		var r=Number(Math.floor(r1+(r2-r1)*v/100)).toString(16);
		var g=Number(Math.floor(g1+(g2-g1)*v/100)).toString(16);
		var b=Number(Math.floor(b1+(b2-b1)*v/100)).toString(16);
		el.css('background-color',"#"+r+g+b);
	};
	el.animate({top: "100%"},options);
}
