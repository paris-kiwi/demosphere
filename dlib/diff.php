<?php

//! Computes the differences between two html strings.
//! Deprecated for client diff : for viewing and editing side-by-side html diffs, use hdiff()
//! This can still be usefull for server side diffs.
//! This function extracts all text, compares it word by word and then
//! rebuilds the original html with diffs marked.
//! Each word in the output diff can be marked with a span.
//! You can optionally specify a function to build the actual output for each word.
//! You can optionally specify a function to preprocess text. Example: 'textPreprocess'=>'strtolower'
function html_diff($html0,$html1,$options=[])
{
	$html=[$html0,$html1];
	$defaults=['renderWord'=>'html_diff_render_word_diff','textPreprocess'=>false];
	$options=array_merge($defaults,$options);

	for($i=0;$i<2;$i++)
	{
		// remove soft hyphen U+00AD
		$html[$i]=str_replace("­",'',$html[$i]);

		// Use a regular expression with a callback to extract all
		// plain-text. 
		// After this $tags contains the html where all texts 
		// have been replaced by numbered tokens.
		// After this $text contains an array which each stretch of text that was 
		// replaced by tokens.
		$textStretches[$i]=[];
		$tags[$i]=preg_replace_callback('@(>|^)([^<>]+)(<|$)@s',
										function($matches)use(&$textStretches,$i)
										{
											global $html_diff_pregcb_res;
											$s=$matches[2];
											// ignore empty (or only space/puntuation) stretches 
											if(strlen(trim($s))==0 ||
											   preg_match('@^[\n\r\p{Z}\p{P}]+$@u',$s)){return $matches[0];}
											$n=count($textStretches[$i]);
											$textStretches[$i][]=$s;
											return 
												(substr($matches[0], 0,1)=='>' ? '>' : '').
												'XYH1Z_'.$n.'_YER'.
												(substr($matches[0],-1,1)=='<' ? '<' : '');
										},
										$html[$i]);
	
		// remove entities from extracted texts
		foreach(array_keys($textStretches[$i]) as $n)
		{$textStretches[$i][$n]=html_entity_decode($textStretches[$i][$n],ENT_QUOTES,'UTF-8');}

		// Extract word-lists for each stretch of plain-text.
		// Wordlists are merged to form a single wordlist for each document.
		// However, we keep info on wordlists in each stretch of plain text,
		// so we know where each word came from.
		$wordlist[$i]=[];
		$wordIndex[$i]=[];
		$stretchIndex[$i]=[];
		foreach($textStretches[$i] as $n=>$stretch)
		{
			$split=preg_split('@[\n\r\p{Z}\p{P}\p{Cf}]+@u',$stretch,0,PREG_SPLIT_OFFSET_CAPTURE);
			//var_dump($split);
			$stretchIndex[$i][$n]=[];
			foreach($split as $word)
			{
				if($word[0]===''){continue;}
				$wordlist[$i][]=$word[0];
				$wordIndex[$i][]=[$n,$word[1]];
				$stretchIndex[$i][$n][]=count($wordlist[$i])-1;
			}
		}
	}

	require_once 'dlib/diff.php';
	// apply word preprocessing (example: lowercase for case-insensitive match)
	for($i=0;$i<2;$i++)
	{
		$processedWordlist[$i]=$wordlist[$i];
		if($options['textPreprocess']!==false)
		{
			foreach($wordlist[$i] as $k=>$v)
			{$processedWordlist[$i][$k]=$options['textPreprocess']($v);}
		}
	}
	//var_dump($wordlist[0],$processedWordlist[0]);fatal('ii');
	$corresponding=longest_common_sequence_unix_diff($processedWordlist[0],$processedWordlist[1]);
	//echo "lcs:";var_dump($corresponding);fatal('ooo');
	// reconstruct original html
	for($i=0;$i<2;$i++)
	{
		$newHtml[$i]=$tags[$i];
		// reconstruct each stretch
		foreach($textStretches[$i] as $stretchNb=>$oldStretch)
		{
			$newStretch='';
			$strPos=0;
			foreach($stretchIndex[$i][$stretchNb] as $wordNb)
			{
				// add the empty spaces (or other split chars) before the word
				$newStretch.=substr($oldStretch,$strPos,$wordIndex[$i][$wordNb][1]-$strPos);
				// add the word itself
				$match=isset($corresponding[$i][$wordNb]) ? $corresponding[$i][$wordNb] : false;
				$fct=$options['renderWord'];
				$newStretch.=$fct($wordlist[$i][$wordNb],$i,$wordNb,$match);
				// update the position
				$strPos=$wordIndex[$i][$wordNb][1]+strlen($wordlist[$i][$wordNb]);
			}
			// add the empty spaces (or other split chars) at the end of the stretch
			$newStretch.=substr($oldStretch,$strPos);

			// replace tag with new stretch 
			$newHtml[$i]=str_replace('XYH1Z_'.$stretchNb.'_YER',$newStretch,$newHtml[$i]);
		}
		//echo '"'.$newHtml[$i].'"'."\n";
	}
	//fatal("xx\n");
	return $newHtml;
}


function html_diff_render_word_diff($word,$i,$wordNb,$match)
{
	$res='';
	$res.=$match!==false ? '' : '<span class="diff">';
	$res.=$word;
	$res.=$match!==false ? '' : '</span>';
	return $res;
}
function html_diff_render_word_full($word,$i,$wordNb,$match)
{
	$res='';
	$res.='<span '.($match===false ? '': 'id="w'.$i.'-'.$wordNb.'" ').
		'class="tms '.($match===false ? 'd':'m').'" '.
		($match===false ? '' : 'data="w'.(($i+1)%2).'-'.$match.'"').'>';
	$res.=$word;
	$res.='</span>';
	return $res;
}



//error_reporting(E_ALL | E_STRICT);
//var_dump(longest_common_sequence_unix_diff(explode(',',$argv[1]),explode(',',$argv[2])));

//! Equivalent to longest_common_sequence() using dynamic programming 
//! slightly faster than longest_common_sequence, but 20xslower than longest_common_sequence_unix_diff()
//! https://github.com/eloquent/php-lcs/blob/develop/src/LcsSolver.php#L59
function longest_common_sequence_dp(array $sequenceA, array $sequenceB)
{
	$m=count($sequenceA);
	$n=count($sequenceB);
	// $a[$i][$j]=length of LCS of $sequenceA[$i..$m] and $sequenceB[$j..$n]
	$a=[];
	// compute length of LCS and all subproblems via dynamic programming
	for ($i=$m - 1; $i >= 0; $i--) 
	{
		for ($j=$n - 1; $j >= 0; $j--) 
		{
			if ($sequenceA[$i]===$sequenceB[$j]) 
			{
				$a[$i][$j] =(isset($a[$i + 1][$j + 1]) ? $a[$i + 1][$j + 1] : 0) +1;
			} 
			else 
			{
				$a[$i][$j]=max((isset($a[$i + 1][$j]) ? $a[$i + 1][$j] : 0),
							   (isset($a[$i][$j + 1]) ? $a[$i][$j + 1] : 0)
							  );
			}
		}
	}
	// recover LCS itself
	$i=0;
	$j=0;
	$lcs=[];
	while($i < $m && $j < $n) 
	{
		if($sequenceA[$i]===$sequenceB[$j]) 
		{
			$lcs[]=$sequenceA[$i];
			$i++;
			$j++;
		} 
		elseif ((isset($a[$i + 1][$j]) ? $a[$i + 1][$j] : 0) >=
				(isset($a[$i][$j + 1]) ? $a[$i][$j + 1] : 0)    ) {$i++;} 
		else                                                      {$j++;}
	}
	return $lcs;
}

//! Returns a common index/dictionnary for all words in the lists.
//! Also returns the original lists where words have been replaced by numbers (indexes)
function wordlists_to_index(array $wordLists)
{
	$dictionnary=[];
	$ct=0;
	foreach($wordLists as $wordList)
	{
		foreach($wordList as $word){if(!isset($dictionnary[$word])){$dictionnary[$word]=$ct++;}}
	}

	$indexedLists=[];
	foreach($wordLists as $i=>$wordList)
	{
		$indexedLists[$i]=[];
		foreach($wordList as $k=>$word){$indexedLists[$i][$k]=$dictionnary[$word];}
	}
	return [$dictionnary,$indexedLists];
}

//! Equivalent to longest_common_sequence() using unix diff command.
function longest_common_sequence_unix_diff($list1,$list2)
{
	require_once 'dlib/tools.php';
	require_once 'dlib/html-tools.php';

	$debug=false;
	if($debug)
	{
		echo 'src list1:';
		print_r($list1);
		echo 'src list2:';
		print_r($list2);
		echo 'result for lcs:';
		print_r(longest_common_sequence($list1,$list2));
	}
	$list1str=implode("\n",str_replace(["\n","\r"],['KLMJSDMF87656SDF','YEZ6FDKS788S74VC'],$list1));
	$list2str=implode("\n",str_replace(["\n","\r"],['KLMJSDMF87656SDF','YEZ6FDKS788S74VC'],$list2));
	$list1str.="\n";//??
	$list2str.="\n";//??
	$diffs=unix_diff($list1str,$list2str);
	$diffs[]=['range'=>['action'=>'d',
						'start-0'=>count($list1)+1,
						'end-0'=>0,
						'start-1'=>0,
						'end-1'=>0]];
	if($debug){echo 'diff res:';print_r($diffs);}
	
	$common1=[];
	$common2=[];
	$lastCommon1=-1;
	$lastCommon2=-1;
	foreach($diffs as $diff)
	{
		$range=$diff['range'];
		$action=$range['action'];
		for($i=$lastCommon1+1;$i<$range['start-0']-1+($action=='a' ? 1 : 0);$i++)
		{
			++$lastCommon1;
			++$lastCommon2;
			$common1[$lastCommon1]=$lastCommon2;
			$common2[$lastCommon2]=$lastCommon1;
			if($debug && $list1[$lastCommon1]!=$list2[$lastCommon2])
			{
				//var_dump($list1);
				//var_dump($list2);
				fatal('argh: diff mismatch!');
			}
		}
		$lastCommon1=$range['end-0']-1;
		$lastCommon2=$range['end-1']-1;
	}

	if($debug){echo 'final unix diff lcs:';}
	if($debug)print_r($common1);
	if($debug)print_r($common2);
	return [$common1,$common2];
}


//! Returns an array describing differences between two strings
//! This uses the unix "diff" command and parses it's output.
function unix_diff($str0,$str1)
{
	// use unix diff command
	$fname0=tempnam("/tmp","diff-");
	$fname1=tempnam("/tmp","diff-");
	file_put_contents($fname0,$str0);
	file_put_contents($fname1,$str1);
	$lines=[];
	exec( "diff --ignore-all-space --ignore-blank-lines $fname0 $fname1",$lines);
	unlink($fname0);
	unlink($fname1);

	$diffs=[];
	foreach($lines as $line)
	{
		$firstChar=substr($line,0,1);
		if(ctype_digit($firstChar))
		{
			$m=preg_match('@^([0-9]+)(,([0-9]+)|)([acd])([0-9]+)(,([0-9]+)|)@',
					   $line,$parts);
			if(!$m){fatal("diff interpretation error!");}
			
			//print_r($parts);
			$range=['start-0'=>intval($parts[1]),
					'end-0'  =>$parts[3]!='' ? 
					intval($parts[3]) : intval($parts[1]),
					'action' =>$parts[4],
					'start-1'=>intval($parts[5]),
					'end-1'  =>val($parts,7,'')!='' ? 
					intval($parts[7]) : intval($parts[5]),
				   ];
			$diffs[]=['range'=>$range,'remove'=>'','insert'=>''];
		}
		if($firstChar=='<')
		{
			$diffs[count($diffs)-1]['remove'].=substr($line,2,strlen($line)-2).
				"\n";
		}
		if($firstChar=='>')
		{
			$diffs[count($diffs)-1]['insert'].=substr($line,2,strlen($line)-2).
				"\n";
		}
	}
	return $diffs;
}


//****************************************************************
// Algo, unused : slow + recursion can be a problem
//****************************************************************


//! Find similarities between array $x and array $y
//! algorithm in :
//! http://en.wikipedia.org/wiki/Longest_common_subsequence_problem
//! Unfortunately memory usage gets out of control with this algo.
//! Use longest_common_sequence_unix_diff()
function longest_common_sequence_rec($x,$y)
{
	// use a dictionnary to replace string comparison by numbers	
	// (not sure if this is really good for performance)
	list($dictionnary,$il)=wordlists_to_index([$x,$y]);
	list($x1,$y1)=$il;
	//$x1=$x;$y1=$y;
	
	$comp=[];
	
	$maxNesting=ini_get('xdebug.max_nesting_level');
	if($maxNesting!==false){ini_set('xdebug.max_nesting_level',1000000000000);}// FIXME!
	$l  =_lcs_recurse  ($x1,count($x1)-1,$y1,count($y1)-1,$comp);
	$seq=_lcs_backtrace($x1,count($x1)-1,$y1,count($y1)-1,$comp);	
	//if($maxNesting!==false){ini_set('xdebug.max_nesting_level',$maxNesting);}
	//print_r($seq);
	$xres=[];
	$yres=[];
	//	foreach($seq[0] as $s){$yres[$s]=$x[$s];}
	//	foreach($seq[1] as $s){$yres[$s]=$y[$s];}
	foreach($seq[0] as $n=>$s)
	{
		$xres[$s]=$seq[1][$n];
		$yres[$seq[1][$n]]=$s;
	}

	return [$xres,$yres];
}


// This is the main algorithm for diff. It  recursively computes an lcs size map
// (in  $comp)  for different  subarrays  of  $x  and $y.   This  only
// computes  the sizes.  The actual  sequence  is then  computed by  a
// similar algo in  _lcs_backtrace. This second pass is  quick, as lcs
// sizes are already known.
// http://en.wikipedia.org/wiki/Longest_common_subsequence_problem
// PROBLEM: This algo does not run correctly in PHP
// (recursion and memory problems).
function _lcs_recurse(&$x,$i,&$y,$j,&$comp)
{
	if($i===-1 || $j===-1){return 0;}	
	if(isset($comp[$i][$j])){return $comp[$i][$j];}
	if($x[$i]===$y[$j])
	{
		$length=_lcs_recurse($x,$i-1,$y,$j-1,$comp);
		$comp[$i][$j]=$length+1;
		return $length+1;
	}
	$lx=_lcs_recurse($x,$i-1,$y,$j  ,$comp);
	$ly=_lcs_recurse($x,$i  ,$y,$j-1,$comp);
	if($lx>$ly)
	{
		$comp[$i][$j]=$lx;
		return $lx;
	}
	else
	{
		$comp[$i][$j]=$ly;
		return $ly;
	}
}

function _lcs_backtrace($x,$i,$y,$j,&$comp)
{
	if($i===-1 || $j===-1){return [[],[]];}
	if($x[$i]===$y[$j])
	{
		$seq=_lcs_backtrace($x,$i-1,$y,$j-1,$comp);
		$seq[0][]=$i;
		$seq[1][]=$j;
		return $seq;
	}
	if((isset($comp[$i-1][$j]) && 
		isset($comp[$i][$j-1]) && $comp[$i-1][$j]>$comp[$i][$j-1]) || 
	   !isset($comp[$i][$j-1])
	   )
	{
		return _lcs_backtrace($x,$i-1,$y,$j ,$comp);
	}
	else
	{
		return _lcs_backtrace($x,$i ,$y,$j-1,$comp);
	}	
}




?>