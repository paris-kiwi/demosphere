<?= isset($topText) ? $topText : '' ?>
<? if($deleteForm!==false){ ?>
	<?= $deleteForm ?>
<?}?>
<ul class="dbobject-ui-view <?: isset($urlClass) ? $urlClass : $table ?>-view">
	<? foreach($display as $name=>$show){ ?>
		<?= val($show,'before','') ?>
		<li class="dbobject-ui-view-$name">
			<div class="title" title="$name">$show['title'] : </div>
			<div class="display"><?= $show['disp'] ?></div>
			<? if($show['description']!==false){ ?>
				<div class="description"><?= $show['description'] ?></div>
			<? } ?>
		</li>
		<?= val($show,'after','') ?>
	<? } ?>
</ul>
<?= isset($bottomText) ? $bottomText : '' ?>