<?php

/** @defgroup database database
 *  Database connection and helper functions
 *  Simple database helper functions using mysqli_ and providing printf-like placeholders.
 * - Module uses: (none)
 *  @{
 */


//! $db_config : configuration options db.
//! List of $db_config keys:
//! - log_slow_queries
//! - database
//! - tablename_map
//! - user
//! - password
//! - host
//! -::-;-::-
$db_config;

/** 
 * Connect to database. 
 *
 * note: Within drupal, DB is already connected, don't call this.
 */
function db_setup()
{
	global $db_config,$dbConnection,$dbSlowQueries;

	if(!isset($db_config['log_slow_queries'])){$db_config['log_slow_queries']=false;}
	$dbSlowQueries=[];

	// Sanity check : only connect once to database
	if(isset($dbConnection)){fatal('db_setup: called multiple times');}
	$dbConnection = 
		mysqli_connect($db_config['host'],
					   $db_config['user'],
					   $db_config['password'],
					   $db_config['database']);
	if(mysqli_connect_errno()){fatal('db_setup : could not connect: '.
									 mysqli_connect_error());}

	if(!mysqli_query($dbConnection,"SET NAMES 'utf8mb4';")){fatal('Query failed: ' . mysqli_error($dbConnection));}
}

/**
 * Build an sql query using printf like arguments.
 *
 * Adds  table  prefix (or  renames)  and  replaces placeholder  args
 * (printf style) and escapes strings against injection attacts.
 */
function db_build_query()
{
	global $db_config,$dbConnection;
	$args=func_get_args();
	$query=array_shift($args);
	$passedNbArgs=count($args);

	// *** replace place holders with args (similar to printf %d %s ...)
	$argNb=0;
	$res=preg_replace_callback('@%.@',
    function ($matches)use($dbConnection,&$argNb,$args,$passedNbArgs)
	{
		$f=$matches[0][1];
		if($f=='%'){return '%';}
		if($argNb>=$passedNbArgs){fatal("db_build_query : insufficient args\n");}
		switch($f)
		{
		case 's': return mysqli_real_escape_string($dbConnection,$args[$argNb++]);
		case 'd': return (int)  ($args[$argNb++]);
		case 'f': return (float)($args[$argNb++]);
		case 'x': return mysqli_real_escape_string($dbConnection,serialize($args[$argNb++]));
		default: fatal("db_build_query unkown format %$f");
		}		
	},$query);

	if($argNb!=$passedNbArgs)
	{fatal("db_build_query : bad nb args for $query\n");}

	return $res;
}

//! Execute an sql query, using placeholders. Returns mysqli_result object. Dies on failure.
function db_query()
{
	global $db_config,$dlib_config,$dbConnection,$dbSlowQueries;
	$args=func_get_args();
	$q=call_user_func_array('db_build_query',$args);
	//echo "query:$q\n";
	if($db_config['log_slow_queries']!==false){$startTime=microtime(true);}
	$qres=mysqli_query($dbConnection,$q);
	if($db_config['log_slow_queries']!==false)
	{
		$endTime=microtime(true);
		$dbSlowQueries[]=['duration'=>$endTime-$startTime,'query'=>$args[0]];
	}
	if($qres===false)
	{
		if(isset($dlib_config['debug']) && $dlib_config['debug'])
		{
			if(val($dlib_config,'is_commandline')){echo "Failed query:\n".$q."\n";}
			else                                  {echo "Failed query:<pre style=\"white-space: pre-wrap;\">".ent($q)."</pre>\n";}
		}
		fatal('db_query failed :' . mysqli_error($dbConnection));
	}
	return $qres;
}

//! Fetches a single line query result into an array. Returns null if no results.
function db_array()
{
	$args=func_get_args();
	$qres=call_user_func_array('db_query',$args);

	$res=mysqli_fetch_assoc($qres);
	mysqli_free_result($qres);
	return $res;
}

//! Fetches a single column query result into an array 
//! or a two column result into an assoc array.
function db_one_col()
{
	$args=func_get_args();
	$qres=call_user_func_array('db_query',$args);

	$res=[];
	while($line=mysqli_fetch_array($qres,MYSQLI_NUM))
	{
		if(count($line)>=2){$res[$line[0]]=$line[1];}
		else
		{$res[]=$line[0];}
	}
	mysqli_free_result($qres);
	return $res;
}

//! Fetches an array of arrays with all results.
function db_arrays()
{
	$args=func_get_args();
	$qres=call_user_func_array('db_query',$args);

	$res=[];
	while($line=mysqli_fetch_assoc($qres))
	{
		$res[]=$line;
	}
	mysqli_free_result($qres);
	return $res;
}

//! Like db_arrays(), but first col is used as key
function db_arrays_keyed()
{
	$res=call_user_func_array('db_arrays',func_get_args());
	if(!count($res)){return $res;}
	if(!count($res[0])){fatal("db_arrays_keyed: no fields found in first line");}
	$k=dlib_first_key($res[0]);
	$res=array_combine(dlib_array_column($res,$k),$res);
	return $res;
}

//! Fetches a single val (one column, one line) query result and returns it.
//! Fatal error on fail.
function db_result()
{
	$args=func_get_args();
	$qres=call_user_func_array('db_query',$args);
	if($qres===true){fatal('db_result: query does not return results');}
	$res=mysqli_fetch_row($qres);
	if($res===null){fatal('db_result: empty result set');}
	mysqli_free_result($qres);
	return $res[0];
}

//! Fetches a single val (one column, one line) query result and returns it.
//! Returns false on empty result set
function db_result_check()
{
	$args=func_get_args();
	$qres=call_user_func_array('db_query',$args);
	if($qres===true){fatal('db_result: query does not return results');}
	$res=mysqli_fetch_row($qres);
	if($res===null){return false;}
	mysqli_free_result($qres);
	return $res[0];
}

function db_fetch_object()
{ 
	return call_user_func_array('mysqli_fetch_object',func_get_args());
}

function db_fetch_array()
{ 
	return call_user_func_array('mysqli_fetch_array',func_get_args());
}

function db_fetch_assoc()
{ 
	return call_user_func_array('mysqli_fetch_assoc',func_get_args());
}

//! Escapes a string for inclusion in any db_query() function.
//! This uses mysqli_real_escape_string and also escapes '%' that is used for arguments.
function db_escape_string(string $string)
{
	global $dbConnection;
	return str_replace('%','%%',mysqli_real_escape_string($dbConnection,$string));
}

/** @} */

?>