// namespace
(function() {
window.dbobject_ui = window.dbobject_ui || {};
var ns=window.dbobject_ui;// shortcut

$(document).ready(function()
{
	// FIXME: most of this code assumes a single table is present. 
	var table=$('.dbobject-ui-list');
	if(table.length===0){return;}

	// ********* 
	// Expand (enlarge) longtext cols until they are either fully visible
	// or the table fills screen.
	var row=table.find('td.longtext').eq(0).parent();
	var maxWidth=100;
	// Expand in several steps, to avoid filling screen unnecessarily
	for(var i =0;i<5;i++)
	{
		var longTds=[];
		var padding=0;
		// find which cols need to be expanded
		row.find('td').each(function(j)
		{
			if($(this).hasClass('longtext') &&
			   $(this).innerWidth()>maxWidth-3)
			{
				longTds.push(j);
				padding+=parseFloat(window.getComputedStyle(this, null).getPropertyValue('padding-left' ).replace('px',''));
				padding+=parseFloat(window.getComputedStyle(this, null).getPropertyValue('padding-right').replace('px',''));
			}
		});
		//console.log(longTds);
		if(longTds.length==0){break;}
		// This is the max we can expand all cols at once.
		var maxAugment=($('#content').width()-row.width()-padding-30)/longTds.length;
		//console.log(maxAugment,$('#content').width(),row.width(),row);
		if(maxAugment<=0){break;}
		// Limit to only doubling width.
		maxWidth=Math.min(maxWidth+maxAugment,2*maxWidth);
		
		$.each(longTds,function(u,j)
		{
			table.find('tr td:nth-child('+(j+1)+')').css('max-width',maxWidth);
		});
		//console.log(row.width());
		//console.log(maxWidth);
	}

	// add_extra_scrollbar needs to be called delay, otherwise widths set by Expand longtext are not yet correct (?!).
	window.setTimeout(add_extra_scrollbar,10);

	// ********* 
	// Click on longtext for details
	if(!table.hasClass('no-popup'))
	{
		table.addClass('use-popup');

		// Add class "dbd" to each element that overflows
		table.find('td.longtext').each(function(){if(this.scrollWidth>1+this.getBoundingClientRect().width){this.className+=' dbd';};});

		table.on('mousedown','td.dbd',function(e)
		{
			if(e.which!=1){return;}
			e.stopPropagation();
			e.preventDefault();
			var td=$(this);
			if(td.hasClass('dbobject-ui-has-popup')){$('.dbobject-ui-popup').remove();td.removeClass('dbobject-ui-has-popup');return;}
			if($.trim(td.text())===''){return;}
			var colClass=this.className.match(/col-([^ ]+)/);
			if(colClass===null){return;}
			colClass=colClass[1];
			var url=$(this).parents('tr').eq(0).attr('data-view-field');

			if(typeof url==='undefined'){return;}
			url=url.replace('$field',colClass);
			$.get(url,
				  function(response)
				  {
					  $('.dbobject-ui-popup').remove();
					  $('.dbobject-ui-has-popup').removeClass('dbobject-ui-has-popup');
					  var popup=$('<div class="dbobject-ui-popup">');
					  popup.text(response.value);
					  $('body').after(popup);
					  var offset=td.offset();
					  offset.top+=td.height();
					  popup.offset(offset);
					  popup.click(function(){$(this).remove();td.removeClass('dbobject-ui-has-popup');});
					  td.addClass('dbobject-ui-has-popup');
				  },'json');
		});
	}
});

function add_extra_scrollbar()
{
	var table=$('.dbobject-ui-list');

	// Add an extra visible horizontal scrollbar if table is too wide and std scrollbar is too far down (out of window)
	// The extra scrollbar is created by two nested divs. It is display: fixed
	var wrapper=$('.dbobject-ui-list-wrapper');
	if(wrapper.length===0){return;}
	var tbody=table.find('tbody');
	var extraScrollBar=$('<div style="position:fixed;bottom: 0px;z-index:3;overflow-x: scroll;" id="extra-scrollbar"><div id="scrollbar-contents"></div></div>');
	extraScrollBar.css('left',wrapper.offset().left);
	extraScrollBar.width(wrapper.width());
	extraScrollBar.height(scrollbar_height());
	//console.log(wrapper.width(),wrapper[0].scrollWidth);
	extraScrollBar.find('div').width(wrapper[0].scrollWidth);
	extraScrollBar.find('div').height(3);
	var visible=false;
	extraScrollBar.hide();
	$('body').append(extraScrollBar);
	$(window).scroll(function()
	{
		//console.log(visible,wrapper.scrollLeft(),extraScrollBar.scrollLeft());
		if(wrapper[0].scrollWidth<=wrapper[0].clientWidth){return;}
		var rect=tbody[0].getBoundingClientRect();
		if(rect.bottom<window.innerHeight ||
		   rect.top   >window.innerHeight)
		{
			if(visible){visible=false;extraScrollBar.hide();}
		}
		else
		{
			if(!visible)
			{
				visible=true;
				var left=wrapper.scrollLeft();
				extraScrollBar.show();
				extraScrollBar.scrollLeft(left);
			}
		}
	}).scroll();
	extraScrollBar.scroll(function()
	{
		if(visible)
		{
			wrapper.scrollLeft(extraScrollBar.scrollLeft());
			table.find('.sticky-table-top-fixed').css('left',wrapper.offset().left-extraScrollBar.scrollLeft());
		}
	});
	wrapper.scroll(function()
	{
		if(!visible)
		{
			table.find('.sticky-table-top-fixed').css('left',wrapper.offset().left-wrapper.scrollLeft());
		}
	});
}

// http://stackoverflow.com/questions/986937/how-can-i-get-the-browsers-scrollbar-sizes
function scrollbar_height() 
{
    var outer = $('<div>').css({visibility: 'hidden', height: 100, overflow: 'scroll'}).appendTo('body');
	var heightWithScroll = $('<div>').css({height: '100%'}).appendTo(outer).outerHeight();
    outer.remove();
    return 100 - heightWithScroll;
};

//// Exports:
//window.dbobject_ui.xyz=xyz;
//// Global exports:
//window.xyz=xyz;

// end namespace wrapper
}());
