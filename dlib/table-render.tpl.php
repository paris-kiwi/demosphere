<?if(isset($config['top_text'])){?><?= $config['top_text']; ?>
<? }else if($tableTitle!==false){?>
	<h1 class="table-render-title">$tableTitle</h1>
<?}?>
<div class="table-render-wrapper
				<?: implode(' ',val($config,'wrapper_class',[])) ?>
				<?: $type==='query'            ? 'table-render-query' : '' ?>
				<?: $type==='class'            ? 'dbobject-ui-list'   : '' ?> ">
<? if($useSearch){ ?>
	<form method="get" class="table-render-search">
		<div>
			<input type="text" name="<?: $tableLabel ?>search" value="$searchTerms" placeholder="_(search)"/>
			<button type="submit"></button>
		</div>
	</form>
<?}?>
<a class="table-render-csv-export" href="<?: dlib_url_add_get(dlib_current_url()) ?>csvExport">[csv]</a>
<? if(count($lines)===0){?>
	<p>_(No items)</p>
<? }else{ ?>
<?= render('pager.tpl.php') ?>
<table class="table-render gray-table
				<?: implode(' ',val($config,'table_class',[])) ?>
				<?: $type==='query'			   ? 'table-render-query' : '' ?>
				<?: $type==='class'			   ? 'dbobject-ui-list'	  : '' ?>
				<?: $hasRotate!==false		   ? 'has-rotate has-rotate'.intval($hasRotate/5) : '' ?>
				<?: $isSortable				   ? 'sortable'			  : '' ?> 
				<?: $isSortable && !$hasPager  ? 'js-sort'			  : '' ?> 
				<?: $isSortable &&	$hasPager  ? 'server-sort'		  : '' ?> 
				<?: $hasRotate ? 'crowded' :''?> 
				column-hover" data-table-label="$tableLabel">
<thead>
	<tr class="sticky-table-top">
		<? foreach($head as $name=>$show){ ?>
			<th <?= isset($show['attr']) ? dlib_render_html_attributes($show['attr']) :'' ?>>
				<?= $show['disp'] ?>
			</th>
		<? } ?>
	</tr>
</thead>
<tbody>
<? foreach($lines as $lineNb=>$line) {?>
	<tr <?= dlib_render_html_attributes($lineAttrs[$lineNb]) ?>>
		<? foreach($line as $name=>$show){ ?>
			<td <?= isset($show['attr']) ? dlib_render_html_attributes($show['attr']) :'' ?>>
				<?= $show['disp'] ?>
			</td>
		<? } ?>
	</tr>
<?}?>
</tbody>
</table>
<?}?>
</div>
<?= val($config,'bottom_text','') ?>