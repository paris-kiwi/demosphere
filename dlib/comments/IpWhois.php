<?php

//! Cache for whois information on IP addresses.
//! Supports both IPv4 and IPv6
//! Whois IP information is useful when moderating anonymous comments.
class IpWhois extends DBObject
{
	static $id_        =['type'=>'autoincrement'];
	public $id;

	//! Start of ip range covered by this whois entry
	//! Binary representaiton of IP address.
	//! Convert to/from text with inet_ntop / inet_pton
	static $ipStart_   =[];
	public $ipStart;

	//! End of ip range covered by this whois entry
	static $ipEnd_   =[];
	public $ipEnd;

	//! Last time this entry was updated
	static $time_=['type'=>'timestamp'];
	public $time;

	//! A big string with the whole whois entry 
	//! Consists of multiple lines of text returned by whois server
	//! Most lines are of the form "name: value"
	static $entry_=[];
	public $entry;

 	static $dboStatic=false;

	//! Returns a cached whois object with a range containing $ip. Returns null if not found.
	static function findIp($ip)
	{
		$id=db_result_check("SELECT id FROM IpWhois WHERE INET6_ATON('%s')>=ipStart AND INET6_ATON('%s')<=ipEnd  LIMIT 1",$ip,$ip);
		return IpWhois::fetch($id,false);
	}
	
	//! Attempts to create and update a IpWhois object for this $ip.
	//! Returns null if update fails.
	static function create($ip) 
	{
		$ipWhois=new IpWhois();
		$ipWhois->ip=$ip;
		if($ipWhois->update()===false){return null;}
		$ipWhois->save();
		return $ipWhois;
	}

	//! Queries whois server and updates all fields in this object.
	function update()
	{
		// Query server using whois command line
		// In theory the protocol is simple and we could query a server directly.
		// In practice, there are a lot of different servers and quirks that are better handled by the command line tool.
		// There are also some PHP libraries for this, but they don't seem to work as well.
		$whoisLines=[];
		$isIPv6=strpos($this->ip,':')!==false;
		exec('whois '.escapeshellarg($this->ip),$whoisLines,$ret);
		if($ret!==0){return false;}

		$this->entry=implode("\n",$whoisLines);
		$this->entry=dlib_cleanup_plain_text($this->entry,false,false);

		// Setup IP range start/end fields
		if($isIPv6)
		{
			$raw=$this->getValue('inet6num');
			if($raw===false || !preg_match('@^[0-9a-f:]+\s*/\s*[0-9]+$@',$raw)){return false;}
			$range=ipv6_prefix_to_range($raw);
			if($range===false){return false;}
			list($ipStart,$ipEnd)=$range;
		}
		else
		{
			$raw=$this->getValue('inetnum');
			if($raw===false){$raw=$this->getValue('netrange');}
			if($raw===false){return false;}
			if(!preg_match('@^([0-9.]+)\s*-\s*([0-9.]+)$@',$raw,$m)){return false;}
			$ipStart=$m[1];
			$ipEnd=$m[2];
		}
		$this->ipStart=inet_pton($ipStart);
		$this->ipEnd  =inet_pton($ipEnd);
		if($this->ipStart===false || $this->ipEnd===false){return false;}

		$this->time=time();
	
		$this->save();
		return true;
	}

	//! Searches $this->entry for the first line starting by a name matching the $nameRe regex.
	function getValue($nameRe)
	{
		foreach(explode("\n",$this->entry) as $line)
		{
			$val=preg_replace('@^'.$nameRe.'\s*:\s*@i','',$line,-1,$ct);
			if($ct>0){return trim($val);}
		}
		return false;
	}

	static function createTable()
	{
		db_query("DROP TABLE IF EXISTS IpWhois");
		db_query("CREATE TABLE IpWhois (
  `id`               int(11)       NOT NULL auto_increment,
  `ipStart`          VARBINARY(16) NOT NULL,
  `ipEnd`            VARBINARY(16) NOT NULL,
  `time`             int(11)       NOT NULL,
  `entry`            longtext      NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `ipStart` (`ipStart`),
  KEY `ipEnd`   (`ipEnd`)
) DEFAULT CHARSET=utf8mb4");
	}
}

//! Example: '2a00:1450:4000::/37' => ['2a00:1450:4000::', '2a00:1450:47ff:ffff:ffff:ffff:ffff:ffff']
function ipv6_prefix_to_range(string $prefixed)
{
	$parts=explode('/',preg_replace('@\s@','',$prefixed));
	if(count($parts)!==2){return false;}
	$bits=ipv6_to_bits($parts[0]);
	$bits=substr($bits,0,$parts[1]);
	$start=ipv6_from_bits(str_pad($bits,128,'0',STR_PAD_RIGHT));
	$end  =ipv6_from_bits(str_pad($bits,128,'1',STR_PAD_RIGHT));
	return [$start,$end];
}

//! Example: 
//! "2a00:1450:4007:812::200e" => "00101010000000000001010001010000010000000000011100001000000100100000000000000000000000000000000000000000000000000010000000001110"
function ipv6_to_bits(string $ip)
{
	$bin=inet_pton($ip);	
	$res="";
	for($i=0;$i<16;$i++)
	{
		$b=decbin(ord($bin[$i]));
		$b=str_pad($b,8,'0',STR_PAD_LEFT);
		$res.=$b;
	}
	return $res;
}

//! Example: 
//! "00101010000000000001010001010000010000000000011100001000000100100000000000000000000000000000000000000000000000000010000000001110" => "2a00:1450:4007:812::200e"
function ipv6_from_bits(string $bits)
{
	$res='                ';
	for($i=0;$i<16;$i++)
	{
		$b=chr(bindec(substr($bits,8*$i,8)));
		$res[$i]=$b;
	}
	$res=inet_ntop($res);
	return $res;
}

?>