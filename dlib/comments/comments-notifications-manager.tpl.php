<h1 class="page-title">
	<? if($userId==$user['id']){?>
		_(My notifications)
	<?}else{?>
		_(Notifications for:) <a href="<?: $user['url']($userId) ?>"><?: $user['nameFct']($userId) ?></a>
	<?}?>
</h1>
<p>_(Receive notifications on your desktop or mobile browser when a non-admin visitor posts a comment.)</p>
<? if($userId==$user['id']){?>
	<p id="is-subscribed" class="hidden">_(Already subscribed on this browser)</p>
	<button id="subscribe">...</button>
<?}?>
<h4>_(Current subscriptions:)</h4>
<ul id="subscriptions">
<?foreach($subscriptions as $subscription){?>
	<li data-endpoint="$subscription['subscription']['endpoint']">
		$subscription['userAgent']<br/>
		_(Registered at:) <?: dlib_format_date('full-date-time',$subscription['time']) ?><br/>
		_(Last sent:)
		<?foreach(array_reverse($subscription['lastSent']) as $ct=>$sent){?>
			<? if($ct>20){echo "...";break;}?>
			<? $comment=Comment::fetch($sent['id'],false); ?>
			<a href="<?: $comment!==null ? $comments_config['link']($comment) : '' ?>"><?: dlib_format_date('relative-short-full',$sent['time']) ?></a>,
		<?}?>
		<br/>
		<div class="night">
			_(Night pause:) <input type="time" name="nightStart" value="<?: $subscription['nightStart'] ?? ''?>"/>-
						    <input type="time" name="nightEnd"   value="<?: $subscription['nightEnd'  ] ?? ''?>"  />
		</div><br/>
		<button class="unsubscribe">_(Unsubscribe)</button><br/>
	</li>
<?}?>
</ul>
<?if(!count($subscriptions)){?>
_(No subscriptions.)
<?}?>