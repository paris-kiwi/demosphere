// This is a very short JavaScript file that is included in pages displaying comments.
// It's purpose is to avoid loading jQuery on all pages that display comments. 
// JQuery is large and loading it always can be a performance problem.
// This script will automatically load jQuery when user hits a button.
(function()
{
window.dlib_comments = window.dlib_comments || {};
var ns=dlib_comments;

// Shortcut for document.getElementById
function _(i){return document.getElementById(i);}

onload=function()
{
	// Highlight comment when user follows links with comment hash (https://example.org/xyz#comment-123)
	onhashchange=function()
	{
		var c=document.querySelector('.highlighted');
		if(c)c.classList.remove('highlighted');
		var m=window.location.hash.match(/^#(comment-[0-9]+)$/);
		// If there are many comments, the highlighted comment might not be yet visible. This case is handled in comments.js
		if(m) 
			if(!_(m[1])) setup('hash',m[1])
	        else _(m[1]).classList.add('highlighted');
	};
	onhashchange();

	// Any click in comments, including buttons & see-more  (uses bubbling)
	_('comments').addEventListener('click',click);
	var c=_('comments-page-settings');
	if(c)c.onchange=function(e){setup('page-settings',c)};

	// Scroll listener to show more comments when user scrolls to bottom of page
	if(document.querySelector('.more-comments')!==null)
		addEventListener('scroll', function(e) 
		{
			var more=document.querySelector('.more-comments');
			if(more!==null && more.getBoundingClientRect().top-window.innerHeight<300)
			{
				if(ns.load_more){ns.load_more();}
				else setup('scroll');
			}
		});
};

function click(e)
{
	var t=e.target;
	if(t.tagName==='BUTTON')setup('button',t);

	if(t.parentNode.classList.contains('see-more'))
	{
		var l=t.parentNode.parentNode.classList;
		if(!l.contains('is-truncated'))l.remove('has-see-more');
		else setup('see-more',t);
	}
}

//! Load jQuery and main comment JavaScript file.
function setup(a,el)
{
	if(setup.ok)return;
	setup.ok=true;

	// Remove event listeners that will be handled in comments.js
	_('comments').removeEventListener('click',click);
	var c=_('comments-page-settings');
	if(c)c.onchange=null;

	// Tell main comments.js file that the current event needs to be handled.
	ns.action=a;
	ns.element=el;

	var as=typeof load_comments_js==='function' ? load_comments_js : function(){add_script('dlib/comments/comments.js');};

	if(typeof $==='undefined')
		add_script('lib/jquery.js',false,as);
	else
		as();
}

// Exports
ns.setup=setup;
}());
