<?php

//! A rule for banning commenters based on different critria.
//! For example Bans can be based on IP, sessionId, userId, ...
//! Ban results are determined by $action: for example: "pre-mod", "block" ...
class Ban extends DBObject
{
	static $id_      =['type'=>'autoincrement'];
	public $id;

	//! The type of this rule. Full list of possible values in $banTypes
	//! Some $banTypes have the same name as Comment fields, but not all of them.
	static $banType_    =[];
	public $banType;
	static $banTypes=['userId','sessionN','ip','ip-list','userAgentHash','word','not-country'];

	//! A value specific to this banType
	//! Example: $banType==='ip'     => $value='12.34.56.78'
	//! Example: $banType==='userId' => $value=543
	static $value_   =[];
	public $value;

	//! An action to take when this Ban rule matches.
	//! Full list of possible values in $actions
	//! "pre-mod" => comments are not published and need moderator aproval
	//! "block"   => cannot create a comment
	static $action_  =[];
	public $action;
	static $actions=['pre-mod','block'];

	//! Time when this Ban starts (usually current time)
	static $start_   =['type'=>'timestamp'];
	public $start;

	//! Time when this Ban ends. Use Ban::permanent() for permanent bans.
	static $end_     =['type'=>'timestamp'];
	public $end;

	//! A label that describes what this ban is about.
	//! All automatically managed bans start with '@'
	//! Examples: "@mod-button" if the moderator used the "ban" button
	static $label_   =[];
	public $label;

	//! An integer value to describe this ban.
	//! Example: a comment id it is associated to.
	static $labelValue_=['type'=>'int'];
	public $labelValue;

 	static $dboStatic=false;

	function __construct()
	{
		$this->start=time();
		$this->action='pre-mod';
	}

	//! Returns a list of bans that apply to $commentVals.
	//! $commentVals is usually a full Comment object, but it can also be an array with only some of the fields that a comment has.
	//! Returned bans can be limited to a certain type of action ($onlyOfAction).
	//! By default logged-in users are immune to bans unless the ban concerns explicitly their userId. This can be overridden with $userIdIsImmune.
	static function getForComment($commentVals,$onlyOfAction=false,$userIdIsImmune=true)
	{
		// Which banTypes can be found for a given commentVal
		$commentValToBanType=[
			'userId'       =>['userId'], 
			'sessionN'     =>['sessionN'], 
			'ip'           =>['ip','ip-list'], 
			'userAgentHash'=>['userAgentHash'], 
			'body'         =>['word'],
			'ipWhoisId'    =>['not-country'],
				  ];

		if($userIdIsImmune)
		{
			$v=(array)$commentVals;
			if(!isset($v['userId'])){fatal('Ban::getForComment: userIdIsImmune userId is not set');}
			if($v['userId']){$commentValToBanType=['userId'=>$commentValToBanType['userId']];}
		}
		$bans=[];
		foreach($commentVals as $key=>$commentVal)
		{
			foreach($commentValToBanType[$key] ?? [] as $banType)
			{
				$bans+=self::getForBanType($banType,$commentVal,true);
			}
		}
		if($onlyOfAction!==false)
		{
			foreach(array_keys($bans) as $k)
			{
				if($bans[$k]->action!==$onlyOfAction){unset($bans[$k]);}
			}
		}
		return $bans;
	}

	//! Return bans of $banType that apply to $commentVal
	//! Examples:
	//! Ban::getForBanType('sessionN',1234 )  returns all sessionN bans for session number 1234
	//! Ban::getForBanType('word'    ,'fck')  returns all word bans 
	static function getForBanType($banType,$commentVal)
	{
		// Cache for ip-list parsed files
		static $ipLists=[];

		// invalid values
		if(!$commentVal && ($banType==='sessionN' || $banType==='userId')){return [];}

		$now=time();
		$timeSql=" AND start<=".$now." AND end>".$now." ";

		$bans=[];
		switch($banType)
		{
		case 'sessionN':
		case 'userId':
		case 'ip':
		case 'userAgentHash':
			$bans=Ban::fetchList("WHERE banType='%s' AND value='%s' ".$timeSql,$banType,$commentVal);
			break;
		case 'word':
			$bans=Ban::fetchList("WHERE banType='word' AND '%s' LIKE CONCAT('%%',value,'%%') ".$timeSql,$commentVal);
			break;
		case 'not-country':
			$whois=IpWhois::fetch($commentVal,false);
			if($whois===null){break;}
			$bans=Ban::fetchList("WHERE banType='not-country' AND value!='%s' ".$timeSql,strtoupper($whois->getValue('country')));
			break;
		case 'ip-list':
			foreach(Ban::fetchList("WHERE banType='ip-list' ".$timeSql) as $ban)
			{
				$fname=$ban->value;
				if(!isset($ipLists[$fname]))
				{
					$lines0=@file_get_contents($fname);
					$ipLists[$fname]=$lines0!==false ? array_flip(explode("\n",trim($lines0))) : [];
				}
				if(isset($ipLists[$fname][$commentVal])){$bans[]=$ban;}
			}
			break;
		default:
			fatal('Ban::getForBanType: unknown banType:'.$banType);
		}
		return $bans;
	}

	//! Makes sure there is only one ban of a given banType and value, and returns it.
	//! Duplicates with shorter end times are deleted.
	static function unique(string $banType,string $value,array $labels=[])
	{
		$fieldBans=Ban::fetchList("WHERE banType='%s' AND value='%s' AND ".
                                  (count($labels) ? " label IN (".implode(',',array_map(function($s){return "'".db_escape_string($s)."'";},$labels)).") " : '').
								  " ORDER BY end DESC",
								  $banType,$value);
		$fieldBans=array_values($fieldBans);
		// cleanup redundant bans (not sure if this ever happens)
		for($i=1;$i<count($fieldBans);$i++){$fieldBans[$i]->delete();}
		$ban=$fieldBans[0] ?? null;
		return $ban;
	}

	//! "End" time for permanent bans. 
	//! FIXME-Y2038 problem. We need to switch all 32 bit timestamps to 64 bits.
	static function permanent()
	{
		return pow(2,31)-1; 
	}

	static function createTable()
	{
		db_query("DROP TABLE IF EXISTS Ban");
		db_query("CREATE TABLE Ban (
  `id`               int(11)       NOT NULL auto_increment,
  `banType`          varchar(191)  NOT NULL,
  `value`            varchar(191)  NOT NULL,
  `action`           varchar(191)  NOT NULL,
  `start`            int(11)       NOT NULL,
  `end`              int(11)       NOT NULL,
  `label`            varchar(191)  NOT NULL,
  `labelValue`       int(11)       NOT NULL,
  PRIMARY KEY       (`id`),
  KEY `banType`     (`banType`),
  KEY `value`       (`value`),
  KEY `action`      (`action`),
  KEY `start`       (`start`),
  KEY `end`         (`end`),
  KEY `label`       (`label`),
  KEY `labelValue`  (`labelValue`)
) DEFAULT CHARSET=utf8mb4");
	}
}
