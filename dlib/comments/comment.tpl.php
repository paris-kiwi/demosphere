<article id="comment-$comment->id" 
	 class="comment
		<?: $comment->parentId       ? 'has-parent'   :'' ?> 
		<?: $comment->nbChildren()>0 ? 'has-children' :'' ?> 
		<? template_tmp_label('see-more-class') ?>
		<?: !$comment->isPublished ? 'not-published'  : '' ?>
		<?: $isAdmin && $comment->needsAttention ? 'needs-attention' : ''  ?>
		<?: ($message ?? false) ? 'has-message'  : '' ?>
		" 
	 data-id="$comment->id"
	 data-parent-id="$comment->parentId"
	>
	<? if($message ?? false){ ?>
		<div class="comment-message">$message</div>
	<?}?>
	<h4 class="comment-topline">
		<span class="username <?: $banned['userId']!=='' ? 'banned' : '' ?> "
			  title="$banned['userId']">
			<a <? if($isAdmin){ ?> href="$base_url/comment/manager?filterField=<?: $comment->userId ? 'userId':'sessionN' ?>&filterValue=<?: $comment->userId ? $comment->userId : $comment->sessionN ?>" <?}?> >
				$comment->userName()
			</a>
		</span>
		<a href="<?: $comments_config['link']($comment) ?>" class="created"><?: dlib_format_date('short-date-time',$comment->created) ?></a>
		<? if($isAdmin){ ?>
			<? if(count($comment->extraData['pre-mod-reasons'] ?? [])){ ?>
				<span class="pre-mod" title="<?= implode(',&#13;',array_map("ent",$comment->extraData['pre-mod-reasons'])) ?>">
					pre-mod
				</span>
			<?}?>
			<span class="stats-info">
				<? spaceless ?>

				<? if($comment->userId!==0){?>
					<span class="stats-block comment-user <?: $banned['userId']!=='' ? 'banned' : '' ?>" data-field="userId">
						<?= $banPopups['userId'] ?>
						<a href="$base_url/comment/manager?filterField=userId&filterValue=$comment->userId"
						   class="stats-link"
						   title="$banned['userId']">user</a>:<? ?>
						<?= comments_stats_render($comment,'userId'  ,$statsCache ?? null) ?>
					</span><?=" "?>
				<?} else if($comment->sessionN!==0){?>
					<span class="stats-block comment-session <?: $banned['sessionN']!=='' ? 'banned' : '' ?>" data-field="sessionN">
						<?= $banPopups['sessionN'] ?>
						<a href="$base_url/comment/manager?filterField=sessionN&filterValue=$comment->sessionN"
						   class="stats-link"
						   title="$banned['sessionN']">
							$fieldNames['sessionN']
						</a>:<? ?>
						<?= comments_stats_render($comment,'sessionN'  ,$statsCache ?? null) ?>
					</span><?=" "?>
				<?}?>

				<span class="stats-block comment-ip <?: $banned['ip']!=='' ? 'banned' : '' ?>" data-field="ip">
					<?= $banPopups['ip'] ?>
					<a href="$base_url/comment/manager?filterField=ip&filterValue=$comment->ip"
					   class="stats-link"
					   title="$comment->ip &#13; $banned['ip']">
						$fieldNames['ip']
					</a>:<? ?>
					<?= comments_stats_render($comment,'ip'           ,$statsCache ?? null) ?>
				</span><?=" "?>

				<? if($comment->userAgentHash!=''){?>
					<span class="stats-block comment-ua <?: $banned['userAgentHash']!=='' ? 'banned' : '' ?>" data-field="userAgentHash">
						<?= $banPopups['userAgentHash'] ?>
						<a href="$base_url/comment/manager?filterField=userAgentHash&filterValue=$comment->userAgentHash"
						   class="stats-link"
						   title="<?: comments_user_agent_format($comment->userAgent) ?> &#13; $banned['userAgentHash']">
							$fieldNames['userAgentHash']
						</a>:<? ?>
						<?= comments_stats_render($comment,'userAgentHash',$statsCache ?? null) ?>
					</span><?=" "?>
				<?}?>

				<? end_spaceless ?>
			</span>
		<? } ?>
	</h4>
	<div class="comment-body">
		<?= comments_body_render($comment->body,true,$overflows) ?>
		<? if($overflows){template_tmp_label('see-more-class','has-see-more '.($overflows==='truncated' ? 'is-truncated' : ''));} ?>
	</div>
	<div class="see-more"><span>_(See more)</span></div>
	<div class="comment-actions">
		<? if($isAdmin){?>
			<span class="mod-buttons">
				<button name="ok">_(Accept)</button>
				<button name="unpublish">_(Reject)</button>
				<button name="unban">_(Unban)</button>
				<button name="ban"  >_(Ban)</button>
			</span>
		<?}?>
		<? if(!$comment->parentId && ($isAdmin || $comment->isPublished)){?> <button class="comment-reply">_(Reply)</button> <?}?>
		<? if($comment->canEdit($skipContext ?? false)                  ){?> <button class="comment-edit" >_(Edit)</button> <?}?>
	</div>
</article>
