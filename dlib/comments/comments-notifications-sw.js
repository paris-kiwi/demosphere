// FIXMEc: this whole file is work in progress.

self.addEventListener('push', function(event) 
{
	console.log('[Service Worker] Push Received.');
	console.log(`[Service Worker] Push had this data: "${event.data.text()}"`);

	var data={}
	if(event.data){data=event.data.json();}
	console.log('v5',event.data);
	const title = data.title  || 'Comment posted on Demosphere';
	const body  = data.body   || 'Check comment';
	const options = 
		  {
			  body: body,
			  silent: false,
			  vibrate: [300, 100, 400],
			  data: data.data || {},
			  actions: [{action:'ok',title:'ok'},{action:'close',title:'close'},],
		  };
	event.waitUntil(self.registration.showNotification(title, options));
});


self.addEventListener('notificationclick', function(event) 
{
	console.log('notificationclick:',event.notification.data);
	event.notification.close();
	if(event.action==='close'){return;}
	const url=(event.notification.data && event.notification.data.url) || false;
	//if(event.action==='ok') FIXMEc ...
	if(url!==false){clients.openWindow(url);}
});
