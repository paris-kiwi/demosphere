<?php

/** \addtogroup comments
 * @{ */

/**
 * A single comment for a simple comment forum.
 */
class Comment extends DBObject
{
	static $id_        =['type'=>'autoincrement'];
	public $id;
	//! Threaded comments: parentId!=0 means that this comment is a reply to the comment designated by parentId
	static $parentId_  =['type'=>'foreign_key','class'=>'Comment'];
	public $parentId=0;
	//! What type of page is this comment on: example 'Event',  'Post'... 
	static $pageType_      =[];
	public $pageType;
	//! The id of the page this comment is on: (eg Event id=1234, Post id=5432...)
	static $pageId_    =['type'=>'int'];
	public $pageId;
	//! Who created this Comment. 0 for anonymous users
	static $userId_    =['type'=>'foreign_key','class'=>'User'];
	public $userId=0;
	//! Whether comment is publicly visible. Non admins can see own comments even if they are not visible.
	static $isPublished_    =['type'=>'bool'];
	public $isPublished;
	//! Set when non admin adds/edits a comment
	static $needsAttention_=['type'=>'bool'];
	public $needsAttention;
	// The following fields (ip,session,userAgent,Whois,...) are used to help moderate comments of same anonymous posters.
	// They describe the person that created the comment (not the last edit)
	//! IP address
	static $ip_         =[];
	public $ip;
	//! The session number (dbsessions.n) that created this comment.
	//! Warning: this is not guaranteed to exist as dbsessions may be deleted.
	static $sessionN_   =['type'=>'int'];
	public $sessionN;
	//! User agent of browser that created this comment
	static $userAgent_  =[];
	public $userAgent;
	//! sha256 of user agent
	static $userAgentHash_=[];
	public $userAgentHash;
	//! id in IP address IpWhois table (a cache for whois information on this ip)
	static $ipWhoisId_  =['type'=>'foreign_key','class'=>'IpWhois'];
	public $ipWhoisId=0;
	//! Plain text body (no longer html)
	static $body_      =[];
	public $body;
	//! This is set at comment creation time and never changed afterwards.
	static $created_=['type'=>'timestamp'];
	public $created;
	//! Key used for sorting threaded comments.
	//! This is the "created" timestamp of either parent or self (if no parent).
	static $sortKey_   =['type'=>'timestamp'];
	public $sortKey;
	//! Experimental or secondary stuff
	//! Known keys: pre-mod-reasons
	public $extraData;
	static $extraData_          =['type'=>'array'];

 	static $dboStatic=false;

	static $settings=['disabled','readonly','pre-mod','enabled'];

	function __construct(string $pageType,int $pageId)
	{
		$this->pageType  =$pageType;
		$this->pageId=$pageId;
	}

	//! Short text computed from body
	function blurb(int $maxSize=40): string
	{
		$res=preg_replace('@[\n\r]+@s',' ; ',$this->body);
		$res=preg_replace('@\s+@s',' ',$res);
		$res=dlib_truncate($res,$maxSize);
		return $res;
	}

	//! Human readable field name
	static function statFieldName(string $field): string
	{
		return ['userId'       =>t('user'),
				'sessionN'     =>t('session'),
				'ip'           =>t('IP address'),
				'userAgentHash'=>t('browser type'),
			   ][$field];
	}

	//! Returns the number of published comments for a given pageType+pageId
	static function nb(string $pageType,int $pageId): int
	{
		return (int)db_result("SELECT COUNT(*) FROM Comment WHERE pageType='%s' AND pageId=%d AND isPublished=1",
							  $pageType,$pageId);
	}

	//! The actual username or "Anonymous-123" if not a logged-in user
	//! This can be overridden by a name in remarks
	function userName(): string
	{
		global $comments_config;
		if($this->userId){$res=$comments_config['user']['nameFct']($this->userId);}
		else             {$res=self::anonName($this->sessionN);}

		if(!$comments_config['user']['isAdmin']){return $res;}

		$remarks=$this->remarks();
		if(count($remarks['name']))
		{
			$abbrevs=['sessionN'=>'@','ip'=>'IP:','userAgentHash'=>'UA:',];
			$field=dlib_first_key($remarks['name']);
			return ($abbrevs[$field] ?? '').$remarks['name'][$field].
				($this->userId==0 ? '('.$this->sessionN.')' : '');
		}

		return $res;
	}

	//! Returns names and remarks that are set for this comment's fields.
	function remarks(): array
	{
		$res=['name'=>[],'remark'=>[]];
		$fields=['userId','sessionN','ip','userAgentHash'];
		foreach($fields as $field)
		{
			if($this->$field==='' || $this->$field===0){continue;}
			$remark=variable_get($this->$field,'comments-remarks-'.$field);
			if($remark===false){continue;}
			if($remark['name'  ]!==''){$res['name'  ][$field]=$remark['name'  ];}
			if($remark['remark']!==''){$res['remark'][$field]=$remark['remark'];}
		}
		return $res;
	}

	static function anonName(int $sessionN): string
	{
		return t('Anonymous').($sessionN!==0 ? '-'.$sessionN : '');
	}

	function nbChildren(): int
	{
		return intval(db_result("SELECT COUNT(*) FROM Comment WHERE parentId=%d",$this->id));
	}

	//! Returns whether curent user can view this comment
	//! Important: for consistent results, the conditions here must match those in comments_list_render()
	function canView(bool $skipContext=false): bool
	{
		global $comments_config;
		$user=$comments_config['user'];

		if($user['isAdmin']){return true;}

		if(!$skipContext)
		{
			require_once 'comments.php';
			if(!comments_page_can_view($this->pageType,$this->pageId)){return false;}
		}

		if($this->isPublished){return true;}

		// Only a comment's author can view un unublished comment
		if($this->userId   && $this->userId===$user['id']         ){return true;}
		if($this->sessionN && $this->sessionN===dbsessions_get_n()){return true;}

		return false;
	}

	//! Returns whether curent user can edit this comment
	function canEdit(bool $skipContext=false,string &$reason=null): bool
	{
		global $comments_config;
		$user=$comments_config['user'];
		$reason=false;

		if($user['isAdmin']){return true;}

		if(!$this->canView($skipContext)){return false;} 

		// Check page settings
		if(!$skipContext)
		{
			require_once 'comments.php';
			$settings=comments_page_settings($this->pageType,$this->pageId);
			if($settings!=='enabled' && $settings!=='pre-mod'){return false;}
		}

		// Only comment's author can edit it
		$isAuthor=false;
		if($this->userId   && $this->userId  ===$user['id']       ){$isAuthor=true;}
		if($this->sessionN && $this->sessionN===dbsessions_get_n()){$isAuthor=true;}
		if(!$isAuthor){return false;}
		
		// Do not allow user to edit his own comment once it has been rejected
		if(!$this->isPublished && !$this->needsAttention){return false;}

		// Do not allow user to edit his own comment after it was accepted if he was initially pre-moded
		if($this->isPublished && count($this->extraData['pre-mod-reasons'] ?? [])){$reason='pre-mod';return false;}

		// Cannot edit comment after it gets replies
		if($this->nbChildren()>0){$reason='children';return false;}

		return true;
	}

	//! Returns the number of comments having status $valField for the "person" (defined by $whoField) that posted this comment.
	//! Stats cover last 6 months.
	//! To avoid too many db requests, comments_list_render() pre-computes aggregates which can optionally be passed in $cached.
	//! Any changes here must be reflected in comments_list_render()
	//! Examples: $whoField='ip', $valField='needs-attention', $valField='12.34.56.78'
	function stats(string $whoField,string $valField,array $cached=null): int
	{
		if($this->$whoField==='' || $this->$whoField===0){return 0;}
		if($cached!==null)
		{
			$res=$cached[$whoField][$this->$whoField][$valField];
			if($valField==='same-page'){$res=$res[$this->pageType][$this->pageId];}
			return $res;
		}
		switch($valField)
		{
		case 'needs-attention':
			return db_result("SELECT COUNT(*) FROM Comment WHERE ".$whoField."='%s' AND created>%d AND needsAttention=1",
							 $this->$whoField,strtotime('6 months ago'));
		case 'published':
		case 'not-published':
			return db_result("SELECT COUNT(*) FROM Comment WHERE ".$whoField."='%s' AND created>%d AND needsAttention=0 AND isPublished=%d",
							 $this->$whoField,strtotime('6 months ago'),intval($valField==='published'));
			break;
		case 'same-page':
			return db_result("SELECT COUNT(*) FROM Comment WHERE pageType='%s' AND pageId=%d AND ".$whoField."='%s'",
							 $this->pageType,$this->pageId,$this->$whoField);
			break;
		default: fatal('strange valField');
		}
	}

	function save()
	{
		global $comments_config;
		if(!$this->id){$this->created=time();}

		$this->sortKey=false;
		if($this->parentId){$this->sortKey=db_result_check('SELECT created FROM Comment WHERE id=%d',$this->parentId);}
		if($this->sortKey===false){$this->sortKey=$this->created;}

		parent::save();
		if(isset($comments_config['save_hook'])){$comments_config['save_hook']($this);}
	}

	function delete()
	{
		global $comments_config;
		db_query('UPDATE Comment SET parentId=0, sortKey=created WHERE parentId=%d',$this->id);
		if(isset($comments_config['delete_hook'])){$comments_config['delete_hook']($this);}
		parent::delete();
	}

	static function createTable()
	{
		db_query("DROP TABLE IF EXISTS Comment");
		db_query("CREATE TABLE Comment (
  `id`              int(11)       NOT NULL auto_increment,
  `parentId`        int(11)       NOT NULL,
  `pageType`            varchar(100) CHARACTER SET ascii NOT NULL,
  `pageId`          int(11)       NOT NULL,
  `userId`          int(11)       NOT NULL,
  `isPublished`     TINYINT(1)    NOT NULL,
  `needsAttention`  TINYINT(1)    NOT NULL,
  `ip`              varchar(100) CHARACTER SET ascii  NOT NULL,
  `sessionN`        int(11)       NOT NULL,
  `userAgent`       varchar(500) CHARACTER SET ascii  NOT NULL,
  `userAgentHash`   varchar(100) CHARACTER SET ascii  NOT NULL,
  `ipWhoisId`       int(11)       NOT NULL,
  `body`            text          NOT NULL,
  `created`         int(11)       NOT NULL,
  `sortKey`         int(11)       NOT NULL,
  `extraData`       longblob      NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `parentId` (`parentId`),
  KEY `pageType` (`pageType`),
  KEY `pageId` (`pageId`),
  KEY `userId` (`userId`),
  KEY `isPublished` (`isPublished`),
  KEY `needsAttention` (`needsAttention`),
  KEY `ip` (`ip`),
  KEY `sessionN` (`sessionN`),
  KEY `userAgentHash` (`userAgentHash`),
  KEY `ipWhoisId` (`ipWhoisId`),
  KEY `created` (`created`),
  KEY `sortKey` (`sortKey`)
) DEFAULT CHARSET=utf8mb4");

	}
}


/** @} */

?>