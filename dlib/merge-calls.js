//! Wait for previous call to finish before calling a function.
//! If multiple call requests are received, only the last one is called.
//! Optionally, a delay can be added between function calls.
//! Usage example: (if user types fast, only a few calls will be made)
//!	input.keyup(function(e)
//!	{
//!		merge_async_calls('typeusername','call',1000,false,function()
//!		{
//!			$.post(url,data,function()
//!			{
//!				 ...
//!				 merge_async_calls('typeusername','end');
//!			});
//!		});
//!	});
function merge_async_calls(label,action,delay,quickStart,fct)
{
	var self=merge_async_calls;
	var now=Date.now();
	if(typeof self.labels==='undefined')
	{
		self.labels={};
	}

	if(action==='labels'){return Object.keys(self.labels);}
	if(action==='status'){return self.labels.hasOwnProperty(label) ?  self.labels[label].status : undefined;}
	if(action==='all-is-idle')
	{
		for(var l in self.labels)
		{
			if(self.labels.hasOwnProperty(l) && self.labels[l].status!=='idle'){return false;}
		}
		return true;
	}

	if(typeof self.labels[label]==='undefined')
	{
		self.labels[label]=
			{
				dbg:label,
				status:'idle',    // idle,delayed,called,pending
				fct:false,        // The function that needs to be called (last one received as "call" param)
				delayUntil: 0,
				delay: delay,
				quickStart: quickStart
			};
	}
	var info=self.labels[label];
	//console.log(label,action,'status before:',info.status);

	// status:
	// delayed: the function needs to be called but we need to wait a bit
	// called:  the function has been called, we expect an *end*
	// pending: same as "called" but the function needs to be called again

	if(action==='call'  && typeof fct!=='undefined'){info.fct=fct;}

	// *************** NO delay (simple case)
	if(!info.delay)
	{
		if(action==='call')
		{
			switch(info.status)
			{
			case 'idle':
				info.status='called';
				info.fct();
				break;
			case  'called':
				info.status='pending';
				break;
			case  'pending':
				break;
			}
		}
		else
		if(action==='end')
		{
			switch(info.status)
			{
			case 'idle': 
				// should not happen. Reset everything just in case.
				info.fct=undefined;
				console.error('merge_async_calls: received "end" call while status=',info.status);
				break;
			case  'called':
				info.status='idle';
				break;
			case  'pending':
				info.status='called';
				info.fct();					
				break;
			}
		}
	}
	else
	// *************** NOT quickStart
	// When a call request initially is received, we wait for a while before we really do it.
	// Examples:
	// idle > *call* > delayed > *timeout* >  called >  *end* > idle
	// idle > *call* > delayed > *call* > *timeout* >  called >  *call* > pending > *end* > delayed > *timeout* > called > *end* > idle
	if(!info.quickStart)
	{
		if(action==='call' || action==='timeout')
		{
			switch(info.status)
			{
			case 'idle':
				info.delayUntil=now+info.delay;
				info.status='delayed';
				window.setTimeout(function(){merge_async_calls(label,'timeout');},info.delay);
				break;
			case  'delayed':
				if(action==='timeout')
				{
					info.delayUntil=now; // just in case setTimeout is short (shouldn't happen)
					info.status='called';
					info.fct();
				}
				break;
			case  'called':
				info.delayUntil=now+info.delay;
				info.status='pending';
				break;
			case  'pending':
				break;
			}
		}
		else
		if(action==='end' )
		{
			switch(info.status)
			{
			case 'idle': 
			case  'delayed':
				// Should not happen. Reset everything just in case.
				info.delayUntil=0;
				info.status='idle';
				info.fct=undefined;
				console.error('merge_async_calls: received "end" call while status=',info.status);
				break;
			case  'called':
				info.status='idle';
				break;
			case  'pending':
				if(now<info.delayUntil)
				{
					info.status='delayed';
					window.setTimeout(function(){merge_async_calls(label,'timeout');},info.delayUntil-now);
				}
				else
				{
					info.status='called';
					info.fct();					
				}
				break;
			}
		}
	}
	else
	// *************** quickStart
	// When a call request initially is received, we do it immediately and set a grace period during which further calls will need to wait.
	// Examples:
	// idle > *call* > called > *end* > idle (with delayUntil)
	// idle > *call* > called > *call* > pending > *end* > delayed > *timeout* > called > *end* > idle
	{
		if(action==='call' || action==='timeout')
		{
			switch(info.status)
			{
			case 'idle':
				if(now<info.delayUntil)
				{
					info.status='delayed';
					window.setTimeout(function(){merge_async_calls(label,'timeout');},info.delayUntil-now);
				}
				else
				{
					info.delayUntil=now+info.delay;
					info.status='called';
					info.fct();
				}
				break;
			case  'delayed':
				if(action==='timeout')
				{
					info.delayUntil=now+info.delay;
					info.status='called';
					info.fct();
				}
				break;
			case  'called':
				info.status='pending';
				break;
			case  'pending':
				break;
			}
		}
		else
		if(action==='end')
		{
			switch(info.status)
			{
			case 'idle': 
			case  'delayed':
				// should not happen. Reset everything just in case.
				info.delayUntil=0;
				info.status='idle';
				info.fct=undefined;
				console.error('merge_async_calls: received "end" call while status=',info.status);
				break;
			case  'called':
				info.status='idle';
				break;
			case  'pending':
				if(now<info.delayUntil)
				{
					info.status='delayed';
					window.setTimeout(function(){merge_async_calls(label,'timeout');},info.delayUntil-now);
				}
				else
				{
					info.delayUntil=now+info.delay;
					info.status='called';
					info.fct();					
				}
				break;
			}
		}
	}
	//console.log(label,action,'status after:',info.status);
}	

//! Simplified wrapper around merge_async_calls() for non async functions.
function merge_calls(label,delay,quickStart,fct)
{
	merge_async_calls(label,'call',delay,quickStart,function()
					  {
						  fct();
						  merge_async_calls(label,'end');
					  });
}
