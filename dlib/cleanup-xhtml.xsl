<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
version="1.0" 
>

<xsl:output method="xml" omit-xml-declaration="yes"/>

<!-- non specified html node types are NOT copied (but child nodes might be) -->
<xsl:template match="node()" >
      <xsl:apply-templates select="node()"/>
</xsl:template>

<!-- list of html node types that are copied   -->
<xsl:template match="br|a|br|li|p|ul|ol|img|h1|h2|h3|h4|h5|h6|strong|em|hr|text()" >
         <xsl:copy>
              <xsl:apply-templates select="node() | @*"/>
         </xsl:copy>
</xsl:template>

<!-- list of html node types that are deleted (ignored)   -->
<xsl:template match="script" >
</xsl:template>

<xsl:template match="style" >
</xsl:template>

<xsl:template match="title" >
</xsl:template>



<!-- replace divs by <p>   -->
<xsl:template match="div" >
    <p>
       <xsl:apply-templates select="node()"/>
    </p>
</xsl:template>

<!-- replace dd by <p>   -->
<xsl:template match="dd" >
    <p>
       <xsl:apply-templates select="node()"/>
    </p>
</xsl:template>

<!-- replace tr by <p>   -->
<xsl:template match="tr" >
    <p>
       <xsl:apply-templates select="node()"/>
    </p>
</xsl:template>

<!-- replace h1 by <h2>   -->
<xsl:template match="h1" >
    <h2>
       <xsl:apply-templates select="node()"/>
    </h2>
</xsl:template>

<!-- replace h5,h6 by <h4>   -->
<xsl:template match="h5|h6" >
    <h4>
       <xsl:apply-templates select="node()"/>
    </h4>
</xsl:template>


<!-- replace th by <strong>   -->
<xsl:template match="th" >
    <strong>
       <xsl:apply-templates select="node()"/>
    </strong>
</xsl:template>

<!-- remove demosphere em   -->
<xsl:template match="em[@class='demosphere_timestamp']" >
       <xsl:apply-templates select="node()"/>
</xsl:template>

<xsl:template match="em[contains(@class,'demosphere_keyword')]" >
       <xsl:apply-templates select="node()"/>
</xsl:template>

<xsl:template match="cite[@class='demosphere_highlight_diff']" >
       <xsl:apply-templates select="node()"/>
</xsl:template>

<!-- FIXME: descendant of ul must be li   -->
<!-- hard to do correctly in XSL, try DOM   -->

<!-- remove block elements in li (list)   -->
<xsl:template match="li//p|li//h2|li//h3|li//h4|li//h5" >
    <br/><xsl:apply-templates select="node()"/><br/>
</xsl:template>

<!-- remove strong inside strong   -->
<xsl:template match="strong/strong" >
       <xsl:apply-templates select="node()"/>
</xsl:template>
<!-- remove em inside em   -->
<xsl:template match="em/em" >
       <xsl:apply-templates select="node()"/>
</xsl:template>


<!-- remove strong in titles   -->
<xsl:template match="h2//strong" >
       <xsl:apply-templates select="node()"/>
</xsl:template>
<xsl:template match="h3//strong" >
       <xsl:apply-templates select="node()"/>
</xsl:template>
<xsl:template match="h4//strong" >
       <xsl:apply-templates select="node()"/>
</xsl:template>

<!-- non specified attributes are deleted (ignored)   -->
<xsl:template match="@*" >
</xsl:template>

<!-- list of accepted attributes   -->
<xsl:template name="accepted-attributes" match="@class|@title|a/@href|img/@src|img/@alt|img/@width|img/@height" >
         <xsl:copy/>
</xsl:template>

</xsl:stylesheet>
