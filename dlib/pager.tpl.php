<p class="pager <?: $pager->nbPages()==1 ? 'pager-single-page' : '' ?> 
				<?: $pager->alwaysShow   ? 'pager-always-show' : '' ?>">
	<span class="pager-label">
		<? if($pager->nbPages()==1 || $pager->shortDisplay){ ?>
			<?:	t('!nbItems !itemName',
				['!nbItems' =>$pager->nbItems,
					  '!itemName'=>$pager->itemName,]) ?>
		<?}else{?>
			<?:	t('!nbItems !itemName on !nbPages pages:',
				  ['!nbItems' =>$pager->nbItems,
						'!itemName'=>$pager->itemName,
						'!nbPages' =>$pager->nbPages()]) ?>
	    <?}?>
	</span>
	<span class="pager-page-numbers">
		<?
			if($pager->nbPages()>1)
			{
				if($pager->shortDisplay){echo ": ";}
			    $currentPage=(int)round($pager->firstItem/$pager->itemsPerPage);
				$prev=-1;
				for($i=0;$i<$pager->nbPages();$i++)
				{		
					if($i==0 ||
					   ($i===2 && $currentPage===0) ||
					   $i===(int)round($currentPage/2) ||
					   $i===$currentPage-1 ||
					   $i===$currentPage ||
					   $i===$currentPage+1 ||
					   $i===(int)round(($currentPage+$pager->nbPages()-1)/2) ||
					   $i===$pager->nbPages()-1
					   )
				    {
						$item=1+$i*$pager->itemsPerPage;
						if($i!==$prev+1){?> ... <?}
						$prev=$i;
						if($i===$currentPage){ ?>
							<strong>$item</strong>
						<?}else{?>
							<a href="<?: $pager->url($item-1) ?>">$item</a>
						<?}
					}
				}
			}
		?>
	</span>
	<select onchange="window.location=this.value">
		<? foreach($pager->itemsPerPageChoices as $newItemsPerPage){ ?>
			<option value="<?: $pager->url(false,$newItemsPerPage)?>"
				<?= $newItemsPerPage==$pager->itemsPerPage ? ' selected="selected" ' :  '' ?>
				>
				<?: t('!nbItems/page',['!nbItems'=>$newItemsPerPage,'!itemName'=>$pager->itemName]) ?> 
			</option>
		<?}?>
	</select>
</p>
