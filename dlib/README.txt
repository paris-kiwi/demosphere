/*! \page Basic design ideas

******* Main design ideas:

Driving idea: minimize complexity 

- mostly procedural
- only use simple class based programming (OOP) when it is really necessary (no complex class hierarchies and patterns)
- program is split into loosely coupled "modules" that regroup some functionality
- each module has a global configuration variable that is constant and initialized by calling a setup function
- callbacks/handlers/hooks/events are provided to modules in config


******* Module design:

Main idea: splits program into several loosely coupled parts. 

*** When ?

- several functions , several hundred lines of code.
- need specific configuration
- more than one file
- little interaction with other modules
- a class is not a module, unless it is accompanied by other functions

*** How

- defines dependencies (what other modules / libraries are needed)
- setup function creates constant global config variable
- functions do not store state
- FIXME: std solution for handling state ?


*/
