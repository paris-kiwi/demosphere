<?php

//! Gathers information to be used when rendering a full html page.
//! Usually used through the $currentPage global variable.
//! Rationale: during a typical page display, most content is rendered by nested calling of functions that use templates.
//! $currentPage provides a mechanism to store information (css,js,meta tags,title...) that is produced in places
//! distant to the main page rendering.
//! $currentPage is not meant to be a way to pass global information around (bad practice).
//! It should be considered as write-only.
class HtmlPageData
{
	public $title=false;
	public $css=[];
	public $js=[];
	public $jsConfig=[];
	public $jsTranslations=[];
	public $head=[];
	public $robots=[];
	public $bodyClasses=[];

	//! $value can be either a file ($location==='file') a url ($location==='external') or actual CSS code ($location==='inline')
	//! If same css added more than once, subsequently added identical items will be ignored by renderCss() (both for normal and compressed)
	function addCss(string $value,string $location='file')
	{
		
		$this->css[]=['value'=>$value,'location'=>$location];
	}

	function addCssTpl(string $tpl,string $location='file')
	{
		require_once 'dlib/css-template.php';
		if($location==='file'){$value=css_template($tpl);}
		else                  {$value=css_template_compile($tpl);}
		$this->css[]=['value'=>$value,'location'=>$location];
	}

	function addJs(string $js,string $location='file',array $options=[])
	{
		$jsEntry=['js'=>$js,'location'=>$location,'options'=>$options];
		if(!($options['prepend']??false)){$this->js[]=$jsEntry;}
		else                             {array_unshift($this->js,$jsEntry);}
	}

	function addJsVar(string $name,$value)
	{
		$declareVar=strpos($name,'.')===false;
		$this->addJs(($declareVar ? 'var ' : '').$name.'='.dlib_json_encode_esc($value).';','inline');
	}

	function addJsConfig(string $module,array $names=[])
	{
		$this->jsConfig[$module]=array_merge(val($this->jsConfig,$module,[]),$names);
	}

	private function prependJsConfig()
	{
		global $base_url;
		$js='var base_url='.dlib_json_encode_esc($base_url).';'."\n";
		foreach($this->jsConfig as $module=>$names)
		{
			$varName=$module.'_config';
			$config=array_intersect_key($GLOBALS[$varName],array_flip($names));
			$js.='var '.$varName.'='.dlib_json_encode_esc($config).';'."\n";
		}
		array_unshift($this->js,['js'=>$js,'location'=>'inline']);
	}

	function addJsTranslations(array $tr)
	{
		$this->jsTranslations=array_merge($this->jsTranslations,$tr);
	}

	private function appendJsTranslations()
	{
		if(count($this->jsTranslations)===0){return;}
		$res='translations='.dlib_json_encode_esc($this->jsTranslations).";\n";
		$res.='function t(s,placeholders)
{
    var str=translations[s];
    if(typeof str==="undefined"){alert("missing translation:"+s);}
    if(typeof placeholders==="undefined"){return str;}
    for(var ph in placeholders)
    {
        if(!placeholders.hasOwnProperty(ph)){continue;}
        str=str.replace(prop,placeholders[prop]);
    }
    return str;
}';
		$this->addJs($res,'inline');
	}

	//! Returns the HTML tags for all CSS of this page
	//! Optionally compresses all inline and local file CSS into a single smaller CSS file.
	//! Optionally forces <link> CSS to become inline <style> CSS (example: for performance)
	function renderCss(bool $compress=false,bool $forceInline=false)
	{
		global $base_url,$dlib_config;
		$res='';

		// *** Apply css_file_alter hooks 
		// These transform a single CSS file into a list of CSS files (custom overrides)
		$descs=[];
		foreach($this->css as $desc)
		{
			if($desc['location']!=='file' || !isset($dlib_config['css_file_alter'])){$descs[]=$desc;continue;}
			$files=$dlib_config['css_file_alter']($desc['value']);
			foreach($files as $file)
			{
				$d=$desc;
				$d['value']=$file;
				$descs[]=$d;
			}
		}

		// *** Determine filename for compressed CSS 
		$alreadyCompressed=false;
		$compressedFname=false;
		if($compress)
		{
			$compressedCss='';
			// compute checksum using filemtime 
			$checksum='';
			foreach($descs as $desc)
			{
				$file=$desc['value'];
				if($desc['location']!=='file' || $forceInline){continue;}
				$checksum.=$file.':'.filemtime($file);
			}
			if($checksum!=='')
			{
				$compressedFname='files/css/compressed/'.md5($checksum).'.css';
				$alreadyCompressed=file_exists($compressedFname);
				$base_path=preg_replace('@^https?://[^/]*@','',$base_url).'/';
			}
		}

		// *** Build html for all css except for compressed file
		$alreadyAdded=[];
		foreach($descs as $desc)
		{
			if($desc['location']==='inline' || ($desc['location']==='file' && $forceInline))
			{
				
				if($desc['location']==='file')
				{
					// FIXME: this breaks relative paths in url(...)
					$css=file_get_contents($desc['value']);
				}
				else
				{
					$css=$desc['value'];
				}
				if(trim($css)==''){continue;}
				$res.='<style type="text/css">'."\n<!--/*--><![CDATA[/*><!--*/\n".
					($compress ? css_compress($css) : $css).
					"\n/*]]>*/-->\n".'</style>'."\n";
			}
			else
			if($desc['location']==='external')
			{
				$res.='<link type="text/css" rel="stylesheet" href="'.ent($desc['value']).'">'."\n";
			}
			else
			{
				if($alreadyCompressed){continue;}
				$file=$desc['value'];
				if(isset($alreadyAdded[$file])){continue;}
				$alreadyAdded[$file]=true;
				if($compress){$compressedCss.=css_compress(file_get_contents($file),$base_path.dirname($file).'/');}
				else
				{
					$res.='<link type="text/css" rel="stylesheet" href="'.ent($base_url.'/'.$file).'">'."\n";
				}
			}
		}

		// *** html for all css compressed into single file
		if($compress && $compressedFname!==false)
		{
			if(!$alreadyCompressed){file_put_contents($compressedFname,$compressedCss);}
			$res.='<link type="text/css" rel="stylesheet" href="'.ent($base_url.'/'.$compressedFname).'">'."\n";
		}
		return $res;
	}
	function renderJs()
	{
		global $base_url,$dlib_config;
		$res='';
		$alreadyAdded=[];
		foreach($this->js as $desc)
		{
			if($desc['location']==='inline')
			{
				// It seems that there is no foolproof way to safely encode arbitrary inline JS :-(
				// (encoding / parsing problems) 
				// We currently just check for dangerous code (better crash than hacked).
				// https://html.spec.whatwg.org/multipage/scripting.html#restrictions-for-contents-of-script-elements
				// https://www.owasp.org/index.php/XSS_(Cross_Site_Scripting)_Prevention_Cheat_Sheet (rule #3)
                // This should be safe (but might crash page on valid JS in very rare cases):
				// Try using addJsVar when possible
				if(strpos($desc['js'],'<!--'    )!==false ||
				   strpos($desc['js'],'<script' )!==false ||
				   strpos($desc['js'],'</script')!==false    ){fatal('Invalid inline JS');}
				$res.='<script type="text/javascript">'.$desc['js'].'</script>'."\n";
			}
			else
			if($desc['location']==='file' ||
			   $desc['location']==='external')
			{
				if(isset($alreadyAdded[$desc['js']])){continue;}
				$alreadyAdded[$desc['js']]=true;
				$src=$desc['js'];
				if($desc['location']==='file')
				{
					if((val($desc['options'],'cache-checksum') || val($dlib_config,'html_page_always_cache_checksum')) &&
					   val($desc['options'],'cache-checksum',1)!==false &&
					   val($dlib_config,'html_page_always_cache_checksum',1)!==false)
					{
						$checksum=substr(md5(filemtime($src)),0,5);
						$src=preg_replace('@^(.*)(\.[^.]+)$@','$1.cache-'.$checksum.'$2',$src);
					}
					$base_path=preg_replace('@^https?://[^/]*@','',$base_url).'/';
					$src=$base_path.$src;
				}

				if(isset($dlib_config['html_page_script_render']))
				{
					$res.=$dlib_config['html_page_script_render']($src,$desc);
				}
				else
				{
					$res.='<script type="text/javascript" '.
						'src="'.ent($src).'"'.
						(val($desc['options'],'defer') ? ' defer="defer"' : '').
						(val($desc['options'],'async') ? ' async="async"' : '').
						'></script>'."\n";
				}
			}
		}
		return $res;
	}
	function renderRobots()
	{
		if(count($this->robots))
		{return '<meta name="robots" content="'.ent(implode(', ',$this->robots)).'" />'."\n";}
		return '';
	}
	function variables(bool $compressCss=false,bool $forceInlineCss=false)
	{
		$res=(array)$this;
		$res['css']=$this->renderCss($compressCss,$forceInlineCss);
		$this->prependJsConfig();
		$this->appendJsTranslations();
		$res['js']=$this->renderJs();
		$res['robots']=$this->renderRobots();
		$res['bodyClasses']=$this->bodyClasses;

		//var_dump($res);
		return $res;
	}
}

//! Reduce css size by removing comments, whitespaces, and a few other things.
//! Copied from Drupal 7.16: drupal_load_stylesheet_content
//! $path: re-anchor url(..) statements using this path. 
//! $path should include trailing slash.
function css_compress(string $contents,$path=false) 
{
  // Remove multiple charset declarations for standards compliance (and fixing Safari problems).
  $contents = preg_replace('/^@charset\s+[\'"](\S*)\b[\'"];/i', '', $contents);

    // Perform some safe CSS optimizations.
    // Regexp to match comment blocks.
    $comment     = '/\*[^*]*\*+(?:[^/*][^*]*\*+)*/';
    // Regexp to match double quoted strings.
    $double_quot = '"[^"\\\\]*(?:\\\\.[^"\\\\]*)*"';
    // Regexp to match single quoted strings.
    $single_quot = "'[^'\\\\]*(?:\\\\.[^'\\\\]*)*'";
    // Strip all comment blocks, but keep double/single quoted strings.
    $contents = preg_replace(
      "<($double_quot|$single_quot)|$comment>Ss",
      "$1",
      $contents
    );
    // Remove certain whitespace.
    // There are different conditions for removing leading and trailing
    // whitespace.
    // @see http://php.net/manual/en/regexp.reference.subpatterns.php
    $contents = preg_replace('<
      # Strip leading and trailing whitespace.
        \s*([@{};,])\s*
      # Strip only leading whitespace from:
      # - Closing parenthesis: Retain "@media (bar) and foo".
      | \s+([\)])
      # Strip only trailing whitespace from:
      # - Opening parenthesis: Retain "@media (bar) and foo".
      # - Colon: Retain :pseudo-selectors.
      | ([\(:])\s+
    >xS',
      // Only one of the three capturing groups will match, so its reference
      // will contain the wanted value and the references for the
      // two non-matching groups will be replaced with empty strings.
      '$1$2$3',
      $contents
    );
    // End the file with a new line.
    $contents = trim($contents);
    $contents .= "\n";

	// Anchor all paths in the CSS, ignoring external and absolute paths.
	if($path!==false)
	{
		$contents=preg_replace_callback('/url\(\s*[\'"]?(?![a-z]+:|\/+)([^\'")]+)[\'"]?\s*\)/i',
			function($matches)use($path)
	        {
		     	return 'url('.$path.$matches[1].')';
	        }, 
	        $contents);
	}

  return $contents;
}

?>