<?php
/** @defgroup dbsessions dbsessions
 * Use database stored sessions instead of built-in PHP file-based sessions.
 * PHP file-based sessions are insecure in a shared hosting environment.
 * This module asumes a global $user object that has an attribute $user->id.
 * $user->id should be 0 for anonymous users.
 *
 * Usage: just call dbsessions_setup(); The rest is automatic.
 *
 * **Module use**: database
 *  @{
 */

function dbsessions_setup()
{
	session_set_save_handler('dbsessions_open',
							 'dbsessions_close',
							 'dbsessions_read',
							 'dbsessions_write',
							 'dbsessions_destroy',
							 'dbsessions_gc'
							 );
}

function dbsessions_install()
{
	db_query("DROP TABLE IF EXISTS dbsessions");
	// n=unique auto-incremented key to join other tables without using security-sensitive and inefficient id string.
	db_query("CREATE TABLE dbsessions (
  `id`           varchar(50) CHARACTER SET ascii  NOT NULL,
  `n`            int(11)       NOT NULL auto_increment,
  `uid`          int(11)       NOT NULL,
  `first_access` int(11)       NOT NULL,
  `last_access`  int(11)       NOT NULL,
  `data`         longblob      NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `id`  (`id`),
  KEY `n`   (`n`),
  KEY `uid` (`uid`),
  KEY `first_access` (`first_access`),
  KEY `last_access` (`last_access`)
) DEFAULT CHARSET=utf8mb4");
}

function dbsessions_open($savePath, $sessionName)
{
	return true;
}

function dbsessions_close()
{
	return true;
}

function dbsessions_read($id)
{
	global $dbsessions_read_session;
	// Write and close handlers are called after destructing objects
	// since PHP 5.0.5.
	// Thus destructors can use sessions but session handler can't use objects.
	// So we are moving session closure before destructing objects.
	register_shutdown_function('session_write_close');
	$res=db_array("SELECT * FROM dbsessions WHERE id='%s'",$id);
	$dbsessions_read_session=$res;
	return $res===null ? '' : $res['data'];
}

function dbsessions_write($id, $data)
{
	global $user,$dbsessions_read_session;

	$sessionDataHasChanged=!isset($dbsessions_read_session) || 
		$dbsessions_read_session['id']!=$id ||
		$dbsessions_read_session['data']!==$data;

	// Don't save this session if data hasn't changed and last access was less than 3 minutes ago
	if(!$sessionDataHasChanged && $dbsessions_read_session['last_access']>time()-180){return true;}

	$now=time();
	$uid=isset($user) ? $user->id : 0;

	// Note: "INSERT ... ON DUPLICATE KEY UPDATE" autoincrements "n" twice. This is a harmless, documented, problem:
	// https://dev.mysql.com/doc/refman/8.0/en/insert-on-duplicate.html
	db_query("INSERT INTO dbsessions (id,uid,first_access,last_access,data) VALUES ('%s',%d,%d,%d,'%s') ".
			 "ON DUPLICATE KEY UPDATE uid=%d, last_access=%d, data='%s'",
			 $id,$uid,$now,$now,$data,
			 $uid,$now,$data);
	return true;
}

function dbsessions_destroy($id)
{
	db_query("DELETE FROM dbsessions WHERE id='%s'",$id);
	return true;
}

//! Unused: your app should call dbsessions_cleanup() from cron.
function dbsessions_gc($maxlifetime)
{
	return true;
}

//! This is just an example.
//! It is up to the application to manage it's own session cleanup policy.
//function dbsessions_cleanup()
//{
// 	// Delete empty old anonymous sessions that have no data
// 	db_result_check("DELETE FROM dbsessions WHERE uid=0 AND last_access<%d AND LENGTH(data)=0",time()-3600*24*2);
//}

//! Returns the unique key "n" of the current session.
//! Sessions are normaly saved at the end of a request.
//! If this is a new session (created in this request), it will not exist yet in the db.
//! In that case, this function writes the current session and returns a valid n.
//! Result value is cached for performance.
function dbsessions_get_n(): int
{
	static $cached=false;
	if($cached!==false){return $cached;}

	$res=db_result_check("SELECT n FROM dbsessions WHERE id='%s'",session_id());
	if($res===false)
	{
		dbsessions_write(session_id(),'');
		$res=db_result_check("SELECT n FROM dbsessions WHERE id='%s'",session_id());
		if($res===false){fatal('dbsessions_get_n failed to re-read');}
	}
	$res=(int)$res;
	$cached=$res;
	return $res;
}

//! Parse session data. PHP's session_decode() puts result in $_SESSION :-(
function dbsessions_session_decode(string $sessionData): array
{
	$res=[];
	for($startIndex=0;;)
	{
		$nameEnd=strpos($sessionData, "|", $startIndex);
		if($nameEnd===false){break;}
		$name=substr($sessionData,$startIndex,$nameEnd-$startIndex);
		$rest=substr($sessionData,$nameEnd+1);
		$res[$name]=unserialize($rest,['allowed_classes'=>false]); // PHP will unserialize only what it needs
		// We don't know the actual length of the string used by unserialize. We re-serialize it to get the length.
		// This seems unreliable as there is no guarantee that serialize will always use the exact same format :-(
		$reSerialize=serialize($res[$name]);
		if(strpos($rest,$reSerialize)!==0){fatal('dbsessions_session_decode: inconsistent re-serialize');}
		$startIndex+=strlen($name)+1+strlen($reSerialize);
   }

   return $res;
}

function dbsessions_session_encode(array $array): string
{
	$res='';
	foreach($array as $key=>$value)
	{
		if(strpos($key,'|')!==false){return false;}
		$res.=$key.'|'.serialize($value);
	}
	return $res;
}

/** @} */

?>