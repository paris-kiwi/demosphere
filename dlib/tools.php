<?php

// **********************************************************
// general, misc fcts
// **********************************************************

//! $dlib_config : Common configuration options for dlib.
//! List of $dlib_config keys:
//! - debug
//! - is_commandline
//! - backtrace_file
//! - secret
//! - data_url_image_dir
//! - translation_files
//! - tmp_dir
//! - permission_denied_403_override
//! - log
//! - variable_unserialize_allowed_classes
//! - translation_exec_path
//! - no_wget_sites
//! - no_ssl_cert_sites
//! - html_page_script_render
//! - html_page_always_cache_checksum
//! - format_date
//! - form_wrapper_template
//! - form_item_templates
//! - error_log
//! - debug_log
//! - css_file_alter
//! -::-;-::-
$dlib_config;


//! Call this at the begining of your program.
function dlib_setup()
{
	global $dlib_config;
	if(!isset($dlib_config)){$dlib_config=[];}
	if(!isset($dlib_config['is_commandline'])){$dlib_config['is_commandline']=PHP_SAPI==='cli';}

	// All calls to mb_... functions will now default to utf8
	mb_internal_encoding("UTF-8");

	// FIXME: this is a bit intrusive. Otherwise PHP outputs "," in french for decimal numbers.
	// This breaks things in several places, including CSS templates (color).
	setlocale(LC_NUMERIC, 'C');

	// libxml generates too many errors, that saturate normal error handling.
	libxml_use_internal_errors(true);
}

// **********************************************************
// Logging and error related functions
// **********************************************************


//! Simple file-based logging. Messages are appended to a file.
//! Default filename can be configured in $dlib_config.
//! Log files are rotated once after 1Mb, and deleted afterwards.
function dlib_log(string $message,$logFile=false)
{
	global $dlib_config;
	if($logFile===false){$logFile=$dlib_config['log'] ?? false;}
	if($logFile===false){return;} // logging not setup, just ignore

	// rotate and delete logs
	if(file_exists($logFile) && filesize($logFile)>1000000)
	{
		if(file_exists($logFile.'.1')){unlink($logFile.'.1');}
		rename($logFile,$logFile.'.1');
	}

	$messagePrefix=date('Y-m-d H:i:s').":".
		'p'.sprintf("%05d",getmypid()).":".
		($_SERVER['REMOTE_ADDR'] ?? '-').":".
		preg_replace('@^(.{30}).+@','$1…',($_SERVER["REQUEST_URI"] ?? '-'));
		
	file_put_contents($logFile,$messagePrefix.': '.$message."\n",FILE_APPEND);
}

//! Log into error_log (or normal log) if they are set. Adds backtrace.
function dlib_log_error(string $message,$backtrace=false,$logFile=false)
{
	global $dlib_config;
	if($logFile===false){$logFile=$dlib_config['error_log'] ?? false;}
	if($logFile===false){$logFile=$dlib_config['log'      ] ?? false;}
	if($logFile===false){return;}

	require_once 'dlib-debug.php';
	$message.="\n";
	$message.=dlib_debug_backtrace($backtrace,'auto','text',false);
	dlib_log($message,$logFile);
}

//! Executes  a  shell  command.  
//! All  output (stdout  and  stderr)  is redirected into a log file.
//! If $failMessage is set and exec fails, the program aborts, otherwise exec status is returned.
//! If exec fails, a message is also written to error_log
//! $failMessage : false: just log in erro.log; string: fatal; null: ignore  (no log)
function dlib_logexec(string $command,$logFile=false,$failMessage=false)
{
	global $dlib_config;
	if($logFile===false){$logFile=$dlib_config['log'];}

	dlib_log("command: ".$command,$logFile);

	$pos=filesize($logFile);

	// execute command
	$fullCmd=$command."  >> ".escapeshellarg($logFile)." 2>&1";
	exec($fullCmd,$output,$ret);

	if($ret!=0)
	{
		$msg="command exec failed".($failMessage!==false ? ' : '.$failMessage : '');
		dlib_log($msg,$logFile);
		// echo failure 
		if($failMessage!==false && $failMessage!==null)
		{
			if($dlib_config['debug'] ?? false)
			{
				echo "command exec failed : ".ent($failMessage)."<br/>\n";
				echo "command:".ent($fullCmd)."<br/>\n";
				echo "return value:".ent($ret)."<br/>\n";
				// Display log file after $pos
				echo "Log file:<br/>\n";
				echo "<pre>\n";
				$f=fopen($logFile,"r");
				if($f===false){fatal('Cannot read logFile');}
				fseek($f,$pos);
				$out=fread($f,10000);
				fclose($f);
				echo ent($out);
				echo "</pre>\n";
			}
			dlib_log_error("command failed: ".$command);
			fatal($failMessage);
		}
		else
		if($failMessage!==null)
		{
			dlib_log_error($msg."\n"."command: ".$command);
		}
	}

	return $ret;
}


function dlib_bad_request_400($message=false)
{
	if(!function_exists('t')){function t($s){return $s;}}
	dlib_abort(t('Bad request (400)'),$message,400);
}

function dlib_not_found_404($message=false)
{
	if(!function_exists('t')){function t($s){return $s;}}
	if($message===false){$message=t('The requested URL was not found on this server.');}
	dlib_abort(t('Not found (404)'),$message,404);
}

//! $tryLogin=true is meant to be used in override to redirect unconnected user to login page
function dlib_permission_denied_403($message=false,bool $tryLogin=true)
{
	global $dlib_config;
	if(isset($dlib_config['permission_denied_403_override'])){$dlib_config['permission_denied_403_override']($message,$tryLogin);}
	if(!function_exists('t')){function t($s){return $s;}}
	dlib_abort(t('Permission denied (403)'),$message,403);
}

//! Display an error message and exit program with http status 500.
//! This should only be used for actual program errors. 
//! Use dlib_not_found_404()  and similar functions for errors related to user input.
function fatal(string $message)
{
	dlib_abort('Fatal',$message,500);
}

//! This is the backend function used to exit the program when there is an problem. 
//! You should call wrapper functions instead (fatal(),dlib_not_found_404(), ...).
//! This function generates appropriate http headers and error display according to context (html, text).
//! It also takes care of logging, if necessary.
//! $exception is used to display another stack trace
function dlib_abort(string $bigMessage,string $message,int $httpCode,$exception=false)
{
	global $dlib_config;
	static $alreadyCalled=false;
	if($alreadyCalled){die('Recursion during call to dlib_abort(). Aborting.');}
	$alreadyCalled=true;	

	require_once 'dlib-debug.php';

	$useHtml=!val($dlib_config,'is_commandline',PHP_SAPI==='cli');

	if(!headers_sent())
	{
		require_once 'download-tools.php';
		header('HTTP/1.0 '.$httpCode.' '.dlib_http_status_code($httpCode));		
		if($useHtml){header("Content-Type: text/html; charset=utf-8");}
		else        {header("Content-Type: text/plain; charset=utf-8");}
	}
	
	if($useHtml)	
	{
		echo '<div style="border: 2px solid '.($httpCode>=500 ? '#f00' : '#900').';background-color: '.($httpCode>=500 ? '#fcc' : '#c66').'">';
		echo '<h2 style="margin:0;padding: .5em;background-color:'.($httpCode>=500 ? '#f00' : '#900').';color: white;font-size: 17px;">'.
			ent($bigMessage).
			"</h2>\n";
		if($message!==false && $message!==''){echo '<p style="padding: .5em;margin:0;font-weight: bold;white-space: pre-line;">'.ent($message)."</p>\n";}
	}
	else
	{
		echo "\033[4m"."\033[1m"."\033[31m".$bigMessage."\033[0m";
		if($message!==false && $message!==''){echo ': '.$message;}
		echo "\n";
	}

	$backtrace=false;
	if($exception!==false)
	{
		$backtrace=$exception->getTrace();
		array_unshift($backtrace,['file'=>$exception->getFile(),'line'=>$exception->getLine(),'function'=>'dlib_abort']);
	}

	// Display stack, only if debugging is enabled
	if($dlib_config['debug'] ?? false)
	{
		echo dlib_debug_backtrace($backtrace)."\n";
	}

	if($useHtml)	
	{
		echo '</div>';
	}

	// Log real errors into error log
	if($httpCode>=500)
	{
		dlib_log_error($bigMessage.': '.$message,$backtrace);
	}

	exit(1);
}


// **********************************************************
// Array related functions
// **********************************************************

//! Return a value if key exists in array, or a default value otherwise
//! FIXME7 : in PHP 7, val($a,$k,d) can be replaced with $a[$k] ?? $d :-)
function val(array $array,$key,$default=false)
{
	if($array===null){return $default;}
	// isset is much faster than array_key_exists, however:
	// $ex=array('z'=>null);isset($ex['z']) === false ; but array_key_exists('z',$ex) === true
	//return isset($array[$key]) ? $array[$key] : $default;
	return !isset($array[$key]) || !array_key_exists($key,$array) ? $default : $array[$key];
}

//! return a value if property exists in object, or a default value otherwise
//! FIXME7 : in PHP 7, val($a,$k,d) can be replaced with $a[$k] ?? $d :-)
function oval($object,$attr,$default=false)
{
	return property_exists($object,$attr) ? $object->$attr : $default;
}

//! Returns the key or the value at a given numerical pos (even if the
//! array does not have integer indexes). 
//! Negative positions start from the end.
function dlib_array_at_pos(array $a,$pos,$key=false)
{
	$s=array_slice($a,$pos,1,true);
	if($key){return key($s);}
	else    {return current($s);}
}
function dlib_first_key(array $a)
{
	return key(array_slice($a,0,1,true));
}
function dlib_last_key(array $a)
{
	return key(array_slice($a,count($a)-1,1,true));
}
function dlib_first(array $a)
{
	return $a[dlib_first_key($a)];
}
function dlib_last(array $a)
{
	return $a[dlib_last_key($a)];
}

function dlib_key_pos(array $a,$k)
{
	return array_search($k,array_keys($a),true);
}

function dlib_prev_key(array $a,$k)
{
	$current=dlib_key_pos($a,$k);
	if($current===0){return false;}
	return dlib_array_at_pos($a,$current-1,true);
}
function dlib_next_key(array $a,$k)
{
	$current=dlib_key_pos($a,$k);
	if($current===count($a)-1){return false;}
	return dlib_array_at_pos($a,$current+1,true);
}

//! Inserts a new key/val into a position defined by a key inside an assoc array.
//! Note : also works for int indexed arrays
function dlib_array_insert_assoc(array $src,$where,$newKey,$newVal,$after=false)
{
	$keys  =array_keys($src);
	$values=array_values($src);
	$pos=array_search($where,$keys,true);
	if($pos===false){return false;}
	if($after){$pos++;}
	
	array_splice($keys  ,$pos,0,[$newKey]);
	array_splice($values,$pos,0,[$newVal]);
	return array_combine($keys , $values );
}

//! Extract all values for $key in an array containing arrays.
//! Example: 
//! $src=array(5 =>array('name'=>'tom','age'=>'25'),
//!            10=>array('name'=>'bil','age'=>'35'),);
//! $key='name'
//! result: array(5=>'tom',10=>'bil')
//! Note: PHP>=5.5 provides a built-in function for this called "array_column".
//! However, array_column does not index the array with the original key !! :-(
//! This is used frequently in dlib code, so we will keep our version.
//! Our version, by default ($ignoreMissing=false) generates an "undefined index" warning if the $key is missing in an element.
//! Note: our function with $ignoreMissing=true is equivalent to : array_combine(array_keys($src),array_column($src,$key))
function dlib_array_column(array $src,string $key,$ignoreMissing=false)
{
	$res=[];
	foreach($src as $k=>$v)
	{
		if(!$ignoreMissing || isset($v[$key])){$res[$k]=$v[$key];}
	}
	return $res;
}

//! Same as dlib_array_column(), for objects.
function dlib_object_column($src,string $key)
{
	$res=[];
	foreach($src as $k=>$v){$res[$k]=$v->$key;}
	return $res;
}

//! Returns the key of the array that minimizes the cost function.
function dlib_array_best(array $array,callable $costFct)
{
	$bestKey=false;
	$lowestCost=false;
	foreach($array as $key=>$val)
	{
		$cost=$costFct($val);
		if($bestKey===false || $cost<$lowestCost){$bestKey=$key;$lowestCost=$cost;}
	}
	return $bestKey;
}


// **********************************************************
// file, directory and filename
// **********************************************************

//! Generates a filename that doesn't exist in given directory.
//! FIXME: not foolproof, concurency.
function dlib_unique_fname(string $dir,string $fname)
{
	if(!file_exists("$dir/$fname")){return $fname;}
	for($i=0;$i<20;$i++)
	{
		$n=substr(md5(mt_rand()),0,10);
		$nfname="f-$n-$fname";
		if(!file_exists("$dir/$nfname")){return $nfname;}
	}
	fatal("error: unique_fname '$dir':'$fname'");
}

//! change a filename until we find one that does not
//! prefix any filename in the directory
function dlib_unique_basename(string $dir,string $fname)
{
	$scanRes=scandir($dir);
	for($i=0;$i<1000;$i++)
	{
		if($i==0){$nfname=$fname;}
		else
		{
			$n=substr(md5(mt_rand()),0,10);
			$nfname="f-$n-$fname";
		}
		$found=false;
		foreach($scanRes as $dirEntry)
		{
			if(strpos($dirEntry,$nfname)===0){$found=true;break;}
		}
		if(!$found){return $nfname;}
	}
	fatal("error: unique_basename '$dir':'$fname'");
}

//! Create a directory with aunique name inside another directory.
//! If PHP mkdir() is atomic (which seems to be true), this should be race-proof.
function dlib_make_tmp_dir($dir=false,$prefix='dlib-tmp.',int $mode=0700)
{
	global $dlib_config;
	if($dir===false){$dir=$dlib_config['tmp_dir'] ?? '/tmp';}
	if(!is_dir($dir)){return false;}
	// Probability of name collision : 1/62^(10*20) 
	for($i=0;$i<20;$i++)
	{
		$full=$dir.'/'.$prefix.dlib_random_string();
		if(@mkdir($full,$mode)===true){return $full;}
	}
	return false;
}

//! Returns a safe and clean filename with only simple caracters (a-z 0-9 - . _)
function dlib_clean_filename(string $fname,bool $removeExtension=false,int $maxFnameSize=80)
{
	$fname=trim($fname);
	$fname=dlib_remove_accents($fname);
	$fname=mb_strtolower($fname);
	// try our custom transilteration for any greek chars
	$langInfo=dlib_greek_language_info();
	$trans=$langInfo['transliteration'];
	$fname=str_replace(array_keys($trans),
					   array_map(function($a){return $a[0];},$trans),
					   $fname);
	// also try iconv transliteration
	$fname=iconv("UTF-8", "ASCII//TRANSLIT",$fname);

	$fname=preg_replace('@[^-a-z0-9.]@','_',$fname);
	// leave a dot only for extension : keep last dot in the fname
	$fname=preg_replace('@[.](?![^.]{1,5}$)@','_',$fname);
	if($removeExtension)
	{
		$fname=preg_replace('@[.].*$@','',$fname);
	}
	$fname=preg_replace('@^[.]@','_',$fname);// no names begining by .
	if($maxFnameSize && strlen($fname)>$maxFnameSize)
	{
		$cs=md5($fname);
		$l=max(5,$maxFnameSize-strlen($cs)-1);
		$fname=$cs."-".substr($fname,strlen($fname)-$l,$l);
	}
	if(strlen($fname)==0){$fname='empty-filename';}
	return $fname;
}

//! Returns everything after the dot in a filename (false if no extension)
function dlib_filename_extension(string $fname)
{
	return val(pathinfo($fname),'extension');
}


//! Removes everything after the dot in a filename, does nothing if no dot.
function dlib_remove_filename_extension(string $fname)
{
	return preg_replace('@[.][^./]+$@','',$fname);
}

//! Returns a list of all filenames that start with $base in directory $dir
function dlib_find_files_with_basename(string $dir,string $base)
{
	$files=[];
	$scanRes=scandir($dir);
	if($scanRes===false){fatal('find_files_with_basename:scandir:'.$dir);}
	foreach($scanRes as $dirEntry)
	{
		if(strpos($dirEntry,$base)===0){$files[]=$dirEntry;}
	}
	return $files;
}

// **********************************************************
// form token
// **********************************************************

//! Returns an html <input> element with form token.
function dlib_add_form_token(string $name='')
{
	return '<input type="hidden" class="dlib-form-token" name="dlib-form-token-'.ent($name).'" '.
		'value="'.dlib_get_form_token($name).'"/>';
}

//! Returns a long hash that is specific to this session and $name.
function dlib_get_form_token(string $name='')
{
	global $dlib_config;
	// $dlib_config['secret'] should be set to an arbitrary random (but persistent) string
	return hash('sha256',session_id().':'.$name.':'.$dlib_config['secret']);
}

//! Exits program with error message if correct token not found in $_POST
function dlib_check_form_token(string $name='',$post=false)
{
	if($post===false){$post=$_POST;}
	$good=dlib_get_form_token($name);
	if($good!==val($post,'dlib-form-token-'.$name)){dlib_permission_denied_403("invalid form submission",false);}
}

// **********************************************************
// string processing
// **********************************************************

//! Returns short (2 letter) locale name.
//! FIXME:  This is very old and needs some thought.
function dlib_get_locale_name($locale=false)
{
	static $known_locales=
		[
			'gl'=>'gl',
			'fr'=>'fr',
			'english'=>'en',
			'C'=>'en',
			'en'=>'en',
			'el'=>'el',
			'ca'=>'ca',
			'es'=>'es',
			'pt'=>'pt',
			'de'=>'de',
		];
	if($locale===false){$locale=setlocale(LC_TIME,"0");}
	$locale=preg_replace('@[._].*$@','',$locale);
	if(!isset($known_locales[$locale]))
	{
		fatal("unknown locale:".$locale.", please repair or add it to dlib_get_locale_name()");
	}
	return $known_locales[$locale];
}


//! Returns information (accents, transliteration) for greek
function dlib_greek_language_info()
{
	return 
		[
			'accents'=>['Ά'=>'Α','Έ'=>'Ε','Ή'=>'Η','Ί'=>'Ι','Ό'=>'Ο','Ύ'=>'Υ','Ώ'=>'Ω','ΐ'=>'ι','Ϊ'=>'Ι','Ϋ'=>'Υ','ά'=>'α','έ'=>'ε','ή'=>'η','ί'=>'ι','ΰ'=>'υ','ϊ'=>'ι','ϋ'=>'υ','ό'=>'ο','ύ'=>'υ','ώ'=>'ω','ώ'=>'ω','ς'=>'σ',],
			'transliteration'=>['α'=>['a'],'β'=>['b','v'],'γ'=>['g','y'],'δ'=>['d'],'ε'=>['e'],'ζ'=>['z'],'η'=>['h','i'],'θ'=>['th','8'],'ι'=>['i'],'κ'=>['k','c'],'λ'=>['l'],'μ'=>['m'],'ν'=>['n','v'],'ξ'=>['ks','x','3'],'ο'=>['o'],'π'=>['p'],'ρ'=>['r'],'σ'=>['s'],'τ'=>['t'],'υ'=>['y','u','v'],'φ'=>['f','ph'],'χ'=>['ch','x','h'],'ψ'=>['ps'],'ω'=>['w','o'],],					
		];
}

//! Convert html to text (removes tags and entities). 
//! This is 4x faster than Html2Text::convert(), but not as pretty or robust.
//! Html2Text::convert() parses the html with DOMDocument. This is regexp based.
function dlib_fast_html_to_text(string $string)
{
	$res=dlib_strip_tags($string);
	$res=dlib_html_entities_decode_all($res);
	$res=preg_replace('@  +@',' ',$res);
	return $res;
}


//! Remove html tags (including, DOCTYPE, CDATA,...) and html content for script and style.
//! Generaly, you should not use this function. In most cases, you should use fast_html_to_text instead.
//! Note: this is regexp based, so it might fail in some cases.
function dlib_strip_tags(string $string)
{
	// DOCTYPE
	$string=preg_replace('@<!DOCTYPE[^><]*>@s',' ',$string);

	// CDATA
	$string=preg_replace('@<!\[CDATA\[.*\]\]>@Us',' ',$string);

	// comments
	$string=preg_replace('@<!--.*-->@Us',' ',$string);

	// remove html head
	$head='</head>';
	$pos=strpos($string,$head);
	if($pos!==false)
	{
		$start=$pos+strlen($head);
		$string=substr($string,$start,strlen($string)-$start);
	}

	// remove everything in style and script tags : 
	$string=preg_replace("@<(style|script)[^><]*>[ /*\n|r]*</\\1>@Usi",
						 ' ',$string);

	// block tags (+br)
	$block='(address|article|aside|audio|blockquote|canvas|dd|div|dl|fieldset|figcaption|figure|footer|form|h\d|header|hgroup|hr|noscript|ol|output|pre|section|table|tfoot|ul|video|p|br)';
	$string=preg_replace('@<'.$block.'\b[^><]*>@',"\n",$string);

	// normal tags ([><] the "<" is in case there was a stray open tag
	$string=preg_replace('@<[a-zA-Z]+[^><]*>@','',$string);
	
	// end tags
	$string=preg_replace('@</[a-z]+[a-z0-9:]*\b>@i','',$string);

	// special
	$string=preg_replace('@<[?][^><]*>@Us',' ',$string);

	return $string;
}

//! Transform accented letters to non-accented ("é"=>"e").
//! For example, this can be used before comparing text or indexing. See also: dlib_simplify_text().
//! We used to use a hand-built per locale replacement array.
//! preg+normalizer is 3.6x slower  on large strings (1.4x on small ones).
//! preg+normalizer works for all languages.
//! small differences. Examples: "ø" and "ς" (greek)... which are fixed by hand
function dlib_remove_accents(string $str)
{
	// Decompose: é => 'e
	$res=normalizer_normalize($str,Normalizer::FORM_D);
	// Remove accents 'e => e
	$res=preg_replace("@\p{M}@u","",$res);
	// Renormalize (not sure if this is needed)
	$res=normalizer_normalize($res,Normalizer::FORM_C);
	// Extra fix
	$res=str_replace(['ς','ø','Ø'],['σ','o','O'],$res);
	return $res;
}

//! Simplifies text, by converting to lowercase, removing accents,
//! and only keeping allowed letters. Result is a list of space separated words.
//! For example, this can be used before indexing (for search).
function dlib_simplify_text(string $string,bool $isHTML=false)
{
	if($isHTML)
	{
		// remove html tags
		$string=dlib_fast_html_to_text($string);
	}

	// normalize, utf-8...
	$string=dlib_cleanup_plain_text($string); // FIXME: $removeHtmlEntities ? normally no, but maybe safer in practice?

	// lowercase
	$string=mb_strtolower($string);
	// remove accents
	$string=dlib_remove_accents($string);
	// Special replacements (ligatures...)
	// note: dlib_normalize_utf8() already removes display-only ligatures.
	// https://en.wikipedia.org/wiki/Typographic_ligature
	$string=str_replace(['œ','æ'],['oe','ae'],$string);
	// remove all non-letter caracters
	$string=preg_replace('@[^\p{L}\p{Nd} ]@u',' ',$string);
	// remove multiple spaces
	$string=preg_replace('@  *@',' ',$string);

	$string=trim($string);

	return $string;
}



//! shorthand for htmlentities
function ent(string $string)
{
	return htmlspecialchars($string,ENT_QUOTES,'UTF-8');
}

//! numerical entities for xml (like ent, except that it uses numerical entities instead of named entities)
function xent(string $str)
{	
	static $convmap = [34   ,    34 , 0, 0xff,	 // " 
					   38   ,    38 , 0, 0xff,	 // & 
					   39   ,    39 , 0, 0xff,	 // '
					   60   ,    60 , 0, 0xff,	 // < 
					   62   ,    62 , 0, 0xff,	 // > 
					  ];
	return mb_encode_numericentity($str, $convmap,'UTF-8');
}

//! Remove all entities from string. This includes all std xml/html (&quot;...), numerical and hex-numerical.
function dlib_html_entities_decode_all(string $str)
{
	$res=$str;
	$res=str_replace('&apos;',"'",$res);
	$res=html_entity_decode($res,ENT_QUOTES,'UTF-8');

	// mb_decode_numericentity does not handle hex entities (&#x03A3;) :-(
	// So we need to use a regexp:
	$res=preg_replace_callback('@&#([xX])?([0-9a-fA-F]{1,10});@',
							   function($matches)
							   { 
								   if($matches[1]===''){$n=(int)  $matches[2];}
								   else                {$n=hexdec($matches[2]);}
								   return dlib_chr_utf8($n); 
							   }, 
							   $res);
	return $res;
}

//! Converts any named or numeric entities into unicode.  
//! Does NOT decode html/xml chars : (&lt;&gt;&amp;&apos;&quot;).
//! This function can, for example, be used before text matching/parsing, where entities would get into
//! the way. The html will continue to display correctly.
function dlib_html_entities_decode_text(string $str)
{
	static $table=false;

	// FIXME: Why dlib_reset_locale ?
	if($table===false  || isset($GLOBALS['dlib_reset_locale'])) 
	{
		$table=array_flip(get_html_translation_table(HTML_ENTITIES,ENT_QUOTES,'UTF-8'));
		unset($table['&lt;']);
		unset($table['&gt;']);
		unset($table['&amp;']);
		unset($table['&apos;']);
		unset($table['&quot;']);
		unset($table['&#039;']);
	}
	// remove all named entities except the html ones (&lt;&gt;&amp;&apos;&quot;)
	$res=strtr($str,$table);

	// mb_decode_numericentity does not handle hex entities (&#x03A3;) :-(
	// So we need to use a regexp:
	$res=preg_replace_callback('@&#([xX])?([0-9a-fA-F]{1,10});@',function($matches)
							   { 
								   if($matches[1]===''){$n=(int)  $matches[2];}
								   else                {$n=hexdec($matches[2]);}
								   if($n===34 || $n===38	|| $n===39	|| $n===60	|| $n===62){return $matches[0];}
								   $res=dlib_chr_utf8($n);
								   if(!mb_check_encoding($res)){$res=dlib_chr_utf8(hexdec('FFFD'));}
								   return $res;
							   }, 
							   $res);
	return $res;
}


//! Multi-byte chr(): Will turn a numeric argument into a UTF-8 string.
//! http://stackoverflow.com/questions/2764781/how-to-decode-numeric-html-entities-in-php
function dlib_chr_utf8(int $num)
{
	if ($num < 128    ) return chr($num);
	if ($num < 2048   ) return chr(($num >> 6 ) + 192) . chr(($num         & 63) + 128);
	if ($num < 65536  ) return chr(($num >> 12) + 224) . chr((($num >>  6) & 63) + 128) . chr(($num        & 63) + 128);
	if ($num < 2097152) return chr(($num >> 18) + 240) . chr((($num >> 12) & 63) + 128) . chr((($num >> 6) & 63) + 128) . chr(($num & 63) + 128);
	return '';
}

//! Cleanup text by enforcing correct UTF8, normalizing and fixing common transcoding errors.
function dlib_cleanup_plain_text(string $str,bool $removeHtmlEntities=true,bool $fixStrangeChars=true)
{
	// enforce valid utf-8
	$str=mb_convert_encoding($str,"UTF-8", "UTF-8");

	if($removeHtmlEntities)
	{
		// some plain text files have entities :-(
		$str=dlib_html_entities_decode_all($str);
	}

	// replace common errors or non standard characters
	if($fixStrangeChars)
	{
		$str=dlib_fix_strange_chars($str);

		// replace unicode nbsp by ordinary space (U+00a0)
		$str=str_replace(" ",' ',$str);
	}

	$str=dlib_normalize_utf8($str);

	return $str;
}

//! Fix common transcoding errors and non standard characters used by certain applications (like MS-Office).
function dlib_fix_strange_chars(string $str)
{
	$str=str_replace(
		[
			""     ,// 1: bad quotes : Unicode "private use two" (U+0092)
			'â',// 2: bad quotes broken by iso-8859-1 / utf8 conversion (U+2019)
			'Å'    ,// 3: bad oe broken by iso-8859-1 / utf8 conversion (U+0153)
			""     ,// 4: bad "-" 'START OF GUARDED AREA' (U+0096)
			"\r\n"     ,// 5: remove dos CR
			"\r"       ,// 6: remove dos CR
			""     ,// 7: bad euro sign : '<control>' (U+0080)
			"¹"        ,// 8: bad apostrophe
			""     ,// 9: 'NEXT LINE (NEL)' (U+0085) => ellipsis (U+2026)
			""     ,// 10: 'STRING TERMINATOR' (U+009C) => oe
			""       ,// 11: - (openoffice?)  (U+F0B7 ?) => -
			"­"        ,// 12: soft hyphens
			'​'         ,// 13: Zero width space (U+200B)
			"‍"      ,   // 14: Zero width joiner (U+200D)
			"‌"      ,   // 15: Zero width non-joiner (U+200C)
		],
		["'",  // 1: 
		 "'",  // 2: 
		 "œ",  // 3: 
		 "-",  // 4: 
		 "\n", // 5: 
		 "\n", // 6: 
		 "€",  // 7: 
		 "'",  // 8: 
		 "…",  // 9: 
		 "œ",  // 10:
		 "-",  // 11: 
		 "",   // 12:
		 '',   // 13:
		 "",   // 14:
		 '',   // 15:
		],
		$str);

	return $str;
}

function dlib_normalize_utf8(string $str)
{
	// Replace unicode composition (e+' => é )
	$str=normalizer_normalize($str,Normalizer::FORM_C);
	
	// Remove "display-only" legacy ligatures. FIXME: This is good for French. How about other languages ?
	$str=str_replace(['ﬀ','ﬁ','ﬂ','ﬃ','ﬄ','ﬅ'],['ff','fi','fl','ffi','ffl','ft'],$str);	

	return $str;
}

//! Wrapper around json_encode, adds extra escapes to avoid XSS when using in <script>
function dlib_json_encode_esc($value,int $opts=0)
{
	return json_encode($value,JSON_HEX_TAG|JSON_HEX_AMP|JSON_HEX_APOS|JSON_HEX_QUOT|$opts);
}

//! Dlib suppresses normal errors from xml parsing as they are way too verbose. 
//! Use this function to print them.
function dlib_show_xml_errors(string $msg='',bool $return=false,bool $html=true)
{
	static $levels=[LIBXML_ERR_WARNING=>'NOTICE', 
					LIBXML_ERR_ERROR  =>'WARNING', 
					LIBXML_ERR_FATAL  =>'FATAL'];
	$out='';
	$eol=$html ? "<br/>\n" : "\n";
	$errors=libxml_get_errors();
	if(count($errors))
	{
		$out.=$eol."LIBXMLERRORS :".ent($msg).$eol;
	}
	foreach (libxml_get_errors() as $error) 
	{
		$out.= 'xml:'.$levels[$error->level].':';
		$out.= '('.
			ent($error->code).') : '.
			'file:'.ent($error->file).'::'.
			'line:'.ent($error->line).': '.
			ent($error->message);
		$out.= $eol;
	}
	libxml_clear_errors();
	if($return){return $out;}
	else       {echo $out;}
}

//! Takes an array ['a'=>'b','c'=>'d'] and renders html attributes ' a="b" c="d" '.
//! Special case: "class" attribute can optionally be an array.
function dlib_render_html_attributes(array $attributes)
{
	$out=' ';
	foreach($attributes as $attribute=>$value)
	{
		if($value===false){continue;}
		if($value===true ){$value=$attribute;}// Ex: checked="checked"
		if($attribute==='class' && is_array($value)){$value=implode(' ',$value);}
		$out.=ent($attribute).'="'.ent($value).'" ';
	}
	if(trim($out)===''){$out='';}
	return $out;
}

//! Returns a string of $length characters chosen from one of several predefined "alphabets".
function dlib_random_string(int $length = 10,string $charType='alnum') 
{
	static $characters = [
		                     'alnum'  =>'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
							 'alnumlc'=>'0123456789abcdefghijklmnopqrstuvwxyz',
						 ];
	$chars=$characters[$charType];
	$randomString = '';
	for($i = 0; $i < $length; $i++) 
	{
		$randomString.=$chars[random_int(0,strlen($chars)-1)];
	}
	return $randomString;
}

//! Truncate a string, adding ellipses
function dlib_truncate(string $s,int $length,string $ellipsis='…')
{
	if(mb_strlen($s)<=$length){return $s;}
	return mb_substr($s,0,$length).$ellipsis;
}

// **********************************************************
// ************* other      ********************
// **********************************************************


function dlib_redirect(string $url,bool $movedPermanently=false)
{
	header('Location: '.strtr($url,"\r\n","  "), true, $movedPermanently ? 301 : 302);
	exit();
}

//! Returns the url of the current request (page).
function dlib_current_url()
{
	$isHttps=val($_SERVER,'HTTPS') == "on";
	$pageURL = $isHttps ? "https://" : "http://";
	if(!(!$isHttps && $_SERVER["SERVER_PORT"]==80  ) &&
	   !( $isHttps && $_SERVER["SERVER_PORT"]==443 ))
	{
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} 
	else 
	{
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}

function dlib_host_url(string $url)
{
	if(!preg_match('@^(https?://[^/]+)@',$url,$m)){return false;}
	return $m[1];
}

function dlib_url_add_get(string $url)
{
	return $url.(strpos($url,'?')===false ? '?' : '&');
}

function dlib_size_with_units(int $size)
{
	static $units=[0=>'B',3=>'K',6=>'M',9=>'G',12=>'T',1000=>false];
	if($size==0){return 0;}
	$last=false;
	foreach($units as $v=>$unit)
	{
		if($unit===false || $size<.8*pow(10,$v))
		{return round($size/pow(10,$lastV),2).' '.$last;}
		$last=$unit;
		$lastV=$v;
	}
}

//! Splits an email address into several parts and returns an array.
//! Does not attempt any validation
//! Returned array always contains 'address' and may or may not also contain name,clean-name,local,domain :
//! Example: "name"<local@domain> => ['address'=>'local@domain', 'name'=>'"name"', 'clean-name'=>'name']
function dlib_email_address_parts(string $full)
{
	$res=[];
	$full=trim($full);
	$address=$full;
	if(preg_match('@^(?P<name>[^<>]*)<(?P<address>[^<>]+)>$@',$full,$m))
	{
		$address=$m['address'];
		$res['name']=trim($m['name']);
		$res['clean-name']=preg_replace('@^"(.*)"$@','$1',$res['name']);
	}
	$res['address']=$address;
	
	if(preg_match('/^(?P<local>.*)@(?P<domain>.*)$/',$address,$m))
	{
		$res['local' ]=$m['local'];
		$res['domain']=$m['domain'];
	}
	return $res;
}

//! Returns true if the domain of this $email address is valid (false otherwise).
//! This actually checks DNS records and hostname, so it can be slow.
function dlib_email_check_domain(string $email)
{
	$parts=dlib_email_address_parts($email);
	if(!isset($parts['domain'])){return false;}
	$domain=$parts['domain'];

	return checkdnsrr($domain,"MX")===true || 
		   checkdnsrr($domain,"A" )===true ||
		   gethostbyname($domain)!==$domain;
}

// ***************************************
// ********** top of page messages  ******
// ***************************************

function dlib_message_add(string  $message,string $type='status')
{
	$_SESSION['dlib_messages'][$type][]=$message;
}

function dlib_message_display()
{
	if(!isset($_SESSION['dlib_messages'])){return '';}
	$out='';
	foreach($_SESSION['dlib_messages'] as $type=>$messages)
	{
		$out.='<div class="messages '.ent($type).' '.(count($messages)>1 ? '' : ' single ').'">';
		$out.='<ul>';
		foreach($messages as $message)
		{
			$out.='<li>'.$message.'</li>';
		}
		$out.='</ul>';
		$out.='</div>';
	}
	unset($_SESSION['dlib_messages']);
	return $out;
}

//! Wrapper to fix  nasty bug in tidy_repair_string()
//! Tidy breaks locale: https://github.com/htacg/tidy-html5/issues/770
function dlib_tidy_repair_string($data,$config,$encoding)
{
	$all=setlocale(LC_ALL,"0");
	$list=[];
	foreach(explode(";",$all) as $expr)
	{
		if(strpos($expr,"=")===false){continue;}
		list($category,$value)=explode("=",$expr);
		$list[$category]=$value;
	};

	$res=tidy_repair_string($data,$config,$encoding);

	if(isset($list['LC_TIME'])){setlocale(LC_ALL,$list['LC_TIME']);}

	foreach($list as $category=>$value)
	{
		if(@constant($category)===null){continue;}
		setlocale(constant($category),$value);
	} 
	return $res;
}

?>