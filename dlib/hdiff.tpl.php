<div class="hdiff-main clearfix">
	<canvas class="hdiff-canvas"></canvas>
	<? foreach($cols as $colNb=>$col){ ?>
		<?if($colNb===0){?><input class="ignore-case" type="checkbox" title="_(Ignore case)"/><?}?>
		<div class="hdiff-col hdiff-col-$colNb 
					<?: isset($col['save']) ? 'editable' : '' ?> 
					<?: $col['class'] ?? '' ?>"
			 data-save="<?: $col['save'] ?? '' ?>"
			 <?= dlib_render_html_attributes($col['attributes'] ?? []) ?> >
			<div class="hdiff-col-inner">
				<div class="hdiff-col-top">
					<? if(isset($col['title'])){?><h2 class="hdiff-title"><?= $col['title'] ?></h2><?}?>
					<? if(isset($col['info' ])){?><div class="hdiff-info"><?= $col['info'] ?></div><?}?>
					<div class="hdiff-editor-tools">
						<div class="hdiff-tinymce-toolbar"><button class="save" title="_(Save)"><span></span></button></div>
						<span class="hdiff-tools-msg">_(Click text to edit)</span>
					</div>
				</div>
				<div class="hdiff-html hdiff-html-$colNb">
					<div class="hdiff-html-spacer-top hdiff-html-spacer"></div>
					<div class="hdiff-html-inner hdiff-html-inner-$colNb"><?= $col['html'] ?></div>
					<div class="hdiff-html-spacer-bot hdiff-html-spacer"></div>
				</div>
			</div>
		</div>
	<?}?>
</div>
