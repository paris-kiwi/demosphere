<?php

//! Finds the first action(function) corresponding to $reqPath, executes it and (optionally) displays results.
//! The list of url/path descriptions is given by $descs.
//! Each item in $descs describes a path/action pair with access control and output options....
//! This function normally does return (null). You should not display anything after it.
function dlib_paths_exec($reqPath,$descs,array $options=[])
{
	global $user;

	if($reqPath===false || $reqPath===''){$reqPath='/';}

	// *** Search for first desc that matches $reqPath
	$foundKey=dlib_paths_search($reqPath,$descs);
	if($foundKey===false){dlib_not_found_404();}
	$desc=$descs[$foundKey];
	
	// *** check permissions
	$accessOk=dlib_paths_desc_access($desc,$reqPath);
	if(!$accessOk){dlib_permission_denied_403();}

	// *** hook for doing something before main function call 
	// (for example: sending headers for un-cached pages)
	if(isset($options['before_function_hook'])){$options['before_function_hook']($desc);}

	// *** call page function
	// Also catch all exceptions
	try
	{
		$returned=dlib_paths_desc_call($desc,$reqPath);
	}
	catch(Exception $e)
	{
		dlib_abort('Uncaught exception',$e->getMessage(),500,$e);
	}

	// *** Render value returned by function (but do not output it yet)
	list($headers,$out)=dlib_paths_desc_render_returned($desc,$returned,$options);

	// *** Actual output
	if(val($options,'custom_output'))
	{
		return [$out,$headers,$desc];
	}
	else
	{
		foreach($headers as $name=>$val){header($name.': '.$val);}
		// *** Actual display is done here:
		if($out!==null){echo $out;}
	}
}

//! Search for first description that matches $reqPath. 
//! Returns key in $descs array or false.
function dlib_paths_search($reqPath,array $descs)
{
	if($reqPath===false || $reqPath===''){$reqPath='/';}

	foreach($descs as $k=>$desc)
	{
		$reParts=false;
		if(!isset($desc['path'])){var_dump($desc);fatal('dlib_exec_request_path: no path set in this description');}
		$path=$desc['path'];
		if(strpos($path,'@')!==0)
		{
			if($reqPath===$path){return $k;}
		}
		else
		{
			if(dlib_paths_desc_preg_match($desc,$reqPath)!==false){return $k;}
		}
	}
	return false;
}


//! Is the user allowed to view $reqPath ?
//! Shortcut for outside use.
//! The $euser arg is optional (default is current user (global $user)).
//! Returns null (instead of false) if path is not found.
function dlib_paths_access($reqPath,array $descs,$euser=false)
{
	if($euser===false){$euser=$GLOBALS['user'];}
	$foundKey=dlib_paths_search($reqPath,$descs);
	if($foundKey===false){return null;}
	return dlib_paths_desc_access($descs[$foundKey],$reqPath,$euser);
}

//! Is the user allowed to view $reqPath, given a path desc ?
//! The user arg is optional (default is current user (global $user)).
function dlib_paths_desc_access($desc,$reqPath,$euser=false)
{
	if($euser===false){$euser=$GLOBALS['user'];}
	// If callback function is provided: use it
	$accessOk=false;
	if(isset($desc['access']))
	{
		if(isset($desc['file'])){require_once $desc['file'];}
		$reParts=dlib_paths_desc_preg_match($desc,$reqPath);
		if($reParts===false){$accessOk=$desc['access']($euser,$reqPath,$desc)==true;}
		else
		{
			// Reorder args: $euser,$match1,$match2,...,$reqPath,$desc
			$args=$reParts;
			array_shift($args);
			array_unshift($args,$euser);
			$args[]=$reqPath;
			$args[]=$desc;
			$accessOk=call_user_func_array($desc['access'],$args)==true;
		}
	}
	else
	{
		// Otherwise use roles (default to "admin")
		$roles=val($desc,'roles','admin');
		$accessOk=$roles===true || // true means anybody can access
			$euser->checkRoles($roles);
	}
	return $accessOk;
}

//! Call the function associated to a $desc.
//! 'file' is included if provided. If desc path is a regexp, $reqPath is split and passed as args to function.
//! Note: this does not send header or output anything returned.
function dlib_paths_desc_call($desc,$reqPath)
{
	if(isset($desc['file'])){require_once $desc['file'];}
	$exec=false;
	if(!isset($desc['function'])){fatal('dlib_paths_desc_call: missing function for item');}
	$exec=$desc['function'];
	$reParts=dlib_paths_desc_preg_match($desc,$reqPath);
	if($reParts===false){$returned=$exec();}
	else
	{
		array_shift($reParts);
		$returned=call_user_func_array($exec,$reParts);
	}
	return $returned;
}


//! Render value returned by function (but do not output it yet)
//! This function returns two values: $headers and $out
//! This function does not display anything.
function dlib_paths_desc_render_returned($desc,$returned,$options=[])
{
	$outputType=val($desc,'output','default_output_render');
	if($outputType===false){$outputType='none';}
	$out=null;
	// Headers that need to be output (typically : Content-Type)
	$headers=[];
	switch($outputType)
	{
	// Most common case: output $returned using default function (typically a page template)
	case 'default_output_render': 
		if(!isset($options['default_output_render']))
		{fatal('dlib_exec_request_path: output is "default_output_render"(or unset), but default_output_render option is is not set.');}
		$headers['Content-Type']='text/html; charset=UTF-8';
		if(isset($options['default_output_render_file'])){require_once $options['default_output_render_file'];}
		$out=$options['default_output_render']($returned);
		break;
	case 'custom': 
		if(!is_array($returned)){fatal('dlib_exec_request_path: output is "custom" but function did not return an array');}
		$headers=$returned['headers'];
		$out=$returned['out'];
		break;
	case 'none': 
		if($returned!==null){fatal('dlib_exec_request_path: output is "none"(false) but function returned something');}
		break;
	// Custom html: output $returned as is ($returned is a full html page)
	case 'html':  
		$headers['Content-Type']='text/html; charset=UTF-8';
		$out=$returned;
		break;
	// $returned is an array or object that will be json encoded
	case 'json': 
		$headers['Content-Type']='application/json; charset=UTF-8';
		$out=json_encode($returned);
		break;
	default: fatal('Invalid render value in path desc');
	}
	if($out===null && $outputType!=='none'){fatal('dlib_exec_request_path: output is not "none"(false) but function returned null');}

	return [$headers,$out];
}

//! Splits $reqPath using regexp path description.
//! return false if it is not a regexp path description.
function dlib_paths_desc_preg_match($desc,$reqPath)
{
	if(strpos($desc['path'],'@')!==false && preg_match($desc['path'],$reqPath,$reParts)){return $reParts;}
	return false;
}

?>