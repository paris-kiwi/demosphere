<?php
//! Downloads an html page given an URL, and fixes links.
//! Note this does nothing else to the page (no encoding fixes, no tidy).
//! You probably want to use : dlib_download_and_clean_html_page()
//! This function can either download to a file or return the download as a string.
//! @param $url: 
//! @param $fname: 
//! @param $options: only 'log' is used here, see dlib_curl_download() for other opts.
//! @return false on failure. On success true if fname is given, result downloaded html otherwise.
function dlib_download_html_page(string $url,$fname=false,array $options=[])
{
	global $dlib_config;
	$tmpOptions=$options;
	$tmpOptions['headers']=&$getHeaders;
	$html=dlib_curl_download($url,false,$tmpOptions);
	$options['headers']=$getHeaders;
	if($html===false){return false;}
	$contentType=val($getHeaders,'content-type');
	if(!isset($options['allowNonHtmlContentTypes']) && 
	   strpos($contentType,'text/html')!==0)
	{
		dlib_download_html_error('headers say this is not a text/html page, aborting. Found content type:"'.ent($contentType).'"',$options);
		return false;
	}

	require_once 'dlib/html-tools.php';

	// Automatically remove src="data:..." urls
	if(isset($dlib_config['data_url_image_dir'])){$html=dlib_html_data_url_remove($html);}

	$effectiveUrl=$url;
	if(isset($tmpOptions['info']) && isset($tmpOptions['info']['redirect'])){$effectiveUrl=$tmpOptions['info']['redirect'];}
	$html=dlib_html_links_to_urls($html,$effectiveUrl);
	if($html===false){dlib_download_html_error('dlib_html_links_to_urls failed for url: '.$url,$options);}

	if($fname!==false)
	{
		$ok=file_put_contents($fname,$html);
		if($ok===false){dlib_download_html_error('file_put_contents for:'.$fname,$options);return false;}
	}
	
	return $fname===false ? $html : true;
}

function dlib_download_html_error(string $message,array $options)
{
	dlib_log('download_html_error: '.$message,val($options,'log'));
	// If caller wants to get error message log
	if(isset($options['info']))
	{
		$options['info']['log']=(isset($options['info']['log'])  ? $options['info']['log'] : '').
			'download_html_error: '.$message."\n";
	}
}

function dlib_url_is_in_no_wget_sites(string $url)
{
	global $dlib_config;
	if(isset($dlib_config['no_wget_sites']))
	{
		foreach($dlib_config['no_wget_sites'] as $site)
		{
			if(strpos($url,$site)===0)
			{return true;}
		}
	}
	return false;
}
function dlib_url_is_in_no_ssl_cert_sites(string $url)
{
	global $dlib_config;
	if(isset($dlib_config['no_ssl_cert_sites']))
	{
		foreach($dlib_config['no_ssl_cert_sites'] as $site)
		{
			if(strpos($url,$site)===0)
			{return true;}
		}
	}
	return false;
}
//! Download an html page, fix links, convert to utf8 and tidy it.
//! If you only want fixed links, (no convert, no tidy), use dlib_download_html_page().
//! This function can either download to a file or return the download as a string.
//! @param $url: 
//! @param $fname: 
//! @param $options: only 'log' & html5 is used here, see dlib_download_html_page() and dlib_curl_download() 
//! for other opts.
//! @return false on failure
function dlib_download_and_clean_html_page(string $url,$fname=false,array $options=[])
{
	$tmpOptions=$options;
	$tmpOptions['headers']=&$getHeaders;
	$html=dlib_download_html_page($url,false,$tmpOptions);
	$options['headers']=$getHeaders;
	if($html===false){return false;}

	$html=dlib_html_convert_page_to_utf8($html,$getHeaders['content-type']);
	$html=dlib_html_cleanup_page_with_tidy($html,false,val($options,'html5',true));

	if($fname!==false)
	{
		$ok=file_put_contents($fname,$html);
		if($ok===false)
		{
			dlib_download_html_error('_download_and_clean_html_page: file_put_contents failed for:'.
								$fname,$options);
			return false;
		}
	}
	return $html;
}

//! Download a file from a remote location using http.
//! This should be used as the only interface for downloading from the web.
//! This function can either download to a file or return the download as a string.
//! To download html files, don't use this function direcly, use:
//! dlib_download_and_clean_html_page() or dlib_download_html_page().
//! Example usage:
//! $html=dlib_curl_download('http://example.org/',false,array('log'=>'/tmp/log',
//!										  	              'headers'=>&$headers,
//!														  'info'=>&$info));
//! @param $url the url of the file you want to download
//! @param $fname the filename where the result will be saved 
//! (if $filename is false, result will be returned)
//! @param $options: array('log'=>see dlib_log(),
//!                         'headers'=>http response headers will be returned here.
//!                         'info'=>array('log'=>...,'redirect'=>...,'time'=>...,'status'=>...);
//! @return false on failure. Contents or true on success.
function dlib_curl_download(string $url,$fname=false,array $options=[])
{
	$res=false;

	dlib_log("curl_download: ".$url.($fname===false ? '': ' : dest file:'.$fname),
				 val($options,'log'));

	// only for testing (do not really download, just return file)
	if(val($options,'testfile')!==false)
	{
		$options['headers']=
			dlib_parse_http_response_headers(file_get_contents($options['testfile'].'.headers'));
		if($fname){return copy($options['testfile'],$fname);}
		else      {return file_get_contents($options['testfile']);}
	}

	$ch = curl_init();
	if($fname===false){$fh=fopen('php://temp','r+');}
	else              {$fh=fopen($fname      ,'w' );}
	$hh=fopen('php://memory','w');
	$errorArgs=[$fname,&$ch,&$fh,&$hh,$options];

	// fail if there where any errors
	if($ch===false || $fh===false || $hh===false)
	{
		dlib_curl_download_error("init failed",$errorArgs);
		return false;
	}
	// By default add an Accept-Language header using current locale. 
	if(val($options,'setlang',true))
	{
		// Found at least one site that fails without the country code (!)
		if(preg_match('@^([a-z]{2})_([A-Z]{2})\.@',setlocale(LC_TIME,"0"),$m))
		{
			$httpLang=$m[1].'-'.$m[2].','.dlib_get_locale_name().';q=0.5';
		}
		else
		{
			$httpLang=dlib_get_locale_name();
		}
 		$options['httpheader']=array_merge(val($options,'httpheader',[]),['Accept-Language: '.$httpLang]);
	}
	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_HEADER, 0);
	curl_setopt($ch,CURLOPT_FAILONERROR, val($options,'failonerror',true)); 
	curl_setopt($ch,CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch,CURLOPT_MAXREDIRS, 20);
	curl_setopt($ch,CURLOPT_FILE,$fh);
	curl_setopt($ch,CURLOPT_WRITEHEADER,$hh);
	curl_setopt($ch,CURLOPT_ENCODING,'');

	if(isset($options['timeout' ]  )){curl_setopt($ch,CURLOPT_TIMEOUT   ,$options['timeout']);}
	if(isset($options['cookiejar' ])){curl_setopt($ch,CURLOPT_COOKIEJAR ,$options['cookiejar']);
		                              curl_setopt($ch,CURLOPT_COOKIEFILE,$options['cookiejar']);}
	if(isset($options['postfields'])){curl_setopt($ch,CURLOPT_POSTFIELDS,$options['postfields']);}
	if(isset($options['httpheader'])){curl_setopt($ch,CURLOPT_HTTPHEADER,$options['httpheader']);}
	if(isset($options['curl-hook'])){$options['curl-hook']($ch);}

	// disable ssl certificates for sites 
	if(dlib_url_is_in_no_ssl_cert_sites($url))
	{
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0); 
	}

	$userAgent="demosphere.net software";
	if(dlib_url_is_in_no_wget_sites($url)){$userAgent='Mozilla/5.0';}
	curl_setopt($ch, CURLOPT_USERAGENT,$options['user-agent'] ?? $userAgent);

	// ******* actually download
	$curlRet=curl_exec($ch);

	// check if there was a redirection (just log that fact)
	$effectiveUrl=curl_getinfo($ch,CURLINFO_EFFECTIVE_URL);
	if(preg_replace('@(https?://[^/]+)/$@','$1',$effectiveUrl)!==
	   preg_replace('@(https?://[^/]+)/$@','$1',$url))
	{
		dlib_log('curl_download: '.$url.' redirected to: '.$effectiveUrl,val($options,'log'));
		$options['info']['log']=(isset($options['info']['log'])  ? $options['info']['log'] : '').
			'curl_download: redirected to: '.ent($effectiveUrl)."\n";
		$options['info']['redirect']=$effectiveUrl;
	}
	dlib_log('curl_download: completed in '.curl_getinfo($ch,CURLINFO_TOTAL_TIME).'s',
			 val($options,'log'));
	$options['info']['time'  ]=curl_getinfo($ch,CURLINFO_TOTAL_TIME);
	$options['info']['status']=curl_getinfo($ch,CURLINFO_HTTP_CODE);

	// if download failed, return
	if($curlRet===false) 
	{
		if(isset($options['error_callback']))
		{
			$options['error_callback'](curl_errno($ch),curl_error($ch),curl_getinfo($ch,CURLINFO_HTTP_CODE),$ch);
			return false;
		}
		if(val($options,'failonerror',true)===true)
		{
			dlib_curl_download_error("status: ".curl_getinfo($ch,CURLINFO_HTTP_CODE),$errorArgs);
			return false;
		}
	}
	curl_close($ch);
	$ch=false;

	// if no filename, read download from temp (memory) stream 
	fflush($fh);
	if($fname===false)
	{
		rewind($fh);
		$res=stream_get_contents($fh);
		if($res===false)
		{
			dlib_curl_download_error("reading temp stream failed",$errorArgs);
			return false;
		}		
	}
	fclose($fh);
	$ff=false;

	// read headers from temp (memory) stream 
	fflush($hh);
	rewind($hh);
	$rawHeaders=stream_get_contents($hh);
	if($rawHeaders===false)
	{
		dlib_curl_download_error("reading headers temp stream failed",$errorArgs);
		return false;
	}
	fclose($hh);
	$hh=false;

	$options['headers']=dlib_parse_http_response_headers($rawHeaders);

	return $fname===false ? $res : true;
}

//! internal for curl_download
function dlib_curl_download_error(string $message,array $errorArgs)
{
	list($fname,$ch,$fh,$hh,$options)=$errorArgs;
	$options['info']['curl_errno']=curl_errno($ch);
	$curlError=curl_error($ch);
	$msg='curl_download error: '.$message."\n    ".$curlError.'"';
	if($ch!==false){curl_close($ch);}else{$msg.="\ncurl_init failed";}
	if($fh!==false){fclose($fh);    }else{$msg.="\ndestination ".
			($fname===false?"stream" : "file (".$fname.")")." open failed";}
	if($hh!==false){fclose($hh);    }else{$msg.="\nheader stream open failed";}
	if($fname!==false && file_exists($fname) && $fname!=='/dev/null'){unlink($fname);}
	dlib_log($msg,val($options,'log'));
	$options['info']['log']=(isset($options['info']['log'])  ? $options['info']['log'] : '').
		$msg."\n";
}

//! Uses the curl library to get the http header (and just the header) from a url request.
function dlib_curl_get_header(string $url,array $options=[])
{
	$ch = curl_init();
	if($ch===false){return false;}

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, true);
	// FIXME: certain servers don't send redirect when using CURLOPT_NOBODY :-(
	// Example: http://cgteduc93.free.fr/?31-aout-2011-Rassemblement
	// FIXME: certain servers return 404 not found when using CURLOPT_NOBODY, but return correct otherwise (why? what can we do about it?)
	// Example: http://www.cgtparis.fr/IMG/pdf/le_22_05_pour_une_reforme_de_progres.pdf
	curl_setopt($ch, CURLOPT_NOBODY, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	if(isset($options['timeout'])){curl_setopt($ch,CURLOPT_TIMEOUT,$options['timeout']);}

	// disable ssl certificates for sites 
	if(dlib_url_is_in_no_ssl_cert_sites($url))
	{
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0); 
	}

	if(isset($options['curl']))
	{
		foreach($options['curl'] as $o=>$v){curl_setopt($ch,$o,$v);}
	}

	$buf=curl_exec ($ch);
	$options['status']=curl_getinfo($ch,CURLINFO_HTTP_CODE);
	//vd(curl_errno($ch),curl_error($ch));
	//$status=curl_getinfo($ch,CURLINFO_HTTP_CODE);
	curl_close ($ch);
	if($buf===false || strlen($buf)===0){return false;}

	$rawHeaders=$buf;
	return dlib_parse_http_response_headers($rawHeaders);
}

//! parse headers into an array
function dlib_parse_http_response_headers(string $rawHeaders)
{
	$getHeaders=[];
	$lines=explode("\r\n",$rawHeaders);
	foreach($lines as $line)
	{
		if(strlen(trim($line))==0){continue;}
		$parts=explode(': ',$line);
		if(count($parts)==1){$name='response';}
		else{$name=strtolower(array_shift($parts));}
		$getHeaders[$name]=implode(': ',$parts);
	}
	return $getHeaders;
}

function dlib_http_status_code($code=false)
{
	static $status=
		[100=>'Continue',
		 101=>'Switching Protocols',
		 102=>'Processing (WebDAV) (RFC 2518)',
		 122=>'Request-URI too long',
		 200=>'OK',
		 201=>'Created',
		 202=>'Accepted',
		 203=>'Non-Authoritative Information (since HTTP/1.1)',
		 204=>'No Content',
		 205=>'Reset Content',
		 206=>'Partial Content',
		 207=>'Multi-Status (WebDAV) (RFC 4918)',
		 226=>'IM Used (RFC 3229)',
		 300=>'Multiple Choices',
		 301=>'Moved Permanently',
		 302=>'Found',
		 303=>'See Other (since HTTP/1.1)',
		 304=>'Not Modified',
		 305=>'Use Proxy (since HTTP/1.1)',
		 306=>'Switch Proxy',
		 307=>'Temporary Redirect (since HTTP/1.1)',
		 400=>'Bad Request',
		 401=>'Unauthorized',
		 402=>'Payment Required',
		 403=>'Forbidden',
		 404=>'Not Found',
		 405=>'Method Not Allowed',
		 406=>'Not Acceptable',
		 407=>'Proxy Authentication Required[2]',
		 408=>'Request Timeout',
		 409=>'Conflict',
		 410=>'Gone',
		 411=>'Length Required',
		 412=>'Precondition Failed',
		 413=>'Request Entity Too Large',
		 414=>'Request-URI Too Long',
		 415=>'Unsupported Media Type',
		 416=>'Requested Range Not Satisfiable',
		 417=>'Expectation Failed',
		 422=>'Unprocessable Entity (WebDAV) (RFC 4918)',
		 423=>'Locked (WebDAV) (RFC 4918)',
		 424=>'Failed Dependency (WebDAV) (RFC 4918)',
		 425=>'Unordered Collection (RFC 3648)',
		 426=>'Upgrade Required (RFC 2817)',
		 444=>'No Response',
		 449=>'Retry With',
		 450=>'Blocked by Windows Parental Controls',
		 499=>'Client Closed Request',
		 500=>'Internal Server Error',
		 501=>'Not Implemented',
		 502=>'Bad Gateway',
		 503=>'Service Unavailable',
		 504=>'Gateway Timeout',
		 505=>'HTTP Version Not Supported',
		 506=>'Variant Also Negotiates (RFC 2295)',
		 507=>'Insufficient Storage (WebDAV) (RFC 4918)[6]',
		 509=>'Bandwidth Limit Exceeded (Apache bw/limited extension)',
		 510=>'Not Extended (RFC 2774)',];
	if($code===false){return $status;}
	return isset($status[$code]) ? $status[$code] : '(unknown)';
}

?>