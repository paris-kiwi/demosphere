<?php

function translation_backend_add_paths(array &$paths,array &$options)
{
	$paths[]=['path'=>'translation-backend',
			  'roles' => ['admin'],
			  'file'=>'dlib/translation-backend.php',
			  'function' => 'translation_backend',
			 ];

	$paths[]=['path'=>'translation-backend/import-file',
			  'roles' => ['admin'],
			  'file'=>'dlib/translation-backend.php',
			  'function' => 'translation_backend_import_file',
			 ];

	$paths[]=['path'=>'translation-backend/export-file',
			  'roles' => ['admin'],
			  'file'=>'dlib/translation-backend.php',
			  'function' => 'translation_backend_export_file',
			 ];

	$paths[]=['path'=>'translation-backend/export-po',
			  'output'=>false,
			  'roles' => ['admin'],
			  'file'=>'dlib/translation-backend.php',
			  'function' => 'translation_backend_export_po',
			 ];

	$paths[]=['path'=>'@^translation-backend/translation/(\d+)/edit$@',
			  'roles' => ['admin'],
			  'file'=>'dlib/translation-backend.php',
			  'function' => 'translation_backend_translation_edit',
			 ];

	$paths[]=['path'=>'translation-backend/unused-delete',
			  'roles' => ['admin'],
			  'file'=>'dlib/translation-backend.php',
			  'function' => 'translation_backend_unused_delete_form',
			 ];

}

function translation_backend()
{
	global $base_url,$currentPage,$dlib_config;
	$translations=false;
	$search=val($_GET,'search');
	
	$currentPage->addCss('dlib/translation-backend.css');

	$pager=false;
	if($search!==false)
	{
		$pager=new Pager(['defaultItemsPerPage'=>200]);

		// Get for all translations on a page given by a URL
		if(isset($dlib_config['translation_exec_path']) && strpos($search,$base_url.'/')===0)
		{
			$path=substr($search,strlen($base_url.'/'));
			t(null);
			$oldPost=$_POST;
			$_POST=[];
			$oldGet=$_GET;
			$_GET=[];
			$oldCurrentPage=$currentPage;
			$currentPage=new HtmlPageData();
			ob_start();
			$dlib_config['translation_exec_path']($path);
			ob_end_clean();
			$_POST=$oldPost;
			$_GET=$oldGet;
			$currentPage=$oldCurrentPage;
			$cache=t(false);
			$translations=
				db_arrays("SELECT SQL_CALC_FOUND_ROWS * FROM translations WHERE msgid IN ('".
						  implode("','",array_map(function($v){global $db_config;return db_escape_string($v);},
												  array_keys($cache)))."') ".
						  $pager->sql());
			$pager->foundRows();
		}
		else
		// Get for all translations on a page given by a URL
		if($search==='_customized_')
		{
			$translations=
				db_arrays("SELECT SQL_CALC_FOUND_ROWS * FROM translations WHERE msgstr!=translation ".
						  $pager->sql());
			$pager->foundRows();
		}
		else
		if($search==='_untranslated_')
		{
			$translations=
				db_arrays("SELECT SQL_CALC_FOUND_ROWS * FROM translations WHERE msgstr='' ".
						  $pager->sql());
			$pager->foundRows();
		}
		else
		if($search==='_unused_')
		{
			$old=translation_backend_unused_list($dlib_config['translation_files']);
			if(!count($old)){$translations=[];}
			else
			{
				$translations=db_arrays("SELECT SQL_CALC_FOUND_ROWS * FROM translations WHERE ".
										"id IN (".implode(",",array_keys($old)).")".
										$pager->sql());
				$pager->foundRows();
			}
		}
		else
		{
			// Search for translations matching search terms
			$translations=
				db_arrays("SELECT SQL_CALC_FOUND_ROWS * FROM translations WHERE ".
						  "LOWER(CONVERT(msgid       USING utf8mb4)) LIKE '%%%s%%' OR ".
						  "LOWER(CONVERT(msgstr      USING utf8mb4)) LIKE '%%%s%%' OR ".
						  "LOWER(CONVERT(translation USING utf8mb4)) LIKE '%%%s%%' ".
						  $pager->sql(),
						  mb_strtolower($search),
						  mb_strtolower($search),
						  mb_strtolower($search));
			$pager->foundRows();
		}
		foreach($translations as &$translation)
		{
			foreach(['msgstr','msgid','translation'] as $i)
			{
				$t=$translation[$i];
				$t=ent($t);
				if(mb_strlen($search))
				{
					$t=preg_replace('@('.preg_quote(ent($search),'@').')@i','<strong>$1</strong>',$t);
				}
				$translation[$i]=$t;
			}
			if($translation['msgstr']===$translation['translation']){$translation['translation']='<span class="unchanged">'.t('[not customized]').'</span>';}
		}
		unset($translation);
	}

	$language=dlib_get_locale_name();

	require_once 'dlib/template.php';
	return template_render('translation-backend.tpl.php',
						   [compact('translations','pager','language')]);
}

function translation_backend_translation_edit(int $id)
{
	global $currentPage,$base_url;
	$currentPage->addCss('dlib/translation-backend.css');

	$translation=db_array('SELECT * FROM translations WHERE id=%d',$id);
	$items=[];
	$items['title' ]=['html'=>
					  '<p>[<a href="'.$base_url.'/translation-backend">'.t('Translation manager').'</a>]'.
					  '   [<a href="'.$base_url.'/translation-backend/import-file">'.t('Import file').'</a>]'.
					  '   [<a href="'.$base_url.'/translation-backend/export-file">'.t('Export file').'</a>]</p>'.
					  '<h1 class="page-title">'.t('Translate item').'</h1>'
					 ];

	$items['msgid'  ]=['type'=>'textarea',
					   'title'=>t('English source'),
					   'value'=>$translation['msgid'],
					   'attributes'=>['readonly'=>true]];

	$items['msgstr']=['type'=>'textarea',
					  'title'=>t('Reference translation'),
					  'value'=>$translation['msgstr'],
					  'attributes'=>['readonly'=>true]];

	$items['description']=
		['html'=>'<p><strong>'.t('Description').'</strong>: '.ent($translation['description']).'</p>'];

	$items['translation']=['type'=>'textarea',
						   'title'=>t('Current translation'),
						   'default-value'=>$translation['translation']];

	$items['submit']=['type'=>'submit',
					  'value'=>t('Save'),
					  'submit'=>function($items)use($id,$translation)
		{
			$v=$items['translation']['value'];
			// "\r" not allowed in translations
			$v=str_replace(["\r\n","\r"],["\n","\n"],$v);
			db_query("UPDATE translations SET translation='%s' WHERE id=%d",
					 $v,
					 $id);
			dlib_message_add(t('Translation updated successfully.'));
		}
					 ];

	require_once 'dlib/form.php';
	return form_process($items,['id'=>'translation-edit']);
}

function translation_backend_import_file()
{
	global $base_url;
	$items=[];
	$items['title' ]=['html'=>'<p>[<a href="'.$base_url.'/translation-backend">'.t('Translation manager').'</a>]'.
					  '   [<a href="'.$base_url.'/translation-backend/import-file">'.t('Import file').'</a>]'.
					  '   [<a href="'.$base_url.'/translation-backend/export-file">'.t('Export file').'</a>]</p>'.
					  '<h1 class="page-title">'.t('Import translation file').'</h1>'];
	$items['file'  ]=['type'=>'file',
					  'title'=>t('File'),
					  'description'=>t('Please enter a ".po" file. The translations in that file will be added.')];

	$items['override']=['type'=>'radios',
						'title'=>t('Existing translation'),
						'options'=>['erase'=>t('Completely erase all existing translations before importing the file (all customizations will be lost, translations not from this file will be erased)'),
									'override'=>t('Translations from file override existing translations (customizations of translations in this file will be lost)'),
									'not-custom'=>t('Translations from file override only non-customized translations'),
									'only-new'=>t('Translations from file are only applied if they do not exist yet (only newly translated strings are added)'),],
						'default-value'=>'not-custom',
						'description'=>t('If you are not sure, use the third option. It is safe.'),
					   ];

	$items['import']=['type'=>'submit',
					  'value'=>t('Import'),
					  'submit'=>function($items)
		{
			$values=dlib_array_column($items,'value');
			if($values['override']==='erase')
			{
				db_query('TRUNCATE TABLE translations');
			}
			$msg=translation_po_file_import($_FILES['file']['tmp_name'],
											$values['override']!=='erase' &&
											$values['override']!=='override',
											$values['override']==='only-new',
											$_FILES['file']['name']);
			dlib_message_add(t('Import translation file results:').' '.$msg);
		}
					 ];

	require_once 'dlib/form.php';
	return form_process($items,['id'=>'translation-edit']);
}

function translation_backend_unused_list(array $allFiles)
{
	$good=[];
	foreach($allFiles as $fname)
	{
		$tmp=translation_po_file_read($fname);
		$good=array_merge($good,array_keys($tmp));
	}
	$existing=db_one_col('SELECT id,msgid FROM translations');
	$res=array_diff($existing,$good);
	return $res;
}

function translation_backend_unused_delete_form()
{
	global $dlib_config;
	$items=[];
	$old=translation_backend_unused_list($dlib_config['translation_files']);
	if(!count($old)){return 'no unused translations';}
	$items['msg']=['html'=>t('Are you sure you want to delete all @nb unused translations?',['@nb'=>count($old)])];
	$items['ok']=['type'=>'submit',
				  'value'=>t('Delete'),
				  'submit'=>function()
			{
				$ct=translation_backend_unused_delete();
				dlib_message_add($ct.' translations deleted');
			},
				  'redirect'=>'translation-backend?search=_unused_',
				  ];

	require_once 'dlib/form.php';
	return form_process($items,['id'=>'translation-edit']);
	
}

function translation_backend_unused_delete()
{
	global $dlib_config;
	$old=translation_backend_unused_list($dlib_config['translation_files']);	
	if(count($old)===0){return 0;}
	db_query("DELETE FROM translations WHERE id IN (".implode(",",array_keys($old)).")");
	return count($old);
}

function translation_install()
{
	db_query("DROP TABLE IF EXISTS translations");
	// we unfortunately need surogate autoincremented key, 
	// since mysql text key length is limited and produces duplicate keys :-(
	// FIXME: forgot reason why msgid, msgstr and translation are "blob" and not "text"
	// Is it really necessary? Performance? Collation?
	db_query("CREATE TABLE translations (
  `id` int(11) NOT NULL auto_increment,
  `msgid` blob NOT NULL,
  `msgstr` blob NOT NULL,
  `translation` blob NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `msgid` (`msgid`(255))
) DEFAULT CHARSET=utf8mb4");

}

function translation_po_file_import(string $filename,bool $skipCustomized=true,bool $onlyNew=false,$description=false)
{
	$strings=translation_po_file_read($filename);
	if($strings===false){fatal('translation file load failed');}
	if($description===false){$description=basename($filename);}
	$new=0;
	$skipped=0;
	$changed=0;
	$unchanged=0;
	foreach($strings as $msgid=>$msgstr)
	{
		if($msgid===''){continue;}
		$existing=db_array("SELECT * FROM translations WHERE msgid='%s'",$msgid);
		if($existing!==null && (($existing['translation']!==$existing['msgstr'] && $skipCustomized) || $onlyNew))
		{
			$skipped++;
			// we update msgstr (reference translation) and description even if we don't update translation
			db_query("UPDATE translations SET msgstr='%s',description='%s' WHERE msgid='%s'",$msgstr,$description,$msgid);
		}
		else
		{
			if($existing!==null)
			{
				if($existing['translation']===$msgstr){$unchanged++;}else{$changed++;}
				db_query("UPDATE translations SET msgstr='%s',translation='%s',description='%s' WHERE id=%d",
						 $msgstr,$msgstr,$description,$existing['id']);
			}
			else
			{
				$new++;
				db_query("INSERT INTO translations (msgid,msgstr,translation,description) VALUES ('%s','%s','%s','%s')",
						 $msgid,$msgstr,$msgstr,$description);
			}
		}
	}
	return "new: $new -- changed: $changed -- not changed (identical): $unchanged -- skipped: $skipped\n";
}

/**
 * Copied from Drupal. Parses Gettext Portable Object file into an array
 *
 * @param $op
 *   Storage operation type: db-store or mem-store
 * @param $file
 *   filename
 * @param $mode
 *   Should existing translations be replaced LOCALE_IMPORT_KEEP or LOCALE_IMPORT_OVERWRITE
 * @param $lang
 *   Language code
 * @param $group
 *   Text group to import PO file into (eg. 'default' for interface translations)
 */
function translation_po_file_read(string $file, $mode = NULL, $lang = NULL, $group = 'default')
{
	$op='mem-store';
	$fd = fopen($file, "rb"); // File will get closed by PHP on return
	if (!$fd)
	{
		translation_import_message('The translation import failed, because the file %filename could not be read.', $file);
		return FALSE;
	}

	$context = "COMMENT"; // Parser context: COMMENT, MSGID, MSGID_PLURAL, MSGSTR and MSGSTR_ARR
	$current = [];   // Current entry being read
	$plural = 0;          // Current plural form
	$lineno = 0;          // Current line

	while (!feof($fd))
	{
		$line = fgets($fd, 10*1024); // A line should not be this long
		if ($lineno == 0)
		{
			// The first line might come with a UTF-8 BOM, which should be removed.
			$line = str_replace("\xEF\xBB\xBF", '', $line);
		}
		$lineno++;
		$line = trim(strtr($line, ["\\\n" => ""]));

		if (!strncmp("#", $line, 1))
		{   // A comment
			if ($context == "COMMENT")
			{   // Already in comment context: add
				$current["#"][] = substr($line, 1);
			}
			else
			if (($context == "MSGSTR") || ($context == "MSGSTR_ARR"))
			{   // End current entry, start a new one
				translation_import_one_string($op, $current, $mode, $lang, $file, $group);
				$current = [];
				$current["#"][] = substr($line, 1);
				$context = "COMMENT";
			}
			else
			{   // Parse error
				translation_import_message('The translation file %filename contains an error: "msgstr" was expected but not found on line %line.', $file, $lineno);
				return FALSE;
			}
		}
		elseif (!strncmp("msgid_plural", $line, 12))
		{
			if ($context != "MSGID") { // Must be plural form for current entry
				translation_import_message('The translation file %filename contains an error: "msgid_plural" was expected but not found on line %line.', $file, $lineno);
				return FALSE;
			}
			$line = trim(substr($line, 12));
			$quoted = translation_import_parse_quoted($line);
			if ($quoted === FALSE)
			{
				translation_import_message('The translation file %filename contains a syntax error on line %line.', $file, $lineno);
				return FALSE;
			}
			$current["msgid"] = $current["msgid"] ."\0". $quoted;
			$context = "MSGID_PLURAL";
		}
		elseif (!strncmp("msgid", $line, 5))
		{
			if (($context == "MSGSTR") || ($context == "MSGSTR_ARR")) { // End current entry, start a new one
				translation_import_one_string($op, $current, $mode, $lang, $file, $group);
				$current = [];
			}
			elseif ($context == "MSGID") { // Already in this context? Parse error
				translation_import_message('The translation file %filename contains an error: "msgid" is unexpected on line %line.', $file, $lineno);
				return FALSE;
			}
			$line = trim(substr($line, 5));
			$quoted = translation_import_parse_quoted($line);
			if ($quoted === FALSE)
			{
				translation_import_message('The translation file %filename contains a syntax error on line %line.', $file, $lineno);
				return FALSE;
			}
			$current["msgid"] = $quoted;
			$context = "MSGID";
		}
		elseif (!strncmp("msgstr[", $line, 7))
		{
			if (($context != "MSGID") && ($context != "MSGID_PLURAL") && ($context != "MSGSTR_ARR")) { // Must come after msgid, msgid_plural, or msgstr[]
				translation_import_message('The translation file %filename contains an error: "msgstr[]" is unexpected on line %line.', $file, $lineno);
				return FALSE;
			}
			if (strpos($line, "]") === FALSE)
			{
				translation_import_message('The translation file %filename contains a syntax error on line %line.', $file, $lineno);
				return FALSE;
			}
			$frombracket = strstr($line, "[");
			$plural = substr($frombracket, 1, strpos($frombracket, "]") - 1);
			$line = trim(strstr($line, " "));
			$quoted = translation_import_parse_quoted($line);
			if ($quoted === FALSE)
			{
				translation_import_message('The translation file %filename contains a syntax error on line %line.', $file, $lineno);
				return FALSE;
			}
			$current["msgstr"][$plural] = $quoted;
			$context = "MSGSTR_ARR";
		}
		elseif (!strncmp("msgstr", $line, 6))
		{
			if ($context != "MSGID") {   // Should come just after a msgid block
				translation_import_message('The translation file %filename contains an error: "msgstr" is unexpected on line %line.', $file, $lineno);
				return FALSE;
			}
			$line = trim(substr($line, 6));
			$quoted = translation_import_parse_quoted($line);
			if ($quoted === FALSE)
			{
				translation_import_message('The translation file %filename contains a syntax error on line %line.', $file, $lineno);
				return FALSE;
			}
			$current["msgstr"] = $quoted;
			$context = "MSGSTR";
		}
		elseif ($line != "")
		{
			$quoted = translation_import_parse_quoted($line);
			if ($quoted === FALSE)
			{
				translation_import_message('The translation file %filename contains a syntax error on line %line.', $file, $lineno);
				return FALSE;
			}
			if (($context == "MSGID") || ($context == "MSGID_PLURAL"))
			{
				$current["msgid"] .= $quoted;
			}
			elseif ($context == "MSGSTR")
			{
				$current["msgstr"] .= $quoted;
			}
			elseif ($context == "MSGSTR_ARR")
			{
				$current["msgstr"][$plural] .= $quoted;
			}
			else
			{
				translation_import_message('The translation file %filename contains an error: there is an unexpected string on line %line.', $file, $lineno);
				return FALSE;
			}
		}
	}

	// End of PO file, flush last entry
	if (($context == "MSGSTR") || ($context == "MSGSTR_ARR"))
	{
		translation_import_one_string($op, $current, $mode, $lang, $file, $group);
	}
	elseif ($context != "COMMENT")
	{
		translation_import_message('The translation file %filename ended unexpectedly at line %line.', $file, $lineno);
		return FALSE;
	}
	return translation_import_one_string('mem-report');
}
/**
 * Sets an error message occurred during locale file parsing.
 *
 * @param $message
 *   The message to be translated
 * @param $file
 *   Drupal file object corresponding to the PO file to import
 * @param $lineno
 *   An optional line number argument
 */
function translation_import_message(string $message, string $file, $lineno = NULL)
{
	$vars = ['%filename' => $file];
	if (isset($lineno))
	{
		$vars['%line'] = $lineno;
	}
	//$t = get_t();
	echo "ERROR:$message\n";
}

/**
 * Imports a string into the database
 *
 * @param $op
 *   Operation to perform: 'db-store', 'db-report', 'mem-store' or 'mem-report'
 * @param $value
 *   Details of the string stored
 * @param $mode
 *   Should existing translations be replaced LOCALE_IMPORT_KEEP or LOCALE_IMPORT_OVERWRITE
 * @param $lang
 *   Language to store the string in
 * @param $file
 *   Object representation of file being imported, only required when op is 'db-store'
 * @param $group
 *   Text group to import PO file into (eg. 'default' for interface translations)
 */
function translation_import_one_string(string $op, $value = NULL, $mode = NULL, $lang = NULL, $file = NULL, $group = 'default')
{
	static $report = ['additions' => 0, 'updates' => 0, 'deletes' => 0, 'skips' => 0];
	static $headerdone = FALSE;
	static $strings = [];

	// Modif Marcel : set empty value for fuzzy translations
	if(isset($value['#']) && count(preg_grep('@^,\s+fuzzy@',$value['#'])))
	{
		$value['msgstr']='';
	}


	switch ($op)
	{
		// Return stored strings
    case 'mem-report':
		$tmp=$strings;
		$report = ['additions' => 0, 'updates' => 0, 'deletes' => 0, 'skips' => 0];
		$headerdone = FALSE;
		$strings = [];
		return $tmp;

		// Store string in memory (only supports single strings)
    case 'mem-store':
		$strings[$value['msgid']] = $value['msgstr'];
		return;

	}
}


/**
 * Parses a string in quotes
 *
 * @param $string
 *   A string specified with enclosing quotes
 * @return
 *   The string parsed from inside the quotes
 */
function translation_import_parse_quoted(string $string)
{
	if (substr($string, 0, 1) != substr($string, -1, 1))
	{
		return FALSE;   // Start and end quotes must be the same
	}
	$quote = substr($string, 0, 1);
	$string = substr($string, 1, -1);
	if ($quote == '"') {        // Double quotes: strip slashes
		return stripcslashes($string);
	}
	elseif ($quote == "'") {  // Simple quote: return as-is
		return $string;
	}
	else
	{
		return FALSE;             // Unrecognized quote
	}
}

//! Small gui to choose which file to export
function translation_backend_export_file()
{
	global $base_url;
	$files=db_one_col('SELECT description FROM translations GROUP BY description ORDER BY COUNT(*) DESC LIMIT 10');
	$out='';
	$out.='<h2>Choose what file you want</h2>';
	$out.='<ul>';
	$out.='<li><a href="'.$base_url.'/translation-backend/export-po">All translations in a single file</a>';
	foreach($files as $file)
	{
		$out.='<li><a href="'.$base_url.'/translation-backend/export-po?file='.urlencode($file).'">'.ent($file).'</a>';
	}
	$out.='</ul>';
	return $out;
}

//! Inspired by Drupal 7 _locale_export_po_generate
function translation_backend_export_po()
{
	global $user;

	$locale=setlocale(LC_TIME,"0");
	$language=dlib_get_locale_name();

	$file=val($_GET,'file');

	if($file===false){$translations=db_one_col('SELECT msgid,translation FROM translations');}
	else 
	{
		$translations=db_one_col("SELECT msgid,translation FROM translations WHERE description='%s'",$file);
	}

	$output=translation_backend_export_po_file($translations,$locale);

	// Download the file for the client.
	if($file===false){$filename='exported.'.$language.'.po';}
	else
	{
		$filename=preg_replace('@[^a-zA-Z_.0-9-]@','_',$file);
	}
	header("Content-Disposition: attachment; filename=$filename");
	header("Content-Type: text/plain; charset=utf-8");
	print $output;
}

function translation_backend_export_po_file(array $translations,string $locale)
{
	$output='';
	$output.="msgid \"\"\n";
	$output.="msgstr \"\"\n";
	$output.="\"Project-Id-Version: demosphere\\n\"\n";
	$output.="\"POT-Creation-Date: " . date("Y-m-d H:iO") . "\\n\"\n";
	$output.="\"PO-Revision-Date: " . date("Y-m-d H:iO") . "\\n\"\n";
	$output.="\"Last-Translator: NAME <EMAIL@ADDRESS>\\n\"\n";
	$output.="\"Language-Team: LANGUAGE <EMAIL@ADDRESS>\\n\"\n";
	$output.="\"Language: ".$locale."\\n\"\n";
	$output.="\"MIME-Version: 1.0\\n\"\n";
	$output.="\"Content-Type: text/plain; charset=utf-8\\n\"\n";
	$output.="\"Content-Transfer-Encoding: 8bit\\n\"\n";
	$output.="\n";


	foreach ($translations as $msgid => $translation)
	{
		$output.='msgid ' .translation_backend_export_po_string($msgid);
		$output.='msgstr '.translation_backend_export_po_string($translation);
		$output.="\n";
	}	
	return $output;
}

//! po file: Print out a string on multiple lines
function translation_backend_export_po_string(string $str)
{
	$stri = addcslashes($str, "\0..\37\\\"");
	$parts = [];

	// Cut text into several lines
	while ($stri != "")
	{
		$i = strpos($stri, "\\n");
		if($i === FALSE)
		{
			$curstr = $stri;
			$stri = "";
		}
		else
		{
			$curstr = substr($stri, 0, $i + 2);
			$stri = substr($stri, $i + 2);
		}
		$curparts = explode("\n", translation_backend_export_po_wrap($curstr, 70));
		$parts = array_merge($parts, $curparts);
	}

	// Multiline string
	if(count($parts) > 1)
	{
		return "\"\"\n\"" . implode("\"\n\"", $parts) . "\"\n";
	}
	// Single line string
	else
	if(count($parts) == 1)
	{
		return "\"$parts[0]\"\n";
	}
	// No translation
	else
	{
		return "\"\"\n";
	}
}

//! po file: word wrapping
function translation_backend_export_po_wrap(string $str, int $len)
{
	$words = explode(' ', $str);
	$return = [];

	$cur = "";
	$nstr = 1;
	while (count($words))
	{
		$word = array_shift($words);
		if ($nstr)
		{
			$cur = $word;
			$nstr = 0;
		}
		elseif (strlen("$cur $word") > $len)
		{
			$return[] = $cur . " ";
			$cur = $word;
		}
		else
		{
			$cur = "$cur $word";
		}
	}
	$return[] = $cur;
	return implode("\n", $return);
}

?>