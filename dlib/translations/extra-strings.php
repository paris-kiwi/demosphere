<?php

// ******* translation-backend.tpl.php ******************* 
t('Translation manager');//translation-backend.tpl.php
t('Import file');//translation-backend.tpl.php
t('Export file');//translation-backend.tpl.php
t('Translation manager');//translation-backend.tpl.php
t('Most of the text displayed on this web site is translated. The original text is written in english inside the source code of this program. Since there are many pieces of text to translate, they are cannot all be displayed here. Please use the search form to find the text you want to change.');//translation-backend.tpl.php
t('Search');//translation-backend.tpl.php
t('English source');//translation-backend.tpl.php
t('Reference translation');//translation-backend.tpl.php
t('Current translation');//translation-backend.tpl.php

// ******* hdiff.tpl.php ******************* 
t('Ignore case');//hdiff.tpl.php
t('Save');//hdiff.tpl.php
t('Click text to edit');//hdiff.tpl.php

// ******* comments.tpl.php ******************* 
t('Manager');//comments.tpl.php
t('Enabled');//comments.tpl.php
t('Pre-moderation');//comments.tpl.php
t('Read-only');//comments.tpl.php
t('Disabled');//comments.tpl.php
t('Add comment');//comments.tpl.php
t('Extra information and comments added by site visitors');//comments.tpl.php
t('Be the first to comment');//comments.tpl.php

// ******* comments-notifications-manager.tpl.php ******************* 
t('My notifications');//comments-notifications-manager.tpl.php
t('Notifications for:');//comments-notifications-manager.tpl.php
t('Receive notifications on your desktop or mobile browser when a non-admin visitor posts a comment.');//comments-notifications-manager.tpl.php
t('Already subscribed on this browser');//comments-notifications-manager.tpl.php
t('Current subscriptions:');//comments-notifications-manager.tpl.php
t('Registered at:');//comments-notifications-manager.tpl.php
t('Last sent:');//comments-notifications-manager.tpl.php
t('Night pause:');//comments-notifications-manager.tpl.php
t('Unsubscribe');//comments-notifications-manager.tpl.php
t('No subscriptions.');//comments-notifications-manager.tpl.php

// ******* comments-manager.tpl.php ******************* 
t('Comments');//comments-manager.tpl.php
t('Get notifications when new comments are posted');//comments-manager.tpl.php
t('Other IPs:');//comments-manager.tpl.php
t('Anonymous user first seen:');//comments-manager.tpl.php
t('start');//comments-manager.tpl.php
t('end');//comments-manager.tpl.php
t('action');//comments-manager.tpl.php
t('label');//comments-manager.tpl.php
t('labelValue');//comments-manager.tpl.php
t('replies to');//comments-manager.tpl.php

// ******* comment.tpl.php ******************* 
t('See more');//comment.tpl.php
t('Accept');//comment.tpl.php
t('Reject');//comment.tpl.php
t('Unban');//comment.tpl.php
t('Ban');//comment.tpl.php
t('Reply');//comment.tpl.php
t('Edit');//comment.tpl.php

// ******* table-render.tpl.php ******************* 
t('search');//table-render.tpl.php
t('No items');//table-render.tpl.php
?>
