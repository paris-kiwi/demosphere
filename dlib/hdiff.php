<?php

// Display side-by-side, optionally editable, diff of two html fragments.
// Warning: by default html is XSS sanitized and simplified. This might not be ok for your html.
// $cols: only html is required, all others are optional
// * html : the html you want to diff
// * save : the url that will receive the Ajax POST request when user hits save button.
//          If this option is not set, then this column is considered non-editable.
// * title : an html title
// * info : html displayed under the title
// * sanitize : true/false : whether the html should sanitized for XSS (default: true)
// * class : an extra html class to add to the column. Usefull for custom CSS.
function hdiff($cols)
{
	global $currentPage;

	$currentPage->addCss('dlib/hdiff.css');
	$currentPage->addJs ('lib/jquery.js');

	if(val($cols[0],'save')!==false || 
	   val($cols[1],'save')!==false    )
	{
		$currentPage->addJs ('lib/tinymce/js/tinymce/tinymce.min.js','file',['cache-checksum'=>false]);
		//$currentPage->addJs ('lib/tinymce-dev/js/tinymce/tinymce.js','file',['cache-checksum'=>false]);
	}
	$currentPage->addJs('dlib/merge-calls.js');
	$currentPage->addJs('dlib/hdiff.js');

	foreach($cols as &$col)
	{
		if(val($col,'sanitize',true))
		{
			require_once 'filter-xss.php';
			$col['html']=filter_xss_admin(dlib_html_cleanup_tidy_xsl($col['html']));
		}
	}
	return template_render('hdiff.tpl.php',
						   [compact('cols')]);
}

?>