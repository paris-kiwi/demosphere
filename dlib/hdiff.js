// namespace
(function() {
window.hdiff = window.hdiff || {};
var ns=window.hdiff;// shortcut

var currentScriptUrl=document.currentScript.src;
var hdiffs=[];

$(document).ready(function()
{
	$('.hdiff-main').each(function()
	{
		hdiffs.push(new HDiff(this,hdiffs.length));
	});
});


//! HDiff constructor. 
//! Multiple hdiffs can be created on a single page.
//! This enclosure creates shared variables for each diff view.
//! The geometry of the different pieces is complex and is documented in hdiff.svg
function HDiff(mainDiv,nb)
{
	mainDiv=$(mainDiv);

	// Number to differentiate each different hdiff on this page.
	var hdiffNb=nb;

	// Only the last hdiff on same page is allowed to scroll. 
	// Others are limited.
	var noScrollHdiff=hdiffNb!==$('.hdiff-main').length-1;
	
	// A list of words for each column.
	// Computed in build_words_ref()
	// Each word has the following properties:
	// node:  the DOM node where this word is (example: a <p>)
	// start: Index of the first letter of the word inside node
	// end:   Index of the last  letter of the word inside node
	// word:  A string with the actual text of this word
	// match: The index of the matching word in the other column of wordsRef; false if no match
	// rect:  {left,top,right,bottom,width,height,middleX,middleY} coords relative to .html-diff-[01]
	// lineNum: index of the line that contains this word in the appropriate column of the "lines" array
	// wordNum: index in this array (for simpler reference)
	var wordsRef=[[],[]];

	// Words are divided into lines, for a cleaner, line oriented display
	// Computed in compute_lines()
	// start:   Index of first word of this line inside the wordsRef array.
	// end:     Index of last  word of this line inside the wordsRef array.
	// top:     y coordinates of top of line (relative to .html-diff-[01]). It is enlarged to make lines adjacent.
	// bottom:  see top.
	// isMatch: whether this line is mostly match (=green) or not (=red).
	// isAllMatches: whether all of the words in this line are matches
	// firstMatch: number of first word in this line that has a match on the other col
	// lastMatch:  number of last word in this line that has a match on the other col
	var lines=[[],[]];

	// List of words on each that have matches and that are spaced by at least dy=7px
	// yPosMatches[col][?]={wordNum: word on this col that has a match on other col
	//                      y:middleY of that word}
	// Built in compute_y_position_matches() and used by matching_position()
	var yPosMatches=null;
	
	// Description of words that are currently around the caret.
	// The hlCursor is determined from the current "selection" 
	// Note: the browser sets the selection when you click anywhere, even in a non-editable col.
	// hlCursor is set in cursor_highlight() and used in draw_canvas()
	// {
	//   wordNums:  [col][0-2] : on each col : word before, on and after the cursor. Each one may be false.
	//   diff0: false,0,1 : false means "no diff in cursor"; 0=> diff starts at   wordNums[col][0] ; 1=> diff starts at   wordNums[col][1]
	//   diff1: false,1,2 : ...                            ; 1=> diff ends before wordNums[col][1] ; 2=> diff ends before wordNums[col][2]
    // }
	var hlCursor=null;

	// Tinymce editors for each col. Currently not used.
	var editors=[false,false];

	// List of text nodes in each column.
	// Used to check for changes. This might not be absolutelly necessary now that we use MutationObserver. 
	var lastNodes=[[],[]];

	// List of strings inside each lastNodes
	var lastText =[[],[]];

	// this allows the interest pos to move away from the center of page, when user has already scrolled all the way to the top of page
	// The y position of the interestPos is the middle of the viewport (relative to main)  + interestPosExcursion 
	// So interestPos <0 means the interestPos moves up (which is -currently- always the case)
	// Use interest_pos_excursion_set() which imposes bounds
	var interestPosExcursion=0;

	// Keep previous scrollTop so that we force it back to block scrolling.
	var scrollTop=$(window).scrollTop();

	// When user is editing right col, don't scroll it. This unblocks as soon as the user uses the mouse wheel or unfocuses.
	// values : true, false, 'needs-resize' 
	var blockRightScroll=false;

	// Time (ms) at which scroll was disabled. 
	// This is set when user scrolls in right column. 
	// Main window scroll event handler is disabled during 500ms. Otherwise it would itself move the right col, leading to unpredictable movements.
	var tmpDisableScrollRightSync=false;

	// Thread for running slow longest_common_subsequence() so that user can edit without being blocked.
	// A single thread is created and reused for each LCS call. 
	// If a new LCS is needed while the other is running, it i s killed and a new Worker is started. 
	// This is wasteful. If it is a problem, we should try to interrupt the LCS algo with a message (instead of killing).
	var webWorker=false;
	// If this data-hdiff-web-worker-url is not defined, the url is guessed from this script's url.
	var webWorkerUrl=mainDiv.attr('data-hdiff-web-worker-url');

	// While LCS is being computed, all nodes, positions, etc. are wrong. Most operations (resize, draw_canvas,...) should be blocked.
	var webWorkerIsComputing=false;

	// For performance only (avoid jQuery searches). Specially for matching_position()
	var htmlInner=[mainDiv.find('.hdiff-html-inner-0'),
				   mainDiv.find('.hdiff-html-inner-1')];

	// Hack: when copy button is pressed to copy diff from one col to another, the editor in the other col might not
	// be initialized yet. So we need to initialize it and recall the copy function when init is done.
	var pendingCopyButtonCall=false;

	// When there are multiple hdiffs on the same page, this is used to make sure hlCursor is set in only one of them.
	this.unset_hl_cursor=function()
	{
		hlCursor=null;
		mainDiv.find('.copy-button').hide();
		if(!webWorkerIsComputing){draw_canvas();}
	};

	// ******** actually launch setup
	initial_setup();


// *************************
//! Called only once on creation
function initial_setup()
{
	mainDiv.addClass("hdiff-main-"+hdiffNb);

	// Guess web worker url from current script url (if it has not been set as data-hdiff-web-worker-url on mainDiv) 
	if(typeof webWorkerUrl==='undefined')
	{
		webWorkerUrl=currentScriptUrl.replace(/hdiff[^\/]*\.js$/,'hdiff-worker.js');
	}

	// *** both col-top's need to have the same size
	var colTops=mainDiv.find('.hdiff-col-top');
	colTops.height(Math.max(colTops.eq(0).height(),
							colTops.eq(1).height()));

	// If ignore case clicked, rebuild matches and update display
	mainDiv.find(".ignore-case").change(function()
										{
											rebuild_all_if_text_has_changed(true);
										});

	// *** tinymce
	for(var col=0;col<2;col++)
	{
		// Column is not editable: no editor.
		if(!mainDiv.find(".hdiff-col-"+col).hasClass('editable'))
		{
			editors[col]=false;
			// Show cursor when user clicks or selects. 
			htmlInner[col].on('mousedown mouseup',function(){cursor_highlight();});
			continue;
		}

		// Create copy arrows
		mainDiv.prepend('<div class="copy-button copy-button-'+(1-col)+'" ></div>');
		mainDiv.find('.copy-button-'+(1-col)).mousedown(copy_button_mousedown);

		// only create tinymce on demand (click) to avoid slowing down page load
		htmlInner[col].one('mousedown',function(xcol,mdownEvent)
		{
			//console.log('First mousedown on editable',hdiffNb,xcol);
			if(xcol===1){blockRightScroll=true;;}
			var tinymceConfig=
			{
				selector: ".hdiff-main-"+hdiffNb+" .hdiff-html-inner-"+xcol,
				inline: true,
				// Tinymce toolbar position is broken in right column when it scrolls. So we position it ourselves in a div.
				fixed_toolbar_container: ".hdiff-main-"+hdiffNb+" .hdiff-col-"+xcol+" .hdiff-tinymce-toolbar",
				toolbar: "undo redo",
				menubar: false,
				setup:  function(ed){editor_setup(ed,xcol,mdownEvent.clientX,mdownEvent.clientY);}
			};

			// Very special case : user types in editor before it is init.
			// supress key so that text is not inserted at begining of doc.
			$(tinymceConfig.selector).on('keypress.initSuppressKey',function(e){e.preventDefault();});

			if(typeof ns.tinymce_config_hook!=='undefined'){ns.tinymce_config_hook(tinymceConfig,mainDiv,xcol);}
			tinymce.init(tinymceConfig);
		}.bind(this,col));
	}

	// *** 

	// Initial build.
	// For some reason (we need to figure this out) sizes (right inner html height) changes after first display, so delay this a bit
	setTimeout(function()
	{
		rebuild_all_if_text_has_changed(true,function()
		{
			// We need to install scroll event handler here to ensure that resize() is called before.
			$(window).scroll(window_scroll);
			$(window).resize(function(){resize();draw_canvas();});
			$(window).on('wheel',mouse_wheel);

			// initial position of col1 
			window_scroll();
		});
	},100);

	// *** event handlers for save button and page leave confirm
	mainDiv.find('.hdiff-tinymce-toolbar .save').click(function(e)
	{
		save($(e.target).parents('.hdiff-col-0').length ? 0:1,e);
	});
	window.addEventListener("beforeunload", function (e) 
    {
		if(mainDiv.find('.hdiff-col.needs-save').length==0){return;}
		// FIXME: translate
		var confirmationMessage = "There are unsaved changes. Are you sure you want to leave ?";
		(e || window.event).returnValue = confirmationMessage;     // Gecko and Trident
		return confirmationMessage;                                // Gecko and WebKit
	});
}

//! Called through Tinymce "setup" hook when editor is first created.
function editor_setup(ed,col,mouseX,mouseY)
{
	//console.log('edsetup',hdiffNb,col);
	lastNodes=[[],[]]; // Avoid "needs save" set in rebuild_all_if_text_has_changed()

	ed.on('init', function() 
	{
		//console.log('hdiff tinymce oninit',hdiffNb,col);
		editors[col]=ed;
		$(ed.settings.selector).off('keypress.initSuppressKey');
		if(pendingCopyButtonCall===false)
		{
			ed.fire('focus');
			var pos=find_cursor_from_position(htmlInner[col][0],mouseX,mouseY);
			//console.log('find_cursor_from_position:',pos);
			if(pos!==null){ed.selection.setCursorLocation(pos.node,pos.offset);}
		}
		else
		{
			var srcCol=pendingCopyButtonCall;
			pendingCopyButtonCall=false;
			mainDiv.find('.copy-button-'+srcCol).mousedown();
		}
	});

	ed.on('focus', function(e) 
	{
		mainDiv.find('.hdiff-col-'+col).addClass('focused');
		if(col===1){blockRightScroll=true;}
	});
	ed.on('blur', function(e) 
    {
		mainDiv.find('.hdiff-col-'+col).removeClass('focused');
		if(col===1)
		{
			if(blockRightScroll==='needs-resize'){resize();}
			blockRightScroll=false;
		}
	});

	// Reposition hlCursor when cursor moves
	// TinyMce doc says: Fires when selection inside the editor is changed.
	ed.on('NodeChange', function(e) 
	{
		//console.log('tinymce NodeChange');
		merge_async_calls('NodeChange','call',100,false,function()
		{
			cursor_highlight();
			merge_async_calls('NodeChange','end');
		});
	});

	// Pasted text may be un-normalized (several consecutive Text Nodes in same parent). 
	// This breaks word detection => renormalize.
	ed.on('paste', function(e) 
	{
		// paste event is received before content is inserted into DOM.
		// This is a hackish workaround
		setTimeout(function()
		{
			mainDiv.find(".hdiff-html-"+col)[0].normalize();
		},0);
	});

	// Setup DOM change handler. 
	// This is simpler and more reliable than having to deal with different Tinymce events (init, KeyUp, NodeChange, change)
	var observer = new MutationObserver(function(mutationList)
										{
											//console.log('Mutation observed',hdiffNb);
											rebuild_all_if_text_has_changed();
										});
	observer.observe(mainDiv.find('.hdiff-html-inner-'+col)[0],
		 			 {characterData: true, childList: true,subtree: true});
}

//! Called when changes to text are suspected, mostly by MutationObserver
//! It checks for actual changes by using lastText and lastNode
//! If text or nodes have really changed everything (wordsRef, lines, yPosMatches...) is rebuilt.
//! This function can be async: for big texts, slow LCS comutation is offloaded to a WebWorker...
//! so this function may return before every thiing is computed.
function rebuild_all_if_text_has_changed(forceRebuild,callAfter)
{
	//console.log('rebuild_all_if_text_has_changed');

	if(typeof forceRebuild==='undefined'){forceRebuild=false;}
	// stack of function to call once we have really finished rebuilding
	if(typeof rebuild_all_if_text_has_changed.callAfter==='undefined'){rebuild_all_if_text_has_changed.callAfter=[];}
	if(typeof callAfter!=='undefined'){rebuild_all_if_text_has_changed.callAfter.push(callAfter);}

	var start=Date.now();

	// Check for changes even if "forceRebuild", because we need to set needs-save
	// This should stay fast (< 10 ms) as it is called at every key press !
	var changeFound=false;
	var start=Date.now();
	for(var col=0;col<2;col++)
	{
		var walk=document.createTreeWalker(mainDiv.find(".hdiff-html-"+col)[0],NodeFilter.SHOW_TEXT);
		var textNode;
		var ct=0;
		var colChanged=false;
		var isFirst=lastNodes[col].length===0;
		while(textNode=walk.nextNode())
		{
			var hasNonTextChanges=colChanged || ct>=lastNodes[col].length || lastNodes[col][ct]!==textNode;
			var hasTextChange=false;
			// Check for a text-only change
			if(!hasNonTextChanges && lastText[col][ct]!==textNode.nodeValue)
			{
				// Special case: Compare without BOM chars that TinyMCE spuriously adds near <a> links (for example during cursor move :-( )
				// BOM addition might change offsets in words... reset word
				if(lastText[col][ct].replace('\ufeff','')===textNode.nodeValue.replace('\ufeff',''))
				{
					update_wordrefs_with_bom_change(textNode,lastText[col][ct]);
					lastText[col][ct]=textNode.nodeValue;
				}
				else
				{
					hasTextChange=true;
				}
			}

			if(hasNonTextChanges || hasTextChange)
			{
				if(colChanged===false)
				{
					colChanged=true;
					// truncate arrays
					lastNodes[col].length=ct;
					lastText [col].length=ct;
				}
				lastNodes[col].push(textNode);
				lastText [col].push(textNode.nodeValue);
			}
			ct++;
		}
		// special case: less text (never seen this happen in practice)
		if(ct<lastNodes[col].length)
		{
			colChanged=true;
			lastNodes[col].length=ct;
			lastText [col].length=ct;			
		}
		if(colChanged)
		{
			changeFound=true;
			if(!isFirst){mainDiv.find('.hdiff-col-'+col).addClass('needs-save');}
		}
	}

	if(!changeFound && !forceRebuild)
	{
		//console.log('no changes found. no rebuild');
		if(webWorkerIsComputing){return;}
		resize();
		draw_canvas();
		while(rebuild_all_if_text_has_changed.callAfter.length){rebuild_all_if_text_has_changed.callAfter.pop()();}
		return;
	}

	mainDiv.find('.hdiff-canvas').css('opacity',.5);

	wordsRef=[];
	wordsRef[0]=build_words_ref(mainDiv.find(".hdiff-html-0")[0]);
	wordsRef[1]=build_words_ref(mainDiv.find(".hdiff-html-1")[0]);;

	compute_word_matches(function()
	{
		//console.log('rebuild_all_if_text_has_changed:part2');
		resize();
		// hlCursor depends on matches, so we have to rebuild it 
		cursor_highlight();
		draw_canvas();

		while(rebuild_all_if_text_has_changed.callAfter.length){rebuild_all_if_text_has_changed.callAfter.pop()();}
	});
}

//! Special case: Tinymce spuriously adds BOM near <a> links while moving cursor.
//! Just update positions in wordsRef instead of rebuilding all
function update_wordrefs_with_bom_change(node,oldText)
{
	var newText=node.nodeValue;
	var posOldToNew=[];
	var j=0;
	for(var i=0;i<oldText.length;)
	{
		if(oldText[i]===newText[j]){posOldToNew[i]=j;i++;j++;}
		else
		if(oldText[i]==='\ufeff'  ){posOldToNew[i]=j;i++;}
		else
		if(newText[j]==='\ufeff'){j++;}
	}
	posOldToNew[oldText.length]=newText.length;
	for(var colNum=0;colNum<2;colNum++)
	{
		for(var wordNum=0;wordNum<wordsRef[colNum].length;wordNum++)
		{
			var word=wordsRef[colNum][wordNum];
			if(word.node===node)
			{
				word.start=posOldToNew[word.start];
				word.end  =posOldToNew[word.end  ];
			}
		}
	}
}


//! Computes words that match between both columns of wordsRef by using an LCS algorithm.
//! Results are stored in wordsRef[?][?].match
//! This fct may be async.
function compute_word_matches(callAfter)
{
	//console.log('compute_word_matches');

	// We could make this faster by only doing the first 3 steps for the column that changed: est. improvement=30%

	// ********** build el and word lists
	var words=[[],[]];
	// Word to number index, so that lcs compares numbers instead of strings
	var dictionary={};
	var dictNb=0;
	var ignoreCase=mainDiv.find('.ignore-case').is(':checked');
	var j;
	for(var col=0;col<2;col++)
	{
		// pre-allocate, to avoid slow push
		words[col].length=wordsRef[col].length;
		for(j=0;j<wordsRef[col].length;j++)
		{
			var simplified=wordsRef[col][j].word;
			if(ignoreCase){simplified=simplified.toLowerCase();}
			if(!dictionary.hasOwnProperty(simplified)){dictionary[simplified]=dictNb++;}
			words[col][j]=dictionary[simplified];
		};
	}
	
	// Abort web worker if it hasn't finished
	if(webWorkerIsComputing)
	{
		//console.log('Terminate worker that has not finished');
		webWorker.terminate();
		webWorker=false;
	}

	// Use Web Worker (external thread) to compute LCS only if LCS is slow: medium & large texts. LCS is O(n x m)
	// The threshhold should depend on how fast this machine is... keep it simple for now.
	var useWorker=wordsRef[0].length*wordsRef[1].length > 300*300;
	//console.log('useWorker:',useWorker,'l:',wordsRef[0].length,wordsRef[1].length);
	if(useWorker)
	{
		if(webWorker===false)
		{
			// create new thread
			//console.log('Start new worker');
			webWorker=new Worker(webWorkerUrl);
		}
		webWorker.onmessage = function(e) 
		{
			//console.log('Message received from worker');
			webWorkerIsComputing=false;
			compute_word_matches_part2(e.data);
		};
		webWorkerIsComputing=true;
		lines=null;      // just for safety (and GC?)
		yPosMatches=null;// just for safety (and GC?)
		
		//console.log('Send message to worker');
		webWorker.postMessage([words[0],words[1]]);
	}
	else
	{
		var start=Date.now();
		var lcs=longest_common_subsequence(words[0],words[1]);
		compute_word_matches_part2(lcs);
	}

	function compute_word_matches_part2(lcs)
	{
		//console.log('compute_word_matches_part2');
		
		// *********** set matching words in wordsRef
		var p0=0;
		var p1=0;
		for(var j=0;j<lcs.length;j++)
		{
			var common=lcs[j];
			for(;words[0][p0]!=common && p0<words[0].length;p0++);
			for(;words[1][p1]!=common && p1<words[1].length;p1++);
			//console.log('common:',common,Object.keys(dictionary).find(key => dictionary[key] === common),wordsRef[0][p0],wordsRef[1][p1]);
			wordsRef[0][p0].match=p1;
			wordsRef[1][p1].match=p0;
			p0++;
			p1++;
		}
		callAfter();
	}
}


//! When geometry changes, we need to reset scroll spacers, resize canvas and recompute word rectangles and lines.
//! This is also called when user types in editor: text height may increase, and need enlargement of both columns
function resize()
{
	//console.log('resize',hdiffNb);
	if(webWorkerIsComputing){return;}
	// Recompute word and line positions. 
	// These don't depend on the height of hdiff-html, hdiff-html-inner, and spacers, so it is OK to compute them first.
	compute_rectangles_for_words();
	compute_lines();
	compute_y_position_matches();

	// toolbar can be position:fixed, so we need to manually set its width (so that save button can be right:0).
	mainDiv.find('.hdiff-tinymce-toolbar').width(mainDiv.find('.hdiff-col-inner').width());

	// Do not move right col if we are editting it (extremely annoying)
	if(blockRightScroll===true){blockRightScroll='needs-resize';}
	if(blockRightScroll===false && !noScrollHdiff)
	{
		// **** Compute the size of right col and spacers

		// ** First supose that there are no spacers and no browser rules (sScroll can be negative and no need for overflow to scroll)
		// Walk through all left col words and see how much right sScroll they would induce if they where at interest pos.
        // Both words (lw,rw) at same ypos means:
        //                  ************** |
        //                  * |          * | sScroll
        //                  * |rw.y      * V
        // *******************|**************** 
        //  *    lw.y|  *   * |          * 
        //  *        V  *   * V          * 
        //  *        lw *---*rw          * 
        //  *           *   *            * 
        //  *           *   **************
        //  ************* 
		// leftWordY = rightWordY - sScroll => sScroll= rightWordY-leftWordY
		var maxRightSScroll=-1000000;
		var minRightSScroll= 1000000;
		var leftInnerHeight =htmlInner[0].outerHeight();
		var rightInnerHeight=htmlInner[1].outerHeight();
		for(var wordNum=0;wordNum<wordsRef[0].length;wordNum++)
		{
			var leftWord=wordsRef[0][wordNum];
			if(leftWord.match===false){continue;}
			var rightWord=wordsRef[1][leftWord.match];
			var sScroll =rightWord.rect.middleY-leftWord.rect.middleY;
			maxRightSScroll=Math.max(maxRightSScroll,sScroll);
			minRightSScroll=Math.min(minRightSScroll,sScroll);
		}
		// add two extra match points : top and bottom
		// top: leftY=0 matches with rightY=0
		maxRightSScroll=Math.max(maxRightSScroll,0);
		minRightSScroll=Math.min(minRightSScroll,0);
		// bot: leftY=leftHeight matches with rightY=rightHeight
		maxRightSScroll=Math.max(maxRightSScroll,rightInnerHeight-leftInnerHeight);
		minRightSScroll=Math.min(minRightSScroll,rightInnerHeight-leftInnerHeight);
		
		// Right height is the max right sScroll + html1.height
		var rightHeight=-minRightSScroll+rightInnerHeight;

		// ** Now add spacers so that browser rules are satisfied
		// maxRightSScroll and minRightSScroll are computed without spacerTop => actual scroll adds spacerTop
		// Browser rules: 0  < actualScroll       < Height of contained - Height of container
		// Browser rule : 0  < actualScroll
		//             => 0  < sScroll + spacerTop => spacerTop > -sScroll ; Max(-sScroll) = -Min(sScroll)
		var spacerTop=-minRightSScroll;
		// Browser rule: actualScroll         < Height of contained - Height of container
		//            => sScroll + spacerTop  < (inner1.height + spacerTop + spacerBot ) - html1.height  
		//            => sScroll + spacerTop  - inner1.height - spacerTop + html1.height  < spacerBot
		var spacerBot= maxRightSScroll + spacerTop - rightInnerHeight - spacerTop + rightHeight;

		// Now set these values:
		mainDiv.find('.hdiff-html-1').outerHeight(rightHeight);
		mainDiv.find('.hdiff-html-1 .hdiff-html-spacer-top').height(spacerTop); 
		mainDiv.find('.hdiff-html-1 .hdiff-html-spacer-bot').height(spacerBot);
	}
	
	// Add min-height to document.body, to make sure that user can scroll interest_pos to bottom of left col.
	// bh>wh/2+bot
	var bodyMinHeight0=parseInt($('body').css('min-height').replace('px',''));
	var bodyMinHeight=bodyMinHeight0;
	var hdiffRect=mainDiv[0].getBoundingClientRect();
	bodyMinHeight=Math.max(bodyMinHeight,$(window).height()/2+hdiffRect.bottom);
	if(bodyMinHeight>bodyMinHeight0){$('body').css('min-height',Math.round(bodyMinHeight)+'px');}
	

	// *** Resize canvas
	var canvas=mainDiv.find('.hdiff-canvas');
	var ctx = canvas[0].getContext("2d");
	ctx.canvas.width =$(window).width();
	ctx.canvas.height=Math.min($(window).height(),mainDiv.height());

	// *** copy arrows
	mainDiv.find('.copy-button-0').css('left' ,mainDiv.find('.hdiff-html-0').width());
	mainDiv.find('.copy-button-1').css('right',mainDiv.find('.hdiff-html-1').width());
};

//! Determine the geometric position of each word by using the BoundingClientRect of a DOM range 
//! This sets wordsRef[?][?].rect
function compute_rectangles_for_words()
{
	// Compute rectangles for all words
	for(var colNum=0;colNum<2;colNum++)
	{
		var colTop=htmlInner[colNum][0].getBoundingClientRect().top;
		for(var wordNum=0;wordNum<wordsRef[colNum].length;wordNum++)
		{
			var word=wordsRef[colNum][wordNum];
			word.rect=word_rectangle(word,colTop);
		}
	}
}

function word_rectangle(word,yReference)
{
	// FIXME: remove this after beta testing
	if(webWorkerIsComputing){console.error('word_rectangle: webWorkerIsComputing');console.trace();alert('word_rectangle while working');return;}
	if(typeof word_rectangle.range==='undefined'){word_rectangle.range=document.createRange();}
	var range=word_rectangle.range;
	range.setStart(word.node,word.start);
	// FIXME: remove this after beta testing
	if(word.end>word.node.nodeValue.length){console.error('bad range',word.word,word.node.nodeValue,word.end);console.trace();alert('bad range');}
	range.setEnd(  word.node,word.end);
	var rects = range.getClientRects();
	if(rects.length===0){return null;}
	var rect=rects[0];
	var left=rect.x;
	var top =rect.y-(typeof yReference!=='undefined' ? yReference : 0);
	var res=
	{
		left:   left,
		top:    top,
		right:  left+rect.width,
		bottom: top +rect.height,
		width:  rect.width,
		height: rect.height,
		middleX: left+rect.width/2,
		middleY: top +rect.height/2
	};
	return res;
}

//! Determines "lines": splits wordsRef into horizontal lines.
//! Lines are list of words that have approximately the same bottom y coords
function compute_lines()
{
	lines=[[],[]];

	//*** Determine horizontal lines (list of words that have approximately the same bottom y coords)
	for(var colNum=0;colNum<2;colNum++)
	{
		var line={start:0,end:false,top:false,bottom:-10000,isMatch:false,isAllMatches:false,firstMatch:false,lastMatch:false};
		for(var wordNum=0;wordNum<wordsRef[colNum].length;wordNum++)
		{
			var word=wordsRef[colNum][wordNum];

			// Found new line: process it and start next
			if(Math.abs(word.rect.bottom-line.bottom)>5 && wordNum!==0)
			{
				line.end=wordNum;
				lines[colNum].push(line);
				line={start:wordNum,end:false,top:false,bottom:word.rect.bottom,isMatch:false,isAllMatches:false,firstMatch:false,lastMatch:false};
			}
			line.bottom=Math.max(line.bottom,word.rect.bottom);
		}
		// special case: last line
		if(wordsRef[colNum].length)
		{
			line.end=wordsRef[colNum].length;
			lines[colNum].push(line);
		}
	}

	// Compute isMatch, firstMatch, lastMatch and vertical size of each line. Also set lineNum on each word.
	for(var colNum=0;colNum<2;colNum++)
	{
		for(var lineNum=0;lineNum<lines[colNum].length;lineNum++)
		{
			var lineTotMatch=0;
			var lineTotDiff=0;
			var line=lines[colNum][lineNum];
			line.top= 100000;
			line.bottom=-100000;
			line.isAllMatches=true;
			line.firstMatch=false;
			line.lastMatch=false;
			for(var wordNum=line.start;wordNum<line.end;wordNum++)
			{
				var word=wordsRef[colNum][wordNum];
				word.lineNum=lineNum;
				var surface=word.rect.width*word.rect.height;
				if(word.match===false){line.isAllMatches=false;}
				else
				{
					if(line.firstMatch===false){line.firstMatch=wordNum;}
					line.lastMatch=wordNum;
				}
				if(word.match===false){lineTotDiff +=surface;}
				else                  {lineTotMatch+=surface;}
				line.top=Math.min(line.top,word.rect.top);
				line.bottom=Math.max(line.bottom,word.rect.bottom);
			}
			line.isMatch=lineTotMatch>lineTotDiff;
		}
	}

	// Vertically enlarge lines to make them adjacent
	for(var colNum=0;colNum<2;colNum++)
	{
		for(var lineNum=1;lineNum<lines[colNum].length;lineNum++)
		{
			var y=(lines[colNum][lineNum-1].bottom+lines[colNum][lineNum].top)/2;
			lines[colNum][lineNum-1].bottom=y;
			lines[colNum][lineNum  ].top=y;
		}
	}

	// Vertically enlarge first and last lines to fit inner-html
	lines[0][0                ].top   =0;
	lines[1][0                ].top   =0;
	lines[0][lines[0].length-1].bottom=htmlInner[0].outerHeight();
	lines[1][lines[1].length-1].bottom=htmlInner[1].outerHeight();
}

//! Creates an array (for each col) with matching words at least 7px appart.
function compute_y_position_matches()
{
	//console.log('compute_y_position_matches');
	yPosMatches=[[],[]];

	// get a list of ids (approx one per "line")
	for(var colNum=0;colNum<2;colNum++)
	{
		var lastYPos=-3;
		var prevWordNum=null;
		var dbg=0;
		// Build list of words that have a match and that are separated by a few (7) vertical pixels 
		var yWords=[];
		for(var wordNum=0;wordNum<wordsRef[colNum].length;wordNum++)
		{
			var word=wordsRef[colNum][wordNum];
			if(word.match===false){continue;}
			if(word.rect ===false){continue;}
			var yPos=Math.round(word.rect.middleY);
			if(yPos>lastYPos+7) // lastYPos===-3 is handled ok: we want to avoid a first word too close to y=0
			{
				yPosMatches[colNum].push({wordNum:wordNum,y:yPos});
				lastYPos=yPos;
			}
		}
	}
}


//! Returns position of vertical cursor, relative to mainDiv
//! It usually is in the middle of the viewport.
//! However, it is bounded to stay inside left col and can also be moved up from the middle (interestPosExcursion)
function interest_pos()
{
	//console.log('interestPosExcursion',interestPosExcursion);
	return $(window).height()/2+interestPosExcursion-mainDiv[0].getBoundingClientRect().top;
}

//! Sets newInterestPosExcursion making sure interest_pos stays within bounds.
function interest_pos_excursion_set(newInterestPosExcursion)
{
	interestPosExcursion=newInterestPosExcursion;
	interestPosExcursion=Math.max(interestPosExcursion,mainDiv.find(".hdiff-html-0")[0].getBoundingClientRect().top-$(window).height()/2);
	interestPosExcursion=Math.min(0,interestPosExcursion);	
}

//! Scroll event handler. See also mouse_wheel()
function window_scroll(e)
{
	//console.log('window_scroll');

	var moveRightCol=!webWorkerIsComputing && !noScrollHdiff;
	// Do not move right col if we are editing it (extremely annoying)
	if(mainDiv.find('.hdiff-col-1').hasClass('focused')){moveRightCol=false;}
	// Do not move right col if scroll is right after a mouse wheel event in right col.
	if(Date.now()<tmpDisableScrollRightSync+500){moveRightCol=false;}

	// ** Move right column
	if(moveRightCol)
	{
		// How much do we need to move scroll the right column to keep matches together at interestPos (vertical cursor)
		var interestPos=interest_pos();
		var rightPos=matching_position(interestPos,0);
		var moveRight=rightPos-interestPos;
		mainDiv.find('.hdiff-html-1').scrollTop(mainDiv.find('.hdiff-html-1').scrollTop()+moveRight);
	}

	// ** position of tinymce toolbar (for both cols)
	if(!noScrollHdiff)
	{
		for(var colNum=0;colNum<2;colNum++)
		{
			var toolsTop=mainDiv.find('.hdiff-col-'+colNum+' .hdiff-editor-tools')[0].getBoundingClientRect().top;
			var mainDivBot=mainDiv[0].getBoundingClientRect().bottom;
			var isFixed=toolsTop<0;
			var toolbar=mainDiv.find('.hdiff-col-'+colNum+' .hdiff-tinymce-toolbar');
			toolbar.toggleClass('fixed',isFixed);
			var isBot=mainDivBot<32;
			toolbar.toggle(!isBot);
		}
	}

	// ** redraw
	if(!webWorkerIsComputing)
	{
		draw_canvas();
	}
}

//! Returns a precise y position on the other column (as well as elements), given a y position on this column.
//! Y positions are relative to mainDiv.
//! Result is mostly computed by interpolating values yPosMatches. Edges cases are dealt with.
function matching_position(yPos,col)
{
	if(webWorkerIsComputing){console.error('matching_position: webWorkerIsComputing');console.trace();return;}
	var otherCol=col ? 0:1;

	var word2mainY=[htmlInner[0][0].getBoundingClientRect().top-mainDiv[0].getBoundingClientRect().top,
			        htmlInner[1][0].getBoundingClientRect().top-mainDiv[0].getBoundingClientRect().top];

	var innerHeight=[htmlInner[0].outerHeight(),
				     htmlInner[1].outerHeight()];

	// Find matching elements before and after y
	// y : coord relative to html-inner
	var y=Math.round(yPos-word2mainY[col]);
	
	// Out of bounds: use simple, soft 1/1, extrapolation 
	if(y<=0               ){return y                                         +word2mainY[otherCol];}
	if(y>=innerHeight[col]){return (y-innerHeight[col])+innerHeight[otherCol]+word2mainY[otherCol];}

	var found=binary_search(yPosMatches[col],y,function(a,i,v){return v>a[i].y;});

	if(y<yPosMatches[col][found].y){prev=yPosMatches[col][found-1];next=yPosMatches[col][found  ];}
	else                           {prev=yPosMatches[col][found  ];next=yPosMatches[col][found+1];}

	if(typeof prev==='undefined'){prev={wordNum:false,y:0};}
	if(typeof next==='undefined'){next={wordNum:false,y:innerHeight[col]};}

	// interpolate between prev and next
	var adjust=(y-prev.y)/(next.y-prev.y);
	
	// Find matching position on the other column 
	var otherPrevY=prev.wordNum!==false ? wordsRef[otherCol][wordsRef[col][prev.wordNum].match].rect.middleY : 0;
	var otherNextY=next.wordNum!==false ? wordsRef[otherCol][wordsRef[col][next.wordNum].match].rect.middleY : innerHeight[otherCol];

	var otherPos=(1-adjust)*otherPrevY+adjust*otherNextY;
	otherPos+=word2mainY[otherCol];
	return otherPos;
}

//! All drawing is done here.
function draw_canvas()
{
	//console.log('draw_canvas');
	// FIXME: remove debugging after beta
	if(webWorkerIsComputing){console.error('draw_canvas: webWorkerIsComputing');console.trace();alert('draw_canvas: webWorkerIsComputing');return;}
	mainDiv.find('.hdiff-canvas').css('opacity',1);
	var mainTop=mainDiv[0].getBoundingClientRect().top;
	var canvas = mainDiv.find('.hdiff-canvas');
	// Update canvas position before drawing on it
	// Canvas is position:absolute relative to mainDiv. That way it scrolls naturally even while rendering is blocked by rebuild.
	// We need to reposition it so that it is at the top of the window or at the top of mainDiv.
	canvas.css('top',Math.max(-mainTop,0)+'px');
	var ctx = canvas[0].getContext("2d");
	var canvasTop =canvas[0].getBoundingClientRect().top;
	var canvasLeft=canvas[0].getBoundingClientRect().left;

	var centerW=70;
	var centerX0=mainDiv.find('.hdiff-html-0').outerWidth()+2/*border*/;
	var canvasHeight=canvas[0].getBoundingClientRect().height;
	ctx.clearRect(0,0,$(window).width(),$(window).height()); // clear canvas

	var colPos=[{left: mainDiv.find('.hdiff-html-'+0)[0].getBoundingClientRect().left -canvasLeft,
				 right:mainDiv.find('.hdiff-html-'+0)[0].getBoundingClientRect().right-canvasLeft },
				{left: mainDiv.find('.hdiff-html-'+1)[0].getBoundingClientRect().left -canvasLeft ,
				 right:mainDiv.find('.hdiff-html-'+1)[0].getBoundingClientRect().right-canvasLeft }];

	var green="rgba(230, 255, 230, 1.0)";
	var red  ="rgba(255, 230, 230, 1.0)";



	// Translate word&line coords (relative to column top) to canvas coords (relative to top of canvas).
	var word2canvasY=[htmlInner[0][0].getBoundingClientRect().top-canvasTop,
			          htmlInner[1][0].getBoundingClientRect().top-canvasTop ];

	// Draw line bg
	for(var colNum=0;colNum<2;colNum++)
	{
		for(var lineNum=0;lineNum<lines[colNum].length;lineNum++)
		{
			var line=lines[colNum][lineNum];
			ctx.fillStyle = line.isMatch ? green : red;
			ctx.fillRect(colPos[colNum].left,line.top+word2canvasY[colNum],
						 colPos[colNum].right-colPos[colNum].left,line.bottom-line.top);
		}
	}

	// Draw red background rectangles behind each word that does not match
	// Merge adjacent rectangles that are on same line.
	for(var colNum=0;colNum<2;colNum++)
	{
		for(var lineNum=0;lineNum<lines[colNum].length;lineNum++)
		{
			var line=lines[colNum][lineNum];
			var prevX1=false;
			var prevWordNum=false;
			for(var wordNum=line.start;wordNum<line.end;wordNum++)
			{
				var word=wordsRef[colNum][wordNum];
				if((word.match===false) === line.isMatch)
				{
					var x0=word.rect.left -canvasLeft;
					var x1=word.rect.right-canvasLeft;
					if(prevWordNum===wordNum-1){x0=prevX1;}
					if(wordNum===line.start){x0=colPos[colNum].left ;}
					if(wordNum===line.end-1){x1=colPos[colNum].right;}
					ctx.fillStyle = word.match!==false ? green : red;
					ctx.fillRect(x0, line.top+word2canvasY[colNum], x1-x0,line.bottom-line.top);
					prevWordNum=wordNum;
					prevX1=word.rect.right-canvasLeft;
				}
			}
		}
	}

	// Draw connection between both columns
	// The goal is to have a visually smooth display, not a precise but unreadable one.
	// start by drawing a red background for the whole central part
	var lh=htmlInner[0].outerHeight();
	var rh=htmlInner[1].outerHeight();
	ctx.beginPath();
	ctx.fillStyle = red;
	ctx.moveTo       (colPos[0].right    ,   word2canvasY[0]);
	ctx.bezierCurveTo(colPos[0].right+20 ,   word2canvasY[0],
					  colPos[1].left-20  ,   word2canvasY[1],
					  colPos[1].left     ,   word2canvasY[1]);
	ctx.lineTo       (colPos[1].left     ,rh+word2canvasY[1]);
	ctx.bezierCurveTo(colPos[1].left-20  ,rh+word2canvasY[1],
					  colPos[0].right+20 ,lh+word2canvasY[0],
					  colPos[0].right    ,lh+word2canvasY[0]);
	ctx.fill();

	ctx.strokeStyle = "#0a0";

	// Trying to match both sides is pretty complex and not really worth it.
	// We simply draw a curve matching each left line and any lines it matches on the right side
	// The result is pretty satisfactory. We also add a few connector lines when there is a lot of green, or a few red words lost in green. 
	var lastGreenConnector=-1000;
	for(var lineNum=0;lineNum<lines[0].length;lineNum++)
	{
		var line=lines[0][lineNum];		
		var prevDrawnMatchingLine=false;
		var mLine;
		if(!line.isMatch){continue;}

		// Performance: stop drawing if we are past bottom
		mLine=lines[1][wordsRef[1][wordsRef[0][line.firstMatch].match].lineNum];
		if( line.top+word2canvasY[0]>canvasHeight &&
		   mLine.top+word2canvasY[1]>canvasHeight    ){break;}
		// Performance: do not draw if too high (verified large performance boost at the bottom of very large pages)
		var isBeforeWindow=false;
		mLine=lines[1][wordsRef[1][wordsRef[0][line.lastMatch].match].lineNum];
		if( line.bottom+word2canvasY[0]<0 &&
		   mLine.bottom+word2canvasY[1]<0    ){isBeforeWindow=true;}

		for(wordNum=line.start;wordNum<line.end;wordNum++)
		{
			var word=wordsRef[0][wordNum];
			if(word.match===false){continue;}
			var mWord=wordsRef[1][word.match];
			if(mWord.lineNum===prevDrawnMatchingLine){continue;}
			mLine=lines[1][mWord.lineNum];
			if(!isBeforeWindow)
			{
				ctx.beginPath();
				ctx.fillStyle = green;
				ctx.moveTo       (colPos[0].right     ,line.top    +word2canvasY[0]);
				ctx.bezierCurveTo(colPos[0].right+20  ,line.top    +word2canvasY[0],
								  colPos[1].left-20   ,mLine.top   +word2canvasY[1],
								  colPos[1].left      ,mLine.top   +word2canvasY[1]);
				ctx.lineTo       (colPos[1].left      ,mLine.bottom+word2canvasY[1]);
				ctx.bezierCurveTo(colPos[1].left-20   ,mLine.bottom+word2canvasY[1],
								  colPos[0].right+20  ,line.bottom +word2canvasY[0],
								  colPos[0].right     ,line.bottom +word2canvasY[0]);
				ctx.fill();
			}
		}
		// add extra green connector curve if everything is green
		if(lineNum>=2 && lines[0][lineNum-1].isAllMatches && lines[0][lineNum-2].isAllMatches && (lineNum-1)>lastGreenConnector+1)
		{
			draw_connector_for_line(0,lineNum-1,'top',"#6f6");
			lastGreenConnector=lineNum-1;
		}
	}	

	// Add extra red connector curve if surrounding lines are green but there is some red in it 
	for(var col=0;col<2;col++)
	{
		var lastRedConnector=-1000;
		for(var lineNum=0;lineNum<lines[col].length;lineNum++)
		{
			if(!lines[col][lineNum].isMatch || lines[col][lineNum].isAllMatches){continue;}
			if(lineNum<lastRedConnector+4){continue;}
			var ok=true;
			for(var i=-5;i<5;i++)
			{
				if(lineNum+i<0 || lineNum+i>=lines[col].length || !lines[col][lineNum+i].isMatch){ok=false;break;}
			}
			if(ok)
			{
		 		draw_connector_for_line(col,lineNum,'middle',"#f00");
		 		lastRedConnector=lineNum;
			}
		}	
	}

	function draw_connector_for_line(col,lineNum,pos,color)
	{
		var cLine=lines[col][lineNum];
		var cMLine=false;
		var leftY,rightY;
		for(var wordNum=cLine.start;wordNum<cLine.end;wordNum++)
		{
			var word=wordsRef[col][wordNum];
			if(word.match!==false){cMLine=lines[col ? 0:1][wordsRef[col ? 0:1][word.match].lineNum];break;}
		}
		if(pos==='middle')
		{
			var  leftLine=col ? cMLine : cLine;
			var rightLine=col ? cLine  : cMLine;
			leftY =( leftLine.top+ leftLine.bottom)/2;
			rightY=(rightLine.top+rightLine.bottom)/2;
		}
		else
		{
			leftY =cLine[pos];
			rightY=cMLine[pos];
		}
		ctx.strokeStyle = color;
		ctx.beginPath();
		ctx.moveTo       (colPos[0].right     , leftY+word2canvasY[0]);
		ctx.bezierCurveTo(colPos[0].right+20  , leftY+word2canvasY[0],
						  colPos[1].left-20   ,rightY+word2canvasY[1],
						  colPos[1].left      ,rightY+word2canvasY[1]);
		ctx.stroke();
	}


	// *** interest position: small black line 
	if(!noScrollHdiff)
	{
		var interestPos=interest_pos();
		var interestPosRight=matching_position(interestPos,0);
		ctx.strokeStyle = "#000";
		ctx.lineWidth=5;
		//ctx.beginPath();
		//ctx.moveTo(centerX0   ,interestPos+mainTop-canvasTop);
		//ctx.lineTo(centerX0+10,interestPos+mainTop-canvasTop);
		//ctx.stroke();
		ctx.beginPath();
		ctx.moveTo(       centerX0            ,interestPos     +mainTop-canvasTop);
		ctx.bezierCurveTo(centerX0        +20 ,interestPos     +mainTop-canvasTop, 
						  centerX0+centerW-20 ,interestPosRight+mainTop-canvasTop, 
						  centerX0+centerW    ,interestPosRight+mainTop-canvasTop);
		ctx.stroke();
		ctx.lineWidth=1;
	}


	// *** cursor_highlight
	if(hlCursor!==null)
	{
		ctx.fillStyle="#ff0";
		
		var buttonOk=false;
		for(var i=0;i<3;i++)
		{
			var wordNums=hlCursor.wordNums;
			if(wordNums[0][i]===false){continue;}
			// Draw highlighted word on both columns
			var words=[];
			for(var col=0;col<2;col++)
			{
				words[col]=wordsRef[col][wordNums[col][i]];
				ctx.fillRect( words[col].rect.left-canvasLeft,
							  words[col].rect.top +word2canvasY[col],
							  words[col].rect.width,
							  words[col].rect.height);
			}

			// Draw connector
			var yLeft  = words[0].rect.middleY +word2canvasY[0];
			var yRight = words[1].rect.middleY +word2canvasY[1];
			ctx.strokeStyle = "#ff0";
			ctx.lineWidth=3;
			ctx.beginPath();
			ctx.moveTo(       centerX0            ,yLeft);
			ctx.bezierCurveTo(centerX0        +20 ,yLeft, 
							  centerX0+centerW-20 ,yRight, 
							  centerX0+centerW    ,yRight);
			ctx.stroke();
			ctx.lineWidth=1;

			if(buttonOk===false)
			{
				mainDiv.find('.copy-button-1').css('top' ,
												   words[1].rect.middleY+htmlInner[1][0].getBoundingClientRect().top-mainDiv[0].getBoundingClientRect().top);
				mainDiv.find('.copy-button-0').css('top' ,
												   words[0].rect.middleY+htmlInner[0][0].getBoundingClientRect().top-mainDiv[0].getBoundingClientRect().top);
				buttonOk=true;
			}
		}
	}

	// ****** Right col: erase canvas outside of hdiff-html-1
	ctx.fillStyle = "#fff";
	var rect=mainDiv.find('.hdiff-html-1')[0].getBoundingClientRect();
	ctx.fillRect(colPos[1].left,0                    ,colPos[1].right-colPos[1].left,rect.top   -canvasTop);
	ctx.fillRect(colPos[1].left,rect.bottom-canvasTop,colPos[1].right-colPos[1].left,5000);
}



//! Handle wheel events both in right and left columns.
//! See also window_scroll()
//! Left column needs to manage interestPosExcursion when user scrolls past window top.
//! Right column needs to manage both interestPosExcursion, right column scroll and main window scroll.
function mouse_wheel(e)
{
	//console.log('mouse_wheel');
	// No geometry is available, just do normal window scroll
	if(webWorkerIsComputing){return;}
	if(noScrollHdiff){return;}

	// Delta is <0 when scrollTop decreases which means that page should move down
	var delta=e.originalEvent.deltaMode===0 /*DOM_DELTA_PIXEL*/ ? e.originalEvent.deltaY : e.originalEvent.deltaY*19;
	//console.log('delta',delta);

	if(blockRightScroll==='needs-resize'){resize();}
	blockRightScroll=false;

	// Scrolling elsewhere than in right column
	if(!$(e.target).parents('.hdiff-main-'+hdiffNb+' .hdiff-col-1').length)
	{
		// this allows the interest pos to move above the center of page, when user has allready scrolled all the way to the top of page
		if(delta<0 && $(window).scrollTop()+delta<0)
		{
			interest_pos_excursion_set(interestPosExcursion+($(window).scrollTop()+delta)/3);
		}
		else
		if(delta>0 && interestPosExcursion<0)
		{
			interest_pos_excursion_set(interestPosExcursion+delta/3);
			// Block normal window scroll down if interestPos hasn't made it back to it's normal position
			if(interestPosExcursion<0)
			{
				e.preventDefault();
				e.stopPropagation();
			}
		}

		window_scroll();
	}
	else
	{
		// **** Scrolling in right column
		//console.log('scrolling in right column',delta);

		// We want to scroll the right column by a fixed value (delta). That's simple.
		// However, we also want to scroll the whole window so that content on the left still matches content on the right.
		// Figuring out how much window scroll is needed is a bit confusing (different moving reference frames, directions and edge situations).
		
		// Do not scroll whole window normally, we will do it manually after computing how much it should scroll.
		e.preventDefault();
		e.stopPropagation();

		// The current position of the black cursor, relative to mainDiv.
		// This is a position that does not depend on the column. There is no "right  interestPos" and "left interestPos".
		var oldInterestPos=interest_pos();
		var right=mainDiv.find('.hdiff-html-1');
		// We are going to simulate different scrolls (windowDelta) and see which one leads to the best match.
		// The right side content has been scrolled (relative to the viewport) by delta.
		// We suppose that the window is scrolled by windowDelta
		var bestD=100000;
		var bestWindowDelta=false;

		// * Right hand scroll mandates (except on edges) a change in viewPort position of delta:
		// newRightHtmlInner.vpTop=oldRightHtmlInner.vpTop-delta
		// => oldRightHtmlInner.vpTop - windowDelta - scrollRight = oldRightHtmlInner.vpTop-delta
		// => windowDelta + scrollRight = delta

		// * interestPosVP should not change ! (in normal conditions - away from edges-, should stay in the center of the VP)
		// newInterestPosVP = oldInterestPosVP
		// newInterestPos + newMainVP = oldInterestPos + oldMainVP
		// newInterestPos= oldInterestPos + oldMainVP - newMainVP
		// newInterestPos= oldInterestPos + oldMainVP - (oldMainVP - windowDelta) 
		// newInterestPos= oldInterestPos + windowDelta

		// * The word on left col that is at newInterestPos depends on windowDelta. 
		// windowDelta can be freely chosen.
		// windowDelta should be chosen so that the word at newInterestPos on the left matches the word at newInterestPos on the right.
		// On the left,  the word that will be at newInterestPos is the one currently at oldInterestPos+windowDelta
		// On the right, the word that will be at newInterestPos is the one currently at oldInterestPos+delta
		// matching_position is a very irregular function, so 
		for(var windowDelta=-1000;windowDelta<1000;windowDelta++)
		{
		 	var rightPos=matching_position(oldInterestPos+windowDelta,0);
		 	var d= (oldInterestPos+delta) - rightPos;
		 	if(Math.abs(d)<Math.abs(bestD)){bestD=d;bestWindowDelta=windowDelta;}
		}
		
		tmpDisableScrollRightSync=Date.now();

		// ** browser rules (max & min scroll)

		var maxRightScroll=right.prop('scrollHeight')-right.innerHeight();
		var maxWindowScroll=$(document).height()-$(window).height();
		var scrollSum= right.scrollTop() + $(window).scrollTop() + delta;

		// Constraints: 
		// dRight+dWindow=delta => newRightScroll-right.scrollTop() + newWindowScroll-$(window).scrollTop() = delta
		//                      => newRightScroll + newWindowScroll = scrollSum
		// newRightScroll>=0
		// newWindowScroll>=0
		// newRightScroll<maxRightScroll
		// newWindowScroll<maxWindowScroll
		// So this is just about finding an intersection of a diagonal line (newRightScroll + newWindowScroll = scrollSum) and rectangle (4 inequalities).

		var newWindowScroll=$(window).scrollTop()+bestWindowDelta;
		var newRightScroll =scrollSum-newWindowScroll;

		// Simple case : already ok
		if(newRightScroll>=0 && newWindowScroll>=0 && newRightScroll<maxRightScroll && newWindowScroll<maxWindowScroll){}
		else
		// No solution (line under rectangle)
		if(scrollSum<0                             ){newWindowScroll=0;newRightScroll=0;}// FIXME: move interest pos excursion ?
		else
		// No solution (line above rectangle)
		if(scrollSum>maxRightScroll+maxWindowScroll){newWindowScroll=maxWindowScroll;newRightScroll=maxRightScroll;}
		else
		// complicated case (line intersects rectangle): find best intersection
		{
			//console.log('complicated case');
			var solutions=[{r:0,w:scrollSum},
						   {r:scrollSum,w:0},
						   {r:maxRightScroll,w:scrollSum-maxRightScroll},
						   {r:scrollSum-maxWindowScroll,w:maxWindowScroll}];
			var minD=10000000;
			var best=false;
			for(var i=0;i<4;i++)
			{
				if(solutions[i].r<0 || solutions[i].w<0 || solutions[i].r>maxRightScroll || solutions[i].w>maxWindowScroll){continue;}
				var d=Math.max(Math.abs(solutions[i].r-newRightScroll),Math.abs(solutions[i].w-newWindowScroll));
				if(best===false || d<minD){best=i;minD=d;}
			}
			newRightScroll =solutions[best].r;
			newWindowScroll=solutions[best].w;
		}

		right.scrollTop(newRightScroll);
		$(window).scrollTop(newWindowScroll);
		//console.log('maxRightScroll',maxRightScroll,'maxWindowScroll:',maxWindowScroll);

		draw_canvas();
	}
}


//function test_horizontal_matching_lines(col,event)
//{
// 	console.log('test_horizontal_matching_lines',col,event.clientY,event.pageY);
// 	var canvas = mainDiv.find('.hdiff-canvas');
// 	var ctx = canvas[0].getContext("2d");
// 	var canvasTop =canvas[0].getBoundingClientRect().top;
// 	var mainTop=mainDiv[0].getBoundingClientRect().top;
// 	var y=event.clientY-mainTop;
// 	var otherY=matching_position(y,col);
// 	ctx.fillStyle = "rgba(0, 255, 0, 1.0)";
// 	ctx.fillRect(0,y+mainTop-canvasTop,
// 				 1900,2);
// 	ctx.fillStyle = "rgba(0, 0, 255, 1.0)";
// 	ctx.fillRect(0,otherY+mainTop-canvasTop,
// 				 1900,2);
// 	console.log('y:',y,'drawy:',y+mainTop-canvasTop);
//}

//! Sets information in hlCursor about words around the start of the current selection and the matching words in the other diff column.
//! Drawing is not done here, it is done in draw_canvas()
//! index_node_to_wordnum() adds hdiffFirstWord and hdiffLastWord properties to each TextNode.
//! This makes it simple to find which words are at selection (range.startContainer).
function cursor_highlight()
{
	//console.log('cursor_highlight',hdiffNb);
	if(webWorkerIsComputing){return;} // will be auto called when work is over
	if(pendingCopyButtonCall){return;}
	var selection = window.getSelection();
	if(selection.rangeCount===0){hlCursor=null;mainDiv.find('.copy-button').hide();return;}
	//if(selection.isCollapsed){hlCursor=null;mainDiv.find('.copy-button').hide();return;}
	var range=selection.getRangeAt(0);
	var hdiffHtml=$(range.startContainer).parents('.hdiff-html');
	if(!mainDiv[0].contains(range.startContainer) || !hdiffHtml.length){hlCursor=null;mainDiv.find('.copy-button').hide();return;}
	hlCursor={wordNums:[[],[]],diff0:false,diff1: false};
	var col=hdiffHtml.hasClass('hdiff-html-0') ? 0 : 1;

	// FIXME: remove this line after debuging finished
	// editor clones nodes, so wordsRef..node is no longer valid. This should be checked/handled seriously elsewhere
	// Actually, is it a good idea to keep ref to "node" in wordsRef  ? If so, this wordsRef needs a refresh when editor clones the whole tree.
	if(!mainDiv[0].contains(wordsRef[0][0].node)){alert('node no longer valid');hlCursor=null;mainDiv.find('.copy-button').hide();return;}
	
	// This should be called elsewhere/ rethink
	index_node_to_wordnum();

	// We only deal with the simple case: range.startContainer is in textNode that is in wordsRef. 
	// This seems work almost all of the time.
	if(typeof range.startContainer.hdiffFirstWord==='undefined'){hlCursor=null;mainDiv.find('.copy-button').hide();return;}

	// We just need to compare start/end positions
	var prevWordNum=false;
	var onWordNum=false;
	var nextWordNum=false;
	var searchPrevMatch=false;
	var searchNextMatch=false;
	for(var wordNum =range.startContainer.hdiffFirstWord;
		wordNum<=range.startContainer.hdiffLastWord;
		wordNum++)
	{
		var word=wordsRef[col][wordNum];
		
		if(range.startOffset<word.start)
		{
			prevWordNum=wordNum>0 ? wordNum-1 : false;
			onWordNum  =false;
			nextWordNum=wordNum;
			searchPrevMatch=wordNum-1;
			searchNextMatch=wordNum;
			break;
		}
		if(range.startOffset<word.end)
		{
			prevWordNum=wordNum>0 ? wordNum-1 : false;
			onWordNum  =wordNum;
			nextWordNum=wordNum<wordsRef[col].length-1 ? wordNum+1 : false;
			searchPrevMatch=wordNum-1;
			searchNextMatch=wordNum+1;
			break;
		}
		if(wordNum==range.startContainer.hdiffLastWord && range.startOffset>=word.end)
		{
			prevWordNum=wordNum;
			onWordNum  =false;
			nextWordNum=wordNum<wordsRef[col].length-1 ? wordNum+1 : false;
			searchPrevMatch=wordNum;
			searchNextMatch=wordNum+1;
			break;				
		}
	}

	// Find prev matching word
	var prevMatching=false;
	for(;searchPrevMatch>=0;searchPrevMatch--)
	{
		if(wordsRef[col][searchPrevMatch].match!==false){prevMatching=searchPrevMatch;break;}
	}

	// Find next maching word
	var nextMatching=false;
	for(;searchNextMatch<wordsRef[col].length;searchNextMatch++)
	{
		if(wordsRef[col][searchNextMatch].match!==false){nextMatching=searchNextMatch;break;}
	}


	var onMatching=onWordNum!==false && wordsRef[col][onWordNum].match!==false ? onWordNum : false;

	// ** No fill up hlCursor.wordNums
	// This col
	hlCursor.wordNums[  col][0]=prevMatching;
	hlCursor.wordNums[  col][1]=onMatching;
	hlCursor.wordNums[  col][2]=nextMatching;
	// Other col
	for(var i=0;i<3;i++)
	{
		hlCursor.wordNums[1-col][i]=hlCursor.wordNums[col][i] === false ? false : wordsRef[col][hlCursor.wordNums[col][i]].match;
	}

	// Find diffs
	hlCursor.diff0=false;
	hlCursor.diff1=false;
	for(var c=0;c<2;c++)
	{
		// special case: at beginning of doc
		if(hlCursor.wordNums[c][0]===false && hlCursor.wordNums[c][1]===false  && hlCursor.wordNums[c][2]!==false    ){hlCursor.diff0=0;hlCursor.diff1=2;}

		if(                                   hlCursor.wordNums[c][1]!==false  && hlCursor.wordNums[c][2]!==false  &&
		                                      hlCursor.wordNums[c][1]+1    !==    hlCursor.wordNums[c][2]            ){hlCursor.diff0=1;hlCursor.diff1=2;}
		if(hlCursor.wordNums[c][0]!==false && hlCursor.wordNums[c][1]===false  && hlCursor.wordNums[c][2]!==false  &&
		   hlCursor.wordNums[c][0]+1                                         !==  hlCursor.wordNums[c][2]            ){hlCursor.diff0=0;hlCursor.diff1=2;}
		if(hlCursor.wordNums[c][0]!==false && hlCursor.wordNums[c][1]!==false  &&
		   hlCursor.wordNums[c][0]+1   !==    hlCursor.wordNums[c][1]                                                ){hlCursor.diff0=0;hlCursor.diff1=1;}
	}

	mainDiv.find('.copy-button').toggle(hlCursor.diff0!==false);

	for(var i=0;i<hdiffs.length;i++)
	{
		if(i===hdiffNb){continue;}
		hdiffs[i].unset_hl_cursor();
	}
	//console.log('cursor_highlight end',hdiffNb);
	draw_canvas();
}

//! Copy diffs around hlCursor from one column to the other column when user clicks on a copy button ([>] or [<])
function copy_button_mousedown()
{
	//console.log('copybutton mousedown',this);
	var srcCol=$(this).hasClass('copy-button-1') ? 1:0;
	
	var wordNums=hlCursor.wordNums;

	// The destination editor needs to be initialized so that we can add an undo level.
	// Use the pendingCopyButtonCall variable (hackish).
	if(editors[1-srcCol]===false)
	{
		// Simulate click on editor to initialize it
		htmlInner[1-srcCol].mousedown();
		pendingCopyButtonCall=srcCol;
		return;
	}

	// Add an undo level in dst col Tinymce, so that user can undo this copy.
	editors[1-srcCol].undoManager.add();

	// hlCursor can potentially show 2 diffs. 
	// Copying both is harder, as the first copy will change nodes.
	// So we just copy the first.

	// ** Determine start / end nodes and positions, from information in hlCursor
	var srcStartNode,srcStartPos,srcEndNode,srcEndPos;
	var dstStartNode,dstStartPos,dstEndNode,dstEndPos;

	// Src start
	var srcStartWordNum=wordNums[srcCol][hlCursor.diff0];
	var srcEndWordNum  =wordNums[srcCol][hlCursor.diff1];
	var dstStartWordNum=false;
	if(srcStartWordNum!==false)
	{
		var srcStartWord=wordsRef[srcCol][srcStartWordNum];
		srcStartNode   =srcStartWord.node;
		srcStartPos    =srcStartWord.end;
		dstStartWordNum=srcStartWord.match;
	}
	else
	{
		// special case: copying from beginning of src doc. First text node in doc.
		srcStartNode=document.createTreeWalker(htmlInner[srcCol][0],NodeFilter.SHOW_TEXT).nextNode();
		srcStartPos=0;
	}
	// Src end
	var srcEndWord=wordsRef[srcCol][srcEndWordNum];
	srcEndNode=srcEndWord.node;
	srcEndPos =srcEndWord.start;

	// Dst start
	if(dstStartWordNum!==false)
	{
		var dstStartWord=wordsRef[1-srcCol][dstStartWordNum];
		dstStartNode=dstStartWord.node;
		dstStartPos =dstStartWord.end;
	}
	else
	{
		// special case: copying towards beginning of dst doc. First text node in doc.
		dstStartNode=document.createTreeWalker(htmlInner[1-srcCol][0],NodeFilter.SHOW_TEXT).nextNode();
		dstStartPos=0;
	}
	// Dst end
	var dstEndWord=wordsRef[1-srcCol][srcEndWord.match];
	dstEndNode=dstEndWord.node;
	dstEndPos =dstEndWord.start;

	//console.log('srcStart pos,node',srcStartPos,srcStartNode);
	//console.log('srcEnd   pos,node',srcEndPos  ,srcEndNode  );
	//console.log('dstStart pos,node',dstStartPos,dstStartNode);
	//console.log('dstEnd   pos,node',dstEndPos  ,dstEndNode  );

	copy_range(srcStartNode,srcStartPos,
			   srcEndNode  ,srcEndWord.start,
			   dstStartNode,dstStartPos,
			   dstEndNode  ,dstEndPos);
	
	// Strange bug. If we try to set the selection (caret) immediately, we get a delayed Firefox error.
	setTimeout(function()
			   {
				   window.getSelection().collapse(srcEndWord.node,srcEndWord.start);
				   cursor_highlight();
			   },1);
}
//! Returns the textNode and position inside it where the cursor should be if the user clicks on cx,cy
//! The result will be constrained to be inside topNode
//! FIXME: this algorithm isn't very good. It can be extremely long if startNode is very large.
function find_cursor_from_position(topNode,cx,cy)
{
	//console.log('find_cursor_from_position:',cx,cy);

	var startNode=document.elementFromPoint(cx, cy);

	// Special case : If we are outside any line, find the closest line.
	// This happens, for example, when user clicks in margin between two lines.
	if(!topNode.contains(startNode))
	{
		var bestLine=false;
		var minDy=false;
		$(topNode).children().each(function()
		{
			var rect=this.getBoundingClientRect();
			var dy=Math.max(Math.max(rect.top-cy   ,0),
							Math.max(cy-rect.bottom,0));
			if(minDy===false || dy<minDy){bestLine=this;minDy=dy;}
		});
		startNode=bestLine;
	}

	// Now we have a startNode. Look into startNode to find exact letter position.
	var bestNode;
	var bestOffset;
	var bestDist=1000000;
	var range=document.createRange();

	var walk=document.createTreeWalker(startNode,NodeFilter.SHOW_TEXT);
	for(var n=startNode;n!==null;n=walk.nextNode())
	{
		if(n.nodeType!==Node.TEXT_NODE){continue;} // for first node. TreeWalker is braindead :-(
		for(var l=0;l<=n.nodeValue.length;l++)
		{
			// Collapsed selection (cursor) is OK on Firefox but not Chrome (getBoundingClientRect is empty)
			// We use a one char long selection
			var isLast=(l===n.nodeValue.length);
			range.setStart(n,!isLast ? l   : l-1);
			range.setEnd  (n,!isLast ? l+1 : l);

			var rect=range.getBoundingClientRect();
			
			var dy=(cy>=rect.top && cy<=rect.bottom) ? 0 : 
				Math.max(Math.max(0,rect.top-cy),
						 Math.max(0,cy-rect.bottom));
			var rectx=!isLast ? rect.left : rect.right;
			var dx=Math.abs(cx-rectx);
			var d=10000*dy+dx;
			if(d<bestDist){bestDist=d;bestNode=n;bestOffset=l;}
		}		
	}

	if(typeof bestNode==='undefined'){return null;}
	return {node:bestNode,offset:bestOffset};
}

//! user has hit the save button. Send edited body to server with Ajax.
function save(colNum,e)
{
	var url=mainDiv.find('.hdiff-col-'+colNum).attr('data-save');
	var postData={html: mainDiv.find('.hdiff-col-'+colNum+' .hdiff-html-inner').html()};
	if(typeof ns.save_hook!=='undefined'){ns.save_hook(postData,mainDiv,colNum);}
	
	$.post(url,postData,
		   function(response)
		   {
			   if(typeof ns.save_response_hook!=='undefined'){ns.save_response_hook(response,mainDiv,colNum);}
			   if(!response.ok)
			   {
				   alert(typeof response.message!=='undefined' ? response.message : 'Save failed');
				   return;
			   }
			   mainDiv.find('.hdiff-col-'+colNum).removeClass('needs-save');
		   });
}

//! Adds hdiffFirstWord and hdiffLastWord to each node in wordsRef
function index_node_to_wordnum()
{
	if(typeof wordsRef[0][0].node.hdiffFirstWord!=='undefined'){return;}
	for(var colNum=0;colNum<2;colNum++)
	{
		for(var wordNum=0;wordNum<wordsRef[colNum].length;wordNum++)
		{
			var word=wordsRef[colNum][wordNum];
			delete word.node.hdiffFirstWord;
		}
		for(var wordNum=0;wordNum<wordsRef[colNum].length;wordNum++)
		{
			var word=wordsRef[colNum][wordNum];
			if(typeof word.node.hdiffFirstWord==='undefined'){word.node.hdiffFirstWord=wordNum;}
			word.node.hdiffLastWord=wordNum;
		}
	}
}


//! Copies the DOM tree from src range to dst range, deleting the DOM tree that was in dst range.
//! Tries to keep a sane document structure and give dst a similar structure as that in src.
//! Assumes all arg nodes (src,dst - start/end) are text nodes (important).
function copy_range(srcStartNode,srcStartPos,srcEndNode,srcEndPos,
					dstStartNode,dstStartPos,dstEndNode,dstEndPos )
{
	//console.log('*********** copy_range');

	var srcRange=document.createRange();
	srcRange.setStart(srcStartNode,srcStartPos);
	srcRange.setEnd  (srcEndNode,srcEndPos);

	var dstRange=document.createRange();
	dstRange.setStart(dstStartNode,dstStartPos);
	dstRange.setEnd  (dstEndNode,dstEndPos);

	var copyFragment=srcRange.cloneContents();

	// Find fragment start and end (copied versions of srcStartNode and srcEndNode) 
	// We will need them for merge
	var fragStart=null;
	for(fragStart=copyFragment;fragStart.childNodes.length;fragStart=fragStart.childNodes[0]);
	var fragEnd=null;
	for(fragEnd=copyFragment;fragEnd.childNodes.length;fragEnd=fragEnd.childNodes[fragEnd.childNodes.length-1]);

	//*** Delete the destination range.
	dstRange.deleteContents();

 	var dstTop=htmlInner[0][0].contains(dstStartNode) ? htmlInner[0][0] : htmlInner[1][0];

	// *** See drawing: hdiff-copy-tree.odg

	// ** First we need to determine where we should insert 
	// Where to insert: default is the common ancestor of the deleted destination range.
	var insertNode=dstRange.commonAncestorContainer;

	// If there is a block level element in the top of the range, assume we need to insert this all the way at the top of the document.
	var fragmentHasBlock=false;
	for(var n=copyFragment.firstChild;n!==null;n=n.nextSibling)
	{
		if(n.nodeType===Node.ELEMENT_NODE && $(n).css('display')==='block'){fragmentHasBlock=true;}
	}
	if(fragmentHasBlock){insertNode=dstTop;}
	// If the insertNode is a text node, move it up a level, to simplify algo.
	if(insertNode.nodeType===Node.TEXT_NODE){insertNode=insertNode.parentNode;}

	// ** Walk up dst tree and split at each level if necessary
	var newDstEndNode=dstEndNode; // special case: track change in dstEndNode, needed by merge later on 
	// splitParent will be split at this position.
	var splitAt=null;
	for(var splitParent=dstStartNode;splitParent!==insertNode;splitParent=splitParent.parentNode)
	{
		// The first iteration is always a text node (since we asume dstStartNode is a text node)
		if(splitParent.nodeType===Node.TEXT_NODE)
		{
			// Check if we need to split this text node and if so, split it.
			// This happens if selection starts and ends within same text node.
			if(dstStartPos<splitParent.nodeValue.length)
			{
				var newNode=splitParent.cloneNode(false);
				newNode.nodeValue=splitParent.nodeValue.substr(dstStartPos);
				splitParent.nodeValue=splitParent.nodeValue.substr(0,dstStartPos);
 				splitParent.parentNode.insertBefore(newNode,splitParent.nextSibling);
				if(splitParent===dstEndNode){newDstEndNode=newNode;}
			}
		}
		else
		{
			// Check if split node is needed and split
			if(splitAt.nextSibling!==null)
			{
				var newNode=splitParent.cloneNode(false);
 				// move children to new node
 				var c;
 				while(c=splitAt.nextSibling){newNode.insertBefore(c,null);}
 				// insert the cloned node after splitParent
 				splitParent.parentNode.insertBefore(newNode,splitParent.nextSibling);
			}
		}
		splitAt=splitParent;
	}
	
	// ** We know where to insert, and have split => we can insert the fragment
	// Insert the fragment inside insertNode after splitAt
	// splitAt cannot be null as dstStartNode is a text node and insertNode is not (so above loop ran at least once)
	insertNode.insertBefore(copyFragment,splitAt.nextSibling);


	// *** At this point we have correctly inserted. 
	// We could very well stop here, but the copy adds extra paragraph breaks and other problems.
	// We try to merge the trees, both at the start and at the end of the copied fragment
	
	var nl,nr;
	var matches=true;

	// ** Merge at the start of the copied fragment.
	// First check if both trees match at boundary between dst doc and fragment.
	for(nl=dstStartNode,nr=fragStart;nl!==insertNode && nr!==insertNode;nl=nl.parentNode,nr=nr.parentNode)
	{
		if(nl.nodeName!==nr.nodeName){matches=false;}
	}
	// If they do match then merge each node on both sides of the boundary.
	if(matches)
	{
		var last=null;
		for(nl=dstStartNode,nr=fragStart;nl!==insertNode && nr!==insertNode;nl=nl.parentNode,nr=nr.parentNode)
		{
 			if(nl.nodeType===Node.TEXT_NODE)
 			{
 				nl.nodeValue+=nr.nodeValue;
 				nr.nodeValue='';
 			}
			else
			{
 				while(nr.childNodes.length)
 				{
 					nl.insertBefore(nr.childNodes[0],null);
 				}
			}
			last=nr;
		}
		// The last node is left empty with no children. Remove it.
		if(last!==null){last.parentNode.removeChild(last);}
	}

	// ** Merge at the end of the copied fragment.
	matches=true;
	// Special case: left merge may have changed or deleted the fragment's nodes. So fragEnd might not even be in the DOM tree.
	if(!dstTop.contains(fragEnd)){matches=false;}
	else
	{
		for(nl=fragEnd,nr=newDstEndNode;nl!==insertNode && nr!==insertNode;nl=nl.parentNode,nr=nr.parentNode)
		{
			if(nl.nodeName!==nr.nodeName){matches=false;}
		}
	}
	if(matches)
	{
		var last=null;
		for(nl=fragEnd,nr=newDstEndNode;nl!==insertNode && nr!==insertNode;nl=nl.parentNode,nr=nr.parentNode)
		{
 			if(nl.nodeType===Node.TEXT_NODE)
 			{
 				nr.nodeValue=nl.nodeValue+nr.nodeValue;
 				nl.nodeValue='';
 			}
			else
			{
 				while(nl.childNodes.length)
 				{
 					nr.insertBefore(nl.childNodes[0],nr.childNodes[0]);
 				}
			}
			last=nl;
		}
		if(last!==null){last.parentNode.removeChild(last);}
	}

	// *** Further merge: special case where src start and end are the same node. Try merging in dst.
	if(srcStartNode===srcEndNode && dstStartNode!==newDstEndNode)
	{
		if(dstStartNode.parentNode.nodeName===newDstEndNode.parentNode.nodeName)
		{
			dstStartNode.nodeValue+=newDstEndNode.nodeValue;
			var p=newDstEndNode.parentNode;
			p.removeChild(newDstEndNode);
			if(p.childNodes.length===0){p.parentNode.removeChild(p);}
		}
	}
	
	// *** Merge adjacent text nodes.
	insertNode.normalize();
}



// *************************
//! end Diff view setup enclosure
}


//! Builds a column for the wordsRef array by walking through all text nodes and splitting them into words.
function build_words_ref(node)
{
	if(typeof build_words_ref.wordRe==='undefined')
	{
		var unicodeCCSupported=true;
		try{new RegExp('\\p{L}','u')}catch(e){unicodeCCSupported=false;}
		if(unicodeCCSupported)
		{
			build_words_ref.wordRe=new RegExp('[0-9\\p{L}]+','gu');
		}
		else
		{
			build_words_ref.wordRe=new RegExp('['+letterRegExp+']+','g');
		}
	}
	var wordRe=build_words_ref.wordRe;
	var res=[];
	var prevPos=0;

	var walk=document.createTreeWalker(node,NodeFilter.SHOW_TEXT);
	var textNode;

	while(textNode=walk.nextNode())
	{
		var text=textNode.nodeValue;
		let found;
		// We use big regexp to find successive letter chars (note: js regexp does not handle unicode natively)
		while((found=wordRe.exec(text))!==null)
		{
			var word=found[0];
			prevPos=wordRe.lastIndex;
			res.push(
				{
				    node:textNode,
					start:wordRe.lastIndex-word.length,
					end:wordRe.lastIndex,
					word:word,
					match:false,
					rect:false,
					lineNum:false,
					wordNum: res.length
				});
		}
	}
	// Make sure index_node_to_wordnum() reindexes. FIXME: it is ugly to put this here, move it elsewhere
	if(res.length){delete res[0].node.hdiffFirstWord;}
	return res;
}


// http://rosettacode.org/wiki/Longest_common_subsequence#JavaScript
// This is slow for long texts.
// Example : fast machine 2018: 
//   - ordinary short event:  315 words =>    5ms
//   -      very long event: 5500 words => 2000ms
// consistent with O(n x m) => O(n2)
// For long texts, this is executed in a Web Worker (hdiff-worker.js)
function longest_common_subsequence(x,y){
	//var start=Date.now();
	var s,i,j,m,n,
		lcs=[],row=[],c=[],
		left,diag,latch;
	//make sure shorter string is the column string
	if(m<n){s=x;x=y;y=s;}
	m = x.length;
	n = y.length;
	//build the c-table
	for(j=0;j<n;row[j++]=0);
	for(i=0;i<m;i++){
		c[i] = row = row.slice();
		for(diag=0,j=0;j<n;j++,diag=latch){
			latch=row[j];
			if(x[i] == y[j]){row[j] = diag+1;}
			else{
				left = row[j-1]||0;
				if(left>row[j]){row[j] = left;}
			}
		}
	}
	i--,j--;
	//row[j] now contains the length of the lcs
	//recover the lcs from the table
	while(i>-1&&j>-1){
		switch(c[i][j]){
			default: j--;
				lcs.unshift(x[i]);
			case (i&&c[i-1][j]): i--;
				continue;
			case (j&&c[i][j-1]): j--;
		}
	}
	//console.log('longest_common_subsequence: ellapsed:',Date.now()-start);
	return lcs;
}


// Searches an array with sorted values and returns one of the (possibly two) closest indexes.
function binary_search(array,value,valueIsBigger,start,end)
{
	if(typeof start==='undefined')
	{
		start=0;
		end=array.length-1;
		if(!valueIsBigger(array,start,value)){return start;}
		if( valueIsBigger(array,end  ,value)){return end  ;}
	}
	if(start>=end-1){return start;}
	var middle=Math.floor((start+end)/2);
	if(valueIsBigger(array,middle,value)){start=middle;}
	else                                 {end  =middle;}
	return binary_search(array,value,valueIsBigger,start,end);
}


// http://stackoverflow.com/questions/280712/javascript-unicode-regexes
// http://difnet.com.br/opensource/unicode_hack.js
// FIXME: ES6 supports Regex character classes : wait until at least 2023 (not supported on ff60, ok on Chrome66)
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/unicode#Browser_compatibility
// See build_words_ref() which does browser support detection

var letterRegExp='0-9\u0041-\u005a\u00c0-\u00d6\u00d8-\u00de\u0100\u0102\u0104\u0106\u0108\u010a\u010c\u010e\u0110\u0112\u0114\u0116\u0118\u011a\u011c\u011e\u0120\u0122\u0124\u0126\u0128\u012a\u012c\u012e\u0130\u0132\u0134\u0136\u0139\u013b\u013d\u013f\u0141\u0143\u0145\u0147\u014a\u014c\u014e\u0150\u0152\u0154\u0156\u0158\u015a\u015c\u015e\u0160\u0162\u0164\u0166\u0168\u016a\u016c\u016e\u0170\u0172\u0174\u0176\u0178\u0179\u017b\u017d\u0181\u0182\u0184\u0186\u0187\u0189-\u018b\u018e-\u0191\u0193\u0194\u0196-\u0198\u019c\u019d\u019f\u01a0\u01a2\u01a4\u01a6\u01a7\u01a9\u01ac\u01ae\u01af\u01b1-\u01b3\u01b5\u01b7\u01b8\u01bc\u01c4\u01c7\u01ca\u01cd\u01cf\u01d1\u01d3\u01d5\u01d7\u01d9\u01db\u01de\u01e0\u01e2\u01e4\u01e6\u01e8\u01ea\u01ec\u01ee\u01f1\u01f4\u01f6-\u01f8\u01fa\u01fc\u01fe\u0200\u0202\u0204\u0206\u0208\u020a\u020c\u020e\u0210\u0212\u0214\u0216\u0218\u021a\u021c\u021e\u0220\u0222\u0224\u0226\u0228\u022a\u022c\u022e\u0230\u0232\u023a\u023b\u023d\u023e\u0241\u0243-\u0246\u0248\u024a\u024c\u024e\u0370\u0372\u0376\u0386\u0388-\u038a\u038c\u038e\u038f\u0391-\u03a1\u03a3-\u03ab\u03cf\u03d2-\u03d4\u03d8\u03da\u03dc\u03de\u03e0\u03e2\u03e4\u03e6\u03e8\u03ea\u03ec\u03ee\u03f4\u03f7\u03f9\u03fa\u03fd-\u042f\u0460\u0462\u0464\u0466\u0468\u046a\u046c\u046e\u0470\u0472\u0474\u0476\u0478\u047a\u047c\u047e\u0480\u048a\u048c\u048e\u0490\u0492\u0494\u0496\u0498\u049a\u049c\u049e\u04a0\u04a2\u04a4\u04a6\u04a8\u04aa\u04ac\u04ae\u04b0\u04b2\u04b4\u04b6\u04b8\u04ba\u04bc\u04be\u04c0\u04c1\u04c3\u04c5\u04c7\u04c9\u04cb\u04cd\u04d0\u04d2\u04d4\u04d6\u04d8\u04da\u04dc\u04de\u04e0\u04e2\u04e4\u04e6\u04e8\u04ea\u04ec\u04ee\u04f0\u04f2\u04f4\u04f6\u04f8\u04fa\u04fc\u04fe\u0500\u0502\u0504\u0506\u0508\u050a\u050c\u050e\u0510\u0512\u0514\u0516\u0518\u051a\u051c\u051e\u0520\u0522\u0524\u0526\u0531-\u0556\u10a0-\u10c5\u1e00\u1e02\u1e04\u1e06\u1e08\u1e0a\u1e0c\u1e0e\u1e10\u1e12\u1e14\u1e16\u1e18\u1e1a\u1e1c\u1e1e\u1e20\u1e22\u1e24\u1e26\u1e28\u1e2a\u1e2c\u1e2e\u1e30\u1e32\u1e34\u1e36\u1e38\u1e3a\u1e3c\u1e3e\u1e40\u1e42\u1e44\u1e46\u1e48\u1e4a\u1e4c\u1e4e\u1e50\u1e52\u1e54\u1e56\u1e58\u1e5a\u1e5c\u1e5e\u1e60\u1e62\u1e64\u1e66\u1e68\u1e6a\u1e6c\u1e6e\u1e70\u1e72\u1e74\u1e76\u1e78\u1e7a\u1e7c\u1e7e\u1e80\u1e82\u1e84\u1e86\u1e88\u1e8a\u1e8c\u1e8e\u1e90\u1e92\u1e94\u1e9e\u1ea0\u1ea2\u1ea4\u1ea6\u1ea8\u1eaa\u1eac\u1eae\u1eb0\u1eb2\u1eb4\u1eb6\u1eb8\u1eba\u1ebc\u1ebe\u1ec0\u1ec2\u1ec4\u1ec6\u1ec8\u1eca\u1ecc\u1ece\u1ed0\u1ed2\u1ed4\u1ed6\u1ed8\u1eda\u1edc\u1ede\u1ee0\u1ee2\u1ee4\u1ee6\u1ee8\u1eea\u1eec\u1eee\u1ef0\u1ef2\u1ef4\u1ef6\u1ef8\u1efa\u1efc\u1efe\u1f08-\u1f0f\u1f18-\u1f1d\u1f28-\u1f2f\u1f38-\u1f3f\u1f48-\u1f4d\u1f59\u1f5b\u1f5d\u1f5f\u1f68-\u1f6f\u1fb8-\u1fbb\u1fc8-\u1fcb\u1fd8-\u1fdb\u1fe8-\u1fec\u1ff8-\u1ffb\u2102\u2107\u210b-\u210d\u2110-\u2112\u2115\u2119-\u211d\u2124\u2126\u2128\u212a-\u212d\u2130-\u2133\u213e\u213f\u2145\u2183\u2c00-\u2c2e\u2c60\u2c62-\u2c64\u2c67\u2c69\u2c6b\u2c6d-\u2c70\u2c72\u2c75\u2c7e-\u2c80\u2c82\u2c84\u2c86\u2c88\u2c8a\u2c8c\u2c8e\u2c90\u2c92\u2c94\u2c96\u2c98\u2c9a\u2c9c\u2c9e\u2ca0\u2ca2\u2ca4\u2ca6\u2ca8\u2caa\u2cac\u2cae\u2cb0\u2cb2\u2cb4\u2cb6\u2cb8\u2cba\u2cbc\u2cbe\u2cc0\u2cc2\u2cc4\u2cc6\u2cc8\u2cca\u2ccc\u2cce\u2cd0\u2cd2\u2cd4\u2cd6\u2cd8\u2cda\u2cdc\u2cde\u2ce0\u2ce2\u2ceb\u2ced\ua640\ua642\ua644\ua646\ua648\ua64a\ua64c\ua64e\ua650\ua652\ua654\ua656\ua658\ua65a\ua65c\ua65e\ua660\ua662\ua664\ua666\ua668\ua66a\ua66c\ua680\ua682\ua684\ua686\ua688\ua68a\ua68c\ua68e\ua690\ua692\ua694\ua696\ua722\ua724\ua726\ua728\ua72a\ua72c\ua72e\ua732\ua734\ua736\ua738\ua73a\ua73c\ua73e\ua740\ua742\ua744\ua746\ua748\ua74a\ua74c\ua74e\ua750\ua752\ua754\ua756\ua758\ua75a\ua75c\ua75e\ua760\ua762\ua764\ua766\ua768\ua76a\ua76c\ua76e\ua779\ua77b\ua77d\ua77e\ua780\ua782\ua784\ua786\ua78b\ua78d\ua790\ua7a0\ua7a2\ua7a4\ua7a6\ua7a8\uff21-\uff3a\u0061-\u007a\u00aa\u00b5\u00ba\u00df-\u00f6\u00f8-\u00ff\u0101\u0103\u0105\u0107\u0109\u010b\u010d\u010f\u0111\u0113\u0115\u0117\u0119\u011b\u011d\u011f\u0121\u0123\u0125\u0127\u0129\u012b\u012d\u012f\u0131\u0133\u0135\u0137\u0138\u013a\u013c\u013e\u0140\u0142\u0144\u0146\u0148\u0149\u014b\u014d\u014f\u0151\u0153\u0155\u0157\u0159\u015b\u015d\u015f\u0161\u0163\u0165\u0167\u0169\u016b\u016d\u016f\u0171\u0173\u0175\u0177\u017a\u017c\u017e-\u0180\u0183\u0185\u0188\u018c\u018d\u0192\u0195\u0199-\u019b\u019e\u01a1\u01a3\u01a5\u01a8\u01aa\u01ab\u01ad\u01b0\u01b4\u01b6\u01b9\u01ba\u01bd-\u01bf\u01c6\u01c9\u01cc\u01ce\u01d0\u01d2\u01d4\u01d6\u01d8\u01da\u01dc\u01dd\u01df\u01e1\u01e3\u01e5\u01e7\u01e9\u01eb\u01ed\u01ef\u01f0\u01f3\u01f5\u01f9\u01fb\u01fd\u01ff\u0201\u0203\u0205\u0207\u0209\u020b\u020d\u020f\u0211\u0213\u0215\u0217\u0219\u021b\u021d\u021f\u0221\u0223\u0225\u0227\u0229\u022b\u022d\u022f\u0231\u0233-\u0239\u023c\u023f\u0240\u0242\u0247\u0249\u024b\u024d\u024f-\u0293\u0295-\u02af\u0371\u0373\u0377\u037b-\u037d\u0390\u03ac-\u03ce\u03d0\u03d1\u03d5-\u03d7\u03d9\u03db\u03dd\u03df\u03e1\u03e3\u03e5\u03e7\u03e9\u03eb\u03ed\u03ef-\u03f3\u03f5\u03f8\u03fb\u03fc\u0430-\u045f\u0461\u0463\u0465\u0467\u0469\u046b\u046d\u046f\u0471\u0473\u0475\u0477\u0479\u047b\u047d\u047f\u0481\u048b\u048d\u048f\u0491\u0493\u0495\u0497\u0499\u049b\u049d\u049f\u04a1\u04a3\u04a5\u04a7\u04a9\u04ab\u04ad\u04af\u04b1\u04b3\u04b5\u04b7\u04b9\u04bb\u04bd\u04bf\u04c2\u04c4\u04c6\u04c8\u04ca\u04cc\u04ce\u04cf\u04d1\u04d3\u04d5\u04d7\u04d9\u04db\u04dd\u04df\u04e1\u04e3\u04e5\u04e7\u04e9\u04eb\u04ed\u04ef\u04f1\u04f3\u04f5\u04f7\u04f9\u04fb\u04fd\u04ff\u0501\u0503\u0505\u0507\u0509\u050b\u050d\u050f\u0511\u0513\u0515\u0517\u0519\u051b\u051d\u051f\u0521\u0523\u0525\u0527\u0561-\u0587\u1d00-\u1d2b\u1d62-\u1d77\u1d79-\u1d9a\u1e01\u1e03\u1e05\u1e07\u1e09\u1e0b\u1e0d\u1e0f\u1e11\u1e13\u1e15\u1e17\u1e19\u1e1b\u1e1d\u1e1f\u1e21\u1e23\u1e25\u1e27\u1e29\u1e2b\u1e2d\u1e2f\u1e31\u1e33\u1e35\u1e37\u1e39\u1e3b\u1e3d\u1e3f\u1e41\u1e43\u1e45\u1e47\u1e49\u1e4b\u1e4d\u1e4f\u1e51\u1e53\u1e55\u1e57\u1e59\u1e5b\u1e5d\u1e5f\u1e61\u1e63\u1e65\u1e67\u1e69\u1e6b\u1e6d\u1e6f\u1e71\u1e73\u1e75\u1e77\u1e79\u1e7b\u1e7d\u1e7f\u1e81\u1e83\u1e85\u1e87\u1e89\u1e8b\u1e8d\u1e8f\u1e91\u1e93\u1e95-\u1e9d\u1e9f\u1ea1\u1ea3\u1ea5\u1ea7\u1ea9\u1eab\u1ead\u1eaf\u1eb1\u1eb3\u1eb5\u1eb7\u1eb9\u1ebb\u1ebd\u1ebf\u1ec1\u1ec3\u1ec5\u1ec7\u1ec9\u1ecb\u1ecd\u1ecf\u1ed1\u1ed3\u1ed5\u1ed7\u1ed9\u1edb\u1edd\u1edf\u1ee1\u1ee3\u1ee5\u1ee7\u1ee9\u1eeb\u1eed\u1eef\u1ef1\u1ef3\u1ef5\u1ef7\u1ef9\u1efb\u1efd\u1eff-\u1f07\u1f10-\u1f15\u1f20-\u1f27\u1f30-\u1f37\u1f40-\u1f45\u1f50-\u1f57\u1f60-\u1f67\u1f70-\u1f7d\u1f80-\u1f87\u1f90-\u1f97\u1fa0-\u1fa7\u1fb0-\u1fb4\u1fb6\u1fb7\u1fbe\u1fc2-\u1fc4\u1fc6\u1fc7\u1fd0-\u1fd3\u1fd6\u1fd7\u1fe0-\u1fe7\u1ff2-\u1ff4\u1ff6\u1ff7\u210a\u210e\u210f\u2113\u212f\u2134\u2139\u213c\u213d\u2146-\u2149\u214e\u2184\u2c30-\u2c5e\u2c61\u2c65\u2c66\u2c68\u2c6a\u2c6c\u2c71\u2c73\u2c74\u2c76-\u2c7c\u2c81\u2c83\u2c85\u2c87\u2c89\u2c8b\u2c8d\u2c8f\u2c91\u2c93\u2c95\u2c97\u2c99\u2c9b\u2c9d\u2c9f\u2ca1\u2ca3\u2ca5\u2ca7\u2ca9\u2cab\u2cad\u2caf\u2cb1\u2cb3\u2cb5\u2cb7\u2cb9\u2cbb\u2cbd\u2cbf\u2cc1\u2cc3\u2cc5\u2cc7\u2cc9\u2ccb\u2ccd\u2ccf\u2cd1\u2cd3\u2cd5\u2cd7\u2cd9\u2cdb\u2cdd\u2cdf\u2ce1\u2ce3\u2ce4\u2cec\u2cee\u2d00-\u2d25\ua641\ua643\ua645\ua647\ua649\ua64b\ua64d\ua64f\ua651\ua653\ua655\ua657\ua659\ua65b\ua65d\ua65f\ua661\ua663\ua665\ua667\ua669\ua66b\ua66d\ua681\ua683\ua685\ua687\ua689\ua68b\ua68d\ua68f\ua691\ua693\ua695\ua697\ua723\ua725\ua727\ua729\ua72b\ua72d\ua72f-\ua731\ua733\ua735\ua737\ua739\ua73b\ua73d\ua73f\ua741\ua743\ua745\ua747\ua749\ua74b\ua74d\ua74f\ua751\ua753\ua755\ua757\ua759\ua75b\ua75d\ua75f\ua761\ua763\ua765\ua767\ua769\ua76b\ua76d\ua76f\ua771-\ua778\ua77a\ua77c\ua77f\ua781\ua783\ua785\ua787\ua78c\ua78e\ua791\ua7a1\ua7a3\ua7a5\ua7a7\ua7a9\ua7fa\ufb00-\ufb06\ufb13-\ufb17\uff41-\uff5a\u01c5\u01c8\u01cb\u01f2\u1f88-\u1f8f\u1f98-\u1f9f\u1fa8-\u1faf\u1fbc\u1fcc\u1ffc\u02b0-\u02c1\u02c6-\u02d1\u02e0-\u02e4\u02ec\u02ee\u0374\u037a\u0559\u0640\u06e5\u06e6\u07f4\u07f5\u07fa\u081a\u0824\u0828\u0971\u0e46\u0ec6\u10fc\u17d7\u1843\u1aa7\u1c78-\u1c7d\u1d2c-\u1d61\u1d78\u1d9b-\u1dbf\u2071\u207f\u2090-\u209c\u2c7d\u2d6f\u2e2f\u3005\u3031-\u3035\u303b\u309d\u309e\u30fc-\u30fe\ua015\ua4f8-\ua4fd\ua60c\ua67f\ua717-\ua71f\ua770\ua788\ua9cf\uaa70\uaadd\uff70\uff9e\uff9f\u01bb\u01c0-\u01c3\u0294\u05d0-\u05ea\u05f0-\u05f2\u0620-\u063f\u0641-\u064a\u066e\u066f\u0671-\u06d3\u06d5\u06ee\u06ef\u06fa-\u06fc\u06ff\u0710\u0712-\u072f\u074d-\u07a5\u07b1\u07ca-\u07ea\u0800-\u0815\u0840-\u0858\u0904-\u0939\u093d\u0950\u0958-\u0961\u0972-\u0977\u0979-\u097f\u0985-\u098c\u098f\u0990\u0993-\u09a8\u09aa-\u09b0\u09b2\u09b6-\u09b9\u09bd\u09ce\u09dc\u09dd\u09df-\u09e1\u09f0\u09f1\u0a05-\u0a0a\u0a0f\u0a10\u0a13-\u0a28\u0a2a-\u0a30\u0a32\u0a33\u0a35\u0a36\u0a38\u0a39\u0a59-\u0a5c\u0a5e\u0a72-\u0a74\u0a85-\u0a8d\u0a8f-\u0a91\u0a93-\u0aa8\u0aaa-\u0ab0\u0ab2\u0ab3\u0ab5-\u0ab9\u0abd\u0ad0\u0ae0\u0ae1\u0b05-\u0b0c\u0b0f\u0b10\u0b13-\u0b28\u0b2a-\u0b30\u0b32\u0b33\u0b35-\u0b39\u0b3d\u0b5c\u0b5d\u0b5f-\u0b61\u0b71\u0b83\u0b85-\u0b8a\u0b8e-\u0b90\u0b92-\u0b95\u0b99\u0b9a\u0b9c\u0b9e\u0b9f\u0ba3\u0ba4\u0ba8-\u0baa\u0bae-\u0bb9\u0bd0\u0c05-\u0c0c\u0c0e-\u0c10\u0c12-\u0c28\u0c2a-\u0c33\u0c35-\u0c39\u0c3d\u0c58\u0c59\u0c60\u0c61\u0c85-\u0c8c\u0c8e-\u0c90\u0c92-\u0ca8\u0caa-\u0cb3\u0cb5-\u0cb9\u0cbd\u0cde\u0ce0\u0ce1\u0cf1\u0cf2\u0d05-\u0d0c\u0d0e-\u0d10\u0d12-\u0d3a\u0d3d\u0d4e\u0d60\u0d61\u0d7a-\u0d7f\u0d85-\u0d96\u0d9a-\u0db1\u0db3-\u0dbb\u0dbd\u0dc0-\u0dc6\u0e01-\u0e30\u0e32\u0e33\u0e40-\u0e45\u0e81\u0e82\u0e84\u0e87\u0e88\u0e8a\u0e8d\u0e94-\u0e97\u0e99-\u0e9f\u0ea1-\u0ea3\u0ea5\u0ea7\u0eaa\u0eab\u0ead-\u0eb0\u0eb2\u0eb3\u0ebd\u0ec0-\u0ec4\u0edc\u0edd\u0f00\u0f40-\u0f47\u0f49-\u0f6c\u0f88-\u0f8c\u1000-\u102a\u103f\u1050-\u1055\u105a-\u105d\u1061\u1065\u1066\u106e-\u1070\u1075-\u1081\u108e\u10d0-\u10fa\u1100-\u1248\u124a-\u124d\u1250-\u1256\u1258\u125a-\u125d\u1260-\u1288\u128a-\u128d\u1290-\u12b0\u12b2-\u12b5\u12b8-\u12be\u12c0\u12c2-\u12c5\u12c8-\u12d6\u12d8-\u1310\u1312-\u1315\u1318-\u135a\u1380-\u138f\u13a0-\u13f4\u1401-\u166c\u166f-\u167f\u1681-\u169a\u16a0-\u16ea\u1700-\u170c\u170e-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176c\u176e-\u1770\u1780-\u17b3\u17dc\u1820-\u1842\u1844-\u1877\u1880-\u18a8\u18aa\u18b0-\u18f5\u1900-\u191c\u1950-\u196d\u1970-\u1974\u1980-\u19ab\u19c1-\u19c7\u1a00-\u1a16\u1a20-\u1a54\u1b05-\u1b33\u1b45-\u1b4b\u1b83-\u1ba0\u1bae\u1baf\u1bc0-\u1be5\u1c00-\u1c23\u1c4d-\u1c4f\u1c5a-\u1c77\u1ce9-\u1cec\u1cee-\u1cf1\u2135-\u2138\u2d30-\u2d65\u2d80-\u2d96\u2da0-\u2da6\u2da8-\u2dae\u2db0-\u2db6\u2db8-\u2dbe\u2dc0-\u2dc6\u2dc8-\u2dce\u2dd0-\u2dd6\u2dd8-\u2dde\u3006\u303c\u3041-\u3096\u309f\u30a1-\u30fa\u30ff\u3105-\u312d\u3131-\u318e\u31a0-\u31ba\u31f0-\u31ff\u3400-\u4db5\u4e00-\u9fcb\ua000-\ua014\ua016-\ua48c\ua4d0-\ua4f7\ua500-\ua60b\ua610-\ua61f\ua62a\ua62b\ua66e\ua6a0-\ua6e5\ua7fb-\ua801\ua803-\ua805\ua807-\ua80a\ua80c-\ua822\ua840-\ua873\ua882-\ua8b3\ua8f2-\ua8f7\ua8fb\ua90a-\ua925\ua930-\ua946\ua960-\ua97c\ua984-\ua9b2\uaa00-\uaa28\uaa40-\uaa42\uaa44-\uaa4b\uaa60-\uaa6f\uaa71-\uaa76\uaa7a\uaa80-\uaaaf\uaab1\uaab5\uaab6\uaab9-\uaabd\uaac0\uaac2\uaadb\uaadc\uab01-\uab06\uab09-\uab0e\uab11-\uab16\uab20-\uab26\uab28-\uab2e\uabc0-\uabe2\uac00-\ud7a3\ud7b0-\ud7c6\ud7cb-\ud7fb\uf900-\ufa2d\ufa30-\ufa6d\ufa70-\ufad9\ufb1d\ufb1f-\ufb28\ufb2a-\ufb36\ufb38-\ufb3c\ufb3e\ufb40\ufb41\ufb43\ufb44\ufb46-\ufbb1\ufbd3-\ufd3d\ufd50-\ufd8f\ufd92-\ufdc7\ufdf0-\ufdfb\ufe70-\ufe74\ufe76-\ufefc\uff66-\uff6f\uff71-\uff9d\uffa0-\uffbe\uffc2-\uffc7\uffca-\uffcf\uffd2-\uffd7\uffda-\uffdc';



// Exports:
// window.hdiff.example=example;
// Global exports:
// window.example=example;

// end namespace wrapper
}());


