<p>[<a href="$base_url/translation-backend">_(Translation manager)</a>] [<a href="$base_url/translation-backend/import-file">_(Import file)</a>] [<a href="$base_url/translation-backend/export-file">_(Export file)</a>]</p>
<h1 class="page-title">_(Translation manager)</h1>
<p>_(Most of the text displayed on this web site is translated. The original text is written in english inside the source code of this program. Since there are many pieces of text to translate, they are cannot all be displayed here. Please use the search form to find the text you want to change.)</p>
<form method="get">
<div id="description"><?= t('You can search for : <ul><li>the text in English, for example: "Translation manager"</li><li>the text in @lang, for example: "Translation manager"</li><li>the link/url of the page where the text is displayed, for example: "@url"</li><li>a list of translations <a href="!base_url/translation-backend?search=_customized_">you have customized (changed)</a></li><li><a href="!base_url/translation-backend?search=_untranslated_">untranslated text</a></li><li><a href="!base_url/translation-backend?search=_unused_">unused translations</a> (<a href="!base_url/translation-backend/unused-delete">delete</a>)</li></ul>',
							['@lang'=>$language,'@url'=>$base_url.'/translation-backend','!base_url'=>$base_url]) ?>
</div>
<p>
<input type="textfield" name="search" size="60" value="<?: $_GET['search'] ?? '' ?>"/><input type="submit" value="_(Search)"/>
</p>
</form>
<? if($translations!==false){ ?>
<?= render('pager.tpl.php'); ?>
<table id="translations">
	<thead>
		<tr><th>id</th><th>_(English source)</th><th>_(Reference translation)</th><th>_(Current translation)</th></tr>
	</thead>
	<? foreach($translations as $translation){ ?>
		<tr>
			<td><a href="$base_url/translation-backend/translation/$translation['id']/edit">$translation['id']</td>
			<td><a href="$base_url/translation-backend/translation/$translation['id']/edit"><?= $translation['msgid'] ?></td>
			<td><a href="$base_url/translation-backend/translation/$translation['id']/edit"><?= $translation['msgstr'] ?></td>
			<td><a href="$base_url/translation-backend/translation/$translation['id']/edit"><?= $translation['translation'] ?></td>
		</tr>
	<?}?>
</table>
<?}?>
