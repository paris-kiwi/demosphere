<?php


//! $commandline_config : configuration for commandline.
//! List of $commandline_config keys:
//! - setup
//! - update_db_init
//! - status
//! - install
//! - help
//! -::-;-::-
$commandline_config;


//! Server-side execute of a command sent by command-line interface.
//! The command is executed by the web server user and not by the user that types the command line.
//! Security: this avoids running untrusted code in a multi-site setup.
//! Coherence: files creation or modification are done by the web-server user, avoiding permission problems.
function dlib_commandline_exec()
{
	global $commandline_config,$commandline_error_file,$dlib_config;

	ini_set('max_execution_time', max(600,ini_get('max_execution_time')));
	// Disable html in xdebug output
	ini_set('html_errors', 0);
	// This is set here, just in case. It is better to set it as early as possible.
	$dlib_config['is_commandline']=true;
	// Avoid output buffering. This is important to have a responsive commandline. 
	// Otherwise you need to wait for script to finish before getting any output.
	// Buffering may be done at several levels, some of which we may not be able to configure appropriately.
	// ini_set('output_buffering','Off') does not work. 
	// With Apache mod_php it is possible to configure "php_flag output_buffering Off" only for requests on commandline.php.
	// With php-fpm, this is not possible. Moreover, php-fpm and the Apache/FCGI proxy seem to do some extra buffering.
	// Our solution is to detect PHP buffering. If it is off, we don't need to do much.
	// If it is on, we handle output with a hook set with ob_start(). 
	// This hook encodes output in base64 and adds whitespace padding to fill up the buffer.
	// The client checks for the "X-Demosphere-Bufferpad" header and the reconstructs the output.
	ini_set('implicit_flush', true);
	ob_implicit_flush(true);
	ignore_user_abort(false);
	$useBufferpad=ini_get('output_buffering')!=0;
	$bufferpadSize=(int)ini_get('output_buffering');

	$command=isset($_POST['command']) ? $_POST['command'] : false;
	$args=$_POST;
	unset($args['command']);
	unset($args['signature']);
	unset($args['rand']);
	unset($args['name']);

	// ************ Security checks

	// only from localhost 
	if($_SERVER['REMOTE_ADDR']!=='127.0.0.1' &&
	   $_SERVER['REMOTE_ADDR']!==$_SERVER['SERVER_ADDR']){die('only allowed on localhost');}

	require_once 'site-config.php';
	$siteConfig=site_config();
	$name=$_POST['name'];
	if(!isset($siteConfig['commandline_admin_pub_keys'][$name])){die('public key not found for name');}
	$commandlineAdminPubKey=$siteConfig['commandline_admin_pub_keys'][$name];
	$commandlineHashDir    =$siteConfig['commandline_hash_dirs'     ][$name];
	$data=$name.':'.$_POST['rand'].':'.$command.':'.serialize($args);

	// check signature
	$ok=openssl_verify($data,base64_decode($_POST['signature']),$commandlineAdminPubKey, OPENSSL_ALGO_SHA256);
	if($ok!==1){die('public-key-verify failed');}

	// check hash in directory
	if(!file_exists($commandlineHashDir)){die('hash dir does not exist');}
	if( is_writable($commandlineHashDir)){die('hash dir is writable!');}
	$hash=hash('sha256',$data);
	if(!file_exists($commandlineHashDir.'/hash-'.$hash)){die('hash check failed');}

	$commandline_error_file=getcwd().'/files/private/tmp/cmdline-error-'.$hash;

	register_shutdown_function(function()
	{
		global $commandline_error_file;
		if(!defined('DLIB_COMMANDLINE_END_REACHED_NORMALLY'))
		{
			dlib_commandline_shutdown();
			dlib_commandline_error_output("dlib: remote exec exited prematurely\n");
			dlib_commandline_error_status();
		}
    });

	// ************ Setup environment, either by loading main app file (index.php) or using custom setup.
	if(!isset($commandline_config['setup'])){fatal('Dlib command line setup function $commandline_config["setup"] not provided');}
	$commandline_config['setup']($args);


	// ************ common options

	if(array_search('--debug',$args)!==false)
	{
		$dlib_config['debug']=true;
		$args=array_values(array_diff($args,['--debug']));
	}

	if(array_search('--no-bufferpad',$args)!==false)
	{
		$useBufferpad=false;
		$args=array_values(array_diff($args,['--no-bufferpad']));
	}

	// ************ execute command line action

	header( "Content-type: text/plain; charset=utf-8" );

	if($useBufferpad)
	{
		header("X-Demosphere-Bufferpad: on");
		// Adds lots of spaces after each output chunk to fill up buffer.
		// Normal output is base64 encoded to avoid mixing it with the spaces.
		ob_start(function($s)use($bufferpadSize)
				 {
					 $s='['.base64_encode($s).']';
					 $missing=$bufferpadSize-strlen($s)%$bufferpadSize;
					 return $s.str_repeat(' ',$missing);
				 },1);
	}

	// Try custom commands before built-in commands
	if(isset($commandline_config['exec' ]))
	{
		if($commandline_config['exec' ]($command,$args))
		{
			define('DLIB_COMMANDLINE_END_REACHED_NORMALLY', TRUE);
			exit(0);
		}
	}

	switch($command)
	{
	case 'status':
		dlib_commandline_status();
		break;
	case 'php-eval':
		//echo $args[0]."\n";
		if(isset($commandline_config['php_eval_init' ]))
		{
			$globs=$commandline_config['php_eval_init' ]();
			foreach($globs as $glob){global $$glob;}
		}
		$ok=eval($args[0]);
		if($ok===false){dlib_commandline_error_output("parse error\n");}
		if($ok===false){dlib_commandline_error_status();}
		break;
	case 'php-script':
		// FIXME: fix argv
		require_once $args[0];
		break;
	case 'update-db':
		$fctNamePrefix='';
		if(isset($commandline_config['update_db_init'])){$fctNamePrefix=$commandline_config['update_db_init' ]();}
		$currentUpdate=variable_get('update-db-current','',-1);
		for($update=$currentUpdate+1;;$update++)
		{
			$updateFctName=$fctNamePrefix.'update_db_'.$update;
			if(!function_exists($updateFctName)){break;}
			echo "=== UPDATE: ".$updateFctName."\n";
			$updateFctName();
			variable_set('update-db-current','',$update);
		}
		break;
	case 'help':
		dlib_commandline_help();
		break;
	default: dlib_bad_request_400("dlib: unknown command: $command");
	}
	dlib_commandline_shutdown();

	if($useBufferpad){ob_end_flush();}

    define('DLIB_COMMANDLINE_END_REACHED_NORMALLY', TRUE);
}


function dlib_commandline_help()
{
	global $commandline_config;
	echo "Usage: dlib [@ALIAS] command [OPTIONS] [ARGUMENTS]\n";
	echo "\n";
	echo "Options:\n";
	echo "  --debug          run in debug mode (sets \$dlib_config['debug'])\n";
	echo "  --no-bufferpad   disable output buffer padding (command display might be delayed)\n";
	echo "\n";
	echo "Commands:\n";
	echo "  install          erases / creates database tables\n";
	echo "                    dlib install FILE:function [-y] [--option=value] \n";
	echo "  update-db        updates database to most recent version (calls any new update functions)\n";
	echo "  status           display usefull information about this site\n";
	echo "  sql              open a SQL command-line interface\n";
	echo "  sql-eval         execute SQL command, raw results displayed\n";
	echo "  php-eval         executes the php code argument\n";
	echo "  php-script       executes the php file\n";
	echo "  add-credentials  adds commandline public key and hash dir to site's site-config.php\n";
	echo "  admin-pub-key    displays commandline public key\n";
	if(isset($commandline_config['help'])){$commandline_config['help']();}
}

function dlib_commandline_status()
{
	global $commandline_config,$db_config,$base_url,$appDir;
	echo dlib_string_pad("directory:"    ).getcwd()."\n";
	if(isset($db_config))
	{
		echo dlib_string_pad("database_host:"    ).$db_config['host']."\n";
		echo dlib_string_pad("database_database:").$db_config['database']."\n";
		echo dlib_string_pad("database_user:"    ).$db_config['user']."\n";
		echo dlib_string_pad("database_password:").$db_config['password']."\n";
		echo dlib_string_pad("update-db:").'finished update '.variable_get('update-db-current','','(none)')."\n";
		echo dlib_string_pad("base_url:").$base_url."\n";
	}
	if(isset($commandline_config['status'])){$commandline_config['status']();}
}

function dlib_string_pad(string $str,int $size=30)
{
	$spaces="                                                                                                    ";
	$l=mb_strlen($str);
	return $l<$size ? $str.mb_substr($spaces,0,$size-$l) : $str;
}

function dlib_commandline_shutdown()
{
	if(isset($_SESSION['dlib_messages']))
	{
		foreach($_SESSION['dlib_messages'] as $type=>$messages)
		{
			foreach($messages as $message)
			{
				// Server side execute of commandline only able to write to stdout :-(
				//fwrite(STDERR,"MESSAGE[$type]: ".$message."\n");
				dlib_commandline_error_output("MESSAGE[$type]: ".$message."\n");
			}
		}
	}
}

function dlib_commandline_error_output(string $msg)
{
	global $commandline_error_file;
	file_put_contents($commandline_error_file,$msg,FILE_APPEND);
}
function dlib_commandline_error_status()
{
	global $commandline_error_file;
	touch($commandline_error_file.'.status');
}
?>