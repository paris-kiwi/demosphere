<?php

function find_dates_and_times_el($src,$verbose=false,$fullResult=false)
{
	static $monthIndex,$weekDayIndex;
	static $re=false;
	static $spaces=false;
	if($verbose){$re=false;}
	if($verbose){echo "find_dates_and_times_el\n";}

	// FIXME: this messes up positions for $fullResult :-( 
	// Quick fix: also call it on the text you will change.
	$src=dlib_html_entities_decode_text($src,true);
	if(strlen($src)==0){return [];}

	// all of this is static / constant, only compute it once
	if($re===false)
	{
		list($monthList,$weekDayList)=date_name_elements_el();

		/******************** weekday regexp list *******************/
		// This list is used to build the regexps, and to find the weekday number
		// after the match.
		$weekDayIndex=[];
		$sortKey=[];
		foreach($weekDayList as $m=>$n)
		{
			$mre=make_regexp_for_greek_text($m);
			$mre=str_replace('.','',$mre);
			$weekDayIndex[]=[$n,$m,$mre];
			$sortKey[]=$mre;
		}
		array_multisort($sortKey,SORT_DESC,$weekDayIndex);
		//print_r($weekDayIndex);fatal("\n");

		/******************** month regexps list *******************/
		// This list is used to build the regexps, and to find the month number
		// after the match.
		$monthIndex=[];
		$sortKey=[];
		foreach($monthList as $m=>$n)
		{
			$mre=preg_replace('@ιος$@iu','(η|ιος)',$m);
			$mre=make_regexp_for_greek_text($mre);
			$mre=preg_replace('@\[sσς\]\)$@iu','[sσςyυΰϋύ])',$mre);
			$mre=str_replace('.','',$mre);
			$monthIndex[]=[$n,$m,$mre];
			$sortKey[]=$mre;
		}
		array_multisort($sortKey,SORT_DESC,$monthIndex);
		//print_r($monthIndex);fatal("\n");

		/******************** helper regular expressions *******************/
		$letterOrNumber='0-9a-z\x{0370}-\x{03FF}';
		$endWord='(?!['.$letterOrNumber.'])';
		$beginWord='(?<!['.$letterOrNumber.'])';
		
		// month RE
		$allMonths=[];
		foreach($monthIndex as $mi){$allMonths[]=$mi[2];}
		$reAnyMonth='(?P<month>'.implode('|',array_unique($allMonths)).')'.$endWord.'\.?';

		// weekday RE
		$allWeekDays=[];
		foreach($weekDayIndex as $mi){$allWeekDays[]=$mi[2];}
		$reAnyWeekday='('.implode('|',array_unique($allWeekDays)).')'.$endWord.'\.?';

		// note: <cite> is used for pagewatch diff highlight
		$spaces='([ '." ".// this last string is not a space, it is a UTF8 nbsp
			',\n\t]|</?(br|strong|b|em|span|font|cite)[^>]*/?>)*';
		$timeRE=get_time_re_el($spaces,$beginWord,$endWord);
		$yearRE='(?P<year>'.$beginWord.'20[0-9][0-9]'.$endWord.'|'.$beginWord.'[012][0-9]'.$endWord.'(?!( *(h|: *[0-9][0-9]|. *[0-9][0-9]| *μ\.?μ\.?|π\.?μ\.?| *to +meshmeri| *το +μεσημέρι))))';
		$yearRE2=str_replace('<year>','<year2>',$yearRE);
		$yearRE3=str_replace('<year>','<year3>',$yearRE);

		// huge regular expression for matching dates and times
		$re="@".
			'(?![  \n\t<>,])'.// do not begin by whitespace (strange?... otherwise crashes)
			// weekday
			"((?P<weekday>$beginWord$reAnyWeekday)$spaces)?".
			// alternative dates: 
			// alternative date 1: daynum month year OR month daynum  year
			"(".
			"((?<![0-9])(?P<daynum>[0-2][0-9]|3[01]|[1-9])$spaces)?".
			"([ηhi][ςsσ]|[ηhi]|th)? *".
			"([τt][οo][υyuv]|[-/])? *".
			"$beginWord$reAnyMonth$spaces".
			"(?(daynum)|(?P<daynum1>[0-2][0-9]|3[01]|[1-9])(?![0-9]) *(th)? *,? *)".
			"$yearRE?".
			"|".
			// alternative date 2: d/m/Y 
			"((?<![0-9/.])(?P<daynum2>[0-2][0-9]|3[01]|[1-9])[/.-](?P<month2>0[1-9]|1[012]|[1-9])(([/.-]".$yearRE2.")?))(?![0-9/.-])".
			"|".
			// alternative date 3: (d-m-Y)
			"(\((?P<daynum3>[0-2][0-9]|3[01]|[1-9])-(?P<month3>0[1-9]|1[012]|[1-9])(?![0-9])(-".$yearRE3.")?\))".
			")".$spaces.
			// '-'  separator between date and time
			'([-\@]'.$spaces.')?'.
			'(?P<time>'.$timeRE.')?'.
			"@iu";
		if($verbose){echo "re:$re\n";}
	}

	$bigMatchRes=preg_match_all($re,$src,$pregMatches,
								($fullResult ? 
								 PREG_OFFSET_CAPTURE : 
								 PREG_PATTERN_ORDER));
	//echo $re."\n";
	if($verbose){echo "preg_match_all return value:";var_dump($bigMatchRes);}
	if(!$bigMatchRes){return [];}

	// if full result  (with pos), then rebuild an  array like the one
	// without full result
	if($fullResult)
	{
		$matches=[];
		foreach($pregMatches as $key => $val)
		{
			foreach($val as $key1 => $val1)
			{
				if($val1===''){$matches[$key][$key1]='';}
				else{$matches[$key][$key1]=$val1[0];}
			}
		}
	}
	else
	{
		$matches=&$pregMatches;
	}
	
	if($verbose)
	{
		foreach($matches as $k=>$ma){$m=$ma[0];if($m!==''){echo "$k : \"$m\"\n";}}
	}
	
	$timestamps=[];
	// compute a timestamp for each matching date and time.
	foreach($matches[0] as $nb => $match)
	{
		$weekday=$matches['weekday'][$nb];
		$daynum =$matches['daynum' ][$nb].$matches['daynum1' ][$nb].
			     $matches['daynum2'][$nb].$matches['daynum3' ][$nb];
		$month  =$matches['month'  ][$nb].$matches['month2'  ][$nb].
			     $matches['month3' ][$nb];
		$year   =$matches['year'   ][$nb].$matches['year2'   ][$nb].
			     $matches['year3'  ][$nb];
		$hour   =$matches['hour1'  ][$nb].$matches['hour2'   ][$nb];
		$minute =$matches['minutes'][$nb];
		$amPm   =$matches['am_pm1' ][$nb].$matches['am_pm2'  ][$nb].
			     $matches['am_pm3' ][$nb];

		if($verbose)
		{
			echo '$weekday:'.		 $weekday."\n"; 
			echo '$daynum :'.		 $daynum ."\n"; 
			echo '$month  :'.		 $month	 ."\n";		
			echo '$year	  :'.		 $year	 ."\n"; 
			echo '$hour	  :'.		 $hour	 ."\n"; 
			echo '$minute :'.		 $minute ."\n"; 
			echo '$amPm	  :'.		 $amPm	 ."\n"; 
			echo "****************\n";
			echo "match  : $match\n";
		}

		if($daynum <= 0 || $daynum>31)
		{
			if($verbose)echo "bad daynum:$daynum\n";
			continue;
		}

		if(strlen($year)===4 && $year<1971)
		{
			if($verbose)echo "Ignore very old dates\n";
			continue;			
		}

		if($year>=2038)
		{
			if($verbose)echo "Ignore dates 2038 or later.\n"; // FIXME tmp year 2038 workaround. (4 bytes overflow)
			continue;			
		}

		if(preg_match('@^'.$spaces.'[0-9]+\.[0-9]+'.$spaces.'$@',$match))
		{
			if($verbose)echo "rejected num.num : this causes too many false positives\n";
			continue;
		}

		// compute month number from month name 
		$monthNum=0;
		if(preg_match('@^[0-9]+$@',$month)){$monthNum=$month;}
		else
		{
			foreach($monthIndex as $mi)
			{
				if(preg_match('@'.$mi[2].'@iu',$month)){$monthNum=$mi[0];break;}
			}
		}
		if($monthNum <= 0 || $monthNum>12){fatal("bad monthNum:$monthNum\n");}

		// compute weekday number from weekday name 
		// compute month number from month name 
		$weekdayNum=false;
		foreach($weekDayIndex as $mi)
		{
			if(preg_match('@^'.$mi[2].'@iu',$weekday)){$weekdayNum=$mi[0];break;}
		}
		//if($weekdayNum!==false){echo $weekdayNum;fatal("\n$src\nwww");}

		// guess year if it is missing (choose year with closest date to now)
		if($year==''){$year=find_date_time_guess_year($daynum,$monthNum);}
		// determine time
		if($minute===""){$minute=0;}
		if($hour  ===""){$hour=3;$minute=33;}
		else
		{
			// Apply AM/PM to time
			if(strlen($amPm)>0 && mb_stripos($amPm,'π',0,'UTF-8')===false && $hour<12){$hour+=12;}
			// for early hours PM is assumed
			if($hour<=9 && strlen($amPm)==0){$hour+=12;}
		}

		if(!checkdate($monthNum,$daynum,$year))
		{
			if($verbose)echo "rejected $year-$monthNum-$daynum : invalid gregorian date (checkdate)\n";
			continue;
		}

		// now build the timestamp
		$ts=mktime($hour,$minute,0,$monthNum,$daynum,$year);
		if($verbose)echo "res: ".date('r',$ts)."  ($ts)\n";

		// if weekday is available, we can check if day is ok
		if($ts==0)
		{
			if($verbose)echo "bad date/time!\n";
		}
		else
		if($weekdayNum!==false && day_of_week($ts)!=($weekdayNum-1))
		{
			if($verbose)echo "WEEKDAY check REJECTED!\n";
		}
		else
		{
			if($fullResult)
			{
				$timestamps[]=[$ts,
							   $pregMatches[0][$nb][0],
							   $pregMatches[0][$nb][1]];
			}
			else
			{$timestamps[]=$ts;}
		}
	}
	return $timestamps;
}

function find_date_and_time_el($src,$verbose=false)
{
	$timestamps=find_dates_and_times($src,$verbose);
	if(count($timestamps)!=1){return false;}
	return $timestamps[0];
}


function find_times_el($src,$format='h',$verbose=false)
{
	$spaces='([ '." ".// this last string is not a space, it is a UTF8 nbsp
		',\n\t]|&nbsp;|</?(br|strong|b|em|span) ?/?>|)*';
	$letterOrNumber='0-9a-z\x{0370}-\x{03FF}';
	$endWord='(?!['.$letterOrNumber.'])';
	$beginWord='(?<!['.$letterOrNumber.'])';
	//$beginWord='';$endWord='';
	//$spaces=' *';
	$timeRE=get_time_re_el($spaces,$beginWord,$endWord);
	//file_put_contents('/tmp/x1',$timeRE);
	//$src="20:00";
	//echo "src:$src\n";
	//echo "timeRE:$timeRE\n";
	//$timeRE='\b([1-9]|[0-2][0-9])([ ]*(heures|heure|h)[ ]*|:)([0-9][0-9]|)\b';
	if(!preg_match_all('@'.$timeRE.'@isu',$src,$matches)){return [];}
	if($verbose)print_r($matches);
	$times=[];
	// compute a time for each matching date and time.
	foreach($matches[0] as $nb => $match)
	{
		$hour   =$matches['hour1' ][$nb].$matches['hour2'][$nb];
		$minute =$matches['minutes'][$nb];
		$amPm   =$matches['am_pm1'][$nb].$matches['am_pm2'][$nb].$matches['am_pm3'][$nb];

		if(preg_match('@[^0-9]@',$hour)   ||
		   preg_match('@[^0-9]@',$minute) ||
		   $hour   < 0 || $hour  >24 || 
		   $minute < 0 || $minute>59)
		{
			$ts='invalid';
		}
		else
		{
			if(strlen($amPm)>0 && mb_stripos($amPm,'π',0,'UTF-8')===false && $hour<12){$hour+=12;}
			switch($format)
			{
			case 'h': $ts=intval($hour).'h'.
						  ($minute!=0 ? sprintf('%02d',$minute) : '');
			case ':': $ts=sprintf('%02d:%02d',$hour,$minute);break;
			default:  fatal("invalid time format requested");
			}
		}
		$times[]=$ts;
	}
	return $times;
}

function get_time_re_el($spaces,$beginWord,$endWord)
{
	$textBeforeTimePhrases=
		[
			'στο στέκι , στις',
			'γύρω στις',
			'στις',
			'από τις',
			'και ώρα',
		];
	// make a more general regular expresion out of each phrase
	$textBeforeTimePhrasesRE=[];
	foreach($textBeforeTimePhrases as $p)
	{
		$p=make_regexp_for_greek_text($p);
		$p=str_replace(' ',' *',$p);
		$p=str_replace(',',',?',$p);
		$p=' *,? *'.$p.' *,? *';
		$textBeforeTimePhrasesRE[]=$p;
	}
	//print_r($textBeforeTimePhrasesRE);
	//$textBeforeTime1='(?P<text_before_time1> *,? *στο Στέκι *, *στις *| *, γύρω στις *| *,? *γύρω στις *| *stis *|στις *|στισ| *από *τις *| *και ώρα)';
	$textBeforeTime1='(?P<text_before_time1>'.implode('|',$textBeforeTimePhrasesRE).')';
	//echo "text_before_time:$textBeforeTime1\n";fatal("\n");
	$textBeforeTime2=str_replace('text_before_time1','text_before_time2',$textBeforeTime1);
	$amPm1='(?P<am_pm1>μ\.?μ\.?|π\.?μ\.?)';
	$amPm2=str_replace('am_pm1','am_pm2',$amPm1);
	$amPm3=str_replace('am_pm1','am_pm3',$amPm1);
	$timeRE=
		"(".
		// time version 1: explicit hour+time
		$textBeforeTime1.'?'.$spaces.
		''.$beginWord.'(?P<hour1>[0-1][0-9]|2[0-4]|[1-9])'.
		'([ ]*h[ ]*|(?P<hour_sep>[:,.]))'.
		'(?(hour_sep)(?P<minutes>[0-5][0-9])|)'.$spaces.
		''.$amPm1.'?'.
		'( *to +meshmeri| *το +μεσημέρι)?'. // FIXME, dont'understand
		")".
		"|".
		// time version 2: simple hour number :
		// needs time designating text before or after
		$textBeforeTime2.'?'.$spaces.
		'(?P<hour2>[0-1][0-9]|2[0-4]|[1-9])'.$spaces.
		'(?(text_before_time2)'.// if text_before_time1 is set
		// then text after time is optional
		'('.$amPm2.'?'.
		'( *to +meshmeri| *το +μεσημέρι)?)?'.
		// otherwise it is necessary
		'|('.$amPm3.'|'.
		'( *to +meshmeri| *το +μεσημέρι))'.
		')'.
		''
		;
	return $timeRE;
}

// a list of different representations of month names and weekday names
function date_name_elements_el()
{
	static $res=false;
	if($res!==false){return $res;}

	/******************** months ************************/
	$months=[
		'Ιανουάριος'  =>1,
		'Φεβρουάριος'  =>2,
		'Μάρτιος'     =>3,
		'Απρίλιος'    =>4,
		'Μάιος'      =>5,
		'Ιούνιος'     =>6,
		'Ιούλιος'  =>7,
		'Αύγουστος'     =>8,
		'Αυγούστου'     =>8,
		'Αυγούστη'     =>8,
		'Σεπτέμβριος'=>9,
		'Οκτώβριος'  =>10,
		'Οκτώμβριος'  =>10,
		'Νοέμβριος' =>11,
		'Δεκέμβριος' =>12,
		'Ιαν.'  =>1, 
		'Φεβ.'  =>2, 
		'Μάρ.'	  =>3, 
		'Απρ.'  =>4, 
		'Μάι.'	  =>5, 
		'Ιούν.' =>6, 
		'Ιούλ.' =>7, 
		'Αύγ.'	  =>8, 
		'Σεπ.'  =>9, 
		'Οκτ.'  =>10, 
		'Νοέ.'	  =>11, 
		'Δεκ.'  =>12, 
			];

	/******************** weeks ************************/

	$weekDays=[
		'Δευτέρα'    => 1,
		'Deftera'  => 1,
		'Τρίτη'    => 2,
		'Τετάρτη' => 3,
		'Πέμτη'    => 4,
		'Πέμπτη'    => 4,
		'Παρασκευή' => 5,
		'Σάββατο'   => 6,
		'Σάβατο'   => 6,
		'Κυριακή' => 7,
		'Kiriaki' => 7,
		'Δευ.'=>1, 
		'Τρι.'=>2, 
		'Τετ.'=>3, 
		'Πεμ.'=>4, 
		'Παρ.'=>5, 
		'Σαβ.'=>6, 
		'Κυρ.'=>7, 
			  ];
					
	$res=[$months,$weekDays];
	return $res;
}

function make_regexp_for_greek_text($str)
{
	static $srcTable=false;
	static $destTable=false;
	if($srcTable===false)
	{
		$info=dlib_greek_language_info();
		$greeklish=$info['transliteration'];
		$greekAccents=$info['accents'];
		// reverse accents array
		$revAccents=[];
		foreach($greekAccents as $k=>$v){$revAccents[$v][]=$k;}
		//print_r($revAccents);
		foreach($greeklish as $srcLetter=>$latinTrans)
		{
			
			// check for multiletter transliteration
			$maxl=0;
			foreach($latinTrans as $t){$maxl=max($maxl,strlen($t));}

			$possibilities=$latinTrans;
			$possibilities[]=$srcLetter;
			$possibilities=array_merge($possibilities,val($revAccents,$srcLetter,[]));
			if($maxl>1){$letterRe='('.implode('|',$possibilities).')';}
			else       {$letterRe='['.implode('' ,$possibilities).']';}
			$srcTable[]=$srcLetter;
			$destTable[]=$letterRe;
		}
	}
	$str=mb_strtolower($str,'UTF-8');
	$str=dlib_remove_accents($str);
	return str_replace($srcTable,$destTable,$str);
}

?>