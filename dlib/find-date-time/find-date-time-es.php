<?php

function find_dates_and_times_es($src,$verbose=false,$fullResult=false)
{
	static $monthIndex,$weekDayIndex;
	static $re=false;
	static $spaces=false;
	if($verbose){$re=false;}
	if($verbose){echo "find_dates_and_times_es\n";}
	
	// FIXME: this messes up positions for $fullResult :-( 
	// Quick fix: also call it on the text you will change.
	$src=dlib_html_entities_decode_text($src,true);
	if(strlen($src)==0){return [];}

	// all of this is static / constant, only compute it once
	if($re===false)
	{
		list($monthList,$weekDayList)=date_name_elements_es();

		// create index for finding month-number 
		// from unaccented lowercase monthname
		$monthIndex=[];
		foreach($monthList as $m=>$n)
		{
			$monthIndex[dlib_remove_accents(mb_strtolower(str_replace('.','',$m),'UTF-8'))]=$n;
		}

		// create index for finding weekDay number
		// from unaccented lowercase weekday name
		$weekDayIndex=[];
		foreach($weekDayList as $m=>$n)
		{
			$weekDayIndex[dlib_remove_accents(mb_strtolower(str_replace('.','',$m),'UTF-8'))]=$n;
		}


		/******************** helper regular expressions *******************/
		$letterOrNumber='0-9a-z\p{L}';
		$endWord='(?!['.$letterOrNumber.'])';
		$beginWord='(?<!['.$letterOrNumber.'])';
		
		// month RE
		$allMonths=array_keys($monthList);
		$allMonths=array_merge($allMonths,array_map('dlib_remove_accents',$allMonths));
		$allMonths=str_replace('.','',$allMonths);
		rsort($allMonths);// longer matches have to come first!!
		$reAnyMonth='(?P<month>'.implode('|',array_unique($allMonths)).')(\.|\b)';
		$reAnyMonth4=str_replace('<month>','<month4>',$reAnyMonth);

		// weekday RE
		$allWeekDays=array_keys($weekDayList);
		$allWeekDays=str_replace('.','',$allWeekDays);
		rsort($allWeekDays);// longer matches have to come first!!
		$reAnyWeekday='('.implode('|',array_unique($allWeekDays)).')(\.|\b)';
		
		// note: <cite> is used for pagewatch diff highlight
		$spaces='(['." ".// this last string is not a space, it is a UTF8 nbsp
			',\t[:space:]]|</?(br|strong|b|em|span|font|cite)[^>]*/?>)*';
		$daynumRE="(?<![0-9])(?P<daynum>[0-2][0-9]|3[01]|[1-9]|1o|1°|1ero|primero|1[ \n]*<sup> *er *</sup>)";
		$daynumRE0=str_replace('<daynum>','<daynumrange0>',$daynumRE);;
		$daynumRE4=str_replace('<daynum>','<daynum4>',$daynumRE);;
		$timeRE=get_time_re_es($spaces,$beginWord,$endWord);
		$yearRE='(?P<year>\b(2\.?0|19|18|17)[0-9][0-9]\b|\b[012][0-9]\b(?!(é|è|[[:space:] ]*(h|:[[:space:] ]*[0-9][0-9]|[–-][[:space:] ]*[0-9]))))';
		$yearRE2=str_replace('<year>','<year2>',$yearRE);
		$yearRE4=str_replace('<year>','<year4>',$yearRE);

		// huge regular expression for matching dates and times
		$re="@".
			'(?![  \n\t<>,])'.// do not begin by whitespace (strange?... otherwise crashes)
			"((?P<weekday>\b$reAnyWeekday)$spaces)?".
			// alternative dates: 
			// alternative date 1: daynum month year
			"(".
			"(?P<daynumrange>$daynumRE0$spaces(al|y)$spaces)?".
			"$daynumRE$spaces".
			"(de$spaces)?". // spanish "12 *de* junio"
			"\b$reAnyMonth$spaces".
			"(del?$spaces(a[nñ]o$spaces)?)?". // spanish "12 de junio *de* 2006" or "17 de diciembre *del año* 2014"
			"$yearRE?".
			"|".
			// alternative date 2: d/m/Y 
			"((?<![0-9/])(?P<daynum2>[0-2][0-9]|3[01]|[1-9])/(?P<month2>0[1-9]|1[012]|[1-9])(?![a-z_-])((/".$yearRE2.")?))(?![0-9/])".
			"|".
			// alternative date 3: YYYY-MM-DD (computer) 
			"((?<![0-9–-])(?P<year3>(20|19)[0-9][0-9]) ?[-–] ?(?P<month3>0[0-9]|1[012]) ?[-–] ?(?P<daynum3>[0-2][0-9]|3[01])(?![0-9–-]))".
			"|".
			// alternative date 4: month daynum year (USA influence sometimes seen in Latin America - Mexico)
			"\b$reAnyMonth4$spaces".
			"$daynumRE4$spaces".
			"(del?$spaces(a[nñ]o$spaces)?)?". // spanish "12 de junio *de* 2006" or "17 de diciembre *del año* 2014"
			"($yearRE4|/(?= *[0-9][0-9]?[:h])|)".
			")".
			"(".$spaces.
			// text before time
			"(desde$spaces"."las$spaces|a$spaces"."las$spaces|de$spaces|".'[-–]'."$spaces"."de$spaces|".'[-–]'."$spaces)?".
			'(?P<time>'.$timeRE.')'.
			")?".
			"@iuS";
		if($verbose){echo "re:$re\n";}
	}

	$bigMatchRes=preg_match_all($re,$src,$pregMatches,
								($fullResult ? 
								 PREG_OFFSET_CAPTURE : 
								 PREG_PATTERN_ORDER));
	//echo $re."\n";
	if($verbose){echo "preg_match_all return value:";var_dump($bigMatchRes);}
	if(!$bigMatchRes){return [];}

	// if full result  (with pos), then rebuild an  array like the one
	// without full result
	if($fullResult)
	{
		$matches=[];
		foreach($pregMatches as $key => $val)
		{
			foreach($val as $key1 => $val1)
			{
				if($val1===''){$matches[$key][$key1]='';}
				else{$matches[$key][$key1]=$val1[0];}
			}
		}
	}
	else
	{
		$matches=&$pregMatches;
	}
	
	if($verbose)
	{
		foreach($matches as $k=>$ma){$m=$ma[0];if($m!==''){echo "$k : \"$m\"\n";}}
	}
	
	$timestamps=[];
	// compute a timestamp for each matching date and time.
	foreach($matches[0] as $nb => $match)
	{
		$weekday=$matches['weekday'][$nb];
		$daynum =$matches['daynum' ][$nb].$matches['daynum2' ][$nb].$matches['daynum3' ][$nb].$matches['daynum4' ][$nb];
		$daynumrange0=$matches['daynumrange0'][$nb];
		$month  =$matches['month'  ][$nb].$matches['month2'  ][$nb].$matches['month3'  ][$nb].$matches['month4'  ][$nb];
		$year   =$matches['year'   ][$nb].$matches['year2'   ][$nb].$matches['year3'   ][$nb].$matches['year4'   ][$nb];
		$hour   =$matches['hour1'  ][$nb];
		$minute =$matches['minutes'][$nb];
		$amPm   =$matches['am_pm1' ][$nb].$matches['am_pm2'  ][$nb].
			     $matches['am_pm3' ][$nb];
		$noon   =$matches['noon'   ][$nb];

		if($verbose)
		{
			echo '$weekday:'.		 $weekday."\n"; 
			echo '$daynum :'.		 $daynum ."\n"; 
			echo '$daynumrange0 :'.	 $daynumrange0 ."\n"; 
			echo '$month  :'.		 $month	 ."\n";		
			echo '$year	  :'.		 $year	 ."\n"; 
			echo '$hour	  :'.		 $hour	 ."\n"; 
			echo '$minute :'.		 $minute ."\n"; 
			echo '$amPm	  :'.		 $amPm	 ."\n"; 
			echo '$noon	  :'.		 $noon	 ."\n"; 
			echo "****************\n";
			echo "match  : $match\n";
		}

		$year=str_replace('.','',$year);

		if(mb_strtolower($daynum)=='1ero' || mb_strtolower($daynum)=='primero' ||
		   preg_match('@^1[^0-9]+@',$daynum)){$daynum=1;}

		if(mb_strtolower($daynumrange0)=='1er' || mb_strtolower($daynumrange0)=='premier' ||
		   preg_match('@^1[^0-9]+@',$daynumrange0)){$daynumrange0=1;}

		if($daynum <= 0 || $daynum>31)
		{
			if($verbose)echo "bad daynum:$daynum\n";
			continue;
		}

		if($daynumrange0 <= 0 || $daynumrange0>31)
		{
			if($verbose)echo "bad daynumrange0:$daynumrange0\n";
			$daynumrange0=false;
		}

		if(strlen($daynumrange0)==0){$daynumrange0=false;}

		if(strlen($year)===4 && $year<1971)
		{
			if($verbose)echo "Ignore very old dates\n";
			continue;			
		}

		if($year>=2038)
		{
			if($verbose)echo "Ignore dates 2038 or later.\n"; // FIXME tmp year 2038 workaround. (4 bytes overflow)
			continue;			
		}

		if(preg_match('@^'.$spaces.'[0-9]+\.[0-9]+'.$spaces.'$@',$match))
		{
			if($verbose)echo "rejected num.num : this causes too many false positives\n";
			continue;
		}

		// compute month number from month name 
		$monthNum=0;
		if(preg_match('@^[0-9]+$@',$month)){$monthNum=$month;}
		else
		{
			$ualcMonth=dlib_remove_accents(mb_strtolower(str_replace('.','',$month),'UTF-8'));
			if(!isset($monthIndex[$ualcMonth])){fatal("bad month: '$month' !\n");}
			$monthNum=$monthIndex[$ualcMonth];
		}
		if($monthNum <= 0 || $monthNum>12){fatal("bad monthNum:$monthNum\n");}

		// compute weekday number from weekday name 
		$ualcWeekDay=dlib_remove_accents(mb_strtolower(str_replace('.','',$weekday),'UTF-8'));
		$weekdayNum=val($weekDayIndex,$ualcWeekDay,false);

		// guess year if it is missing (choose year with closest date to now)
		if($year==''){$year=find_date_time_guess_year($daynum,$monthNum);}
		// determine time
		if($minute===""){$minute=0;}

		if(strlen($noon)){$hour=12;}
		if($hour  ===""){$hour=3;$minute=33;}
		else
		{
			// Apply AM/PM to time
			$amPm=mb_strtolower($amPm);
			if(strlen($amPm)>0 && mb_stripos($amPm,'a',0,'UTF-8')!==false && $hour==12){$hour='00';}
			if(strlen($amPm)>0 && mb_stripos($amPm,'a',0,'UTF-8')===false && $hour <12){$hour+=12;}
			// for early hours PM is assumed (unless it starts with a zero)
			if(strpos($hour,'0')!==0 && $hour<=9 && strlen($amPm)==0){$hour+=12;}
		}

		if(!checkdate($monthNum,$daynum,$year))
		{
			if($verbose)echo "rejected $year-$monthNum-$daynum : invalid gregorian date (checkdate)\n";
			continue;
		}

		// now build the timestamp
		$ts=mktime($hour,$minute,0,$monthNum,$daynum,$year);
		if($verbose)echo "res: ".date('r',$ts)."  ($ts)\n";

		// if weekday is available, we can check if day is ok
		if($ts==0)
		{
			if($verbose)echo "bad date/time!\n";
		}
		else
		if($weekdayNum!==false && day_of_week($ts)!=($weekdayNum-1))
		{
			if($verbose)echo "WEEKDAY check REJECTED!\n";
		}
		else
		{
			if($daynumrange0===false)
			{
				if($fullResult)
				{
					$timestamps[]=[$ts,
								   $pregMatches[0][$nb][0],
								   $pregMatches[0][$nb][1]];
				}
				else
				{$timestamps[]=$ts;}
			}
			else
			{
				// special case for date ranges("Du 23 au 27 mars"):
				// Create two dates.
				$ts0=mktime($hour,$minute,0,$monthNum,$daynumrange0,$year);
				if($verbose)echo "daynumrange0: ".date('r',$ts0)."  ($ts0)\n";

				if($fullResult)
				{
					$timestamps[]=[$ts0,
								   $pregMatches['daynumrange0'][$nb][0],
								   $pregMatches['daynumrange0'][$nb][1]];
					$timestamps[]=[$ts,
								   $pregMatches['daynum'][$nb][0],
								   $pregMatches['daynum'][$nb][1]];
				}
				else
				{
					$timestamps[]=$ts0;
					$timestamps[]=$ts;
				}
				
			}
		}
	}
	return $timestamps;
}

function find_date_and_time_es($src,$verbose=false)
{
	$timestamps=find_dates_and_times($src,$verbose);
	if(count($timestamps)!=1){return false;}
	return $timestamps[0];
}


function find_times_es($src,$format='h',$verbose=false)
{
	$spaces='([ '." ".// this last string is not a space, it is a UTF8 nbsp
		',\n\t]|&nbsp;|</?(br|strong|b|em|span) ?/?>|)*';
	$letterOrNumber='0-9a-z\p{L}';
	$endWord='(?!['.$letterOrNumber.'])';
	$beginWord='(?<!['.$letterOrNumber.'])';
	$timeRE=get_time_re_es($spaces,$beginWord,$endWord);
	if(!preg_match_all('@'.$timeRE.'@isu',$src,$matches)){return [];}
	if($verbose)print_r($matches);
	$times=[];
	// compute a time for each matching date and time.
	foreach($matches[0] as $nb => $match)
	{
		$hour   =$matches['hour1' ][$nb].$matches['hour2'][$nb];
		$minute =$matches['minutes'][$nb];
		$amPm   =$matches['am_pm1'][$nb].$matches['am_pm2'][$nb].$matches['am_pm3'][$nb];

		if(preg_match('@[^0-9]@',$hour)   ||
		   preg_match('@[^0-9]@',$minute) ||
		   $hour   < 0 || $hour  >24 || 
		   $minute < 0 || $minute>59)
		{
			$ts='invalid';
		}
		else
		{
			if(strlen($amPm)>0 && mb_stripos($amPm,'a',0,'UTF-8')===false && $hour<12){$hour+=12;}
			switch($format)
			{
			case 'h': $ts=intval($hour).'h'.
						  ($minute!=0 ? sprintf('%02d',$minute) : '');
			case ':': $ts=sprintf('%02d:%02d',$hour,$minute);break;
			default:  fatal("invalid time format requested");
			}
		}
		$times[]=$ts;
	}
	return $times;
}

function get_time_re_es($spaces,$beginWord,$endWord)
{
	$textBeforeTimePhrases=
		[
			'at',
			'around',
			'from',
			'begining at',
		];
	// make a more general regular expresion out of each phrase
	$textBeforeTimePhrasesRE=[];
	foreach($textBeforeTimePhrases as $p)
	{
		$p=str_replace(' ',' *',$p);
		$p=str_replace(',',',?',$p);
		$p=' *,? *'.$p.' *,? *';
		$textBeforeTimePhrasesRE[]=$p;
	}
	//print_r($textBeforeTimePhrasesRE);
	//$textBeforeTime1='(?P<text_before_time1> *,? *στο Στέκι *, *στις *| *, γύρω στις *| *,? *γύρω στις *| *stis *|στις *|στισ| *από *τις *| *και ώρα)';
	$textBeforeTime1='(?P<text_before_time1>'.implode('|',$textBeforeTimePhrasesRE).')';
	//echo "text_before_time:$textBeforeTime1\n";fatal("\n");
	$textBeforeTime2=str_replace('text_before_time1','text_before_time2',$textBeforeTime1);
	$textBeforeTime3=str_replace('text_before_time1','text_before_time3',$textBeforeTime1);
	$amPm1='(?P<am_pm1>a\.?m\.?|p\.?m\.?)';
	$amPm2=str_replace('am_pm1','am_pm2',$amPm1);
	$amPm3=str_replace('am_pm1','am_pm3',$amPm1);
	$timeRE=
		"(".
		$textBeforeTime1.'?'.$spaces.
		''.$beginWord.'(?P<hour1>[0-1][0-9]|2[0-4]|[1-9])'. // begin with number
		'([[:space:] ]*(h)[[:space:] ]*|[:.])'.            // " h " or ":" or "."
		'((?P<minutes>[0-5][0-9])|)'.$spaces.    // 
		'('.$amPm1.'|horas?|hs|h)?'.
		")".
		"|".
		// time version 2: simple hour number :
		// needs time designating text before or after
		$textBeforeTime2.'?'.$spaces.
		'(?P<hour2>[0-1][0-9]|2[0-4]|[1-9])'.$spaces.
		'(?(text_before_time2)'.// if text_before_time1 is set
		// then text after time is optional
		$amPm2.'?'.
		// otherwise it is necessary
		'|'.$amPm3.
		')'.
		'|'.
		$textBeforeTime3.'?'.$spaces.'(?P<noon>mediodía)'
		;
	return $timeRE;
}

// a list of different representations of month names and weekday names
function date_name_elements_es()
{
	static $res=false;
	if($res!==false){return $res;}

	/******************** months ************************/
	$months=[
		'enero'     =>1,	
		'febrero'	  =>2,	
		'marzo'	  =>3,	
		'abril'	  =>4,	
		'mayo'	  =>5,	
		'junio'	  =>6,	
		'julio'	  =>7,	
		'agosto'	  =>8,	
		'septiembre'=>9,	
		'octubre'	  =>10, 
		'noviembre' =>11, 
		'diciembre' =>12, 
		'Ene.'		   =>1,
		'Feb.'		 =>2,
		'Mar.'		 =>3,
		'Abr.'      =>4,
		'May.'      =>5,
		'Jun.'		 =>6,
		'Jul.'		 =>7,
		'Ago.'		 =>8,
		'Sept.'	 =>9,
		'Sep.'	      =>9,
		'Oct.'		 =>10,
		'Nov.'		 =>11,
		'Dic.' 	 =>12,
			];

	/******************** weeks ************************/

	$weekDays=[
		'lunes'     => 1,
		'martes'	=> 2,
		'miércoles'	=> 3,
		'jueves'	=> 4,
		'viernes'	=> 5,
		'sábado'	=> 6,
		'domingo'	=> 7,
		'Lun.'		=> 1,
		'Mar.'		=> 2,
		'Mié.'		=> 3,
		'Jue.'		=> 4,
		'Vie.'		=> 5,
		'Sáb.'		=> 6,
		'Dom.'		=> 7,
			  ];
	$res=[$months,$weekDays];
	return $res;
}


?>