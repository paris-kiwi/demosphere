<?php

require_once 'test-find-date-time-common.php';
require_once 'test-dlib.php';

function test_find_date_time_ca()
{
	require_once 'dlib/tools.php';
	require_once 'dlib/find-date-time/find-date-time.php';
	require_once 'dlib/find-date-time/find-date-time-es.php';
	setlocale(LC_TIME, "ca_ES.utf8");
	test_list_find_date_time_ca();
	test_find_times_ca();
	test_random_find_date_time_ca(true);
}

// Automatically generates a large number of random dates and times
// using predefined patterns.
function test_random_find_date_time_ca($verbose=false)
{
	$ok=0;
	$failed=0;
	// try different dates
	for($i=0;$i<100;$i++)
	{
		if($i<50)
		{$dateTS=mktime(0,0,0,1,1,mt_rand(2000,2020))+mt_rand(0,366*24*3600);}
		else{$dateTS=time()+mt_rand(-366*24*3600,366*24*3600);}
		$m=date('m',$dateTS);
		$d=date('d',$dateTS);
		$y=date('Y',$dateTS);
		$dateTS=mktime(0,0,0,$m,$d,$y);
		if($verbose)echo "date:".strftime('%A %e %B %Y - %R',$dateTS)."\n";
		for($t=0;$t<20;$t++)
		{
			$hasTime=true;
			if(true ){$h=mt_rand(0,23);$min=mt_rand(0,59);}
			if($t<15){$h=mt_rand(0,23);$min=30;}
			if($t<10){$h=mt_rand(0,23);$min= 0;}
			if($t==0){$h=			 3;$min=33;$hasTime=false;}
			$hasTime=intval($hasTime);

			$ts=mktime($h,$min,0,$m,$d,$y);

			// has min, has time, has year
			//       y       y      y 
			//       y       y      n   
			//       y       n      y   X
			//       y       n      n   X
			//       n		 y		y
			//		 n		 y		n 
			//		 n		 n		y
			//		 n		 n		n 
			for($type=0;$type<4;$type++)
			{
				$hasMin =$type%2;
				$hasYear=($type>>1)%2;
				if(!$hasTime && $hasMin){continue;}
				if($hasTime && !$hasMin  && $min!=0){continue;}
				if(!$hasYear && abs($ts-time())>(3600*24*30*5)){continue;}

			
				$formats=['%A %e %B %Y - %R',
						  '%e %b %Y - %R',
						  '%d/%m/%Y - %R'];
				if(!$hasYear)
				{
					$formats=array_merge($formats,['%A %e %B - %R',
												   '%d/%m - %R']);
				}

				if(!$hasTime && !$hasYear)
				{
					$formats=array_merge($formats,['%e %B',
												   '%d/%m']);
				}
				if(!$hasTime)
				{
					$formats=array_merge($formats,['%A %e %B %Y']);
					$f0=$formats;
					foreach($f0 as $k=>$f){$formats[$k]=str_replace(' - %R','',$f);}
				}
				foreach($formats as $format)
				{
					$string=strftime($format,$ts);

					//				echo "m:$hasMin t:$hasTime y:$hasYear ".
					//					"time:".strftime('%A %e %B %Y - %R',$ts)." :::: ".
					//					$string."\n";
					$contexts=['sdf sfd %s sdfsd',
							   '%s',
							   '%s csdsdf ',
							   'fsdf %s'];
					foreach($contexts as $context)
					{
						$fstring=$string;
						if(!(mt_rand()%2)){$fstring=str_replace(':','h',$fstring);}
						//if(!(mt_rand()%4)){$fstring=str_replace('-','des',$fstring);}
						if(!(mt_rand()%4)){$fstring=str_replace('-','a las',$fstring);}
						if(!(mt_rand()%4)){$fstring=str_replace('-',',',$fstring);}
						if(!(mt_rand()%4)){$fstring=dlib_remove_accents($fstring);}
						if(!(mt_rand()%4)){$fstring=mb_strtoupper($fstring,'UTF-8');}
						if(!(mt_rand()%4)){$fstring=str_replace('-','&nbsp;',$fstring);}
						else
						if(!(mt_rand()%4)){$fstring=ent($fstring);}
						//echo 'pre-final string:'.$fstring."\n";
						$fstring=str_replace('%s',$fstring,$context);
						//if(!(mt_rand()%1000))echo 'final string:'.$fstring."\n";
						$res=find_dates_and_times($fstring);
						$rts=val($res,0,false);
						if(count($res)!=1 || $rts!=$ts)
						{
							echo "FAILED find_dates_and_times test for $ts \"$fstring\"\n";
							var_dump($res);
							echo "\n";
							$res=find_dates_and_times($fstring,true);
							fatal("abort after fail\n");
							$failed++;
						}
						else{$ok++;}
					}
				}
			}
		}
	}
	echo "test_random_find_date_time_ca: ok:$ok failed:$failed\n";
	test_equals($failed,0);
}

// Test a list of predefined strings.
function test_list_find_date_time_ca()
{
	$tests=[
		['dilluns, 7 de març de 2016 - 18h','7/3/2016 18:00'],
		['3/2/2000'                     ,'3/2/2000 3:33'],
		['3/02/2000'                    ,'3/2/2000 3:33'],
		['3-2-2000'                     ,'3/2/2000 3:33'],
		['3-02-2000'                    ,'3/2/2000 3:33'],
		['3 de febrer de 2000'          ,'3/2/2000 3:33'],
		['3 de febrer del 2000'         ,'3/2/2000 3:33'],
		['dijous, 3 de febrer de 2000' ,'3/2/2000 3:33'],
		['dijous, 3 de febrer del 2000','3/2/2000 3:33'],
//3,4,5 de Febrer de(l) 2000
//3-5 de Febrer de(l) 2000
//Respecto a las horas, se me ocurre:
//12:00h
//12:00-13:00h
//12h
//12-13h
//12h-13h


// Also add spanish tests:
		['jueves  9 mayo 2019 - 19h30','9/5/2019 19:30'],
		['12 de junio de 2006','12/06/2006 3:33'],
		['12 de junio del 2006','12/06/2006 3:33'],
		['16 de noviembre a las 15:30 h','16/11 15:30'],
		['16 de noviembre 2006 a las 22 h','16/11/2006 22:00'],
		['16 de noviembre 2006 a las 22.30 h','16/11/2006 22:30'],
		['10 de octubre a las 20.00 hs','10/10 20:00'],
		['17 de diciembre del año 2014','17/12/2014 3:33'],
		['noviembre 18 2015 a las 8:00 am','18/11 8:00'],
		['noviembre 18 2015 a las 8:00 pm','18/11 20:00'],
		['noviembre 18 a las 8:00 p.m.','18/11 20:00'],
		['enero 5 de 2012"','5/1/2012 3:33'],
		['enero 5 de 2.012"','5/1/2012 3:33'],
		['primero de enero de 2012"','1/1/2012 3:33'],
		['1ero de marzo de 2012"','1/3/2012 3:33'],
		['1° de marzo de 2012"','1/3/2012 3:33'],
		['1o de marzo de 2012"','1/3/2012 3:33'],
		['noviembre 20/5:00 am','20/11 5:00'],
		['noviembre 4/5:00 pm','4/11 17:00'],
		['noviembre 18/8:30 am - noviembre 19/5:00 pm',['18/11 8:30','19/11 17:00']],


		   ];
	test_list_find_date_time('es',$tests);
}


function specific_test_find_dates_and_times_ca()
{
	$text=file_get_contents('problem-text');
	echo strlen($text)."\n";
	$res=find_dates_and_times($text,true);
	print_r($res);
}

function test_find_times_ca()
{
	$tests=['3am'=>['03:00'],
			'3AM'=>['03:00'],
			'3pm'=>['15:00'],
			'3PM'=>['15:00'],
			'3 P.M.'=>['15:00'],
			'3 A.M.'=>['03:00'],
			'3.30pm'=>['15:30'],
			'3.30am'=>['03:30'],
			'12:00h'=>['12:00'],
			'12:00-13:00h'=>['12:00','13:00'],
			'12h'=>['12:00'],
			// FIXME: '12-13h'=>['12:00','13:00'],
			'12h-13h'=>['12:00','13:00'],
			];
	test_find_times('es',$tests);
}

?>