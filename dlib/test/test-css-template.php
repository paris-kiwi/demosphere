<?php

require_once 'test-dlib.php';


function test_css_template()
{
	return; // FIXME: this needs rewrite. css_template and sprite need database
	require_once 'dlib/testing.php';
	require_once 'dlib/tools.php';
	require_once 'dlib/database.php';
	require_once 'dlib/css-template.php';
	require_once 'dlib/sprite.php';
	require_once 'dlib/variable.php';

	global $sprite_config,$css_template_config;
	$css_template_config=
		[
		 'dest_dir'=>'files/css/templates',
		 ];
	$sprite_config=
		[
		 'enable'=>true,
		 'dest_dir'=>'/tmp',
		 'sprites'=>
		 [
		  'out1.png'=>['src_images'=>['dlib/test/t1.png','dlib/test/t2.png']],
		  ]
		 ];

	var_dump(css_template('demosphere/css/demosphere-frontpage.tpl.css'));

	variable_delete(false,'sprite');
	//css_template_tile_sprite_images(['srcImages'=>['t1.png','t2.png']],'/tmp/out1.png');
	global $css_template_context;
	$css_template_context['cssFileDir']='dlib/test';
	var_dump(css_sprite("t1.png"));
	//test_equals(css_template_compile('@@sprite("t1.png")'),'/tmp/out1.png');
	die("\n");

//  
//  .something
//  {
//     background-image: url(@@sprite('icons/carpool.png'));
//  }
//  
//  .xyz
//  {
//     background-image: url(small/edit.png);
//  }
//
//
//	
//  
//  


	// die("\n");
	// die("\n");

	//  **

	test_equals(css_template_eval('A@@(10+21)B'),'A31B');
	test_equals(css_template_eval('<? $x=123; ?>A$x B'),'A123 B');
	test_equals(css_template_eval('A@@(100+10*(.31+.9))B(X)'),'A112.1B(X)');
	test_equals(css_template_eval('A@@round(100+10*(.31+.9))B(X)'),'AC112B(X)');
	test_equals(css_template_eval('@@darken("#a0a0a0",.1)'),'#a0a0a0');
	test_equals(css_template_eval('@@desaturate("#ff0000",100)'),'#808080');
	test_equals(css_template_eval('@@fadeout("#ff0000",50)'),'rgba(255,0,0,0.5)');
	test_equals(css_template_eval('@@spin("#ff0000",180)'),'#00ffff');
	test_equals(css_template_eval('@@mix("#ff0000","#0000ff",50)'),'#800080');
}

function css_round($x)
{
	return 'C'.round($x);
}

?>