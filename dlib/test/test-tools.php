<?php

require_once 'test-dlib.php';

function test_tools()
{
	// *** test ical_parse (lib/iCalcreator/)
	require_once 'lib/iCalcreator/iCalcreator.php';
	ini_set('date.timezone','Europe/Paris');
	$ics1="BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//test1calname//agenda::fr
METHOD:PUBLISH
X-WR-CALNAME:test1calname2
X-WR-CALDESC:test1desc
X-WR-RELCALID:123456
BEGIN:VEVENT
UID:test1event1@demosphere.net
DTSTART:20171206T173000Z
DTEND:20171206T210000Z
DTSTAMP:20171201T1035Z
TZID:Europe-Paris
SUMMARY:test1event1summary
DESCRIPTION:test1event1description
STATUS:CONFIRMED
LOCATION:test1event1location
GEO:48.7913026;2.3968105
URL:https://demosphere.net/test1event1url
LAST-MODIFIED:20171201T1045Z
ORGANIZER:test1event1organizer
END:VEVENT
END:VCALENDAR";
	$events=ical_parse($ics1,'text',strtotime("january 1st 2017"),strtotime("january 1st 2018"),"test1id");

	$parsedOk=[
					'dtstart'     => '1512581400' ,
					'summary'     => 'test1event1summary' ,
					'location'    => 'test1event1location' ,
					'description' => 'test1event1description' ,
					'url'         => 'https://demosphere.net/test1event1url' ,
					'uid'         => 'test1event1@demosphere.net' ,
				];

	test_equals(count($events),1);
	test_equals(array_intersect_key($events[0],array_flip(['dtstart','summary','location','description','url','uid'])),$parsedOk);

	$ics2=$ics1;
	$ics2=preg_replace("@BEGIN:VEVENT.*END:VEVENT\n@s",'',$ics2);
	$events=ical_parse($ics2,'text',strtotime("january 1st 2017"),strtotime("january 1st 2018"),"test1id");
	test_equals($events,[]);

	$events=ical_parse($ics1,'text',strtotime("December 8 2017"),strtotime("january 1st 2018"),"test1id");
	test_equals($events,[]);
	$events=ical_parse($ics1,'text',strtotime("December 1 2017"),strtotime("December 5 2017"),"test1id");
	test_equals($events,[]);

	$events=ical_parse('zzzz','text',strtotime("December 1 2017"),strtotime("December 5 2017"),"test1id");
	test_equals($events,false);

	ini_set('date.timezone','America/New_York');
	$events=ical_parse($ics1,'text',strtotime("january 1st 2017"),strtotime("january 1st 2018"),"test1id");
	test_equals(array_intersect_key($events[0],array_flip(['dtstart','summary','location','description','url','uid'])),$parsedOk);

	ini_set('date.timezone','Europe/Paris');
	// test recurring event
	$ics3="BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//test2calname//agenda::fr
METHOD:PUBLISH
X-WR-CALNAME:test2calname2
X-WR-CALDESC:test1desc
X-WR-RELCALID:123456
BEGIN:VEVENT
UID:test2event1@demosphere.net
DTSTAMP:20160101T123456Z
DTSTART:20160105T100000Z
DTEND:20160105T110000Z
SUMMARY:test2event1summary
CLASS:PUBLIC
RRULE:FREQ=MONTHLY;BYDAY=TU;BYSETPOS=1
CREATED:20160101T123456Z
LAST-MODIFIED:20160101T123456Z
END:VEVENT
END:VCALENDAR";
	$events=ical_parse($ics3,'text',strtotime("November 1st 2017"),strtotime("December 31 2017"),"test1id");	
	test_equals(count($events),2);
	test_equals(array_intersect_key($events[0],array_flip(['dtstart','summary'])),['dtstart'=>1510045200,'summary'=>'test2event1summary']);


	// *** test unique_basename
	$tmp=tempnam('/tmp','test-tools-');
	unlink($tmp);
	mkdir($tmp);
	touch($tmp.'/'.'zzz.abc');
	$res1=dlib_unique_basename($tmp,'xxx');
	test_equals($res1,'xxx');
	$res2=dlib_unique_basename($tmp,'zzz');
	test_different($res2,'zzz');

	// *** test html_entities_decode_text
	test_equals(dlib_html_entities_decode_text('&lt;'),'&lt;');
	test_equals(dlib_html_entities_decode_text('&oplus;'),'⊕');
	test_equals(dlib_html_entities_decode_text('&#893;'),'ͽ');
	test_equals(dlib_html_entities_decode_text('&#62;'),'&#62;');
	test_equals(dlib_html_entities_decode_text('&#34;&#38;&#39;&#60;&#62;'),'&#34;&#38;&#39;&#60;&#62;');
	test_equals(dlib_html_entities_decode_text('&#33;&#35;&#40;&#59;&#61;&#63;'),'!#(;=?');
	test_equals(dlib_html_entities_decode_text('&#931;&#0931;&#x3A3;&#x03A3;&#x3a3;'),'ΣΣΣΣΣ');
	test_equals(dlib_html_entities_decode_text('<p>cet &eacute;té &lt;  </p>'),'<p>cet été &lt;  </p>');


}
?>