<?php

require_once 'test-dlib.php';

function test_wordlist()
{
	wordlist_build_simplified_with_pos("<a>b");

	// *** simplify text with pos
	list($words,$pos)=wordlist_build_simplified_with_pos(" abcd - efg hi j ");
	test_equals(implode(' ',$words),dlib_simplify_text(" abcd - efg hi j "));
	test_equals($pos[0][0],1);
	test_equals($pos[0][1],5);
	test_equals($pos[3][0],15);
	test_equals($pos[3][1],16);
	list($words,$pos)=wordlist_build_simplified_with_pos(" <abcd xx> - </abcd> efg hi &lt; &#123; j ",true);
	test_equals(implode(' ',$words),dlib_simplify_text(" <abcd xx> - </abcd> efg hi &lt; &#123; j ",true));

	$str="àb­cdé xyz";
	list($words,$pos)=wordlist_build_simplified_with_pos($str);
	test_equals(implode(' ',$words),dlib_simplify_text($str));	

}
?>