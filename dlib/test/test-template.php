<?php

error_reporting(E_STRICT | E_ALL);
if(!isset($_SERVER['SERVER_NAME'])){template_command_line_test();}

function template_command_line_test()
{
	setlocale(LC_TIME, "fr_FR.utf8");
    ini_set("include_path", ini_get("include_path").":..:../..");
	require_once 'dlib/tools.php';
	require_once 'dlib/testing.php';
	test_template();
}

function test_template()
{
	require_once 'template.php';
	require_once 'dlib/tools.php';
	global $template_dir;

	system('rm -rf /tmp/test-template');
	mkdir('/tmp/test-template');
	chdir('/tmp/test-template');
	mkdir('compiled');
	$template_dir='compiled';

	// most basic simple text
	file_put_contents('test1','a5Rt');
	$ret=template\render('test1',[],true);
	//echo "res:|$ret|\n";
	test_equals($ret,'a5Rt');

	// basic simple text and php
	file_put_contents('test1','X<?php echo "A" ?>Y');
	$ret=template\render('test1',[],true);
	test_equals($ret,'XAY');

	// basic simple text and short php tag
	file_put_contents('test1','X<?= "A" ?>Y');
	$ret=template\render('test1',[],true);
	test_equals($ret,'XAY');

	// basic simple text and simple variable
	file_put_contents('test1','X<?= $x ?>Y');
	$ret=template\render('test1',['x'=>123],true);
	test_equals($ret,'X123Y');

	// basic simple text and simple unescaped variable
	file_put_contents('test1','X<?= $x ?>Y');
	$ret=template\render('test1',['x'=>'a>b'],true);
	test_equals($ret,'Xa>bY');

	// basic simple text and simple escaped variable
	file_put_contents('test1','X<?: $x ?>Y');
	$ret=template\render('test1',['x'=>'a>b'],true);
	test_equals($ret,'Xa&gt;bY');

	// basic simple text and simple variable with ending separator
	file_put_contents('test1','X$x~Y');
	$ret=template\render('test1',['x'=>123],true);
	test_equals($ret,'X123Y');

	// simple import
	file_put_contents('test1','X<? import("test2") ?>Y');
	file_put_contents('test2','AB');
	$ret=template\render('test1',[],true);
	test_equals($ret,'XABY');

	// import and variable
	file_put_contents('test1','X<? import("test2") ?>Y');
	file_put_contents('test2','A<?= $x ?>B');
	$ret=template\render('test1',['x'=>'Z'],true);
	test_equals($ret,'XAZBY');

	// simple block
	file_put_contents('test1','X<? block_begin("myblock") ?>A<? block_end("myblock") ?>Y');
	$ret=template\render('test1',[],true);
	test_equals($ret,'XAY');

	// simple block override
	file_put_contents('test1','X<? block_begin("myblock") ?>A<? block_end("myblock") ?>Y<? block_begin("myblock") ?>B<? block_end("myblock") ?>Z');
	$ret=template\render('test1',[],true);
	test_equals($ret,'XBYZ');

	// double block override
	file_put_contents('test1','X<? block_begin("myblock") ?>A<? block_end("myblock") ?>Y<? block_begin("myblock") ?>B<? block_end("myblock") ?>Z<? block_begin("myblock") ?>C<? block_end("myblock") ?>W');
	$ret=template\render('test1',[],true);
	test_equals($ret,'XCYZW');

	// block in loop
	file_put_contents('test1','X<? for($i=0;$i<=3;$i++){block_begin("myblock");echo "A$i";block_end("myblock");} ?>Y');
	$ret=template\render('test1',[],true);
	test_equals($ret,'XA0A1A2A3Y');

	// extends (import + block override)
	file_put_contents('test1','X<? import("test2") ?><? block_begin("myblock") ?>V<? block_end("myblock") ?>Y');
	file_put_contents('test2','A<? block_begin("myblock") ?>W<? block_end("myblock") ?>B');
	$ret=template\render('test1',[],true);
	test_equals($ret,'XAVBY');
}

?>