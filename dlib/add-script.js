function add_script(u,as,cb)
{
	var s=document.createElement("script");
	s.type="text/javascript";
	if(u.indexOf("http")!==0  && u.indexOf("//")!==0){u=base_url+"/"+u;}
	if(as){s.async="async";}
	s.src=u;
	if(cb)
	{
		if(s.readyState)
		{
			s.onreadystatechange=function()
			{
				if(s.readyState=="loaded" || s.readyState=="complete"){s.onreadystatechange=null;cb();}
			};
		}else{s.onload=function(){cb();};}
	}
	document.getElementsByTagName("head")[0].appendChild(s);
}
