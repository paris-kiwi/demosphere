<?php

/** @defgroup dbobject_ui dbobject_ui
 * Automatically generated user interface for listing, adding and deleting DBObject's.
 *
 * **Module use**: DBObject, HtmlPageData, table-tools
 *  @{
 */

//! dbobject_ui_auto_setup(): Quick, premliminary setup of $dbobject_ui_config.
//!
//! Setup of $dbobject_ui_config is done in 3 steps, for performance:
//! 1) $dbobject_ui_config must be created by program during init phase.
//! 2) dbobject_ui_auto_setup() is called (only if necessary)
//! 3) dbobject_ui_class_setup() is called (only if necessary & for a specific class)
//!
//! ** $dbobject_ui_config **
//! You must list which classes use dbobject_ui and optionally give some information about them.
//! Optional information for each class:
//! - main_url           : base url for all this class
//! - setup-file		 : file that will be included before setup ('require_once')
//! - setup				 : user supplied function to further setup $dbobject_ui_config when it is actually needed for a specific class
//! - list_setup		 : called before displaying list
//! - list_action_url	 : function that returs an url for delete, edit, view, and view-field actions
//! - new				 : if the class has a non empty constructor, you must provide form items to create arguments for the constructor.
//! - new_alter			 : 
//! - field_desc		 : extra information on fields (title, html description)
//! - edit_form_alter	 : 
//! - view				 : user supplied function for customizing view
//! - delete_alter		 : 
//! - view_vars			 : 
//! - list_...           : see table_render() and add "list_" prefix
//!
//! Configuration example:\code 
//! $dbobject_ui_config=
//!    ['classes'=>
//!  	  [// simple class, no config
//!        'User'=>[], 
//!        // setup delegated to example_post_class_info(&$classInfo)
//!  	   'Post' =>['setup-file'=>'example-post.php'  ,'setup'=>'example_post_class_info' ],
//!        // other example
//! 	   'Article'=>['list_columns'=>function(&$columns){...},...]
//! \endcode


//! $dbobject_ui_config : configuration for dbobject_ui.
//! List of $dbobject_ui_config keys:
//! - classes
//! - urlToClass
//! -::-;-::-
$dbobject_ui_config;


function dbobject_ui_auto_setup()
{
	global $dbobject_ui_config;
	// map urlClass (class name in url, usually lowercase) to real class name.
	foreach($dbobject_ui_config['classes'] as $class=>&$cConfig)
	{
		$name=strtolower($class);
		$cConfig['urlClass']=$name;
		$cConfig['class']=$class;
		$dbobject_ui_config['urlToClass'][$name]=$class;
	}
}

//! Adds paths for each operation (list,add,edit,delete,view) for each class.
function dbobject_ui_add_paths(&$urls,&$options)
{
	global $dbobject_ui_config,$base_url;
	dbobject_ui_auto_setup();

	$urls[]=['path'=>'backend',
			 'roles' => ['admin'],
			 'function'=>'dbobject_ui_menu',
			];

	foreach($dbobject_ui_config['classes'] as $class=>$cConfig)
	{
		$urlClass=$cConfig['urlClass'];
		$path='backend/'.$urlClass;
		if(isset($cConfig['main_url']))
		{
			$path=substr($cConfig['main_url'],strlen($base_url)+1);
		}

		$urls['dbobject-ui-list-'.$urlClass]
			=['path'=>$path,
			  'roles' => ['admin'],
			  'function'=>function()use($class){return dbobject_ui_list($class);},
			 ];
		$urls['dbobject-ui-view-'.$urlClass]=
			['path'=>'@^'.$path.'/(\d+)$@',
			 'roles' => ['admin'],
			 'function'=>function($id)use($class){return dbobject_ui_view($class,$id);},
			];
		$urls['dbobject-ui-view-field-'.$urlClass]=
			['path'=>'@^'.$path.'/(\d+)/view-field$@',
			 'output'=>'json',
			 'roles' => ['admin'],
			 'function'=>function($id)use($class){return dbobject_ui_view_field($class,$id,$_GET['field']);},
			];
		$urls['dbobject-ui-edit-'.$urlClass]=
			['path'=>'@^'.$path.'/(\d+)/edit$@',
			 'roles' => ['admin'],
			 'function'=>function($id)use($class){return dbobject_ui_edit($class,$id);},
			];
		$urls['dbobject-ui-delete-'.$urlClass]=
			['path'=>'@^'.$path.'/(\d+)/delete$@',
			 'roles' => ['admin'],
			 'function'=>function($id)use($class){return dbobject_ui_delete($class,$id);},
			];
		$urls['dbobject-ui-add-'.$urlClass]=
			['path'=>$path.'/add',
			 'roles' => ['admin'],
			 'function'=>function()use($class){return dbobject_ui_edit($class);},
			];
	}
}

//! Display a list of all DBObject's and tables in $dbobject_ui_config and $dbtable_ui_config
function dbobject_ui_menu()
{
	global $dbobject_ui_config,$dbtable_ui_config,$currentPage,$base_url;
	$out='';
	$out.='<h1 class="page-title">'.t('Data backend').'</h1>';
	$out.='<p>'.t('Direct access to the data used by this site. Be very careful: changing or deleting information from here can cause your site to crash.').'</p>';

	// **** Display list of classes
	$out.='<h2>'.t('Classes').'</h2>';
	$out.='<ul>';
	$s=$dbobject_ui_config['classes'];
	ksort($s);
	foreach($s as $class=>$classInfo)
	{
		$out.='<li>';
		$out.='<a href="'.ent(val($classInfo,'main_url',$base_url.'/backend/'.$classInfo['urlClass'])).'">'.ent($class).'</a>';
		$out.='</li>';
	}
	$out.='</ul>';

	
	if(isset($dbtable_ui_config['tables']))
	{
		// **** Display list of tables
		$out.='<h2>'.t('Tables').'</h2>';
		$out.='<ul>';
		$s=$dbtable_ui_config['tables'];
		ksort($s);	
		foreach($s as $table=>$tableInfo)
		{
			$out.='<li>';
			$out.='<a href="'.ent(val($tableInfo,'main_url',$base_url.'/backend/'.$table)).'">'.ent($table).'</a>';
			$out.='</li>';
		}
		$out.='</ul>';
	}
	return $out;
}

//! Setup a specific class in $dbobject_ui_config['classes'][$class]
//! This is called only if necessary. 
//! It can be called more than once, but subsequent calls are ignored.
//! This calls user defined setup function if available.
function dbobject_ui_class_setup($class)
{
	global $dbobject_ui_config;
	$classInfo=&$dbobject_ui_config['classes'][$class];
	if(isset($classInfo['setup-is-ok'])){return $classInfo;}
	if(!isset($classInfo['urlClass'])){dbobject_ui_auto_setup();$classInfo=&$dbobject_ui_config['classes'][$class];}

	// **** call custom setup function / files
	if(isset($classInfo['setup-file'])){require_once $classInfo['setup-file'];}
	if(isset($classInfo['setup'     ])){$classInfo['setup']($classInfo);}
	$classInfo['setup-is-ok']=true;
	return $classInfo;
}

//! Display a table with a list of all objects of this class.
//! Note : this is just a wrapper around table_render()
function dbobject_ui_list($class,$classInfoOverride=[])
{
	global $dbobject_ui_config,$currentPage;
	$classInfo=dbobject_ui_class_setup($class);
	$classInfo=array_merge($classInfo,$classInfoOverride);

	if(isset($classInfo['list_setup'])){$classInfo['list_setup']();}

	dbobject_ui_tabs($class,false,'list');

	// Function that returns an url for delete, edit, view, and view-field actions
	$actionUrl=function($object,$action)use($classInfo)
		{
			global $base_url;
			return val($classInfo,'main_url',$base_url.'/backend/'.$classInfo['urlClass']).'/'.
			$object->getPk().($action!=='view' ? '/'.$action : '').
			($action==='view-field' ? '?field=$field' :'');
		};

	// Allow caller to override default $actionUrl
	if(isset($classInfo['list_action_url']))
	{
		$actionUrl=function($object,$action)use($classInfo,$actionUrl)
			{
				return $classInfo['list_action_url']($object,$action,$actionUrl);
			};
	}

	// Convert $classInfo to $config (that is used in table_render()).
	$keys=preg_grep('@^list_@',array_keys($classInfo));
	$config=array_combine(preg_replace('@^list_@','',$keys),array_intersect_key($classInfo,array_flip($keys)));
	$queryDesc=['class'=>$class];

	$config['table_class'  ][]='dbobject-ui-list';
	$config['table_class'  ][]=$classInfo['urlClass'].'-list';
	$config['wrapper_class'][]='dbobject-ui-list-wrapper';
	$config['wrapper_class'][]=$classInfo['urlClass'].'-list-wrapper';

	// Add source code comments if available.
	require_once 'parse-php.php';
	$srcCodeComments=parse_php_dbobject_comments($class);
	if(!isset($config['top_text']) && 
	   $srcCodeComments[$class]!==false) 
	{
		$config['top_text']='<h1 class="table-render-title">'.ent($class).'</h1>'.
			'<div class="source-code-comments">'.ent($srcCodeComments[$class]).'</div>';
	}

	// Add classInfo field_desc (title) to $columns from schema
	$columns=$class::getSchema();
	foreach(val($classInfo,'field_desc',[]) as $fieldName=>$fieldDesc)
	{
		if(isset($columns[$fieldName]) && isset($fieldDesc['title'])){$columns[$fieldName]['title']=$fieldDesc['title'];}
	}
	require_once 'table-tools.php';
	return table_render('dbobject',$columns,$queryDesc,$config,$class::dboGetPKeyName(),$actionUrl);
}


//! Display a form to edit a DBObject
function dbobject_ui_edit($class,$id=false,$classInfoOverride=[])
{
	global $dbobject_ui_config,$currentPage,$base_url;
	$classInfo=dbobject_ui_class_setup($class);
	$classInfo=array_merge($classInfo,$classInfoOverride);
	$urlClass=$classInfo['urlClass'];
	$main_url=val($classInfo,'main_url',$base_url.'/backend/'.$urlClass);

	$currentPage->addCss('dlib/dbobject-ui.css');

	require_once 'dlib/form.php';
	// **** edit new object
	if($id===false)
	{
		// ** If object constructor requires arguments
		// we need an extra step: a form so that the user can provide constructor arguments.
		// The form items are provided in config.
		if(isset($classInfo['new']))
		{
			dbobject_ui_tabs($class,false,'add');
			$items=$classInfo['new'];
			// add the submit button with a handler
			$items['new']=['type'=>'submit',
						   'value'=>t('Create !class',['!class'=>$class]),
						   'submit'=>function($items)use($class,$urlClass,$main_url)
				{
					// On submission, create new object, save it, and redirect to its edit page.
					global $base_url;
					$reflect  = new ReflectionClass($class);
					$args=dlib_array_column($items,'value');
					array_pop($args);
					$object = $reflect->newInstanceArgs($args);
					$object->save();
					dlib_redirect($main_url.'/'.$object->getPk().'/edit');
				},
						  ];
			if(isset($classInfo['new_alter'])){$classInfo['new_alter']($items);}
			return form_process($items);
		}
		// ** no constructor args required
		$object=new $class();
	}
	else
	{
		// **** edit existing object
		$object=$class::fetch($id,false);
		if($object===null){dlib_not_found_404($class."::id=".$id." not found");}
	}
	// **** create standard form from object
	list($items,$options)=form_dbobject($object);

	dbobject_ui_tabs($class,$object->id ? $object : false,$object->id ? 'edit' : 'add');

	// * add source code comments as item description if no description is available
	$customFieldsDesc=val($classInfo,'field_desc',[]);
	require_once 'parse-php.php';
	$srcCodeComments=parse_php_dbobject_comments($class);
	foreach($items as $name=>&$item)
	{
		$thisFieldDesc=val($customFieldsDesc,$name,[]);
		if(!isset($item['description']) && val($srcCodeComments,$name)!==false )
		{
			$item['description']=ent($srcCodeComments[$name]);
		}
	}
	unset($item);

	// redirect to view after save
	$items['save']['submit-1']=function(&$items)use($object,$main_url)
		{if(!isset($items['save']['redirect'])){$items['save']['redirect']=$main_url.'/'.$object->getPk();}};

	// delete button does not exists for newly created unsaved objects
	if(isset($items['delete']))
	{
		// Override built-in delete, and redirect to pretier delete confirm form instead
		$items['delete']['submit-2']=function()use($main_url,$object){dlib_redirect($main_url.'/'.$object->id.'/delete');};
	}

	$items['cancel']=['type'=>'submit',
					  'value'=>t('Cancel'),
					  'skip-processing'=>true,
					  'submit'=>function()use($object,$main_url){dlib_redirect($main_url.($object->id ? '/'.$object->id : ''));},
					 ];
	
	$options['attributes']['class'][]='dbobject-ui-edit';

	if(isset($classInfo['edit_form_alter'])){$classInfo['edit_form_alter']($items,$options,$object);}

	return form_process($items,$options);
}


//! Display a single DBObject
//! Note that this function is also used to display the delete form.
function dbobject_ui_view($class,$id,$isDelete=false,$classInfoOverride=[])
{
	global $dbobject_ui_config,$currentPage,$base_url;
	$classInfo=dbobject_ui_class_setup($class);
	$classInfo=array_merge($classInfo,$classInfoOverride);
	$urlClass=$classInfo['urlClass'];
	$main_url=val($classInfo,'main_url',$base_url.'/backend/'.$urlClass);
	$currentPage->addCss('dlib/dbobject-ui.css');

	$object=$class::fetch($id,false);
	if($object===null){dlib_not_found_404();}
	$schema=$class::getSchema();

	$customFieldsDesc=val($classInfo,'field_desc',[]);

	dbobject_ui_tabs($class,$object,'view');

	require_once 'parse-php.php';
	$srcCodeComments=parse_php_dbobject_comments($class);

	// **** build display for each field in this object
	$display=[];
	foreach($schema as $name=>$desc)
	{ 
		$v=$object->$name;
		switch($desc['type'])
		{
		case 'foreign_key': 
			if($v==0){$d='0';}
			else
			{
				$oclass=$desc['class'];
				$oClassInfo=$dbobject_ui_config['classes'][$oclass];
				$d='<a href="'.ent(val($oClassInfo,'main_url',$base_url.'/backend/'.$oClassInfo['urlClass'])).'/'.intval($v).'">'.intval($v).'</a>';
			}
			break;
		case 'enum': 
			$d=ent($desc['values'][$v]);
			break;
		case 'bool': 
			$d=$v ? 'true' : 'false';
			break;
		case 'timestamp': 
			$d=$v==0 ? '-' : ent(date('r',$v));
			break;
		case 'email': 
			$d='<a href="mailto:'.ent($v).'">'.ent($v).'</a>';
			break;
		case 'url': 
			require_once 'dlib/filter-xss.php';
			$d='<a href="'.ent(u($v)).'">'.ent(mb_substr($v,0,300)).'</a>';
			break;
		case 'array':
			$p=implode("\n",array_slice(explode("\n",print_r($v,true)),2,-2));
			
			if(count($v)===0){$d=t('[empty]');}
			else
			if(mb_strlen($p)<200 && substr_count($p,"\n")<7)
			{
				$d='<pre>'.ent($p).'</pre>';
			}
			else
			{
				$currentPage->addJs('lib/jquery.js');
				$d='['.t('!nb items',['!nb'=>count($v)]).']';
				$d.='<div class="expand-wrap">'.
					'<button type="button" class="expand" onclick="$(this).parent().find(\'.expanded\').toggle()">'.
					t('more...').'</button>'.
					'<div class="expanded" style="display:none"><pre>'.ent(mb_substr($p,0,100000)).'</pre></div></div>';
			}			
			break;
		default:
			if(mb_strlen($v)<100)
			{
				$d=ent($v);
			}
			else
			{
				$currentPage->addJs('lib/jquery.js');
				$d='<div>'.ent(mb_substr($v,0,100)).'[...]</div>';
				$d.='<div class="expand-wrap">'.
					'<button type="button" class="expand" onclick="$(this).parent().find(\'.expanded\').toggle()">'.
					t('more...').' ('.mb_strlen($v).')</button>'.
					'<div class="expanded" style="display:none"><pre>'.ent($v).'</pre></div></div>';
			}
		}
		$thisFieldDesc=val($customFieldsDesc,$name,[]);
		$display[$name]=['title'=>val($thisFieldDesc,'title',$name),
						 'disp'=>$d,
						 'description'=>val($thisFieldDesc,'description'),
						];
		if($display[$name]['description']===false){$display[$name]['description']=ent($srcCodeComments[$name]);}
	}

	// call customization hook if requested
	if(isset($classInfo['view'])){$classInfo['view']($display,$object,$schema,$isDelete);}

	// **** If this is actually a delete page, create the delete confirm form
	$deleteForm=false;
	if($isDelete)
	{
		require_once 'dlib/form.php';
		// **** Simple form with two buttons delete & cancel
		$items=[];
		$items['title']=['html'=>'<h3>'.t('Are you sure you want to delete this @class ?',['@class'=>$class]).'</h3>'];

		$refWarn=dbobject_ui_delete_foreign_key_warning($class,$object);
		if($refWarn!==false)
		{
			$items['ref-warning']=['html'=>t('WARNING !!!!!!: references to this object have been found! Deleting this object will probably break your site. Delete only if you REALLY know what you are doing. Here is a list of references:').
								   $refWarn];			
		}

		$items['delete']=['type'=>'submit',
						  'value'=>'delete',
						  'submit'=>function()use(&$object){$object->delete();},
						  'redirect'=>$main_url,
						 ];
		$items['cancel']=['type'=>'submit',
						  'value'=>'cancel',
						  'redirect'=>$main_url,
						 ];
		
		if(isset($classInfo['delete_alter'])){$classInfo['delete_alter']($items,$object);}
		$deleteForm=form_process($items);
	}

	require_once 'dlib/template.php';
	return template_render('dbobject-ui-view.tpl.php',
						   [compact('object','class','urlClass','display','deleteForm','main_url'),
							isset($classInfo['view_vars']) ? $classInfo['view_vars']($object,$isDelete) : [],
						   ]);
}

//! Response to json request for full details on a specific field
function dbobject_ui_view_field($class,$id,$field)
{
	global $dbobject_ui_config;
	$object=$class::fetch($id,false);
	if($object===null){dlib_not_found_404();}
	return ['value'=>$object->$field];
}

//! Warn user if he wants to delete an object that is referenced as a foreign key by another object
function dbobject_ui_delete_foreign_key_warning($class,$object)
{
	global $dbobject_ui_config,$base_url;
	$refs=[];
	foreach($dbobject_ui_config['classes'] as $oclass=>&$cConfig)
	{
		$oschema=$oclass::getSchema();
		foreach($oclass::$dboStatic['foreign'] as $field)
		{
			if($oschema[$field]['class']===$class)
			{
				// Found other class field $oclass::$field with foreign key referencing this class $class
				$ids=db_one_col("SELECT ".($oclass::dboGetPKeyName())." FROM `".$oclass."` WHERE ".$field."=%d",
								$object->getPk());
				// special case: self reference
				if($class===$oclass){$ids=array_diff($ids,[$object->getPk()]);}
				if(count($ids))
				{
					dbobject_ui_class_setup($oclass);
					$oclassInfo=$dbobject_ui_config['classes'][$oclass];
					$refs[$oclass.'::'.$field]=['ids'=>$ids,
												'oclass'=>$oclass,
												'oci'=>$oclassInfo,];
				}
			}
		}
	}
	if(count($refs))
	{
		$refWarn='';
		foreach($refs as $refName=>$ref)
		{
			$refWarn.='<p>'.$refName.': <br/>';
			$oclass=$ref['oclass'];
			$oci   =$ref['oci'];
			$omain_url=val($oclassInfo,'main_url',$base_url.'/backend/'.$oci['urlClass']);
			foreach($ref['ids'] as $id){$refWarn.='<a href="'.ent($omain_url.'/'.$id).'">'.ent($id).'</a>, ';}
			$refWarn.='</p>';
		}
		return $refWarn;
	}
	return false;
}

//! Delete form for a DBObject : simply displays the DBObject with delete & cancel buttons.
function dbobject_ui_delete($class,$id)
{
	return dbobject_ui_view($class,$id,true);
}

//! Creates tabs (links) for a dbobject_ui page. Tabs are a variable in $currentPage. 
//! The application should render these tabs in its main page template.
function dbobject_ui_tabs($class,$object,$active)
{
	global $base_url,$currentPage;
	$classInfo=dbobject_ui_class_setup($class);

	$urlClass=$classInfo['urlClass'];
	$main_url=val($classInfo,'main_url',$base_url.'/backend/'.$urlClass);

	$tabs=[];
	$tabs['list']=['href'=>$main_url,'text'=>t('List')];
	$tabs['add' ]=['href'=>$main_url.'/add','text'=>ent(t('Create !class',['!class'=>$classInfo['class']]))];
	if($object)
	{
		$tabs['view']=['href'=>$main_url.'/'.$object->getPk()        ,'text'=>ent(t('View'))];
		$tabs['edit']=['href'=>$main_url.'/'.$object->getPk().'/edit','text'=>ent(t('Edit'))];
	}
	if(isset($classInfo['tabs'])){$classInfo['tabs']($tabs,$object,$active);}

	if(isset($tabs[$active])){$tabs[$active]['active']=true;}
	$currentPage->tabs=$tabs;
}

/** @} */

?>