<?php

//! A simple look-alike replacement to xdebug's var_dump() for environments where xdebug is not available.
//! Enabling Xdebug in production has a big performance penalty and might have security risks.
//! Plain var_dump() is dangerous (entities not escaped) and is not readable in an html context.
//! xdebug var_dump, uses ini_get('html_errors') to determine whether to output html or text.
//! This may be an XSS issue, if html_errors is false and output is done in an html context.
//! We are more restrictive: html is always used for errors, unless we're on commandline ($dlib_config['is_commandline'] or PHP_SAPI)
//! Like xdebug var_dump, text output generates color text using ANSI/VT100 terminal control escape sequences.
//! http://www.termsys.demon.co.uk/vtansi.htm
function vd()
{
	$useHtml=dlib_debug_use_html();

	$varName=dlib_debug_guess_var_name('vd');

	foreach(func_get_args() as $n=>$var)
	{
		dlib_debug_vd($var,$useHtml,$n===0 && $varName!==null ? $varName : null);
	}
}

//! Like vd(), but writes into log file, using dlib_log().
//! Logfile can be configured with $dlib_config['debug_log']. 
//! It defaults to the default logFile of dlib_log().
function vl()
{
	global $dlib_config;
	$varName=dlib_debug_guess_var_name('vl');

	$out='';
	foreach(func_get_args() as $n=>$var)
	{
		$out.=dlib_debug_vd_get($var,false,$varName);
	}
	dlib_log("\n".trim($out),$dlib_config['debug_log'] ?? false);
}

//! Wrapper around dlib_debug_vd() that returns result string instead displaying on stdout.
function dlib_debug_vd_get($var,bool $useHtml,$label=null): string
{
	ob_start();
	dlib_debug_vd($var,$useHtml,$label);
	$res=ob_get_contents();
	ob_end_clean();
	return $res;
}

//! Returns the name of the first argument of $fctName by looking into the stack trace.
//! It parses the calling line in the PHP files using a regex.
//! It ignores all functions in backtrace that are inside dlib-debug.php
//! This first argument must be a simple variable : fctName($example), not a more complex expression fctName(1+1)
//! This is *very* hackish and can be fooled, but it works fine for debugging.
function dlib_debug_guess_var_name(string $fctName)
{
	$call=false;
	foreach(debug_backtrace() as $trace)
	{
		if(basename($trace['file'])!=='dlib-debug.php')
		{
			$call=$trace;
			break;
		}
	}
	if($call===false){return null;}

	$file=@file_get_contents($call['file']);
	if($file===false){return null;}

	$lines=explode("\n",$file);
	$line=$lines[$call['line']-1] ?? false;
	if($line===false){return null;}

	if(!preg_match_all('@\b'.preg_quote($fctName,'@').'\s*\(\s*(\$[a-z_][a-z0-9_]*)\s*[,\)]@i',$line,$m)){return null;}
	if(count($m[1])!==1){return null;}
	return $m[1][0];
}

//! You should generaly use vd() or vl().
function dlib_debug_vd($var,bool $useHtml,$label=null)
{
	global $dlib_config;
	$ent=function($t      )use($useHtml){return $useHtml ? htmlspecialchars($t,ENT_QUOTES,'UTF-8') : $t;};
	if($useHtml){echo '<pre class="xdebug-var-dump" style="white-space: pre-wrap;" dir="ltr">';}
	// display a barely visible (space) link to file
	if($useHtml && isset($dlib_config['backtrace_file']))
	{
		$backtrace=debug_backtrace();
		if(isset($backtrace[1]) && isset($backtrace[1]['file']) && isset($backtrace[1]['line']))
		{
			$link=$dlib_config['backtrace_file']($backtrace[1]['file'],$backtrace[1]['line'],$useHtml,true);
			echo '<a href="'.htmlspecialchars($link,ENT_QUOTES,'UTF-8').'"> </a>';
		}
	}
	if($label!==null){echo $ent($label).":";}
	dlib_debug_vd_recursive($var,$useHtml,0,[]);
	if($useHtml){echo "</pre>";}
}

function dlib_debug_vd_recursive($var,$useHtml,$depth=0,$parents=[])
{
	global $dlib_config;
	$h  =function($h,$t='')use($useHtml){return $useHtml ? $h : $t;};
	$ent=function($t      )use($useHtml){return $useHtml ? htmlspecialchars($t,ENT_QUOTES,'UTF-8') : $t;};

	// *** Display array or object
	if(is_array($var) || is_object($var))
	{
		// Check for recursive object ref. (FIXME: we should do this for other types of references)
		if(is_object($var))
		{
			foreach($parents as $parent)
			{
				if($var===$parent){echo $h('<i>').'recursive object reference...'.$h('</i><br/>',"\n");return;}
			}
		}

		if($depth>5){echo $h('<i>').'max depth...'.$h('</i><br/>',"\n");return;}
		echo $h('<b>').$ent(gettype($var)).$h('</b>');
		if(is_object($var))
		{
			echo '('.$h('<i>').$ent(get_class($var)).$h('</i>').')';
		}
		else
		{
			echo $h(' ').$h('<i>').'('.$h('size=').count($var).')'.$h('</i>');
		}

		// we need this to access private fields in objects 
		$objectVals=false;
		$objProperties=false;
		if(is_object($var))
		{
			$objProperties=(new ReflectionObject($var))->getProperties();
			$objectVals=[];
			foreach($objProperties as $prop)
			{
				if(!$prop->isStatic())
				{
					$prop->setAccessible(true);
					// added "@" to avoid warning in very special case:
					// DBObject::dboNewObjectNotSaved is unset in DBObject::load(), but it still shows up in $objProperties
					// Possible PHP 7.0.6 change: https://bugs.php.net/bug.php?id=72174
					// Check if this is still a problem in some future version.
					$objectVals[$prop->name]=@$prop->getValue($var);
				}
			}
		}

		if($var instanceof Closure)
		{
			$r=new ReflectionFunction($var);

			$closureLink=false;
			if($useHtml && isset($dlib_config['backtrace_file']))
			{
				$closureLink=$dlib_config['backtrace_file']($r->getFileName(),$r->getStartLine(),true,true);
				echo ' <a href="'.htmlspecialchars($closureLink,ENT_QUOTES,'UTF-8').'">'.ent(basename($r->getFileName()).':'.$r->getStartLine()).'</a>';
			}
			else
			{
				echo ' '.$ent(basename($r->getFileName()).':'.$r->getStartLine());
			}
			echo "\n";
		}
		else
		if(count(is_object($var) ?  $objProperties : $var)===0)
		{
			// Empty array or object
			echo ' '.$h("<i style='color: #888a85'>","\033[30m").'empty'.$h("</i>","\033[0m")."\n";
		}
		else
		{
			// Non-empty array or object

			echo $h('',' {');
			echo "\n";

			// Determine padding used vertical align values
			$maxkeyLength=0;
			foreach($var as $key=>$value){$maxkeyLength=max($maxkeyLength,mb_strlen($key)+(is_string($key) ? 2: 0));}
			$keyLength=min(20,$maxkeyLength);

			// Display list of values
			$ct=0;
			foreach((is_object($var) ? $objectVals : $var) as $key=>$value)
			{
				dlib_debug_indent($depth+1);
				if($ct>=128){echo $h('<i>').'more elements...'.$h('</i>');break;}
				$keyDisp=$key;
				if(is_string($key)){$keyDisp="'".$key."'";}
				$keyDisp=dlib_debug_string_pad($keyDisp,$keyLength);
				echo $ent($keyDisp);
				echo " ".$h("<font color='#888a85'>","\033[30m").$ent("=>").$h("</font>","\033[0m")." ";
				$parents[]=$var;
				dlib_debug_vd_recursive($value,$useHtml,$depth+1,$parents);
				$ct++;
			}
			if(!$useHtml){dlib_debug_indent($depth);echo "}\n";}
		}
		return;
	}
	else
	if(is_resource($var))
	{
		echo $h('<b>').'resource'.$h('</b>').' ';
	}
	else
	if(is_null($var))
	{
		echo $h("<font color='#3465a4'>","\033[34m").'null'.$h("</font>","\033[0m");
	}
	else
	{
		
		echo $h('<small>').$ent(gettype($var)==='integer' ? 'int' : gettype($var)).$h('</small>');
		if(is_string($var)){echo $h('','('.strlen($var).')');}
		echo ' ';
	}
	switch(gettype($var))
	{
	case 'array':
	case 'object':
	case 'resource':
	case 'NULL':
		break;
	case 'string':
		$tooLong=mb_strlen($var)>512;
		$s=$tooLong ? mb_substr($var,0,512) : $var;

		// Manage invalid or binary caracters in string
		$oldSubs=mb_substitute_character();
		mb_substitute_character(hexdec('FFFD'));
		$s=mb_convert_encoding($s,mb_internal_encoding(),mb_internal_encoding());
		mb_substitute_character($oldSubs);

		//  '/[^[:print:]]/u'
		// http://perldoc.perl.org/perlunicode.html
		$s=preg_replace_callback('/[^\p{L}\p{M}\p{N}\p{P}\p{S}\p{Z}\n\t]/u',function($c)use($useHtml)
								  {
									  if($c[0]==="\r"){return '\\r';}
									  return '\\'.ord($c[0]);
								  },$s);
		echo $h('<font color="#cc0000">',"\033[31m").$ent("'").
			($s===null ? '[[vd() : ERROR]]' : $ent($s)).
			$ent("'").($tooLong ? '...' : '').$h('</font>',"\033[0m");
		echo ' '.$h('<i>(length='.strlen($var).')</i>');
		break;
	case 'integer':
		echo $h('<font color="#4e9a06">',"\033[32m").$ent($var).$h('</font>',"\033[0m");
		break;
	case 'float':
	case 'double':
		echo $h('<font color="#f57900">',"\033[33m").$ent($var).$h('</font>',"\033[0m");
		break;
	case 'boolean':
		echo $h('<font color="#75507b">',"\033[36m").$ent($var ? 'true' : 'false').$h('</font>',"\033[0m");
		break;
	default:
		echo $h('<font color="#ff0000">',"\033[31m").'???'.$h('</font>',"\033[0m");
		break;		
	}
	if(!$useHtml || $depth>0){echo  "\n";}
}

function dlib_debug_indent($depth)
{
	for($i=0;$i<4*$depth;$i++){echo " ";}
}

function dlib_debug_string_pad($str,$size=30)
{
	$spaces="                                                                                                    ";
	$l=mb_strlen($str);
	return $l<$size ? $str.mb_substr($spaces,0,$size-$l) : $str;
}

//! Returns a rendered stack trace roughly similar to xdebug's backtrace.
//! $offset : number of calls to skip at top of stack, or "auto" to skip all error and logging related functions
//! $backtrace : stack (false means "use debug_backtrace()")
//! Link to file can be customized with $dlib_config['backtrace_file']
//! * $offset: 'auto', int : skip $offset elements at begining of stack. auto: skip debugging/logging related function
//! * $outputType: 'auto','html','term' or 'text'
//! * $showArgs: 'auto',true or false. auto: $showArgs=$dlib_config['debug'] 
function dlib_debug_backtrace($backtrace=false,$offset='auto',$outputType='auto',$showArgs='auto')
{
	global $dlib_config;
	if($outputType==='auto'){$outputType=dlib_debug_use_html() ? 'html' : 'term';}
	
	// when using $offset=auto, skip these functions at the begining of the stack
	$skipFunctions=['dlib_abort','fatal',
					'dlib_permission_denied_403','dlib_not_found_404','dlib_bad_request_400',
					'dlib_log_error','dlib_debug_error_display','dlib_debug_error_handler'];

	$term=function($s)use($outputType){return $outputType==='term' ? $s:'';};
	
	$out='';
	
	if($outputType==='html')
	{
		$out.='<style type="text/css">
                 .dlib-debug-backtrace {background-color:#eee; border-collapse: collapse;}
                 .dlib-debug-backtrace td {border: 1px solid black;padding: 2px 4px;}
              </style>'."\n";
	}
	if($backtrace===false){$backtrace=debug_backtrace();}
	if($outputType==='html'){$out.='<table class="dlib-debug-backtrace">'."\n";}
	$hasStarted=false;
	foreach($backtrace as $n=>$call)
	{
		if($offset!=='auto' && ($offset--)>0){continue;}

		$prev    =$backtrace[$n+1] ?? [];
		$prevArgs=$prev['args'   ] ?? [];

		$file    =$call['file'    ] ?? 'no file';
		$line    =$call['line'    ] ?? 'no line';
		$function=$prev['function'] ?? 'no fct' ;

		if($hasStarted===false && $offset==='auto' && array_search($function,$skipFunctions)!==false){continue;}

		$hasStarted=true;
		$evalLine='';

		// Special case for eval() 
		if(preg_match('@^(.*)\(([0-9]+)\) : eval\(@',$file,$m))
		{
			$evalLine=':'.$line.'';
			$file=$m[1];
			$line=$m[2];
		}

		if(isset($dlib_config['backtrace_file']))
		{
			$link=$dlib_config['backtrace_file']($file,$line,$outputType);
		}
		else                                     
		{
			if($outputType==='html'){$link=ent($file);}
			else                    {$link=    $file ;}
		}

		$argsOut='';
		// Arguments can leak sensitive data : only display if we are in debug mode
		if($showArgs==='auto' ? ($dlib_config['debug'] ?? false) : $showArgs)
		{
			foreach($prevArgs as $arg)
			{
				$argsOut.=dlib_debug_backtrace_arg($arg).', ';
			}
			$argsOut=preg_replace('@, $@','',$argsOut);
		}

		if($outputType==='html')
		{
			$out.="<tr>";	
			$out.="<td>".ent($function).'(<span style="color:#aaa;">'.ent($argsOut).'</span>)'.$evalLine.'</td>';
			$out.='<td>'.$link.'</td>';
			$out.="<td>".ent($line).'</td>';
			$out.="</tr>\n";
		}
		else
		{
			$tmp=$function."(".$term("\033[30m").$argsOut.$term("\033[0m").")".$evalLine;
			$tmp=dlib_debug_string_pad($tmp,40)."  ";
			$out.=$tmp;
			$out.=$term("\033[35m").$link.':'.$line.$term("\033[0m");
			$out.="\n";			
		}
	}
	if($outputType==='html')$out.="</table>\n";
	$out=rtrim($out);
	return $out;
}

function dlib_debug_backtrace_arg($arg)
{
	switch(gettype($arg))
	{
	case 'array':
		return '['.count($arg).']';
	case 'object':
	case 'resource':
	case 'NULL':
		return gettype($arg);
	case 'string':
		return '"'.mb_substr($arg,0,10).(mb_strlen($arg)>10 ? '…':'').'"';
	case 'integer':
		return (string)$arg;
	case 'float':
	case 'double':
		return (string)$arg;
	case 'boolean':
		return $arg ? 'true' : 'false';
	default:
		return '???';
	}

}

//! Displays a PHP error or warning in a way similar (but not identical) to xdebug
//! To use this, you need to call: set_error_handler('dlib_debug_error_handler')
function dlib_debug_error_handler($errno, $errstr, $errfile, $errline, $errcontext)
{
	dlib_debug_error_display($errno, $errstr, $errfile, $errline, $errcontext);
}

//! Displays uncaught PHP exceptions in a way similar (but not identical) to xdebug
//! PHP 7 uses excepetions for "most" (?) errors
//! To use this, you need to call: use set_exception_handler('dlib_debug_exception_handler')
function dlib_debug_exception_handler(Throwable $e)
{
	$backtrace=$e->getTrace();
	array_unshift($backtrace,['file'=>$e->getFile(),'line'=>$e->getLine(),'function'=>'dlib_debug_exception_handler']);
	dlib_debug_error_display($e->getCode(),$e->getMessage(),$e->getFile(), $e->getLine(),[],$backtrace);
}

//! Displays a PHP error or warning in a way similar (but not identical) to xdebug. Also outputs to error log file.
//! This is meant to be used from an error or exception manager (example: dlib_debug_error_handler() dlib_debug_exception_handler())
function dlib_debug_error_display($errno, $errstr, $errfile, $errline, $errcontext,$backtrace=false)
{
	static $errorCount=0;
	global $dlib_config;

	$useHtml=dlib_debug_use_html();
	$h  =function($h,$t='')use($useHtml){return $useHtml ? $h : $t;};
	$ent=function($t      )use($useHtml){return $useHtml ? htmlspecialchars($t,ENT_QUOTES,'UTF-8') : $t;};

	$errId=$errfile.':'.$errline;

	// ignore errors for statements called with "@'
	if($errno===E_NOTICE  ||  $errno===E_STRICT || $errno===E_WARNING)
	{
		if(error_reporting()===0){return;}
	}

	$errorCount++;
	if($errorCount>20){fatal("too many errors, aborting");}

	$errorCodes=
		[
			'Error'             =>E_ERROR			 , 
			'Warning'           =>E_WARNING			 , 
			'Parse'             =>E_PARSE			 , 
			'Notice'            =>E_NOTICE			 , 
			'Core error'        =>E_CORE_ERROR		 , 
			'Core warning'      =>E_CORE_WARNING	 , 
			'Compile error'     =>E_COMPILE_ERROR	 , 
			'Compile warning'   =>E_COMPILE_WARNING	 , 
			'User error'        =>E_USER_ERROR		 , 
			'User warning'      =>E_USER_WARNING	 , 
			'User notice'       =>E_USER_NOTICE		 , 
			'Strict'            =>E_STRICT			 , 
			'Recoverable error' =>E_RECOVERABLE_ERROR, 
			'Deprecated'        =>E_DEPRECATED		 , 
			'User deprecated'   =>E_USER_DEPRECATED	 , 
			//'E_ALL'				=>E_ALL				 ,
		];
	$errnoDecode='';
	foreach($errorCodes as $name=>$val){if($errno & $val){$errnoDecode.=$name.', ';}}
	$errnoDecode=preg_replace('@, $@','',$errnoDecode);
	// This happens for exceptions. (other cases?)
	if($errno===0){$errnoDecode='error';}

	// hackish. How do we know when to abort ?
	$doAbort=stripos($errnoDecode,'error')!==false ||
		     stripos($errnoDecode,'parse')!==false   ;
	if($doAbort && !headers_sent()){header('HTTP/1.0 500');}


	// Custom error display 
	// xdebug often has wrong filename/numbers, our display works better (show both)

    $errOut='';
	if(!$useHtml)
	{
		$errOut.="\033[4m"."\033[1m"."\033[31m".' **** '.$errnoDecode.':'."\033[0m".' '.$errstr."\n";
	}
	else
	{
		$errOut.='<style type="text/css">
.dlib-debug-error {padding-bottom: 1em;margin-bottom: 1em;}
.dlib-debug-error h2    {background-color:#f57900; color: black;margin-bottom: 0;font-size: 17px;display: -webkit-flex;display: flex;}
.dlib-debug-error h2>span:nth-child(1) {padding: .2em;font-size: 22px;background-color: #c00;color: fce94f;}
.dlib-debug-error h2>span:nth-child(2) {padding: .5em;}
</style>';

		$errOut.='<div class="dlib-debug-error">';
		$errOut.='<h2><span>( ! )</span><span>'.ent($errnoDecode.': '.$errstr).'</span></h2>';
	}

	flush(); // added to force header output and avoid redirect that would hide errors

    echo $errOut;
	echo dlib_debug_backtrace($backtrace)."\n";
	if($useHtml){echo '</div>';}

    // Also output to error log file.
    // This is is usefull because this function returns true, so errors are not reported in normal apache error log.
	dlib_log_error('error handler:'.$errnoDecode.': '.$errstr,$backtrace);

	$fcts=dlib_array_column(debug_backtrace(),'function');
	if(array_search('css_template_compile',$fcts)!==false && 
	   array_search('eval',$fcts)!==false   )
	{
		fatal('Abort after error or warning in css template');
	}

	if($doAbort){echo "Abort.\n";exit(1);}
	return true;
}


//! Displays an Exception message and, if we're in debug mode, its stack.
function dlib_debug_exception_display($e)
{
	global $dlib_config;
	require_once 'tools.php';
	$useHtml=dlib_debug_use_html();

	if($useHtml)
	{
		echo '<h2 style="margin:0;margin-top: 1em;padding: .5em;background-color:#f00;color: white;font-size: 17px;">ERROR/Exception:</h2>'.
			'<pre style="background-color: #fcc;margin: 0; padding: 1em;white-space: pre-wrap;">'.ent($e->getMessage())."</pre>";
	}
	else
	{
		echo "\033[4m"."\033[1m".'ERROR/Exception'."\033[0m".': '.$e->getMessage()."\n";
	}
	if(val($dlib_config,'debug'))
	{
		$backtrace=$e->getTrace();
		array_unshift($backtrace,['file'=>$e->getFile(),'line'=>$e->getLine(),'function'=>'?','args'=>[]]);
		echo dlib_debug_backtrace($backtrace,0);
	}
	else
	{
		echo 'FILE:'.ent($e->getFile()).':'.$e->getLine();
	}
}

//! dlib-debug.php is meant to be usable alone. We can't be sure $dlib_config['is_commandline'] has been set.
function dlib_debug_use_html()
{
	if(isset($GLOBALS['dlib_config']['is_commandline'])){return !$GLOBALS['dlib_config']['is_commandline'];}
	return PHP_SAPI!=='cli';
}

?>