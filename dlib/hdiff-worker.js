
//console.log('WORKER: has started');

onmessage = function(e) 
{
	//console.log('WORKER: Message received from main script',e.data);
	var lcs=longest_common_subsequence(e.data[0],e.data[1]);
	postMessage(lcs);
	//console.log('WORKER: finished');
}

// http://rosettacode.org/wiki/Longest_common_subsequence#JavaScript
// This is slow for long texts.
// Example : fast machine 2018: 
//   - ordinary short event:  315 words =>    5ms
//   -      very long event: 5500 words => 2000ms
// consistent with O(n x m) => O(n2)
function longest_common_subsequence(x,y){
	//var start=Date.now();
	var s,i,j,m,n,
		lcs=[],row=[],c=[],
		left,diag,latch;
	//make sure shorter string is the column string
	if(m<n){s=x;x=y;y=s;}
	m = x.length;
	n = y.length;
	//build the c-table
	for(j=0;j<n;row[j++]=0);
	for(i=0;i<m;i++){
		c[i] = row = row.slice();
		for(diag=0,j=0;j<n;j++,diag=latch){
			latch=row[j];
			if(x[i] == y[j]){row[j] = diag+1;}
			else{
				left = row[j-1]||0;
				if(left>row[j]){row[j] = left;}
			}
		}
	}
	i--,j--;
	//row[j] now contains the length of the lcs
	//recover the lcs from the table
	while(i>-1&&j>-1){
		switch(c[i][j]){
			default: j--;
				lcs.unshift(x[i]);
			case (i&&c[i-1][j]): i--;
				continue;
			case (j&&c[i][j-1]): j--;
		}
	}
	//console.log('WORKER: longest_common_subsequence: ellapsed:',Date.now()-start);
	return lcs;
}

