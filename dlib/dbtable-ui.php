<?php

/** @defgroup dbtable_ui dbtable_ui
 * Automatically generated user interface for listing, adding and deleting database table rows
 *
 * **Module use**: FIXME HtmlPageData
 *  @{
 */

//! dbtable_ui_auto_setup(): Quick preliminary setup of $dbtable_ui_config.
//!
//! Setup of $dbtable_ui_config is done in 3 steps, for performance:
//! 1) $dbtable_ui_config must be created by program during init phase.
//! 2) dbtable_ui_auto_setup is called (only if necessary)
//! 3) dbtable_ui_table_setup is called (only if necessary & for a specific table)
//!
//! ** $dbtable_ui_config **
//! You must list which tables use dbtable_ui and optionally give some information about them.
//! Optional information for each table:
//! - main_url           : base url for all this class
//! - setup-file		 : file that will be included before setup ('require_once')
//! - setup				 : user supplied function to further setup $dbobject_ui_config when it is actually needed for a specific class
//! - list_setup		 : called before displaying list
//! - list_action_url	 : function that returs an url for delete, edit, view, and view-field actions
//! - new				 : if the class has a non empty constructor, you must provide form items to create arguments for the constructor.
//! - new_alter			 : 
//! - field_desc		 : extra information on fields (title, html description)
//! - edit_form_alter	 : 
//! - view				 : user supplied function for customizing view
//! - delete_alter		 : 
//! - view_vars			 : 
//! - list_...           : see table_render() and add "list_" prefix
//! \endcode

//! $dbtable_ui_config : configuration for dbtable_ui.
//! List of $dbtable_ui_config keys:
//! - tables
//! -::-;-::-
$dbtable_ui_config;


function dbtable_ui_auto_setup()
{
	global $dbtable_ui_config;
	
	foreach($dbtable_ui_config['tables'] as $table=>&$tConfig)
	{
		$tConfig['table']=$table;
	}
}

//! Adds paths for each operation (list,add,edit,delete,view) for each table.
function dbtable_ui_add_paths(array &$urls,array &$options)
{
	global $dbtable_ui_config,$base_url;
	dbtable_ui_auto_setup();

	foreach($dbtable_ui_config['tables'] as $table=>$tConfig)
	{
		$path='backend/'.$table;
		if(isset($tConfig['main_url']))
		{
			$path=substr($tConfig['main_url'],strlen($base_url)+1);
		}

		$urls['dbtable-ui-list-'.$table]
			=['path'=>$path,
			  'roles' => ['admin'],
			  'function'=>function()use($table){return dbtable_ui_list($table);},
			 ];
		$urls['dbtable-ui-view-'.$table]=
			['path'=>$path.'/view',
			 'roles' => ['admin'],
			 'function'=>function()use($table){return dbtable_ui_view($table,$_GET);},
			];
		$urls['dbtable-ui-view-field-'.$table]=
			['path'=>$path.'/view-field',
			 'output'=>'json',
			 'roles' => ['admin'],
			 'function'=>function()use($table){return dbtable_ui_view_field($table,$_GET,$_GET['_field']);},
			];
		$urls['dbtable-ui-edit-'.$table]=
			['path'=>$path.'/edit',
			 'roles' => ['admin'],
			 'function'=>function()use($table){return dbtable_ui_edit($table,$_GET);},
			];
		$urls['dbtable-ui-delete-'.$table]=
			['path'=>$path.'/delete',
			 'roles' => ['admin'],
			 'function'=>function()use($table){return dbtable_ui_delete($table,$_GET);},
			];
		$urls['dbtable-ui-add-'.$table]=
			['path'=>$path.'/add',
			 'roles' => ['admin'],
			 'function'=>function()use($table){return dbtable_ui_edit($table);},
			];
	}
}

//! Setup a specific table in $dbtable_ui_config['tables'][$table]
//! This is called only if necessary. 
//! It can be called more than once, but subsequent calls are ignored.
//! This calls user defined setup function if available.
//! This also builds a schema for the table by using both a DESCRIBE query and SELECT+mysqli_fetch_fields().
function dbtable_ui_table_setup(string $table)
{
	global $dbtable_ui_config;
	$tableInfo=&$dbtable_ui_config['tables'][$table];
	if(isset($tableInfo['setup-is-ok'])){return $tableInfo;}
	if(!isset($tableInfo['table'])){dbtable_ui_auto_setup();$tableInfo=&$dbtable_ui_config['tables'][$table];}

	// **** setup schema
	$describe=db_arrays('DESCRIBE `'.$table.'`');
	$describe=array_combine(dlib_array_column($describe,'Field'),$describe);
	$pks=array_keys(array_filter($describe,function($v){return $v['Key']==='PRI';}));
	$tableInfo['pks']=$pks;

	$qres=db_query('SELECT * FROM `'.$table.'`');
	$fields=mysqli_fetch_fields($qres);
	$fields=array_combine(dlib_object_column($fields,'orgname'),$fields);

	require_once 'table-tools.php';
	$mysqliTypes=table_mysqli_types();

	$initialSchema=val($tableInfo,'schema',[]);
	$tableInfo['schema']=[];
	$ct=0;
	foreach($describe as $name=>$desc)
	{
		if(!isset($fields[$name])){fatal('dbtable_ui_table_setup: field not found');}
		if(!isset($mysqliTypes[$fields[$name]->type])){fatal('dbtable_ui_table_setup: field type not found');}
		$type=$mysqliTypes[$fields[$name]->type];
		$s=['describe'=>$desc,
			'sql'     =>$fields[$name],
			'format'  =>DBObject::dbFormat($type),
			'type'    =>$type,
			'nb'      =>$ct++,
			];
		// merge with initial (config) supplied values
		$tableInfo['schema'][$name]=array_merge($s,val($initialSchema,$name,[]));
	}

	// **** call custom setup function / files
	if(isset($tableInfo['setup-file'])){require_once $tableInfo['setup-file'];}
	if(isset($tableInfo['setup'     ])){$tableInfo['setup']($tableInfo);}
	$tableInfo['setup-is-ok']=true;
	return $tableInfo;
}

//! Display a table with a list of all objects of this table.
//! Note : this is just a wrapper around table_render()
function dbtable_ui_list(string $table,array $tableInfoOverride=[])
{
	global $dbtable_ui_config,$currentPage;
	$tableInfo=dbtable_ui_table_setup($table);
	$tableInfo=array_merge($tableInfo,$tableInfoOverride);

	require_once 'table-tools.php';

	if(isset($tableInfo['list_setup'])){$tableInfo['list_setup']();}

	dbtable_ui_tabs($table,false,'list');

	// Function that returs an url for delete, edit, view, and view-field actions
	$actionUrl=function($dataLine,$action)use($table,$tableInfo)
		{
			global $base_url;
			$k=[];

			foreach($tableInfo['pks'] as $pk){$k[$pk]=$dataLine[$tableInfo['schema'][$pk]['nb']];}
			return val($tableInfo,'main_url',$base_url.'/backend/'.$table.'/'.$action.'?'.http_build_query($k)).
					   ($action==='view-field' ? '&_field=$field' :'');
		};

	// Allow caller to override default $actionUrl
	if(isset($config['list_action_url']))
	{
		$actionUrl=function($dataLine,$action)use($config,$actionUrl)
			{
				return $config['list_action_url']($dataLine,$action,$actionUrl);
			};
	}

	$query='SELECT * FROM `'.$table.'`';
	$queryDesc=['query'=>$query,'args'=>[]];

	// Convert $tableInfo to $config (that is used in table_render()).
	$keys=preg_grep('@^list_@',array_keys($tableInfo));
	$config=array_combine(preg_replace('@^list_@','',$keys),array_intersect_key($tableInfo,array_flip($keys)));
	$queryDesc['query'].=' '.val($config,'where','');

	$config['table_class'  ][]='dbobject-ui-list';
	$config['table_class'  ][]=$table.'-list';
	$config['wrapper_class'][]='dbobject-ui-list-wrapper';
	$config['wrapper_class'][]=$table.'-list-wrapper';

	require_once 'table-tools.php';
	return table_render('dbtable',$tableInfo['schema'],$queryDesc,$config,$tableInfo['pks'][0],$actionUrl);
}


//! Display a form to edit a DBObject
function dbtable_ui_edit(string $table,$pkValues=false)
{
	global $dbtable_ui_config,$currentPage,$base_url;
	$tableInfo=dbtable_ui_table_setup($table);
	$main_url=val($tableInfo,'main_url',$base_url.'/backend/'.$table);

	$currentPage->addCss('dlib/dbobject-ui.css');

	require_once 'dlib/form.php';
	// **** edit new row
	if($pkValues===false){$row=[];}
	else
	{
		// **** edit existing row
		$row=dbtable_ui_fetch_row($table,$pkValues);
		if($row===null){dlib_not_found_404("Row not found in table ".$table);}
	}
	// **** create standard form from object
	list($items,$options)=dbtable_ui_form($table,$row);

	dbtable_ui_tabs($table,$row,$row!==[] ? 'edit' : 'add');

	$customFieldsDesc=val($tableInfo,'field_desc',[]);
	foreach($items as $name=>&$item)
	{
		$thisFieldDesc=val($customFieldsDesc,$name,[]);
		if(isset($thisFieldDesc['description'])){$item['description']=$thisFieldDesc['description'];}
		if(isset($thisFieldDesc['title'      ])){$item['title'      ]=$thisFieldDesc['title'      ];}
	}
	unset($item);

	// redirect to view after save
	$items['save']['submit-1']=function(&$items)use($table,$row)
		{
			// we need to get $pks from values since $row pks may have changed
			$pks=array_merge($row,dlib_array_column($items,'value'));
			if(!isset($items['save']['redirect'])){$items['save']['redirect']=dbtable_ui_action_url($table,$pks,'view');}
		};

	// delete button does not exists for newly created unsaved objects
	if(isset($items['delete']))
	{
		// Override built-in delete, and redirect to pretier delete confirm form instead
		$items['delete']['submit-2']=function()use($table,$row){dlib_redirect(dbtable_ui_action_url($table,$row,'delete'));};
	}

	$items['cancel']=['type'=>'submit',
					  'value'=>t('Cancel'),
					  'skip-processing'=>true,
					  'submit'=>function()use($table,$row,$main_url)
		{
			dlib_redirect($row==[] ? $main_url : dbtable_ui_action_url($table,$row,'view'));
		},
					 ];
	
	$options['attributes']['class'][]='dbtable-ui-edit';

	if(isset($tableInfo['edit_form_alter'])){$tableInfo['edit_form_alter']($items,$options,$row);}

	return form_process($items,$options);
}

//! Returns form items and form options to edit $row of a given $table (name)
//! Similar to form_dbobject()
function dbtable_ui_form($table,$row)
{
	$tableInfo=dbtable_ui_table_setup($table);
	$items=[];

	$items['form_row']=['type'=>'data','data'=>$row];

	foreach($tableInfo['schema'] as $name=>$desc)
	{
		$item=[];
		if(!form_dbobject_item_type($desc,$item)){continue;}
		if($item['type']===false){continue;}
		if(isset($row[$name]))
		{
			$item['default-value']=$row[$name];
			// FIXME: workaround for strange int requirement in form_render_item
			if($item['type']==='timestamp'){$item['default-value']=(int)$item['default-value'];}
		}
		$item['title']=val($desc,'title',$name.(array_search($name,$tableInfo['pks'])!==false ? ' *':''));
		$items[$name]=$item;
	}
	$items['save']=
		['type'=>'submit',
		 'value'=>t('Save'),
		 'submit-2'=>function (&$items) use ($table,&$row,$tableInfo)
			{
				$oldPkValues=array_intersect_key($row,array_flip($tableInfo['pks']));
				$values=dlib_array_column($items,'value');
				foreach($tableInfo['schema'] as $name=>$desc)
				{
					if(isset($values[$name])){$row[$name]=$values[$name];}
				}
				dbtable_ui_save_row($table,$row);
				// Special case: saving may change pk... so we need to delete old row 
				$newPkValues=array_intersect_key($row,array_flip($tableInfo['pks']));
				ksort($oldPkValues);
				ksort($newPkValues);
				if($oldPkValues!==[] && $oldPkValues!=$newPkValues)
				{
					dbtable_ui_delete_row($table,$oldPkValues);
				}
			},
		];

	if($row!==[])
	{
		$items['delete']=
			['type'=>'submit',
			 'value'=>t('Delete'),
			 'skip-processing'=>true,
			 'submit-2'=>function() use ($table,&$row)
				{
					dbtable_ui_delete_row($table,$row);
				}
			];
	}

	$options=['id'=>$table,
			  'attributes'=>['class'=>['form-dbrow']],
			 ];
	
	return [$items,$options];
}

//! Returns url to edit/view/delete... a row. 
//! $row must contain entries for each pk.
function dbtable_ui_action_url(string $table,array $row,string $action)
{
	global $base_url;
	$tableInfo=dbtable_ui_table_setup($table);
	$pkValues=array_intersect_key($row,array_flip($tableInfo['pks']));
	return val($tableInfo,'main_url',$base_url.'/backend/'.$table.'/'.$action.'?'.http_build_query($pkValues));
}

//! dbtable_ui CRUD : Read row from table
function dbtable_ui_fetch_row(string $table,array $pkValues)
{
	global $dbtable_ui_config;
	$tableInfo=dbtable_ui_table_setup($table);

	$query='SELECT * FROM `'.$table.'` WHERE ';
	$args=[];
	foreach($tableInfo['pks'] as $pk)
	{
		$format=$tableInfo['schema'][$pk]['format'];
		if($format===false){fatal('dbtable_ui_fetch: unsupported mysql type for primary key '.$table.':'.$pk);}
		$query.=$pk.'='.$format.' AND ';
		if(!isset($pkValues[$pk])){fatal('dbtable_ui_fetch: unset value for '.$table.':'.$pk);}
		$args[]=$pkValues[$pk];
	}
	$query=substr($query,0,-strlen(' AND '));
	return call_user_func_array('db_array',array_merge([$query],$args));
}

//! dbtable_ui CRUD : delete row from table
function dbtable_ui_delete_row(string $table,array $pkValues)
{
	global $dbtable_ui_config;
	$tableInfo=dbtable_ui_table_setup($table);

	$query='DELETE FROM `'.$table.'` WHERE ';
	$args=[];
	foreach($tableInfo['pks'] as $pk)
	{
		$format=$tableInfo['schema'][$pk]['format'];
		if($format===false){fatal('dbtable_ui_delete_row: unsupported mysql type for primary key '.$table.':'.$pk);}
		$query.=$pk.'='.$format.' AND ';
		$val=val($pkValues,$pk);
		if(!isset($pkValues[$pk])){fatal('dbtable_ui_delete_row: unset value for '.$table.':'.$pk);}
		$args[]=$pkValues[$pk];
	}
	$query=substr($query,0,-strlen(' AND '));
	call_user_func_array('db_query',array_merge([$query],$args));
}

//! dbtable_ui CRUD : save row to table (REPLACE INTO)
function dbtable_ui_save_row(string $table,array $row)
{
	global $dbtable_ui_config;
	$tableInfo=dbtable_ui_table_setup($table);

	$query='REPLACE INTO `'.$table.'` ';
	$query.='('.implode(',',array_keys($tableInfo['schema'])).') VALUES ';
	$args=[];
	$formats=[];
	foreach($tableInfo['schema'] as $name=>$desc)
	{
		$format=$desc['format'];
		if($format===false){fatal('dbtable_ui_save_row: unsupported mysql type for primary key '.$table.':'.$name);}
		$formats[]=$format;
		if(!isset($row[$name])){fatal('dbtable_ui_save_row: unset value for '.$table.':'.$name);}
		$args[]=$row[$name];
	}
	$query.='('.implode(',',$formats).') ';
	call_user_func_array('db_query',array_merge([$query],$args));
}

//! Display a single DBObject
//! Note that this function is also used to display the delete form.
function dbtable_ui_view(string $table,array $pkValues,bool $isDelete=false)
{
	global $dbtable_ui_config,$currentPage,$base_url;
	$tableInfo=dbtable_ui_table_setup($table);
	$main_url=val($tableInfo,'main_url',$base_url.'/backend/'.$table);
	$currentPage->addCss('dlib/dbobject-ui.css');

	$row=dbtable_ui_fetch_row($table,$pkValues);
	if($row===null){dlib_not_found_404();}

	dbtable_ui_save_row($table,$row);

	$customFieldsDesc=val($tableInfo,'field_desc',[]);

	dbtable_ui_tabs($table,$row,'view');

	// **** build display for each field in this row
	$display=[];
	foreach($tableInfo['schema'] as $name=>$desc)
	{ 
		$v=$row[$name];
		switch($desc['type'])
		{
		case 'foreign_key': 
			if($v==0){$d='0';}
			else
			{
				global $dbobject_ui_config;
				$oclass=$desc['class'];
				$oClassInfo=$dbobject_ui_config['classes'][$oclass];
				$d='<a href="'.ent(val($oClassInfo,'main_url',$base_url.'/backend/'.$oClassInfo['urlClass'])).'/'.intval($v).'">'.intval($v).'</a>';
			}
			break;
		case 'enum': 
			$d=ent($desc['values'][$v]);
			break;
		case 'bool': 
			$d=$v ? 'true' : 'false';
			break;
		case 'timestamp': 
			$d=$v==0 ? '-' : ent(date('r',$v));
			break;
		case 'email': 
			$d='<a href="mailto:'.ent($v).'">'.ent($v).'</a>';
			break;
		case 'url': 
			require_once 'dlib/filter-xss.php';
			$d='<a href="'.ent(u($v)).'">'.ent(mb_substr($v,0,300)).'</a>';
			break;
		case 'array':
			$p=implode("\n",array_slice(explode("\n",print_r($v,true)),2,-2));
			
			if(count($v)===0){$d=t('[empty]');}
			else
			if(mb_strlen($p)<200 && substr_count($p,"\n")<7)
			{
				$d='<pre>'.ent($p).'</pre>';
			}
			else
			{
				$currentPage->addJs('lib/jquery.js');
				$d='['.t('!nb items',['!nb'=>count($v)]).']';
				$d.='<div class="expand-wrap">'.
					'<button type="button" class="expand" onclick="$(this).parent().find(\'.expanded\').toggle()">'.
					t('more...').'</button>'.
					'<div class="expanded" style="display:none"><pre>'.ent(mb_substr($p,0,100000)).'</pre></div></div>';
			}			
			break;
		default:
			if(mb_strlen($v)<100)
			{
				$d=ent($v);
			}
			else
			{
				$currentPage->addJs('lib/jquery.js');
				$d='<div>'.ent(mb_substr($v,0,100)).'[...]</div>';
				$d.='<div class="expand-wrap">'.
					'<button type="button" class="expand" onclick="$(this).parent().find(\'.expanded\').toggle()">'.
					t('more...').' ('.mb_strlen($v).')</button>'.
					'<div class="expanded" style="display:none"><pre>'.ent($v).'</pre></div></div>';
			}
		}
		$thisFieldDesc=val($customFieldsDesc,$name,[]);
		$display[$name]=['title'=>val($thisFieldDesc,'title',$name),
						 'disp'=>$d,
						 'description'=>val($thisFieldDesc,'description'),
						];
	}

	// call customization hook if requested
	if(isset($tableInfo['view'])){$tableInfo['view']($display,$row,$schema,$isDelete);}

	// **** If this is actually a delete page, create the delete confirm form
	$deleteForm=false;
	if($isDelete)
	{
		require_once 'dlib/form.php';
		// **** Simple form with two buttons delete & cancel
		$items=[];
		$items['title']=['html'=>'<h3>'.t('Are you sure you want to delete this row of @table ?',['@table'=>$table]).'</h3>'];

		// FIXME
		//$refWarn=dbtable_ui_delete_foreign_key_warning($table,$row);
		//if($refWarn!==false)
		//{
		// 	$items['ref-warning']=array('html'=>t('WARNING !!!!!!: references to this row have been found! Deleting this row will probably break your site. Delete only if you REALLY know what you are doing. Here is a list of references:').
		// 								$refWarn);			
		//}

		$items['delete']=['type'=>'submit',
						  'value'=>'delete',
						  'submit'=>function()use($table,&$row){dbtable_ui_delete_row($table,$row);},
						  'redirect'=>$main_url,
						 ];
		$items['cancel']=['type'=>'submit',
						  'value'=>'cancel',
						  'redirect'=>$main_url,
						 ];
		
		if(isset($tableInfo['delete_alter'])){$tableInfo['delete_alter']($items,$row);}
		$deleteForm=form_process($items);
	}

	require_once 'dlib/template.php';
	return template_render('dbobject-ui-view.tpl.php',
						   [compact('row','table','display','deleteForm','main_url'),
							isset($tableInfo['view_vars']) ? $tableInfo['view_vars']($row,$isDelete) : [],
						   ]);
}

//! Response to json request for full details on a specific field
function dbtable_ui_view_field(string $table,array $pkValues,$field)
{
	global $dbtable_ui_config;
	$row=dbtable_ui_fetch_row($table,$pkValues);
	if($row===null || !isset($row[$field])){dlib_not_found_404();}
	return ['value'=>$row[$field]];
}

//! Warn user if he wants to delete an object that is referenced as a foreign key by another object
function dbtable_ui_delete_foreign_key_warning(string $table,$row)
{
	global $dbtable_ui_config,$base_url;
	$refs=[];
	foreach($dbtable_ui_config['tables'] as $oclass=>&$tConfig)
	{
		$oschema=$oclass::getSchema();
		foreach($oclass::$dboStatic['foreign'] as $field)
		{
			if($oschema[$field]['class']===$table)
			{
				// Found other class field $oclass::$field with foreign key referencing this class $table
				$ids=db_one_col("SELECT ".($oclass::dboGetPKeyName())." FROM `".$oclass."` WHERE ".$field."=%d",
								$row->getPk());
				// special case: self reference
				if($table===$oclass){$ids=array_diff($ids,[$row->getPk()]);}
				if(count($ids))
				{
					$oclassInfo=$dbtable_ui_config['tables'][$oclass];
					if(isset($oclassInfo['setup'])){$oclassInfo['setup']($oclassInfo);}
					$refs[$oclass.'::'.$field]=['ids'=>$ids,
												'oclass'=>$oclass,
												'oci'=>$oclassInfo,];
				}
			}
		}
	}
	if(count($refs))
	{
		$refWarn='';
		foreach($refs as $refName=>$ref)
		{
			$refWarn.='<p>'.$refName.': <br/>';
			$oclass=$ref['oclass'];
			$oci   =$ref['oci'];
			$omain_url=val($oclassInfo,'main_url',$base_url.'/backend/'.$oci['urlClass']);
			foreach($ref['ids'] as $id){$refWarn.='<a href="'.ent($omain_url.'/'.$id).'">'.ent($id).'</a>, ';}
			$refWarn.='</p>';
		}
		return $refWarn;
	}
	return false;
}
//! Delete form for a DBObject : simply displays the DBObject with delete & cancel buttons.
function dbtable_ui_delete(string $table,array $pkValues)
{
	return dbtable_ui_view($table,$pkValues,true);
}

//! Creates tabs (links) for a dbtable_ui page. Tabs are a variable in $currentPage. 
//! The application should render these tabs in its main page template.
function dbtable_ui_tabs(string $table,$row,string $active)
{
	global $base_url,$currentPage;
	$tableInfo=dbtable_ui_table_setup($table);

	$table=$tableInfo['table'];
	$main_url=val($tableInfo,'main_url',$base_url.'/backend/'.$table);

	$tabs=[];
	$tabs['list']=['href'=>$main_url,'text'=>t('List')];
	$tabs['add' ]=['href'=>$main_url.'/add','text'=>ent(t('Create !table row',['!table'=>$tableInfo['table']]))];
	if($row)
	{
		$tabs['view']=['href'=>dbtable_ui_action_url($table,$row,'view'),'text'=>ent(t('View'))];
		$tabs['edit']=['href'=>dbtable_ui_action_url($table,$row,'edit'),'text'=>ent(t('Edit'))];
	}
	if(isset($tableInfo['tabs'])){$tableInfo['tabs']($tabs,$row,$active);}

	if(isset($tabs[$active])){$tabs[$active]['active']=true;}
	$currentPage->tabs=$tabs;
}

/** @} */

?>