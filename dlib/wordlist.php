<?php

// **********************************************************
// Experimental functions for handling lists of words
// This is temporary (8/2016). Remove it after a while if it is not really used.
// **********************************************************


//! Similar to dlib_simplify_text(), but also returns the positions of each word in the source $string
//! Numeric entities that code for letter chars and composed unicode (separate accent + letter) will produce incorrect results.
//! It is better to use clean html (normalized with entities removed).
//! Note: positions are NOT mb (performance), but will work with unicode
//! This function can be a performance bottleneck and has been optimized. Care should be taken not to make it slower.
function wordlist_build_simplified_with_pos($string,$isHTML=false)
{
	//$timer = microtime(true);
	//$tagBase='hpk6hgni2xvnzefhg'.mt_rand();
	while(true)
	{
		$tagBase=dlib_random_string(5,'alnumlc');
		if(!ctype_digit($tagBase[0]) && strpos($string,$tagBase)===false){break;}
	}
	$tags=[];
	$taggedString=preg_match_all('@(^|[^\p{L}\p{Nd}­])'.                        // word boundary begin by non-letter
								 ($isHTML ? '(?<!<)(?<!</)(?<!&)(?<!&#)' : ''). // skip html tags and entities
								 '([\p{L}\p{Nd}­]+)'.                           // followed by word itself
								 '(?=([^\p{L}\p{Nd}­]|$))@u',                   // finished by non-letter (lookahead)
								 $string,$matches,PREG_PATTERN_ORDER|PREG_OFFSET_CAPTURE);

	// Build $taggedString and store positions of each tag
	$tagsPos=[];
	$taggedString='';
	$prevPos=0;
	$ct=0;
	foreach($matches[2] as $i=>$match)
	{
		$tag=($ct++).$tagBase;
		$word=$match[0];
		$wordBegin=$match[1];
		$wordEnd  =$wordBegin+strlen($word);

		// Our split is not exactly the same as the one that dlib_simplify_text() would normally do.
		// Add spaces to guarantee that dlib_simplify_text() will really split here.
		$taggedWord=' '.'b'.$tag.$word.'e'.$tag.' ';

		$taggedString.=substr($string,$prevPos,$wordBegin-$prevPos).$taggedWord;
		$prevPos=$wordEnd;
		$tagsPos[$tag]=[$wordBegin,$wordEnd];
	}

	// Simplify the tagged string. Each word in the simplified string will be tagged.
	$simplifiedTS=dlib_simplify_text($taggedString,$isHTML);
	
	// Now remove tags from simplified words and list their positions in source $string
	$simplifiedWords=[];
	$wordsPos=[];
	foreach(explode(' ',$simplifiedTS) as $wordTS)
	{
		if(!preg_match('@^b([0-9]+'.$tagBase.')(.*)e\1$@',$wordTS,$match))
		{
			// Silently ignore words that are not tagged. This should not happen.
			continue;
		}
		$pos=$tagsPos[$match[1]];
		$wordsPos[]=[$pos[0],$pos[1],count($simplifiedWords)];
		$simplifiedWords[]=$match[2];
	}

	// useful for debugging, do not erase
	//$disp=$string;
	//foreach(array_reverse($wordsPos) as $n=>$pos)
	//{
	// 	$disp=mb_substr($disp,0,$pos['end'  ]).$pos['wn'].mb_substr($disp,$pos['end'  ]);
	// 	$disp=mb_substr($disp,0,$pos['begin']).$pos['wn'].mb_substr($disp,$pos['begin']);
	//}
	//vd($disp);

	return [$simplifiedWords,$wordsPos];
}


//! Finds a list of words inside a text (or html) and adds tags around them.
//! This is insensitive to case, accents and any non text characters or HTML tags between the words.
//! Example:
//!    $src='The, cat <img> (falls) asleep', $wordsToTag=['cat','falls'], $openTag="XX" $closeTag="YY" 
//!    Result: "The, XXcatYY <img> (XXfallsYY) asleep"
//! $tagEachWord===true: Tags are added around each word  (like previous example)
//! $tagEachWord===false: Tags are added around the whole list:  "The, XXcat <img> (fallsYY) asleep"
//! if $openTag===false, then erase the words or the whole wordlist (in that case $closeTag can be empty or a replacement)
function wordlist_add_tags($src,array $wordsToTag,$openTag=false,$closeTag=false,$tagEachWord=true,$isHTML=true)
{
	list($wordsSrc,$positions)=wordlist_build_simplified_with_pos($src,$isHTML);

	// *** First build a list of places in $wordsSrc where we found $wordsToTag
	$addTagList=[];
	for($srcPos=0;$srcPos<count($wordsSrc);$srcPos++)
	{
		$found=true;
		for($i=0;$i<count($wordsToTag);$i++)
		{
			if($wordsToTag[$i]!==$wordsSrc[$srcPos+$i]){$found=false;break;}
		}
		if($found)
		{
			$addTagList[]=$srcPos;
			$srcPos+=count($wordsToTag)-1;
		}
	}

	// *** Now add tags in $src
	if($tagEachWord)
	{
		// Walk through each word in reverse order so that $positions remain valid
		foreach(array_reverse($addTagList) as $startPos)
		{
			for($srcPos=$startPos+count($wordsToTag)-1;$srcPos>=$startPos;$srcPos--)
			{
				// Tag word at $srcPos
				$begin=$positions[$srcPos][0];
				$end  =$positions[$srcPos][1];
				$src=substr($src,0,$begin).$openTag.($openTag!==false ? substr($src,$begin,$end-$begin) : '').$closeTag.substr($src,$end);
			}
		}
	}
	else
	{
		if(!count($addTagList)){return $src;}
		$begin=$positions[$addTagList[0                   ]                     ][0];
		$end  =$positions[$addTagList[count($addTagList)-1]+count($wordsToTag)-1][1];
		$src=substr($src,0,$begin).$openTag.($openTag!==false ? substr($src,$begin,$end-$begin) : '').$closeTag.substr($src,$end);
	}
	return $src;
}


//! Finds (sub)wordlists that are common to one of $commonList and $addDoc.
//! When searching for common lists for a set of documents call this function for each document.
function wordlist_find_common_between_set_and_doc($commonList,$addDoc,$minSize)
{
	$res=[];
	$ct=0;
	foreach($commonList as $common)
	{
		$found=wordlist_find_common_between_2_docs($common,$addDoc,$minSize);
		$res=array_merge($res,$found);
	}
	$res=wordlist_longest_unique($res);
	return $res;
}

//! Returns all common wordlists for to docs (wordlists).
//! Example: 
//! $words1=["abc","cde","fgh","ijk","lmn"] 
//! $words2=["xyz","abc","cde","rst","ijk","lmn"] 
//! returns: [["abc","cde"],["ijk","lmn"]] 
function wordlist_find_common_between_2_docs($words1,$words2,$minSize)
{
	//vd(count($words1),count($words2));
	$index1=[];
	foreach($words1 as $i=>$word){$index1[$word][]=$i;}

	$done=[];
	$common=[];
	foreach($words2 as $w2Start=>$word)
	{
		foreach(val($index1,$word,[]) as $w1Start)
		{
			if(isset($done[$w1Start.':'.$w2Start])){continue;}
			$newCommon=[];
			for($i=0;($w1Start+$i)<count($words1) && ($w2Start+$i)<count($words2);$i++)
			{
				if($words1[$w1Start+$i]!==$words2[$w2Start+$i]){break;}
				$newCommon[]=$words1[$w1Start+$i];
				$done[($w1Start+$i).':'.($w2Start+$i)]=true;
			}
			if(count($newCommon)>=$minSize){$common[]=$newCommon;}
		}
	}
	$common=wordlist_longest_unique($common);
	return $common;
}

//! Return unique and longest lists of words (starting by longest)
//! This removes lists that are contained in other lists.
//! Expects simplified words only.
function wordlist_longest_unique($wordLists)
{
	usort($wordLists,function($a,$b){$ca=count($a);$cb=count($b);return $ca===$cb ? 0 : (($ca > $cb) ? -1 : 1);});

	$check=[];
	$res=[];
	foreach($wordLists as $words)
	{
		$checkWords='/'.implode('/',$words).'/';
		if(count(preg_grep('@'.$checkWords.'@',$check))){continue;}
		$check[]=$checkWords;
		$res[]=$words;
	}
	return $res;
}

?>