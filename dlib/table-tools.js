// namespace
(function() {
window.sortable_table = window.sortable_table || {};
var ns=window.sortable_table;// shortcut

$(document).ready(function()
{
	table_header_rotate_setup();
	js_sortable_table_setup();
	server_sortable_table_setup();
	sticky_table_top_setup();
	column_hover_setup();
});

function table_header_rotate_setup()
{
	$('table').has('th.rotate,th.auto-rotate').each(table_header_rotate_setup_table);
}

function table_header_rotate_setup_table(unused,table)
{
	table=$(table);
	table.find('th.auto-rotate').each(function()
	{
		var th=$(this);
		var span=th.find('span');
		if(span.width()>th.width()){th.addClass('rotate');table.addClass('has-rotate');}
		th.removeClass('auto-rotate');
	});

	if(table.hasClass('has-rotate')){table.addClass('crowded');}

	var maxHeight=0;
	table.find('th.rotate').each(function()
	{
		var th=$(this);
		var span=th.find('span');
		var h=span[0].getBoundingClientRect().height;
		//console.log($.trim(th.text()),th.height(),span.height(),span[0].clientHeight,h);
		maxHeight=Math.max(maxHeight,h);
	});
	if(maxHeight>25)
	{
		table.css('margin-top',(maxHeight-25)+'px');
		table.parents('.table-render-wrapper').find('.table-render-search').css('margin-bottom',(maxHeight-25)+'px');
	}
	table.trigger('demos:table-rotate-finished',[table]);
}


function js_sortable_table_setup()
{
	$('table.sortable.js-sort').each(js_sortable_table_setup_table);
}

function js_sortable_table_setup_table(unused,table)
{
	table=$(table);
	table.find('tr:first-child').eq(0).find('th').not('.no-sort').addClass('sort-col');
	table.on('mousedown','th.sort-col',function(e)
	{
		if(e.which!=1){return;}
		e.preventDefault();
		var table=$(this).parents('table').first();
		// find number of clicked column
		var a=$(this);
		var pos;
		for(pos=0;pos<100;pos++){a=a.prev();if(a.length==0){break;}}
		// find direction of sort (and set / reset classes)
		var ascending=!$(this).hasClass('sort-ascending');
		$(this).parent().children("th").removeClass('sort-ascending' );// reset classes
		$(this).parent().children("th").removeClass('sort-descending');// reset classes
		$(this).addClass("sort-"+(ascending ?  'ascending' : "descending"));
		// special case: multiple tbody's : sort each one independently if table has "sort-tbody" class
		if(table.hasClass('sort-tbody'))
		{
			table.find('tbody').each(function()
			{
				js_sortable_table_sort_column($(this),pos,ascending);
			});
		}
		else
		{
			// normal case (sort all table)
			js_sortable_table_sort_column(table,pos,ascending);
		}
	});
}

//! Sort a table (or a tbody) according a sortCol (col. number).
//! The first line is assumed to be a header and is ignored.
function js_sortable_table_sort_column(table,sortCol,ascending)
{
	var rows=[];
	table.find('tr').each(function(nbRow)
    {
		var tr=$(this);
		if(nbRow===0 || tr.hasClass('sticky-table-top') || tr.hasClass('sticky-table-top-fixed')){return true;}
		var td=tr.children().eq(sortCol);
		var text=td.attr('data-sortable');
		if(typeof text==='undefined'){text=$.trim(td.text());}
		var isNumber=$.isNumeric(text);
		if(isNumber){text=parseInt(text);}
		rows.push({text:text,el:this,isNumber:isNumber});
	});

	rows.sort(function(ra, rb)
	{
		var a=ra.text;
		var b=rb.text;
		if(ra.isNumber!==rb.isNumber){a=''+a;b=''+b;}
		if(typeof a==='string' && typeof b==='string' &&
		   typeof a.localeCompare!=='undefined')
		{return (ascending ? 1 : -1)*a.localeCompare(b);}
		if (a<b) return ascending ? -1 : 1;
		if (a>b) return ascending ?	 1 :-1;
		return 0;
	});

	// Hide table and re-insert rows into tbody
	// Inserting into tbody (not table) makes a huge performance difference in Firefox
	var wasVisibile=table.is(':visible');
	if(wasVisibile){table.hide();}
	var tbody=table.find('tbody');
	if(tbody.length===0){tbody=table;}
	for(var i=0;i<rows.length;i++)
	{
		tbody.append(rows[i].el);
	}
	if(wasVisibile){table.show();}

}

function server_sortable_table_setup()
{
	$('table.sortable.server-sort').each(server_sortable_table_setup_table);
}

function server_sortable_table_setup_table(unused,table)
{
	table=$(table);
	var tableLabel=table.attr('data-table-label');
	table.find('tr:first-child th').not('th[data-name]').addClass('no-sort');
	table.find('tr:first-child th').not('.no-sort'     ).addClass('sort-col');
	var url=window.location.href;
	var currentSort=url.match(new RegExp("[?&]"+tableLabel+"order(Asc|Desc)=([^&]*)"));
	// Clickable column headers
	table.on('mousedown','th.sort-col',function(e)
	{
		if(e.which!=1){return;}	
		e.stopPropagation();
		e.preventDefault();
		$(this).css('background-color','#ff0');
		url=url.replace(new RegExp("&"  +tableLabel+"order(Asc|Desc)=[^&]*","g"),'');
		url=url.replace(new RegExp("[?]"+tableLabel+"order(Asc|Desc)=[^&]*","g"),'?');
		url=url.replace(/\?$/,'');
		url+=(/[?]/.test(url) ? '&' : '?')+
			tableLabel+
			'order'+(currentSort!==null && currentSort[1]=='Asc'  ? 'Desc' : 'Asc')+'='+
			$(this).attr('data-name');
		window.location=url;
	});
	// Add classes to col. headers to display sort
	table.find('tr:first-child th').removeClass('sort-ascending').removeClass('sort-descending');
	if(currentSort!==null)
	{
		table.find('th.sort-col[data-name='+currentSort[2]+']').
			addClass('sort-'+(currentSort[1]=='Asc' ? 'ascending' : 'descending'));
	}
}


//! Sticky table top : Table's first row stays visible when it scrolls out of window.
//! The sticky row is a clone of the first row. Widths of th's are set so that they match with the original row.
function sticky_table_top_setup()
{
	$('.sticky-table-top').each(sticky_table_top_setup_table);
}
function sticky_table_top_setup_table(unused,refRow)
{
	refRow=$(refRow);
	var table=refRow.parents('table').eq(0);
	var fixedRow=refRow.clone();
	var isBootstrap=$('.navbar-fixed-top').length!=0;
	fixedRow.css({position:'fixed',top: isBootstrap ? $('.navbar-fixed-top').height() : 0,display: 'none','z-index' : 1});
	fixedRow.removeClass('sticky-table-top');
	fixedRow.addClass('sticky-table-top-fixed');
	// copy width's of each th to fixed row
	var i=0;
	refRow.find('th').each(function()
	{
		fixedRow.find('th').eq(i).width(Math.ceil($(this).width()));
		i++;
	});
	fixedRow.width(refRow.width());
	fixedRow.css('left',table.offset().left);
	fixedRow.find('th.rotate').each(function(){$(this).attr('title',$.trim($(this).text()));});

	refRow.after(fixedRow);
	var refRowHeight=refRow.height();
	var lastRow=table.find('tr').last();

	var isVisible=false
	$(window).scroll(function()
	{
		var rectRef  =refRow [0].getBoundingClientRect();
		var rectLast =lastRow[0].getBoundingClientRect();
		//console.log(isVisible,'refRow',rectRef,'lastRow',rectLast);
		if(rectRef.top <0 &&
		   rectLast.top>0	  )
		{
			if(!isVisible)
			{
				isVisible=true;
				fixedRow.css('display','table-row');
			}
		}
		else
		{
			if(isVisible){isVisible=false;fixedRow.css('display','none');}
		}
	});
}

function column_hover_setup()
{
	$('table.column-hover').each(column_hover_setup_table);
}

function column_hover_setup_table(unused,table)
{
	table=$(table);

	// Setup table classes
	table.find('tr:nth-child(1)').first().find('td,th').each(function(i,th)
	{
		if($(th).hasClass('no-column-hover')){return;}
		table.find('tr>td:nth-child('+(i+1)+')').addClass('ch-'+i);
		table.find('tr>th:nth-child('+(i+1)+')').addClass('ch-'+i);
	});

	// Highlight column on hover
	table.on('mouseleave mouseenter','td,th',function(e)
	{
		var colClass=this.className.match(/ch-[^ ]+/);
		if(colClass===null){return;}
		colClass=colClass[0];
		if(e.type==='mouseenter')
		{
			// only for performance
			if($(this).hasClass('colhover')){return;}
			// just in case mouseleave missed something
			table.find('.colhover').removeClass('colhover'); 
			table.find('.'+colClass).addClass('colhover');
		}
		else
		{
			table.find('.'+colClass).removeClass('colhover');
		}
	});
}

//// Exports:
//window.sortable_table.xyz=xyz;
//// Global exports:
//window.xyz=xyz;

// end namespace wrapper
}());
