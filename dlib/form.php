<?php

/** @defgroup form form
 * Display and process forms using an array describing each form item.
 * Several item types (int, email, textfield, textarea...) are buit-in.
 * Validation rules are provided for certain types.
 * You can specify custom functions for validating, submission, etc.
 * Form items can be automatically generated for a DBObject and customized.
 *
 * **Module use**: DBObject (optional), HtmlPageData FIXME (optional)
 *  @{
 */

//! Renders a form or does something if the form is being submitted.
//! Most of the time, this is the only form function you will need.
//! $step: returned value stating what step of the form proceesing we are at ('display','skip','ok-no-redirect','validation-error')
//! You normally don't need to use this. It might be usefull for ajax forms.
function form_process(array $items,array $formOptions=[],string &$step=null)
{
	$formId=val($formOptions,'id','no-id-form');

	// **** This is a simple display request, not a form submission
	// Just return html for this form
	if(!isset($_POST['form-id']) || $_POST['form-id']!==$formId)
	{
		if(isset($formOptions['pre-render'])){$formOptions['pre-render']($items,$formOptions,false);}
		$step='display';
		return form_render($items,$formOptions);
	}

	// **** This is a form submission. 

	// security
	dlib_check_form_token($formId);

	// set default item type (just for convenience)
	foreach($items as &$item)
	{
		if(!isset($item['type'])){$item['type']='html';}
	}
	unset($item);


	// **** $activeButton: which button was pressed in form
	$activeButton=false;
	foreach($items as $name=>$item)
	{
		if($item['type']==='submit' && isset($_POST[$name])){$activeButton=$name;break;}
	}
	if($activeButton===false){fatal('form_process: no active button');}

	// **** skip-processing: special button : no processing : (example: delete button)
	if(val($items[$activeButton],'skip-processing'))
	{
		// call active button submit handlers
		foreach(form_callbacks($items[$activeButton],'submit') as $cb)
		{
			$items[$activeButton][$cb]($items,$activeButton);
		}
		// call active button redirect
		if(isset($items[$activeButton]['redirect']))
		{
			global $base_url;
			$url=$items[$activeButton]['redirect'];
			if(!preg_match('@^https?://@',$url)){$url=$base_url.'/'.$url;}
			dlib_redirect($url);
		}
		$step='skip';
		return;
	}

	// **** set item values
	foreach(array_keys($items) as $name)
	{
		$item=&$items[$name];

		// Set item value from POST (unless this is a fixed value)
		if(!isset($item['value']))
		{
			// Check for strange cases (something really wrong)
			// This does not work: items that have "disabled" property (eg, disabled by js) do not appear in $_POST.
			// Workaround seems too complex... just remove this safety test :-(
			//if(!isset($_POST[$name]) &&
			//   array_search($item['type'],['textfield','password','int','email','hidden','float','url','file','spamcheck',
			// 				 'timestamp','color','textarea','htmlarea','select'])!==false &&
			//   !($item['type']==='select' && val($item,'multiple')!==false))
			//{
			// 	fatal('form_process: strange : no value found in $_POST for '.$name);
			//}
			$item['value']=val($_POST,$name,'');
		}
			
		if($item['type']==='select' && val($item,'multiple') && $item['value']===''){$item['value']=[];}
		if($item['type']==='checkbox'){$item['value']=isset($_POST[$name]);}
		if($item['type']==='array_of_arrays'){$item['value']=json_decode($item['value'],true);}
		// HTML5 color input cannot be empty, so we add extra checkbox 
		if($item['type']==='color' && !val($item,'required',false) && !isset($_POST[$name.'_enable']))
		{
			$item['value']='';
		}

		// trim (remove whitespace at begining and end)
		switch($item['type'])
		{
		case 'password': 
		case 'search': 
		case 'textfield': 
		case 'email': 
		case 'url': 
			if(val($item,'trim',true)){$item['value']=trim($item['value']);}
			break;
		}

	}
	unset($item);// PHP bug: https://bugs.php.net/bug.php?id=29992

	// **** validation
	foreach($items as $name=>&$item)
	{
		// is this value "empty" ?
		$isEmpty=false;
		switch($item['type'])
		{
		// never empty
		case 'checkbox':
		case 'value':
		case 'submit':
			break;
		case 'select':
			if(val($item,'multiple')){$isEmpty=(count($item['value'])===0);}
			break;
		case 'radios':$isEmpty=!isset($_POST[$name]);
			break;
		case 'array_of_arrays':
			$isEmpty=(count($item['value'])===0);
			break;
		case 'file':
			$isEmpty=$_FILES[$name]['error']===UPLOAD_ERR_NO_FILE;
			break;
		// empty string
		default:
			$isEmpty=trim($item['value'])==='';
		}
		$item['value-is-empty']=$isEmpty;
		
		$isRequired=val($item,'required',false);

		if($item['type']==='radios'){$isRequired=true;}

		// very special case: user supplied function checks if required or not
		if(!is_bool($isRequired)){$isRequired=$isRequired($item['value'],$name,$items);}

		// do not validate empty items that are not required
		if($isEmpty && !$isRequired){continue;}
		
		// do not further validate empty items that ARE required
		if($isEmpty && $isRequired){if(!isset($item['error'])){$item['error']=ent(t('This field is required'));}continue;}
		
		// **** item type validation
		switch($item['type'])
		{
		case 'select': 
			if(val($item,'options-validate',true))
			{
				$vals=$item['value'];
				if(!val($item,'multiple')){$vals=[$item['value']];}
				foreach($vals as $val)
				{
					if(!isset($item['options'][$val]))
					{dlib_bad_request_400('form_process: strange select value for '.$name);}
				}
			}
			break;
		case 'radios': 
			if(!isset($item['options'][$item['value']]))
			{dlib_bad_request_400('form_process: strange value for radio '.$name);}
			break;
		case 'int': 
			if(filter_var($item['value'], FILTER_VALIDATE_INT  )===false){$item['error']=ent(t('Item should be a number'));}
			break;
		case 'float': 
			if(filter_var($item['value'], FILTER_VALIDATE_FLOAT)===false){$item['error']=ent(t('Item should be a number'));}
			break;
		case 'url': 
			if(filter_var($item['value'], FILTER_VALIDATE_URL  )===false ||
			   !preg_match('@^https?://@',trim($item['value']))) // Note: we only accept http urls, not ftp, mailto...
			{$item['error']=ent(t('Invalid url'));}
			break;
		case 'email': 
			$error=form_validate_email($item['value'],$item);
			if($error!==null){$item['error']=$error;}
			break;
		case 'timestamp': 
			if(!preg_match('@^(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})[ T]'.
						   '(?P<hour>\d{2}):(?P<minute>\d{2})(:(?P<second>\d{2}))?$@',$item['value'],$cm) ||
			   !checkdate($cm['month'],$cm['day'],$cm['year']) ||
			   (isset($cm['hour'  ]) && $cm['hour'  ]>23) ||
			   (isset($cm['minute']) && $cm['minute']>59) || 
			   (isset($cm['second']) && $cm['second']>59))
			{$item['error']=ent(t('Invalid timestamp : expected YYYY-MM-DD HH:MM:SS'));}
			break;
		case 'date': 
			if(!preg_match('@^(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})$@',$item['value'],$cm) ||
			   !checkdate($cm['month'],$cm['day'],$cm['year']))
			{$item['error']=ent(t('Invalid date'));}
			break;
		case 'color': 
			if(!preg_match('@^#[0-9a-f]{6}$@',$item['value']))
			{$item['error']=ent(t('Invalid color : expected 6 digit hex, with #, for example: "#789abc"'));}
			break;
		case 'array_of_arrays':
			require_once 'dlib/array-of-arrays.php';
			$aofaErrors=array_of_arrays_validate($item['value'],$item['aofa-options'],true);
			if($aofaErrors!==false){$item['error']=$aofaErrors;}
			break;
		case 'spamcheck':
			global $dlib_config;
			// Check for text that should have been copied by js (or by hand if js is missing)
			$spamtext=strtr(substr(hash('sha256',session_id().':'.date("d/m/Y").':spamcheck:'.$dlib_config['secret']),0,4),"0123456789","ghijklmnop");
			if($item['value']!==$spamtext){$item['error']=ent(t('You must enter the anti-spam text to prove you are not a robot trying to spam us.'));}
			// Check for invalid changes in hidden fields (canary)
			if($_POST['yellow-bird-1']!='' || 
			   $_POST['yellow-bird-2']!='yb'.$spamtext){dlib_bad_request_400("Something strange happened. Please contact admin.");}
			// Check for very quick form submits (default < 1 sec )
			$oldTime=(float)$_POST['spamcheck-time'];
			if((microtime(true)-$oldTime)> 3600*24){dlib_bad_request_400('Form expired.');}
			if((microtime(true)-$oldTime)< val($item,'spamcheck-delay',1)){$item['error']=ent(t('That was too fast. Please retry that slowly.'));}
			break;
		case 'file':
			if(!isset($_FILES[$name]['error']) ||
			   $_FILES[$name]['error']!==UPLOAD_ERR_OK)
			{
				$messages=[
						   UPLOAD_ERR_INI_SIZE=>"The uploaded file exceeds the upload_max_filesize directive in php.ini",
						   UPLOAD_ERR_FORM_SIZE=>"The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form",
						   UPLOAD_ERR_PARTIAL=>"The uploaded file was only partially uploaded",
						   UPLOAD_ERR_NO_FILE=>"No file was uploaded",
						   UPLOAD_ERR_NO_TMP_DIR=>"Missing a temporary folder",
						   UPLOAD_ERR_CANT_WRITE=>"Failed to write file to disk",
						   UPLOAD_ERR_EXTENSION=>"File upload stopped by extension", 
						   ];
				$item['error']=ent(t('File upload failed.').' '.val($messages,$_FILES[$name]['error'],''));
			}
			break;
		}

		// **** custom per-item validation by user provided regexp
		$validateRegexp=val($item,'validate-regexp');
		if($validateRegexp!==false)
		{
			if(!preg_match($validateRegexp,$item['value']))
			{
				$item['error']=val($item,'validate-regexp-message',ent(t('Invalid item')));
			}
		}

		// **** custom per-item validation by user provided function
		$validate=val($item,'validate');
		if($validate!==false && $item['type']!=='submit')
		{
			$vres=$validate($item['value'],$item);
			if($vres!==true && $vres!==null){$item['error']=$vres;}
		}
	}
	unset($item);// PHP bug: https://bugs.php.net/bug.php?id=29992

	$ok=true;
	// **** call active button validate handlers
	foreach(form_callbacks($items[$activeButton],'validate') as $cb)
	{
		$ret=$items[$activeButton][$cb]($items);
		if($ret!==null && !$ret){$ok=false;}
	}
	// **** custom whole-form validation by user provided function
	if(isset($formOptions['validate']))
	{
		$ret=$formOptions['validate']($items,$activeButton);
		if($ret!==null && !$ret){$ok=false;}
	}

	// Check if there are any errors
	foreach($items as $item){if(isset($item['error'])){$ok=false;}}

	if($ok)
	{
		// **** This form is valid.

		// cast values to correct type
		foreach($items as &$item)
		{

			switch($item['type'])
			{
			case 'int':       $item['value']=$item['value-is-empty'] ? false :    (int  )$item['value']; break;
			case 'float':     $item['value']=$item['value-is-empty'] ? false :    (float)$item['value']; break;
			case 'timestamp': $item['value']=$item['value-is-empty'] ? 0     : strtotime($item['value']);break;
			case 'array_of_arrays':
				require_once 'dlib/array-of-arrays.php';
				$item['value']=array_of_arrays_cast($item['value'],$item['aofa-options']);
				break;
			}
		}
		unset($item);

		// call pre-submit handlers for each item
		foreach($items as &$item)
		{
			foreach(form_callbacks($item,'pre-submit') as $cb)
			{
				$item[$cb]($item['value'],$item,$activeButton,$items);
			}
		}
		unset($item);

		// call active button submit handlers
		foreach(form_callbacks($items[$activeButton],'submit') as $cb)
		{
			$items[$activeButton][$cb]($items,$activeButton);
		}
		// call $formOptions submit handlers
		foreach(form_callbacks($formOptions,'submit') as $cb)
		{
			$formOptions[$cb]($items,$formOptions,$activeButton);
		}
		// call active button redirect
		if(isset($items[$activeButton]['redirect']))
		{
			global $base_url;
			$url=$items[$activeButton]['redirect'];
			if(!preg_match('@^https?://@',$url)){$url=$base_url.'/'.$url;}
			dlib_redirect($url);
		}
		// call form options redirect
		if(isset($formOptions['redirect']))
		{
			global $base_url;
			if($formOptions['redirect']===false){$step='ok-no-redirect';return;}
			$url=$formOptions['redirect'];
			if(!preg_match('@^https?://@',$url)){$url=$base_url.'/'.$url;}
			dlib_redirect($url);
		}

		// No redirects found, redirect to same page.
		// If we redisplay the form here, we would need to empty all fields (default values).
		dlib_redirect(dlib_current_url());
	}
	else
	{
		// **** This form is not valid.

		$validationMessages=[];
		foreach($items as $name=>$item)
		{
			if(isset($item['error']))
			{
				$validationMessages[$name]=ent(val($item,'title',$name)).': '.
					($item['error']===false ? 'Invalid' : $item['error']);
			}
		}

		// Display validation errors as messages
		if(isset($formOptions['validation-messages'])){$formOptions['validation-messages']($validationMessages,$items);}
		else
		{
			foreach($validationMessages as $message){dlib_message_add($message,'error');}
		}

		// Re-display form (this is a validation error)
		if(isset($formOptions['pre-render'])){$formOptions['pre-render']($items,$formOptions,true);}
		$step='validation-error';
		return form_render($items,$formOptions);
	}
}

function form_callbacks(array $item,string $cbName)
{
	$res=[];
	foreach($item as $name=>$value)
	{
		if(preg_match('@^'.$cbName.'([+-][0-9]+)?@',$name,$matches))
		{
			$res[$name]=intval(val($matches,1));
		}
	}
	asort($res);
	return array_keys($res);
}

//! Validate a single email address.
//! If $item['check-dns'] is true, also check DNS MX or A records.
//! If $item['allow-display-name'] it allows address like: "DispName <actualaddress@example.org>"
function form_validate_email(string $value,array $item=[])
{
	$value=trim($value);
	$parts=dlib_email_address_parts($value);
	// Display name : DispName <actualaddress@example.org>
	if(val($item,'allow-display-name')){$value=$parts['address'];}

	// ex: root@localhost
	if(preg_match('/^[a-z0-9_-]+@[a-z0-9-]+$/i',$value))
	{
		if(!val($item,'check-dns')){return;}
	}
	else
	if(filter_var($value, FILTER_VALIDATE_EMAIL)===false){return ent(t('Invalid email address').': '.$value);}

	if(val($item,'check-dns') &&  !dlib_email_check_domain($value))
	{
		return ent(t('domain "!domain" is incorrect.',['!domain'=>$parts['domain']]));
	}
}

//! Returns html of a form built from a list of items.
//! Optionally, can return an array of rendered items.
//! Each item is descriped by an array:
//! type => (possible values: see "$allowedItemTypes" in code below )
//! default-value => the value that will be displayed in this item (unless this is a validation error redisplay)
//! title => Title displayed in label
//! Each item is rendered in form_render_item() using two templates defined in form_item_templates()
//! Inspired by : http://api.drupal.org/api/drupal/developer!topics!forms_api_reference.html/6
function form_render(array $items,array $formOptions=[])
{
	global $currentPage;
	// This list of item types is usefull only for documentation: (enforcing it ensures the list is up to date).
	static $allowedItemTypes=['textfield','password','checkbox','int','search','email','hidden','float','url','file','spamcheck','timestamp','date','color', // <input>
							  'textarea','htmlarea', // textarea based
							  'select','radios', // multiple choice
							  'submit','button', // buttons
							  'custom', // rendered by callback function
							  'array_of_arrays', 
							  'data','html','fieldset','fieldset-end' // not really form items
							 ];

	$formId=val($formOptions,'id','no-id-form');

	$css=val($formOptions,'css','dlib/form.css');
	if(isset($currentPage) && $css!==false){$currentPage->addCss($css);}

	$formAttributes=val($formOptions,'attributes',[]);
	$formAttributes['class'][]='dlib-form';

	$hasFileUpload=false;
	foreach($items as $item){if(val($item,'type')==='file'){$hasFileUpload=true;}}
	if($hasFileUpload){$formAttributes['enctype']="multipart/form-data";}

	$render=[];

	$out='';
	$out.='<form method="post" '.($formId==='no-id-form' ? '' : 'id="'.ent($formId).'" ').' '.
		dlib_render_html_attributes($formAttributes).'>';
	$out.='<input type="hidden" name="form-id" value="'.ent($formId).'"/>'."\n";
	$out.=dlib_add_form_token($formId);

	if($hasFileUpload){$out.='<input type="hidden" name="MAX_FILE_SIZE" value="2000000" />';}

	$render['form-start']=$out;

	foreach($items as $name=>$item)
	{
		if(!preg_match('@^\w+(-\w+)*$@',$name)){fatal('form_render: invalid item name:'.$name);}
		
		$type=val($item,'type','html');
		if(array_search($type,$allowedItemTypes)===false){fatal('form: unknown item type '.$item['type'].' for :'.$name);}


		// Add form-submit-buttons wrapper before a series of submit buttons
		if(val($item,'type')==='submit' && 
		   (dlib_prev_key($items,$name)===false || val($items[dlib_prev_key($items,$name)],'type')!='submit'))
		{
			$render[$name.'-buttons-wrapper-begin']='<div class="form-submit-buttons">';
		}

		$render[$name]=form_render_item($name,$item,$item['custom-templates'] ?? $formOptions['custom-templates'] ?? false);

		// Close form-submit-buttons wrapper after a series of submit buttons
		if(val($item,'type')==='submit' && 
		   (dlib_next_key($items,$name)===false || val($items[dlib_next_key($items,$name)],'type')!='submit'))
		{
			$render[$name.'-buttons-wrapper-end']='</div>';
		}
	}

	$render['form-end']='</form>'."\n";

	if(val($formOptions,'render-array')){return $render;}
	return implode('',$render);
}

function form_render_item(string $name,array $item,$customTemplates=false)
{
	global $currentPage;
	$out='';
	$type=val($item,'type','html');
	// ***** process "fake" form items (html display or internal data)
	$isStd=false;
	switch($type)
	{		
	case 'html':  
		if(!isset($item['html'])){fatal('form_render: no html field for html form item : '.$name);}
		$out.=$item['html'];break;
	case 'data': break;
	case 'fieldset': 
		$currentPage->addJs('lib/jquery.js');
		$currentPage->addJs('dlib/form.js');
		$attributes=val($item,'attributes',[]);
		$attributes['id']='edit-'.str_replace('_','-',$name);
		if(val($item,'collapsible'))
		{
			$attributes['class'][]='collapsible';
		}
		if(val($item,'collapsed'))
		{
			$attributes['class'][]='collapsed';
		}
		$out.='<fieldset '.dlib_render_html_attributes($attributes).'>'.
			(isset($item['title']) ? '<legend><span>'.$item['title'].'</span></legend>' : '');
		$out.='<div class="collapsible-content">';
		break;
	case 'fieldset-end': 
		$out.='</div></fieldset>';
		break;
	default: $isStd=true;
	}
	if(!$isStd){return $out;}

	// *********
	// This is a standard form item, rendered with a template from $templates, wrapped in $wrapperTemplate

	$attributes=val($item,'attributes',[]);
	if(isset($item['value'])){$value=$item['value'];}
	else                     {$value=val($item,'default-value');}
	$cname=str_replace('_','-',$name);
	$attributes['name']=$name;
	$options='';
	$radios='';
	$custom='';

	// ** Type specific processing. 
	// Setup variables for rendering (which is done with templates)
	$templateName='input';
	switch($item['type'])
	{
	case 'search':
	case 'email': 
	case 'int': 
	case 'url': 
	case 'float': 
	case 'textfield': 
		$attributes['type']='text';
		if($item['type']==='search' || 
		   $item['type']==='email'  || 
		   $item['type']==='url'  	  ){$attributes['type']=$item['type'];}
		if($item['type']==='int'      ){$attributes['type']='number';}
		if($item['type']==='url' && !isset($attributes['pattern'])){$attributes['pattern']='https?://.*';}
		if($item['type']==='email'){$attributes['autocomplete']='email';}
		$attributes['size'     ]=val($attributes,'size'     ,60);
		if($item['type']!=='int' && $item['type']!=='float'){$attributes['maxlength']=val($attributes,'maxlength',128);}
		$attributes['class'    ][]='form-text';
		break;
	case 'color':
		$attributes['type']='color';
		$attributes['size'     ]=val($attributes,'size'     ,7);
		$attributes['maxlength']=val($attributes,'maxlength',7);
		$attributes['class'    ][]='form-color';
		$attributes['class'    ][]='color {hash:true,caps:false,required:true}';
		// HTML 5 color input does not allow unset colors :-( 
		// We need to add a checkbox
		if(!val($item,'required'))
		{
			$isEmpty=$value==='' || $value===false;
			$item['wrapper-attributes']['class'][]=$isEmpty ? 'color-empty' : 'color-set';
			$id='edit-'.ent($cname);
			$ide=$id.'-enable';
			$item['field-suffix']=
				'<input type="checkbox" '.($isEmpty ? '' : 'checked').' '.
				'id="'.$ide.'" name="'.ent($name).'_enable" '.
				'onclick="this.parentNode.className='.
				'this.parentNode.className.replace(/color-(empty|set)/,this.checked ? \'color-set\' : \'color-empty\');">'.
				val($item,'field-suffix','');
			// only usefull for a special case : user clicked on label of hidden color
			$attributes['onclick']="document.getElementById('".$ide."').checked='checked';this.parentNode.className=this.parentNode.className.replace('color-empty','color-set')";
		}
		global $currentPage;
		$currentPage->addJs('lib/jscolor/jscolor.js','file',['cache-checksum'=>false]);
		break;
	case 'timestamp':
		$attributes['type']='datetime-local';
		$attributes['class'][]='check-support';
		// normally value is int, except on validate error : still in string form
		$value=$value===0 ? '' :
			(is_int($value) ? strftime('%Y-%m-%dT%H:%M:%S',$value) : $value);
		$currentPage->addJs('lib/jquery.js');
		$currentPage->addJs('dlib/form.js');
		break;
	case 'date':
		$attributes['type']='date';
		$attributes['class'][]='check-support';
		$currentPage->addJs('lib/jquery.js');
		$currentPage->addJs('dlib/form.js');
		break;
	case 'array_of_arrays':
		require_once 'dlib/array-of-arrays.php';
		static $aOfAOk=false;
		if(!$aOfAOk)
		{
			$aOfAOk=true;
			$currentPage->addJs('lib/jquery.js');
			$currentPage->addJs('$(document).ready(function() {array_of_arrays("input[type=text].array_of_arrays:not(.custom-init)");});','inline');
			$currentPage->addJs(array_of_arrays_js_translations(),'inline');
			$currentPage->addJs('dlib/array-of-arrays.js');
			$currentPage->addCss('dlib/array-of-arrays.css');
		}
		$attributes['type']='text';
		$attributes['class'][]='array_of_arrays';
		$attributes['data-aofa']=json_encode($item['aofa-options']);
		$value=json_encode($value);
		break;
	case 'hidden': 
		$attributes['type']='hidden';
		break;
	case 'spamcheck': 
		$attributes['type']='text';
		$item['title']=t('Anti-spam check');
		global $dlib_config;
		$spamtext=strtr(substr(hash('sha256',session_id().':'.date("d/m/Y").':spamcheck:'.$dlib_config['secret']),0,4),"0123456789","ghijklmnop");
		$id='edit-'.ent($cname);
		$item['field-suffix']=
			// spammers often don't have javascript enabled
			'<script type="text/javascript">'.
			'document.getElementById("'.$id.'").setAttribute("value","'.$spamtext.'");'.
			'document.getElementById("'.$id.'-wrapper").style.display="none";'.
			'</script>'.
			// spammers sometimes autofill fields, this is a simple check (canary)
			'<input type="text" name="yellow-bird-1" value=""                class="form-yellow-bird"/>'.
			'<input type="text" name="yellow-bird-2" value="yb'.$spamtext.'" class="form-yellow-bird"/>'.
			'<input type="hidden" name="spamcheck-time" value="'.microtime(true).'" class="form-yellow-bird"/>';
		$item['description']=t("Anti-spam check : please enter the following text '!text'",['!text'=>$spamtext]);
		break;
	case 'password': 
		$value='';
		$attributes['type']='password';
		break;
	case 'file': 
		$attributes['type']='file';
		break;
	case 'checkbox': 
		$templateName='checkbox';
		$attributes['checked']=(bool)$value;
		break;
	case 'radios': 
		$templateName='radios';
		unset($attributes['name']);
		foreach($item['options'] as $key=>$val)
		{
			$radios.='<label class="form-radio-item '.str_replace('_','-',$name).'-'.ent($key).'">';
			$radios.='<input type="radio" name="'.$name.'" value="'.ent($key).'" '.($key==$value ? 'checked="checked"' : '').'>';
			$radios.=ent($val);
			$radios.='</label>';
		}
		break;
	case 'textarea': 
		$templateName='textarea';
		unset($attributes['value']);
		$attributes['cols']=val($attributes,'cols',60);
		$attributes['rows']=val($attributes,'rows',5);
		break;
	case 'htmlarea': 
		$templateName='textarea';
		break;
	case 'select': 
		$templateName='select';
		$isMultiple=val($item,'multiple');
		if($isMultiple)
		{
			if(!isset($attributes['size'])){$attributes['size']=count($item['options']);}
			$attributes['multiple']=true;
			$attributes['name'].='[]';
		}
		foreach($item['options'] as $key=>$val)
		{
			// FIXME : this is $key==$value" and not $key===$value (type casting selected val unimplemented)
			// This means that mixing string keys and integer key=0 in options is not supported.
			// Note that no type casting is currently done during submission (FIXME)
			if($isMultiple){$isSelected=array_search($key,$value)!==false;}
			else		   {$isSelected=$key==$value;}
			$options.='<option value="'.ent($key).'" '.($isSelected ? 'selected="selected"' : '').'>';
			$options.=ent($val);
			$options.='</option>';
		}
		$value='';
		break;
	case 'submit': 
		$attributes['type']='submit';
		break;
	case 'custom': 
		$templateName='custom';
		$custom=$item['custom']($value,$attributes,$item);
		break;
	case 'button': 
		$templateName='button';
		break;
			
	default:
		fatal('form_render: unknown item type:'.$item['type']);
	}

	if($value===false){$value='';}

	$wrapperClass=implode(' ',val(val($item,'wrapper-attributes',[]),'class',[]));
	$class       =implode(' ',val($attributes                       ,'class',[]));
	unset($item['wrapper-attributes']['class']);
	unset($attributes['class']);
	if(isset($item['error']))
	{
		$class       .=' error';
		$wrapperClass.=' form-item-error';
	}

	$vars=[
		   'value'	            =>ent($value),
		   'cname'		        =>ent($cname),
		   'wrapper_class'      =>ent($wrapperClass),
		   'class'              =>ent($class),
		   'options'	        =>$options,
		   'radios'	            =>$radios,
		   'custom'	            =>$custom,
		   'prefix'		        =>    val($item,'prefix'      ,''),
		   'field_prefix'       =>    val($item,'field-prefix',''),
		   'field_suffix'       =>    val($item,'field-suffix',''),
		   'suffix'		        =>    val($item,'suffix'      ,''),
		   'description'        =>    val($item,'description' ,''),
		   'type'		        =>ent(val($item,'type'        ,'')),
		   'title'		        =>ent(val($item,'title'       ,'')),
		   'wrapper_attributes' =>dlib_render_html_attributes(val($item,'wrapper-attributes',[])),
		   'attributes'	        =>dlib_render_html_attributes($attributes  ),
		   ];
	// Choose inner template and merge into wrapper
	list($wrapperTemplate,$templates)=$customTemplates!==false ? $customTemplates : form_item_templates();
	$wrapperTemplate=val($item,'wrapper-template',$wrapperTemplate);
	$template=val($item,'template',$templates[$templateName]);
	$html=str_replace('$template',$template,$wrapperTemplate);
	// Remove optional parts inside parentheses in template:
	// (<label>$title</label>) => <label>$title</label> or '' if no title. 
	$html=preg_replace_callback('@\((<[^>]*>\$(title|description)</[^>]*>)\)@',function($m)use($vars)
								{
									return $vars[$m[2]]!=='' ? $m[1] : '';
								},$html);
	// Replace variables in template
	$html=preg_replace_callback('@(\$'.implode('|\$',array_keys($vars)).')@',function($var)use($vars)
								{
									return $vars[substr($var[0],1)];
								},$html);
	return $html;
}

//! Form item-wrapper and item templates.
//! These can be overridden globaly in $dlib_config['form_wrapper_template'] and $dlib_config['form_item_templates']
//  or per-form, using custom-templates
function form_item_templates()
{
	global $dlib_config;
	$wrapperTemplate='
$prefix
<div id="edit-$cname-wrapper" class="form-item form-item-$type $wrapper_class" $wrapper_attributes>
    $template
	(<div class="description">$description</div>)
</div>
$suffix';

	$templates=
		[
'input'=>'
	(<label for="edit-$cname">$title</label>)
	$field_prefix
	<input id="edit-$cname" class="$class" $attributes value="$value" />
	$field_suffix',

'radios'=>'
	(<label>$title</label>)
	$field_prefix
	<div id="edit-$cname" class="form-radios $class" $attributes>   
		$radios
	</div>
	$field_suffix',

'select'=>'
	(<label>$title</label>)
	$field_prefix
	<select id="edit-$cname" class="$class" $attributes>
		$options
	</select>
	$field_suffix',

'checkbox'=>'
	$field_prefix
	<label for="edit-$cname" class="option">
		<input type="checkbox" id="edit-$cname" class="$class" $attributes />$title
	</label>
	$field_suffix',

'textarea'=>'
	(<label for="edit-$cname">$title</label>)
	$field_prefix
	<textarea id="edit-$cname" class="$class" $attributes>$value</textarea>
	$field_suffix',

'button'=>'
	(<label for="edit-$cname">$title</label>)
	$field_prefix
	<button id="edit-$cname" class="$class" $attributes>$value</button>
	$field_suffix',

//FIXME !!!!! important
'custom'=>'(<label for="edit-$cname">$title</label>)$custom',
		 ];

	$wrapperTemplate=val($dlib_config,'form_wrapper_template',$wrapperTemplate);
	$template       =array_merge($templates,val($dlib_config,'form_item_templates',[]));

	return [$wrapperTemplate,$templates];
}


//! Create form items for a DBObject using its schema.
//! A submit handler is also added to save the object to the database.
//! Call this function and then customize / unset the items before calling form_process().
//! Only items that are actually set will be updated on POST (others will not be affected).
//! Example: 
//! class User extends DBObject { ... }
//! list($items,$options)=form_dbobject($aUser)
//! unset($items['created']);
//! $items['login']['title']='A prettier title';
//! form_process($items,$options)
function form_dbobject(DBObject $object)
{
	global $dbobject_ui_config;

	$items=[];

	$class=get_class($object);

	$items['form_object']=['type'=>'data','data'=>$object];

	$schema=$class::getSchema();
	foreach($schema as $name=>$desc)
	{
		$item=[];
		if(!form_dbobject_item_type($desc,$item)){continue;}
		if($object!==false && isset($object->$name)){$item['default-value']=$object->$name;}
		$item['title']=val($desc,'title',$name);
		$items[$name]=$item;
	}

	$items['save']=
		['type'=>'submit',
		 'value'=>t('Save'),
		 'submit-2'=>function ($items,$activeButton) use ($class,&$object,$schema)
			{
				$values=dlib_array_column($items,'value');
				// special case for array_of_arrays : dboCopyFromStringArray expects serialized array
				// also for other special cases: (multiple select)
				foreach($items as $name=>$item)
				{
					if($item['type']==='array_of_arrays' ||
					   is_array($values[$name])            ){$values[$name]=serialize($values[$name]);}
				}
				$object->dboCopyFromStringArray($values);
				if(isset($items[$activeButton]['pre-dbobject-save'])){$items[$activeButton]['pre-dbobject-save']($object,$items);}
				$object->save();
			},
		];

	if(!isset($object->dboNewObjectNotSaved))
	{
		$items['delete']=
			['type'=>'submit',
			 'value'=>t('Delete'),
			 'skip-processing'=>true,
			 'submit-2'=>function() use (&$object){$object->delete();}
			];
	}

	$options=['id'=>$class,
			  'attributes'=>['class'=>['form-dbobject']],
			 ];

	// Add any dbobject_ui class information (title, description) if available
	if(isset($dbobject_ui_config) && 
	   isset($dbobject_ui_config['classes']) && 
	   isset($dbobject_ui_config['classes'][$class]))
	{
		require_once 'dbobject-ui.php';
		$classInfo=dbobject_ui_class_setup($class);
		foreach($items as $name=>&$item)
		{
			$thisFieldDesc=val(val($classInfo,'field_desc',[]),$name,[]);
			if(isset($thisFieldDesc['description'])){$item['description']=$thisFieldDesc['description'];}
			if(isset($thisFieldDesc['title'      ])){$item['title'      ]=$thisFieldDesc['title'      ];}
		}
	}

	return [$items,$options];
}

function form_dbobject_item_type(array $fieldSchema,array &$item)
{
	switch($fieldSchema['type'])
	{
	case 'autoincrement': 
	case 'array': 
		return false;
	case 'foreign_key':  $item['type']='int';      break;
	case 'bool':         $item['type']='checkbox'; break;
	case 'enum': 
		$item['type']='select';
		$item['options']=$fieldSchema['values'];
		break;
	case 'html': 
		$item['type']='htmlarea';break;
	case 'string': 
		$item['type']='textfield';break;
	default: 
		$item['type']=$fieldSchema['type'];
	}
	return true;
}

/** @} */



?>