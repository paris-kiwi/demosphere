<?php
/** @defgroup css_template CSS template
 * A very simple css template engine.
 * Write your CSS with a few PHP and syntax helpers:
 * - Variables are replaced : $xyz -> < ?php echo $xyz ?>
 * - Names starting with @@ are replaced : @@xyz(...) -> < ?php echo css_xyz(...) ?>
 * - Expressions starting with @@ are replaced : @@(...) -> < ?php echo (...) ?>
 * Provides color functions extracted from less PHP.
 *
 * **Module use**: (none)
 *  @{
 */

//! $css_template_config : configuration for css_template.
//! List of $css_template_config keys:
//! - variables
//! - svg_to_png_dir
//! - image_override_hook
//! - filename_hook
//! - dest_dir
//! - colormap
//! -::-;-::-
$css_template_config;


//! Compiles file given filename into a new, pure CSS file.
//! This is just a wrapper around css_template_compile().
//! Compiled template files are cached, so this cannot be used for css templates with variables that change on each request.
function css_template($cssFname)
{
	global $css_template_config,$base_url;
	$base_path=preg_replace('@^https?://[^/]*@','',$base_url).'/';
	$destFname=$css_template_config['dest_dir'].'/'.preg_replace('@\.tpl\.css$@','.css',basename($cssFname));
	$cssFileDir=dirname($cssFname);

	// Only recompile css template if it has changed
	if(file_exists($destFname) && 
	   filemtime($destFname) >= filemtime($cssFname)){return $destFname;}

	$css=file_get_contents($cssFname);
	$css=css_template_compile($css,$cssFileDir);

	// Anchor all paths in the CSS, ignoring external and absolute paths.
	$css=preg_replace_callback('/url\(\s*[\'"]?(?![a-z]+:|\/+)([^\'")]+)[\'"]?\s*\)/i',
							   function($matches)use($cssFileDir,$base_path)
							   {
								   return 'url('.$base_path.$cssFileDir.'/'.$matches[1].')';
							   }, 
							   $css);

	file_put_contents($destFname,$css);

	return $destFname;
}

//! Actually compile CSS template (PHP) code into CSS.
//! $cssFileDir is needed to fix relative paths url() 
function css_template_compile($css,$cssFileDir=false,$vars=[])
{
	global $css_template_config;
	$cssFileDir=preg_replace('@/$@','',$cssFileDir);

	// remove all php code from template
	$phpCode=[];
	$tag='|Fx5CKy5f5zhqiQbA';
	$css=preg_replace_callback('@<\?.*\?>@Us',
							   function($m) use(&$phpCode,$tag)
							   {$t=$tag.count($phpCode).'|';$phpCode[$t]=$m[0];return $t;},
							   $css);
	// replace functions in  css:  @@width("abc") -> < ?= css_width("abc") ? >
	// also replaces expressions:  @@(2+3) -> < ?= 2+3 ? >
	while(true)
	{
		// first find function name: @@example(
		if(!preg_match('/@@([a-zA-Z_][a-zA-Z_0-9]*\s*|)(\()/',$css,$m,PREG_OFFSET_CAPTURE)){break;}
		// then find the balanced parentheses expression right after the function name
		if(!preg_match('/\((?>[^()]|(?R))*\)/',substr($css,$m[2][1]),$s)){fatal("Unbalanced parentheses after function in CSS template");}
		// note: if we insert the php directly here, then variables in the inserted code
		// will be incorrectly replaced afterwards. Therefore we use $phpCode
		$t=$tag.count($phpCode).'|';
		$inner=$s[0];
		$phpCode[$t]='<?= '.($m[1][0]==='' ? '' : 'css_'.$m[1][0]).$inner.' ?>';
		$css=substr($css,0,$m[0][1]).$t.substr($css,$m[0][1]+strlen($m[0][0])+strlen($s[0])-1);
		//var_dump($t,$phpCode[$t]);var_dump($m,$s);die('pp');
	}

	// replace @@ by css_ inside php code. (this is a special case, just to make function names coherent)
	$phpCode=str_replace('@@','css_',$phpCode);

	// replace variables in  css:  $xyz -> < ?= $xyz ? >
	$css=preg_replace('/\$[a-zA-Z_][a-zA-Z_0-9]*/','<?= $0 ?>',$css);

	// re-merge removed php back into css
	$css=str_replace(array_keys($phpCode),$phpCode,$css);

	// Execute eval in scope without so many variables
	global $css_template_context;
	$css_template_context['cssFileDir']=$cssFileDir;
	if(isset($css_template_config['variables'])){$vars=array_merge($css_template_config['variables'](),$vars);}
	$tmp=function($cssContents_,$cssVariables_)
	{
		global $css_template_config;
		extract($cssVariables_);
		ob_start();
		$cssEvalOk_=eval('?>'.$cssContents_);
		$cssContents_=ob_get_contents();
		ob_end_clean();
		if($cssEvalOk_===false){fatal('Parse error in CSS');}
		return $cssContents_;
	};
	$css=$tmp($css,$vars);

	return $css;
}

//! Add directory of css file to relative path.
//! Example: images/abc.png => myapp/css/images/abc.png
function css_template_path($src)
{
	global $css_template_context;
	if($src[0]==='/'){return substr($src,1);}
	$dir=$css_template_context['cssFileDir'];
	if($dir===false){return $src;}
	return $dir.'/'.$src;
}

function css_template_cache_clear($slow=false)
{
	global $css_template_config;

	// delete merged and compressed css files in files/css
	foreach(glob($css_template_config['dest_dir'].'/*.css') as $fname)
	{
		unlink($fname);
	}
	if($slow)
	{
		// We need to lock here, otherwise we will delete png's that are being used by sprite_build
		css_template_lock();
		// also delete generated png images (svg to png), this will force sprite recomp
		foreach(glob($css_template_config['svg_to_png_dir'].'/*.png') as $fname)
		{
			unlink($fname);
		}
	}
}

//! Hook that allows calling program to substitute images by other images
function css_template_image_override_hook($fname)
{
	global $css_template_config;
	if(!isset($css_template_config['image_override_hook'])){return $fname;}
	return $css_template_config['image_override_hook']($fname);
}

//! Do a small change to filename (example: add .cache-xyz. suffix)
function css_template_filename_hook($fname)
{
	global $css_template_config;
	if(!isset($css_template_config['filename_hook'])){return $fname;}
	return $css_template_config['filename_hook']($fname);
}

//! Simple flock to avoid concurrency problems when rebuilding css templates, images and also sprites
//! This lock is global to this script execution and is automatically released when this script finishes.
//! Other scripts trying to run this will simply fail (400: bad request).
function css_template_lock()
{
	global $dlib_config;
	static $lockFp=null;

	if($lockFp===null)
	{
		$lockFp=fopen($dlib_config['tmp_dir'].'/css-template-lock.txt', "w+");
		if($lockFp===false){fatal('sprite-lock failed open');}
		$flockOk=flock($lockFp, LOCK_EX | LOCK_NB, $wouldblock);
		if(!$flockOk)
		{
			if($wouldblock){dlib_bad_request_400("Please wait a few minutes. Rebuilding sprites is not over yet.");}
			else
			{fatal('sprite-lock failed lock');}
		}
	}
}


// ********************
// ********************

//! Converts an svg image to css. Conversion is done with ImageMagick.
//! By default uses supersampling and reduces filter size to avoid excessive blurring.
function css_template_svg_to_png($svg,$options=[])
{
	global $css_template_config;
	$defaults=['png_fname'=>false,'background'=>'none','resize'=>false,'colormap'=>false,'level_colors'=>false,'flop'=>false,'optimize'=>true];
	extract(array_merge($defaults,array_intersect_key($options,$defaults)));
	
	if($png_fname===false){$png_fname=basename($svg,'.svg').'.png';}
	$png_fname=$css_template_config['svg_to_png_dir'].'/'.$png_fname;
	
	// don't recompute if already there
	if(file_exists($png_fname) && filemtime($png_fname)>filemtime($svg)){return $png_fname;}

	// avoid concurrency : 2 simultaneous requests try to build same image
	css_template_lock();

	if($colormap!==false){$svg=$css_template_config['colormap']($colormap,$svg);}

    // ImageMagick convert actually uses inkscape (!!) (without inkscape ImageMagick falls back to incorrect svg rendering)
    // So we might as well use inkscape directly (avoids ImageMagick resizing problems and ImageMagick instabilities)
	// Update: 8/2018 rsvg-convert now works pretty well (checked diffs) but is packaged in librsvg2-bin that also includes a gui and dependencies.
	dlib_logexec('/usr/bin/inkscape --export-background-opacity=0 -e '.
        escapeshellarg($png_fname).' '.
        escapeshellarg($svg),
        false,'css_template_svg_to_png failed');

    if($level_colors!==false || $flop!==false || $resize!==false)
    {
        dlib_logexec('convert '.
    			 escapeshellarg($png_fname).' '.
    			 ($level_colors===false ? '' : '+level-colors '.escapeshellarg($level_colors).' ').
                 ($resize===false ? '' : '-geometry '.escapeshellarg($resize).' ').
    			 ($flop===false ? '' : '-flop ').
    			 'PNG32:'.escapeshellarg($png_fname),false,'css_template_svg_to_png 2 failed');
    }

	if($optimize){dlib_logexec('optipng -quiet -o7 '.escapeshellarg($png_fname));}
	return $png_fname;
}

// ********************
// ********************
// The color manipulation functions below are copied/adapted from lessphp
//
// lessphp v0.4.0
// http://leafo.net/lessphp
//  
// LESS css compiler, adapted from http://lesscss.org
//  
// Copyright 2012, Leaf Corcoran <leafot@gmail.com>
// Licensed under MIT or GPLv3, see LICENSE
//  
// // ********************
// ********************

function css_template_parse_color($value)
{
	if(preg_match('@rgba\(([0-9]+),([0-9]+),([0-9]+),([0-9.]+)\)@',$value,$m))
	{
		return ['color',intval($m[1]),intval($m[2]),intval($m[3]),(float)($m[4])];
	}

	$c = ["color", 0, 0, 0];

	$colorStr = substr($value, 1);
	$num = hexdec($colorStr);
	$width = strlen($colorStr) == 3 ? 16 : 256;

	for ($i = 3; $i > 0; $i--) { // 3 2 1
		$t = $num % $width;
		$num /= $width;

		$c[$i] = $t * (256/$width) + $t * floor(16/$width);
	}
	return $c;
}

function css_template_color_to_css($value)
{
	if ($value[0] == 'hsl'){$value=css_template_to_rgb($value);}

	list(, $r, $g, $b) = $value;
	$r = round($r);
	$g = round($g);
	$b = round($b);

	if (count($value) == 5 && $value[4] != 1) { // rgba
		return 'rgba('.$r.','.$g.','.$b.','.$value[4].')';
	}
	return sprintf("#%02x%02x%02x", $r, $g, $b);
	
}

function css_template_fix_color($c) 
{
	foreach (range(1, 3) as $i) 
	{
		if ($c[$i] < 0) $c[$i] = 0;
		if ($c[$i] > 255) $c[$i] = 255;
	}
	return $c;
}


function css_template_clamp($v, $max = 1, $min = 0) 
{
	return min($max, max($min, $v));
}

function css_template_to_rgb($color) 
{
	if ($color[0] == 'color') return $color;

	$H = $color[1] / 360;
	$S = $color[2] / 100;
	$L = $color[3] / 100;

	if ($S == 0) {
		$r = $g = $b = $L;
	} else {
		$temp2 = $L < 0.5 ?
			$L*(1.0 + $S) :
			$L + $S - $L * $S;

		$temp1 = 2.0 * $L - $temp2;

		$r = css_template_to_rgb_helper($H + 1/3, $temp1, $temp2);
		$g = css_template_to_rgb_helper($H, $temp1, $temp2);
		$b = css_template_to_rgb_helper($H - 1/3, $temp1, $temp2);
	}

	// $out = array('color', round($r*255), round($g*255), round($b*255));
	$out = ['color', $r*255, $g*255, $b*255];
	if (count($color) > 4) $out[] = $color[4]; // copy alpha
	return $out;
}

function css_template_to_rgb_helper($comp, $temp1, $temp2) 
{
	if ($comp < 0) $comp += 1.0;
	elseif ($comp > 1) $comp -= 1.0;

	if (6 * $comp < 1) return $temp1 + ($temp2 - $temp1) * 6 * $comp;
	if (2 * $comp < 1) return $temp2;
	if (3 * $comp < 2) return $temp1 + ($temp2 - $temp1)*((2/3) - $comp) * 6;

	return $temp1;
}

function css_template_to_hsl($color) 
{
	if ($color[0] == 'hsl') return $color;

	$r = $color[1] / 255;
	$g = $color[2] / 255;
	$b = $color[3] / 255;

	$min = min($r, $g, $b);
	$max = max($r, $g, $b);

	$L = ($min + $max) / 2;
	if ($min == $max) {
		$S = $H = 0;
	} else {
		if ($L < 0.5)
		$S = ($max - $min)/($max + $min);
		else
		$S = ($max - $min)/(2.0 - $max - $min);

		if ($r == $max) $H = ($g - $b)/($max - $min);
		elseif ($g == $max) $H = 2.0 + ($b - $r)/($max - $min);
		elseif ($b == $max) $H = 4.0 + ($r - $g)/($max - $min);

	}

	$out = ['hsl',
			($H < 0 ? $H + 6 : $H)*60,
			$S*100,
			$L*100,
		   ];

	if (count($color) > 4) $out[] = $color[4]; // copy alpha
	return $out;
}

// ********************
// ********************

function css_template_darken($color,$delta)
{
	$hsl = css_template_to_hsl($color);
	$hsl[3] = css_template_clamp($hsl[3] - $delta, 100);
	return css_template_to_rgb($hsl);
}

function css_template_lighten($color,$delta)
{
	$hsl = css_template_to_hsl($color);
	$hsl[3] = css_template_clamp($hsl[3] + $delta, 100);
	return css_template_to_rgb($hsl);
}

function css_template_saturate($color,$delta)
{
	$hsl = css_template_to_hsl($color);
	$hsl[2] = css_template_clamp($hsl[2] + $delta, 100);
	return css_template_to_rgb($hsl);
}

function css_template_desaturate($color,$delta)
{
	$hsl = css_template_to_hsl($color);
	$hsl[2] = css_template_clamp($hsl[2] - $delta, 100);
	return css_template_to_rgb($hsl);
}

function css_template_spin($color,$delta)
{
	$hsl = css_template_to_hsl($color);
	$hsl[1] = $hsl[1] + $delta % 360;
	if ($hsl[1] < 0) $hsl[1] += 360;
	return css_template_to_rgb($hsl);
}

function css_template_fadeout($color,$delta)
{
	$color[4] = css_template_clamp((isset($color[4]) ? $color[4] : 1) - $delta/100);
	return $color;
}

function css_template_fadein($color,$delta)
{
	$color[4] = css_template_clamp((isset($color[4]) ? $color[4] : 1) + $delta/100);
	return $color;
}

function css_template_mix($first,$second,$weight=50)
{
	$first_a  = isset($first[4] ) ? $first [4] : 1;
	$second_a = isset($second[4]) ? $second[4] : 1;

	$weight /= 100.0;

	$w = $weight * 2 - 1;
	$a = $first_a - $second_a;

	$w1 = (($w * $a == -1 ? $w : ($w + $a)/(1 + $w * $a)) + 1) / 2.0;
	$w2 = 1.0 - $w1;

	$new = ['color',
			$w1 * $first[1] + $w2 * $second[1],
			$w1 * $first[2] + $w2 * $second[2],
			$w1 * $first[3] + $w2 * $second[3],
		   ];

	if ($first_a != 1.0 || $second_a != 1.0) {
		$new[] = $first_a * $weight + $second_a * ($weight - 1);
	}

	return css_template_fix_color($new);
}


// ********************
// ******* Function called from css template
// ********************

// ********** color functions

function css_darken($color,$delta)
{
	return css_template_color_to_css(css_template_darken(css_template_parse_color($color),$delta));
}

function css_lighten($color,$delta)
{
	return css_template_color_to_css(css_template_lighten(css_template_parse_color($color),$delta));
}

function css_saturate($color,$delta)
{
	return css_template_color_to_css(css_template_saturate(css_template_parse_color($color),$delta));
}

function css_desaturate($color,$delta)
{
	return css_template_color_to_css(css_template_desaturate(css_template_parse_color($color),$delta));
}

function css_spin($color,$delta)
{
	return css_template_color_to_css(css_template_spin(css_template_parse_color($color),$delta));
}

function css_fadeout($color,$delta)
{
	return css_template_color_to_css(css_template_fadeout(css_template_parse_color($color),$delta));
}

function css_fadein($color,$delta)
{
	return css_template_color_to_css(css_template_fadein(css_template_parse_color($color),$delta));
}

function css_mix($first,$second,$weight=50)
{
	return css_template_color_to_css(css_template_mix(css_template_parse_color($first),
													  css_template_parse_color($second),$weight));
}

// ********** image related functions

function css_width($srcImage,$spriteFname=false)
{
	$srcImage=css_template_path($srcImage);
	$tmp=getimagesize($srcImage);
	if($tmp===false){fatal('css_width failed for :'.$srcImage);}
	return $tmp[0];
}

function css_height($srcImage,$spriteFname=false)
{
	$srcImage=css_template_path($srcImage);
	$tmp=getimagesize($srcImage);
	if($tmp===false){fatal('css_width height for :'.$srcImage);}
	return $tmp[1];
}

function css_svg_to_png($svg,$options=[])
{
	global $css_template_config;
	$svg=css_template_path($svg);
	$svg=css_template_image_override_hook($svg);
	$res=css_template_svg_to_png($svg,$options);
	$res=css_template_image_override_hook($res);
	return '/'.css_template_filename_hook($res);
}

//! makes path absolute and calls filename hook (for example, to add a caching prefix)
function css_file($fname)
{
	global $css_template_config;
	$fname=css_template_path($fname);
	$fname=css_template_image_override_hook($fname);
	return '/'.css_template_filename_hook($fname);
}

// ********** CSS3 browser compatibility 

function css_rgba($color,$alpha)
{
	$c=css_template_parse_color($color);
	return 'rgba('.$c[1].','.$c[2].','.$c[3].','.$alpha.')';
}

function css_linear_gradient($dir,$color1,$pos1,$color2,$pos2,$bgImage=false)
{
	// FIXME: this is very approximate
	// FIXME: handle transparency (rgba) for filter (or just forget it??)
	// FIXME: handle angles, ... ie direction, test on all browsers...
	// https://developer.mozilla.org/en-US/docs/Web/CSS/linear-gradient
	// http://www.colorzilla.com/gradient-editor/
	// http://caniuse.com/#search=gradient
	$dirs=['to bottom'=>['moz'=>'top'   ,'webkit-old'=>'left top, left bottom','ie'=>0],
		   'to top'   =>['moz'=>'bottom','webkit-old'=>'left bottom, left top','ie'=>0],
		   'to right' =>['moz'=>'left'  ,'webkit-old'=>'left top, right top'  ,'ie'=>1],
		   'to left'  =>['moz'=>'right' ,'webkit-old'=>'left top, right top'  ,'ie'=>1],
		   ];
	if(!isset($dirs[$dir])){fatal('Unsupported gradient direction:'.$dir);}
	$d=$dirs[$dir];
	
	$background=$bgImage ? 'background-image' :'background';
	return "
$background: -moz-linear-gradient(".$d['moz']." $color1 $pos1, $color2 $pos2); /* FF3.6+ */
$background: -webkit-gradient(linear, ".$d['webkit-old'].", color-stop($pos1,$color1), color-stop($pos2,$color2)); /* Chrome,Safari4+ */
$background: -webkit-linear-gradient(".$d['moz']." $color1 $pos1,$color2 $pos2); /* Chrome10+,Safari5.1+ */
$background: -o-linear-gradient(".$d['moz']." $color1 $pos1,$color2 $pos2); /* Opera 11.10+ */
$background: -ms-linear-gradient(".$d['moz']." $color1 $pos1,$color2 $pos2); /* IE10+ */
$background: linear-gradient($dir, $color1 $pos1,$color2 $pos2); /* W3C */
";
	// Not supporting IE6-8 anymore, and IE 9 gradients have problems with rounded corners.
	// filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='$color1', endColorstr='$color2',GradientType=".$d['ie']." ); /* IE6-8 */
}



/** @} */

?>