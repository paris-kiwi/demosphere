<?php

//! Translate a string, optionally using placeholder args.
function t($s,array $args = [])
{
	static $cache=[];
	if($s===null){$cache=[];return;}
	if($s===false){return $cache;}
	if(isset($cache[$s])){$res=$cache[$s];}
	else
	{
		// The following is just a performance optimisation for the simple query:
		//$res=db_result_check("SELECT translation FROM translations WHERE msgid='%s'",$s);
		// It caches the built query. It also inlines everything to avoid multiple fct calls.
		// Measured time gain : x3
		{
			global $dbConnection;
			static $sql=false;
			if($sql===false)
			{
				$sql=db_build_query("SELECT translation FROM translations WHERE msgid='%s'",'x');
				$sql=substr($sql,0,-2);
			}
			$query=$sql.mysqli_real_escape_string($dbConnection,$s)."'";
			$qres=mysqli_query($dbConnection,$query);
			if(!$qres){fatal('translations::t(): query failed');}
			$row=mysqli_fetch_row($qres);
			$res=$row===null ? false : $row[0];
		}
		$cache[$s]=$res;
	}
	if($res===false || $res===''){$res=$s;}
	if(!empty($args)){$res=translation_format_string($res,$args);}
	return $res;
}

//! No-op function used to tell translation system that this string needs to be translated.
//! This can be used, for example, when immediate translation has a big performance impact.
function dt(string $s)
{
	return $s;
}

/**
 * Replaces placeholders with sanitized values in a string.
 *
 * @param $string
 *   A string containing placeholders.
 * @param $args
 *   An associative array of replacements to make. Occurrences in $string of
 *   any key in $args are replaced with the corresponding value, after
 *   sanitization. The sanitization function depends on the first character of
 *   the key:
 *   - !variable: Inserted as is. Use this for text that has already been
 *     sanitized.
 *   - @variable: Escaped to HTML using check_plain(). Use this for anything
 *     displayed on a page on the site.
 *   - %variable: Escaped as a placeholder for user-submitted content using
 *     drupal_placeholder(), which shows up as <em>emphasized</em> text.
 *
 * @see t()
 */
function translation_format_string(string $string,array $args = []) 
{
	// Transform arguments before inserting them.
	foreach ($args as $key => $value) 
	{
		switch ($key[0]) {
		case '@':
			// Escaped only.
			$args[$key] = ent($value);
			break;
		case '!':// Pass-through.
			break;
		case '%':// Escaped and placeholder.
			$args[$key] = '<em class="placeholder">' . ent($value) . '</em>';
			break;
		default:
			fatal('translation_format_string: invalid placeholder:'.$key[0]);
		}
	}
	return strtr($string, $args);
}

function dlib_format_date(string $description,string $ts,$ts2=false): string
{
	global $dlib_config;
	if(isset($dlib_config['format_date'])){return $dlib_config['format_date']($description,$ts,$ts2);}
	fatal('unimplemented');
}

?>